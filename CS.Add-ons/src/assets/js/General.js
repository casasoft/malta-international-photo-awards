 
 $( document ).ready(function() {
        
    // initialisation Masonry
    var $grid = $('.masonry-grid').masonry({
        itemSelector: '.masonry-grid-item',
        percentPosition: true,
        columnWidth: '.grid-sizer',
    });

    // layout Masonry after each image loads
    $('.masonry-grid').imagesLoaded().progress( function() {
        $grid.masonry();
    });
    
	//lazyloading for background images
	document.addEventListener('lazybeforeunveil', function (e) {
            var target = $(e.target);
            var path = target.attr("data-lazyload");
			var inlineBackground = "background-image: url(" + path + ")";
			target.attr("style", inlineBackground );
            target.removeAttr('data-lazyload')
            
     });
     //initialisation of carousels
     
     $('.list-featured-photos-container').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<div class="slick-prev"><i class="ion-chevron-left"></i>',
        nextArrow: '<div class="slick-next"><i class="ion-chevron-right"></i>',
        responsive: [
            {
                breakpoint: 1450,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
           
        ]
    });


    //photo full page carousel
    $('.submission-images-container').slick({
        dots: true,
        // autoplay: true,
        // autoplaySpeed: 2000,
        prevArrow: '<div class="slick-prev"><i class="ion-chevron-left"></i>',
        nextArrow: '<div class="slick-next"><i class="ion-chevron-right"></i>',

    });

   
    //appending jury modals to the end of the body tag
    var juryModal = $('.list-jury-container .modal')
    juryModal.appendTo(".jury-modals-container");

    //append search modal to the end of the body
    var searchModal = $('.search-modal');
    searchModal.appendTo(".search-modal-container");





    //photo full page modal
    var loader =  window.top.document.querySelector('.loader-container');
        
    $(".photo-modal-toggle").click(function(e) {
        e.preventDefault();

        var photoFullPageModal = window.top.$('#imageFullPage');
        photoFullPageModal.modal('hide')

        var opener = $(this);
        var photoFullPageLink = opener.attr('data-url')
      
        var photoIframe = photoFullPageModal.find('#photo-full-page-iframe');
        photoIframe.attr("src", photoFullPageLink);
         
        photoIframe.css('height', '80vh');
        loader.style.display = 'block';
        photoFullPageModal.modal('show');
        
    })

    //change height of iframe
    window.top.document.getElementById('photo-full-page-iframe').onload = function() {
        //console.log('iframe loaded')
        var newHeight = window.top.document.getElementById('photo-full-page-iframe').contentWindow.document.body.offsetHeight + "px";

        $(this).css('height', newHeight);
        //console.log("applied height immediately after load " + newHeight)

        loader.style.display = 'none';
        var lastHeight = 0;
        var that = $(this);

        //16:9 ratio for full page photo
        var widthOfImageContainer = $('.current-submission-photos').width();
        var heightOfImageContainer = $('.current-submission-photos').height(0.5624*widthOfImageContainer);

        function checkHeightAndCallSetTimeoutIfDifferent() {
            var newHeight = window.top.document.getElementById('photo-full-page-iframe').contentWindow.document.body.offsetHeight;
            var newHeightWithPx = newHeight + "px";

            that.css('height', newHeightWithPx);
            //console.log("newheightwithpx " + newHeightWithPx);
            
            if (newHeight != lastHeight) {
                //Call this again
                setTimeout(function() {
                    checkHeightAndCallSetTimeoutIfDifferent();
                }, 1000);

                lastHeight = newHeight;
            }  
        }    

        checkHeightAndCallSetTimeoutIfDifferent();
	
    }

    // $( window ).resize(function() {
    //     var widthOfImageContainer = $('.current-submission-photos').width();
    //     var heightOfImageContainer = $('.current-submission-photos').height(0.5624*widthOfImageContainer);

    // });

    //focus when modal is open
    $('.search-modal').on('shown.bs.modal', function() {
        $('.search-modal-text-container').find('input:first').focus();
    })   

});


