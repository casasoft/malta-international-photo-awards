﻿using CSTemplate.Code.Services.Members;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.ExportMembers;
using CSTemplate.Code.Services.ExportMembers;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    
    public partial class ExportMembersPageDataController : USNBaseController
    {
        private readonly IExportMembersFilterByDate _exportMembersFilterByDate;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetExportMembersPageService _getExportMembersPageService;

        public ExportMembersPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IExportMembersFilterByDate exportMembersFilterByDate,
            IUmbracoHelperService umbracoHelperService,
            IGetExportMembersPageService getExportMembersPageService
            )
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _exportMembersFilterByDate = exportMembersFilterByDate;
            _umbracoHelperService = umbracoHelperService;
            _getExportMembersPageService = getExportMembersPageService;
        }
        
        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult ExportMembersPage(RenderModel model)
        {
            var _umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            ExportMembersPageModel exportMembersPageModel =
                _getExportMembersPageService.Get(_umbracoHelper, model.Content);

            return base.Populate(exportMembersPageModel);
        }

        [HttpPost]
        public virtual ActionResult ExportMembersPage(DateTime dateFrom, DateTime dateTo)
        {
            var _umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var fileName = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(_umbracoHelper, Enums.DictionaryKey.Export_Members_Excel_File_Name);

            var urlBuilder = new System.UriBuilder(Request.Url.AbsoluteUri)
            {
                Path = Url.Content(ConstantValues.Submitted_Photo_Source_Address),
            };
            string absoluteUrl = urlBuilder.ToString();

            var exportTheMembersAsExcelFile = _exportMembersFilterByDate.Export(_umbracoHelper, dateFrom, dateTo);
            if (exportTheMembersAsExcelFile.Length>0)
            {
                fileName = string.Format(fileName, DateTime.Now);
                return File(exportTheMembersAsExcelFile, "application/octet-stream", $"{fileName}");
            }
            return View();
            
        }
    }
}