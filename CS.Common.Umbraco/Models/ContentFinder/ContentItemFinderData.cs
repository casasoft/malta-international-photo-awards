﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace CS.Common.Umbraco.Models.ContentFinder
{
    public class ContentFinderItem
    {
        public string TemplateAlias { get; set; }
        public IPublishedContent Content { get; set; }
    }
}
