﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Order
{

    public interface IGetOrderByOrderRefIdService
    {
        OrderPoco Get(string orderRefId);
    }

    [Service]
    public class GetOrderByOrderRefIdService : IGetOrderByOrderRefIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetOrderByOrderRefIdService(IGetCurrentUmbracoDatabaseService
            getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public OrderPoco Get(string orderRefId)
        {
            var db = _getCurrentUmbracoDatabaseService.Get();

            var sql = new Sql()
                .Select("*")
                .From(DatabaseConstantValues.OrdersTableName + " " +
                DatabaseConstantValues.aliasForOrdersPocoTableNameAlias)
                .Where(DatabaseConstantValues.aliasForOrdersPocoTableNameAlias + "." +
                DatabaseConstantValues.Orders_OrderRefId_Field_Name + " = '" +
                orderRefId + "'");

            return db.Fetch<OrderPoco>(sql).FirstOrDefault();
        }
    }
}
