﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.ViewOrder
{
    public class ViewOrderPageModel : USNBaseViewModel
    {
        public string ButtonText { get; set; }
        public string OrderTitleText { get; set; } 
        public string OrderDateText { get; set; }
        public string OrderNumberText { get; set; }
        public string OrderStatusText { get; set; }
        public string OrderTotalText { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderNumber { get; set; }
        public string OrderStatus { get; set; }
        public decimal OrderTotal { get; set; }
        public Enums.PaymentStatus PaymentStatus { get; set; }

        public string BillingTitleText { get; set; }
        public string BillingNameText { get; set; }
        public string BillingEmailText { get; set; }
        public string BillingContactText { get; set; }
        public string BillingAddressText { get; set; }

        public string BillingName { get; set; }
        public string BillingEmail { get; set; }
        public string BillingContact { get; set; }
        public string BillingAddress { get; set; }

        public string OrderSummaryTitle { get; set; }
        public List<_OrderPhotosModel> ListOfOrderPhotos { get; set; }
        public string TablePhotoTitle { get; set; }
        public string TablePriceTitle { get; set; }
        public string TableSubtotalTitle { get; set; }
        public decimal SubtotalPrice { get; set; }
        public bool IsAStoryTelling { get; set; }
        public bool IsOrderOwner { get; set; }

        public Enums.PaymentStatus PayStatus { get; set; }

        public _PaymentButtonsModel PaymentButtons { get; set; }
        public ViewOrderPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
