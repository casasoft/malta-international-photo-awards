﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Models.Paging;
using CS.Common.Umbraco.Services.Pager;
using Umbraco.Web;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;

namespace CSTemplate.Code.Services.Common
{

    public interface IGetPagerModelService
    {
        PagerModel CreatePagerModelForCurrentUrl(UmbracoHelper umbracoHelper,
            int pageSize,
            int totalResults,
            string itemTypeTextSingular = "",
            string itemTypeTextPlural = "");
    }

    [Service]
    public class GetPagerModelService : IGetPagerModelService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly ICreatePagingModelForCurrentUrlService _createPagingModelForCurrentUrlService;

        public GetPagerModelService(
            IUmbracoHelperService umbracoHelperService,
            ICreatePagingModelForCurrentUrlService createPagingModelForCurrentUrlService)
        {
            _umbracoHelperService = umbracoHelperService;
            _createPagingModelForCurrentUrlService = createPagingModelForCurrentUrlService;
        }

        public PagerModel CreatePagerModelForCurrentUrl(UmbracoHelper umbracoHelper,
            int pageSize,
            int totalResults,
            string itemTypeTextSingular = "",
            string itemTypeTextPlural = "")
        {
            return _createPagingModelForCurrentUrlService.CreatePagerModelForCurrentUrl(
                umbracoHelper,
                new CreatePagingModelForCurrentUrlService.CreatePagerModelForCurrentUrlParameters()
                {
                    ItemsPerPage = pageSize,
                    TotalNumberOfItems = totalResults,
                    ItemTypeTextSingular = itemTypeTextSingular,
                    ItemTypeTextPlural = itemTypeTextPlural
                });
            //_umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
            //                Enums.DictionaryKey.Dictionary_Pager_Blog_Item_Type_Text_Singular)
        }

    }
}
