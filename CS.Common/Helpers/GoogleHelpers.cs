﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Helpers
{
    public static class GoogleHelpers
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gpsCoordinates">GPS coordinates with comma seperated eg.35.907016, 14.507835 </param>
        /// <returns></returns>
        public static string GetGoogleMapsLinkWithGPSCoordinates(string gpsCoordinates)
        {
            var trimedGpsCoordinates = gpsCoordinates.Replace(" ", "");
            return $"https://www.google.com/maps/place/{trimedGpsCoordinates}";
        }
    }
}
