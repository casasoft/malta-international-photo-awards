﻿using System;
using System.Collections.Generic;
using System.Linq;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Constants;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using System.Web;

namespace CS.Common.Umbraco.Services.Umbraco.Wrappers.Services
{
    public interface IUmbracoHelperService
    {
        IPublishedContent TypedContent(UmbracoHelper umbracoHelper, int id);
        IPublishedContent TypedContent(UmbracoContext umbracoContext, int id);
        string GetDictionaryValue(UmbracoHelper umbracoHelper, string key);
        string GetDictionaryValueOrUpdateDefaultValueIfEmpty(UmbracoHelper umbracoHelper, Enum key);
        IPublishedContent TypedMedia(UmbracoHelper umbracoHelper, int id);
        IPublishedContent TypedMedia(UmbracoContext umbracoContext, int id);
        string NiceUrl(UmbracoHelper umbracoHelper, int id);
        IPublishedContent TypedMember(UmbracoHelper umbracoHelper, int id);
        /// <summary>
        /// Load the root node for current domain.   This is done because there could be a site which has multiple domains and thus this will
        /// take into consideration the current domain.
        /// </summary>
        /// <param name="umbracoHelper"></param>
        /// <returns></returns>
        IPublishedContent LoadRootNodeForCurrentDomain(UmbracoHelper umbracoHelper);
        /// <summary>
        /// This will get the first node found by document type, and if there are multiple nodes using the same document type 
        /// (e.g. The page is of type Text Page) we can setup a queryIdentifier to get the specified node.
        /// </summary>
        /// <param name="umbracoHelper"></param>
        /// <param name="documentType">This will be set as a Constant Value with the name of the Document Type</param>
        /// <param name="queryIdentifier">This will be set as a parameter with the name of the queryIdentifier</param>
        /// <returns></returns>
        IPublishedContent GetNodeFromDocumentTypeAndQueryString(UmbracoHelper umbracoHelper, string documentType, string queryIdentifier = "");
    }

    [Service]
    public class UmbracoHelperWrapperService : IUmbracoHelperService
    {
        public IPublishedContent TypedContent(UmbracoHelper umbracoHelper, int id)
        {
            return umbracoHelper.TypedContent(id);
        }

        public IPublishedContent TypedContent(UmbracoContext umbracoContext, int id)
        {
            return TypedContent(new UmbracoHelper(umbracoContext), id);
        }

        public IPublishedContent LoadRootNodeForCurrentDomain(UmbracoHelper umbracoHelper)
        {
            // Get currrent domain
            var currentDomain = HttpContext.Current.Request.Url.Host;

            // Get all the home node domains
            var allRootHomeNodes = umbracoHelper.TypedContentAtRoot()
                .Where(x => x.DocumentTypeAlias == ConstantValues.Home_Page_Document_Type_Alias);

            IPublishedContent currentDomainHome = null;

            // Check to see which domain the current request is in
            foreach (var rootHomeNode in allRootHomeNodes)
            {
                var homeNodeAssignedDomains = ApplicationContext.Current.Services.DomainService
                    .GetAssignedDomains(rootHomeNode.Id, false);
                //When it is null (no domains) you need to load the first root node
                if (homeNodeAssignedDomains == null)
                {
                    currentDomainHome = rootHomeNode;
                    break;
                }
                else
                {
                    foreach (var homeNodeDomain in homeNodeAssignedDomains)
                    {
                        if (string.Compare(homeNodeDomain.DomainName, currentDomain, StringComparison.OrdinalIgnoreCase) == 0)
                        {
                            currentDomainHome = rootHomeNode;
                            break;
                        }
                    }
                }
            }
            if (currentDomainHome == null)
            {
                currentDomainHome = allRootHomeNodes.FirstOrDefault();
            }

            return currentDomainHome;
        }

        public IPublishedContent GetNodeFromDocumentTypeAndQueryString(UmbracoHelper umbracoHelper, string documentType, string queryIdentifier = "")
        {
            var publishedContent = LoadRootNodeForCurrentDomain(umbracoHelper)
                .Children(x => x.DocumentTypeAlias == documentType);

            if (!String.IsNullOrEmpty(queryIdentifier))
            {
                publishedContent = publishedContent.Where(x => x.GetPropertyValue(ConstantValues.Query_Identifier_Property_Alias).Equals(queryIdentifier));
            }

            return publishedContent.FirstOrDefault(); ;
        }

        public IPublishedContent TypedMember(UmbracoHelper umbracoHelper, int id)
        {
            return umbracoHelper.TypedMember(id);
        }

        public string NiceUrl(UmbracoHelper umbracoHelper, int id)
        {
            return umbracoHelper.NiceUrl(id);
        }

        public IPublishedContent TypedMedia(UmbracoHelper umbracoHelper, int id)
        {
            return umbracoHelper.TypedMedia(id);
        }
        public IPublishedContent TypedMedia(UmbracoContext umbracoContext, int id)
        {
            return TypedMedia(new UmbracoHelper(umbracoContext), id);
        }

        public string GetDictionaryValue(UmbracoHelper umbracoHelper, string key)
        {
            return umbracoHelper.GetDictionaryValue(key);
        }
        public string GetDictionaryValueOrUpdateDefaultValueIfEmpty(UmbracoHelper umbracoHelper, Enum key)
        {
            string sKey = EnumHelpers.ConvertEnumToStringByType(key.GetType(), key, true, false, true);

            string dictionaryValue = umbracoHelper.GetDictionaryValue(sKey);

            if (string.IsNullOrWhiteSpace(dictionaryValue))
            {


                var localizationService = ApplicationContext.Current.Services.LocalizationService;
                var item = localizationService.GetDictionaryItemByKey(sKey);
                string descriptionValue = EnumHelpers.GetDescriptionAttributeValue(key);



                if (item != null)
                {
                    //This seems actually to be irrelevant

                    var culture = umbracoHelper.CultureDictionary.Culture;


                    var dictionaryTranslations = item.Translations;
                    var translation = dictionaryTranslations.FirstOrDefault(x => x.Language.CultureInfo == culture);

                    if (translation == null)
                    {
                        var translationForDefaultLanguage = dictionaryTranslations
                            .FirstOrDefault(x => x.Language.CultureInfo.Name.Contains(ConstantValues.LanguageDefaultName));
                        dictionaryValue = translationForDefaultLanguage.Value;
                    }

                    var language = translation != null ? translation.Language : null;

                    if (language != null)
                    {
                        dictionaryTranslations =
                       dictionaryTranslations.RemoveAllFromEnumerable(x => x.Language == language);

                        var newTranslations = new List<IDictionaryTranslation>(dictionaryTranslations)
                       {
                           new DictionaryTranslation(language, descriptionValue)
                       };
                        item.Translations = newTranslations;
                        localizationService.Save(item);

                        dictionaryValue = descriptionValue;

                    }

                }
                else
                {
                    dictionaryValue = descriptionValue;
                    localizationService.CreateDictionaryItemWithIdentity(sKey, null, descriptionValue);
                }
            }

            return dictionaryValue;
        }
    }
}
