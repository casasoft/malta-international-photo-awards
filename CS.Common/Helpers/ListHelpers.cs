﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace CS.Common.Helpers
{
    public static class ListHelpers
    {
        /// <summary>
        /// This helper gets the page elements from a list of elements given and the page number
        /// </summary>
        /// <typeparam name="T">Generic model passed</typeparam>
        /// <param name="list">List of T elements passed</param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize">Number of elements displayed in the page</param>
        /// <returns>Returns a list with the elements available for the current page</returns>
        public static IEnumerable<T> GetPageFromList<T>(IEnumerable<T> list, int pageNumber, int pageSize)
        {
            return list.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public static int GetTotalPagesForPageSizeFromTotalItems(int totalItems, int pageSize)
        {
            return (int)Math.Ceiling((double)totalItems / (double)pageSize);
        }

        private class DuplicateEqualityComparer<T> : IEqualityComparer<T>
        {
            private readonly Func<T, object> _valueFunction;

            public DuplicateEqualityComparer(Func<T, object> valueFunction)
            {
                _valueFunction = valueFunction;
            }

            public bool Equals(T x, T y)
            {
                if (x == null && y == null)
                {
                    return true;
                }
                else if (x == null || y == null)
                {
                    return false;
                }
                else
                {
                    return _valueFunction(x).Equals(_valueFunction(y));
                }
            }

            public int GetHashCode(T obj)
            {
                if (obj != null)
                {
                    var value = _valueFunction(obj);
                    if (value != null)
                    {
                        return value.GetHashCode();
                    }
                }
                return 0;
            }
        }

        public static IEnumerable<T> RemoveDuplicates<T>(IEnumerable<T> source, Func<T, object> valueFunction)
        {
            return source.Distinct(new DuplicateEqualityComparer<T>(valueFunction));
        }

        public static List<T> ShallowCloneListOfItems<T>(List<T> items)
        {
            var clonedListOfItems = new List<T>();

            foreach (var item in items)
            {
                var clonedItem = ObjectHelpers.ShallowClone(item);
                clonedListOfItems.Add(clonedItem);
            }

            return clonedListOfItems;
        }

        public static string ConvertListStringToSingleString(List<string> lines)
        {
            var stringBuilder = new StringBuilder();
            for (int i = 0; i < lines.Count; i++)
            {
                if (i == lines.Count - 1)
                {
                    if (!String.IsNullOrWhiteSpace(lines[i]))
                    {
                        stringBuilder.AppendLine(lines[i]);
                    }
                }
                else
                {
                    stringBuilder.AppendLine(lines[i]);
                }
            }
            return stringBuilder.ToString();
        }

        public static IEnumerable<string> SplitOnLength(this string input, int length)
        {
            int index = 0;
            while (index < input.Length)
            {
                if (index + length < input.Length)
                    yield return input.Substring(index, length);
                else
                    yield return input.Substring(index);

                index += length;
            }
        }

        public static void AddRangeIfNotNullOrEmpty<T>(this List<T> list, List<T> toAdd)
        {
            if (toAdd != null && toAdd.Any())
            {
                list.AddRange(toAdd);
            }
        }

        public static void AddIfNotAlready<T>(this List<T> list, T toAdd)
        {
            if (!list.Contains(toAdd))
            {
                list.Add(toAdd);
            }
        }

        public delegate string StringConverterDelegate(string s);
        public delegate bool CompareTwoItemsHandler<in TItem1, in TItem2>(TItem1 item1, TItem2 item2);

        private static int getDistinctListOfItemsInEnumerableByComparingProperty_comparer<T>(T x, T y, Expression<Func<T, IComparable>> propertyToCompare)
        {
            int cmpResult = -1;
            var value1 = (IComparable)ReflectionHelpers.GetValueForObjectBySelector(x, propertyToCompare);
            var value2 = (IComparable)ReflectionHelpers.GetValueForObjectBySelector(y, propertyToCompare);
            bool isTheSame = false;
            if (value1 == null && value2 == null)
            {
                cmpResult = 0;
            }
            else if (value1 != null && value2 != null)
            {
                cmpResult = (value1.CompareTo(value2));
            }
            return cmpResult;
        }

        public static List<T> GetDistinctListOfItemsInEnumerableByComparingProperty<T>(this IEnumerable<T> enumerable,
                                                                    Expression<Func<T, IComparable>> propertyToCompare)
        {


            return GetDistinctListOfItemsInEnumerable(enumerable, (x, y) => getDistinctListOfItemsInEnumerableByComparingProperty_comparer(x, y, propertyToCompare));

        }

        public static List<T> GetDistinctListOfItemsInEnumerable<T>(
            this IEnumerable<T> enumerable,
            Comparison<T> comparer)
        {
            if (enumerable == null) throw new ArgumentNullException("enumerable");
            if (comparer == null) throw new ArgumentNullException("comparer");

            var list = enumerable.ToList();
            for (int i = 0; i < list.Count; i++)
            {
                var itemA = list[i];
                for (int j = i + 1; j < list.Count; j++)
                {
                    var itemB = list[j];
                    int cmpResult = comparer(itemA, itemB);
                    if (cmpResult == 0)
                    {
                        list.RemoveAt(j);
                        j--;
                    }
                }

            }
            return list;
        }



        /// <summary>
        /// Returns an item from a list, which is based on the greatest condition.  E.g, take the item with the largest date
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="comparer">A comparer which returns whether item 1 is 'larger' or 'better' than item 2, for the condition required.</param>
        /// <returns></returns>
        public static T TakeItemFromListBasedOnGreatestCondition<T>(this IEnumerable<T> list, Func<T, T, bool> comparer)
        {

            T mostItem = default(T);
            if (list != null)
            {
                foreach (var item in list)
                {
                    if (object.Equals(mostItem, default(T)) ||
                        (comparer(item, mostItem))) //item is 'larger' 
                    {
                        mostItem = item;
                    }
                }
            }
            return mostItem;
        }
        /// <summary>
        /// Compare the internal items of a list by reference of each item
        /// </summary>
        public static bool CompareListItems<T>(params List<T>[] lists)
             where T : class

        {
            CompareTwoItemsHandler<T, T> comparer = (a, b) => a == b;
            return CompareListItems(comparer, lists);
        }
        /// <summary>
        /// Compare the internal items of a list by reference of each item
        /// </summary>
        public static bool CompareBasicListItems<T>(params List<T>[] lists)
            where T : IComparable
        {
            CompareTwoItemsHandler<T, T> comparer = (a, b) => a.CompareTo(b) == 0;
            return CompareListItems(comparer, lists);
        }
        /// <summary>
        /// Compare the internal items of a list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="comparer"></param>
        /// <param name="lists"></param>
        /// <returns></returns>
        public static bool CompareListItems<T>(CompareTwoItemsHandler<T, T> comparer, params List<T>[] lists)
        {
            //check lengths
            int listLength = lists[0].Count;
            bool ok = true;

            for (int i = 1; i < lists.Length; i++)
            {
                var checkList = lists[i];
                ok = checkList.Count == listLength;
                if (!ok) break;
            }
            if (ok)
            {
                var firstList = lists[0];
                foreach (var listItem in firstList)
                {
                    bool found = false;
                    for (int i = 1; i < lists.Length; i++)
                    {
                        //Check the item in each list
                        var checkList = lists[i];
                        foreach (var checkItem in checkList)
                        {
                            found = comparer(listItem, checkItem);
                            if (found) break;
                        }
                        if (found) break;
                    }
                    ok = found;
                    if (!ok) break;
                }
            }
            return ok;
        }



       
        public static List<T> RemoveAllFromEnumerable<T>(this IEnumerable<T> list, Predicate<T> comparer)
        {
            int n = 0;
            return RemoveAllFromEnumerable(list, comparer, out n);
        }
        public static List<T> RemoveAllFromEnumerable<T>(this IEnumerable<T> list, Predicate<T> comparer, out int result)
        {
            List<T> tmpList = new List<T>();
            tmpList.AddRange(list);
            result = tmpList.RemoveAll(comparer);

            return tmpList;
        }
        public static int RemoveAll<T>(this IList<T> list, Predicate<T> comparer)
        {
            int result;
            var afterRemoval = RemoveAllFromEnumerable(list, comparer, out result);

            list.Clear();
            list.AddRange(afterRemoval);
            return result;
        }

        public static IEnumerable<T> Sort<T>(this IEnumerable<T> list, Comparison<T> comparer)
        {
            List<T> tmpList = new List<T>();
            tmpList.AddRange(list);
            tmpList.Sort(comparer);
            return tmpList;

        }

        public static void AddRange<T>(this IList<T> list, IEnumerable<T> itemsToAdd)
        {
            foreach (var item in itemsToAdd)
            {
                list.Add(item);
            }
        }

        public static IList<T> FindAll<T>(this IEnumerable<T> list, Predicate<T> predicate)
        {
            List<T> retList = new List<T>();
            retList.AddRange(Array.FindAll(list.ToArray(), predicate));
            return retList;
        }


        /// <summary>
        /// Removes items from list, that are not in the given page. IMPORTANT, from index should start from ZERO!
        /// </summary>
        /// <param name="list"></param>
        /// <param name="fromIndex"></param>
        /// <param name="pageSize"></param>
        public static void RemoveItemsFromListNotInPage(IList list, int fromIndex, int pageSize)
        {
            for (int i = 0; i < fromIndex; i++)
            {
                list.RemoveAt(0);
            }
            while (list.Count > pageSize)
            {
                list.RemoveAt(pageSize);
            }

        }

        public static NameValueCollection ParseStringIntoNameValueCollection(string s, char valueDelimeter, char keyValueDelimiter)
        {
            string[] tokens = s.Split(keyValueDelimiter);
            NameValueCollection nv = new NameValueCollection();
            for (int i = 0; i < tokens.Length; i++)
            {
                string t = tokens[i];
                string key = t.Substring(0, t.IndexOf(valueDelimeter));
                string value = t.Substring(t.IndexOf(valueDelimeter) + 1);
                nv[key] = value;
            }
            return nv;
        }

        /// <summary>
        /// Converts a name value collection to a string
        /// </summary>
        /// <param name="nv"></param>
        /// <param name="keyValuePairDelimeter">The delimter between key/value pairs.  For querystring/forms, this should be '&'</param>
        /// <param name="valueDelimeter">The delimter between the key and the value, default should be '='</param>
        /// <param name="keyConverter">A function to call to convert the key before apending to return string. Can be null</param>
        /// <param name="valueConverter">A function to call to convert the value before apending to return string. Can be null</param>
        /// <returns></returns>
        public static string ConvertNameValueCollectionToString(NameValueCollection nv, string keyValuePairDelimeter = "&",
            string valueDelimeter = "=",
            StringConverterDelegate keyConverter = null, StringConverterDelegate valueConverter = null)
        {

            StringBuilder sb = new StringBuilder();
            if (nv != null)
            {
                for (int i = 0; i < nv.AllKeys.Length; i++)
                {
                    if (i > 0)
                        sb.Append(keyValuePairDelimeter);
                    string key = nv.AllKeys[i];
                    string value = nv[key];
                    if (keyConverter != null)
                        key = keyConverter(key);
                    if (valueConverter != null)
                        value = valueConverter(value);
                    sb.Append(key + valueDelimeter + value);
                }
            }
            return sb.ToString();
        }


        public static List<T> EnumerateHashTableValues<T>(this Hashtable ht)
        {
            var list = GetListFromEnumerator<DictionaryEntry>(ht.GetEnumerator());
            List<T> result = new List<T>();
            foreach (var dicEntry in list)
            {
                result.Add((T)dicEntry.Value);
            }
            return result;
        }
        public static List<T> EnumerateHashTableKeys<T>(this Hashtable ht)
        {
            return GetListFromEnumerator<T>(ht.Keys.GetEnumerator());
        }
        public static List<T> GetListFromSingleItem<T>(T item)
        {
            return GetListFromItems<T>(item);
        }
        public static List<T> GetListFromItems<T>(params T[] items)
        {

            List<T> list = new List<T>();
            if (items != null)
            {
                foreach (var item in items)
                {

                    list.Add(item);
                }
            }
            return list;
        }

        public static List<T> GetListFromEnumerator<T>(this IEnumerable<T> enumerable)
        {
            return GetListFromEnumerator(enumerable.GetEnumerator());
        }
        public static List<T> GetListFromEnumerator<T>(this IEnumerable enumerable)
        {
            return GetListFromEnumerator<T>(enumerable.GetEnumerator());
        }
        public static List<T> GetListFromEnumerator<T>(this IEnumerator<T> enumerator)
        {
            return GetListFromEnumerator<T>((IEnumerator)enumerator);
        }
        public static List<T> GetListFromEnumerator<T>(this IEnumerator enumerator)
        {

            List<T> list = new List<T>();

            while (enumerator.MoveNext())
            {

                T entry = (T)enumerator.Current;
                list.Add(entry);

            }

            return list;

        }
        public static List<object> GetListFromEnumerator(this IEnumerator enumerator)
        {
            return GetListFromEnumerator<object>(enumerator);
        }

        private class __comparer<T> : IComparer<T>
        {
            private Comparison<T> comparer = null;
            public __comparer(Comparison<T> p)
            {
                this.comparer = p;
            }
            public int Compare(T x, T y)
            {
                return comparer(x, y);
            }
        }

        public static int BinarySearch<T>(this IList<T> list, T itemToFind, Comparison<T> p)
        {


            int index = _BinarySearch<T>(list, itemToFind, p, 0, list.Count - 1);
            return index;

        }
        private static int _BinarySearch<T>(this IList<T> list, T itemToFind, Comparison<T> p, int low, int high)
        {
            if (high < low)
                return -1;
            int middle = (low + high) / 2;
            int cmpVal = p(itemToFind, list[middle]);
            if (cmpVal == -1)
                return _BinarySearch(list, itemToFind, p, low, middle - 1);
            else if (cmpVal == 1)
                return _BinarySearch(list, itemToFind, p, middle + 1, high);
            else
                return middle;

        }
        /*
        int binary_search(int a[], int low, int high, int target) {
    if (high < low)
        return -1;
    int middle = (low + high)/2;
    if (target < a[middle])
        return binary_search(a, low, middle-1, target);
    else if (target > a[middle])
        return binary_search(a, middle+1, high, target);
    else if (target == a[middle])
        return middle;
}*/
        public static List<T> BinarySearchAll<T>(this List<T> list, T itemToFind, Comparison<T> p)
        {

            int index = list.BinarySearch(itemToFind, p);
            int startIndex = index, endIndex = index;
            List<T> result = new List<T>();
            if (index > -1)
            {
                for (int i = index; i >= 0; i--)
                {
                    if (p(itemToFind, list[i]) == 0)
                    {
                        result.Insert(0, list[i]);
                    }
                }
                for (int i = index + 1; i < list.Count; i++)
                {
                    if (p(itemToFind, list[i]) == 0)
                    {
                        result.Add(list[i]);
                    }
                }
            }
            return result;
        }
        public static T FindElem<T>(this IEnumerable<T> list, Predicate<T> p)
        {
            T result = default(T);
            if (list != null)
            {
                IEnumerator<T> enumerator = list.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (p(enumerator.Current))
                    {
                        result = enumerator.Current;
                        break;
                    }
                }
            }
            return result;
        }
        public static void RemoveBasicDataTypesExactDuplicates<T>(this List<T> list) where T : IComparable
        {
            RemoveDuplicates(list, (a, b) => a.CompareTo(b));
        }
        public static void RemoveDuplicates<T>(this List<T> list, Comparison<T> p)
        {
            list.Sort(p);
            for (int i = 0; i < list.Count - 1; i++)
            {
                T item1 = list[i];
                T item2 = list[i + 1];
                if (p(item1, item2) == 0)
                {
                    list.RemoveAt(i + 1);
                    i--;
                }
            }
        }
        public static object FindElemObj(this IEnumerable list, Predicate<object> p)
        {


            IEnumerator enumerator = list.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (p(enumerator.Current))
                {
                    return enumerator.Current;
                }
            }
            return null;
        }
        /// <summary>
        /// Gets X random elements from the list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="totalElements"></param>
        /// <returns></returns>
        public static List<T> GetXRandomElements<T>(this IEnumerable<T> list, int totalElements)
        {


            List<T> currList = new List<T>();
            currList.AddRange(list);
            List<T> result = new List<T>();
            int cnt = 0;
            while (cnt < totalElements && currList.Count > 0)
            {
                int rndIndex = RandomHelpers.GetInt(0, currList.Count - 1);
                result.Add(currList[rndIndex]);
                currList.RemoveAt(rndIndex);
                cnt++;
            }
            return result;
        }

        public static T GetRandomElement<T>(this IEnumerable<T> enumerable)
        {
            var list = enumerable.ToList();
            if (list.Count > 0)
            {
                int n = RandomHelpers.GetInt(0, list.Count - 1);
                return list[n];
            }
            return default(T);
        }

        public static void RandomizeListInSame<T>(this IList<T> list)
        {
            List<T> tmpList = new List<T>();
            tmpList.AddRange(list);
            list.Clear();
            while (tmpList.Count > 0)
            {
                int rndIndex = RandomHelpers.GetInt(0, tmpList.Count - 1);
                list.Add(tmpList[rndIndex]);
                tmpList.RemoveAt(rndIndex);
            }



        }
        public static IEnumerable<T> RandomizeEnumerable<T>(this IEnumerable<T> list)
        {
            List<T> rndList = list.ToList();
            RandomizeListInSame(rndList);
            return rndList;


        }
        public static string JoinList<T>(IEnumerable<T> list, Func<T, string> toStringHandler, string delimiter, bool addIfNullOrEmpty = false, string lastDelimiter = null, bool removeDelimiterIfItemEndsWithDelimiter = true)
        {

            List<string> strings = new List<string>();
            if (list != null)
            {
                foreach (var item in list)
                {
                    if (addIfNullOrEmpty || item != null)
                    {
                        strings.Add(toStringHandler(item));
                    }
                }
            }
            return JoinList(strings, delimiter, addIfNullOrEmpty, lastDelimiter, removeDelimiterIfItemEndsWithDelimiter);
        }
        public static string JoinList(IEnumerable<string> list, string delimiter, bool addIfNullOrEmpty = false, string lastDelimiter = null, bool removeDelimiterIfItemEndsWithDelimiter = true)
        {
            return JoinList((IEnumerable)list, delimiter, addIfNullOrEmpty, lastDelimiter, removeDelimiterIfItemEndsWithDelimiter);
        }

        public static string JoinList(IEnumerable listItems,
            string delimiter = ", ",
            bool addIfNullOrEmpty = false,
            string lastDelimiter = null,
            bool removeDelimiterIfItemEndsWithDelimiter = true)
        {
            if (listItems != null)
            {
                List<object> list = listItems.GetListFromEnumerator<object>();
                if (!addIfNullOrEmpty)
                {
                    list =
                        list.RemoveAllFromEnumerable(
                            (item => string.IsNullOrEmpty(item == null ? null : item.ToString())));
                }

                StringBuilder sb = new StringBuilder();
                int i = 0;
                int count = list.Count();
                int lastIndex = count - 1;
                var delimiterTrimmed = delimiter.Trim();
                foreach (object oItem in list)
                {
                    string item = oItem == null ? null : oItem.ToString();

                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        item = item.Trim();
                        if (item.EndsWith(delimiterTrimmed))
                        {
                            item = item.Substring(0, item.Length - delimiterTrimmed.Length);
                        }
                    }

                    if (sb.Length > 0)
                    {
                        if (lastDelimiter == null || i < lastIndex)
                        {
                            // not last delimiter
                            sb.Append(delimiter);
                        }
                        else
                        {
                            sb.Append(lastDelimiter);
                        }
                    }
                    sb.Append(item);

                    i++;
                }
                return sb.ToString();
            }
            else
            {
                return null;
            }

        }

        //public static string JoinList<T>(IList<T> list)
        //{
        //    return JoinList(list, (item => item.ToString()), ",", false, null);
        //}

        public static List<int> GetListOfIntFromString(string s, string delim)
        {
            List<long> longs = GetListOfLongFromString(s, delim);
            List<int> ints = new List<int>();
            for (int i = 0; i < longs.Count; i++)
            {
                ints.Add((int)longs[i]);
            }
            return ints;

        }
        public static List<long> GetListOfLongFromString(string s, string delim)
        {
            if (s == null) s = "";
            string[] tokens = s.Split(new string[] { delim }, StringSplitOptions.RemoveEmptyEntries);
            List<long> list = new List<long>();
            for (int i = 0; i < tokens.Length; i++)
            {
                long tmp = 0;
                if (long.TryParse(tokens[i], out tmp))
                {
                    list.Add(tmp);
                }
            }
            return list;
        }
        public static List<int> ConvertArrayToListOfInts<T>(IEnumerable<T> arr)
        {
            List<int> result = new List<int>();
            foreach (var item in arr)
            {
                result.Add(Convert.ToInt32(item));
            }
            return result;
        }
        public static List<long> ConvertArrayToListOfLongs<T>(IEnumerable<T> arr)
        {
            List<long> result = new List<long>();
            foreach (var item in arr)
            {
                result.Add(Convert.ToInt64(item));
            }
            return result;
        }
        public static string ConvertListToString<T>(this IEnumerable<T> list, string delim = ", ")
        {
            return ConvertListToString(list, null, delim);

        }
        public static List<double> GetListOfDoubleFromString(string s, string delim)
        {
            if (s == null) s = "";
            string[] tokens = s.Split(new string[] { delim }, StringSplitOptions.RemoveEmptyEntries);
            List<double> list = new List<double>();
            for (int i = 0; i < tokens.Length; i++)
            {
                double tmp = 0;
                if (double.TryParse(tokens[i], out tmp))
                {
                    list.Add(tmp);
                }
            }
            return list;
        }
        public delegate string ConvertItemToString<T>(T item);
        public static string ConvertListToString(this IEnumerable list,
            ConvertItemToString<dynamic> itemConverter = null,
            string delim = ", ",
            bool skipEmptyValues = true)
        {


            string s = null;
            if (list != null)
            {
                List<string> stringList = new List<string>();

                foreach (var item in list)
                {
                    string value = null;
                    if (itemConverter != null)
                    {
                        value = itemConverter(item);
                    }
                    else
                    {
                        value = item != null ? item.ToString() : null;
                    }
                    if (!skipEmptyValues || value != null)
                    {
                        stringList.Add(value);
                    }

                }
                s = TextHelpers.AppendStrings(delim, stringList);
            }
            return s;

        }
        public static string ConvertListToString<T>(this IEnumerable<T> list,
            ConvertItemToString<T> itemConverter = null,
            string delim = ", ",
            bool skipEmptyValues = true)
        {
            ConvertItemToString<dynamic> itemConvertorDynamic = null;
            if (itemConverter != null)
            {
                itemConvertorDynamic = x => itemConverter(x);
            }
            return ConvertListToString(list, itemConvertorDynamic, delim, skipEmptyValues);
        }

        /// <summary>
        /// Divide a list into an amount of lists of same type
        /// </summary>
        /// <typeparam name="T">General type</typeparam>
        /// <param name="List">The full list</param>
        /// <param name="splitInto">Amount of lists to split into</param>
        /// <param name="horizontal">If true then items are spread in horizontal order A B C and in next row D E F etc... </param>
        /// <returns></returns>
        public static List<List<T>> SplitList<T>(IEnumerable<T> list, int splitInto, bool horizontal)
        {
            var listItem = list.ToList();
            int listSize = (int)Math.Ceiling((double)listItem.Count() / (double)splitInto);

            return SplitListIntoMultipleLists(listItem, listSize, false, horizontal);


        }


        /*public static List<List<T>> SplitListIntoListsOfSize<T>(IList<T> List, int smallListSize, bool horizontal, bool addNullElementsToEqualizeLists)
        {
            int amtLists = (int)Math.Ceiling((double)List.Count / (double)smallListSize);
            List<List<T>> smallLists = SplitList(List, amtLists, horizontal);
            if (smallLists[smallLists.Count - 1].Count != smallListSize)
            {
                List<T> smallList = smallLists[smallLists.Count - 1];
                int remaining = smallListSize - smallList.Count;
                for (int i = 0; i < remaining; i++)
                {
                    smallList.Add(default(T));
                }
            }
            return smallLists;

        }
        */
        /// <summary>
        /// Splits a list into a list of lists, of size page.  Used for building tables
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">Original List</param>
        /// <param name="listSize">List size per each item</param>
        /// <param name="nullType">The null type to fill, normally it should be 'null'</param>
        /// <returns></returns>
        public static List<List<T>> SplitListIntoMultipleLists<T>(IList<T> list, int listSize)
        {
            return SplitListIntoMultipleLists(list, listSize, true, true);
        }
        /// <summary>
        /// Splits a list into a list of lists, of size page.  Used for building tables
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">Original List</param>
        /// <param name="listSize">List size per each item</param>
        /// <param name="nullType">The null type to fill, normally it should be 'null'</param>
        /// <param name="horiztonal">The direction of the splitting.  If horizontal, then order will be A B C D and on next set E F G H etc.. else it will split
        /// them vertically</param>
        /// <returns></returns>
        public static List<List<T>> SplitListIntoMultipleLists<T>(IList<T> list, int listSize, bool horiztonal)
        {
            return SplitListIntoMultipleLists(list, listSize, true, horiztonal);
        }
        /// <summary>
        /// Splits a list into a list of lists, of size page.  Used for building tables
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">Original List</param>
        /// <param name="listSize">List size per each item</param>
        /// <param name="nullType">The null type to fill, normally it should be 'null'</param>
        /// <param name="fillLastSet">Wheter to keep on filling the last set with the null types</param>
        /// <param name="horiztonal">The direction of the splitting.  If horizontal, then order will be A B C D and on next set E F G H etc.. else it will split
        /// <returns></returns>

        public static List<List<T>> SplitListIntoMultipleLists<T>(IEnumerable<T> list, int listSize, bool fillLastSet, bool horizontal)
        {
            List<List<T>> result = new List<List<T>>();
            //List<T> list = originalList.ToList();

            if (list.Count() == 0) return result;

            int amtLists = (int)Math.Ceiling((double)list.Count() / (double)listSize);
            for (int i = 0; i < amtLists; i++)
            {
                result.Add(new List<T>());
            }
            int count = 0;
            foreach (var item in list)
            {
                int listIndex = horizontal ? (count % amtLists) : ((int)Math.Floor((double)count / (double)listSize));
                result[listIndex].Add(item);
                count++;
            }
            if (fillLastSet)
            {
                for (int i = 0; i < result.Count; i++)
                {
                    if (result[i].Count < listSize)
                    {
                        int amtRemaining = listSize - result[i].Count;
                        for (int j = 0; j < amtRemaining; j++)
                        {
                            result[i].Add(default(T));
                        }
                    }
                }
            }
            /*

            while (index < count)
            {
                List<T> tmpList = new List<T>();
                for (int j = 0; j < listSize; j++)
                {
                    if (index < count)
                    {
                        tmpList.Add(list[index]);
                        index++;
                    }
                    else
                    {
                        if (fillLastSet)
                        {
                            tmpList.Add(default(T));
                        }
                    }
                }
                result.Add(tmpList);
            }

            if (horizontal)
            {
                List<List<T>> vertList = new List<List<T>>();
                for (int i =0; i < listSize; i++)
                {
                    vertList.Add(new List<T>());
                }
                int maxRowSize = (list.Count + listSize - 1) / listSize;
                for (int y = 0; y < listSize; y++)
                {
                    for (int x =0; x < maxRowSize; x++)
                    {
                        if (x < result.Count && y < result[x].Count)
                        {
                            vertList[y].Add(result[x][y]);
                        }
                    }
                }
                result = vertList;
            }*/
            return result;
        }



        private class CombinationGenerator<T>
        {
            private List<List<T>> list = null;
            private List<List<T>> getCombinations(int startIndex)
            {
                List<List<T>> result = new List<List<T>>();
                if (startIndex < list.Count)
                {
                    var list1 = list[startIndex];
                    foreach (var item in list1)
                    {

                        if (startIndex < list.Count - 1)
                        {
                            List<List<T>> combinations = getCombinations(startIndex + 1);
                            foreach (var comb in combinations)
                            {
                                List<T> itemResult = new List<T>();
                                itemResult.Add(item);
                                itemResult.AddRange(comb);
                                result.Add(itemResult);

                            }

                        }
                        else
                        {
                            List<T> itemResult = new List<T>();
                            itemResult.Add(item);
                            result.Add(itemResult);
                        }

                    }
                }
                return result;
            }
            public List<List<T>> Generate(List<List<T>> itemList)
            {
                this.list = itemList;
                return getCombinations(0);

            }
        }

        /// <summary>
        /// This will iterate through all the lists one by one and create one list with one of each, 
        /// one after the other
        /// e.g":
        /// List 1 : A,B,C
        /// List 2 : D,E,F
        /// List 3 : G,H
        /// 
        /// Output List: A,D,G,B,E,H,C,F
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lists"></param>
        /// <returns></returns>
        public static List<T> CombineListsSequentially<T>(params IEnumerable<T>[] inputLists)
        {
            List<T> outputResult = new List<T>();

            List<List<T>> lists = new List<List<T>>();
            foreach (var listItem in inputLists)
            {
                lists.Add(listItem.ToList());
            }


            int biggestListCount = 0;
            for (int i = 0; i < lists.Count; i++)
            {
                biggestListCount = Math.Max(lists[i].Count, biggestListCount);
            }

            for (int i = 0; i < biggestListCount; i++)
            {
                for (int j = 0; j < lists.Count; j++)
                {
                    List<T> list = lists[j];
                    if (i < list.Count)
                    {
                        T value = list[i];
                        outputResult.Add(value);
                    }

                }
            }
            return outputResult;
        }

        /// <summary>
        /// Generates a list of ALL combinations.  It will combine each in each list, with every list item. 
        /// The total amount of combinations is all sizes of lists multiplied together
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<List<T>> GenerateCombinations<T>(List<List<T>> list)
        {
            CombinationGenerator<T> gen = new CombinationGenerator<T>();
            return gen.Generate(list);
        }


        public static List<TOutput> ConvertAll<TInput, TOutput>(IEnumerable enumerable, Converter<TInput, TOutput> converter)
        {



            List<TInput> list = GetListFromEnumerator<TInput>(enumerable);

            List<TOutput> result = new List<TOutput>();
            foreach (var item in list)
            {
                result.Add(converter(item));
            }
            return result;
        }


        public static List<TItem> ConvertItemsToList<TItem>(params TItem[] items)
        {
            List<TItem> list = new List<TItem>();
            foreach (var item in items)
            {
                if (item != null)
                {
                    list.Add(item);
                }

            }
            return list;
        }

        public static ListItem[] ConvertListItemCollectionToArray(ListItemCollection listItems)
        {


            List<ListItem> list = new List<ListItem>();
            foreach (ListItem li in listItems)
            {
                list.Add(li);
            }
            return list.ToArray();
        }
        /*public static void SortByMultipleComparers<T>(ist<T> list, params Comparison<T>[] comparers)
        {
            list.Sort((item1, item2) =>
            {
                int result = 0;
                for (int i = 0; i < comparers.Length; i++)
                {
                    result = comparers[i](item1, item2);
                    if (result != 0)
                    {
                        break;
                    }
                }
                return result;
            });
        }*/

        /// <summary>
        /// Sorts the list and returns a new one
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="comparers"></param>
        /// <returns></returns>
        public static IEnumerable<T> SortByMultipleComparers<T>(IEnumerable<T> list, params Comparison<T>[] comparers)
        {
            List<T> newList = new List<T>();
            if (list != null)
            {
                newList.AddRange(list);
            }
            SortInListByMultipleComparers(newList, comparers);
            return newList;

        }
        /// <summary>
        /// Sorts in the same list provided
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="comparers"></param>
        public static void SortInListByMultipleComparers<T>(this ICollection<T> list, params Comparison<T>[] comparers)
        {
            var tmpList = list.ToList();

            tmpList.Sort((item1, item2) =>
            {
                int result = 0;
                for (int i = 0; i < comparers.Length; i++)
                {
                    result = comparers[i](item1, item2);
                    if (result != 0)
                    {
                        break;
                    }
                }
                return result;
            });

            list.Clear();
            foreach (var item in tmpList)
            {
                list.Add(item);
            }
        }


        public static bool RemoveFromConcurrentDictionary<TKey, TValue>(this System.Collections.Concurrent.ConcurrentDictionary<TKey, TValue> _dict, TKey key)
        {
            bool result = false;
            TValue tmp;
            result = _dict.TryRemove(key, out tmp);
            return result;
        }
        /// <summary>
        /// This checks if a collection is a set, list, etc, and clears it. throws an exception if its not any
        /// </summary>
        /// <param name="c"></param>
        /// <param name="item"></param>
        public static void CopyEnumerableToCollection<T>(ICollection<T> c, IEnumerable<T> enumerable, bool clearCollectionFirst = false)
        {
            if (clearCollectionFirst)
                ClearEnumerable(c);
            foreach (var item in enumerable)
            {
                AddToEnumerable(c, item);
            }

        }

        /// <summary>
        /// This checks if a collection is a set, list, etc, and clears it. throws an exception if its not any
        /// </summary>
        /// <param name="c"></param>
        /// <param name="item"></param>
        public static void ClearEnumerable(IEnumerable c)
        {


            if (c is IList)
            {
                IList list = (IList)c;
                list.Clear();

            }
            else
            {
                Type t = c.GetType();
                var method = t.GetMethod("Clear");
                method.Invoke(c, null);

            }


        }
        /// <summary>
        /// This checks if a collection is a set, list, etc, and adds the item. throws an exception if its not any
        /// </summary>
        /// <param name="c"></param>
        /// <param name="item"></param>
        public static bool AddToEnumerable(IEnumerable c, object item)
        {
            bool b = false;
            if (c is IList)
            {
                IList list = (IList)c;
                list.Add(item);
                b = true;
            }
            else
            {
                Type t = c.GetType();
                var method = t.GetMethod("Add");
                method.Invoke(c, new object[] { item });
            }

            return b;
        }
        /// <summary>
        /// This checks if a collection is a set, list, etc, and adds the item. throws an exception if its not any
        /// </summary>
        /// <param name="c"></param>
        /// <param name="item"></param>
        public static bool RemoveFromEnumerable(IEnumerable c, object item)
        {
            bool b = false;
            if (c is IList)
            {
                IList list = (IList)c;
                list.Remove(item);
                b = true;
            }
            else
            {
                Type t = c.GetType();
                var method = t.GetMethod("Remove");
                object result = method.Invoke(c, new object[] { item });
                if (result is bool)
                {
                    b = (bool)result;
                }



            }


            return b;
        }
        public static T GetSmallestInList<T>(IEnumerable<T> list, Comparison<T> comparer)
        {

            bool firstItem = true;
            T smallest = default(T);
            foreach (var item in list)
            {
                if (firstItem || comparer(item, smallest) < 0)
                {
                    smallest = item;
                    firstItem = false;
                }
            }

            return smallest;

        }


        public static IEnumerable<T> GetPagedList<T>(IEnumerable<T> list, int pgNo, int pgSize)
        {
            return list.ToList().Skip((pgNo - 1) * pgSize).Take(pgSize);
        }

        public static T GetLargestInList<T>(IEnumerable<T> list, Comparison<T> comparer)
        {
            bool firstItem = true;

            T largest = default(T);
            foreach (var item in list)
            {
                if (firstItem || comparer(item, largest) > 0)
                {
                    largest = item;
                    firstItem = false;
                }
            }

            return largest;

        }


        public static bool ContainsItem<T>(this IEnumerable<T> list, Func<T, bool> comparer)
        {
            if (list != null)
            {
                var matchingItems = list.Where(comparer);
                return matchingItems.Any();
            }
            else
            {
                return false;
            }
        }

        public static bool ContainsAll<T>(IEnumerable<T> enumerable, params T[] itemsToCheck)
        {

            bool b = true;
            if (enumerable != null)
            {
                if (itemsToCheck != null)
                {
                    var list = enumerable.ToList();
                    foreach (var item in itemsToCheck)
                    {
                        if (!list.Contains(item))
                        {
                            b = false;
                            break;
                        }
                    }
                }
            }
            return b;
        }

        public static List<T> CloneList<T>(this IEnumerable<T> list)
        {
            List<T> clonedList = new List<T>();
            clonedList.AddRange(list);
            return clonedList;
        }

        public static string Join<T>(this IEnumerable<T> list, string delimiter, bool addIfNullOrEmpty = false, string lastDelimiter = null, bool removeDelimiterIfItemEndsWithDelimiter = true, Func<T, string> valueToStringHandler = null)
        {
            if (!addIfNullOrEmpty)
            {
                list = list.RemoveAllFromEnumerable((item => string.IsNullOrWhiteSpace(item != null ? item.ToString() : null)));
            }

            StringBuilder sb = new StringBuilder();
            int i = 0;
            int count = list.Count();
            int lastIndex = count - 1;
            foreach (T item in list)
            {
                string sItem = null;
                if (item != null)
                {
                    if (valueToStringHandler != null)
                    {
                        sItem = valueToStringHandler(item);
                    }
                    else
                    {
                        sItem = item.ToString();
                    }
                }
                if (!string.IsNullOrWhiteSpace(sItem))
                {
                    sItem = sItem.Trim();
                    if (sItem.EndsWith(delimiter))
                    {
                        sItem = sItem.Substring(0, sItem.Length - delimiter.Length);
                    }
                }

                if (sb.Length > 0)
                {
                    if (lastDelimiter == null || i < lastIndex)
                    {
                        // not last delimiter
                        sb.Append(delimiter);
                    }
                    else
                    {
                        sb.Append(lastDelimiter);
                    }
                }
                sb.Append(sItem);
                i++;
            }
            return sb.ToString();
        }

        public static IEnumerable<T> TruncateList<T>(this IEnumerable<T> listToTruncate, int numberOfDesiredRemainingItems)
        {
            return listToTruncate.Take(numberOfDesiredRemainingItems);
        }

        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = list.Count;
            while (n > 1)
            {
                byte[] box = new byte[1];
                do provider.GetBytes(box);
                while (!(box[0] < n * (Byte.MaxValue / n)));
                int k = (box[0] % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }

        public static void MoveItemAtIndexToFront<T>(this List<T> list, int index)
        {
            T item = list[index];
            list.RemoveAt(index);
            list.Insert(0, item);
        }
    }
}
