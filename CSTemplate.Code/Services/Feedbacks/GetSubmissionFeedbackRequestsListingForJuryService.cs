﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Photo_Feedback_Requests;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Photos;
using CSTemplate.Code.Services.Users;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Feedbacks
{

    public interface IGetSubmissionFeedbackRequestsListingForJuryService
    {
        PhotoFeedbackRequestsJuryListingPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent publishedContent, int pageNumber = 1);
    }

    [Service]
    public class GetSubmissionFeedbackRequestsListingForJuryService : IGetSubmissionFeedbackRequestsListingForJuryService
    {

        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetPagerModelService _getPagerModelService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetFirstPhotoUrlFilterBySubmissionIdService _getFirstPhotoUrlFilterBySubmissionIdService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly ICheckSubmissionFeedbackPaymentStatusFilterBySubmissionIdService _checkSubmissionFeedbackPaymentStatusFilterBySubmissionIdService;

        public GetSubmissionFeedbackRequestsListingForJuryService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IUmbracoHelperService umbracoHelperService,
            IGetPagerModelService getPagerModelService,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService,
            IGetFirstPhotoUrlFilterBySubmissionIdService getFirstPhotoUrlFilterBySubmissionIdService,
            IGetUserFilterByUserIdService getUserFilterByUserIdService,
            ICheckSubmissionFeedbackPaymentStatusFilterBySubmissionIdService checkSubmissionFeedbackPaymentStatusFilterBySubmissionIdService
        )
        {
            _checkSubmissionFeedbackPaymentStatusFilterBySubmissionIdService = checkSubmissionFeedbackPaymentStatusFilterBySubmissionIdService;
            _umbracoHelperService = umbracoHelperService;
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _getFirstPhotoUrlFilterBySubmissionIdService = getFirstPhotoUrlFilterBySubmissionIdService;
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _getPagerModelService = getPagerModelService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public PhotoFeedbackRequestsJuryListingPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent publishedContent, int pageNumber = 1)
        {
            var photoFeedbackRequestsJuryListingPageModel = new PhotoFeedbackRequestsJuryListingPageModel(publishedContent);

            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            List<_ItemPhotoModel> listOfPhotoModels = new List<_ItemPhotoModel>();


            var submissionsTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = Constants.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = Constants.DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = Constants.DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = Constants.DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = Constants.DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = Constants.DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionTitleField = Constants.DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionFeedbackRequestField = Constants.DatabaseConstantValues.Submissions_Feedback_Request_Field_Name;
            var submissionFeedbackDescriptionField = Constants.DatabaseConstantValues.Submissions_Feedback_Description_Field_Name;
            var submissionStatusEnumIdField = Constants.DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;


            var featuredFieldId = Constants.DatabaseConstantValues.Photos_Featured_Field_Name;

            var orderByDescValue = Constants.DatabaseConstantValues.ORDER_BY_DESC;

            var sqlQuery = new Sql($@" 
                     SELECT
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId},
                        {submissionsTableAlias}.{submissionTitleField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionFeedbackRequestField},
                        {submissionsTableAlias}.{submissionFeedbackDescriptionField}
                            FROM
                                {submissionsTableName}
                                     WHERE {submissionsTableAlias}.{submissionFeedbackRequestField}={Convert.ToInt32(Constants.Enums.FeedbackStatus.FeedbackRecieved)}  
                                        AND 
                                        ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                                        AND 
                                        {submissionsTableAlias}.{submissionFeedbackDescriptionField} IS NULL
                                         ORDER BY {submissionPrimaryKeyField} {orderByDescValue}
            ");

            var mySubmissionsSqlResult = umbracoDb.Query<SubmissionPoco>(sqlQuery).ToList();

            var listOfItemPhotoModel = new List<_ItemPhotoModel>();
            foreach (var submittedPhoto in mySubmissionsSqlResult)
            {
                var feedbackRequestPaymentStatus = _checkSubmissionFeedbackPaymentStatusFilterBySubmissionIdService.Check(submittedPhoto.Id);
                if (feedbackRequestPaymentStatus)
                {
                    var awardCategory = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, submittedPhoto.CategoryId);
                    //var status = string.IsNullOrEmpty(submittedPhoto.FeedbackDescription) == true ? CSTemplate.Code.Constants.Enums.FeedbackStatus.WaitingForFeedback : CSTemplate.Code.Constants.Enums.FeedbackStatus.FeedbackRecieved;
                    var status = string.IsNullOrEmpty(submittedPhoto.FeedbackDescription) == true ? EnumHelpers.GetDescriptionAttributeValue(CSTemplate.Code.Constants.Enums.FeedbackStatus.WaitingForFeedback) : EnumHelpers.GetDescriptionAttributeValue(CSTemplate.Code.Constants.Enums.FeedbackStatus.FeedbackRecieved);
                    var photographerInfo = _getUserFilterByUserIdService.Get(umbracoHelper, submittedPhoto.UserId);
                    var photographerFullName = photographerInfo.Name + " " + photographerInfo.Surname;
                    listOfItemPhotoModel.Add(new _ItemPhotoModel
                    {
                        ItemPhotoImgURL = _getFirstPhotoUrlFilterBySubmissionIdService.Get(submittedPhoto.Id),
                        ItemPhotoDate = submittedPhoto.Date,
                        ItemPhotoName = photographerFullName,
                        ItemPhotoAward = awardCategory.AwardItem.AwardItemName,
                        ItemPhotoLocation = submittedPhoto.Location,
                        ItemPhotoDescription = submittedPhoto.Description,
                        ItemPhotoCategory = awardCategory.AwardCategoryItemName,
                        ItemPhotoTitle = submittedPhoto.Title,
                        SubmissionId = submittedPhoto.Id.ToString(),
                        CurrentSubmissionStatus = status.ToString(),
                        SubmissionStatusTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, CSTemplate.Code.Constants.Enums.DictionaryKey.Submission_Status_Title_Text)


                    });
                }
            }
            photoFeedbackRequestsJuryListingPageModel.DownloadButtonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, CSTemplate.Code.Constants.Enums.DictionaryKey.Download_Feedback_Requested_Submissions_Button_Text);
            int itemTotalAmounts = listOfItemPhotoModel.Count;
            //how many item to show on the page
            photoFeedbackRequestsJuryListingPageModel.ItemsPerPage = CSTemplate.Code.Constants.ConstantValues.Mansory_Item_Count;
            var pageSize = photoFeedbackRequestsJuryListingPageModel.ItemsPerPage;
            //total number of listing items.
            photoFeedbackRequestsJuryListingPageModel.ItemsTotalAmount = itemTotalAmounts;

            listOfItemPhotoModel = listOfItemPhotoModel.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper, photoFeedbackRequestsJuryListingPageModel.ItemsPerPage, photoFeedbackRequestsJuryListingPageModel.ItemsTotalAmount);
            photoFeedbackRequestsJuryListingPageModel.ListingItemsAsMasonryPartial = new _ListingItemsAsMasonryPartialModel()
            {
                Pagination = pagerModel,
                ListOfPhotos = listOfItemPhotoModel
            };
            return photoFeedbackRequestsJuryListingPageModel;
        }
    }
}
