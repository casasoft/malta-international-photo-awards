﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.Shared
{
    public class _FooterPaymentMethodModel 
    {
        public string FooterPhotoTitleImgURL { get; set; }
        public string FooterText { get; set; }
        public string FooterPaymentMethodText { get; set; }
        public string FooterPaymentMethodImgURL1 { get; set; }
        public string FooterPaymentMethodImgURL2 { get; set; }
        public string FooterPaymentMethodImgURL3 { get; set; }

    }
}
