﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Models.Shortlist_Photos;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.ShortListedPhotos;
using Umbraco.Web.Models;
using CSTemplate.Code.Services.AwardCategory;
using Umbraco.Web;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ShortlistPhotosListingPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetShortlistPhotosListingPageDataControllerService _getShortlistPhotosListingPageDataControllerService;
       
        
        //private readonly IGetUnShortListedPhotosListingService _getUnShortListedPhotosListingService;
        //private readonly IGetPagerModelService _getPagerModelService;

        public ShortlistPhotosListingPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetShortlistPhotosListingPageDataControllerService getShortlistPhotosListingPageDataControllerService
            
            //IGetUnShortListedPhotosListingService  getUnShortListedPhotosListingService
            //IGetPagerModelService getPagerModelService
            )
        {
            //_getUnShortListedPhotosListingService = getUnShortListedPhotosListingService;
            //_getPagerModelService = getPagerModelService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getShortlistPhotosListingPageDataControllerService = getShortlistPhotosListingPageDataControllerService;
           
           
        }
        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User_And_Short_Time)]
        public virtual ActionResult ShortlistPhotosListingPage(RenderModel model,string shortListedFilters,string shortListedStatus,string searchKey,string awardId, string awardCategoryId, string page)
        {
           
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            int pageNumber = 1;
            int shortListedFilterId = 0;
            int shortListedStatusId = 0;
            int awardEditionId = 0;
            //Search key include Title,UserEmail,Description,NameAndSurname Filters
            string searchKeyValue = string.Empty;
            int awardEditionCategoryId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["page"]) && Request.QueryString["page"] != null)
            {
                pageNumber = Convert.ToInt32(Request.QueryString["page"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["shortListedFilters"]) && Request.QueryString["shortListedFilters"] != null)
            {
                shortListedFilterId = Convert.ToInt32(Request.QueryString["shortListedFilters"]);

            }
            if (!string.IsNullOrEmpty(Request.QueryString["shortListedStatus"]) && Request.QueryString["shortListedStatus"] != null)
            {
                shortListedStatusId = Convert.ToInt32(Request.QueryString["shortListedStatus"]);

            }
            if (!string.IsNullOrEmpty(Request.QueryString["searchKey"]) && Request.QueryString["searchKey"] != null)
            {
                searchKeyValue = Request.QueryString["searchKey"].ToString();

            }
            if (!string.IsNullOrEmpty(Request.QueryString["awardId"]) && Request.QueryString["awardId"] != null)
            {
                awardEditionId = Convert.ToInt32(Request.QueryString["awardId"]);

            }
            if (!string.IsNullOrEmpty(Request.QueryString["awardCategoryId"]) && Request.QueryString["awardCategoryId"] != null)
            {
                awardEditionCategoryId = Convert.ToInt32(Request.QueryString["awardCategoryId"]);

            }
            ShortlistPhotosListingPageModel shortlistPhotosListingPageModel = _getShortlistPhotosListingPageDataControllerService.Get
                (umbracoHelper, 
                model.Content,
                (Enums.SubmissionFilters)shortListedFilterId,
                (Enums.ShortListedStatusFilter)shortListedStatusId,
                searchKeyValue,
                awardEditionId,
                awardEditionCategoryId, 
                pageNumber);

            #region Dummy

            //new ShortlistPhotosListingPageModel(model.Content);

            //DateTime currentTime = DateTime.Now;



            //int itemTotalAmounts = 40;
            ////how many item to show on the page
            //shortlistPhotosListingPageModel.ItemsPerPage = 6;
            ////total number of listing items.
            //shortlistPhotosListingPageModel.ItemsTotalAmount = itemTotalAmounts;

            //var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper, shortlistPhotosListingPageModel.ItemsPerPage, shortlistPhotosListingPageModel.ItemsTotalAmount);

            //shortlistPhotosListingPageModel.ShortlistPhotosFormModel = new ShortlistPhotosFormModel()
            //{

            //    ListOfPhotos = new List<_ItemPhotoModel>()
            //    {
            //        #region Dummy Data

            //        new _ItemPhotoModel()
            //        {
            //            ItemPhotoName = "John Smith",
            //            ItemPhotoDate = currentTime,
            //            ItemPhotoImgURL = "/dist/assets/img/1.jpg",
            //            ItemPhotoAward = "Landscape photography Award",
            //            ItemPhotoLocation = "Vence, Italy",
            //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //            ItemPhotoCategory = "nature",
            //            PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //            SubmissionId = "62",
            //            ItemPhotoTitle = "Test title Malta1",
            //            ShowShortlistRadioButtons = true,
            //            SubmissionStatusTitleText = "To shortlist: ",
            //            ListOfRadiosValues = new List<string>() {"Yes", "No", "Not yet"},
            //            ChosenRadioValue = "Not yet", //by default will be ticked if defined here
            //        },
            //        new _ItemPhotoModel()
            //        {
            //            ItemPhotoName = "John Smith",
            //            ItemPhotoDate = currentTime,
            //            ItemPhotoImgURL = "/dist/assets/img/2.jpg",
            //            ItemPhotoAward = "Landscape photography Award",
            //            ItemPhotoLocation = "Vence, Italy",
            //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //            ItemPhotoCategory = "nature",
            //            PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test2/",
            //            SubmissionId = "63",
            //            ItemPhotoTitle = "Test title Malta",
            //            ShowShortlistRadioButtons = true,
            //            SubmissionStatusTitleText = "To shortlist: ",
            //            ListOfRadiosValues = new List<string>() {"Yes", "No", "Not yet"},
            //            ChosenRadioValue = "Not yet", //by default will be ticked if defined here
            //        },
            //        new _ItemPhotoModel()
            //        {
            //            ItemPhotoName = "John Smith",
            //            ItemPhotoDate = currentTime,
            //            ItemPhotoImgURL = "/dist/assets/img/3.jpg",
            //            ItemPhotoAward = "No awards",
            //            ItemPhotoLocation = "Vence, Italy",
            //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //            ItemPhotoCategory = "nature",
            //            PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //            SubmissionId = "64",
            //            ItemPhotoTitle = "sunset malta",
            //            ShowShortlistRadioButtons = true,
            //            SubmissionStatusTitleText = "To shortlist: ",
            //            ListOfRadiosValues = new List<string>() {"Yes", "No", "Not yet"},
            //            ChosenRadioValue = "Not yet", //by default will be ticked if defined here
            //        },
            //        new _ItemPhotoModel()
            //        {
            //            ItemPhotoName = "John Smith",
            //            ItemPhotoDate = currentTime,
            //            ItemPhotoImgURL = "/dist/assets/img/4.jpg",
            //            ItemPhotoAward = "No awards",
            //            ItemPhotoLocation = "Vence, Italy",
            //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //            ItemPhotoCategory = "nature",
            //            PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //            SubmissionId = "68",
            //            ItemPhotoTitle = "Test title Malta",
            //            ShowShortlistRadioButtons = true,
            //            SubmissionStatusTitleText = "To shortlist: ",
            //            ListOfRadiosValues = new List<string>() {"Yes", "No", "Not yet"},
            //            ChosenRadioValue = "Not yet", //by default will be ticked if defined here
            //        },
            //        new _ItemPhotoModel()
            //        {
            //            ItemPhotoName = "John Smith",
            //            ItemPhotoDate = currentTime,
            //            ItemPhotoImgURL = "/dist/assets/img/5.jpg",
            //            ItemPhotoAward = "Landscape photography Award",
            //            ItemPhotoLocation = "Vence, Italy",
            //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //            ItemPhotoCategory = "war",
            //            PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //            SubmissionId = "69",
            //            ItemPhotoTitle = "Test Malta",
            //            ShowShortlistRadioButtons = true,
            //            SubmissionStatusTitleText = "To shortlist: ",
            //            ListOfRadiosValues = new List<string>() {"Yes", "No", "Not yet"},
            //            ChosenRadioValue = "Not yet", //by default will be ticked if defined here
            //        },
            //        new _ItemPhotoModel()
            //        {
            //            ItemPhotoName = "John Smith",
            //            ItemPhotoDate = currentTime,
            //            ItemPhotoImgURL = "/dist/assets/img/1.jpg",
            //            ItemPhotoAward = "No awards",
            //            ItemPhotoLocation = "Vence, Italy",
            //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //            ItemPhotoCategory = "nature",
            //            PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //            SubmissionId = "62",
            //            ItemPhotoTitle = "Test ",
            //            ShowShortlistRadioButtons = true,
            //            SubmissionStatusTitleText = "To shortlist: ",
            //            ListOfRadiosValues = new List<string>() {"Yes", "No", "Not yet"},
            //            ChosenRadioValue = "Not yet", //by default will be ticked if defined here

            //        },
            //        new _ItemPhotoModel()
            //        {
            //            ItemPhotoDate = currentTime,
            //            ItemPhotoImgURL = "/dist/assets/img/2.jpg",
            //            ItemPhotoName = "John Smith",
            //            ItemPhotoAward = "No awards",
            //            ItemPhotoLocation = "Vence, Italy",
            //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //            ItemPhotoCategory = "nature",
            //            PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //            SubmissionId = "63",
            //            ItemPhotoTitle = "Test title Malta10",
            //            ShowShortlistRadioButtons = true,
            //            SubmissionStatusTitleText = "To shortlist: ",
            //            ListOfRadiosValues = new List<string>() {"Yes", "No", "Not yet"},
            //            ChosenRadioValue = "Not yet", //by default will be ticked if defined here

            //        },
            //        new _ItemPhotoModel()
            //        {
            //            ItemPhotoDate = currentTime,
            //            ItemPhotoImgURL = "/dist/assets/img/3.jpg",
            //            ItemPhotoAward = "No awards",
            //            ItemPhotoLocation = "Vence, Italy",
            //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //            ItemPhotoCategory = "nature",
            //            PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //            SubmissionId = "64",
            //            ItemPhotoName = "John Smith",
            //            ItemPhotoTitle = "Test title Malta",
            //            ShowShortlistRadioButtons = true,
            //            SubmissionStatusTitleText = "To shortlist: ",
            //            ListOfRadiosValues = new List<string>() {"Yes", "No", "Not yet"},
            //            ChosenRadioValue = "Not yet", //by default will be ticked if defined here

            //        },
            //        #endregion
            //    },
            //    ShortlistButtonText = "Shortlist Photos"
            //};

            //shortlistPhotosListingPageModel.Pagination = pagerModel;



            #endregion

            return base.Populate(shortlistPhotosListingPageModel);
        }

       
    }
}