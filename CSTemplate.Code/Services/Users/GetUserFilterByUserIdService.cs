﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Constants;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Members;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Database;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;
namespace CSTemplate.Code.Services.Users
{

    public interface IGetUserFilterByUserIdService
    {
        _MemberItemModel Get(UmbracoHelper umbracoHelper, int userId);
    }

    [Service]
    public class GetUserFilterByUserIdService : IGetUserFilterByUserIdService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetUserFilterByUserIdService(
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public _MemberItemModel Get(UmbracoHelper umbracoHelper, int userId)
        {
            var returnMemberModel = new _MemberItemModel();
            if (userId <= 0) return returnMemberModel;
            var member = new Member(_umbracoHelperService.TypedMember(umbracoHelper, userId));
            var memberEmailAddress = ApplicationContext.Current.Services.MemberService.GetById(userId);
            returnMemberModel.MemberId = member.Id;
            returnMemberModel.Name = member.ClientName;
            returnMemberModel.Surname = member.ClientSurname;
            returnMemberModel.InsertedPhone = member.Phone;
            returnMemberModel.Nationality = member.Nationality;
            returnMemberModel.County = member.Country;
            returnMemberModel.InsertedEmail = memberEmailAddress.Username;
            return returnMemberModel;
        }
    }
}
