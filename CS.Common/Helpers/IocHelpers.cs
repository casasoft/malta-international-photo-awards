﻿using SimpleInjector;
using SimpleInjector.Integration.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Helpers
{
    public static class IocHelpers
    {
        public readonly static Container Container = null;
        static IocHelpers()
        {


            Container = new Container();
            Container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            Container.Options.AllowOverridingRegistrations = true;
        }

        public static T Get<T>()
            where T : class
        {

            

            return (T)Get(typeof(T));


        }

        public static object Get(Type t)
        {

            var result = Container.GetInstance(t);
            
            return result;
        }
    }
}
