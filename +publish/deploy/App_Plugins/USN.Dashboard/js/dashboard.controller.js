﻿angular.module("umbraco").controller("USN.Dashboard.Controller",function ($scope, $http, notificationsService) {

    $scope.isLoaded = false;

        $http.get('backoffice/api/USNDashboard/GetViewModel').
            success(function (data, status, headers, config) {
                $scope.vm = data;
                $scope.isLoaded = true;
            }).
            error(function (data, status, headers, config) {
                notificationsService.error("Error", "Issue getting dashboard information");
                $scope.isLoaded = true;
            });

       
    });