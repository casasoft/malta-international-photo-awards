﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.DataAnnotations;
using CSTemplate.Code.Models.ChangePasswordForm;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.ChangePassword
{
    public class ChangePasswordPageModel : USNBaseViewModel
    {
        public _ChangePasswordFormModel ChangePasswordForm { get; set; }
        
        public ChangePasswordPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
