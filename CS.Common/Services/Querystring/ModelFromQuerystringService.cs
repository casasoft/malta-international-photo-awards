﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using System.Linq.Expressions;
using CS.Common.Helpers;
using System.Collections.Specialized;
using System.Web;
using System.Collections;
using System.Web.Mvc;

namespace CS.Common.Services.Querystring
{
    public interface IModelFromQuerystringService
    {
        T PopulateModelFromQuerystringValues<T>(string url, T model,
            List<Expression<Func<T, object>>> OmitProperties = null);
    }

    [Service]
    public class ModelFromQuerystringService : IModelFromQuerystringService
    {

        public T PopulateModelFromQuerystringValues<T>(string url, T model,
            List<Expression<Func<T, object>>> OmitProperties = null)
        {
            var allProperties = ReflectionHelpers.GetAllPropertiesAndFieldsForObjct(model, false, false, false, OmitProperties);

            NameValueCollection nvc = UrlHelpers.GetQuerystringFromUrlAsNameValueCollection(url);
            foreach (var property in allProperties)
            {
                var valueForMemberInfo = ReflectionHelpers.GetValueForMemberInfo(model, property);
                string valueStr = nvc[property.Name];
                if (valueForMemberInfo != null)
                {
                    var propertyType = ReflectionHelpers.GetMemberInfoType(property);
                    if (TypeHelpers.IsTypeEnumerableList(propertyType))
                    {
                        //list
                        var listType = ReflectionHelpers.GetUnderlyingType(propertyType);
                        IList newList = (IList)ReflectionHelpers.CreateGenericType(typeof(List<>), new List<Type>()
                        {
                            listType
                        });

                        List<string> listValues = TextHelpers.Split(valueStr, ModelToQuerystringService.Delimiter);
                        foreach (var listValue in listValues)
                        {
                            var convertedValue = ConversionHelpers.ConvertStringToBasicDataType(listValue, listType);
                            newList.Add(convertedValue);
                        }
                        ReflectionHelpers.SetPropertyValue(model, property.Name, newList);
                    }
                    else
                    {
                            var convertedValue = ConversionHelpers.ConvertStringToBasicDataType(valueStr, propertyType);
                        ReflectionHelpers.SetPropertyValue(model, property.Name, convertedValue);
                    }
                }
                
            }
           
            return model;
        }

    }
}
