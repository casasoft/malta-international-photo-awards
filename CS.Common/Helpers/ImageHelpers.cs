﻿using CS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using CS.Common.Services.Page;

namespace CS.Common.Helpers
{
    public static class ImageHelpers
    {


       
       
        public static List<string> GetImageFilesExtensions()
        {
            List<string> list = new List<string>();
            list.Add("jpg");
            list.Add("jpeg");
            list.Add("bmp");
            list.Add("gif");
            list.Add("tif");
            list.Add("tiff");
            list.Add("png");
            return list;
        }
        public static bool CheckIfFilenameIsAnImage(string filename)
        {
            List<string> allowed = GetImageFilesExtensions();
            string ext = FileHelpers.GetExtension(filename);
            ext = ext.ToLower();
            ext = ext.Replace(".", "");
            return (allowed.Contains(ext));

        }


        public static Bitmap GetBitmap(string path)
        {
            FileStream f = new FileStream(path, FileMode.Open, FileAccess.Read);

            Bitmap bmp = new Bitmap(f);
            f.Close();
            return bmp;
        }
        


        /// <summary>
        /// Retreives the size of an image, in 2 output params.  If file does not exist, they are returned as 0.
        /// </summary>
        /// <param name="path">Local path, with Server.MapPath</param>
        /// <param name="width">Width</param>
        /// <param name="height">Height</param>
        public static void GetImageSize(string path, out int width, out int height)
        {
            
            if (path.Contains("/")) path = IocHelpers.Get<IPageService>().MapPath(path);
            if (File.Exists(path))
            {
                FileStream f = new FileStream(path, FileMode.Open, FileAccess.Read);
                try
                {
                    Bitmap bmp = new Bitmap(f);
                    width = bmp.Width;
                    height = bmp.Height;
                    bmp.Dispose();
                    bmp = null;
                }
                catch (ArgumentException)
                {
                    width = 0;
                    height = 0;
                }

                f.Close();


                f.Dispose();
                f = null;

            }
            else
            {
                width = height = 0;
            }

        }

   
        /// <summary>
        /// Resizes image to completely fill the MAXWIDTH x MAXHEIGHT box
        /// </summary>
        /// <param name="src"></param>
        /// <param name="maxWidth"></param>
        /// <param name="maxHeight"></param>
        /// <param name="to"></param>
       
        //public static Bitmap ResizeImage(System.Drawing.Image img, Brush transparentColor, int maxWidth, int maxHeight, bool fillsBox)
        //{

        //    return ImageManipulatorNew.InstanceGeneral.ResizeImage(img, transparentColor, maxWidth, maxHeight, fillsBox);
            

        //}

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns the extension from an image format, e.g 'jpg', 'bmp'.  Does not include a period / dot (.).
        /// </summary>
        /// <param name="imageFormat"></param>
        /// <returns></returns>
        public static string GetExtensionForImageFormat(Enums.ImageType imageFormat)
        {
            return EnumHelpers.ConvertEnumToString(imageFormat).ToLower();
            
        }

        public static Enums.ImageType GetImageTypeFromFilename(string filename)
        {
            
            string ext = FileHelpers.GetExtension(filename).ToLower();
            switch (ext)
            {
                case "bmp": return Enums.ImageType.BMP;
                case "gif": return Enums.ImageType.GIF;
                case "png": return Enums.ImageType.PNG;
                case "tiff":
                case "tif": return Enums.ImageType.TIFF;
                case "pjpg": 
                case "pjpeg": return Enums.ImageType.ProgressiveJpeg;

            }
            return Enums.ImageType.JPEG;

        }
        public static ImageFormat GetImageFormatFromFilename(string to)
        {
            var imageType = GetImageTypeFromFilename(to);
            return GetImageFormatFromImageType(imageType);
        }
        public static System.Drawing.Imaging.ImageFormat GetImageFormatFromImageType(Enums.ImageType imageType)
        {

            
            switch (imageType)
            {
                    case Enums.ImageType.BMP: return ImageFormat.Bmp;
                    case Enums.ImageType.GIF: return ImageFormat.Gif;
                    case Enums.ImageType.JPEG: return ImageFormat.Jpeg;
                    case Enums.ImageType.PNG: return ImageFormat.Png;
                    case Enums.ImageType.ProgressiveJpeg: return ImageFormat.Jpeg;
;                   case Enums.ImageType.TIFF: return ImageFormat.Tiff;
                    
            }
            throw new InvalidOperationException(string.Format("ImageType [{0}] could not be converted to ImageFormat as it is not defined", imageType));
 
        }

        //public static System.Drawing.Bitmap CreateThumbnail(string path, double ratio)
        //{
        //    FileStream fs = new FileStream(path, FileMode.Open);
        //    Bitmap resized = CreateThumbnail(fs, ratio);
        //    fs.Close();
        //    return resized;
        //}
        //public static System.Drawing.Bitmap CreateThumbnail(Stream stream, double ratio)
        //{
        //    return CreateThumbnail(new Bitmap(stream), ratio);
        //}
        //public static System.Drawing.Bitmap CreateThumbnail(System.Drawing.Bitmap loBMP, double ratio)
        //{
        //    return CreateThumbnail(loBMP, Brushes.White, ratio);
        //}
        //public static System.Drawing.Bitmap CreateThumbnail(System.Drawing.Bitmap loBMP, Brush transparentColor, double ratio)
        //{
        //    System.Drawing.Bitmap bmpOut = null;
        //    try
        //    {
        //        ImageFormat loFormat = loBMP.RawFormat;
        //        decimal lnRatio = Convert.ToDecimal(ratio);
        //        int lnNewWidth = Convert.ToInt32(loBMP.Width * ratio);
        //        int lnNewHeight = Convert.ToInt32(loBMP.Height * ratio);


        //        bmpOut = new Bitmap(lnNewWidth, lnNewHeight);
        //        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmpOut);

        //        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        //        Color clrTransparent = (new Pen(transparentColor).Color);
        //        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        //        // g.FillRectangle(transparentColor, 0, 0, lnNewWidth, lnNewHeight);
        //        // bmpOut.MakeTransparent(clrTransparent);
        //        g.DrawImage(loBMP, 0, 0, lnNewWidth, lnNewHeight);


        //        loBMP.Dispose();
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //    return bmpOut;
        //}
       

       




        
    }
}
