 
 $( document ).ready(function() {
     
    $( window ).resize(function() {
               
        //16:9 ratio for full page photo
        var widthOfImageContainer = $('.current-submission-photos').width();
        var heightOfImageContainer = $('.current-submission-photos').height(0.5624*widthOfImageContainer);

    });

    
    // initialisation Masonry
    var $grid = $('.masonry-grid').masonry({
        itemSelector: '.masonry-grid-item',
        percentPosition: true,
        columnWidth: '.grid-sizer',
    });

    // layout Masonry after each image loads
    $('.masonry-grid').imagesLoaded().progress( function() {
        $grid.masonry();
    });
    
	//lazyloading for background images
	document.addEventListener('lazybeforeunveil', function (e) {
            var target = $(e.target);
            var path = target.attr("data-lazyload");
			var inlineBackground = "background-image: url(" + path + ")";
			target.attr("style", inlineBackground );
            target.removeAttr('data-lazyload')
            
     });
     //initialisation of carousels
     
     $('.list-featured-photos-container').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<div class="slick-prev"><i class="ion-chevron-left"></i>',
        nextArrow: '<div class="slick-next"><i class="ion-chevron-right"></i>',
        responsive: [
            {
                breakpoint: 1450,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
           
        ]
    });



    //photo full page modal

    //taking the url of iframe from data attribute
    $('#imageFullPage').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('url') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('#photo-full-page-iframe').attr("src", recipient);
      })

      $('#imageFullPage').on('hidden.bs.modal', function (e) {
        var modal = $(this)
        modal.find('#photo-full-page-iframe').attr("src", "");
      })


      $('#jury-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('content') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('#jury-content').text(recipient);


      })

      $('#imageFullPage').on('hidden.bs.modal', function (e) {
        var modal = $(this)
        modal.find('#photo-full-page-iframe').attr("src", "");
      })


    

    //photo full page carousel
    $('.submission-images-container').slick({
        dots: true,
        // autoplay: true,
        // autoplaySpeed: 2000,
        prevArrow: '<div class="slick-prev"><i class="ion-chevron-left"></i>',
        nextArrow: '<div class="slick-next"><i class="ion-chevron-right"></i>',

    });
 
    //iframe(modal) dynamic height height
    document.getElementById('photo-full-page-iframe').onload = function() {
        this.height = this.contentWindow.document.body.scrollHeight + "px";
    }

  
});


