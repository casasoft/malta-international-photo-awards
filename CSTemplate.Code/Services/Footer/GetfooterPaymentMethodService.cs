﻿using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Footer
{

    //public interface IGetfooterPaymentMethodService
    //{
    //    _FooterPaymentMethodModel Get(UmbracoHelper umbracoHelper);
    //}

    //[Service]
    //public class GetfooterPaymentMethodService : IGetfooterPaymentMethodService
    //{
    //    private readonly IUmbracoHelperService _umbracoHelperService;
    //    private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

    //    public GetfooterPaymentMethodService(
    //        IUmbracoHelperService umbracoHelperService,
    //        IUmbracoControllerWrapperService umbracoControllerWrapperService
    //    )
    //    {
    //        _umbracoHelperService = umbracoHelperService;
    //        _umbracoControllerWrapperService = umbracoControllerWrapperService;
    //    }

    //    public _FooterPaymentMethodModel Get(UmbracoHelper umbracoHelper)
    //    {
    //        var footerMainText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
    //            Enums.DictionaryKey.Footer_Left_Main_Text);
    //        var footerPaymentMethodText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
    //            Enums.DictionaryKey.Footer_Payment_Method_Description_Text);

    //        var footerPaymentMethod = new _FooterPaymentMethodModel()
    //        {
    //            FooterPhotoTitleImgURL = "/dist/assets/img/logostandard.png",
    //            FooterText = footerMainText,
    //            FooterPaymentMethodText = footerPaymentMethodText,
    //            FooterPaymentMethodImgURL1 = "/dist/assets/img/paypal.png",
    //            FooterPaymentMethodImgURL2 = "/dist/assets/img/visa.png",
    //            FooterPaymentMethodImgURL3 = "/dist/assets/img/mastercard.png",

    //        };

    //        return footerPaymentMethod;
    //    }

    //}
}