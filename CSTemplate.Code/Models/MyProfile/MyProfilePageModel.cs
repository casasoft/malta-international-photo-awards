﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.MyProfile
{
    public class MyProfilePageModel : USNBaseViewModel
    {
        public string SuccessMessage { get; set; }
        public _RegisterFormModel UpdateProfileFormModel { get; set; }

        public MyProfilePageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
