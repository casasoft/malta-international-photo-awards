﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Constants
{
    public static class DatabaseConstantValues
    {

        //Order By Conditions
        public const string ORDER_BY_DESC = "DESC";
        public const string ORDER_BY_ASC = "ASC";

        //Pocos
        public const string PhotosTableName = "pocoPhotos";
        public const string OrdersTableName = "pocoOrders";
        public const string OrderItemsTableName = "pocoOrderItems";
        public const string SubmissionsTableName = "pocoSubmissions";



        /******Photos******/
        public static readonly string aliasForPhotosPocoTableNameAlias = "pocoPhotos";
        public static readonly string Photos_Poco_Primary_Key_Value = "Id";
        public static readonly string Photos_Featured_Field_Name = "Featured";
        public static readonly string Photos_PhotoUrl_Field_Name = "PhotoUrl";
        public static readonly string Photos_Submission_Id_Field_Name = "SubmissionId";


        /******Orders******/
        public static readonly string aliasForOrdersPocoTableNameAlias = "pocoOrders";
        public static readonly string Orders_Poco_Primary_Key_Value = "Id";
        public static readonly string Orders_Date_Field_Name = "Date";
        public static readonly string Orders_Payment_Status_Enum_Id_Field_Name = "PaymentStatusEnumId";
        public static readonly string Orders_Description_Field_Name = "Description";
        public static readonly string Orders_PSPID_Field_Name = "PSPID";
        public static readonly string Orders_OrderRefId_Field_Name = "OrderRefId";
        public static readonly string Orders_UserId_Field_Name = "UserId";
        public static readonly string Orders_Payment_Transaction_Response = "PaymentTransactionResponse";



        /******OrderItems******/
        public static readonly string aliasForOrderItemsPocoTableNameAlias = "pocoOrderItems";
        public static readonly string OrderItems_Poco_Primary_Key_Value = "Id";
        public static readonly string OrderItems_Submission_Id_Field_Name = "SubmissionId";
        public static readonly string OrderItems_Order_Id_Field_Name = "OrderId";
        public static readonly string OrderItems_Pay_For_Submission_Field_Name = "PayForSubmission";
        public static readonly string OrderItems_Pay_For_Feedback_Field_Name = "PayForFeedback";
        public static readonly string OrderItems_Submission_Cost_Field_Name = "SubmissionCost";
        public static readonly string OrderItems_Feedback_Cost_Field_Name = "FeedbackCost";

        /******Submissions******/
        public static readonly string aliasForSubmissionsPocoTableNameAlias = "pocoSubmissions";
        public static readonly string Submissions_Poco_Primary_Key_Value = "Id";
        public static readonly string Submissions_User_Id_Field_Name = "UserId";
        public static readonly string Submissions_Status_Enum_Id_Field_Name = "StatusEnumID";
        public static readonly string Submissions_Short_Listed_Enum_Id_Field_Name = "ShortListedEnumId";
        public static readonly string Submissions_Category_Id_Field_Name = "CategoryId";
        public static readonly string Submissions_Short_Listed_Date_Field_Name = "ShortListedDate";
        public static readonly string Submissions_Description_Field_Name = "Description";
        public static readonly string Submissions_Date_Field_Name = "Date";
        public static readonly string Submissions_Location_Field_Name = "Location";
        public static readonly string Submissions_Feedback_Request_Field_Name = "FeedbackRequest";
        public static readonly string Submissions_Feedback_Description_Field_Name = "FeedbackDescription";
        public static readonly string Submissions_Is_A_Story_Telling_Field_Name = "IsAStoryTelling";
        public static readonly string Submissions_Title_Field_Name = "Title";
        public static readonly string Submissions_Gallery_Category_Id_Name = "GalleryCategoryId";
        public static readonly string Submission_Award_Edition_Id = "AwardEditionId";
        public static readonly string Submission_Feedback_Replied_Mail_Sent = "FeedbackRepliedMailSent";




    }
}
