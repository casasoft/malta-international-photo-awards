﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;

namespace CSTemplate.Code.Services.Submissions
{

    public interface IGetSubmissionPocoDetailsFilterByIdService
    {
        SubmissionPoco Get(int submissionId);
    }

    [Service]
    public class GetSubmissionPocoDetailsFilterByIdService : IGetSubmissionPocoDetailsFilterByIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetSubmissionPocoDetailsFilterByIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public SubmissionPoco Get(int submissionId)
        {
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var submissionTable = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;

            var submission =umbracoDb.Query<SubmissionPoco>($"SELECT * FROM {submissionTable} WHERE {submissionPrimaryKeyField}={submissionId}").FirstOrDefault();

            return submission;

        }
    }
}
