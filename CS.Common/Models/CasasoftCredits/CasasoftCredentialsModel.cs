﻿namespace CS.Common.Models.CasasoftCredits
{
    public class CasasoftCredentialsModel
    {
        public string Key { get; set; }
        public string Url { get; set; }
    }
}
