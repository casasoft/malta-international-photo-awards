﻿using CS.Common.Constants;
using System.Drawing;

namespace CS.Common.Helpers
{
    public static class ColorsHelpers
    {
        private static char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        /// <summary>
        /// Returns the color as a hex string, WITHOUT the # in front
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static string ColorToHex(System.Drawing.Color color)
        {
            string str = ColorToInt(color).ToString("X");
            return str;

        }
        /// <summary>
        /// Gets the color form a text description, like 'red'.
        /// </summary>
        /// <param name="color">Color Text</param>
        /// <returns>Color</returns>
        public static System.Drawing.Color? GetColorFromText(string color)
        {
            color = color ?? "";
            color = color.ToLower().Trim();
            switch (color)
            {
                case "grey": color = "gray"; break;

            }
            System.Drawing.Color? clr = null;
            try
            {
                clr = System.Drawing.ColorTranslator.FromHtml(color);
            }
            catch
            {

            }
            return clr;


        }
        
        public static int ColorToInt(Color color)
        {
            int value = (color.B) | (color.G << 8) | (color.R << 16);
            return value;

        }
        public static string IntToColorHex(uint color, bool includeHashAtBeginning)
        {

            string s = color.ToString("X");
            s = TextHelpers.PadText(s, Enums.TextAlign.Right, 6, "0");
            if (includeHashAtBeginning)
            {
                s = "#" + s;
            }
            return s;

        }
        public static Color IncreaseBrightness(Color color, int amount)
        {
            if (amount > 255)
                amount = 255;
            int r, g, b;
            r = (int)color.R;
            g = (int)color.G;
            b = (int)color.B;

            r += amount;
            g += amount;
            b += amount;

            if (r > 255) r = 255;
            if (g > 255) g = 255;
            if (b > 255) b = 255;
            if (r < 0) r = 0;
            if (b < 0) b = 0;
            if (g < 0) g = 0;
            return Color.FromArgb(r, g, b);



        }
    }
}
