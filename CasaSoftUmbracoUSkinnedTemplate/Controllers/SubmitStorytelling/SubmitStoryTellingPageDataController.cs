﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.SubmitStorytelling;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Awards;
using CSTemplate.Code.Services.Orders;
using CSTemplate.Code.Services.SubmitStorytelling;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.SubmitStorytelling
{
    public partial class SubmitStoryTellingPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetSubmitStoryTellingPageService _getSubmitStoryTellingPageService;
        private readonly IControlActiveAnyAwardService _controlActiveAnyAwardService;
        private readonly IControlActiveStoryTellingCategoryService _controlActiveStoryTellingCategoryService;
        private readonly IUmbracoHelperService _umbracoHelperService;

        public SubmitStoryTellingPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetSubmitStoryTellingPageService getSubmitStoryTellingPageService,
            IControlActiveStoryTellingCategoryService controlActiveStoryTellingCategoryService,
            IControlActiveAnyAwardService controlActiveAnyAwardService,
            IUmbracoHelperService umbracoHelperService
            )
        {
            _umbracoHelperService = umbracoHelperService;
            _controlActiveStoryTellingCategoryService = controlActiveStoryTellingCategoryService;
            _controlActiveAnyAwardService = controlActiveAnyAwardService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getSubmitStoryTellingPageService = getSubmitStoryTellingPageService;

        }

        // GET: SubmitStoryTellingPageData
        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult SubmitSToryTellingPage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var currentMemberId = Members.GetCurrentMemberId();

            bool doesExist = _controlActiveAnyAwardService.Control(umbracoHelper);
            bool activeStoryTellingCategory = _controlActiveStoryTellingCategoryService.Control(umbracoHelper);

            if (!doesExist)
            {
                var pageNotFound = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, CSTemplate.Code.Constants.ConstantValues.Competition_Not_Active_Data_Document_Type_Alias);
                return new RedirectToUmbracoPageResult(pageNotFound);
            }

            if (!activeStoryTellingCategory)
            {
                return Redirect(ConstantValues.Submit_Photo_Page_Link);
            }

           
            SubmitStoryTellingPageModel submitStoryTellingPageModel =
                _getSubmitStoryTellingPageService.Get(umbracoHelper, model.Content);
            return base.Populate(submitStoryTellingPageModel);
        }
    }
}