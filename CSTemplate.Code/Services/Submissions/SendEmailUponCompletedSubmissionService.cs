﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.RazorEngine;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Submissions;
using CSTemplate.Code.Models.ViewOrder;
using CSTemplate.Code.Services.Emails;
using CSTemplate.Code.Services.Orders;
using CSTemplate.Code.Services.Payment.Bilderlings;
using CSTemplate.Code.Services.ViewOrder;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.Submissions
{

    public interface ISendEmailUponCompletedSubmissionService
    {
        void Send(UmbracoHelper umbracoHelper, int orderId, int currentUserId,
            bool paymentSuccessfull = true);
    }

    [Service]
    public class SendEmailUponCompletedSubmissionService : ISendEmailUponCompletedSubmissionService
    {
        private readonly IGetViewOrderPageService _getViewOrderPageService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly ISendEmailService _sendEmailService;
        private readonly IGetViewOrderPageUrlService _getViewOrderPageUrlService;
        private readonly IGetEmailTemplatesPlaceholderForCurrentDomainRootNodeService _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService;
        private readonly IRazorEngineCompileTemplate _razorEngineCompileTemplate;
        private readonly IGetViewOrderModelService _getViewOrderModelService;

        public SendEmailUponCompletedSubmissionService(IGetViewOrderPageService
            getViewOrderPageService,
            IUmbracoHelperService
            umbracoHelperService,
            ISendEmailService
            sendEmailService,
            IGetViewOrderPageUrlService
            getViewOrderPageUrlService,
            IGetEmailTemplatesPlaceholderForCurrentDomainRootNodeService
            getEmailTemplatesPlaceholderForCurrentDomainRootNodeService,
            IRazorEngineCompileTemplate
            razorEngineCompileTemplate,
            IGetViewOrderModelService
            getViewOrderModelService)
        {
            _getViewOrderModelService = getViewOrderModelService;
            _razorEngineCompileTemplate = razorEngineCompileTemplate;
            _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService = getEmailTemplatesPlaceholderForCurrentDomainRootNodeService;
            _getViewOrderPageUrlService = getViewOrderPageUrlService;
            _sendEmailService = sendEmailService;
            _umbracoHelperService = umbracoHelperService;
            _getViewOrderPageService = getViewOrderPageService;
        }

        public void Send(UmbracoHelper umbracoHelper, int orderId, int currentUserId, bool paymentSuccessfull = true)
        {
            if (umbracoHelper == null)
            {
                umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
            }

            ViewOrderModel viewOrderModel = _getViewOrderModelService.Get(umbracoHelper, orderId, currentUserId);

            var emailTemplates =
               _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService.Get(umbracoHelper);
            var fromMailAddress = new MailAddress(CS.Common.Helpers.SmtpHelpers.SmtpFrom, emailTemplates.EmailSenderName);

            LogHelper.Info<SendEmailUponCompletedSubmissionService>("Sending Emails To Both Client and Administrator");


            SendEmailToClient(umbracoHelper, viewOrderModel, emailTemplates, fromMailAddress, paymentSuccessfull);
            SendEmailToAdministartor(umbracoHelper, viewOrderModel, emailTemplates, fromMailAddress, paymentSuccessfull);
        }

        private void SendEmailToClient(UmbracoHelper umbracoHelper, ViewOrderModel viewOrderModel, EmailTemplates emailTemplates, MailAddress fromMailAddress,
            bool paymentSuccessfull)
        {
            var viewOrderPageUrl = _getViewOrderPageUrlService.Get(umbracoHelper, viewOrderModel.OrderNumber);

            var emailModel = new SendEmailUponCompletedSubmissionEmailModel()
            {
               ClientName = viewOrderModel.BillingName,
               ClientEmailAddress = viewOrderModel.BillingEmail,
               OrderDate = viewOrderModel.OrderDate.ToString(ConstantValues.GeneralDateTimeFormat),
               OrderNumber = viewOrderModel.OrderNumber,
               OrderStatus = viewOrderModel.OrderStatus,
               OrderTotal = string.Format("€{0:N2}", viewOrderModel.OrderTotal),
               ViewOrderFullPageUrl = viewOrderPageUrl.ToAbsoluteUrl(),
               OrderSummaryTable = PrepareOrderSummaryHtmlTableForEmail(viewOrderModel)
            };

            EmailTemplate emailTemplateForClient;

            if (!paymentSuccessfull)
            {
                emailTemplateForClient =
                    new EmailTemplate(emailTemplates.Children.FirstOrDefault(x => x.Id.Equals(ConstantValues
                        .EmailNotificationToClientOnSubmissionSubmittedButPaymentNotCompleted)));
            }
            else
            {
                emailTemplateForClient = 
                    new EmailTemplate(emailTemplates.Children.FirstOrDefault(x => x.Id.Equals(ConstantValues.EmailNotificationToClientOnSubmissionAndPaymentCompleted)));
            }

            var keyForNewMemberEmailBody = $"{emailTemplateForClient.Name}-{"body"}-{emailTemplateForClient.UpdateDate}";

            var keyForNewMemberEmailSubject = $"{emailTemplateForClient.Name}-{"subject"}-{emailTemplateForClient.UpdateDate}";

            var newMemberEmailSubject = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplateForClient.Subject, keyForNewMemberEmailSubject, emailModel);

            var newMemberEmailBody = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplateForClient.EmailBody.ToString(), keyForNewMemberEmailBody, emailModel);


            _sendEmailService.SendEmail(umbracoHelper, newMemberEmailSubject, newMemberEmailBody,
                viewOrderModel.BillingEmail, null, null, fromMailAddress);

        }

        private void SendEmailToAdministartor(UmbracoHelper umbracoHelper,
            ViewOrderModel viewOrderModel, EmailTemplates emailTemplates, MailAddress fromMailAddress,
            bool paymentSuccessfull)
        {
            var emailModel = new SendEmailUponCompletedSubmissionEmailModel()
            {
                ClientName = viewOrderModel.BillingName,
                ClientEmailAddress = viewOrderModel.BillingEmail,
                OrderDate = viewOrderModel.OrderDate.ToString(ConstantValues.GeneralDateTimeFormat),
                OrderNumber = viewOrderModel.OrderNumber,
                OrderStatus = viewOrderModel.OrderStatus,
                OrderTotal = string.Format("€{0:N2}", viewOrderModel.OrderTotal),
                OrderSummaryTable = PrepareOrderSummaryHtmlTableForEmail(viewOrderModel)
            };

            EmailTemplate emailTemplateForAdministrator; 
            if (!paymentSuccessfull)
            {
                emailTemplateForAdministrator =
                    new EmailTemplate(emailTemplates.Children.FirstOrDefault(x => x.Id.Equals(ConstantValues
                        .EmailNotificationToAdministratorOnSubmissionNotYetCompleted)));
            }
            else
            {
                emailTemplateForAdministrator =
                     new EmailTemplate(emailTemplates.Children.FirstOrDefault(x => x.Id.Equals(ConstantValues.EmailNotificationToAdministratorOnSubmissionCompleted)));
            }

            var keyForNewMemberEmailBody = $"{emailTemplateForAdministrator.Name}-{"body"}-{emailTemplateForAdministrator.UpdateDate}";

            var keyForNewMemberEmailSubject = $"{emailTemplateForAdministrator.Name}-{"subject"}-{emailTemplateForAdministrator.UpdateDate}";

            var newMemberEmailSubject = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplateForAdministrator.Subject, keyForNewMemberEmailSubject, emailModel);

            var newMemberEmailBody = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplateForAdministrator.EmailBody.ToString(), keyForNewMemberEmailBody, emailModel);

            var administrationEmailReceiver = emailTemplates.AdministrationEmailReceiver;

            _sendEmailService.SendEmail(umbracoHelper, newMemberEmailSubject, newMemberEmailBody,
               administrationEmailReceiver, null, null, fromMailAddress);
        }

        private string PrepareOrderSummaryHtmlTableForEmail(ViewOrderModel viewOrderModel)
        {
            string htmlTable = "<table style=\"width:70%\">";

            htmlTable += "<tr style=\"background-color:gray;color:white;\"><th style=\"text-align:left;padding:10pt;\">Photo</th>";
            htmlTable += "<th style=\"text-align:center;padding:10pt;\">Price</th></tr>";

            var alternatingFirstGrayColor = "#C8C8C8";
            var alternatingSecondGrayColor = "#D0D0D0";
            var cnt = 1;
            foreach (var photoItem in viewOrderModel.ListOfOrderPhotos)
            {
                var backgroundColorToConsider = alternatingFirstGrayColor;
                if (cnt%2 == 0)
                {
                    backgroundColorToConsider = alternatingSecondGrayColor;
                }

                htmlTable += "<tr style=\"background-color: " + backgroundColorToConsider + ";\"> <td style=\"padding:10pt;\">";

                var photosType = "";
                    switch (photoItem.OrderPhotosType)
                    {
                        case CS.Common.Constants.Enums.OrderPhotosSubmitType.FeedbackOnly:
                                photosType = "Feedback Request";
                            break;
                        case CS.Common.Constants.Enums.OrderPhotosSubmitType.SubmissionWithFeedback:
                                photosType = "Submission and Feedback Request";
                            break;
                        default:
                              photosType = "Submission";
                            break;
                    }



                htmlTable += "<span style=\"padding-top:10pt;\">";
                        htmlTable += "<div style=\"padding-bottom:10pt;font-weight:bold;\">" + photosType + "</div>";
                        htmlTable += "<div style=\"padding-bottom:10pt;\">Date: " + photoItem.OrderPhotoDate.ToString(ConstantValues.GeneralDateFormat) + "</div>";
                        htmlTable += "<div style=\"padding-bottom:10pt;\">Category: " + photoItem.OrderCategory + "</div>";
                        htmlTable += "<div style=\"padding-bottom:10pt;\">Description: " + photoItem.OrderPhotogDescription + "</div>";
                    htmlTable += "</span>";

                htmlTable += "<span style=\"width:30%;padding-right:10pt;\">";
                        foreach (var img in photoItem.ListOfPhotosUrls)
                        {
                            htmlTable += "<img style=\"width:100px;padding-right:10pt;\" src=\"" + img.ToAbsoluteUrl() + "\"/>";
                        }
                    htmlTable += "</span>";

                   

                htmlTable += "</td>";
                htmlTable += "<td style=\"vertical-align:top;text-align:center;padding-top:10pt;\"><span>" + string.Format("€{0:N2}", photoItem.OrderPhotoPrice) + "</span></td>";
                htmlTable += "</tr>";

                cnt++;
            }

            //for total
            htmlTable += "<tr style=\"background-color:gray;color:white;\"> <td>";

  

            htmlTable += "<div style=\"text-align:right;\">";
            htmlTable += "<div style=\"padding:10pt;\">Subtotal</div>";
            htmlTable += "</div>";

            htmlTable += "</td>";
            htmlTable += "<td style=\"text-align:center;padding:10pt;font-weight:bold;\"><span>" + string.Format("€{0:N2}", viewOrderModel.OrderTotal) + "</span></td>";
            htmlTable += "</tr>";

            htmlTable += "</table>";
            return htmlTable;
        }
    }
}
