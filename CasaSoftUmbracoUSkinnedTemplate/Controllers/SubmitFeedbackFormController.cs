﻿using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared.SubmitFeedbackForm;
using CSTemplate.Code.Services.Feedbacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using Umbraco.Web.Mvc;
using System.Web.Routing;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class SubmitFeedbackFormController : SurfaceController
    {
        private readonly ISubmitAFeedbackToSubmissionAsAJuryService _submitAFeedbackToSubmissionAsAJuryService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        public SubmitFeedbackFormController(ISubmitAFeedbackToSubmissionAsAJuryService submitAFeedbackToSubmissionAsAJuryService,IUmbracoHelperService umbracoHelperService, IUmbracoControllerWrapperService umbracoControllerWrapperService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _umbracoHelperService = umbracoHelperService;
            _submitAFeedbackToSubmissionAsAJuryService = submitAFeedbackToSubmissionAsAJuryService;
        }

        [HttpPost]
        public virtual ActionResult HandleSubmitFeedbackFormSubmit(_SubmitFeedbackFormModel submitFeedbackFormModel)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            _submitAFeedbackToSubmissionAsAJuryService.Submit(int.Parse(submitFeedbackFormModel.SubmissionID), submitFeedbackFormModel.SubmittedFeedbackFromJury);

            var uriBuilder = new UriBuilder(Request.Url.AbsoluteUri);
            var queryString = HttpUtility.ParseQueryString(uriBuilder.Query);

  
           queryString[ConstantValues.Jury_Profile_Submit_Feedback_Query_String] = 
                    _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey
                                                     .Dictionary_Jury_Profile_Successfully_Submitted_Feedback);
            

            uriBuilder.Query = queryString.ToString();
            var urlToPhotoSubmissionFullPageUrl = uriBuilder.ToString();


            HttpRuntime.Cache.Remove(ConstantValues.Cache_Key_Submission_Full_Page);

            return Redirect(urlToPhotoSubmissionFullPageUrl);

           


        }
    }
}