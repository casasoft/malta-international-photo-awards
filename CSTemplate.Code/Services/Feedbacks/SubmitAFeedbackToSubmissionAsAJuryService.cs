﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Submissions;

namespace CSTemplate.Code.Services.Feedbacks
{

    public interface ISubmitAFeedbackToSubmissionAsAJuryService
    {
        bool Submit(int submissionId,string feedbackDescription);
    }

    [Service]
    public class SubmitAFeedbackToSubmissionAsAJuryService : ISubmitAFeedbackToSubmissionAsAJuryService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetSubmissionPocoDetailsFilterByIdService _getSubmissionPocoDetailsFilterByIdService;

        public SubmitAFeedbackToSubmissionAsAJuryService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IGetSubmissionPocoDetailsFilterByIdService getSubmissionPocoDetailsFilterByIdService
            )
        {
            _getSubmissionPocoDetailsFilterByIdService = getSubmissionPocoDetailsFilterByIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public bool Submit(int submissionId, string feedbackDescription)
        {
            var result = false;
            if (submissionId <= 0) return result;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var submissionTable = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;

            var submissionInfo = _getSubmissionPocoDetailsFilterByIdService.Get(submissionId);
            submissionInfo.FeedbackDescription = feedbackDescription;
            var sqlResult = umbracoDb.Update(submissionTable, submissionPrimaryKeyField, submissionInfo);
            return sqlResult > 0 ? true : false;
        }
    }
}
