﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Constants
{
    public class Enums
    {
        public enum DictionaryKey
        {
            [Description("Example")]
            Contact_Page_Contact_Form_Title_Text,
            //Global
            [Description("required")]
            Dictionary_Form_Required_Text,
            [Description("I agree to receive promotional material from MIPA")]
            Dictionary_Form_Promotional_Material_Text,
            [Description("I agree to the <a href='{0}' target='_blank' class='textp' title='terms and conditions url' >terms and conditions</a>")]
            Dictionary_Form_Terms_And_Conditions_Text,
            [Description("I agree to the <a href='{0}' target='_blank' class='text' title='award rules url' >Award Rules</a> and the  <a href='{1}' target='_blank' class='textp' title='terms and conditions url' >Terms & Conditions</a>")]
            Dictionary_Form_Award_Rules_And_Terms_And_Conditions_Text,



            //MyProfile
            [Description("update profile")]
            Profile_Page_Title_Form_Text,
            [Description("your details")]
            Profile_Page_SubTitle_Form_Text,
            [Description("register")]
            Profile_Page_Button_Form_Text,

            //SubmiPhotoSelection

            [Description("Take part in our higly recognised awards and send us your submissions. Please choose from the following two options.")]
            Submit_Photo_Selection_Explanation_Text,
            [Description("submit photo")]
            Submit_Photo_Selection_Submit_Photo_Title,
            [Description("Submit a selection of photos in different categories.")]
            Submit_Photo_Selection_Submit_Photo_Text,
            [Description("submit storytelling")]
            Submit_Photo_Selection_Submit_StoryTelling_Title,
            [Description("Submit 5 photos within the Storytelling category")]
            Submit_Photo_Selection_Submit_StoryTelling_Text,
            [Description("/submit-photos/")]
            Submit_Photo_Selection_Url,
            [Description("/submit-storytelling/")]
            Submit_StoryTelling_Selection_Url,


            //SubmitPhoto
            [Description("Take part in the Malta International Photo Award and send us your submissions by simply filling in the details below.")]
            Submit_Photo_Explanation_Text,
            [Description("You can only submit 10 photographs in a single order,. If you want to send us more pictures, simply send them to us in separate orders.")]
            Submit_Photo_SubExplanation_Text,
            [Description("Photo")]
            Submit_Photo_Title_Form_Text,
            [Description("Next")]
            Submit_Photo_Button_Form_Text,
            [Description("Your are submitting your photos for the Winter 2018")]
            Submit_Photo_Explanation_From_Text,
            [Description("We suggest you use Google Chrome to upload photos as through that you will see a % indicator of your progress.")]
            Browser_Suggestion_Text,
            [Description("Clicking on NEXT will start uploading your photos for the submission. Depending on the file size of your photos, this might take between 5 - 15 minutes. Please do not close this window, refresh the page or try to click on the button multiple times as this will stop the upload.")]
            Upload_Information_Text,

            //Loader
            [Description("Uploading Photos. Please Wait")]
            Loader_Title_Text,
            [Description("Depending on the file size of your photos, this might take between 5 - 15 minutes. Please do not close this window or refresh the page as this will stop the upload.")]
            Loader_Explanation_Text,

            [Description("Please Wait")]
            RegisterPage_Loader_Title_Text,
            [Description("Depending on your connection speed this might some minutes. Please do not close this window or refresh the page as this will stop the register.")]
            RegisterPage_Loader_Explanation_Text,



            //SubmitStoryTelling
            [Description("Take part in our higly recognised awards and send us your submissions by simply filling in the details below:")]
            Submit_StoryTelling_Explanation_Text,
            [Description("We invite the photographer to tell us a story through 5 images.")]
            Submit_StoryTelling_Explanation_StoryTellingText,
            [Description("To submit 5 photos, simply click on 'Choose Files' below and select 5 photos, by clicking on 5 photos on your computer from the same folder while holding down the 'Ctrl' key.")]
            Submit_StoryTelling_Explanation_FileInput,
            


            //ChangePassword
            [Description("Please enter your new desired password.")]
            ChangePassword_Page_Subtitle_Form_Text,
            [Description("register")]
            ChangePassword_Page_Button_Form_Text,
            [Description("Password changed successfully")]
            ChangePassword_Page_Message_Successfully,
            [Description("Password change failed")]
            ChangePassword_Page_Message_Failed,

            //ContactUs
            [Description("Subject")]
            Contact_Us_Page_Form_Subject_Label_Text,

            //PrizesSection
            [Description("Prizes")]
            HomePage_Prizes_Label_Text,
            [Description("These are the great prices you can win by participating")]
            HomePage_Prizes_Explanation_Text,
            [Description("See More Prizes")]
            HomePage_Prizes_See_More_Prizes_Button_Text,

            //JurySection
            [Description("Juries")]
            HomePage_Juries_Listing_Title_Text,
            [Description("These are the judges adjudicating the awards")]
            HomePage_Juries_Listing_Explanation_Text,

            //Order Summary
            [Description("Photo")]
            Order_Summary_Photo_Title_Table,
            [Description("Price")]
            Order_Summary_Pirce_Title_Table,
            [Description("Subtotal")]
            Order_Summary_Subtotal_Title_Table,
            [Description("Pay with Paypal")]
            Order_Summary_Paypal_Button_Text,
            [Description("Pay with Card")]
            Order_Summary_Card_Button_Text,
            [Description("Feedback")]
            Order_Sumary_Feedback_Price_Text,
            [Description("Submission")]
            Order_Sumary_Submission_Price_Text,


            //Order History
            [Description("date")]
            Order_History_Table_Date_Text,
            [Description("order number")]
            Order_History_Table_OrderNumber_Text,
            [Description("status")]
            Order_History_Table_Status_Text,
            [Description("description")]
            Order_History_Table_Description_Text,
            [Description("total")]
            Order_History_Table_Total_Text,
            [Description("Delete pending order")]
            Order_History_Delete_pending_order,
            [Description("/order-history/")]
            Order_History_Item_Link_Url,



            //View Order
            [Description("PRINT")]
            View_Order_Print_Button_Text,
            [Description("order details")]
            View_Order_Order_Details_Text,
            [Description("billing details")]
            View_Order_Billing_Details_Text,
            [Description("date:")]
            View_Order_Order_Date_Text,
            [Description("order number:")]
            View_Order_Order_Number_Text,
            [Description("status:")]
            View_Order_Order_Status_Text,
            [Description("total:")]
            View_Order_Order_Total_Text,
            [Description("name:")]
            View_Order_Billing_Name_Text,
            [Description("email:")]
            View_Order_Billing_Email_Text,
            [Description("contact us:")]
            View_Order_Billing_ContactUs_Text,
            [Description("address:")]
            View_Order_Billing_Address_Text,

            //Download Shortlist
            [Description("Click the button below to download the shortlisted photographs in Excel format.")]
            Download_Shortlist_Explanation_Text,
            [Description("Download Shortlist")]
            Download_Shortlist_Button_Text,

            //Download FeedbackRequested Submissions
            [Description("Download Feedback Requested Submissions")]
            Download_Feedback_Requested_Submissions_Button_Text,

            //HomePage Time Left To Submit Your Application Section
            [Description("Time Left To Submit Your Application")]
            HomePage_Time_Left_To_Submit_Your_Application_Text,
            [Description("Days")]
            HomePage_Time_Left_Days_Text,
            [Description("Hours")]
            HomePage_Time_Left_Hours_Text,
            [Description("Min")]
            HomePage_Time_Left_Minute_Text,

            //Featured Photos Section For Home Page
            [Description("Featured Photos")]
            HomePage_Featured_Photos_Title,
            [Description("VIEW ALL SUBMITED PHOTOS")]
            HomePage_Featured_Photos_Explanation,

            //Forgot Password
            [Description("If you have forgot your password, enter your email in the field below and press the 'Submit' button. If the email address provided is valid, you will receive instructions in your maibox on how to reset your password.")]
            Forgot_Password_Description_Text,
            [Description("Submit")]
            Forgot_Password_Button_Text,
            [Description("Email Address is required")]
            Forgot_Password_Email_Address_Required,
            [Description("Email Address does not belong to any existing user account!")]
            Forgot_Password_Email_Does_Not_Belong_To_Existing_Account,
            [Description("Great! You should have received an email which contains your username and a link to reset your password.")]
            Forgot_Password_Success,

            //Reset Password
            [Description("To reset your password, enter your email address and your new password!")]
            Dictionary_Reset_Password_Page_Message,
            [Description("Reset Password")]
            Dictionary_Reset_Password_Page_Button_Text,
            [Description("Required")]
            Dictionary_Reset_Password_Required_Text,
            [Description("Request is not valid! Please make sure that you have clicked on the link recevied as email!")]
            Dictionary_Reset_Password_Request_Not_Valid,
            [Description("Thank you.Now you can log in with your new password")]
            Reset_Password_Successful,


            //General Error
            [Description("An unexpected error has occurred. Please contact us to get this solved as soon as possible.")]
            Unexpected_Error_Text,

            //Update Profile
            [Description("You have successfully updated your profile details!")]
            Dictionary_Member_Profile_Successfully_Update_Text,

            //Update Profile
            [Description("You have successfully submitted a feedback!")]
            Dictionary_Jury_Profile_Successfully_Submitted_Feedback,


            //SubmissionStatusTitleText
            [Description("Status:")]
            Submission_Status_Title_Text,

            //SubmissionFullPage
            [Description("Submit Photo Feedback")]
            Submit_Feedback_Title_Text,
            [Description("This user would like to receive feedback on this photo. Please submit your feedback below.")]
            Submit_Feedback_Subtitle_Text,
            [Description("Submit Feedback")]
            Submit_Feedback_Button_Text,
            [Description("Request Photo Feedback")]
            Request_Feedback_Title_Text,
            [Description("Click the button below to request feedback by the Jury.")]
            Request_Photo_Feedback_Description,
            [Description("Request Feedback")]
            Request_Feedback_Button_Text,
            [Description("Photo Feedback")]
            Received_Feedback_Title_Text,
            [Description("Other submissions by {0}")]
            Authors_Other_Submissions_Paragraph,

            //ChangePassword
            [Description("Please enter your current password and new password")]
            Change_Password_Message,
            [Description("Required")]
            Change_Password_Required_Word_Text,
            [Description("Change Password")]
            Change_Password_Button_Form_Text,

            //OrderAutoGeneratedDescription
            [Description("Submission of {0} photos")]
            Order_Desctiption_Submission_Of_Count_Photos,

            [Description("Submission of {0} photos and {1} feedbacks.")]
            Order_Desctiption_Submission_Of_Count_Photos_And_Count_Of_Feedbacks,

            [Description("Selected {0} photos feedback request.")]
            Order_Desctiption_Selected_Count_Of_Photos_Feedback_Request,

            [Description("Submission of Storytelling.")]
            Order_Desctiption_Submission_of_StoryTelling,

            [Description("Submission of Storytelling and {1} feedbacks.")]
            Order_Desctiption_Submission_of_StoryTelling_And_Count_Of_Feedbacks,

            //ShortListedButton
            [Description("Shortlist Photos")]
            Short_List_Button_Form_Text,
            [Description("To ShortList:")]
            Short_List_Item_Title_Text,

            //Register
            [Description("YOUR DETAILS")]
            Register_Page_Title__Your_Details_Form,
            [Description("ACCOUNT DETAILS")]
            Register_Page_Title__Acount_Details_Form,
            [Description("register")]
            Register_Page_Button_Text,
            [Description("You have successfully registered with Malta International Photo Award. You can now log in and submit your photos.")]
            Register_Page_Success,

            //ExportShortedListPhotosExcelFile
            [Description("ShortListedPhotos-{0}.xlsx")]
            Download_Short_Listed_Photos_Excel_File_Name,

            [Description("FeedbackRequestedSubmissions-{0}.xlsx")]
            Download_Feedback_Requested_Submissions_Excel_File_Name,

            //Members ExcelFile Exportation
            [Description("MembersListing-{0}.xlsx")]
            Export_Members_Excel_File_Name,

            //LoginPage
            [Description("login")]
            Login_Page_Title_Login_Panel,
            [Description("Existing users can login form the below")]
            Login_Page_Explanation_Text_Login_Panel,

            //SubmissionPhotoAmount
            [Description("1")]
            Submission_Minimum_Photo_Amount,
            [Description("20")]
            Submission_Maximum_Photo_Amount,

            //StoryTellingAmount
            [Description("5")]
            Story_Telling_Amount,
            [Description("Dear User! You Are Submitting a StoryTelling.Please check your photo amount.Story Telling Submissions have to had just {0} Photo !")]
            Story_Telling_Amount_Message,

            //footer
            [Description("The Malta International Photography Award is a premier photo award that strives at promoting the art of photography from all across the world.")]
            Footer_Description_Company,

            [Description("We accept online payment securely via the following services:")]
            Footer_Payments_Method_Description,


            //recaptcha
            [Description("Please confirm that you are not a robot. Tick reCaptcha before submit.")]
            Recaptcha_Not_Filled,

            //ShortlistphotosFilterLabels
            [Description("Filter Submissions")]
            Shortlisted_Submissions_Filter_Form_Title_Text,
            [Description("Award Edition: ")]
            AwardEdition_Filter_Label_Text,
            [Description("Award Edition Category: ")]
            AwardEdition_Category_Filter_Label_Text,
            [Description("Sort By: ")]
            Submission_Filter_Label_Text,
            [Description("Shortlist Status: ")]
            Shortlist_Status_Filter_Label_Text,
            [Description("Keywords: ")]
            Search_Parameter_Filter_Label_Text,
            [Description("Filter Submissions")]
            Shortlisted_Submissions_Filter_Button_Text,

            //ExportMemberListAsAnExcelFile
            [Description("Click the button below to download the Memberlist in Excel format.")]
            Export_Members_Explanation_Text,
            [Description("Download Memberlist")]
            Export_Members_Button_Text,


        }

        public enum ActiveStatus
        {
            Active = 1,
            Deactive = 0
        }

        public enum RequestFeedback
        {
            Yes = 1,
            No = 0
        }

        public enum PaymentForSubmission
        {
            Yes = 1,
            No = 0
        }

        public enum PaymentForFeedback
        {
            Yes = 1,
            No = 0
        }

        public enum IsStoryTelling
        {
            Yes = 1,
            No = 0
        }

        public enum AwardEditionCompleted
        {
            NotCompleted=0,
            Completed=1
        }

        public enum PaymentStatus
        {
            [Description("Pending")]
            Recevied = 0,
            [Description("Completed")]
            OK = 1,
            [Description("Not Completed")]
            NOTOK = 2,
            [Description("Declined")]
            DECLINED = 3,
            [Description("Not Yet Paid")]
            PENDING = 4,
            [Description("Cancelled")]
            CANCEL = 5,
            [Description("Deleted")]
            DELETED = 6
        }

      
        public enum ShortListedStatus
        {
            [Description("Yes")]
            Yes = 1,
            [Description("No")]
            No = 2,
            [Description("Not Yet")]
            NotYet =0
        }

        public enum ShortListedStatusFilter
        {
            [Description("Yes")]
            Yes = 1,
            [Description("No")]
            No = 2,
            [Description("All")]
            All = 3,
            [Description("Not Yet")]
            NotYet = 0
        }


        public enum SubmissionStatus
        {
            [Description("Pending")]
            WaitingForPayment = 0,
            [Description("Paid And Waiting For Shortlisting")]
            PaidAndWaitingforShortlisting,
            [Description("Payment Not Completed")]
            NOTOK,
            [Description("Shortlisted")]
            Shortlisted,
            [Description("Not Shortlisted")]
            NotShortlisted,
            [Description("Winner-First Place")]
            WinnerFirstPlace,
            [Description("Winner-Second Place")]
            WinnerSecondPlace,
            [Description("Winner-Third Place")]
            WinnerThirdPlace,

        }

        public enum FeedbackStatus
        {
            [Description("Waiting For Feedback")]
            WaitingForFeedback = 0,
            [Description("Feedback Received")]
            FeedbackRecieved,
        }

        public enum FeedbackRepliedMailSent
        {
            [Description("Not Yet")]
            Notyet = 0,
            [Description("Replied")]
            Replied,
        }

        public enum SubmissionFilters
        {
            [Description("Date (Oldest First)")]
            DateOldestFirst = 0,
            [Description("Date (Newest First)")]
            DateNewestFirst,
            [Description("Name Of Submission (A...Z)")]
            SubmissionNameAscending,
            [Description("Name Of Submission (Z...A)")]
            SubmissionNameDescending
        }
    }

}

