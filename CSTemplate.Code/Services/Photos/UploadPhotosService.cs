﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CS.Common.Helpers;
using CS.Common.Services;
using CSTemplate.Code.Models.Shared;
using constants = CSTemplate.Code.Constants;
namespace CSTemplate.Code.Services.Photos
{

    public interface IUploadPhotosService
    {
        /// <summary>
        /// It turns uploaded file name
        /// </summary>
        /// <param name="photoList">List<HttpPostedFileBase> photoList</param>
        /// <returns>It turns uploaded photo name</returns>
        string Upload(HttpPostedFileBase photo);
    }

    [Service]
    public class UploadPhotosService : IUploadPhotosService
    {
        public string Upload(HttpPostedFileBase photo)
        {
            string returnOfPhotoName = string.Empty;
            if (photo == null) return returnOfPhotoName;
            if (photo != null)
            {
                if (photo.ContentType.Contains("image"))
                {
                    string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg",".JPG",".JPEG",".PNG",".GIF" };
                    var isPhoto = formats.Any(item => photo.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
                    if (isPhoto)
                    {
                        returnOfPhotoName = Guid.NewGuid().ToString() + Path.GetFileName(Constants.ReplaceDangerousCharacters.Replace(photo.FileName));
                        var urlOfFile = constants.ConstantValues.Submitted_Photo_Source_Address + Constants.ReplaceDangerousCharacters.Replace(returnOfPhotoName);
                        var serverSavePath = UrlHelpers.FileLocationToBaseDirectoryPath(urlOfFile.Trim());
                        photo.SaveAs(serverSavePath);
                    }
                   
                }
               
            }
            return returnOfPhotoName;
        }
    }
}
