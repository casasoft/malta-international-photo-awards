﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared.RequestFeedbackForSubmissionForm
{
    public class _RequestFeedbackForSubmissionFormModel
    {
        public string SubmissionID { get; set; }

        public string RequestFeedbackTitleText { get; set; }
        public string RequestPhotoFeedbackDescription { get; set; }
        public string RequestFeedbackButtonText { get; set; }

        //todo: [For: Fatih | 2018/09/13] These properties have to be here. Not in the SubmissionFullPageModel (WrittenBy: Radek)        			
        public bool PendingForFeedbackPayment { get; set; }
        public int SubmissionFeedbackRequestOrderId { get; set; }
        public bool HasBeenSubmittedForFeedback { get; set; }


    }
}
