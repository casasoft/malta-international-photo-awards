﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CSTemplate.Code.SitemapXml
{
    public class SitemapifyDocumentBuilder
    {
        public XDocument BuildSitemapXmlDocument(IEnumerable<SitemapLanguageUrl> sitemapUrls)
        {
            var elements = sitemapUrls.Select(url => url.ToXElement()).ToList();

            var assemblyName = GetType().Assembly.GetName();
            var description =
                $"Sitemapify - {GetType().Assembly.GetCustomAttribute<AssemblyDescriptionAttribute>()?.Description}";

            var headerComments = new[]
            {
                new XComment($" {description} "),
                new XComment($" Version: {assemblyName.Version} "),
                new XComment($" Generated: {DateTime.UtcNow:O} "),
                new XComment($" Count: {elements.Count} ")
            };

            return new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                headerComments,
                new XElement(XName.Get("urlset", SitemapLanguageUrl.SitemapNs), new XAttribute(XNamespace.Xmlns + "xhtml", SitemapLanguageUrl.SitemapXhtml), elements));
        }
    }
}
