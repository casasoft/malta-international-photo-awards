﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;
using ConstantValues = CS.Common.Umbraco.Constants.ConstantValues;

namespace CSTemplate.Code.Services.SubmitStorytelling
{
    public interface IGetSubmitStoryTellingFormService
    {
        _SubmitStoryTellingFormModel Get(UmbracoHelper umbracoHelper, IPublishedContent content);
    }

    [Service]
    public class GetSubmitStoryTellingFormService : IGetSubmitStoryTellingFormService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetSubmitStoryTellingFormService(
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }

        public _SubmitStoryTellingFormModel Get(UmbracoHelper umbracoHelper, IPublishedContent content)
        {
            var termsAndConditionsRawText =
                _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    Enums.DictionaryKey.Dictionary_Form_Terms_And_Conditions_Text);
            var termsAndConditionsUrl = _umbracoHelperService.TypedContent(umbracoHelper, ConstantValues.Terms_And_Conditions_Page_Id).Url;

            var termsAndConditionsFormatted = String.Format(termsAndConditionsRawText, termsAndConditionsUrl);

            var requiredText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Dictionary_Form_Required_Text);

            var buttonFromText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_Photo_Button_Form_Text);

            var PhotoFormTitle = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_Photo_Title_Form_Text);

            var uploadInformationText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
             Enums.DictionaryKey.Upload_Information_Text);


            _SubmitStoryTellingFormModel submitStoryTellingFormModel = new _SubmitStoryTellingFormModel()
            {
                TermsAndConditionsText = termsAndConditionsFormatted,
                ButtonFormText = buttonFromText,
                UploadInformationText=uploadInformationText,
                SubmitPhotoItem = new _SubmitPhotoItemModel()
                {
                    TitleForm = PhotoFormTitle,
                    Required = requiredText,
                    
                    isAStortyTelling = true,
                }
            };
            return submitStoryTellingFormModel;
        }
    }
}
