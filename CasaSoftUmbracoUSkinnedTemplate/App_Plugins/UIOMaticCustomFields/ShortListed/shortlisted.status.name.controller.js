﻿angular.module("umbraco").controller("ShortListed",
    function ($scope, $http) {
        var url = "backoffice/ShortListed/ShortListedApi/Get/" + $scope.property.value;
        $http.get(url).then(function (response) {
            $scope.shortListedStatus = response.data;

        });
    });