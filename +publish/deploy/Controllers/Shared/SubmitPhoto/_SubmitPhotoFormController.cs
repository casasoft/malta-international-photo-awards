﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.SubmitPhoto;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Shared.SubmitPhoto
{
    public partial class _SubmitPhotoFormController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetSubmitPhotosFormService _getSubmitPhotoSelectionService;

        public _SubmitPhotoFormController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetSubmitPhotosFormService getSubmitPhotoSelectionService
        )
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getSubmitPhotoSelectionService = getSubmitPhotoSelectionService;
        }

        // GET: _SubmitPhotoForm
        //public virtual ActionResult _SubmitPhotoForm(RenderModel model)
        //{
        //    var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

        //    _SubmitPhotoFormModel submitPhotoFormModel = _getSubmitPhotoSelectionService.Get(umbracoHelper, model.Content);
        //    return base.Populate(submitPhotoFormModel);
        //}
    }
}