﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using Umbraco.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using System.Reflection;
using System.Reflection.Emit;
using CS.Common.Helpers;

namespace CS.Common.Umbraco.Services.Umbraco.Wrappers.Services
{
    public interface IUmbracoControllerWrapperService
    {
        UmbracoHelper GetUmbracoHelperForUmbracoController(UmbracoController controller);
        UmbracoHelper GetUmbracoHelperForUmbracoController(PluginController controller);
        ActionResult CallBaseIndexMethod(RenderMvcController controller, RenderModel renderModel);
    }

    [Service]
    public class UmbracoControllerWrapperService : IUmbracoControllerWrapperService
    {
        public UmbracoHelper GetUmbracoHelperForUmbracoController(UmbracoController controller)
        {
            return controller.Umbraco;
        }
        public UmbracoHelper GetUmbracoHelperForUmbracoController(PluginController controller)
        {
            return controller.Umbraco;
        }

        public ActionResult CallBaseIndexMethod(RenderMvcController controller, RenderModel renderModel)
        {
            //Invoke the method in the base as else it will be a stack overflow.
            var actionResult =
                (ActionResult)
                    ReflectionHelpers.InvokeMethodInBaseNotOverridden(typeof (RenderMvcController).GetMethod("Index"),
                        controller,
                        renderModel);
            return actionResult;
        }
    }
}
