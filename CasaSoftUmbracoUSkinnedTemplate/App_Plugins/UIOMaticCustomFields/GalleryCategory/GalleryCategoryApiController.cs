﻿using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.GalleryCategories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.WebPages.Html;
using Umbraco.Web;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedContentModels;

namespace CasaSoftUmbracoUSkinnedTemplate.App_Plugins.UIOMaticCustomFields.GalleryCategory
{
    [PluginController("GalleryCategory")]
    public class GalleryCategoryApiController : UmbracoAuthorizedJsonController
    {
        private readonly IGetAwardEditionCategoryListFilterByAwardEditionId _getAwardEditionCategoryListFilterBySubmissionCategoryId;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private Umbraco.Web.UmbracoHelper umbracoHelper;
        public GalleryCategoryApiController(
            IGetAwardEditionCategoryListFilterByAwardEditionId getAwardEditionCategoryListFilterBySubmissionCategoryId,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService
            )
        {
            umbracoHelper = new Umbraco.Web.UmbracoHelper(UmbracoContext.Current);
            _getAwardEditionCategoryListFilterBySubmissionCategoryId = getAwardEditionCategoryListFilterBySubmissionCategoryId;
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
        }
        public List<SelectListItem> GetAll(int id)
        {
           
            var galleryCategoryList = _getAwardEditionCategoryListFilterBySubmissionCategoryId.Get(umbracoHelper,id);
            
            return galleryCategoryList;

        }
        public List<SelectListItem> GetStatusList()
        {

            var orderStatusList = Enum.GetValues(typeof(CSTemplate.Code.Constants.Enums.PaymentStatus)).Cast<CSTemplate.Code.Constants.Enums.PaymentStatus>().Select(v => new SelectListItem
            {
                
                Text = CS.Common.Helpers.EnumHelpers.GetDescriptionAttributeValue(v),
                Value = ((int)v).ToString()
            }).ToList();
            return orderStatusList;

        }
        public string GetGalleryCategoryName(int id)
        {
            if (id <= 0) return "None";
            var galleryCategory = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, id);
            return galleryCategory.AwardCategoryItemName;
        }

        
    }
}
