//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>Pod</summary>
	[PublishedContentModel("USN_SC_Pod")]
	public partial class Usn_Sc_Pod : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "USN_SC_Pod";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Usn_Sc_Pod(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Usn_Sc_Pod, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Name: The name you enter is for your reference only and will not appear on the website.
		///</summary>
		[ImplementPropertyType("itemName")]
		public string ItemName
		{
			get { return this.GetPropertyValue<string>("itemName"); }
		}

		///<summary>
		/// Image: 800px x 600px  Focal point is defined within your "Media" section.
		///</summary>
		[ImplementPropertyType("podImage")]
		public IPublishedContent PodImage
		{
			get { return this.GetPropertyValue<IPublishedContent>("podImage"); }
		}

		///<summary>
		/// Image alt text
		///</summary>
		[ImplementPropertyType("podImageAltText")]
		public string PodImageAltText
		{
			get { return this.GetPropertyValue<string>("podImageAltText"); }
		}

		///<summary>
		/// Link: The "Caption" field will be used for your link text.
		///</summary>
		[ImplementPropertyType("podLink")]
		public Umbraco.Web.Models.RelatedLinks PodLink
		{
			get { return this.GetPropertyValue<Umbraco.Web.Models.RelatedLinks>("podLink"); }
		}

		///<summary>
		/// Text
		///</summary>
		[ImplementPropertyType("podText")]
		public IHtmlString PodText
		{
			get { return this.GetPropertyValue<IHtmlString>("podText"); }
		}

		///<summary>
		/// Heading
		///</summary>
		[ImplementPropertyType("podTitle")]
		public string PodTitle
		{
			get { return this.GetPropertyValue<string>("podTitle"); }
		}
	}
}
