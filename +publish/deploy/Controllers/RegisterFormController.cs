﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Registration;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedContentModels;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class RegisterFormController : SurfaceController
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private UmbracoHelper _umbracoHelper;
        private readonly IMemberServiceWrapper _memberServiceWrapper;
        private readonly ISendEmailsAfterRegistrationService _sendEmailsAfterRegistrationService;

        public RegisterFormController(IUmbracoHelperService
            umbracoHelperService,
            IUmbracoControllerWrapperService
            umbracoControllerWrapperService,
            IMemberServiceWrapper
            memberServiceWrapper,
            ISendEmailsAfterRegistrationService
            sendEmailsAfterRegistrationService)
        {
            _sendEmailsAfterRegistrationService = sendEmailsAfterRegistrationService;
            _memberServiceWrapper = memberServiceWrapper;
            _umbracoHelperService = umbracoHelperService;

            _umbracoHelper = umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
        }

        [HttpPost]
        public virtual ActionResult HandleRegisterFormSubmit(_RegisterFormModel registerFormModel)
        {
            //the code below and the services above have been copied from CSTemplate from the RegisterFormController ( Radek, 16.08.18).
            var registerSuccessfulPage =
                _umbracoHelperService.LoadRootNodeForCurrentDomain(_umbracoHelper)
                .Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Login_Page_Document_Type_Alias))
                .FirstOrDefault().Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Register_Page_Document_Type_Alias))
                .FirstOrDefault().Children().FirstOrDefault();

            var umbracoRegisterModel = Members.CreateRegistrationModel();
            umbracoRegisterModel.RedirectUrl = registerSuccessfulPage.Url;
            umbracoRegisterModel.Name = registerFormModel.Name + " " + registerFormModel.Surname;
            umbracoRegisterModel.Email = registerFormModel.InsertedEmail;
            umbracoRegisterModel.Password = registerFormModel.InsertedPassword;
            umbracoRegisterModel.UsernameIsEmail = true;
            umbracoRegisterModel.MemberProperties = new List<UmbracoProperty>()
            {
                new UmbracoProperty()
                {
                    Alias = ConstantValues.MemberApproved,
                    Value = "1"
                },

                new UmbracoProperty()
                {
                    Alias = ConstantValues.MemberAccountFirstName,
                    Value = registerFormModel.Name
                },

                new UmbracoProperty()
                {
                    Alias = ConstantValues.MemberAccountSurname,
                    Value = registerFormModel.Surname
                },

                new UmbracoProperty()
                {
                    Alias = ConstantValues.MemberAccountPhone,
                    Value = registerFormModel.InsertedPhone
                },

                new UmbracoProperty()
                {
                    Alias = ConstantValues.MemberAccountCountry,
                    Value = registerFormModel.InsertedCountry.ToString()
                },

                new UmbracoProperty()
                {
                    Alias = ConstantValues.MemberAccountNationality,
                    Value = registerFormModel.Nationality
                },

                new UmbracoProperty()
                {
                    Alias = ConstantValues.MemberAccountAgreeToReceivePromotionalMaterials,
                    Value = registerFormModel.PromotionalMaterials ? "1" : "0"
                },
            };


            MembershipCreateStatus status;
            var member = Members.RegisterMember(umbracoRegisterModel, out status, umbracoRegisterModel.LoginOnSuccess);

            switch (status)
            {
                case MembershipCreateStatus.Success:

                    var registeredMember = _memberServiceWrapper.GetByUsername(_umbracoHelper.UmbracoContext, member.UserName);

                    //assign group to a user
                    _memberServiceWrapper.AssignRole(_umbracoHelper.UmbracoContext, registeredMember.Id,
                        ConstantValues.Member_Group_Client_Name);

                    Members.Logout();

                    //send emails accordingly
                    _sendEmailsAfterRegistrationService.SendEmails(_umbracoHelper,
                        new Member(_umbracoHelperService.TypedMember(_umbracoHelper, registeredMember.Id)), registeredMember.Email);

                    //if there is a specified path to redirect to then use it
                    if (umbracoRegisterModel.RedirectUrl.IsNullOrWhiteSpace() == false)
                    {
                        return Redirect(umbracoRegisterModel.RedirectUrl);
                    }
                    //redirect to current page by default

                    return RedirectToCurrentUmbracoPage();
                case MembershipCreateStatus.InvalidUserName:
                    ModelState.AddModelError((umbracoRegisterModel.UsernameIsEmail || umbracoRegisterModel.Username == null)
                        ? "InsertedEmail"
                        : "registerModel.Username",
                        "Username is not valid");
                    break;
                case MembershipCreateStatus.InvalidPassword:
                    ModelState.AddModelError("InsertedPassword", "The password must be at least 8 characters long.");
                    break;
                case MembershipCreateStatus.InvalidQuestion:
                case MembershipCreateStatus.InvalidAnswer:
                    //TODO: Support q/a http://issues.umbraco.org/issue/U4-3213
                    throw new NotImplementedException(status.ToString());
                case MembershipCreateStatus.InvalidEmail:
                    ModelState.AddModelError("InsertedEmail", "Email is invalid");
                    break;
                case MembershipCreateStatus.DuplicateUserName:
                    ModelState.AddModelError((umbracoRegisterModel.UsernameIsEmail || umbracoRegisterModel.Username == null)
                        ? "InsertedEmail"
                        : "registerModel.Username",
                        "A member with this username already exists.");
                    break;
                case MembershipCreateStatus.DuplicateEmail:
                    ModelState.AddModelError("registerModel.Email", "A member with this e-mail address already exists");
                    break;
                case MembershipCreateStatus.UserRejected:
                case MembershipCreateStatus.InvalidProviderUserKey:
                case MembershipCreateStatus.DuplicateProviderUserKey:
                case MembershipCreateStatus.ProviderError:
                    //don't add a field level error, just model level
                    ModelState.AddModelError("registerModel", "An error occurred creating the member: " + status);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return CurrentUmbracoPage();
        }

    }
}