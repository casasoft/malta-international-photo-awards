﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.CategoryGallery;
using CSTemplate.Code.Models.Shared;
using Microsoft.Owin.Security.Provider;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class CategoryGalleryPageDataController : USNBaseController
    {


        public virtual ActionResult CategoryGalleryPage(RenderModel model)
        {

            CategoryGalleryPageModel categoryGalleryPageModel = new CategoryGalleryPageModel(model.Content);

            DateTime currentTime = DateTime.Now;
            categoryGalleryPageModel.ListOfPhotos = new List<_ItemPhotoModel>()
            {
                #region DummyData
               new _ItemPhotoModel()
                            {
                                ItemPhotoDate = currentTime,
                                ItemPhotoImgURL = "/dist/assets/img/1.jpg",
                                ItemPhotoName = "John Smith",
                                ItemPhotoAward = "Landscape photography Award",
                                ItemPhotoLocation = "Vence, Italy",
                                ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
                                ItemPhotoCategory = "nature"
                            },
                            new _ItemPhotoModel()
                            {
                                ItemPhotoDate = currentTime,
                                ItemPhotoImgURL = "/dist/assets/img/2.jpg",
                                ItemPhotoName = "John Smith",
                                ItemPhotoAward = "Landscape photography Award",
                                ItemPhotoLocation = "Vence, Italy",
                                ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
                                ItemPhotoCategory = "nature"
                            },
                            new _ItemPhotoModel()
                            {
                                ItemPhotoDate = currentTime,
                                ItemPhotoImgURL = "/dist/assets/img/3.jpg",
                                ItemPhotoName = "John Smith",
                                ItemPhotoAward = "Landscape photography Award",
                                ItemPhotoLocation = "Vence, Italy",
                                ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
                            },
                            new _ItemPhotoModel()
                            {
                                ItemPhotoDate = currentTime,
                                ItemPhotoImgURL = "/dist/assets/img/4.jpg",
                                ItemPhotoName = "John Smith",
                                ItemPhotoAward = "Landscape photography Award",
                                ItemPhotoLocation = "Vence, Italy",
                                ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
                                ItemPhotoCategory = "nature"
                            },
                            new _ItemPhotoModel()
                            {
                                ItemPhotoDate = currentTime,
                                ItemPhotoImgURL = "/dist/assets/img/5.jpg",
                                ItemPhotoName = "John Smith",
                                ItemPhotoAward = "Landscape photography Award",
                                ItemPhotoLocation = "Vence, Italy",
                                ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
                                ItemPhotoCategory = "war"
                            }
                #endregion
            };
            return base.Populate(categoryGalleryPageModel);
        }
        // GET: CategoryGalleryPage

    }
}