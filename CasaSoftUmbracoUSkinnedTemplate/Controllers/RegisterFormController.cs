﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CS.Common.Helpers;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.GoogleReCaptcha;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Registration;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedContentModels;
using USNOptions = USNStarterKit.USNEnums.Options;
using MailChimp;
using MailChimp.Helper;
using MailChimp.Lists;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class RegisterFormController : SurfaceController
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private UmbracoHelper _umbracoHelper;
        private readonly IMemberServiceWrapper _memberServiceWrapper;
        private readonly ISendEmailsAfterRegistrationService _sendEmailsAfterRegistrationService;

        public RegisterFormController(IUmbracoHelperService
                umbracoHelperService,
            IUmbracoControllerWrapperService
                umbracoControllerWrapperService,
            IMemberServiceWrapper
                memberServiceWrapper,
            ISendEmailsAfterRegistrationService
                sendEmailsAfterRegistrationService)
        {
            _sendEmailsAfterRegistrationService = sendEmailsAfterRegistrationService;
            _memberServiceWrapper = memberServiceWrapper;
            _umbracoHelperService = umbracoHelperService;

            _umbracoHelper = umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
        }



        /// <summary>  
        /// Validate Captcha  
        /// </summary>  
        /// <param name="response"></param>  
        /// <returns></returns>  
        public static CaptchaResponse ValidateCaptcha(string response)
        {
            string secret = System.Web.Configuration.WebConfigurationManager.AppSettings["recaptchaPrivateKey"];
            var client = new WebClient();
            var jsonResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
            return JsonConvert.DeserializeObject<CaptchaResponse>(jsonResult.ToString());
        }

        [HttpPost]
        public virtual ActionResult HandleRegisterFormSubmit(_RegisterFormModel registerFormModel)
        {
            //the code below and the services above have been copied from CSTemplate from the RegisterFormController ( Radek, 16.08.18).
            //var registerSuccessfulPage =
            //    _umbracoHelperService.LoadRootNodeForCurrentDomain(_umbracoHelper)
            //    .Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Login_Page_Document_Type_Alias))
            //    .FirstOrDefault().Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Register_Page_Document_Type_Alias))
            //    .FirstOrDefault().Children().FirstOrDefault();
            CaptchaResponse response = ValidateCaptcha(Request["g-recaptcha-response"]);
            if (response.Success)
            {
                //todo: [For: Backend | 2018/09/26] We need to register WhereDidYouHearAboutUs and WhereDidYouHearAboutUsCustomText  (WrittenBy: Radek)
                CS.Common.Constants.Enums.WhereDidYouHearAboutUsList WhereDidYouHearAboutUsList = (CS.Common.Constants.Enums.WhereDidYouHearAboutUsList)registerFormModel.WhereDidYouHearAboutUsInserted;
                string WhereDidYouHearAboutUs = EnumHelpers.GetDisplayName(WhereDidYouHearAboutUsList);

                //when user picks "Other" from dropdown, WhereDidYouHearAboutUsCustomText property will be filled in with text. Otherwise it's a null always.
                string whereDidYouHearAboutUsCustomText = registerFormModel.WhereDidYouHearAboutUsCustomText;

                string whereDidYouHearAboutUsText = WhereDidYouHearAboutUs;
                if (!string.IsNullOrEmpty(whereDidYouHearAboutUsCustomText))
                {
                    whereDidYouHearAboutUsText += " - " + whereDidYouHearAboutUsCustomText;
                }

                var loginpageUrl = _umbracoHelperService.LoadRootNodeForCurrentDomain(_umbracoHelper)
                    .Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Login_Page_Document_Type_Alias))
                    .FirstOrDefault();
                var umbracoRegisterModel = Members.CreateRegistrationModel();
                //umbracoRegisterModel.RedirectUrl = registerSuccessfulPage.Url;
                umbracoRegisterModel.Name = registerFormModel.Name + " " + registerFormModel.Surname;
                umbracoRegisterModel.Email = registerFormModel.InsertedEmail;
                umbracoRegisterModel.Password = registerFormModel.InsertedPassword;
                umbracoRegisterModel.UsernameIsEmail = true;
                umbracoRegisterModel.MemberProperties = new List<UmbracoProperty>()
                {
                    new UmbracoProperty()
                    {
                        Alias = ConstantValues.MemberApproved,
                        Value = "1"
                    },

                    new UmbracoProperty()
                    {
                        Alias = ConstantValues.MemberAccountFirstName,
                        Value = registerFormModel.Name
                    },

                    new UmbracoProperty()
                    {
                        Alias = ConstantValues.MemberAccountSurname,
                        Value = registerFormModel.Surname
                    },

                    //new UmbracoProperty()
                    //{
                    //    Alias = ConstantValues.MemberAccountPhone,
                    //    Value = registerFormModel.InsertedPhone
                    //},

                    new UmbracoProperty()
                    {
                        Alias = ConstantValues.MemberAccountCountry,
                        Value = registerFormModel.InsertedCountry.ToString()
                    },

                    new UmbracoProperty()
                    {
                        Alias = ConstantValues.MemberAccountNationality,
                        Value = registerFormModel.Nationality
                    },

                    new UmbracoProperty()
                    {
                        Alias = ConstantValues.MemberAccountAgreeToReceivePromotionalMaterials,
                        Value = registerFormModel.PromotionalMaterials ? "1" : "0"
                    },
                    new UmbracoProperty{
                        Alias = ConstantValues.WhereDidYouHearAboutUs,
                        Value = whereDidYouHearAboutUsText
                    }
                };

              

                MembershipCreateStatus status;
                var member = Members.RegisterMember(umbracoRegisterModel, out status, umbracoRegisterModel.LoginOnSuccess);

                if (registerFormModel.PromotionalMaterials)
                {
                    try
                    {
                        var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(_umbracoHelper)).WebsiteConfigurationNode;
                        var globalSettingsNode = websiteConfigurationNode.Children()
                            .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.USN_Global_Settings_Document_Type_Alias))
                            .Select(x => (UsnglobalSettings)x).FirstOrDefault();
                        var emailMarketingPlatform = globalSettingsNode.EmailMarketingPlatform;
                        var newsletterAPIKey = globalSettingsNode.NewsletterApikey;
                        var defaultNewsletterSubscriberListID = globalSettingsNode.DefaultNewsletterSubscriberListID;
                        if (emailMarketingPlatform == USNOptions.Newsletter_Mailchimp)
                        {
                            var mc = new MailChimpManager(newsletterAPIKey);
                            string subsciberListID = String.Empty;
                            subsciberListID = defaultNewsletterSubscriberListID;
                            var email = new EmailParameter()
                            {
                                Email = registerFormModel.InsertedEmail
                            };

                            var myMergeVars = new MergeVar();
                            myMergeVars.Add("FNAME", registerFormModel.Name);
                            myMergeVars.Add("LNAME", registerFormModel.Surname);

                            EmailParameter results = mc.Subscribe(subsciberListID, email, myMergeVars, "html", false, true, false, false);
                        }
                    }
                    catch (Exception e)
                    {
                        //do nothing
                    }

                }
                switch (status)
                {
                    case MembershipCreateStatus.Success:

                        var registeredMember = _memberServiceWrapper.GetByUsername(_umbracoHelper.UmbracoContext, member.UserName);

                        //assign group to a user
                        _memberServiceWrapper.AssignRole(_umbracoHelper.UmbracoContext, registeredMember.Id,
                            ConstantValues.Member_Group_Client_Name);

                        Members.Logout();

                        //send emails accordingly
                        _sendEmailsAfterRegistrationService.SendEmails(_umbracoHelper,
                            new Member(_umbracoHelperService.TypedMember(_umbracoHelper, registeredMember.Id)), registeredMember.Email);

                        //if there is a specified path to redirect to then use it

                        TempData["registerSuccess"] = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(_umbracoHelper, Enums.DictionaryKey.Register_Page_Success);
                        //if (umbracoRegisterModel.RedirectUrl.IsNullOrWhiteSpace() == false)
                        //{

                        //    return Redirect(umbracoRegisterModel.RedirectUrl);
                        //}
                        //redirect to current page by default

                        return Redirect(loginpageUrl.Url);
                    case MembershipCreateStatus.InvalidUserName:
                        ModelState.AddModelError((umbracoRegisterModel.UsernameIsEmail || umbracoRegisterModel.Username == null)
                                ? "InsertedEmail"
                                : "registerModel.Username",
                            "Username is not valid");
                        break;
                    case MembershipCreateStatus.InvalidPassword:
                        ModelState.AddModelError("InsertedPassword", "The password must be at least 8 characters long.");
                        break;
                    case MembershipCreateStatus.InvalidQuestion:
                    case MembershipCreateStatus.InvalidAnswer:
                        //TODO: Support q/a http://issues.umbraco.org/issue/U4-3213
                        throw new NotImplementedException(status.ToString());
                    case MembershipCreateStatus.InvalidEmail:
                        ModelState.AddModelError("InsertedEmail", "Email is invalid");
                        break;
                    case MembershipCreateStatus.DuplicateUserName:
                        ModelState.AddModelError((umbracoRegisterModel.UsernameIsEmail || umbracoRegisterModel.Username == null)
                                ? "InsertedEmail"
                                : "registerModel.Username",
                            "A member with this username already exists.");
                        break;
                    case MembershipCreateStatus.DuplicateEmail:
                        ModelState.AddModelError("registerModel.Email", "A member with this e-mail address already exists");
                        break;
                    case MembershipCreateStatus.UserRejected:
                    case MembershipCreateStatus.InvalidProviderUserKey:
                    case MembershipCreateStatus.DuplicateProviderUserKey:
                    case MembershipCreateStatus.ProviderError:
                        //don't add a field level error, just model level
                        ModelState.AddModelError("registerModel", "An error occurred creating the member: " + status);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                return CurrentUmbracoPage();


            }
            else
            {

                TempData.Add("recaptchaFailed", _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(_umbracoHelper, Enums.DictionaryKey.Recaptcha_Not_Filled));
                return RedirectToCurrentUmbracoPage();

            }




        }

    }
}