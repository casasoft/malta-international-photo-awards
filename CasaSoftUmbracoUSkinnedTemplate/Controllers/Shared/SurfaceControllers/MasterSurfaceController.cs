﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Shared.SurfaceControllers
{
    public partial class MasterSurfaceController : SurfaceController
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        public MasterSurfaceController(IUmbracoHelperService umbracoHelperService, IUmbracoControllerWrapperService umrbaControllerWrapperService)
        {
            _umbracoControllerWrapperService = umrbaControllerWrapperService;
            _umbracoHelperService = umbracoHelperService;
        }

        /// <summary>
        /// Creates an antiforgery token without being cached
        /// </summary>
        /// <returns>AntiForgery Token</returns>
        [ChildActionOnly]
        public virtual ContentResult GetAntiForgeryToken()
        {
            using (var viewPage = new ViewPage())
            {
                var htmlHelper = new HtmlHelper(new ViewContext(), viewPage);
                var token = htmlHelper.AntiForgeryToken();
                return Content(token.ToHtmlString());
            }
        }

        [ChildActionOnly]
        public virtual ActionResult GetFooterPayments()
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            var footerPaymentsModel = new _FooterPaymentMethodModel();


            footerPaymentsModel.FooterPhotoTitleImgURL = "/dist/assets/img/logo.png";
            footerPaymentsModel.FooterText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.Footer_Description_Company) ;
            footerPaymentsModel.FooterPaymentMethodText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.Footer_Payments_Method_Description);
            footerPaymentsModel.FooterPaymentMethodImgURL1 = "/dist/assets/img/securepayment.png";
            footerPaymentsModel.FooterPaymentMethodImgURL2 = "/dist/assets/img/visa.png";
            footerPaymentsModel.FooterPaymentMethodImgURL3 = "/dist/assets/img/mastercard.png";


            return PartialView(MVC.Shared.Views.FooterPaymentMethod._FooterPaymentMethod, footerPaymentsModel);
        }

    }
}