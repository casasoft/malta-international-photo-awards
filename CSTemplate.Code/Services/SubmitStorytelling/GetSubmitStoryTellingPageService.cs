﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.SubmitStorytelling;
using CSTemplate.Code.Services.Loader;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.SubmitStorytelling
{

    public interface IGetSubmitStoryTellingPageService
    {
        SubmitStoryTellingPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content);
    }

    [Service]
    public class GetSubmitStoryTellingPageService : IGetSubmitStoryTellingPageService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetSubmitStoryTellingFormService _getSubmitStoryTellingFormService;
        private readonly IGetLoaderPageService _getLoaderPageService;

        public GetSubmitStoryTellingPageService(
            IUmbracoHelperService umbracoHelperService,
            IGetSubmitStoryTellingFormService getSubmitStoryTellingFormService,
            IGetLoaderPageService getLoaderPageService
        )
        {
            _umbracoHelperService = umbracoHelperService;
            _getSubmitStoryTellingFormService = getSubmitStoryTellingFormService;
            _getLoaderPageService = getLoaderPageService;
        }


        public SubmitStoryTellingPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content)
        {
            var explanationFormText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_StoryTelling_Explanation_Text);
            var explanationStoryTellingFormText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_StoryTelling_Explanation_StoryTellingText);
            var explanationFileInputFormText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_StoryTelling_Explanation_FileInput);
            var browserSuggestionText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
               Enums.DictionaryKey.Browser_Suggestion_Text);
            SubmitStoryTellingPageModel submitStoryTellingPageModel = new SubmitStoryTellingPageModel(content)
            {
                ExplanationText = explanationFormText,
                ExplanationStoryTelling = explanationStoryTellingFormText,
                ExplanationFileInput = explanationFileInputFormText,
                BrowserSuggestionText = browserSuggestionText,
                LoaderPageModel = _getLoaderPageService.Get(umbracoHelper),
                SubmitStoryTellingForm = _getSubmitStoryTellingFormService.Get(umbracoHelper, content)
            };

            return submitStoryTellingPageModel;
        }
    }
}
