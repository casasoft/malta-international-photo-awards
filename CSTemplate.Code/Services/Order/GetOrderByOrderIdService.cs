﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Order
{

    public interface IGetOrderByOrderIdService
    {
        OrderPoco Get(int orderId);
    }

    [Service]
    public class GetOrderByOrderIdService : IGetOrderByOrderIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetOrderByOrderIdService(IGetCurrentUmbracoDatabaseService
            getgCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getgCurrentUmbracoDatabaseService;
        }

        public OrderPoco Get(int orderId)
        {
            var db = _getCurrentUmbracoDatabaseService.Get();

            var sql = new Sql()
                .Select("*")
                .From(DatabaseConstantValues.OrdersTableName + " " +
                      DatabaseConstantValues.aliasForOrdersPocoTableNameAlias)
                .Where(DatabaseConstantValues.aliasForOrdersPocoTableNameAlias + "." +
                       DatabaseConstantValues.Orders_Poco_Primary_Key_Value + " = " +
                       orderId);

            return db.Fetch<OrderPoco>(sql).FirstOrDefault();
        }
    }
}
