﻿using System.Collections.Generic;
using System.Web.Routing;

namespace CS.Common.Helpers
{
    public static class DictionaryHelpers
    {
        public static Dictionary<string, object> GetDictionaryFromNameValueParameters(params string[] parameters)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            for (int i = 0; i < parameters.Length; i += 2)
            {
                dict[parameters[i]] = parameters[i + 1];
            }
            return dict;
        }
        public static TOutputType MergeDictionaries<TOutputType, TKey, TValue>(params IDictionary<TKey, TValue> [] dictionaries)
            where TOutputType : IDictionary<TKey,TValue>, new()
        {
            TOutputType dict = new TOutputType();
            for (int i = 0;i<dictionaries.Length;i++) 
            {
                IDictionary<TKey, TValue> dictionary = dictionaries[i];
                foreach (KeyValuePair<TKey, TValue> dictionaryEntry in dictionary)
                {
                    dict[dictionaryEntry.Key] = dictionaryEntry.Value;
                }
            }
            return dict;
        }
        public static RouteValueDictionary MergeRouteValueDictionaries(params IDictionary<string, object>[] dictionaries)
        {
            return MergeDictionaries<RouteValueDictionary, string, object>(dictionaries);
        }
    }
}
