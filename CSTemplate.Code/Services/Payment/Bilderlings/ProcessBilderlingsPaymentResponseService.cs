﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Order;
using CSTemplate.Code.Services.Submissions;
using Umbraco.Core.Logging;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Payment.Bilderlings
{

    public interface IProcessBilderlingsPaymentResponseService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="umbracoHelper"></param>
        /// <param name="Request"></param>
        /// <returns>Payment Status</returns>
        string Process(UmbracoHelper umbracoHelper, string status, string invoice_ref, string orderRefId);
    }

    [Service]
    public class ProcessBilderlingsPaymentResponseService : IProcessBilderlingsPaymentResponseService
    {
        private readonly IGetOrderByOrderRefIdService _getOrderByOrderRefIdService;
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IUpdateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService _updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService;
        private readonly ISendEmailUponCompletedSubmissionService _sendEmailUponCompletedSubmissionService;

        public ProcessBilderlingsPaymentResponseService(
            IGetOrderByOrderRefIdService
            getOrderByOrderRefIdService,
            IGetCurrentUmbracoDatabaseService
            getCurrentUmbracoDatabaseService,
            IUpdateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService
            updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService,
            ISendEmailUponCompletedSubmissionService
            sendEmailUponCompletedSubmissionService)
        {
            _sendEmailUponCompletedSubmissionService = sendEmailUponCompletedSubmissionService;
            _updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService = updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
            _getOrderByOrderRefIdService = getOrderByOrderRefIdService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="umbracoHelper"></param>
        /// <param name="Request"></param>
        /// <returns>Payment Status</returns>
        public string Process(UmbracoHelper umbracoHelper, string status, string invoice_ref, string orderRefId)
        {
            var orderInvoiceId = invoice_ref;
            var transactionStatus = status;

            LogHelper.Info<ProcessBilderlingsPaymentResponseService>("Order Invoice: " + orderInvoiceId);
            LogHelper.Info<ProcessBilderlingsPaymentResponseService>("Transaction Status: " + transactionStatus);
            LogHelper.Info<ProcessBilderlingsPaymentResponseService>("Order Ref Id: " + orderRefId);


            if (!String.IsNullOrEmpty(orderInvoiceId) && !String.IsNullOrEmpty(transactionStatus) &&
                !String.IsNullOrEmpty(orderRefId))
            {
                LogHelper.Info<ProcessBilderlingsPaymentResponseService>("Everything Filled In");
                var orderAsPoco = _getOrderByOrderRefIdService.Get(orderRefId);
                if (orderAsPoco != null)
                {
                    LogHelper.Info<ProcessBilderlingsPaymentResponseService>("orderAsPoco Is not null");
                    orderAsPoco.Date = DateTime.Now;

                   
                        LogHelper.Info<ProcessBilderlingsPaymentResponseService>("BilderlingsInvoiceRef Empty");
                        LogHelper.Info<ProcessBilderlingsPaymentResponseService>("Transaction status to lower Empty: " + transactionStatus.ToLower());
                        LogHelper.Info<ProcessBilderlingsPaymentResponseService>("Success status as constant: " + ConstantValues.Bilderlings_Transaction_Payment_Response_Succeeded);

                        var transactionStatusAsLower = transactionStatus.ToLower();
                        orderAsPoco.PaymentTransactionResponse = transactionStatusAsLower;
                        if (transactionStatusAsLower.Equals(
                            ConstantValues.Bilderlings_Transaction_Payment_Response_Succeeded))
                        {
                            LogHelper.Info<ProcessBilderlingsPaymentResponseService>("Bilderlings success");

                            orderAsPoco.PaymentStatusEnumId = (int) Enums.PaymentStatus.OK;

                            //you get the submission linked with this order that has been paid for photos
                            //and change it to payment completed
                            var orderId = orderAsPoco.Id;
                            _updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService
                                .Update(orderId);
                            orderAsPoco.BilderlingsInvoiceRef = orderInvoiceId;

                            LogHelper.Info<ProcessBilderlingsPaymentResponseService>("After Update");


                            //HERE WE NEED TO SEND EMAILS BOTH TO ADMINISTRATORS AND TO THE CLIENT ABOUT THE NEW SUBMISSIONS
                            try
                            {
                                LogHelper.Info<ProcessBilderlingsPaymentResponseService>("Before Sending Emails for payment successfull");

                                _sendEmailUponCompletedSubmissionService.Send(umbracoHelper,
                                    orderId,
                                    orderAsPoco.UserId);

                                LogHelper.Info<ProcessBilderlingsPaymentResponseService>("After Sending Emails for payment successfull");

                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        else
                        {
                            //something failed.
                            LogHelper.Info<ProcessBilderlingsPaymentResponseService>("Before Sending Emails for Payment Failed");

                            var orderId = orderAsPoco.Id;
                            _sendEmailUponCompletedSubmissionService.Send(umbracoHelper,
                                orderId,
                                orderAsPoco.UserId, paymentSuccessfull:false);

                            LogHelper.Info<ProcessBilderlingsPaymentResponseService>("After Sending Emails for Payment Failed");
                        }

                        using (var db = _getCurrentUmbracoDatabaseService.Get())
                        {
                            LogHelper.Info<ProcessBilderlingsPaymentResponseService>("Before Update Orders Table");

                            db.Update(DatabaseConstantValues.OrdersTableName,
                                DatabaseConstantValues.Orders_Poco_Primary_Key_Value,
                                orderAsPoco,
                                orderAsPoco.Id);

                            LogHelper.Info<ProcessBilderlingsPaymentResponseService>("After Update Orders Table");

                        }


                        switch (transactionStatus.ToLower())
                        {
                            case ConstantValues.Bilderlings_Transaction_Payment_Response_Succeeded:
                                return ConstantValues.Bilderlings_Transaction_Payment_Response_Succeeded;
                                break;
                            default:
                                //DO something: OTHER RESULT
                                break;
                        }
                    
                }
               
                
            }

            return ConstantValues.Bilderlings_Transaction_Payment_Response_Failed;
        }
    }
}
