﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CS.Common.Umbraco.Constants;
using CS.Common.Umbraco.Helpers;
using CS.Common.Umbraco.Helpers.UmbracoPackages.Vorto;
using Umbraco.Core;
using Umbraco.Core.Cache;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;
using Our.Umbraco.Vorto.Models;

namespace CS.Common.Umbraco.UrlProviders
{
    /// <summary>
    /// This is the multilingual URL provider for the pages which follow the 
    /// Umbraco URL structure
    /// </summary>
    public class MultilingualUrlProvider : IUrlProvider
    {
        public string GetUrl(UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            // Don't use umbracoContext.PublishedContentRequest.Culture because this code is also called in the backend.
            var currentCultureName = Thread.CurrentThread.CurrentCulture.Name;

            var url = UmbracoContext.Current.Application.ApplicationCache.RequestCache.GetCacheItem<string>(
                "MultilingualUrlProvider-GetUrl-" + id + "-" + currentCultureName,
                () =>
                {
                    var content = umbracoContext.ContentCache.GetById(id);

                    if (content != null)
                    {
                     

                        var allLanguages = LanguageHelpers.GetAllWebsiteLanguages(ApplicationContext.Current).ToList();

                        if (allLanguages.Any())
                        {
                            return MultilingualHelpers.GetMultilingualUrl(umbracoContext, allLanguages, currentCultureName, content, current);
                        }
                    }

                    return null;
                });

            if (mode == UrlProviderMode.Absolute)
            {
                var uriBuilder = new UriBuilder(current.Scheme, current.Host, current.Port, url);
                url = uriBuilder.ToString();
            }

            return url;
        }

        public IEnumerable<string> GetOtherUrls(UmbracoContext umbracoContext, int id, Uri current)
        {
            // Don't use umbracoContext.PublishedContentRequest.Culture because this code is also called in the backend.
            var currentCultureName = Thread.CurrentThread.CurrentCulture.Name;

            return UmbracoContext.Current.Application.ApplicationCache.RequestCache.GetCacheItem<IEnumerable<string>>(
                "MultilingualUrlProvider-GetOtherUrls-" + id + "-" + currentCultureName,
                () =>
                {
                    var content = umbracoContext.ContentCache.GetById(id);

                    if (content != null)
                    {
                        //var domains = ApplicationContext.Current.Services.DomainService.GetAll(true).OrderBy(x => x.Id).ToList();

                        var allLanguages = LanguageHelpers.GetAllWebsiteLanguages(ApplicationContext.Current).ToList();

                        if (allLanguages.Count > 1)
                        {
                            var urls = new List<string>();

                            // Find the domain that's used in the GetUrl method.
                            //var cultureToUse = UmbracoContext.Current.IsFrontEndUmbracoRequest
                            //                 ? allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(currentCulture))
                            //                 : allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(ConstantValues.LanguageDefaultName));

                            ILanguage cultureToUse = null;

                            var isCultureNameValid = allLanguages.SingleOrDefault(x => String.Compare(x.CultureInfo.Name, currentCultureName, StringComparison.OrdinalIgnoreCase) == 0) != null;
                            if (isCultureNameValid)
                            {
                                cultureToUse = allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(currentCultureName));
                            }
                            else
                            {
                                cultureToUse = allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(ConstantValues.LanguageDefaultName));
                            }

                            // Remove the found domain because it's already used.
                            allLanguages.Remove(cultureToUse);

                            if (content.DocumentTypeAlias.InvariantEquals(ConstantValues.Home_Page_Document_Type_Alias) ||
                                content.DocumentTypeAlias.InvariantEquals(ConstantValues.Website_Configuration_Document_Type_Alias))
                            {
                                // Return the domain if we're on the homepage because on that node we've added the domains.
                                urls.AddRange(allLanguages.Select(x => x.CultureInfo.Name.EnsureEndsWith("/").EnsureStartsWith("/")));
                            }
                            else
                            {
                                // Get the other urls for the parent which aren't the main url.
                                List<string> parentUrls = null;
                                if (content.Parent != null)
                                {
                                    parentUrls = umbracoContext.UrlProvider.GetOtherUrls(content.Parent.Id).ToList();
                                }

                                for (int i = 0; i < allLanguages.Count; i++)
                                {
                                    // Get the domain and matching parent url.
                                    var otherDomain = allLanguages[i];
                                    var otherParentUrl = String.Empty;

                                    if (parentUrls != null)
                                    {
                                        otherParentUrl = parentUrls[i];
                                        otherParentUrl = otherParentUrl.EnsureEndsWith("/");
                                    }

                                    // Get the parent url and add the url segment of this culture.
                                    var urlSegment = MultilingualHelpers.GetUrlSegment(
                                        content.GetPropertyValue<VortoValue>(ConstantValues.Common_Content_Url_Title),
                                        content.GetPropertyValue<VortoValue>(ConstantValues.Common_Content_Title),
                                        content.Name,
                                        otherDomain.CultureInfo.Name);

                                    urls.Add(otherParentUrl + urlSegment);
                                }
                            }

                            return urls;
                        }
                    }

                    return null;
                });
        }
    }
}