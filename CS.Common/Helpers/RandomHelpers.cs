﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CS.Common.Helpers
{
    public static class RandomHelpers
    {
        private static System.Random _rnd = null;
        /// <summary>
        /// The random generator
        /// </summary>
        public static System.Random Rnd
        {
            get
            {
                if (_rnd == null)
                {

                    InitialiseRandomNumberGenerator(null);
                }
                return _rnd;
            }
        }
        public static void InitialiseRandomNumberGenerator(int? seed)
        {
            if (seed.HasValue)
                _rnd = new System.Random(seed.Value);
            else
                _rnd = new System.Random();
        }


        public static TEnum GetRandomEnumValue<TEnum>() where TEnum : struct
        {
            var valueList = EnumHelpers.GetListOfEnumValues<TEnum>();
            if (valueList.Count > 0)
                return valueList[GetInt(0, valueList.Count - 1)];
            else
                return default(TEnum);
        }

        public static string GetGuid(bool removeDashes = false)
        {

            string s = null;
            if (removeDashes)
                s = Guid.NewGuid().ToString("N");
            else
                s = Guid.NewGuid().ToString("");
            return s;
        }

        /// <summary>
        /// Generates a website URL (www.xxxxxxx.xxx)
        /// </summary>
        /// <returns></returns>
        public static string GetWebsite()
        {
            string web = "www.";
            web += GetString(5, 30) + ".";
            web += GetString(1, 3);
            return web;
        }

        /// <summary>
        /// Generates an email address
        /// </summary>
        /// <returns></returns>
        public static string GetEmail()
        {
            string email = "";
            email = GetString(5, 10) + "@" + GetString(10, 20) + "." + GetString(1, 3);
            return email;
        }

        /// <summary>
        /// Returns a random date time
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <returns>The date time</returns>
        public static DateTimeOffset GetDateTime(DateTimeOffset from, DateTimeOffset to)
        {
            TimeSpan time = to.Subtract(from);
            double totSecs = (time.TotalSeconds);
            double rndSecs = GetDouble(0, totSecs);
            return from.AddSeconds(rndSecs);
        }

        /// <summary>
        /// Returns a random boolean
        /// </summary>
        /// <returns></returns>
        public static bool GetBool()
        {
            return GetBool(50);
        }
        /// <summary>
        /// Returns a random boolean
        /// </summary>
        /// <param name="PercRequiredForTrue">The percentage required to get a true, eg. 60 signifies 60% chance of true</param>
        /// <returns></returns>
        public static bool GetBool(int PercRequiredForTrue)
        {

            int tmp = GetInt(0, 100);
            return (tmp <= PercRequiredForTrue);
        }
        /// <summary>
        /// Returns a unique number list 
        /// </summary>
        /// <param name="tot">The total numbers to be generated</param>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <returns>The list</returns>
        public static int[] GetUniqueIntegerList(int tot, int from, int to)
        {
            int[] list = new int[tot];
            int num;
            for (int i = 0; i < tot; i++)
            {
                bool ok = false;
                do
                {
                    ok = true;
                    num = GetInt(from, to);
                    for (int j = 0; j < i; j++)
                    {
                        if (list[j] == num)
                        {
                            ok = false;
                            break;
                        }
                    }
                } while (!ok);
                list[i] = num;
            }
            return list;
        }

        /// <summary>
        /// generates a random number, inclusive
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns>The random number</returns>
        public static int GetInt(int from = 0, int to = int.MaxValue)
        {
            if (to == Int32.MaxValue)
                to--;
            if (from <= to)
            {
                return Rnd.Next(from, to + 1);
            }
            else
                return 0;
        }

        /// <summary>
        /// generates a random number, inclusive
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns>The random number</returns>
        public static long GetLong(long from = 0, long to = long.MaxValue)
        {
            if (to < from)
            {
                long tmp = to;
                to = from;
                from = tmp;
            }

            long range = to - from;
            return (long)(Rnd.NextDouble() * range) + from;
        }


        public static string GetRandomIPv4()
        {
            string ip = "";
            for (int i = 0; i < 4; i++)
            {
                if (i > 0) ip += ".";
                ip += GetInt(1, 255).ToString();
            }
            return ip;
        }
        public static uint GetUInt()
        {
            return (uint)GetInt(0, int.MaxValue - 1);
        }
        /// <summary>
        /// generates a random number
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns>The random number</returns>
        public static double GetDouble(double from, double to)
        {
            if (from <= to)
            {
                double val = Rnd.NextDouble();
                double diff = to - from;
                val *= diff;
                val += from;
                return val;

                /*
                double diff = to - from;
                int integral = Convert.ToInt32(Math.Floor(diff));
                double fractal = diff - Math.Floor(diff);

                double num = from, tmp;
                for (int i = 0; i < integral; i++)
                {
                    num += Rnd.NextDouble();
                }

                do
                {
                    tmp = Rnd.NextDouble();
                }
                while (tmp > fractal);
                num += tmp;
                return num;*/
            }
            else
                return 0;
        }
        /// <summary>
        /// Generates a random string that can include numbers
        /// </summary>
        /// <param name="minLen">Minimum length</param>
        /// <param name="maxLen">Max Length</param>
        /// <returns>The string</returns>
        public static string GetAlpha(int minLen, int maxLen)
        {
            int len = GetInt(minLen, maxLen);
            StringBuilder builder = new StringBuilder();
            char ch;
            int i = 0;
            while (i < len)
            {
                ch = Convert.ToChar(Convert.ToInt32(GetInt(48, 122)));
                if ((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
                {
                    builder.Append(ch);
                    i++;
                }
            }
            return builder.ToString();
        }

        /// <summary>
        /// Generates a random string, which only includes alphabetical characters
        /// </summary>
        /// <param name="minLen">Minimum length</param>
        /// <param name="maxLen">Max Length</param>
        /// <returns>The string</returns>
        public static string GetString(int minLen, int maxLen)
        {
            int len = GetInt(minLen, maxLen);
            StringBuilder builder = new StringBuilder();
            char ch;
            int i = 0;
            while (i < len)
            {
                ch = Convert.ToChar(Convert.ToInt32(GetInt(48, 122)));
                if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
                {
                    builder.Append(ch);
                    i++;
                }
            }
            return builder.ToString();
        }
        /// <summary>
        /// Generates a paragraph with rubbish text
        /// </summary>
        /// <param name="minWords">Min words</param>
        /// <param name="maxWords">Max words</param>
        /// <returns>The paragraph</returns>
        public static string GetParagraph(int minWords, int maxWords)
        {
            int len = GetInt(minWords, maxWords);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++)
            {
                string word = GetString(5, 15);
                if (sb.Length > 0)
                    sb.Append(" ");
                sb.Append(word);
            }
            return sb.ToString();
        }
        /// <summary>
        /// Generates a paragraph, from the data given (Space-delimeted)
        /// </summary>
        /// <param name="Text">The text used for data</param>
        /// <param name="minWords">Min words</param>
        /// <param name="maxWords">Max words</param>
        /// <returns>The paragraph</returns>
        public static string GetParagraph(string Text, int minWords, int maxWords)
        {
            string[] list = Text.Split(' ');
            int len = GetInt(minWords, maxWords);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++)
            {
                int index = GetInt(0, list.Length - 1);
                string word = list[index];
                if (sb.Length > 0)
                    sb.Append(" ");
                sb.Append(word);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Gets a random file from a folder
        /// </summary>
        /// <param name="path">The path of the folder</param>
        /// <param name="canBeEmpty">Whether it can return a null stream, signifying no file chosen</param>
        /// <returns>The file</returns>
        public static FileStream GetFile(string path, bool canBeEmpty)
        {
            int val = 0;
            if (canBeEmpty)
                val = 1;
            return GetFile(path, val);
        }

        /// <summary>
        /// Gets a random file from a folder
        /// </summary>
        /// <param name="path">The path of the folder</param>
        /// <param name="canBeEmpty">An integer value of whether the file can be empty.  Can be seen as the number of files that can be chosen to be empty. 0 means no empty is ever returned.  The higher, the more chance that an empty one is returned</param>
        /// <returns>The file</returns>
        public static FileStream GetFile(string path, int canBeEmpty)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            FileInfo[] files = dir.GetFiles();
            int max = files.Length - 1 + canBeEmpty;
            int index = GetInt(0, max);
            FileStream fs = null;
            if (index < files.Length)
            {
                fs = files[index].OpenRead();
            }
            return fs;
        }

        public static void RandomizeArray<T>(this IList<T> list)
        {
            System.Random rng = new System.Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }


        public static List<T> RandomizeArrayAndClone<T>(List<T> array)
        {



            List<T> result = new List<T>();
            result.AddRange(array);
            RandomizeArray(result);
            return result;

        }

        public static T GetRandomElementFromEnumerable<T>(IEnumerable<T> enumerable)
        {
            T result = default(T);
            if (enumerable != null)
            {
                var list = enumerable.ToList();
                int rndIndex = GetInt(0, list.Count - 1);
                result = list[rndIndex];


            }
            return result;

        }


        public static string GetHexString(int len)
        {
            return GetHexString(len, len);
        }

        public static string GetHexString(int lengthFrom, int lengthTo)
        {
            int len = GetInt(lengthFrom, lengthTo);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++)
            {

                int rnd = GetInt(0, 15);
                switch (rnd)
                {
                    case 15: sb.Append("F"); break;
                    case 14: sb.Append("E"); break;
                    case 13: sb.Append("D"); break;
                    case 12: sb.Append("C"); break;
                    case 11: sb.Append("B"); break;
                    case 10: sb.Append("A"); break;
                    default:
                        sb.Append(rnd.ToString()); break;
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Waits a random amount of time (milleseconds)
        /// </summary>
        /// <param name="fromTimeInMs"></param>
        /// <param name="toTimeInMs"></param>
        public static void WaitARandomAmountOfTime(int fromTimeInMs, int toTimeInMs)
        {

            int rnd = GetInt(fromTimeInMs, toTimeInMs);
            System.Threading.Thread.Sleep(rnd);

        }
    }
}
