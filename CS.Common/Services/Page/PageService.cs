﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using System.Web;
using CS.Common.Services.Application;
using System.Collections.Specialized;
using CS.Common.Helpers;
using System.Web.Routing;

namespace CS.Common.Services.Page
{
    public interface IPageService
    {
        Uri GetCurrentPageUri();
        string GetCurrentPageUrlAbsolutePath();
        HttpContext GetCurrentHttpContext();
        string ConvertAbsoluteUrlToRelativeRootUrl(string absoluteUrl);
        string MapPath(string webPath);
        T GetVariableFromForm<T>(HttpRequestBase request, string key, bool getUnvalidated = false);
        T GetVariableFromQuerystring<T>(HttpRequestBase request, string key, bool getUnvalidated = false);
        T GetVariableFromQuerystring<T>(HttpRequest request, string key, bool getUnvalidated = false);
        string GetVariableFromForm(HttpRequestBase request, string key, bool getUnvalidated = false);
        string GetVariableFromQuerystring(HttpRequestBase request, string key, bool getUnvalidated = false);

        T GetVariableFromRoutingQuerystringOrForm<T>(HttpContextBase context,
            string key,
            bool getUnvalidated = false);

        T GetRouteDataVariable<T>(HttpContextBase context, string key);
        RouteData GetCurrentRouteData(HttpContextBase context);
        string GetRouteDataVariable(HttpContextBase context, string key);
        T GetRouteDataToken<T>(HttpContextBase context, string key);
        object GetRouteDataToken(HttpContextBase context, string key);
        object GetHttpContextItem(object key);
        T GetHttpContextItem<T>(object key);
        void SetHttpContextItem(object key, object value);

        /// <summary>
        /// Either load it from context and if not, call a function to load it and then it will store it to the context item
        /// 
        /// Unit Testing: In order to unit test this, you need to test that the function passed is of type Func[t] and that when calling the method func() it actually
        /// returns the item which should be loaded
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="loadValueFunc"></param>
        /// <returns></returns>
        T GetHttpContextItemAndIfEmptyLoadItAndStoreIt<T>(object key, Func<T> loadValueFunc);

        string GetCurrentPageUrlWithoutQueryStrings();
        string GetServerVariable(string variableName);
    }

    [Service]
    public class PageService : IPageService
    {
        private readonly IApplicationService _applicationService;

        public PageService(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        public Uri GetCurrentPageUri()
        {
            return GetCurrentHttpContext().Request.Url;
        }

        public string GetCurrentPageUrlWithoutQueryStrings()
        {
            return GetCurrentPageUri().GetLeftPart(UriPartial.Path);
        }

        public string GetCurrentPageUrlAbsolutePath()
        {
            return GetCurrentPageUri().AbsolutePath;
        }

        public HttpContext GetCurrentHttpContext()
        {
            return HttpContext.Current;
        }

        public string GetServerVariable(string variableName)
        {
            return GetCurrentHttpContext().Request.ServerVariables[variableName];
        }

        public string ConvertAbsoluteUrlToRelativeRootUrl(string absoluteUrl)
        {
            //Remove the 'http://' or 'https://'
            if (absoluteUrl != null && absoluteUrl.IndexOf("://", System.StringComparison.Ordinal) != -1)
            {
                int startIndex = absoluteUrl.IndexOf("/",
                    absoluteUrl.IndexOf("://", System.StringComparison.Ordinal) + 3,
                    System.StringComparison.Ordinal);
                if (startIndex != -1)
                {
                    absoluteUrl = absoluteUrl.Substring(startIndex);
                }
            }
            return absoluteUrl;
        }

        public string MapPath(string webPath)
        {

            webPath = ConvertAbsoluteUrlToRelativeRootUrl(webPath);
                //this is so that if you pass a full url e.g http://www.visitgozo.com/cikku.aspx, this reduces it to /cikku.aspx

            string path = webPath;
            path = path.Replace("//", "/");
            path = path.Replace("\\\\", "\\");
            var currentHttpContext = GetCurrentHttpContext();
            if (currentHttpContext != null)
            {
                string s = "";
                if (path.Contains("/"))
                {

                    try
                    {
                        s = currentHttpContext.Server.MapPath(path);
                    }
                    catch (Exception)
                    {
                        if (path.StartsWith("/"))
                        {
                            string basePath = currentHttpContext.Server.MapPath("/");
                            s = basePath + path.Substring(1);
                            s = s.Replace("/", "\\");
                        }
                        else
                            throw;
                    }
                }

                else
                {
                    s = currentHttpContext.Server.MapPath(path);
                    //s = path;
                }
                return s;
            }
            else
            {

                string s = null;
                if (path.StartsWith("~")) path = path.Substring(1);
                if (path.StartsWith("/")) path = path.Substring(1);
                string rootPath = _applicationService.GetApplicationRootLocalFolder();
                if (rootPath.EndsWith("\\"))
                    rootPath = rootPath.Substring(0, rootPath.Length - 1);
                while (path.StartsWith("../"))
                {
                    int lastIndexOfSlash = rootPath.LastIndexOf("\\");
                    if (lastIndexOfSlash >= 0)
                    {
                        rootPath = rootPath.Substring(0, lastIndexOfSlash);

                    }
                    else
                    {
                        throw new InvalidOperationException("Not enough folders to traverse path");
                    }
                    path = path.Substring(3);
                }

                s = rootPath + "\\";

                //if (!s.EndsWith("\\"))
                //s += "\\";
                path = path.Replace("/", "\\");
                if (path.StartsWith("\\"))
                    path = path.Remove(0, 1);
                s += path;


                return s;
            }
        }

        private NameValueCollection getRequestForm(HttpRequestBase request, bool getUnvalidated = false)
        {
            if (request != null)
            {


                return getUnvalidated ? request.Unvalidated.Form : request.Form;
            }
            else
            {
                return null;
            }

        }

        public T GetVariableFromForm<T>(HttpRequestBase request, string key, bool getUnvalidated = false)
        {
            var form = getRequestForm(request, getUnvalidated);
            if (form != null)
            {
                string value = form[key];

                return ConversionHelpers.ConvertStringToBasicDataType<T>(value);
            }
            else
            {
                return default(T);
            }
        }

        private NameValueCollection getRequestQueryString(HttpRequestBase request, bool getUnvalidated = false)
        {
            if (request != null)
            {
                return getUnvalidated ? request.Unvalidated.QueryString : request.QueryString;
            }
            else
            {
                return null;
            }
        }

        public T GetVariableFromQuerystring<T>(HttpRequestBase request, string key, bool getUnvalidated = false)
        {
            var qs = getRequestQueryString(request, getUnvalidated);
            if (qs != null)
            {
                return ConversionHelpers.ConvertStringToBasicDataType<T>(qs[key]);
            }
            else
            {
                return default(T);
            }
        }
        public T GetVariableFromQuerystring<T>(HttpRequest request, string key, bool getUnvalidated = false)
        {
            return GetVariableFromQuerystring<T>(new HttpRequestWrapper(request), key, getUnvalidated);
        }
        public string GetVariableFromForm(HttpRequestBase request, string key, bool getUnvalidated = false)
        {
            return GetVariableFromForm<string>(request, key, getUnvalidated);
        }

        public string GetVariableFromQuerystring(HttpRequestBase request, string key, bool getUnvalidated = false)
        {
            return GetVariableFromQuerystring<string>(request, key, getUnvalidated);
        }

        public T GetVariableFromRoutingQuerystringOrForm<T>(HttpContextBase context,
            string key,
            bool getUnvalidated = false)
        {

            object sValue = GetVariableFromQuerystring(context.Request, key, getUnvalidated: getUnvalidated);
            if (sValue == null)
            {
                sValue = GetVariableFromForm(context.Request, key, getUnvalidated: getUnvalidated);
            }

            if (sValue == null)
            {
                sValue = GetRouteDataVariable<string>(context, key);
            }
            if (sValue == null)
            {
                sValue = GetRouteDataToken(context, key);
            }

            return ConversionHelpers.ConvertObjectToBasicDataType<T>(sValue);
        }

        public T GetRouteDataVariable<T>(HttpContextBase context, string key)
        {
            string s = GetRouteDataVariable(context, key);
            return ConversionHelpers.ConvertStringToBasicDataType<T>(s);
        }

        public RouteData GetCurrentRouteData(HttpContextBase context)
        {
            try
            {
                if (context != null)
                {
                    RouteData routeData = RouteTable.Routes.GetRouteData(context);

                    return routeData;
                }
                else
                {
                    return null;
                }
            }
            catch (HttpException ex)
            {
                return null;
            }
        }

        public string GetRouteDataVariable(HttpContextBase context, string key)
        {
            string var = null;

            RouteData routeData = GetCurrentRouteData(context);
            if (routeData != null)
            {
                object routeValue;

                if (routeData.Values.TryGetValue(key, out routeValue))
                {
                    var = (string) routeValue;
                }
            }
            return var;
        }

        public T GetRouteDataToken<T>(HttpContextBase context, string key)
        {
            var value = GetRouteDataToken(context, key);
            return ConversionHelpers.ConvertObjectToBasicDataType<T>(value);
        }

        public object GetRouteDataToken(HttpContextBase context, string key)
        {
            object var = null;



            if (context != null)
            {
                try
                {
                    RouteData routeData = RouteTable.Routes.GetRouteData(context);
                    if (routeData != null)
                    {
                        if (routeData.DataTokens.ContainsKey(key))
                        {
                            var = routeData.DataTokens[key];
                        }

                    }
                }
                catch (HttpException ex)
                {
                }

            }

            return var;
        }

        public object GetHttpContextItem(object key)
        {
            var currentHttpContext = GetCurrentHttpContext();
            if (currentHttpContext.Items.Contains(key))
            {
                return currentHttpContext.Items[key];
            }
            else
            {
                return null;
            }
        }

        public T GetHttpContextItem<T>(object key)
        {
            var item = GetHttpContextItem(key);
            return (T)(item);
        }

        public void SetHttpContextItem(object key, object value)
        {
            var currentHttpContext = GetCurrentHttpContext();
            currentHttpContext.Items[key] = value;
        }
        /// <summary>
        /// Either load it from context and if not, call a function to load it and then it will store it to the context item
        /// 
        /// Unit Testing: In order to unit test this, you need to test that the function passed is of type Func[t] and that when calling the method func() it actually
        /// returns the item which should be loaded
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="loadValueFunc"></param>
        /// <returns></returns>
        public T GetHttpContextItemAndIfEmptyLoadItAndStoreIt<T>(object key, Func<T> loadValueFunc)
        {
            var item = GetHttpContextItem(key);
            if (item == null)
            {
                item = loadValueFunc();
                SetHttpContextItem(key, item);
            }
            return (T)(item);
        }
    }
}
