﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CSTemplate.Code.Constants;
using Sitemapify.Models;
using Sitemapify.Providers.Impl;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Security;

namespace CSTemplate.Code.SitemapXml
{
    public class GenerateXmlSitemapNodes : AbstractSitemapContentProvider<IPublishedContent>
    {

        public IEnumerable<IPublishedContent> GetAllSitemapEntries()
        {
            return GetSitemapEntries();
        }
        public SitemapUrl CreateSitemapUrlFromPublishedContent(IPublishedContent entry, Uri baseUri)
        {
            return CreateSitemapUrl(entry, baseUri);
        }
        protected override SitemapUrl CreateSitemapUrl(IPublishedContent entry, Uri baseUri)
        {
            var uri =  $"{baseUri}{entry.Url()}";
            
            var absoluteUri = uri.TrimEnd("/");
            return SitemapUrl.Create(absoluteUri, entry.UpdateDate);
        }

        protected override IEnumerable<IPublishedContent> GetSitemapEntries()
        {
            var listOfNodesToReturn = new List<IPublishedContent>();
            var ctx = GetUmbracoContext();
            if (ctx != null)
            {
                var root = FromContent(ctx);
                if (root.ItemType == PublishedItemType.Content)
                {
                    listOfNodesToReturn.Add(root);
                    listOfNodesToReturn.AddRange(root
                        .DescendantSitemapNodes(ConstantValues.ExludeFromXmlSitemapAliasName,
                        ConstantValues.ExcludeAllChildrenFromXMLSitemapAliasName)
                        .Where(node => node.ItemType == PublishedItemType.Content));

                    return listOfNodesToReturn.AsEnumerable();
                }
            }
            return Enumerable.Empty<IPublishedContent>();
        }
        protected virtual IPublishedContent FromContent(UmbracoContext context)
        {
            return context.ContentCache.GetByRoute("/");
        }
        protected UmbracoContext GetUmbracoContext()
        {
            var httpContextWrapper = new HttpContextWrapper(HttpContext.Current);
            return UmbracoContext.EnsureContext(httpContextWrapper, ApplicationContext.Current, new WebSecurity(httpContextWrapper, ApplicationContext.Current));
        }
    }
}