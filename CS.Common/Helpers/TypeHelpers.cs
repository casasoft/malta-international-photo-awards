﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;

namespace CS.Common.Helpers
{
    public static class TypeHelpers
    {
        public static object GetDefaultValue(Type t)
        {
            if (t.IsValueType)
            {
                return Activator.CreateInstance(t);
            }
            else
            {
                return null;
            }
        }
        public static bool IsTypeString(Type t)
        {
            return t == typeof(string) || t == typeof(Guid);
        }
        public static bool IsTypeEnum(Type t)
        {
            if (IsTypeNullable(t))
            {
                t = GetUnderlyingType(t);
            }
            return t.IsEnum;
        }

        public static bool IsEnumDefined(Type t, object value)
        {
            return t.IsEnumDefined(value);
        }
        public static bool IsTypeNumericWholeNumbers(Type t)
        {
            t = TypeHelpers.GetUnderlyingType(t);
            return
                  t == typeof(sbyte)
                 || t == typeof(byte)
                 || t == typeof(short)
                 || t == typeof(ushort)
                 || t == typeof(int)
                 || t == typeof(uint)
                 || t == typeof(long)
                 || t == typeof(ulong)
                 || t == typeof(Int16)
                 || t == typeof(Int32)
                 || t == typeof(Int64);
        }
        public static bool IsTypeNumericWithDecimal(Type t)
        {
            t = TypeHelpers.GetUnderlyingType(t);
            return
                  t == typeof(double)
                 || t == typeof(decimal)
                 || t == typeof(float)
                 || t == typeof(Decimal);
        }

        public static bool IsTypeNumeric(Type t)
        {
            t = TypeHelpers.GetUnderlyingType(t);
            return
                IsTypeNumericWholeNumbers(t) || IsTypeNumericWithDecimal(t);
        }

        /// <summary>
        /// Retruns whether the type is a basic list of say string, int, etc (basic data type)
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static bool IsTypeBasicEnumerableList(Type t)
        {
            var isList = IsTypeEnumerableList(t);
            if (isList)
            {
                var listItemType = GetGenericArgumentType(t);
                if (IsTypeBasicDataType(listItemType))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns whether the type is a list (IEnumerable).  However, string is excluded. 
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static bool IsObjectAnEnumerableList(object o)
        {
            bool isList = false;
            if (o != null)
            {
                isList = IsTypeEnumerableList(o.GetType());
            }
            return isList;
        }

        /// <summary>
        /// Returns whether the type is a list (IEnumerable).  However, string is excluded. 
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static bool IsTypeEnumerableList(Type t)
        {
            t = TypeHelpers.GetUnderlyingType(t);
            return
                 t != typeof(string) && 
                (t == typeof (IEnumerable<>) || 
                t == typeof (IEnumerable) ||
               ReflectionHelpers.CheckIfTypeExtendsFrom(t, typeof (IEnumerable)));
        }

        public static bool IsTypeBoolean(Type t)
        {
            t = TypeHelpers.GetUnderlyingType(t);
            return
                t == typeof(bool)
              || t == typeof(Boolean);
        }
        
        public static bool IsTypeDate(Type t)
        {
            t = TypeHelpers.GetUnderlyingType(t);
            return
                t == typeof(DateTime)
              || t == typeof(DateTimeOffset);
        }

        public static bool IsTypeNullable(Type type)
        {
            return type != null && type.IsGenericType && type.GetGenericTypeDefinition() == typeof(System.Nullable<>);
        }

        public static Type GetGenericArgumentType(Type t)
        {
            var genericArgs = t.GetGenericArguments();
            if (genericArgs.Length > 0)
            {
                return genericArgs[0];
            }
            else
            {
                return null;
            }
        }
        public static Type GetUnderlyingType(Type t)
        {
            Type underlyingType = t;
            if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(System.Nullable<>))
            {
                underlyingType = GetGenericArgumentType(t);
            }
            return underlyingType;
        }
        public static bool CompareTypeEvenWithGenericTypes<T>(T item, Type typeToCompareWith)
        {
            return CompareTypeEvenWithGenericTypes(item.GetType(), typeToCompareWith);
        }
        public static bool CompareTypeEvenWithGenericTypes(Type t, Type typeToCompareWith)
        {
            return t == typeToCompareWith || ReflectionHelpers.CheckIfTypeIsSubclassOfGenericType(t, typeToCompareWith);
        }

        public static bool IsTypeHttpPostedFile(Type t)
        {
            return t == typeof(HttpPostedFile) || t == typeof(HttpPostedFileBase);
        }

        public static bool IsTypeBasicDataType(Type t)
        {
            t = GetUnderlyingType(t);

            return
                t == typeof(byte) ||
                t == typeof(Enum) ||
                ReflectionHelpers.CheckIfTypeIsEnum(t, true) ||
                t == typeof(sbyte) ||
                t == typeof(short) ||
                t == typeof(ushort) ||
                t == typeof(int) ||
                t == typeof(uint) ||
                t == typeof(long) ||
                t == typeof(ulong) ||
                t == typeof(float) ||
                t == typeof(double) ||
                t == typeof(decimal) ||
                t == typeof(char) ||
                t == typeof(string) ||
                t == typeof(bool) ||
                t == typeof(DateTime) ||
                t == typeof(DateTimeOffset);
        }
        public static bool IsObjectBasicDataType(object o)
        {
            Type t = o.GetType();
            return IsTypeBasicDataType(t);
        }

        public static bool IsTypeClass(Type type)
        {
            return type != typeof(object) && !IsTypeBasicDataType(type);
        }
    }
}
