﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Models.Shared.RequestFeedbackForSubmissionForm;
using CSTemplate.Code.Models.Shared.SubmitFeedbackForm;
using CSTemplate.Code.Models.SubmissionFullPage;
using CSTemplate.Code.Services.Awards;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.SubmissionPhotos;
using CSTemplate.Code.Services.Users;
using CSTemplate.Code.Services.UserSubmissions;
using Umbraco.Core.Models;
using Umbraco.Web;
using databaseConstants = CSTemplate.Code.Constants.DatabaseConstantValues;
using dictionaryValues = CSTemplate.Code.Constants.Enums.DictionaryKey;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.OrderItems;
using CSTemplate.Code.Services.Orders;

namespace CSTemplate.Code.Services.Submissions
{

    public interface IGetSubmissionDetailsService
    {
        SubmissionFullPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent publishedContent, int submissionId, int userId);
    }

    [Service]
    public class GetSubmissionDetailsService : IGetSubmissionDetailsService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetPagerModelService _getPagerModelService;
        private readonly IGetSubmissionPhotosService _getSubmissionPhotosService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetUserSubmissionsServices _getUserSubmissionsServices;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;
        private readonly IMemberServiceWrapper _memberServiceWrapper;
        private readonly IGetIsAwardEditionCompletedFilterByCategoryIdService _getIsAwardEditionCompletedFilterByCategoryIdService;
        private readonly IGetOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService _getOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService;
        private readonly IGetOrderDetailsFilterByOrderId _getOrderDetailsFilterByOrderId;

        public GetSubmissionDetailsService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IGetPagerModelService getPagerModelService,
            IGetSubmissionPhotosService getSubmissionPhotos,
            IUmbracoHelperService umbracoHelperService,
            IGetUserSubmissionsServices getUserSubmissionsServices,
            IGetUserFilterByUserIdService getUserFilterByUserIdService,
            IMemberServiceWrapper memberServiceWrapper,
            IGetIsAwardEditionCompletedFilterByCategoryIdService getIsAwardEditionCompletedFilterByCategoryIdService,
            IGetOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService getOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService,
            IGetOrderDetailsFilterByOrderId getOrderDetailsFilterByOrderId
        )
        {
            _getOrderDetailsFilterByOrderId = getOrderDetailsFilterByOrderId;
            _getOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService = getOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService;
            _getIsAwardEditionCompletedFilterByCategoryIdService = getIsAwardEditionCompletedFilterByCategoryIdService;
            _memberServiceWrapper = memberServiceWrapper;
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _getUserSubmissionsServices = getUserSubmissionsServices;
            _umbracoHelperService = umbracoHelperService;
            _getSubmissionPhotosService = getSubmissionPhotos;
            _getPagerModelService = getPagerModelService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public SubmissionFullPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent publishedContent, int submissionId, int userId)
        {
            var submissionFullPage = new SubmissionFullPageModel(publishedContent);
            if (submissionId <= 0 || userId<=0) return submissionFullPage;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();




            var submissionsTableName = databaseConstants.SubmissionsTableName;
            var submissionAlias = databaseConstants.aliasForSubmissionsPocoTableNameAlias;
            var submissionUserIdField = databaseConstants.Submissions_User_Id_Field_Name;
            var submissionPrimaryKeyField = databaseConstants.Submissions_Poco_Primary_Key_Value;
            var submissionDateField = databaseConstants.Submissions_Date_Field_Name;
            var submissionLocationField = databaseConstants.Submissions_Location_Field_Name;
            var submissionDescriptionField = databaseConstants.Submissions_Description_Field_Name;
            var submissionFeedbackDescriptionField = databaseConstants.Submissions_Feedback_Description_Field_Name;
            var submissionFeedbackRequestField = databaseConstants.Submissions_Feedback_Request_Field_Name;
            var submissionTitleField = databaseConstants.Submissions_Title_Field_Name;
            var submissionCategoryIdField = databaseConstants.Submissions_Category_Id_Field_Name;
            var submissionStatusEnumIdField = databaseConstants.Submissions_Status_Enum_Id_Field_Name;

            var sqlQueryResult = umbracoDb.Query<SubmissionPoco>($@"
                SELECT 
                    {submissionUserIdField},
                    {submissionDateField},
                    {submissionLocationField},
                    {submissionDescriptionField},
                    {submissionFeedbackDescriptionField},
                    {submissionFeedbackRequestField},
                    {submissionTitleField},
                    {submissionCategoryIdField}
                        FROM {submissionsTableName}
                                WHERE {submissionAlias}.{submissionPrimaryKeyField}={submissionId}
                                   AND ({submissionAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 

            ").FirstOrDefault();

            if (sqlQueryResult == null) return submissionFullPage;
            int submissionUserId = sqlQueryResult.UserId;
            bool isphotoOwner = userId == submissionUserId ? true : false;

            var photographerInfos = _getUserFilterByUserIdService.Get(umbracoHelper, submissionUserId);
            var photographerFullName = photographerInfos.Name + " " + photographerInfos.Surname;

            submissionFullPage.SubmissionDate = sqlQueryResult.Date;
            submissionFullPage.SubmissionLocation = sqlQueryResult.Location;
            submissionFullPage.SubmissionDescription = sqlQueryResult.Description;
            submissionFullPage.SubmissionTitle = sqlQueryResult.Title;
            submissionFullPage.AuthorOfSubmission = photographerFullName;


            var currentMemberRoles = _memberServiceWrapper.GetAllRoles(umbracoHelper.UmbracoContext, userId);
            var isCurrentMemberJury = false;
            if (currentMemberRoles.Contains(ConstantValues.Member_Group_Jury_Name))
            {
                isCurrentMemberJury = true;
            }

            var feedbackSubmitted = (sqlQueryResult.FeedbackRequest == true && !string.IsNullOrEmpty(sqlQueryResult.FeedbackDescription));
            var hasBeenSubmittedForFeedback = sqlQueryResult.FeedbackRequest;
            var feedbackAlreadyRecieved = !string.IsNullOrEmpty(sqlQueryResult.FeedbackDescription);
            submissionFullPage.IsJury = isCurrentMemberJury;
            submissionFullPage.IsOwnerOfSubmission = isphotoOwner;
            submissionFullPage.HasBeenSubmittedForFeedback = hasBeenSubmittedForFeedback;
            submissionFullPage.JurySubmittedFeedback = feedbackSubmitted;
            submissionFullPage.FeedbackAlreadyReceived = feedbackAlreadyRecieved;
            submissionFullPage.AwardEditionCompleted = _getIsAwardEditionCompletedFilterByCategoryIdService.Get(umbracoHelper, sqlQueryResult.CategoryId);

            //request for feedback from photographer
            var requestFeedbackTitleTextFromDictionary = dictionaryValues.Request_Feedback_Title_Text;
            var requestPhotoFeedbackDescriptionFromDictionary = dictionaryValues.Request_Photo_Feedback_Description;
            var requestFeedbackButtonTextFromDictionary = dictionaryValues.Request_Feedback_Button_Text;
            submissionFullPage.RequestFeedbackForSubmissionFormModel = new _RequestFeedbackForSubmissionFormModel()
            {
                RequestFeedbackTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, requestFeedbackTitleTextFromDictionary),
                RequestPhotoFeedbackDescription = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, requestPhotoFeedbackDescriptionFromDictionary),
                RequestFeedbackButtonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, requestFeedbackButtonTextFromDictionary),
                SubmissionID = submissionId.ToString(),
                HasBeenSubmittedForFeedback = hasBeenSubmittedForFeedback


            };
            var paymentForFeedbackStatus = Convert.ToBoolean(Enums.PaymentForFeedback.Yes);

            var submissionOrderItemForFeedbackPaymentStatus = _getOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService.Get(submissionId, paymentForFeedbackStatus);
            submissionFullPage.RequestFeedbackSectionVisible = true;

            if (submissionOrderItemForFeedbackPaymentStatus != null)
            {
                var orderDetail = _getOrderDetailsFilterByOrderId.Get(submissionOrderItemForFeedbackPaymentStatus.OrderId);
                var orderPaymentStatus = (Enums.PaymentStatus)orderDetail.PaymentStatusEnumId;
                //submissionFullPage.PendingForFeedbackPayment = orderPaymentStatus != Enums.PaymentStatus.OK ? true : false;
                //submissionFullPage.SubmissionFeedbackRequestOrderId = orderDetail.OrderId;
                submissionFullPage.RequestFeedbackForSubmissionFormModel.PendingForFeedbackPayment = (orderPaymentStatus != Enums.PaymentStatus.OK ? true : false);
                submissionFullPage.RequestFeedbackForSubmissionFormModel.SubmissionFeedbackRequestOrderId = orderDetail.OrderId;
                submissionFullPage.RequestFeedbackSectionVisible = (submissionFullPage.RequestFeedbackForSubmissionFormModel.PendingForFeedbackPayment && (!hasBeenSubmittedForFeedback || !feedbackAlreadyRecieved));
            }


            //submissionFullPage.IsJury = true;
            //submissionFullPage.IsOwnerOfSubmission = true;
            //submissionFullPage.HasBeenSubmittedForFeedback = sqlQueryResult.FeedbackRequest;
            //submissionFullPage.JurySubmittedFeedback = true;
            //submissionFullPage.FeedbackAlreadyReceived = false;
            //submissionFullPage.AwardEditionCompleted = true;

            //submit feedback by Jury
            var submitFeedbackTitleTextFromDictionary = dictionaryValues.Submit_Feedback_Title_Text;
            var submitFeedbackSubtitleTextFromDictionary = dictionaryValues.Submit_Feedback_Subtitle_Text;
            var submitFeedbackButtonTextFromDictionary = dictionaryValues.Submit_Feedback_Button_Text;
            submissionFullPage.SubmitFeedbackFormModel = new _SubmitFeedbackFormModel()
            {
                SubmitFeedbackTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, submitFeedbackTitleTextFromDictionary),
                SubmitFeedbackSubtitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, submitFeedbackSubtitleTextFromDictionary),
                SubmissionID = submissionId.ToString(),
                SubmitFeedbackButtonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, submitFeedbackButtonTextFromDictionary)
            };



            //feedback received
            var receivedFeedbackTitleTextFromDictionary = dictionaryValues.Received_Feedback_Title_Text;
            submissionFullPage.ReceivedFeedbackTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, receivedFeedbackTitleTextFromDictionary);
            submissionFullPage.ReceivedFeedbackContent = sqlQueryResult.FeedbackDescription;

            //Get the Dictionary, then use String.Format(dictionaryValue, nameOfAuthor)
            var authorsOtherSubmissionsParagraphFromDictionary = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, dictionaryValues.Authors_Other_Submissions_Paragraph);
            var formattedValue = String.Format(authorsOtherSubmissionsParagraphFromDictionary, submissionFullPage.AuthorOfSubmission);
            submissionFullPage.AuthorsOtherSubmissionsParagraph = formattedValue;

            //SubmissionImages
            submissionFullPage.ListOfImages = new List<_ItemPhotoModel>();
            submissionFullPage.ListingOfAuthorsOtherSubmissions = new _ListingItemsAsMasonryPartialModel();

            var submissionImages= _getSubmissionPhotosService.Get(umbracoHelper, submissionId);
            if (submissionImages!=null && submissionImages.Count>0)
            {
                submissionFullPage.ListOfImages = submissionImages;
            }
            //Other Photos By PhotoOwner (Except current photo)
            var listOfPhotos = _getUserSubmissionsServices.Get(umbracoHelper, submissionUserId, submissionId);
            if (listOfPhotos!=null && listOfPhotos.Count>0)
            {
                int itemTotalAmounts = listOfPhotos.Count;
                //how many submissions to show on the page
                submissionFullPage.ItemsPerPage = Constants.ConstantValues.Photographer_Other_Photos_Submission_Full_Page_Mansory_Item_Count;
                //total number of listing items.
                submissionFullPage.ItemsTotalAmount = itemTotalAmounts;

                var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper, submissionFullPage.ItemsPerPage, submissionFullPage.ItemsTotalAmount);

                submissionFullPage.ListingOfAuthorsOtherSubmissions = new _ListingItemsAsMasonryPartialModel
                {
                    ListOfPhotos = listOfPhotos,
                    Pagination = pagerModel
                };
            }
            

            return submissionFullPage;

        }
    }
}