﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.Loader;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.SubmitStorytelling
{
    public class SubmitStoryTellingPageModel : USNBaseViewModel
    {
        public string ExplanationText { get; set; }
        public string ExplanationStoryTelling { get; set; }
        public string ExplanationFileInput { get; set; }
        public string BrowserSuggestionText { get; set; }
        public _LoaderPageModel LoaderPageModel { get; set; }

        public _SubmitStoryTellingFormModel SubmitStoryTellingForm { get; set; }

        public SubmitStoryTellingPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
