﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.SubmissionPhotos
{

    public interface IGetSubmissionPhotosService
    {
        List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper,int submissionId);
    }

    [Service]
    public class GetSubmissionPhotosService : IGetSubmissionPhotosService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;

        public GetSubmissionPhotosService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService
            )
        {
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, int submissionId)
        {
            List<_ItemPhotoModel> photoList = new List<_ItemPhotoModel>();
            if (submissionId <= 0) return photoList;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var photosTable = DatabaseConstantValues.PhotosTableName;
            var photosAlias = DatabaseConstantValues.aliasForPhotosPocoTableNameAlias;
            var photoSubmissionIdField = DatabaseConstantValues.Photos_Submission_Id_Field_Name;
            var photoUrlField = DatabaseConstantValues.Photos_PhotoUrl_Field_Name;

            var submissionTable = DatabaseConstantValues.SubmissionsTableName;
            var submissionAlias = DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionCategoryField = DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionDateField = DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionDescriptionField = DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionLocationField = DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionTitleField = DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionStatusEnumIdField = DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;

            var submissionIdField = DatabaseConstantValues.Photos_Submission_Id_Field_Name;
            var submissionPhotoList = umbracoDb.Query<PhotoPoco,SubmissionPoco>($@"
                    SELECT 
                        {photosAlias}.{photoUrlField},
	                    {submissionAlias}.{submissionCategoryField},
	                    {submissionAlias}.{submissionDateField},
	                    {submissionAlias}.{submissionLocationField},
	                    {submissionAlias}.{submissionDescriptionField},
                        {submissionAlias}.{submissionTitleField},
                        {submissionAlias}.{submissionPrimaryKeyField}
	                    FROM {submissionTable} 
		                    INNER JOIN {photosTable} ON {submissionAlias}.{submissionPrimaryKeyField} = {photosAlias}.{photoSubmissionIdField}
			                    WHERE {submissionAlias}.{submissionPrimaryKeyField}={submissionId}
                           
            ").ToList();
           
            foreach (var submissionPhoto in submissionPhotoList)
            {
                var awardInformation = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, submissionPhoto.Submission.CategoryId);
                photoList.Add(new _ItemPhotoModel
                {
                    ItemPhotoDate = submissionPhoto.Submission.Date,
                    ItemPhotoImgURL = ConstantValues.Submitted_Photo_Source_Address+submissionPhoto.PhotoUrl,
                    ItemPhotoAward = awardInformation.AwardItem.AwardItemName,
                    ItemPhotoLocation = submissionPhoto.Submission.Location,
                    ItemPhotoDescription = submissionPhoto.Submission.Description,
                    ItemPhotoCategory = awardInformation.AwardCategoryItemName,
                    ItemPhotoTitle = submissionPhoto.Submission.Title,
                    SubmissionId = submissionPhoto.SubmissionId.ToString()
                });

                //Fatih, please don't delete cause I need it to tests.
                //photoList.Add(new _ItemPhotoModel
                //{
                //    ItemPhotoDate = submissionPhoto.Submission.Date,
                //    ItemPhotoImgURL = "/dist/assets/img/2.jpg",
                //    ItemPhotoAward = "Landscape photography Award",
                //    ItemPhotoLocation = submissionPhoto.Submission.Location,
                //    ItemPhotoDescription = submissionPhoto.Submission.Description,
                //    ItemPhotoCategory = "nature",
                //    PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
                //});
                //photoList.Add(new _ItemPhotoModel
                //{
                //    ItemPhotoDate = submissionPhoto.Submission.Date,
                //    ItemPhotoImgURL = "/dist/assets/img/4.jpg",
                //    ItemPhotoAward = "Landscape photography Award",
                //    ItemPhotoLocation = submissionPhoto.Submission.Location,
                //    ItemPhotoDescription = submissionPhoto.Submission.Description,
                //    ItemPhotoCategory = "nature",
                //    PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
                //});
            }
            return photoList;
        }

        
    }
}
