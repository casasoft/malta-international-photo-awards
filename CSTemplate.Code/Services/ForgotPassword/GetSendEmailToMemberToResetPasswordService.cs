﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CS.Common.SendGrid.Wrappers.Services;
using CS.Common.Services;
using CS.Common.Umbraco.Models.Authentication;
using CS.Common.Umbraco.Services.RazorEngine;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Emails;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.ForgotPassword
{

    public interface IGetSendEmailToMemberToResetPasswordService
    {
        void Send(UmbracoHelper umbracoHelper, string userEmailAddress,
            string userFullName,
            string resetPasswordPageUrl);
    }

    [Service]
    public class GetSendEmailToMemberToResetPasswordService : IGetSendEmailToMemberToResetPasswordService
    {
        private readonly ISendEmailService _sendEmailService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IRazorEngineCompileTemplate _razorEngineCompileTemplate;
        private readonly IGetEmailTemplatesPlaceholderForCurrentDomainRootNodeService _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService;

        public GetSendEmailToMemberToResetPasswordService(ISendEmailService sendEmailService,
            IUmbracoHelperService umbracoHelperService,
            IRazorEngineCompileTemplate razorEngineCompileTemplate,
            IGetEmailTemplatesPlaceholderForCurrentDomainRootNodeService
            getEmailTemplatesPlaceholderForCurrentDomainRootNodeService)
        {
            _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService = getEmailTemplatesPlaceholderForCurrentDomainRootNodeService;
            _razorEngineCompileTemplate = razorEngineCompileTemplate;
            _umbracoHelperService = umbracoHelperService;
            _sendEmailService = sendEmailService;
        }

        public void Send(UmbracoHelper umbracoHelper, string userEmailAddress,
            string userFullName,
            string resetPasswordPageUrl)
        {
            //Send user an email to reset password with GUID in it
            var emailMessageModel = new EmailMessageResetPasswordModel()
            {
                Name = userFullName,
                EmailAddress = userEmailAddress,
                ResetPasswordUrl = resetPasswordPageUrl
            };

            var emailSender =
                _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService.Get(umbracoHelper).EmailSenderName;

            var fromMailAddress = new MailAddress(CS.Common.Helpers.SmtpHelpers.SmtpFrom, emailSender);

            var sendGridTemplateID = ConstantValues.Default_SendGrid_Template_ID;

            var emailTemplate = new EmailTemplate(_umbracoHelperService.TypedContent(umbracoHelper, 1532));
            var keyForEmailSubject = $"{emailTemplate.Name}-{"subject"}-{emailTemplate.UpdateDate}";
            var keyForEmailBody = $"{emailTemplate.Name}-{"body"}-{emailTemplate.UpdateDate}";

            var emailSubject = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplate.Subject.ToString(), keyForEmailSubject, emailMessageModel);
            var emailBody = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplate.EmailBody.ToString(), keyForEmailBody, emailMessageModel);

            //Send Email To User
            _sendEmailService.SendEmail(umbracoHelper, emailSubject, emailBody, userEmailAddress, null, null, fromMailAddress);
        }
    }
}
