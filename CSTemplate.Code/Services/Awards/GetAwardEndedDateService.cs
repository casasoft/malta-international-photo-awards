﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.Awards
{

    public interface IGetAwardEndedDateService
    {
        _SubmitAwardCountdownModel Get(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class GetAwardEndedDateService : IGetAwardEndedDateService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetAwardEndedDateService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public _SubmitAwardCountdownModel Get(UmbracoHelper umbracoHelper)
        {
            var submitAwardCountdownModel = new _SubmitAwardCountdownModel();
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;

            
            var awardEditionsNode = websiteConfigurationNode
                .Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionPageData)x)
                .Where(x => DateTime.Now.Date >= x.StartedDate && DateTime.Now <= x.EndedDate)
                .FirstOrDefault();
            if(awardEditionsNode!=null)
            {
                var awardSectionTitle = awardEditionsNode.Title;
                var awardDaysTitle = Enums.DictionaryKey.HomePage_Time_Left_Days_Text;
                var awardHourTitle = Enums.DictionaryKey.HomePage_Time_Left_Hours_Text;
                var awardMinutesTitle = Enums.DictionaryKey.HomePage_Time_Left_Minute_Text;

                submitAwardCountdownModel.SectionTitle = awardSectionTitle;
                submitAwardCountdownModel.CountdownDaysText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, awardDaysTitle);
                submitAwardCountdownModel.CountdownHoursText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, awardHourTitle);
                submitAwardCountdownModel.CountdownMinText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, awardMinutesTitle);
                
                submitAwardCountdownModel.BackgroundImgUrl = awardEditionsNode.BackgroundImage.Url;
                submitAwardCountdownModel.DeadlineDateTime = awardEditionsNode.EndedDate.ToString(CultureInfo.InvariantCulture);
                
            }
            return submitAwardCountdownModel;
        }
    }
}
