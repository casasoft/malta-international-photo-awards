﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.Shared.JuryMember
{
    public class _ListingJuryMembersModel 
    {
        public string SectionTitle { get; set; }
        public string SectionText { get; set; }
        public List<_JuryMemberModel> ListofJudges { get; set; }

    }
 
}
