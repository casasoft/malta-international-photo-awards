﻿using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared.Awards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.Awards
{
    public interface IGetAllAwardEditionsService
    {
        List<_AwardItemModel> Get(UmbracoHelper umbracoHelper);
    }
    [Service]
    public class GetAllAwardEditionsService : IGetAllAwardEditionsService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetAllAwardEditionsService(IUmbracoHelperService umbracoHelperService)
        {
            this._umbracoHelperService = umbracoHelperService;
        }
        public List<_AwardItemModel> Get(UmbracoHelper umbracoHelper)
        {
            var returnOfAwardList = new List<_AwardItemModel>();
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;

            var awardEditionsNode = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionPageData)x).ToList();
            foreach (var awardItem in awardEditionsNode)
            {
                returnOfAwardList.Add(new _AwardItemModel
                {
                    AwardItemId=awardItem.Id,
                    AwardItemName=awardItem.Name,
                    AwardItemEndedDate=awardItem.EndedDate,
                    AwardItemStartedDate=awardItem.StartedDate
                });
            }
            return returnOfAwardList;
        }
    }
}
