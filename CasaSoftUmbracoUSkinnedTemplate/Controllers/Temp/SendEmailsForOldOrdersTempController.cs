﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Services.Orders;
using CSTemplate.Code.Services.Submissions;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Temp
{
    public partial class SendEmailsForOldOrdersTempController : SurfaceController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetAllOrdersService _getAllOrdersService;
        private readonly ISendEmailUponCompletedSubmissionService _sendEmailUponCompletedSubmissionService;

        public SendEmailsForOldOrdersTempController(IUmbracoControllerWrapperService
            umbracoControllerWrapperService,
            IGetAllOrdersService
            getAllOrdersService,
            ISendEmailUponCompletedSubmissionService
            sendEmailUponCompletedSubmissionService)
        {
            _sendEmailUponCompletedSubmissionService = sendEmailUponCompletedSubmissionService;
            _getAllOrdersService = getAllOrdersService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
        }

        /// <summary>
        /// This is one-time temp action that was used to send emals of successfull submission to all orders before we introduced
        /// the send emails upon successfull submission
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult SendEmails()
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            var allOrders = _getAllOrdersService.Get();

            var filteredOrders = allOrders.Where(x => x.UserId != 1567 && x.PayPalTransactionId != null && x.PaymentStatusEnumId == 1).ToList();

            foreach (var order in filteredOrders)
            {
                //_sendEmailUponCompletedSubmissionService.Send(umbracoHelper, order.Id, order.UserId);
            }

            return null;
        }
    }
}