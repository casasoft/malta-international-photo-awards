﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using System.Web.Mvc;
using CS.Common.Helpers;

namespace CS.Common.Services.ModelBinding
{
    public interface IModelBindingService
    {
        TModel UpdateModel<TModel>(Controller controller, TModel model, string prefix, string[] includeProperties) where TModel : class;
    }

    [Service]
    public class ModelBindingService : IModelBindingService
    {
        public TModel UpdateModel<TModel>(Controller controller, TModel model, string prefix, string[] includeProperties) where TModel : class
        {
            ReflectionHelpers.InvokeNonPublicGenericMethodOnObject<TModel>(controller, "TryUpdateModel", model, prefix, includeProperties);
            return model;
        }


    }
}
