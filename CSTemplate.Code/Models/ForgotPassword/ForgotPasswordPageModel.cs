﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.ForgotPassword
{
    public class ForgotPasswordPageModel : USNBaseViewModel
    {
        public _ForgotPasswordFormModel ForgotPasswordForm { get; set; }

        public ForgotPasswordPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
