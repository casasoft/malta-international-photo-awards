﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.FeedbackEmail
{
    public class FeedbackInformationEmailModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string SubmissionTitle { get; set; }
        public string PhotoFullPageUrl { get; set; }
    }
}
