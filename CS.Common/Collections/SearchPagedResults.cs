﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Collections
{
    public class SearchPagedResults<T>
    {
        public int TotalResults { get; }
        public IEnumerable<T> Results { get; }


        public SearchPagedResults(IEnumerable<T> results = null, int totalResults = 0)
        {
            TotalResults = totalResults;
            Results = results;
        }
    }
}
