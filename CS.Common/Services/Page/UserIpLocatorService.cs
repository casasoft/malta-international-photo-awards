﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using System.Web;
using CS.Common.Helpers;

namespace CS.Common.Services.Page
{
    public interface IUserIpLocatorService
    {
        string GetIPAddress(bool ipv4 = true);
    }

    [Service]
    public class UserIpLocatorService : IUserIpLocatorService
    {
        private readonly IPageService _pageService;

        public UserIpLocatorService(IPageService pageService)
        {
            _pageService = pageService;
        }

        public string GetIPAddress(bool ipv4 = true)
        {

            HttpContext ctx = _pageService.GetCurrentHttpContext();


            string IP = null;
            if (ctx != null && ctx.Request != null)
            {
                IP = ctx.Request.UserHostAddress;
                if (!ipv4)
                {
                    IP = IpAddressHelpers.ConvertIPV4ToIPV6(IP);
                }


            }
            if (ipv4 && IP == "::1")
            {
                IP = "127.0.0.1";
            }
            
            return IP;

        }

    }
}
