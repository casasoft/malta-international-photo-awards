﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.Loader;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.Register
{
    public class RegisterPageModel : USNBaseViewModel
    {
       public _RegisterFormModel RegisterForm { get; set; }
       public _LoaderPageModel LoaderPageModel { get; set; }

        public RegisterPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
