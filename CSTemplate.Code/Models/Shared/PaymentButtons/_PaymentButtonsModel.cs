﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared.PaymentButtons;

namespace CSTemplate.Code.Models.Shared
{
    public class _PaymentButtonsModel
    {
        public string PaypalButtonText { get; set; }
        public string CardButtonText { get; set; }
        public string PaypalLogoImgUrl { get; set; }
        public string VisaLogoImgUrl { get; set; }
        public string MasterCardLogoImgUrl { get; set; }
        public string BilderlingsLogImgUrl { get; set; }

        public string OrderRefId { get; set; }
        public int OrderId { get; set; }
        public string TransactionDescription { get; set; }
        public string ClientEmailAddress { get; set; }
        public decimal SubTotalPrice { get; set; }

        public List<_OrderPhotosModel> ListOfOrderPhotos { get; set; }
        //we need also a list of order items to pass to paypal

        public BilderlingsPaymentModel BilderlingsPaymentModel { get; set; }
         
        public _PaymentButtonsModel()
        {
            PaypalLogoImgUrl = Constants.ConstantValues.PaypalLogoImgUrl;
            VisaLogoImgUrl = Constants.ConstantValues.VisaLogoImgUrl;
            MasterCardLogoImgUrl = Constants.ConstantValues.MasterCardLogoImgUrl;
            BilderlingsLogImgUrl = Constants.ConstantValues.BilderlingsLogImgUrl;
        }

        public string True
        {
            get
            {
                return "true";
            }
        }

        public bool TermsConditions { get; set; }

        public string AwardRulesTermsAndConditionsText { get; set; }
    }
}
