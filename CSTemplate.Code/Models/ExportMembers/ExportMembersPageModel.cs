﻿using CSTemplate.Code.Models.USNModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.ExportMembers
{
    public class ExportMembersPageModel : USNBaseViewModel
    {
        public string ExplanationText { get; set; }
        public string ButtonText { get; set; }
        [Display(Name = "Date From: ")]
        public DateTime? DateFrom { get; set; }
        [Display(Name = "Date To: ")]
        public DateTime? DateTo { get; set; }

        public ExportMembersPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
