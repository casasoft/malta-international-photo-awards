﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Models.Paging;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;
using static CSTemplate.Code.Constants.Enums;
using CSTemplate.Code.Models.Shared.Awards;

namespace CSTemplate.Code.Models.Shortlist_Photos
{
    public class ShortlistPhotosListingPageModel : USNBaseViewModel
    {
        public string ShowingResults { get; set; }
        public int ItemsPerPage { get; set; }
        public int ItemsTotalAmount { get; set; }

        public PagerModel Pagination { get; set; }


        public ShortlistPhotosFormModel ShortlistPhotosFormModel { get; set; }

        public _ShortListFilterFormModel ShortListFilterFormModel { get; set; }
        


        public ShortlistPhotosListingPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
