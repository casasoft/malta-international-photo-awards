﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using tooorangey.DictionaryItemSearch.Api;
using umbraco;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Startup
{
    public class StartupHandler : IApplicationEventHandler
    {
        public class CheckIfItIsADigit : IRouteConstraint
        {

            public bool Match(
                HttpContextBase httpContext,
                Route route,
                string parameterName,
                RouteValueDictionary values,
                RouteDirection routeDirection)
            {
                var isMatch = false;
                Object idVariable = null;
                values.TryGetValue("id", out idVariable);

                return NumberHelpers.IsNumberInteger(idVariable);
            }
        }

        #region IApplicationEventHandler Implementation

        public void OnApplicationInitialized(UmbracoApplicationBase application, ApplicationContext context)
        {
        }

        public void OnApplicationStarting(UmbracoApplicationBase application, ApplicationContext context)
        {
        }

        public void OnApplicationStarted(UmbracoApplicationBase application, ApplicationContext context)
        {

            RouteTable.Routes.MapUmbracoRoute
             (
                 "ApcoPaymentSuccessfullStatusUrl",
                 "payment/paymentsuccessfullstatusUrl",
                 new
                 {
                     controller = "PaymentPageSurface",
                     action = "PaymentSuccessfullStatusUrl"
                 },
                 new UmbracoVirtualNodeByIdRouteHandler(ConstantValues.Default_Payment_Page_Id)
             );

            RouteTable.Routes.MapUmbracoRoute
                (
                    "ApcoPaymentRedirectionUrl",
                    "payment/PaymentRedirectionUrl",
                    new
                    {
                        controller = "PaymentPageSurface",
                        action = "PaymentRedirectionUrl"
                    },
                    new UmbracoVirtualNodeByIdRouteHandler(ConstantValues.Default_Payment_Page_Id)
                );

            RouteTable.Routes.MapUmbracoRoute
               (
                   "ApcoPaymentFailed",
                   "payment/paymentfailedUrl",
                   new
                   {
                       controller = "PaymentPageSurface",
                       action = "PaymentFailed"
                   },
                   new UmbracoVirtualNodeByIdRouteHandler(ConstantValues.Default_Payment_Page_Id)
               );
            RouteTable.Routes.MapUmbracoRoute
            (
                "Photos",
                "photo/{title}-{id}",
      
                new
                {
                    controller = "SubmissionFullPageData",
                    action = "SubmissionFullPage",
                    id = UrlParameter.Optional

                },

            new UmbracoVirtualNodeByIdRouteHandler(ConstantValues.Submission_Page_Id)
            );

            RouteTable.Routes.MapUmbracoRoute
            (
                "Orders",
                "members-area/order-history/order-{id}",

                new
                {
                    controller = "ViewOrderPageData",
                    action = "ViewOrderPage",
                    //id = UrlParameter.Optional,
                    id = @"\d+" //why doesnt work?
                    

                },
                constraints: new
                {
                    checkIfItItADigit = new CheckIfItIsADigit()
                },

                virtualNodeHandler:
                new UmbracoVirtualNodeByIdRouteHandler(ConstantValues.Order_Page_Id)

            );


            var builder = new ContainerBuilder();

            // register all controllers found in this assembly
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // register Umbraco MVC + web API controllers used by the admin site
            builder.RegisterControllers(typeof(UmbracoApplication).Assembly);
            builder.RegisterApiControllers(typeof(UmbracoApplication).Assembly);

            //here, register thrid party packages controllers
                //UIOMatic
                builder.RegisterControllers(typeof(UIOMatic.Web.Controllers.UIOMaticTreeController).Assembly);
                builder.RegisterApiControllers(typeof(UIOMatic.Web.Controllers.FieldApiController).Assembly);

                //tooorangey.uDictionaryItemSearch
                builder.RegisterControllers(typeof(DictionarySearchController).Assembly);
                builder.RegisterApiControllers(typeof(DictionarySearchController).Assembly);

            //// Add types to be resolved
            registerTypes(builder, context);

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        #endregion

        #region Private Helpers

        private static void registerTypes(ContainerBuilder builder, ApplicationContext applicationContext)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                builder.RegisterAssemblyTypes(assembly)
                    .Where(t => t.GetCustomAttributes(typeof(ServiceAttribute), true).Length > 0)
                    .AsImplementedInterfaces();
            }

        }

        #endregion
    }
}