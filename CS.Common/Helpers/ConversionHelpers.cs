﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Helpers
{
    public static class ConversionHelpers
    {

        public static readonly CultureInfo _convertStringToBasicDataTypeCultureToUse;

        /// <summary>
        /// Returns 'Yes' if true, else 'No'
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public static string BoolToText(bool? b)
        {
            string s = "";
            if (b.HasValue)
            {
                if (b.Value)
                    s = "Yes";
                else
                    s = "No";
            }
            return s;
        }

        public static string ConvertDateTimeOffsetToStringWithEnglishCulture(DateTimeOffset dateTimeOffset)
        {
            return dateTimeOffset.ToString(_convertStringToBasicDataTypeCultureToUse);
        }

        public static string ConvertBasicDataTypeToString(object o, bool ifEnumConvertToIntValue = true, bool useDescriptionAttributeIfEnum = false, bool useFormattingIfDateTime = false)
        {
            string s = null;
            if (o != null)
            {
                var v = o;
                Type objectType = ReflectionHelpers.GetDataTypeForNullableTypeIfNullable(v.GetType());

                if (objectType == typeof(double) || objectType == typeof(float) || objectType == typeof(decimal))
                {
                    //2014-07-21 Commented by Mark as per https://casasoft.atlassian.net/browse/JV-423
                    //s = Convert.ToDouble(v).ToString ( "0.0000",_convertStringToBasicDataTypeCultureToUse);
                    s = Convert.ToDouble(v).ToString(_convertStringToBasicDataTypeCultureToUse);
                }
                else if (objectType == typeof(int) ||
                    objectType == typeof(long) ||
                    objectType == typeof(byte) ||
                    objectType == typeof(short))
                {
                    s = Convert.ToInt64(v).ToString(_convertStringToBasicDataTypeCultureToUse);
                }
                else if (v is DateTime && useFormattingIfDateTime)
                {
                    s = DateHelpers.FormatDateTime((DateTime)v, "dd/MM/yyyy");
                }
                else if (v is DateTime)
                {
                    s = DateHelpers.ConvertToUnixTimestamp(((DateTime)v).ToUniversalTime()).ToString();
                }
                else if (v is DateTimeOffset)
                {
                    s =
                        DateHelpers.ConvertToUnixTimestamp(((DateTimeOffset)v))
                            .ToString(CultureInfo.InvariantCulture);
                }
                else if (v is bool)
                {
                    bool b = (bool)v;
                    if (b)
                        s = "1";
                    else
                        s = "0";


                }
                else if (v is IEnumerable && !(v is string))
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var item in ((IEnumerable)v))
                    {
                        if (sb.Length > 0)
                            sb.Append(",");
                        sb.Append(ConvertBasicDataTypeToString(item));
                    }
                    s = sb.ToString();
                }
                else if (v is Enum)
                {
                    if (ifEnumConvertToIntValue)
                        s = ((int)v).ToString();
                    else
                        s = EnumHelpers.ConvertEnumToString((Enum)v,
                            getFromDescriptionAttributeIfAvailable: useDescriptionAttributeIfEnum);
                }
                else
                {
                    s = v.ToString();
                }

            }
            return s;
        }

        public static TDataType ConvertObjectToBasicDataType<TDataType>(object o)
        {
            // string oString = o == null ? null : o.ToString();
            //return ConvertStringToBasicDataType<TDataType>(oString);




            Type t = typeof(TDataType);
            return (TDataType)ConvertObjectToBasicDataType(o, t);
        }
        /// <summary>
        /// Converts an object to its basic data type, IF t is a basic datatype.  Else, leaves the same.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static object ConvertObjectToBasicDataType(object o, Type t, bool throwErrorIfCannotConvert = false)
        {
            object result = null;
            if (TypeHelpers.IsTypeBasicDataType(t))
            {
                var strObject = String.Format(CultureInfo.InvariantCulture, "{0}", o);
                return ConvertStringToBasicDataType(o == null ? null : strObject, t, throwErrorIfCannotConvert);
            }
            else
            {
                return o;
            }
        }

        public static string ConvertObjectToStringIfBasicDataType(object o, Type t)
        {
            string result = null;

            if (TypeHelpers.IsTypeBasicDataType(t))
            {
                result = String.Format(CultureInfo.InvariantCulture, "{0}", o);
            }

            return result;
        }

        public static T ConvertStringToBasicDataType<T>(string s, bool throwErrorIfCannotBeParsed = false)
        {
            return (T)ConvertStringToBasicDataType(s, typeof(T), throwErrorIfCannotBeParsed);

        }

        public static bool? TextToBoolNullable(string s)
        {
            bool? b = null;
            if (s == null) s = "";
            s = s.ToLower();
            switch (s)
            {

                case "1":
                case "y":
                case "yes":
                case "true":
                case "t":
                case "on":
                    b = true;
                    break;
                case "off":
                case "false":
                case "no":
                case "n":
                case "0":
                case "f":
                    b = false;
                    break;

            }
            return b;
        }
        public static object ConvertStringToBasicDataType(string s, Type tType, bool throwErrorIfCannotBeParsed = false)
        {

            object returnValue = null;

            bool isNullable = false;
            Type underlyingType = tType;
            if (tType.IsGenericType && tType.GetGenericTypeDefinition() == typeof(System.Nullable<>))
            {
                underlyingType = tType.GetGenericArguments()[0];
                isNullable = true;
            }

            if (tType == typeof(bool?) || (underlyingType == typeof(bool)))
            {
                bool? b = TextToBoolNullable(s);
                if (isNullable)
                {
                    return b;
                }
                else
                {
                    if (b == null && throwErrorIfCannotBeParsed) throw new InvalidCastException("'" + s + "' cannot be casted into " + tType.Name);
                    return b.GetValueOrDefault();
                }
            }

            else if (underlyingType == typeof(int))
            {

                int val = 0;
                if (int.TryParse(s, NumberStyles.Any, _convertStringToBasicDataTypeCultureToUse, out val))
                {
                    returnValue = val;
                }
                else if (tType == typeof(int))
                {
                    if (throwErrorIfCannotBeParsed) throw new InvalidCastException("'" + s + "' cannot be casted into " + tType.Name);
                    returnValue = ReflectionHelpers.GetDefaultValueOfType(tType);

                }

            }
            else if (underlyingType == typeof(uint))
            {

                uint val = 0;
                if (uint.TryParse(s, NumberStyles.Any, _convertStringToBasicDataTypeCultureToUse, out val))
                {
                    returnValue = val;
                }
                else if (tType == typeof(uint))
                {
                    if (throwErrorIfCannotBeParsed) throw new InvalidCastException("'" + s + "' cannot be casted into " + tType.Name);
                    returnValue = ReflectionHelpers.GetDefaultValueOfType(tType);
                    //throw new InvalidCastException("'" + s + "' cannot be casted into int");
                }

            }


            else if (underlyingType == typeof(double))
            {
                double val = 0;
                if (double.TryParse(s, NumberStyles.Any, _convertStringToBasicDataTypeCultureToUse, out val))
                {
                    returnValue = val;
                }
                else if (tType == typeof(double))
                {
                    if (throwErrorIfCannotBeParsed) throw new InvalidCastException("'" + s + "' cannot be casted into " + tType.Name);
                    returnValue = returnValue = ReflectionHelpers.GetDefaultValueOfType(tType);
                    //throw new InvalidCastException("'" + s + "' cannot be casted into double");
                }

            }

            else if (underlyingType == typeof(decimal))
            {

                decimal val = 0;
                if (decimal.TryParse(s, NumberStyles.Any, _convertStringToBasicDataTypeCultureToUse, out val))
                {
                    returnValue = val;
                }
                else if (tType == typeof(decimal))
                {
                    if (throwErrorIfCannotBeParsed) throw new InvalidCastException("'" + s + "' cannot be casted into " + tType.Name);
                    returnValue = ReflectionHelpers.GetDefaultValueOfType(tType);
                }
            }
            else if (underlyingType == typeof(Guid))
            {
                Guid val = Guid.Empty;
                if (Guid.TryParse(s, out val))
                {
                    returnValue = val;
                }
                else if (tType == typeof(Guid))
                {
                    if (throwErrorIfCannotBeParsed) throw new InvalidCastException("'" + s + "' cannot be casted into " + tType.Name);
                    returnValue = ReflectionHelpers.GetDefaultValueOfType(tType);
                }
            }
            else if (underlyingType == typeof(long))
            {
                long val = 0;
                long? nVal = null;
                if (long.TryParse(s, NumberStyles.Any, _convertStringToBasicDataTypeCultureToUse, out val))
                {
                    nVal = val;

                    if (isNullable)
                    {
                        returnValue = nVal;
                    }
                    else
                    {
                        if (throwErrorIfCannotBeParsed) throw new InvalidCastException("'" + s + "' cannot be casted into " + tType.Name);
                        returnValue = nVal.GetValueOrDefault();
                    }

                }
                else if (tType == typeof(long))
                {

                    returnValue = ReflectionHelpers.GetDefaultValueOfType(tType);
                    //instead of throw error, set default value

                    //throw new InvalidCastException("'" + s + "' cannot be casted into long");
                }


            }
            else if (underlyingType.IsEnum)
            {
                long val = 0;

                object enumVal = null;
                try
                {


                    enumVal = Enum.Parse(underlyingType, s, true);
                }
                catch (Exception)
                {
                    enumVal = null;

                    if (!isNullable)
                    {
                        enumVal = ReflectionHelpers.GetDefaultValueOfType(underlyingType);
                        //throw;
                    }
                }
                if (enumVal == null)
                {
                    if (long.TryParse(s, out val))
                    {
                        enumVal = Enum.Parse(underlyingType, val.ToString());

                    }
                    else if (tType == typeof(long))
                    {
                        if (throwErrorIfCannotBeParsed) throw new InvalidCastException("'" + s + "' cannot be casted into " + tType.Name);

                        enumVal = ReflectionHelpers.GetDefaultValueOfType(underlyingType);
                        //instead of throw error, set default value

                        //throw new InvalidCastException("'" + s + "' cannot be casted into Enumeration <" + underlyingType.Name + ">");
                    }
                }
                returnValue = enumVal;


            }
            else if (underlyingType == typeof(string))
            {
                returnValue = s;
            }
            else if (underlyingType == typeof(DateTime) || underlyingType == typeof(DateTimeOffset))
            {
                DateTimeOffset? d = null;

                var utcTicks = ConvertStringToBasicDataType<long?>(s);
                if (utcTicks.HasValue)
                {
                    d = DateHelpers.ConvertFromUnixTimestamp(utcTicks.Value);
                }
                else
                {
                    DateTimeOffset parsedDate;
                    if (DateTimeOffset.TryParse(s, out parsedDate))
                    {
                        d = parsedDate;
                    }
                }

                if (d.HasValue)
                {
                    returnValue = underlyingType == typeof(DateTime) ? d.Value.ToLocalTime().DateTime : d;
                }
                else
                {
                    if (isNullable)
                    {
                        returnValue = null;
                    }
                    else
                    {
                        if (throwErrorIfCannotBeParsed) throw new InvalidCastException("'" + s + "' cannot be casted into " + tType.Name);
                        returnValue = ReflectionHelpers.GetDefaultValueOfType(underlyingType); ;

                    }

                }
            }
            else
            {
                throw new InvalidOperationException("data type not yet implemented!");
            }
            return returnValue;



        }
    }

}
