﻿angular.module("umbraco").controller("MyGalleryCategoryName",
    function ($scope, $http) {
       
        var url = "backoffice/GalleryCategory/GalleryCategoryApi/GetGalleryCategoryName/" + $scope.property.value;
        $http.get(url).then(function (response) {
            $scope.myCategory = response.data;
           
        });
    });