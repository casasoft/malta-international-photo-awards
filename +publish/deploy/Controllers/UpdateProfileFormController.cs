﻿using CSTemplate.Code.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class UpdateProfileFormController : SurfaceController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IMemberServiceWrapper _memberServiceWrapper;
        private readonly IUmbracoHelperService _umbracoHelperService;

        public UpdateProfileFormController(IUmbracoControllerWrapperService
            umbracoControllerWrapperService,
            IMemberServiceWrapper
            memberServiceWrapper,
            IUmbracoHelperService
            umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
            _memberServiceWrapper = memberServiceWrapper;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
        }

        [HttpPost]
        public virtual ActionResult HandleUpdateProfileFormSubmit(_RegisterFormModel updateProfileFormModel)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            if (!User.Identity.Name.Equals(updateProfileFormModel.InsertedEmail))
            {
                var foundAnotherUser = _memberServiceWrapper.GetByEmail(umbracoHelper.UmbracoContext,
                    updateProfileFormModel.InsertedEmail);
                if (foundAnotherUser != null)
                {
                    ModelState.Clear();
                    ModelState.AddModelError("Model", "Email is not valid since it belongs to another account");

                    return CurrentUmbracoPage();
                }
            }
            else
            {
                var currentMemberToEdit = _memberServiceWrapper.GetByEmail(umbracoHelper.UmbracoContext, User.Identity.Name);

                currentMemberToEdit.SetValue(ConstantValues.MemberAccountFirstName, updateProfileFormModel.Name);
                currentMemberToEdit.SetValue(ConstantValues.MemberAccountSurname, updateProfileFormModel.Surname);
                currentMemberToEdit.Email = updateProfileFormModel.InsertedEmail;
                currentMemberToEdit.SetValue(ConstantValues.MemberAccountPhone, updateProfileFormModel.InsertedPhone);
                currentMemberToEdit.SetValue(ConstantValues.MemberAccountCountry, updateProfileFormModel.InsertedCountry.ToString());
                currentMemberToEdit.SetValue(ConstantValues.MemberAccountNationality, updateProfileFormModel.Nationality);
                currentMemberToEdit.SetValue(ConstantValues.MemberAccountAgreeToReceivePromotionalMaterials, updateProfileFormModel.PromotionalMaterials);

                _memberServiceWrapper.Save(umbracoHelper.UmbracoContext, currentMemberToEdit);
            }

            return RedirectToCurrentUmbracoPage(ConstantValues.Member_Profile_Update_Successful_Query_String + "=" + 
                _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey
                .Dictionary_Member_Profile_Successfully_Update_Text));
        }



    }
}