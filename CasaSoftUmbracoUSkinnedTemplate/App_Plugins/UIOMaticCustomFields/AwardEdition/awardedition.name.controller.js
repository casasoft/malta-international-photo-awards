﻿﻿angular.module("umbraco").controller("AwardEdition",
     function ($scope, $http) {
         var id = $scope.property.value;
         var url = "backoffice/AwardEdition/AwardEditionApi/GetName/"+id;
        $http.get(url).then(function (res) {

            $scope.awardEditionName = res.data;
        });
       
    });