﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Payment.Bilderlings;
using Umbraco.Core.Logging;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Payment
{
    public partial class BilderlingsPaymentController : SurfaceController
    {
        private readonly IProcessBilderlingsPaymentResponseService _processBilderlingsPaymentResponseService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        public BilderlingsPaymentController(IProcessBilderlingsPaymentResponseService 
            processBilderlingsPaymentResponseService,
            IUmbracoControllerWrapperService
            umbracoControllerWrapperService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _processBilderlingsPaymentResponseService = processBilderlingsPaymentResponseService;
        }

        //[HttpPost]
        //public virtual void SendDirectPost(string orderRefId, int orderId)
        //{
        //    var request = (HttpWebRequest)WebRequest.Create("https://bpayprocessing.iamoffice.lv/direct/v1");

        //    var postData = "X-Shop-Name=maltaphotoaward";
        //    postData += "&Content-Type=application/x-www-form-urlencoded";
        //    postData += "&X-Nonce=l4gMIo1OHiVDteJjQMNWr4txb";
        //    postData += "&X-Request-Signature=6076538778a039bff41761ee2de3d6f17aecedfe4e6843a8cd028b253974c45082fcf472a4cbd2ff3913b51c7b40bc0a622e4a7e314ca02c9825cfefbba13ca0";
        //    postData += "&order_id=order-" + orderId;
        //    postData += "&amount=44";
        //    postData += "&currency=EUR";
        //    postData += "&payment_method=FD_SMS_3D_OPTIONAL";
        //    var data = Encoding.ASCII.GetBytes(postData);

        //    request.Method = "POST";
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    request.ContentLength = data.Length;

        //    request.Headers.Add("X-Shop-Name", "maltaphotoaward");
        //    request.Headers.Add("X-Nonce", "l4gMIo1OHiVDteJjQMNWr4txb");
        //    request.Headers.Add("X-Request-Signature", "6076538778a039bff41761ee2de3d6f17aecedfe4e6843a8cd028b253974c45082fcf472a4cbd2ff3913b51c7b40bc0a622e4a7e314ca02c9825cfefbba13ca0");

        //    using (var stream = request.GetRequestStream())
        //    {
        //        stream.Write(data, 0, data.Length);
        //    }

        //    var response = (HttpWebResponse)request.GetResponse();

        //    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        //}

        public virtual ActionResult BilderlingsPaymentNotificationUrlHandler(string status,
            string invoice_ref, string order_id)
        {
            LogHelper.Info<PaymentPageSurfaceController>("Entered on BilderlingsPaymentNotificationUrlHandler");
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            var responseStatus = _processBilderlingsPaymentResponseService.Process(umbracoHelper,
                status, invoice_ref, order_id);

            if (!String.IsNullOrEmpty(responseStatus))
            {
                responseStatus = responseStatus.ToLower();

                if (ConstantValues.Bilderlings_Transaction_Payment_Response_Succeeded.ToLower().Equals(responseStatus))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.OK);  // OK = 200
                }
            }

            //this is because, bilderlings want that always return Status code 200
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        public virtual ActionResult BilderlingsPaymentReturnUrlHandler(string status,
            string invoice_ref, string order_id)
        {
            LogHelper.Info<PaymentPageSurfaceController>("Entered on BilderlingsPaymentReturnUrlHandler");

            //var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            //var responseStatus = _processBilderlingsPaymentResponseService.Process(umbracoHelper,
            //    status, invoice_ref, order_id);

            //if (!String.IsNullOrEmpty(responseStatus))
            //{
            //    responseStatus = responseStatus.ToLower();

            //    if (ConstantValues.Bilderlings_Transaction_Payment_Response_Succeeded.ToLower().Equals(responseStatus))
            //    {
            //        return RedirectToUmbracoPage(ConstantValues.Default_Successful_Page_Id);
            //    }
            //} 

            return RedirectToUmbracoPage(ConstantValues.Order_History_Page_Id);
        }
    }
}