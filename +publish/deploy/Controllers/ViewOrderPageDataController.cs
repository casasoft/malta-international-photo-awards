﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.ActionFilters;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.ViewOrder;
using CSTemplate.Code.Services.ViewOrder;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    [CustomMemberAuthorize(AllowGroup = "Developer")]
    public partial class ViewOrderPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetViewOrderPageService _getViewOrderPageService;
        private readonly IUmbracoHelperService _umbracoHelperService;

        public ViewOrderPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetViewOrderPageService getViewOrderPageService,
            IUmbracoHelperService umbracoHelperService
            )
        {
            _umbracoHelperService = umbracoHelperService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getViewOrderPageService = getViewOrderPageService;
        }

        // GET: ViewOrderPageData
        public virtual ActionResult ViewOrderPage(RenderModel model, string id)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            int orderId = int.Parse(id);
            bool doesExist = orderId > 0 ? true : false;
            var currentMemberId = Members.GetCurrentMemberId();


            if (!doesExist)
            {
                var pageNotFound = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, "pageNotFoundData");
                return new RedirectToUmbracoPageResult(pageNotFound);
            }
            ViewOrderPageModel viewOrderPageModel = _getViewOrderPageService.Get(umbracoHelper, model.Content,orderId,currentMemberId);

            //if the order number doesnt exist in database return the bool false

            //todo: [For: backend | 2018/08/28] Redirect to page not found if the it's not a valid order number (WrittenBy: Radek)        			
            

            return base.Populate(viewOrderPageModel);

        }
    }
}