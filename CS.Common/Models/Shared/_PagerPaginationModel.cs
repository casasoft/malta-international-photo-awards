﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Models.Paging;

namespace CS.Common.Models.Shared
{
    public class _PagerPaginationModel
    {
        public List<PagerItemModel> Pages { get; set; }
    }
}
