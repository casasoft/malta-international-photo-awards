﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared.OrderHistoryItem
{
    public class _OrderHistoryItem
    {
        public DateTime OrderDate { get; set; }
        public string OrderNumber { get; set; }
        public string OrderStatus { get; set; }
        public string OrderDescription { get; set; }
        public decimal OrderTotal { get; set; }
        public string OrderLinkUrl { get; set; }
    }
}
