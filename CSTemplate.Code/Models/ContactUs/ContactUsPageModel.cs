﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.ContactUs
{
    public class ContactUsPageModel : USNBaseViewModel
    {   
        public _ContactUsForm ContactUsForm { get; set; }
        public string LocationMapTitle { get; set; }
        public string EmbeddedMap { get; set; }
        public ContactUsPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
