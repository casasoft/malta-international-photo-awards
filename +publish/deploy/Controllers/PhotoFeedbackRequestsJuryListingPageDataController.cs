﻿using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSTemplate.Code.Models.Photo_Feedback_Requests;
using Umbraco.Web.Models;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Feedbacks;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class PhotoFeedbackRequestsJuryListingPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        private readonly IGetPhotoFeedbackRequestsListingForJuryService _getPhotoFeedbackRequestsListingForJuryService;
        //private readonly IGetPagerModelService _getPagerModelService;
       
        public PhotoFeedbackRequestsJuryListingPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetPhotoFeedbackRequestsListingForJuryService getPhotoFeedbackRequestsService
            //IGetPagerModelService getPagerModelService
            )
        {
            _getPhotoFeedbackRequestsListingForJuryService = getPhotoFeedbackRequestsService;

            //_getPagerModelService = getPagerModelService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
        }

        public virtual ActionResult PhotoFeedbackRequestsJuryListingPage(RenderModel model, string page)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            int pageNumber = 1;
            if (!string.IsNullOrEmpty(Request.QueryString["page"]) && Request.QueryString["page"] != null)
            {
                pageNumber = Convert.ToInt32(Request.QueryString["page"]);
            }
            PhotoFeedbackRequestsJuryListingPageModel photoFeedbackRequestsJuryListingPageModel = _getPhotoFeedbackRequestsListingForJuryService.Get(umbracoHelper, model.Content, pageNumber);

            #region DummyData

            //new PhotoFeedbackRequestsJuryListingPageModel(model.Content);

            //DateTime currentTime = DateTime.Now;
            //var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);


            //int itemTotalAmounts = 89;
            ////how many item to show on the page
            //photoFeedbackRequestsJuryListingPageModel.ItemsPerPage = 10;
            ////total number of listing items.
            //photoFeedbackRequestsJuryListingPageModel.ItemsTotalAmount = itemTotalAmounts;

            //var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper, photoFeedbackRequestsJuryListingPageModel.ItemsPerPage, photoFeedbackRequestsJuryListingPageModel.ItemsTotalAmount);
            //photoFeedbackRequestsJuryListingPageModel.ListingItemsAsMasonryPartial = new _ListingItemsAsMasonryPartialModel()
            //{
            //    ListOfPhotos = new List<_ItemPhotoModel>()
            //    {
            //        #region DummyData

            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoName = "John Smith",
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/1.jpg",
            //        ItemPhotoAward = "Landscape photography Award",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //        SubmissionStatusTitleText = "Status: ",
            //        CurrentSubmissionStatus = "Waiting For Feedback",
            //        SubmissionId = "62",
            //        ItemPhotoTitle = "Test title Malta",

            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoName = "Michael Smith",
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/2.jpg",
            //        ItemPhotoAward = "Landscape photography Award",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test2/",
            //        SubmissionStatusTitleText = "Status: ",
            //        CurrentSubmissionStatus = "Waiting For Feedback",
            //        SubmissionId = "63",
            //        ItemPhotoTitle = "Test title 2 Malta",

            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoName = "Chris Franklin",
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/3.jpg",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //        SubmissionStatusTitleText = "Status: ",
            //        CurrentSubmissionStatus = "Waiting For Feedback",
            //        SubmissionId = "64",
            //        ItemPhotoTitle = "Test title 3 Malta",

            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoName = "Mitch Borg",
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/4.jpg",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //        SubmissionStatusTitleText = "Status: ",
            //        CurrentSubmissionStatus = "Waiting For Feedback",
            //        SubmissionId = "68",
            //        ItemPhotoTitle = "Test title 4 Malta",
            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoName = "Peter Abela",
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/5.jpg",
            //        ItemPhotoAward = "Landscape photography Award",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "war",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //        SubmissionStatusTitleText = "Status: ",
            //        CurrentSubmissionStatus = "Waiting For Feedback",
            //        SubmissionId = "69",
            //        ItemPhotoTitle = "Test title 2 Malta",
            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoName = "Mary Attard",
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/1.jpg",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //        SubmissionStatusTitleText = "Status: ",
            //        CurrentSubmissionStatus = "Waiting For Feedback",
            //        SubmissionId = "62",
            //        ItemPhotoTitle = "Test title 2 Malta",

            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoName = "Kurt Spiteri",
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/2.jpg",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //        SubmissionStatusTitleText = "Status: ",
            //        CurrentSubmissionStatus = "Waiting For Feedback",
            //        SubmissionId = "63",
            //        ItemPhotoTitle = "TestWith sp3c1al $ign$&characters url",
            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoName = "Sara Jane",
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/3.jpg",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //        SubmissionStatusTitleText = "Status: ",
            //        CurrentSubmissionStatus = "Waiting For Feedback",
            //        SubmissionId = "64",
            //        ItemPhotoTitle = "TestWith sp3c1al $ign$&characters url",
            //    },
            //    #endregion
            //    },

            //    Pagination = pagerModel

            //};


            #endregion



            return base.Populate(photoFeedbackRequestsJuryListingPageModel);
        }
    }
}