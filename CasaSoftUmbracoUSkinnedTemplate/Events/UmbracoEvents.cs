﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.Cache;

namespace CasaSoftUmbracoUSkinnedTemplate.Events
{
    public class UmbracoEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {

            // Events.
            PageCacheRefresher.CacheUpdated += this.PageCacheRefresherCacheUpdated;
            ContentService.Saved += this.ContentServiceSaved;

        }

        private void PageCacheRefresherCacheUpdated(PageCacheRefresher sender, Umbraco.Core.Cache.CacheRefresherEventArgs e)
        {
            // After content has been updated clear content finder cache.
            this.ClearCache();
        }

        private void ContentServiceSaved(IContentService sender, SaveEventArgs<IContent> e)
        {
            // The cms should be on a separate server. So in the save event the cache will only be cleared on that server.
            // This will make the preview update to date after saving.
            this.ClearCache();
        }

        private void ClearCache()
        {
            // clear our runtime cache
            CS.Common.Umbraco.Helpers.ContentHelpers.ClearUmbracoCache();
        }




    }
}