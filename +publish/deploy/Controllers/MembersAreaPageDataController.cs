﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSTemplate.Code.Models.MembersArea;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class MembersAreaPageDataController : USNBaseController
    {
        public MembersAreaPageDataController()
        {

        }


        public virtual ActionResult MembersAreaPage(RenderModel model)
        {


            MembersAreaPageModel memebrsAreaPageModel = new MembersAreaPageModel(model.Content);

            return base.Populate(memebrsAreaPageModel);
        }
    }
}