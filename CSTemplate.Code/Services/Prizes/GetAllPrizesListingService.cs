﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.HomePageComponents;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.PublishedContentModels;
using constantValues = CSTemplate.Code.Constants.ConstantValues;
using dictionaryValues = CSTemplate.Code.Constants.Enums.DictionaryKey;
namespace CSTemplate.Code.Services.Prizes
{

    public interface IGetAllPrizesListingService
    {
        _AllListingPrizesModel Get(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class GetAllPrizesListingService : IGetAllPrizesListingService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetListingPrizesButtonComponentService _getListingPrizesButtonComponentService;

        public GetAllPrizesListingService(
            IUmbracoHelperService umbracoHelperService,
            IGetListingPrizesButtonComponentService getListingPrizesButtonComponentService
            )
        {
            _getListingPrizesButtonComponentService = getListingPrizesButtonComponentService;
            _umbracoHelperService = umbracoHelperService;
        }
        public _AllListingPrizesModel Get(UmbracoHelper umbracoHelper)
        {
            var getPrizesList = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, constantValues.Prizes_Listing_Page_Data_Document_Type_Alias)
                .Descendants()
                .Where(x => x.DocumentTypeAlias.Equals(constantValues.Prize_Item_Data_Document_Type_Alias))
                .Select(x => (PrizeItemData)x)
                .ToList();
            
            var prizeItemList = new List<_PrizeItemModel>();
            foreach (var prize in getPrizesList)
            {
                prizeItemList.Add(new _PrizeItemModel
                {
                    PrizeItemImgURL = prize.Photo.Url,
                    PrizeItemText = prize.ShortDescription,
                    PrizeItemTitle = prize.Title,
                    
                });
            }
            var returnOfAllListingPrizesModel = new _AllListingPrizesModel
            {
                ListOfPrizes = prizeItemList
            };

            var prizesSection = _getListingPrizesButtonComponentService.Get(umbracoHelper);
            if (prizesSection == null) return returnOfAllListingPrizesModel;

            returnOfAllListingPrizesModel.SectionTitle = prizesSection.Title;
            returnOfAllListingPrizesModel.SectionText = prizesSection.ShortDescription;

            var button = prizesSection.ButtonLink;
            if (button != null)
            {
                var buttonProperties = button.FirstOrDefault();
                var target = buttonProperties.NewWindow == true ? constantValues.Link_Target_Blank : constantValues.Link_Target_Self;
                returnOfAllListingPrizesModel.ButtonTarget = target;
                returnOfAllListingPrizesModel.ButtonURL = buttonProperties.Link;
                returnOfAllListingPrizesModel.ButtonText = buttonProperties.Caption;
            }
           
           
           
            
            
          
                

            return returnOfAllListingPrizesModel;
        }
    }
}
