﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.OrderSummary;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.PaymentButtons;
using CSTemplate.Code.Services.OrderItems;
using CSTemplate.Code.Services.Orders;
using CSTemplate.Code.Services.Payment.Bilderlings;
using CSTemplate.Code.Services.Users;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.OrderSummary
{
    public interface IGetOrderSummaryPageService
    {
        OrderSummaryPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content, int currentLoggedInUserId);
    }
    [Service]
    public class GetOrderSummaryPageService : IGetOrderSummaryPageService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetOrderTotalCalculateService _getOrderTotalCalculateService;
        private readonly IGetOrderDetailsFilterByOrderId _getOrderDetailsFilterByOrderId;
        private readonly IGetIsAStoryTellingOrderService _getIsAStoryTellingOrderService;
        private readonly IGetOrderItemsFilterByOrderIdOrUserIdService _getOrderItemsFilterByOrderIdOrUserIdService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;
        private readonly IGetBilderlingsPaymentModelService _getBilderlingsPaymentModelService;

        public GetOrderSummaryPageService(
            IUmbracoHelperService umbracoHelperService,
            IGetOrderTotalCalculateService getOrderTotalCalculateService,
            IGetOrderDetailsFilterByOrderId getOrderDetailsFilterByOrderId,
            IGetIsAStoryTellingOrderService getIsAStoryTellingOrderService,
            IGetOrderItemsFilterByOrderIdOrUserIdService getOrderItemsFilterByOrderIdOrUserIdService,
            IGetUserFilterByUserIdService getUserFilterByUserIdService,
            IGetBilderlingsPaymentModelService getBilderlingsPaymentModelService


        )
        {
            _getBilderlingsPaymentModelService = getBilderlingsPaymentModelService;
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _getOrderItemsFilterByOrderIdOrUserIdService = getOrderItemsFilterByOrderIdOrUserIdService;
            _getIsAStoryTellingOrderService = getIsAStoryTellingOrderService;
            _getOrderDetailsFilterByOrderId = getOrderDetailsFilterByOrderId;
            _getOrderTotalCalculateService = getOrderTotalCalculateService;
            _umbracoHelperService = umbracoHelperService;
        }

        public OrderSummaryPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content, int currentLoggedInUserId)
        {
            if (currentLoggedInUserId <= 0) return new OrderSummaryPageModel(content);

           
            var photoTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_Summary_Photo_Title_Table);
            var priceTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_Summary_Pirce_Title_Table);
            var subtotalTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_Summary_Subtotal_Title_Table);
            var paypalbuttonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_Summary_Paypal_Button_Text);
            var buttonUrl = _umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)
                .Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Payment_Page_Document_Type_Alias))
                .FirstOrDefault().Url;
            var cardbuttonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_Summary_Card_Button_Text);

            var awardtermsAndConditionsRawText =
                _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    Enums.DictionaryKey.Dictionary_Form_Award_Rules_And_Terms_And_Conditions_Text);
            var awardRulesUrl = _umbracoHelperService.TypedContent(umbracoHelper,
                CS.Common.Umbraco.Constants.ConstantValues.Awards_Rules_Page_Id).Url;
            var termsAndConditionsUrl = _umbracoHelperService.TypedContent(umbracoHelper,  CS.Common.Umbraco.Constants.ConstantValues.Terms_And_Conditions_Page_Id).Url;

            var awardRulesAndtermsAndConditionsFormatted = String.Format(awardtermsAndConditionsRawText, awardRulesUrl, termsAndConditionsUrl);

            DateTime currentTime = DateTime.Now;
            var orderItems = _getOrderItemsFilterByOrderIdOrUserIdService.Get(umbracoHelper: umbracoHelper, orderId: 0, userId: currentLoggedInUserId);
            var userDetails = _getUserFilterByUserIdService.Get(umbracoHelper, currentLoggedInUserId);
            var orderId = Convert.ToInt32(orderItems.Select(x => x.OrderId).FirstOrDefault());
            var orderDetails = _getOrderDetailsFilterByOrderId.Get(orderId);
            var subTotalPrice = _getOrderTotalCalculateService.Get(orderId);



            OrderSummaryPageModel orderSummaryPageModel = new OrderSummaryPageModel(content)
            {
                
                TablePhotoTitle = photoTitleText,
                TablePriceTitle = priceTitleText,
                TableSubtotalTitle = subtotalTitleText,
                SubtotalPrice = subTotalPrice,
                
                IsAStoryTelling = _getIsAStoryTellingOrderService.Get(orderId),
                
                 		
                OrderRefId = orderDetails.OrderRefId,
                ClientEmailAddress = userDetails.InsertedEmail,
                   			
                TransactionDescription = orderDetails.Description,
                ListOfOrderPhotos = orderItems,

                PaymentButtons = new _PaymentButtonsModel()
                {
                    OrderRefId = orderDetails.OrderRefId,
                    OrderId = orderId,
                    ClientEmailAddress = userDetails.InsertedEmail,
                    TransactionDescription = orderDetails.Description,
                    SubTotalPrice = subTotalPrice,
                    ListOfOrderPhotos = orderItems,
                 
                    CardButtonText = cardbuttonText,
                    AwardRulesTermsAndConditionsText = awardRulesAndtermsAndConditionsFormatted,
                    PaypalButtonText = paypalbuttonText,

                    BilderlingsPaymentModel = _getBilderlingsPaymentModelService.Get(orderDetails.OrderRefId,
                        subTotalPrice, userDetails.InsertedEmail, userDetails.Name, userDetails.Surname, orderItems)
                },

            };
            return orderSummaryPageModel;
        }
    }
}
