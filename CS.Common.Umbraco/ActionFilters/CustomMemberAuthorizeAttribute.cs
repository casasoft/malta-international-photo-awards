﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Constants;
using Umbraco.Web;
using Umbraco.Web.Routing;
using Umbraco.Web.Security;

namespace CS.Common.Umbraco.ActionFilters
{
    public class CustomMemberAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly UmbracoContext _umbracoContext;

        private UmbracoContext GetUmbracoContext()
        {
            return _umbracoContext ?? UmbracoContext.Current;
        }

        /// <summary>
        /// Flag for whether to allow all site visitors or just authenticated members
        /// </summary>
        /// <remarks>
        /// This is the same as applying the [AllowAnonymous] attribute
        /// </remarks>
        [Obsolete("Use [AllowAnonymous] instead")]
        public bool AllowAll { get; set; }

        /// <summary>
        /// Comma delimited list of allowed member types
        /// </summary>
        public string[] AllowType { get; set; }

        /// <summary>
        /// Comma delimited list of allowed member groups
        /// </summary>
        public string AllowGroup { get; set; }

        /// <summary>
        /// Comma delimited list of allowed members
        /// </summary>
        public string AllowMembers { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (String.IsNullOrWhiteSpace(AllowMembers))
                AllowMembers = "";
            if (String.IsNullOrWhiteSpace(AllowGroup))
                AllowGroup = "";
            //if (String.IsNullOrWhiteSpace(AllowType))
            //    AllowType = "";

            var members = new List<int>();
            foreach (var s in AllowMembers.Split(','))
            {
                int id;
                if (int.TryParse(s, out id))
                {
                    members.Add(id);
                }
            }

            var memHelper = new MembershipHelper(GetUmbracoContext());
            var isMemberAuthorized = memHelper.IsMemberAuthorized(AllowAll,
                AllowType,
                AllowGroup.Split(','),
                members);

            return isMemberAuthorized;
        }


        /// <summary>
        /// Override method to do custom logic instead of returning a 401 result
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var lastAccessedUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            var loginPageUrl = GetUmbracoContext().UrlProvider.GetUrl(/*ConstantValues.NotAuthorizedPageId*/ 1626, UrlProviderMode.Absolute);

            var uriBuilder = new UriBuilder(loginPageUrl);
            var queryString = HttpUtility.ParseQueryString(uriBuilder.Query);
            queryString[ConstantValues.LastAccessedUrlQueryStringName] = lastAccessedUrl;
            uriBuilder.Query = queryString.ToString();

            filterContext.Result = new RedirectResult(uriBuilder.ToString());
        }
    }
}
