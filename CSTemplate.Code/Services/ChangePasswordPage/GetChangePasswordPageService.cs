﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.ChangePassword;
using CSTemplate.Code.Models.ChangePasswordForm;
using CSTemplate.Code.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.ChangePasswordPage
{
    
    public interface IGetChangePasswordPageService
    {
        ChangePasswordPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content);
    }

    [Service]
    public class GetChangePasswordPageService : IGetChangePasswordPageService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetChangePasswordPageService(
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }

        public ChangePasswordPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content)
        {
            //var subTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
            //    Enums.DictionaryKey.ChangePassword_Page_Subtitle_Form_Text);
            //var requiredText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
            //    Enums.DictionaryKey.Dictionary_Form_Required_Text);
            //var buttonFormText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
            //    Enums.DictionaryKey.ChangePassword_Page_Button_Form_Text);

            ChangePasswordPageModel changePasswordPageModel = new ChangePasswordPageModel(content)
            {
                ChangePasswordForm = new _ChangePasswordFormModel()
                {
                    #region DummyData

                    Message = "Please enter your current password and new password",
                    RequiredWordText = "Required",
                    ButtonFormText = "Change Password",
                    
                    #endregion
                   
                }
                


        };
            return changePasswordPageModel;
        }

    }
}