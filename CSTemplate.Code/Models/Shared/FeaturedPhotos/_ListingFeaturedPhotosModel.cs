﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using umbraco.presentation.actions;

namespace CSTemplate.Code.Models.Shared
{
    public class _ListingFeaturedPhotosModel
    {
        public string SectionTitle { get; set; }
        public string SummitedPhotosButtontext { get; set; }
        public List<_ItemPhotoModel> ListOfPhotos { get; set; }
    }
}
