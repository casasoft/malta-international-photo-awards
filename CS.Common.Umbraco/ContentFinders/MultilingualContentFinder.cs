﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using CS.Common.Umbraco.Constants;
using CS.Common.Umbraco.Helpers;
using Umbraco.Core;
using Umbraco.Core.Cache;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;
using CS.Common.Umbraco.Services.ContentFinder;
using CS.Common.Helpers;

namespace CS.Common.Umbraco.ContentFinders
{
    public class MultilingualContentFinder : IContentFinder
    {
        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            if (contentRequest == null)
            {
                return false;
            }

            try
            {
                using (
                    ApplicationContext.Current.ProfilingLogger.TraceDuration<MultilingualContentFinder>(
                        "Started TryFindContent",
                        "Completed TryFindContent"))
                {
                    var helper = new UmbracoHelper(UmbracoContext.Current);

                    if (UmbracoContext.Current != null)
                    {
                        //var urls = UmbracoContext.Current.Application.ApplicationCache.RuntimeCache.GetCacheItem<List<Tuple<int, string>>>(
                        //    "MultilingualContentFinder-Urls",
                        //    () =>

                        var multilingualUrlCacheKey = "MultilingualContentFinder-Urls";

                        IDomain currentDomain = null;


                        // If the UmbracoDomain is filled in then it means that either this node, or one of its
                        // ancestors has a Domain Filled so we are using multiple domains for this site.
                        if (contentRequest.UmbracoDomain != null)
                        {
                            currentDomain = contentRequest.UmbracoDomain;
                            multilingualUrlCacheKey = multilingualUrlCacheKey + "-" + currentDomain.Id;
                        }

                        var urlsService = IocHelpers.Container.GetInstance<IContentFinderCachedMultilingualUrlsService>();

                        var urls = urlsService.Get(helper, currentDomain);

                        if (urls.Any())
                        {
                            // Get the current url without querystring.
                            //var url = this.RemoveQueryFromUrl(contentRequest.Uri.ToString()).EnsureEndsWith("/");
                            var url = contentRequest.Uri.GetAbsolutePathDecoded().EnsureEndsWith("/");

                            var parts = url.Split(new[] { '/' }, System.StringSplitOptions.RemoveEmptyEntries);

                            //this part was added so that if url is indicating that user entered the full url without specific language
                            //we add the default lanaguge to parts array so that we still indicating
                            //that it still belongs to the default language
                            if (parts.Length == 0)
                            {
                                parts = new[] { ConstantValues.LanguageDefaultName };
                            }
                            if (url.Replace('/', ' ').Trim().Equals(ConstantValues.LanguageDefaultName))
                            {
                                url = "/";
                            }

                            if (parts.Length != 0) // if in root redirect to default language 
                            {
                                var currentUrlItem = urls.FirstOrDefault(x => url.InvariantEquals(x.Item2));

                                if (currentUrlItem != null)
                                {
                                    var allLanguages = LanguageHelpers.GetAllWebsiteLanguages(ApplicationContext.Current);

                                    var cultureName = parts[0];
                                    var isCultureNameValid = allLanguages.SingleOrDefault(x => String.Compare(x.CultureInfo.Name, cultureName, StringComparison.OrdinalIgnoreCase) == 0) != null;

                                    if (isCultureNameValid)
                                    {
                                        contentRequest.Culture = new CultureInfo(cultureName);

                                        //var route = "/" + (parts.Length > 1 ? String.Join("/", parts, 1, parts.Length - 1) : "");
                                        //contentRequest.PublishedContent = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetByRoute(route, true);

                                        var contentItem = UmbracoContext.Current.ContentCache.GetById(currentUrlItem.Item1);

                                        if (contentItem != null)
                                        {
                                            contentRequest.PublishedContent = contentItem;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error<MultilingualContentFinder>("Error in contenfinder MultilingualContentFinder", ex);
            }

            return contentRequest.PublishedContent != null;
        }

        public string RemoveQueryFromUrl(string url)
        {
            var index = url.IndexOf('?');

            return index > 0 ? url.Substring(0, index) : url;
        }

        private void AddNodes(List<IPublishedContent> allNodes, IPublishedContent rootNode)
        {
            allNodes.Add(rootNode);
            var allNodesUnderRootNode = rootNode.Descendants();
            allNodes.AddRange(allNodesUnderRootNode);
        }
    }
}
