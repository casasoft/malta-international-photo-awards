﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CS.Common.Helpers;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Order;
using CSTemplate.Code.Services.OrderItems;
using CSTemplate.Code.Services.Submissions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Payment
{
    public partial class PayPalResponseController : SurfaceController
    {
        private readonly IGetOrderByOrderRefIdService _getOrderByOrderRefIdService;
        private readonly IUpdateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService _updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService;
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetOrderItemsFilterByOrderIdOrUserIdService _getOrderItemsFilterByOrderIdOrUserIdService;
        private readonly IUmbracoControllerWrapperService _controllerWrapperService;
        private readonly ISendEmailUponCompletedSubmissionService _sendEmailUponCompletedSubmissionService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        public PayPalResponseController(IGetOrderByOrderRefIdService
            getOrderByOrderRefIdService,
            IUpdateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService
            updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService,
            IGetCurrentUmbracoDatabaseService
            getCurrentUmbracoDatabaseService,
            IGetOrderItemsFilterByOrderIdOrUserIdService getOrderItemsFilterByOrderIdOrUserIdService,
            IUmbracoControllerWrapperService controllerWrapperService,
            ISendEmailUponCompletedSubmissionService
            sendEmailUponCompletedSubmissionService,
            IUmbracoControllerWrapperService
            umbracoControllerWrapperService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _sendEmailUponCompletedSubmissionService = sendEmailUponCompletedSubmissionService;
            _controllerWrapperService = controllerWrapperService;
            _getOrderItemsFilterByOrderIdOrUserIdService = getOrderItemsFilterByOrderIdOrUserIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
            _updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService = updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService;
            _getOrderByOrderRefIdService = getOrderByOrderRefIdService;
        }

        [HttpPost]
        public virtual void SendPayPalPostRequest(string orderRefId, int orderId)
        {
            var umbracoHelper = _controllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            //Data parameter Example
            //string data = "name=" + value
            var url = "https://www.paypal.com/cgi-bin/webscr";
            var orderItems = _getOrderItemsFilterByOrderIdOrUserIdService.Get(umbracoHelper: umbracoHelper, orderId: orderId, userId: 0);
            var paymentCancelledAbsoluteUrl = "payment/cancelled/".ToAbsoluteUrl();
            var processPayPalPaymentUrl = "paypal-response-after-payment/".ToAbsoluteUrl();
            var paypalImageHeader = "https://www.maltaphotoaward.com/maltainternationalphotoaward_paypalheader.jpg";

            HttpResponseBase response = HttpContext.Response;
            response.Clear();

            StringBuilder s = new StringBuilder();
            s.Append("<html>");
            s.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
            s.AppendFormat("<form name='form' action='{0}' method='post'>", url); 
            //foreach (string key in data)
            //{
            //    s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", key, data[key]);
            //}

            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "cmd", "_cart");
            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "custom", orderRefId);
            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "cancel_return", paymentCancelledAbsoluteUrl);
            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "return", processPayPalPaymentUrl);
            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "image_url", paypalImageHeader);
            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "upload", "1");

            var orderItemCnt = 1;
            foreach (var item in orderItems)
            {
                s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "item_name_"+ orderItemCnt, item.OrderItemTitle);
                s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "amount_"+ orderItemCnt, item.OrderPhotoPrice.ToString());
                orderItemCnt++;
            }

            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "business", "9FT8BJV3XFGYL");
           s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "lc", "MT");
           s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "currency_code", "EUR");
            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "no_note", "0");
            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "button_subtype", "products");
            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "cn", "Add special instructions to the seller:");
            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "no_shipping", "1");
            s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "bn", "PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted");


            s.Append("</form></body></html>");
            response.Write(s.ToString()); 
            response.End();
        }

        public virtual ActionResult ProcessPayPalResponse(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var orderRefId = Request.QueryString["cm"];
            var transactionStatus = Request.QueryString["st"];
            var transactionId = Request.QueryString["tx"];

            if (!String.IsNullOrEmpty(orderRefId) && !String.IsNullOrEmpty(transactionStatus) &&
                !String.IsNullOrEmpty(transactionId))
            {

                var orderAsPoco = _getOrderByOrderRefIdService.Get(orderRefId);
                orderAsPoco.Date = DateTime.Now;
                

                switch (transactionStatus)
                {
                    
                    case ConstantValues.PayPal_Transaction_Response_Completed:
                        orderAsPoco.PaymentStatusEnumId = (int) Enums.PaymentStatus.OK;
                        orderAsPoco.PaymentTransactionResponse = transactionStatus;
                        //you get the submission linked with this order that has been paid for photos
                        //and change it to payment completed
                        var orderId = orderAsPoco.Id;
                        _updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService.Update(orderId);
                        orderAsPoco.PayPalTransactionId = transactionId;

                        //HERE WE NEED TO SEND EMAILS BOTH TO ADMINISTRATORS AND TO THE CLIENT ABOUT THE NEW SUBMISSIONS
                        try
                        {
                            _sendEmailUponCompletedSubmissionService.Send(umbracoHelper,
                                orderId,
                                Members.GetCurrentMemberId());
                        }
                        catch (Exception ex) { }

                        break;
                    case ConstantValues.PayPal_Transaction_Response_Denied:
                        orderAsPoco.PaymentStatusEnumId = (int)Enums.PaymentStatus.NOTOK;
                        orderAsPoco.PaymentTransactionResponse = transactionStatus;
                        //you get the submission linked with this order that has been paid for photos
                        //and change it to payment completed
                        orderId = orderAsPoco.Id;
                        _updateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService.Update(orderId);
                        orderAsPoco.PayPalTransactionId = transactionId;

                        //HERE WE NEED TO SEND EMAILS BOTH TO ADMINISTRATORS AND TO THE CLIENT ABOUT THE NEW SUBMISSIONS
                        try
                        {
                            _sendEmailUponCompletedSubmissionService.Send(umbracoHelper,
                                orderId,
                                Members.GetCurrentMemberId(),paymentSuccessfull:false);
                        }
                        catch (Exception ex) { }

                        break;
                    default:
                        //DO something: OTHER RESULT
                        break;
                }

                using (var db = _getCurrentUmbracoDatabaseService.Get())
                {
                    db.Update(DatabaseConstantValues.OrdersTableName,
                        DatabaseConstantValues.Orders_Poco_Primary_Key_Value,
                        orderAsPoco,
                        orderAsPoco.Id);
                }


                switch (transactionStatus)
                {
                    case ConstantValues.PayPal_Transaction_Response_Completed:
                        return RedirectToUmbracoPage(ConstantValues.Default_Successful_Page_Id);
                        break;
                    case ConstantValues.PayPal_Transaction_Response_Pending:
                        return RedirectToUmbracoPage(ConstantValues.Default_Successful_Page_Id);
                    default:
                        //DO something: OTHER RESULT
                        break;
                }
            }

            return RedirectToUmbracoPage(ConstantValues.Default_Failed_Page_Id);
        }
    }
}