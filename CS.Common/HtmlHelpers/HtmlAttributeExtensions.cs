﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CS.Common.HtmlHelpers
{
    public class HtmlAttribute : IHtmlString
    {
        private string _InternalValue = String.Empty;
        private string _Seperator;

        public string Name { get; set; }
        public string Value { get; set; }
        public bool Condition { get; set; }

        public HtmlAttribute(string name)
            : this(name, null)
        {
        }

        public HtmlAttribute(string name, string seperator)
        {
            Name = name;
            _Seperator = seperator ?? " ";
        }

        public HtmlAttribute Add(string value)
        {
            return Add(value, true);
        }

        public HtmlAttribute Add(string value, bool condition)
        {
            if (!String.IsNullOrWhiteSpace(value) && condition)
                _InternalValue += value + _Seperator;

            return this;
        }

        public string ToHtmlString()
        {
            if (!String.IsNullOrWhiteSpace(_InternalValue))
                _InternalValue = String.Format("{0}=\"{1}\"",
                    Name,
                    _InternalValue.Substring(0, _InternalValue.Length - _Seperator.Length));
            return _InternalValue;
        }
    }

    public static class HtmlAttributeExtensions
    {
        public static HtmlAttribute Css(this HtmlHelper html, string value)
        {
            return Css(html, value, true);
        }

        public static HtmlAttribute Css(this HtmlHelper html, string value, bool condition)
        {
            return Css(html, null, value, condition);
        }

        public static HtmlAttribute Css(this HtmlHelper html, string seperator, string value, bool condition)
        {
            return new HtmlAttribute("class", seperator).Add(value, condition);
        }

        public static HtmlAttribute Attr(this HtmlHelper html, string name, string value)
        {
            return Attr(html, name, value, true);
        }

        public static HtmlAttribute Attr(this HtmlHelper html, string name, string value, bool condition)
        {
            return Attr(html, name, null, value, condition);
        }

        public static HtmlAttribute Attr(this HtmlHelper html,
            string name,
            string seperator,
            string value,
            bool condition)
        {
            return new HtmlAttribute(name, seperator).Add(value, condition);
        }
    }
}
