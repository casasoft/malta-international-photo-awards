﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSTemplate.Code.Models.Shortlist_Photos;
using CSTemplate.Code.Services.ShortListedPhotos;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ShortlistPhotosFormController : SurfaceController
    {
        private readonly ISubmitAsAShortListPhotoService _submitAsAShortListPhotoService;

        public ShortlistPhotosFormController(ISubmitAsAShortListPhotoService submitAsAShortListPhotoService)
        {
            _submitAsAShortListPhotoService = submitAsAShortListPhotoService;
        }
        [HttpPost]
        public virtual ActionResult HandleShortlistPhotos(ShortlistPhotosFormModel shortlistPhotosFormModel)
        {
            //todo: [For: Fatih | 2018/08/27] ShortListStatusChanged Service Will Be Here (WrittenBy: Fatih)        			
            //logic for handling the shortlisted photos

            _submitAsAShortListPhotoService.Submit(shortlistPhotosFormModel);

            var sortlistPage = "/members-area/shortlist-photos/download-shortlist/";
            return Redirect(sortlistPage);
        }
    }
}