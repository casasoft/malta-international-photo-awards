﻿using CS.Common.Constants;

namespace CS.Common.Helpers
{
    public static class IpAddressHelpers
    {
        
        public static string ConvertIPV4ToIPV6(string IPV4)
        {
            string[] octets = IPV4.Split('.');
            byte[] octetBytes = new byte[4];
            for (int i = 0; i < 4; ++i)
            {
                octetBytes[i] = (byte)int.Parse(octets[i]);
            }

            byte[] ipv4asIpV6addr = new byte[16];
            ipv4asIpV6addr[10] = (byte)0xff;
            ipv4asIpV6addr[11] = (byte)0xff;
            ipv4asIpV6addr[12] = octetBytes[0];
            ipv4asIpV6addr[13] = octetBytes[1];
            ipv4asIpV6addr[14] = octetBytes[2];
            ipv4asIpV6addr[15] = octetBytes[3];

            string ipv6 = "";
            for (int i = 0; i < ipv4asIpV6addr.Length; i += 2)
            {
                if (i > 0)
                {
                    ipv6 += ":";
                }

                ipv6 += TextHelpers.PadText(ipv4asIpV6addr[i].ToString("X"), Enums.TextAlign.Right, 2, "0");
                ipv6 += TextHelpers.PadText(ipv4asIpV6addr[i + 1].ToString("X"), Enums.TextAlign.Right, 2, "0");
            }
            return ipv6;

        }
    }
}
