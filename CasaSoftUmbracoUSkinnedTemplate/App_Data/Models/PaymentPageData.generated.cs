//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>Payment Page</summary>
	[PublishedContentModel("paymentPageData")]
	public partial class PaymentPageData : PublishedContentModel, IUsn_Cmp_DisableDelete, IUsn_Cmp_PageGenericProperties, IUsn_Cmp_PageNavSettings, IUsn_Cmp_ReusableContent, IUsn_Cmp_Scripts, IUsn_Cmp_Seo, IUsn_Cmp_Sp_BannerSection, IUsn_Cmp_Sp_Content
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "paymentPageData";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public PaymentPageData(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<PaymentPageData, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Disable delete: If "Yes" is selected, when a user attempts to delete this node they will be presented with a warning message indicating that this node cannot be deleted.
		///</summary>
		[ImplementPropertyType("disableDelete")]
		public bool DisableDelete
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_DisableDelete.GetDisableDelete(this); }
		}

		///<summary>
		/// Custom body classes: Override the default styles of this page by adding page specific classes. Leave a space between each class e.g. custom-class another-class
		///</summary>
		[ImplementPropertyType("customBodyClasses")]
		public string CustomBodyClasses
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_PageGenericProperties.GetCustomBodyClasses(this); }
		}

		///<summary>
		/// Hide website footer section: Remove the global footer content at the end of this page. For example, contact information and navigation.
		///</summary>
		[ImplementPropertyType("hideWebsiteFooterSection")]
		public bool HideWebsiteFooterSection
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_PageGenericProperties.GetHideWebsiteFooterSection(this); }
		}

		///<summary>
		/// Hide website header section: Remove the global header content at the start of this page. For example, logo and navigation.
		///</summary>
		[ImplementPropertyType("hideWebsiteHeaderSection")]
		public bool HideWebsiteHeaderSection
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_PageGenericProperties.GetHideWebsiteHeaderSection(this); }
		}

		///<summary>
		/// Breadcrumb link text: Node name will be used by default if nothing is entered.
		///</summary>
		[ImplementPropertyType("breadcrumbLinkText")]
		public string BreadcrumbLinkText
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_PageNavSettings.GetBreadcrumbLinkText(this); }
		}

		///<summary>
		/// Hide breadcrumb
		///</summary>
		[ImplementPropertyType("hideBreadcrumb")]
		public bool HideBreadcrumb
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_PageNavSettings.GetHideBreadcrumb(this); }
		}

		///<summary>
		/// Hide from all navigation
		///</summary>
		[ImplementPropertyType("umbracoNaviHide")]
		public bool UmbracoNaviHide
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_PageNavSettings.GetUmbracoNaviHide(this); }
		}

		///<summary>
		/// Bottom components: Select the components you would like to appear on this page. The components selected will only be displayed if "Yes" is selected above.
		///</summary>
		[ImplementPropertyType("bottomComponents")]
		public IEnumerable<IPublishedContent> BottomComponents
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_ReusableContent.GetBottomComponents(this); }
		}

		///<summary>
		/// Override default bottom components: Select "Yes" to override the default components selected within "Global Settings".
		///</summary>
		[ImplementPropertyType("overrideDefaultBottomComponents")]
		public bool OverrideDefaultBottomComponents
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_ReusableContent.GetOverrideDefaultBottomComponents(this); }
		}

		///<summary>
		/// Override default pods: Select "Yes" to override the default pods selected within "Global Settings".
		///</summary>
		[ImplementPropertyType("overrideDefaultPods")]
		public bool OverrideDefaultPods
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_ReusableContent.GetOverrideDefaultPods(this); }
		}

		///<summary>
		/// Override default top components: Select "Yes" to override the default components selected within "Global Settings".
		///</summary>
		[ImplementPropertyType("overrideDefaultTopComponents")]
		public bool OverrideDefaultTopComponents
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_ReusableContent.GetOverrideDefaultTopComponents(this); }
		}

		///<summary>
		/// Pods: Select the pods you would like to appear on this page. The pods selected will only be displayed if "Yes" is selected above.
		///</summary>
		[ImplementPropertyType("pagePods")]
		public IEnumerable<IPublishedContent> PagePods
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_ReusableContent.GetPagePods(this); }
		}

		///<summary>
		/// Top components: Select the components you would like to appear on this page. The components selected will only be displayed if "Yes" is selected above.
		///</summary>
		[ImplementPropertyType("topComponents")]
		public IEnumerable<IPublishedContent> TopComponents
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_ReusableContent.GetTopComponents(this); }
		}

		///<summary>
		/// After opening body scripts: Anything you enter here will be placed after the opening &lt;body&gt; tag on this page of your website.
		///</summary>
		[ImplementPropertyType("pageAfterOpeningBodyScripts")]
		public string PageAfterOpeningBodyScripts
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Scripts.GetPageAfterOpeningBodyScripts(this); }
		}

		///<summary>
		/// Before closing body scripts: Anything you enter here will be placed before the closing &lt;/body&gt; tag on this page of your website.
		///</summary>
		[ImplementPropertyType("pageBeforeClosingBodyScripts")]
		public string PageBeforeClosingBodyScripts
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Scripts.GetPageBeforeClosingBodyScripts(this); }
		}

		///<summary>
		/// Header scripts: Anything you enter here will be placed before the closing &lt;/head&gt; tag on this page of your website.
		///</summary>
		[ImplementPropertyType("pageHeaderScripts")]
		public string PageHeaderScripts
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Scripts.GetPageHeaderScripts(this); }
		}

		///<summary>
		/// Hide from search engines: This will add a noindex meta tag to your page and exclude this page from your sitemap.xml
		///</summary>
		[ImplementPropertyType("hideFromSearchEngines")]
		public bool HideFromSearchEngines
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Seo.GetHideFromSearchEngines(this); }
		}

		///<summary>
		/// Meta description: This is shown in search engines, when the page is shared via social media and in your sites own search results. Try to keep this below 155 characters.
		///</summary>
		[ImplementPropertyType("metaDescription")]
		public string MetaDescription
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Seo.GetMetaDescription(this); }
		}

		///<summary>
		/// Open Graph image: The image used when someone shares the page on social media, such as, Twitter or Facebook.  We suggest that you use an image of at least 1200x630 pixels.  If nothing is uploaded here the "Default Open Graph image" from your "Global Settings" will be used.
		///</summary>
		[ImplementPropertyType("openGraphImage")]
		public IPublishedContent OpenGraphImage
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Seo.GetOpenGraphImage(this); }
		}

		///<summary>
		/// Web page title: This is shown in search engines, web browsers and when the page is shared via social media. If left empty the default page title will be used.  The default page title will be a combination of the current page node name and the website name defined in "Global Settings".
		///</summary>
		[ImplementPropertyType("pageTitle")]
		public string PageTitle
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Seo.GetPageTitle(this); }
		}

		///<summary>
		/// Sitemap XML change frequency
		///</summary>
		[ImplementPropertyType("sitemapXMLChangeFrequency")]
		public USNStarterKit.USNEnums.Options SitemapXmlchangeFrequency
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Seo.GetSitemapXmlchangeFrequency(this); }
		}

		///<summary>
		/// Sitemap XML priority
		///</summary>
		[ImplementPropertyType("sitemapXMLPriority")]
		public decimal SitemapXmlpriority
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Seo.GetSitemapXmlpriority(this); }
		}

		///<summary>
		/// Twitter creator username: Username for the content creator/author used in the twitter card.
		///</summary>
		[ImplementPropertyType("twitterCreatorUsername")]
		public object TwitterCreatorUsername
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Seo.GetTwitterCreatorUsername(this); }
		}

		///<summary>
		/// Alternative URL: Use this field to provide multiple URLs for a content node.  For example if you were to enter "examplepage1,examplepage2/this-is-a-test", this would resolve the following URLs to the same page.  /examplepage1/ /examplepage2/this-is-a-test/  Please note that the values you use must be lowercase, not use a leading slash and not use a file extension.
		///</summary>
		[ImplementPropertyType("umbracoUrlAlias")]
		public string UmbracoUrlAlias
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Seo.GetUmbracoUrlAlias(this); }
		}

		///<summary>
		/// Add color overlay: If “Yes” is selected a semi transparent block of the background color is added over your image.
		///</summary>
		[ImplementPropertyType("addColorOverlay")]
		public bool AddColorOverlay
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetAddColorOverlay(this); }
		}

		///<summary>
		/// Content: {div class="panel-heading"}{h3}Content{/h3}{/div}
		///</summary>
		[ImplementPropertyType("content")]
		public object Content
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetContent(this); }
		}

		///<summary>
		/// Display banner section
		///</summary>
		[ImplementPropertyType("displayHeaderImageSection")]
		public bool DisplayHeaderImageSection
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetDisplayHeaderImageSection(this); }
		}

		///<summary>
		/// Animate
		///</summary>
		[ImplementPropertyType("headerAnimate")]
		public bool HeaderAnimate
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderAnimate(this); }
		}

		///<summary>
		/// Background color: Color selected will dictate the color to be used for any text.  If a background image is used, select the color that works best for the text over the image.
		///</summary>
		[ImplementPropertyType("headerBackgroundColor")]
		public string HeaderBackgroundColor
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderBackgroundColor(this); }
		}

		///<summary>
		/// Button color
		///</summary>
		[ImplementPropertyType("headerButtonColor")]
		public string HeaderButtonColor
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderButtonColor(this); }
		}

		///<summary>
		/// Heading: Your heading hierarchy is important for SEO. This will not affect font size.
		///</summary>
		[ImplementPropertyType("headerHeading")]
		public USNStarterKit.USNHeading.Models.USNHeading HeaderHeading
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderHeading(this); }
		}

		///<summary>
		/// Image: Takeover: 1500px x 1100px Medium: 1500px x 550px Short: 1500px x 400px Parallax: 1500px x 1100px  Focal point is defined within your "Media" section.
		///</summary>
		[ImplementPropertyType("headerImage")]
		public IPublishedContent HeaderImage
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderImage(this); }
		}

		///<summary>
		/// Image alt text
		///</summary>
		[ImplementPropertyType("headerImageAltText")]
		public string HeaderImageAltText
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderImageAltText(this); }
		}

		///<summary>
		/// Link: The "Caption" field will be used for your link text.
		///</summary>
		[ImplementPropertyType("headerLink")]
		public Umbraco.Web.Models.RelatedLinks HeaderLink
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderLink(this); }
		}

		///<summary>
		/// Parallax
		///</summary>
		[ImplementPropertyType("headerParallax")]
		public bool HeaderParallax
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderParallax(this); }
		}

		///<summary>
		/// Scroll prompt
		///</summary>
		[ImplementPropertyType("headerScrollPrompt")]
		public bool HeaderScrollPrompt
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderScrollPrompt(this); }
		}

		///<summary>
		/// Secondary heading
		///</summary>
		[ImplementPropertyType("headerSecondaryHeading")]
		public string HeaderSecondaryHeading
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderSecondaryHeading(this); }
		}

		///<summary>
		/// Banner style
		///</summary>
		[ImplementPropertyType("headerStyle")]
		public USNStarterKit.USNEnums.Options HeaderStyle
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderStyle(this); }
		}

		///<summary>
		/// Text
		///</summary>
		[ImplementPropertyType("headerText")]
		public IHtmlString HeaderText
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderText(this); }
		}

		///<summary>
		/// Text alignment
		///</summary>
		[ImplementPropertyType("headerTextAlignment")]
		public USNStarterKit.USNEnums.Options HeaderTextAlignment
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetHeaderTextAlignment(this); }
		}

		///<summary>
		/// Settings: {div class="panel-heading"}{h3}Settings{/h3}{/div}
		///</summary>
		[ImplementPropertyType("settings")]
		public object Settings
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetSettings(this); }
		}

		///<summary>
		/// Video: MP4 format only. Image above will display until video loads. Small file size recommended for quick downloads.
		///</summary>
		[ImplementPropertyType("video")]
		public IPublishedContent Video
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_BannerSection.GetVideo(this); }
		}

		///<summary>
		/// Main content
		///</summary>
		[ImplementPropertyType("bodyText")]
		public IHtmlString BodyText
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_Content.GetBodyText(this); }
		}

		///<summary>
		/// Main image: 1200px x 600px  Focal point is defined within your "Media" section.
		///</summary>
		[ImplementPropertyType("mainImage")]
		public IPublishedContent MainImage
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_Content.GetMainImage(this); }
		}

		///<summary>
		/// Main image alt text
		///</summary>
		[ImplementPropertyType("mainImageAltText")]
		public string MainImageAltText
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_Content.GetMainImageAltText(this); }
		}

		///<summary>
		/// Main image caption
		///</summary>
		[ImplementPropertyType("mainImageCaption")]
		public string MainImageCaption
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_Content.GetMainImageCaption(this); }
		}

		///<summary>
		/// Page title: Node name will be used by default if nothing is entered.
		///</summary>
		[ImplementPropertyType("onPageTitle")]
		public string OnPageTitle
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_Sp_Content.GetOnPageTitle(this); }
		}
	}
}
