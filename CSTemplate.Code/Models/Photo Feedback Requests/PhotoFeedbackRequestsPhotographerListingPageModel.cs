﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.Photo_Feedback_Requests
{
    public class PhotoFeedbackRequestsPhotographerListingPageModel : USNBaseViewModel
    {
        public _ListingItemsAsMasonryPartialModel ListingItemsAsMasonryPartial { get; set; }
        public string ShowingResults { get; set; }
        public int ItemsPerPage { get; set; }
        public int ItemsTotalAmount { get; set; }

        public PhotoFeedbackRequestsPhotographerListingPageModel(IPublishedContent content) : base(content)
        {

        }

    }
}
