﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.AwardCategory
{

    public interface IGetCategoryFilterByCategoryIdService
    {
        List<SelectListItem> Get(UmbracoHelper umbracoHelper,int categoryId);
    }

    [Service]
    public class GetCategoryFilterByCategoryIdService : IGetCategoryFilterByCategoryIdService
    {

        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetCategoryFilterByCategoryIdService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public List<SelectListItem> Get(UmbracoHelper umbracoHelper, int categoryId)
        {
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;


            var awardEditionsNode = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionPageData)x).FirstOrDefault();

            var awardCategory = awardEditionsNode.Descendants()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Category_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionCategoryPageData)x).Where(x => x.Id==categoryId).FirstOrDefault();
            List<SelectListItem> returnofAwardCategoryList = new List<SelectListItem>();
            returnofAwardCategoryList.Add(new SelectListItem()
            {
                Text = awardCategory.Name,
                Value = awardCategory.Id.ToString()
            });
            return returnofAwardCategoryList;
        }
    }
}
