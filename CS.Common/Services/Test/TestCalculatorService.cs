﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Services.Test
{
    public interface ITestCalculatorService
    {
        int Add(int num1, int num2);
    }
    [Service(Priority=2000)]
    public class TestCalculatorService : ITestCalculatorService
    {
        public int Add(int num1, int num2)
        {
            return num1 + num2;
        }
    }
}
