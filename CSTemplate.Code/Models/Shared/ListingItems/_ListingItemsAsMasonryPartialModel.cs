﻿using CS.Common.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared.ListingItems
{
    public class _ListingItemsAsMasonryPartialModel
    {
        public List<_ItemPhotoModel> ListOfPhotos { get; set; }

        public PagerModel Pagination { get; set; }

    }
}
