﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.My_Submissions;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Submissions;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class MySubmissionsListingPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetMySubmissionsService _getMySubmissionsService;

        public MySubmissionsListingPageDataController(IUmbracoControllerWrapperService umbracoControllerWrapperService, IGetMySubmissionsService getMySubmissionsService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getMySubmissionsService = getMySubmissionsService;
        }

        public virtual ActionResult MySubmissionsListingPage(RenderModel model, string page)
        {
            int pageNumber = 1;
            if (!string.IsNullOrEmpty(Request.QueryString["page"]) && Request.QueryString["page"]!=null)
            {
                pageNumber = Convert.ToInt32(Request.QueryString["page"]);
            }
            
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var userId = Members.GetCurrentMemberId();
            MySubmissionsListingPageModel mySubmissionsListingPageModel = _getMySubmissionsService.Get(umbracoHelper,model.Content,userId,pageNumber);
            return base.Populate(mySubmissionsListingPageModel);
        }
    }
}