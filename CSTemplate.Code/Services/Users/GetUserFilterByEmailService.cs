﻿using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Members;
using CS.Common.Services;

namespace CSTemplate.Code.Services.Users
{
    public interface IGetUserFilterByEmailService
    {
        _MemberItemModel Get(UmbracoHelper umbracoHelper, string email);
    }

    [Service]
    public class GetUserFilterByEmailService: IGetUserFilterByEmailService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetUserFilterByEmailService(
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public _MemberItemModel Get(UmbracoHelper umbracoHelper, string email)
        {
            var returnMemberModel = new _MemberItemModel();
            if (string.IsNullOrEmpty(email)) return returnMemberModel;
            
            var getMember = ApplicationContext.Current.Services.MemberService.GetByEmail(email);
            if (getMember!=null)
            {
                var member = new Member(_umbracoHelperService.TypedMember(umbracoHelper, getMember.Id));
                returnMemberModel.MemberId = member.Id;
                returnMemberModel.Name = member.ClientName;
                returnMemberModel.Surname = member.ClientSurname;
                returnMemberModel.InsertedPhone = member.Phone;
                returnMemberModel.Nationality = member.Nationality;
                returnMemberModel.County = member.Country;
                returnMemberModel.InsertedEmail = getMember.Email;
            }
            
            return returnMemberModel;
        }
    }
}
