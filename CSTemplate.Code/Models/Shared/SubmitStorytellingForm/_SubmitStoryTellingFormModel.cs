﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;
using CSTemplate.Code.Models.EditorTemplates;

namespace CSTemplate.Code.Models.Shared
{
    public class _SubmitStoryTellingFormModel
    {
        public _SubmitPhotoItemModel SubmitPhotoItem { get; set; }

        public string TermsAndConditionsText { get; set; }
        public string ButtonFormText { get; set; }
        public string UploadInformationText { get; set; }

        public string True
        {
            get
            {
                return "true";
            }
        }

        [Required]
        [Compare("True", ErrorMessage = "The 'Terms and Conditions' field is required")]
        public bool TermsConditions { get; set; }
    }
}
