﻿angular.module("umbraco").controller("OrderStatus",
    function ($scope, $http) {

        var url = "backoffice/Orders/OrdersApi/GetStatusList/";
        $http.get(url).then(function (response) {
            console.log("OK");
            $scope.orderStatusList = response.data;

        });
    });