﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.MembersArea
{
    public class MembersAreaPageModel : USNBaseViewModel
    {
        public MembersAreaPageModel(IPublishedContent content) : base(content)
        {

        }

    }
}
