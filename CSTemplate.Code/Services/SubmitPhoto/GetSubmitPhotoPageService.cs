﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.SubmitPhotos;
using CSTemplate.Code.Models.SubmitPhotoSelection;
using CSTemplate.Code.Services.Loader;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.SubmitPhoto
{
    public interface IGetSubmitPhotoPageService
    {
        SubmitPhotoPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content);
    }

    [Service]
    public class GetSubmitPhotoPageService : IGetSubmitPhotoPageService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetSubmitPhotosFormService _getSubmitPhotosFormService;
        private readonly IGetLoaderPageService _getLoaderPageService;

        public GetSubmitPhotoPageService(
            IUmbracoHelperService umbracoHelperService,
            IGetSubmitPhotosFormService getSubmitPhotosFormService,
            IGetLoaderPageService getLoaderPageService
            )
        {
            _umbracoHelperService = umbracoHelperService;
            _getSubmitPhotosFormService = getSubmitPhotosFormService;
            _getLoaderPageService = getLoaderPageService;
        }

        public SubmitPhotoPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content)
        {
            var explanationText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_Photo_Explanation_Text);
            var subExplanationText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_Photo_SubExplanation_Text);
            var browserSuggestionText= _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Browser_Suggestion_Text);
         
            SubmitPhotoPageModel submitPhotoPageModel = new SubmitPhotoPageModel(content)
            {
                ExplanationText = explanationText,
                SubExplanationText = subExplanationText,
                BrowserSuggestionText = browserSuggestionText,
                LoaderPageModel = _getLoaderPageService.Get(umbracoHelper),
                SubmitPhotoForm = _getSubmitPhotosFormService.Get(umbracoHelper, content),
            };
            return submitPhotoPageModel;
        }
    }
}
