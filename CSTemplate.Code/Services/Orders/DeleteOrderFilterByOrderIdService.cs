﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.OrderItems;
using CSTemplate.Code.Services.Submissions;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Orders
{

    public interface IDeleteOrderFilterByOrderIdService
    {
        /// <summary>
        /// It deletes the order filter by order id.
        /// </summary>
        /// <param name="orderId">int Order Id </param>
        /// <returns>Process result if the order delete process has been completed successfully it turns true else it turns false</returns>
        bool Delete(UmbracoHelper umbracoHelper,int orderId);
    }

    /// <summary>
    /// It deletes the order filter by order id.
    /// </summary>
    [Service]
    public class DeleteOrderFilterByOrderIdService : IDeleteOrderFilterByOrderIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetOrderPocoFilterByOrderIdService _getOrderPocoFilterByOrderIdService;
        private readonly IGetOrderItemPocoFilterByOrderIdService _getOrderItemPocoFilterByOrderIdService;
        private readonly IGetSubmissionPocoDetailsFilterByIdService _getSubmissionPocoDetailsFilterByIdService;
        private readonly IUpdateSubmissionFeedbackRequestStatusFilterBySubmissionIdService _updateSubmissionFeedbackRequestStatusFilterBySubmissionIdService;

        public DeleteOrderFilterByOrderIdService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService, 
            IGetOrderPocoFilterByOrderIdService getOrderPocoFilterByOrderIdService,
            IGetOrderItemPocoFilterByOrderIdService getOrderItemPocoFilterByOrderIdService,
            IGetSubmissionPocoDetailsFilterByIdService getSubmissionPocoDetailsFilterByIdService,
            IUpdateSubmissionFeedbackRequestStatusFilterBySubmissionIdService updateSubmissionFeedbackRequestStatusFilterBySubmissionIdService
            )
        {
            _updateSubmissionFeedbackRequestStatusFilterBySubmissionIdService = updateSubmissionFeedbackRequestStatusFilterBySubmissionIdService;
            _getSubmissionPocoDetailsFilterByIdService = getSubmissionPocoDetailsFilterByIdService;
            _getOrderItemPocoFilterByOrderIdService = getOrderItemPocoFilterByOrderIdService;
            _getOrderPocoFilterByOrderIdService = getOrderPocoFilterByOrderIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        /// <summary>
        /// It deletes the order filter by order id.
        /// </summary>
        /// <param name="orderId">int Order Id </param>
        /// <returns>Process result if the order delete process has been completed successfully it turns true else it turns false</returns>
        public bool Delete(UmbracoHelper umbracoHelper, int orderId)
        {
            var result = false;
            if (orderId <= 0) return result;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var orderTable = Constants.DatabaseConstantValues.OrdersTableName;
            var orderPrimaryKeyField = Constants.DatabaseConstantValues.Orders_Poco_Primary_Key_Value;

            var orderResult = _getOrderPocoFilterByOrderIdService.Get(orderId);
            var updatedOrder = orderResult;
            updatedOrder.PaymentStatusEnumId = (int)Constants.Enums.PaymentStatus.DELETED;

            var orderItem = _getOrderItemPocoFilterByOrderIdService.Get(orderId);
            var submissionId = orderItem.SubmissionId;
            var submissionDetail = _getSubmissionPocoDetailsFilterByIdService.Get(submissionId);
            if (submissionDetail.FeedbackRequest)
            {
                _updateSubmissionFeedbackRequestStatusFilterBySubmissionIdService.Update(umbracoHelper:umbracoHelper,submissionId: submissionId, feedbackRequest: false);
            }
            var updateOrder = umbracoDb.Update(orderTable, orderPrimaryKeyField, updatedOrder);
            result = updateOrder > 0 ? true : result;

            return result;
        }
    }
}
