﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Constants;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.SubmitPhotos;
using CSTemplate.Code.Services.Photos;
using CSTemplate.Code.Services.SubmitPhoto;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class SubmitPhotoPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetSubmitPhotoPageService _getSubmitPhotoPageService;

        public SubmitPhotoPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetSubmitPhotoPageService getSubmitPhotoPageService)
        {
            
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getSubmitPhotoPageService = getSubmitPhotoPageService;
        }
        // GET: SubmitPhotoPageData
        public virtual ActionResult SubmitPhotoPage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            
            SubmitPhotoPageModel submitPhotoPageModel = _getSubmitPhotoPageService.Get(umbracoHelper, model.Content);
            return base.Populate(submitPhotoPageModel);
        }


    }
}