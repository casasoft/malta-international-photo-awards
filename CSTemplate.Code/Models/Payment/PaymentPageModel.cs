﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.Payment
{
    public class PaymentPageModel : USNBaseViewModel
    {
        public RouteValueDictionary RouteValues { get; set; }
        public bool ItIsAPayment { get; set; }

        public PaymentPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
