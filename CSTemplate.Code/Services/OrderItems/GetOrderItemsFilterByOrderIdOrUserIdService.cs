﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Prices;
using CSTemplate.Code.Services.SubmissionPhotos;
using Umbraco.Core.Persistence;
using Umbraco.Web;

namespace CSTemplate.Code.Services.OrderItems
{

    public interface IGetOrderItemsFilterByOrderIdOrUserIdService
    {
        List<_OrderPhotosModel> Get(UmbracoHelper umbracoHelper, int orderId, int userId);
    }

    [Service]
    public class GetOrderItemsFilterByOrderIdOrUserIdService : IGetOrderItemsFilterByOrderIdOrUserIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetSubmissionPhotosService _getSubmissionPhotosService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetAllPricesService _getAllPricesService;

        public GetOrderItemsFilterByOrderIdOrUserIdService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IGetSubmissionPhotosService getSubmissionPhotosService,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService,
            IGetAllPricesService getAllPricesService
        )
        {
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _getAllPricesService = getAllPricesService;
            _getSubmissionPhotosService = getSubmissionPhotosService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public List<_OrderPhotosModel> Get(UmbracoHelper umbracoHelper, int orderId, int userId)
        {
            var orderItemList = new List<_OrderPhotosModel>();
            if (orderId < 0 || userId < 0) return orderItemList;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var orderItemsTable = DatabaseConstantValues.OrderItemsTableName;
            var submissionsTable = DatabaseConstantValues.SubmissionsTableName;
            var photosTable = DatabaseConstantValues.PhotosTableName;
            var orderTable = DatabaseConstantValues.OrdersTableName;

            var orderItemsAlias = DatabaseConstantValues.aliasForOrderItemsPocoTableNameAlias;
            var submissionAlias = DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var photoAlias = DatabaseConstantValues.aliasForPhotosPocoTableNameAlias;
            var ordersAlias = DatabaseConstantValues.aliasForOrdersPocoTableNameAlias;

            var orderItemsSubmissionIdField = DatabaseConstantValues.OrderItems_Submission_Id_Field_Name;
            var submissionIdPrimaryField = DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var photoSubmissionIdField = DatabaseConstantValues.Photos_Submission_Id_Field_Name;
            var orderPrimaryKeyField = DatabaseConstantValues.Orders_Poco_Primary_Key_Value;

            var orderItemsOrderIdField = DatabaseConstantValues.OrderItems_Order_Id_Field_Name;
            var photoUrlField = DatabaseConstantValues.Photos_PhotoUrl_Field_Name;
            var orderUserIdField = DatabaseConstantValues.Orders_UserId_Field_Name;

            var orderItemsPayForSubmissionField = DatabaseConstantValues.OrderItems_Pay_For_Submission_Field_Name;
            var orderItemsPayForFeedbackField = DatabaseConstantValues.OrderItems_Pay_For_Feedback_Field_Name;
            var orderItemsFeedbackCostField = DatabaseConstantValues.OrderItems_Feedback_Cost_Field_Name;
            var orderItemsSubmissionCostField = DatabaseConstantValues.OrderItems_Submission_Cost_Field_Name;

            var submissionCategoryIdField = DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionLocationField = DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionDescriptionField = DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionTitleField = DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionDateField = DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionUserIdField = DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionStoryTellingField = DatabaseConstantValues.Submissions_Is_A_Story_Telling_Field_Name;



            string conditional = string.Empty;

            if (orderId > 0)
            {
                conditional = $"{orderItemsAlias }.{ orderItemsOrderIdField}={orderId}";
            }
            else if (userId > 0)
            {
                conditional = $"{ordersAlias}.{orderUserIdField}={userId}";
            }
            

            var sqlQuery = new Sql($@"
                SELECT
                    {orderItemsAlias}.{orderItemsPayForFeedbackField},
                    {orderItemsAlias}.{orderItemsPayForSubmissionField},
                    {orderItemsAlias}.{orderItemsOrderIdField},
                    {orderItemsAlias}.{orderItemsSubmissionIdField},
                    {orderItemsAlias}.{orderItemsFeedbackCostField},
                    {orderItemsAlias}.{orderItemsSubmissionCostField},
                    {orderItemsAlias}.{orderItemsOrderIdField},
                    {photoAlias}.{photoUrlField},
                    {submissionAlias}.{submissionCategoryIdField},
                    {submissionAlias}.{submissionLocationField},
                    {submissionAlias}.{submissionDescriptionField},
                    {submissionAlias}.{submissionTitleField},
                    {submissionAlias}.{submissionDateField},
                    {submissionAlias}.{submissionUserIdField},
                    {submissionAlias}.{submissionStoryTellingField}
                    
                    FROM  {orderItemsTable}
		                INNER JOIN {submissionsTable} ON {orderItemsAlias}.{orderItemsSubmissionIdField} = {submissionAlias}.{submissionIdPrimaryField}
		                INNER JOIN {photosTable} ON {submissionAlias}.{submissionIdPrimaryField} = {photoAlias}.{photoSubmissionIdField}
			                WHERE  {orderItemsAlias}.{orderItemsOrderIdField}=
                (
                    SELECT TOP(1)  
                        {ordersAlias}.{orderPrimaryKeyField}
                            FROM {orderTable} 
                              INNER JOIN {orderItemsTable} ON {ordersAlias}.{orderPrimaryKeyField}={orderItemsAlias}.{orderItemsOrderIdField}
                             WHERE  {conditional} 
                                    ORDER BY {ordersAlias}.{orderPrimaryKeyField} DESC
                )  
                                ORDER BY {orderItemsAlias}.{orderItemsOrderIdField} DESC
        ");
            var sqlOrderItemsList = umbracoDb.Query<OrderItemsPoco, PhotoPoco, SubmissionPoco, OrderItemsPoco>((orderItems, photos, submissions) =>
            {

                orderItems.Submission = submissions;
                photos.Submission = submissions;
                return orderItems;

            }, sqlQuery).ToList();
            bool storyTellingRecordControl = false;
            var prices = _getAllPricesService.Get(umbracoHelper);
            foreach (var orderItem in sqlOrderItemsList)
            {
                var orderCategory = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, orderItem.Submission.CategoryId);
                int orderPhotoType = 0;
                var orderItemTitle = "";

                if (orderItem.PayForSubmission == true && orderItem.PayForFeedback == true)
                {
                    orderItemTitle = orderItem.Submission.Title + " & Feedback Request";
                    orderPhotoType = (int)CS.Common.Constants.Enums.OrderPhotosSubmitType.SubmissionWithFeedback;
                }
                else if (orderItem.PayForSubmission == true)
                {
                    orderItemTitle = orderItem.Submission.Title;
                    orderPhotoType = (int)CS.Common.Constants.Enums.OrderPhotosSubmitType.SubmissionOnly;
                }
                else if (orderItem.PayForFeedback == true)
                {
                    orderItemTitle = orderItem.Submission.Title + " Feedback Request only";
                    orderPhotoType = (int)CS.Common.Constants.Enums.OrderPhotosSubmitType.FeedbackOnly;
                }
                var photoUrl = _getSubmissionPhotosService.Get(umbracoHelper, orderItem.SubmissionId).Select(x => x.ItemPhotoImgURL).FirstOrDefault();
                var submissionPhotos = _getSubmissionPhotosService.Get(umbracoHelper, orderItem.SubmissionId).Select(x => x.ItemPhotoImgURL).ToList();
                if (orderItem.Submission.IsAStoryTelling && storyTellingRecordControl == false)
                {

                    orderItemList.Add(new _OrderPhotosModel
                    {
                        OrderCategory = orderCategory.AwardCategoryItemName,
                        OrderPhotoDate = orderItem.Submission.Date,
                        OrderPhotoLocation = orderItem.Submission.Location,
                        OrderPhotoPrice = orderItem.PayForSubmission == true && orderItem.PayForFeedback == true ? (orderItem.SubmissionCost + orderItem.FeedbackCost) : orderItem.PayForSubmission == true ? orderItem.SubmissionCost : orderItem.FeedbackCost,
                        OrderPhotogDescription = orderItem.Submission.Description,
                        OrderItemTitle = orderItemTitle,
                        OrderPhotosType = (CS.Common.Constants.Enums.OrderPhotosSubmitType)orderPhotoType,
                        ListOfPhotosUrls = orderItem.Submission.IsAStoryTelling == true ? submissionPhotos : new List<string> { photoUrl },
                        OrderId = orderItem.OrderId,
                        OrderFeedbackPrice = prices.FeedbackPrice,
                        FeedbackRequesPriceText = "Feedback", //Dictionary   Order_Sumary_Feedback_Price_Text,
                        SubmissionPriceText = "Submission", // Dictionary Order_Sumary_Submission_Price_Text,
                    });
                    storyTellingRecordControl = true;
                }
                else if (!orderItem.Submission.IsAStoryTelling)
                {
                    orderItemList.Add(new _OrderPhotosModel
                    {
                        OrderCategory = orderCategory.AwardCategoryItemName,
                        OrderPhotoDate = orderItem.Submission.Date,
                        OrderPhotoLocation = orderItem.Submission.Location,
                        OrderPhotoPrice = orderItem.PayForSubmission == true && orderItem.PayForFeedback == true ? (orderItem.SubmissionCost + orderItem.FeedbackCost) : orderItem.PayForSubmission == true ? orderItem.SubmissionCost : orderItem.FeedbackCost,
                        OrderPhotogDescription = orderItem.Submission.Description,
                        OrderItemTitle = orderItemTitle,
                        OrderPhotosType = (CS.Common.Constants.Enums.OrderPhotosSubmitType)orderPhotoType,
                        ListOfPhotosUrls = orderItem.Submission.IsAStoryTelling == true ? submissionPhotos : new List<string> { photoUrl },
                        OrderId = orderItem.OrderId,
                        OrderFeedbackPrice = prices.FeedbackPrice,
                        FeedbackRequesPriceText = "Feedback", //Dictionary   Order_Sumary_Feedback_Price_Text,
                        SubmissionPriceText = "Submission", // Dictionary Order_Sumary_Submission_Price_Text,
                    });
                }

            }
            return orderItemList;
        }
    }
}
