﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.MyProfile;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.ProfilePage;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.PublishedContentModels;
using ConstantValues = CS.Common.Umbraco.Constants.ConstantValues;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class MyProfilePageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetProfilePageService _getProfilePageService;
        private readonly IMemberServiceWrapper _memberServiceWrapper;

        public MyProfilePageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetProfilePageService getProfilePageService,
            IMemberServiceWrapper
            memberServiceWrapper)
        {
            _memberServiceWrapper = memberServiceWrapper;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getProfilePageService = getProfilePageService;
        }

        // GET: MyProfilePageData
        public virtual ActionResult MyProfilePage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            var currentMemberAsMemberModel = new Umbraco.Web.PublishedContentModels.Member(Members.GetCurrentMember());

            MyProfilePageModel myProfilePageModel = _getProfilePageService.Get(umbracoHelper, model.Content,
                _memberServiceWrapper.GetByEmail(umbracoHelper.UmbracoContext, User.Identity.Name), currentMemberAsMemberModel);

            var successMessageQueryStringText = Request.QueryString[
                    CSTemplate.Code.Constants.ConstantValues.Member_Profile_Update_Successful_Query_String];


            if (!String.IsNullOrEmpty(successMessageQueryStringText))
            {
                myProfilePageModel.SuccessMessage = successMessageQueryStringText;
            }

            return base.Populate(myProfilePageModel);
        }
    }
}