﻿using CSTemplate.Code.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shortlist_Photos
{
    public class ShortlistPhotosFormModel
    {
        public string ShortlistButtonText { get; set; }

        public List<_ItemPhotoModel> ListOfPhotos { get; set; }
    }
}
