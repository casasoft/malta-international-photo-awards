﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Constants;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using umbraco;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.Security;

namespace CS.Common.Umbraco.Services.Authentication
{

    public interface ILoginService
    {
        /// <summary>
        /// A method which handles login for Umbraco Members
        /// </summary>
        /// <param name="umbracoContext"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Enums.LoginResults HandleLogin(UmbracoContext umbracoContext, string username, string password);
    }

    [Service]
    public class LoginService : ILoginService
    {
        private readonly IMemberServiceWrapper _memberServiceWrapper;

        public LoginService(IMemberServiceWrapper memberServiceWrapper)
        {
            _memberServiceWrapper = memberServiceWrapper;
        }

        /// <summary>
        /// A method which handles login for Umbraco Members
        /// </summary>
        /// <param name="umbracoContext"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Enums.LoginResults HandleLogin(UmbracoContext umbracoContext, string username, string password)
        {
            var memberShipHelper = new MembershipHelper(umbracoContext);

            if (memberShipHelper.Login(username, password) == false)
            {
                var member = _memberServiceWrapper.GetByUsername(umbracoContext, username);

                if (member == null)
                {
                    return Enums.LoginResults.InvalidUsername;
                }
                else if (member.GetValue<bool>(ConstantValues.MemberIslockedOutAliasName))
                {
                    return Enums.LoginResults.MemberLockedOut;
                }
                else if (!member.GetValue<bool>(ConstantValues.MemberIsApprovedAliasName))
                {
                    return Enums.LoginResults.AccountNotYetApproved;
                }
                else
                {
                    return Enums.LoginResults.IncorrectPassword;
                }
            }
            else
            {
                return Enums.LoginResults.Successful;
            }

        }
    }
}
