﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using databaseConstants = CSTemplate.Code.Constants.DatabaseConstantValues;
using constantsEnums = CSTemplate.Code.Constants.Enums;
using CSTemplate.Code.Services.Prices;
using Umbraco.Web;

namespace CSTemplate.Code.Services.OrderItems
{

    public interface IAddNewOrderItemService
    {
        bool Add(UmbracoHelper umbracoHelper,int submissionId, int orderId, bool isStoryTelling, bool paymentForFeedback,bool paymentForSubmission);
    }

    [Service]
    public class AddNewOrderItemService : IAddNewOrderItemService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetAllPricesService _getAllPricesService;

        public AddNewOrderItemService(IGetCurrentUmbracoDatabaseService  getCurrentUmbracoDatabaseService,IGetAllPricesService getAllPricesService)
        {
            _getAllPricesService = getAllPricesService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public bool Add(UmbracoHelper umbracoHelper, int submissionId, int orderId,bool isStoryTelling, bool paymentForFeedback, bool paymentForSubmission)
        {
            if (submissionId <= 0 || orderId <= 0) return CSTemplate.Code.Constants.ConstantValues.Process_Failed;
            var feedbackCosts = _getAllPricesService.Get(umbracoHelper);
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var orderItemsTableName = databaseConstants.OrderItemsTableName;
            var orderItemsPrimaryKey = databaseConstants.OrderItems_Poco_Primary_Key_Value;
            var feedbackCost = 0m;
            
            if (paymentForFeedback)
            {
                if (isStoryTelling)
                {
                    feedbackCost = feedbackCosts.StorytellingFeedbackPrice;
                }
                else
                {
                    feedbackCost = feedbackCosts.FeedbackPrice;
                }
            }
            
        
            var normalPhotoCost = _getAllPricesService.Get(umbracoHelper).NormalPhotoPrice;
            var storyTellingPhotoCost = _getAllPricesService.Get(umbracoHelper).StoryTellingPhotoPrice;
            var submissionCost = (isStoryTelling == Convert.ToBoolean(constantsEnums.IsStoryTelling.Yes) ? storyTellingPhotoCost : normalPhotoCost);

            var newOrderItem = new OrderItemsPoco
            {
                OrderId = orderId,
                SubmissionId = submissionId,
                PayForFeedback = paymentForFeedback,
                PayForSubmission = paymentForSubmission,
                FeedbackCost = feedbackCost,
                //FeedbackCost = (paymentForFeedback == Convert.ToBoolean(constantsEnums.PaymentForFeedback.Yes) ? feedbackCost : 0),
                SubmissionCost = (paymentForSubmission== Convert.ToBoolean(constantsEnums.PaymentForFeedback.No)?0:submissionCost)
            };
            var insertedOrderItem = umbracoDb.Insert(orderItemsTableName,orderItemsPrimaryKey,newOrderItem);
            var res = Convert.ToInt32(insertedOrderItem)>0 ? Constants.ConstantValues.Process_Successful:Constants.ConstantValues.Process_Failed;
            return res;
        }
    }
}
