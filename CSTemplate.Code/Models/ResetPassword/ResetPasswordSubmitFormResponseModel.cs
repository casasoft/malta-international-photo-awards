﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.ResetPassword
{
    public class ResetPasswordSubmitFormResponseModel
    {
        public bool WasSuccess { get; set; }
        public string Message { get; set; }
    }
}
