﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.SubmitPhotoSelection
{
    public class SubmitPhotoSelectionPageModel : USNBaseViewModel
    {
        public string ExplanationText { get; set; }
        public string SubmitPhotoTitle { get; set; }
        public string SubmitPhotoText { get; set; }
        public string SubmitPhotoImgUrl { get; set; }
        public string SubmitStoryTellingTitle { get; set; }
        public string SubmitStoryTellingText { get; set; }
        public string SubmitStoryTellingImgUrl { get; set; }

        public string SubmitPhotoUrl { get; set; }
        public string SubmitStoryTellingUrl { get; set; }

        public SubmitPhotoSelectionPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
