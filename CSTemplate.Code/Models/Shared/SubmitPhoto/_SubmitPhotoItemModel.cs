﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CS.Common.Helpers;
using CSTemplate.Code.DataAnnotations;

namespace CSTemplate.Code.Models.Shared
{
    public class _SubmitPhotoItemModel
    {
        public string TitleForm { get; set; }
        public string Required { get; set; }

        [Required(ErrorMessage = "The 'Photo Title' field is required")]
        [DisplayName("Photo Title")]
        public string PhotoTitle { get; set; }



        [Required(ErrorMessage = "The 'StoryTelling Title' field is required")]
        [DisplayName("StoryTelling Title")]
        public string StoryTellingTitle { get; set; }



        public IEnumerable<SelectListItem> CategorySelectListItems { get; set; }

        [Required(ErrorMessage = "The 'Category' field is required")]
        [DisplayName("Category")]
        public string CategorySelected { get; set; }

        [Required(ErrorMessage = "Please select file.")]
        [DisplayName("Photo (only *.png, *.jpg, *.jpeg files allowed with only alphanumeric characters as a name).")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:()])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$", ErrorMessage = "Only *.png, *.jpg, *.jpeg files allowed with alphanumeric characters as a name.")]
        //[MaxFileSize(5* 1024 * 1024, ErrorMessage = "The maximum allowed is 100Mb. Please split the submission into multiple photos to reduce this size.")]
        public List<HttpPostedFileBase> SubmitPhoto { get; set; }

        [Required(ErrorMessage = "The 'Date of Submission' field is required")]
        [DisplayName("Date of Submission")]
        public DateTime PhotoDate { get; set; }

        [Required(ErrorMessage = "The 'Location' field is required")]
        [DisplayName("Location")]
        public string Location { get; set; }

        [Required(ErrorMessage = "The 'Description' field is required")]
        [DisplayName("Description")]
        public string DescriptionPhoto { get; set; }

        [Required(ErrorMessage = "The 'Description' field is required")]
        [DisplayName("Description")]
        public string DescriptionStoryTelling { get; set; }

        [DisplayName("StoryTelling")]
        public bool isAStortyTelling { get; set; }

        [Required(ErrorMessage = "The 'Picture Evaluation Request' field is required")]
        [DisplayName("Picture Evaluation Request")]
        public bool FeedBackRequest { get; set; }

    }
}
