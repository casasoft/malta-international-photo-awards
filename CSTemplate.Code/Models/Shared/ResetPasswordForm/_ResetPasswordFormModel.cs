﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared
{
    public class _ResetPasswordFormModel
    {
        public string Message { get; set; }
        public string RequiredWordText { get; set; }
        public string ButtonFormText { get; set; }

        [EmailAddress]
        [Required]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(int.MaxValue, MinimumLength = 8, ErrorMessage = "Password length must be composed at least from 8 characters")]
        public string InsertedPassword { get; set; }

        [Required]
        [System.Web.Mvc.Compare("InsertedPassword", ErrorMessage = "Passwords must match")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string InsertedConfirmPassword { get; set; }

        public string MemberRequestGuid { get; set; }

    }
}
