﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.ContactUs;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ContactUs;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ContactUsPageDataController : USNBaseController
    {
        // GET: ContactUsPageData
        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult ContactUsPage(RenderModel model)
        {
            ContactUsPageModel contactUsPageModel = new ContactUsPageModel(model.Content)
            {
                #region DummyData

                LocationMapTitle = "LOCATION MAP",
                EmbeddedMap = "<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3232.4090971859355!2d14.494954715586038!3d35.88799502621138!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x130e5ac840973c43%3A0xd7870e9db153d6de!2sCasaSoft+Ltd.!5e0!3m2!1sen!2smt!4v1533738268136\" width=\"auto\" height=\"300\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>",

                #endregion

            };

            contactUsPageModel.ContactUsForm = new _ContactUsForm()
            {
                #region DummyData
                TitleForm = "ENQUIRY FORM",
                TextForm = "We'll be more than glad to hear from you!",
                Required = "Required",
                LabelName = "Full Name",
                LabelMail = "Email Address",
                LabelNumber = "Contact Number",
                Subjet = "Subject",
                Message = "Message",
                ButtonText = "SEND"
                
                #endregion
            };
            contactUsPageModel.ContactUsInformation = new _ContactUsInformation()
            {
                Title = "CONTACT US",
                ExplanationText = "Get in touch with us from the details below"
            };

            return base.Populate(contactUsPageModel);
        }
    }
}