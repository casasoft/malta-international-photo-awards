﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CS.Common.Services;
using CS.Common.Umbraco.Services.ContentFinder;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Core;

namespace CSTemplate.Code.Services.Rss
{

    public interface IGetContentNodeFromPassedRequestService
    {
        IPublishedContent Get(UmbracoHelper umbracoHelper, HttpRequestBase Request);
    }

    [Service]
    public class GetContentNodeFromPassedRequestService : IGetContentNodeFromPassedRequestService
    {

        private readonly IContentFinderCachedMultilingualUrlsService _contentFinderCachedMultilingualUrlsService;

        public GetContentNodeFromPassedRequestService(
            IContentFinderCachedMultilingualUrlsService contentFinderCachedMultilingualUrlsService)
        {
            _contentFinderCachedMultilingualUrlsService = contentFinderCachedMultilingualUrlsService;
        }

        public IPublishedContent Get(UmbracoHelper umbracoHelper, HttpRequestBase Request)
        {
            IPublishedContent contentNode = null;

            var requestUrl = Request.RawUrl.EnsureEndsWith("/");
            var urls = _contentFinderCachedMultilingualUrlsService.Get(umbracoHelper, null);


            var partsOfUrl = requestUrl.Split(new[] { '/' }, System.StringSplitOptions.RemoveEmptyEntries);
            var urlList = new List<string>();
            for (var i = 0; i < partsOfUrl.Length; i++)
            {
                if (partsOfUrl[i] != CS.Common.Umbraco.Constants.ConstantValues.RssTitle &&
                    partsOfUrl[i] != CS.Common.Umbraco.Constants.ConstantValues.RssSocialMediaTitle)
                {
                    urlList.Add(partsOfUrl[i]);
                }
            }

            var urlFiltered = "";

            if (urlList.Count > 0)
            {
                urlFiltered =
                    string.Join("/", urlList.ToArray()).EnsureEndsWith("/").EnsureStartsWith("/");
            }
            else
            {
                urlFiltered = "/";
            }

            if (urls.Any())
            {
                var currentUrlItem = urls.FirstOrDefault(x => urlFiltered.InvariantEquals(x.Item2));

                if (currentUrlItem != null)
                {
                    contentNode = UmbracoContext.Current.ContentCache.GetById(currentUrlItem.Item1);
                }
            }
            return contentNode;
        }
    }
}
