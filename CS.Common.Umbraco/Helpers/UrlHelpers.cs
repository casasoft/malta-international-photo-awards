﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CS.Common.Umbraco.Helpers
{
    public static class UrlHelpers
    {

        public static string GetAbsoluteUrlWithTheCorrentProtocol(string relativeUrl)
        {
            var connectionType = CS.Common.Helpers.UrlHelpers.GetProtocolForCurrentPage();

            return string.Concat(connectionType, umbraco.library.RequestServerVariables("HTTP_HOST"), relativeUrl);
        }

    }
}
