﻿using CSTemplate.Code.Services.EMailNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using Umbraco.Web.Mvc;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class EmailNotificationPageDataController : SurfaceController
    {
        private readonly ISendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted _sendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        public EmailNotificationPageDataController(
            ISendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted sendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted,
            IUmbracoControllerWrapperService umbracoControllerWrapperService
            )
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _sendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted = sendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted;
        }
        // GET: EmailNotificationPageData
        //THIS IS A SCHEDULE TASK
        public virtual void EmailNotificationWithJuryFeedbackAfterAwardMarkedAsCompleted()
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            //todo: [For: Backend | 2018/08/30] The photofull page url doesn't make sense  (WrittenBy: Fatih)        			
            _sendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted.Send(umbracoHelper);
            
        }
    }
}