﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CS.Common.Helpers
{
    public static class SmtpHelpers
    {
         private static readonly Configuration configurationFile = WebConfigurationManager.OpenWebConfiguration("~/web.config");

         private static readonly MailSettingsSectionGroup mailSettings = configurationFile
            .GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;

        public static string SmtpUsername = mailSettings.Smtp.Network.UserName;

        public static string SmtpPassword = mailSettings.Smtp.Network.Password;

        public static string SmtpHost = mailSettings.Smtp.Network.Host;

        public static int SmtpPort = mailSettings.Smtp.Network.Port;

        public static string SmtpFrom = mailSettings.Smtp.From;
    }
}
