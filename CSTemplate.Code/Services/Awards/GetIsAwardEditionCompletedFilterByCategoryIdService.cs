﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.AwardCategory;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Awards
{

    public interface IGetIsAwardEditionCompletedFilterByCategoryIdService
    {
        bool Get(UmbracoHelper umbracoHelper,int categoryId);
    }

    [Service]
    public class GetIsAwardEditionCompletedFilterByCategoryIdService : IGetIsAwardEditionCompletedFilterByCategoryIdService
    {
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;

        public GetIsAwardEditionCompletedFilterByCategoryIdService(IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService)
        {
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
        }
        public bool Get(UmbracoHelper umbracoHelper, int categoryId)
        {
            if (categoryId <= 0) return false;
            var awardCategoryDetail = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, categoryId);
            //If IsAnActiveAward has true value which means the award doesn't completed yet
            //Else the award has already been completed.
            var result = awardCategoryDetail.AwardItem.IsAnActiveAward == true ? Convert.ToBoolean(Enums.AwardEditionCompleted.NotCompleted) : Convert.ToBoolean(Enums.AwardEditionCompleted.Completed);
            return result;
        }
    }
}
