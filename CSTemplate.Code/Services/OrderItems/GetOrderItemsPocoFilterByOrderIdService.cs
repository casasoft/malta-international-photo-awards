﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.OrderItems
{

    public interface IGetOrderItemsPocoFilterByOrderIdService
    {
        List<OrderItemsPoco> Get(int orderId);
    }

    [Service]
    public class GetOrderItemsPocoFilterByOrderIdService : IGetOrderItemsPocoFilterByOrderIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetOrderItemsPocoFilterByOrderIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public List<OrderItemsPoco> Get(int orderId)
        {
            var listOfOrderItems = new List<OrderItemsPoco>();
            if (orderId <= 0) return listOfOrderItems;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var orderItemsTable = DatabaseConstantValues.OrderItemsTableName;
            var orderItemsAlias = DatabaseConstantValues.aliasForOrderItemsPocoTableNameAlias;
            var orderItemsOrderIdField = DatabaseConstantValues.OrderItems_Order_Id_Field_Name;
            var sql = new Sql().Select("*").From(orderItemsTable).Where($"{orderItemsOrderIdField}={orderId}");

            listOfOrderItems = umbracoDb.Fetch<OrderItemsPoco>(sql).ToList();

            return listOfOrderItems;
        }
    }
}
