﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Database;
using dbConstantValues = CSTemplate.Code.Constants.DatabaseConstantValues;
using constantEnums = CSTemplate.Code.Constants.Enums;
namespace CSTemplate.Code.Services.Orders
{

    public interface IAddNewOrderService
    {
        /// <summary>
        /// It turns last added Order Id
        /// </summary>
        /// <returns>int</returns>
        int Add(int userId,string description);
    }

    [Service]
    public class AddNewOrderService : IAddNewOrderService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetNewOrderRefForOrderService _getNewOrderRefForOrderService;

        public AddNewOrderService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IGetNewOrderRefForOrderService
            getNewOrderRefForOrderService)
        {
            _getNewOrderRefForOrderService = getNewOrderRefForOrderService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public int Add(int userId, string description)
        {
            
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var newOrder = new OrderPoco
            {
                PaymentStatusEnumId = (int)constantEnums.PaymentStatus.NOTOK,
                Date = DateTime.Now,
                OrderRefId = _getNewOrderRefForOrderService.Get(),
                Description = description,
                UserId = userId,
            };
            var insertOrderToDatabase = umbracoDb.Insert(dbConstantValues.OrdersTableName, dbConstantValues.Orders_Poco_Primary_Key_Value, newOrder);
            var insertedOrderId = Convert.ToInt32(insertOrderToDatabase);

            return insertedOrderId;
        }
    }
}
