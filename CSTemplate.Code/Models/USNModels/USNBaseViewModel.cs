﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Components;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ContactUs;
using Umbraco.Core.Models;
using Umbraco.Web.Models;

namespace CSTemplate.Code.Models.USNModels
{
    public class USNBaseViewModel : RenderModel
    {
        //Standard Model Pass Through
        public USNBaseViewModel(IPublishedContent content) : base(content) { }

        public IPublishedContent GlobalSettings { get; set; }

        public IPublishedContent Navigation { get; set; }
        
        //public _FooterPaymentMethodModel FooterPaymentMethod { get; set; }
        public _ContactUsInformation ContactUsInformation { get; set; }

        public List<CustomComponentModel> CustomComponents { get; set; }
    }
}
