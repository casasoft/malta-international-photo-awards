//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>Reusable Components</summary>
	[PublishedContentModel("USN_AC_ReusableComponents_AN")]
	public partial class Usn_Ac_ReusableComponents_AN : PublishedContentModel, IUsn_Cmp_DisableDelete
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "USN_AC_ReusableComponents_AN";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Usn_Ac_ReusableComponents_AN(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Usn_Ac_ReusableComponents_AN, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Components
		///</summary>
		[ImplementPropertyType("components")]
		public IEnumerable<IPublishedContent> Components
		{
			get { return this.GetPropertyValue<IEnumerable<IPublishedContent>>("components"); }
		}

		///<summary>
		/// Instructions: {div class="panel-heading"}{h3 class="panel-title"}Reusable Components{i class="icon sprTree icon-repeat color-orange pull-right"}{/i}{/h3}{/div}{div class="panel-body"}{p}The node name you enter above is for your reference only and will not appear on the website.{/p}{/div}
		///</summary>
		[ImplementPropertyType("instructions")]
		public object Instructions
		{
			get { return this.GetPropertyValue("instructions"); }
		}

		///<summary>
		/// Disable delete: If "Yes" is selected, when a user attempts to delete this node they will be presented with a warning message indicating that this node cannot be deleted.
		///</summary>
		[ImplementPropertyType("disableDelete")]
		public bool DisableDelete
		{
			get { return Umbraco.Web.PublishedContentModels.Usn_Cmp_DisableDelete.GetDisableDelete(this); }
		}
	}
}
