﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.SubmitPhotoSelection;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Awards;
using CSTemplate.Code.Services.Orders;
using CSTemplate.Code.Services.SubmitPhotoSelectionPage;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class SubmitPhotoSelectionPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetSubmitPhotoSelectionService _getSubmitPhotoSelectionService;
        private readonly IControlActiveAnyAwardService _controlActiveAnyAwardService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IControlActiveStoryTellingCategoryService _controlActiveStoryTellingCategoryService;

        public SubmitPhotoSelectionPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetSubmitPhotoSelectionService getSubmitPhotoSelectionService,
            IControlActiveAnyAwardService controlActiveAnyAwardService,
            IControlActiveStoryTellingCategoryService controlActiveStoryTellingCategoryService,
            IUmbracoHelperService umbracoHelperService
            )
        {
            _controlActiveStoryTellingCategoryService = controlActiveStoryTellingCategoryService;
            _umbracoHelperService = umbracoHelperService;
            _controlActiveAnyAwardService = controlActiveAnyAwardService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getSubmitPhotoSelectionService = getSubmitPhotoSelectionService;
        }
        // GET: SubmitPhotoSelectionPageData
        //[OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult SubmitPhotoSelectionPage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            var currentMemberId = Members.GetCurrentMemberId();
           
            bool doesExist = _controlActiveAnyAwardService.Control(umbracoHelper);
            bool activeStoryTellingCategory = _controlActiveStoryTellingCategoryService.Control(umbracoHelper);

            if (!doesExist)
            {
                var pageNotFound = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, CSTemplate.Code.Constants.ConstantValues.Competition_Not_Active_Data_Document_Type_Alias);
                return new RedirectToUmbracoPageResult(pageNotFound);
            }

            if(!activeStoryTellingCategory)
            {
                return Redirect(ConstantValues.Submit_Photo_Page_Link);
            }

            


            SubmitPhotoSelectionPageModel submitPhotoSelectionPageModel =
                _getSubmitPhotoSelectionService.Get(umbracoHelper, model.Content);

            return base.Populate(submitPhotoSelectionPageModel);
        }
    }
}