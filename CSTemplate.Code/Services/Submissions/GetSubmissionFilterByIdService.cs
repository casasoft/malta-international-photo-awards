﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Database;
using Umbraco.Web;
using databaseConstants = CSTemplate.Code.Constants.DatabaseConstantValues;
namespace CSTemplate.Code.Services.Submissions
{

    public interface IGetSubmissionFilterByIdService
    {
        _SubmitPhotoItemModel Get(UmbracoHelper umbracoHelper,int submissionId);
    }

    [Service]
    public class GetSubmissionFilterByIdService : IGetSubmissionFilterByIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetCategoryFilterByCategoryIdService _getCategoryFilterByCategoryIdService;

        public GetSubmissionFilterByIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService, IGetCategoryFilterByCategoryIdService getCategoryFilterByCategoryIdService)
        {
            _getCategoryFilterByCategoryIdService = getCategoryFilterByCategoryIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public _SubmitPhotoItemModel Get(UmbracoHelper umbracoHelper, int submissionId)
        {

            if (submissionId <= 0) return null;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var submissionTableName = databaseConstants.SubmissionsTableName;
            var submissionPrimaryKeyField = databaseConstants.Submissions_Poco_Primary_Key_Value;

            var gettedSubmission = umbracoDb.Query<SubmissionPoco>($@"
                    SELECT *
                        FROM {submissionTableName}
                            WHERE {submissionPrimaryKeyField}={submissionId}
        ").FirstOrDefault();
            var returnOfSubmitPhotoItemModel = new _SubmitPhotoItemModel
            {
                isAStortyTelling = gettedSubmission.IsAStoryTelling,
                DescriptionPhoto = gettedSubmission.Description,
                FeedBackRequest = gettedSubmission.FeedbackRequest,
                Location = gettedSubmission.Location,
                PhotoDate = gettedSubmission.Date,
                CategorySelectListItems = _getCategoryFilterByCategoryIdService.Get(umbracoHelper,gettedSubmission.CategoryId)
            };
            return returnOfSubmitPhotoItemModel;
        }
    }
}
