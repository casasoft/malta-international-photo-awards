//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	// Mixin content Type 1122 with alias "USN_CMP_CustomComponentClasses"
	/// <summary>Custom Component Classes</summary>
	public partial interface IUsn_Cmp_CustomComponentClasses : IPublishedContent
	{
		/// <summary>Custom component classes</summary>
		string CustomComponentClasses { get; }
	}

	/// <summary>Custom Component Classes</summary>
	[PublishedContentModel("USN_CMP_CustomComponentClasses")]
	public partial class Usn_Cmp_CustomComponentClasses : PublishedContentModel, IUsn_Cmp_CustomComponentClasses
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "USN_CMP_CustomComponentClasses";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Usn_Cmp_CustomComponentClasses(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Usn_Cmp_CustomComponentClasses, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Custom component classes: Add your own touch with specific classes. Leave a space between each class e.g. custom-class another-class
		///</summary>
		[ImplementPropertyType("customComponentClasses")]
		public string CustomComponentClasses
		{
			get { return GetCustomComponentClasses(this); }
		}

		/// <summary>Static getter for Custom component classes</summary>
		public static string GetCustomComponentClasses(IUsn_Cmp_CustomComponentClasses that) { return that.GetPropertyValue<string>("customComponentClasses"); }
	}
}
