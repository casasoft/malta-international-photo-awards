﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class USNBaseController : Umbraco.Web.Mvc.RenderMvcController
    {
       
        protected ActionResult Populate(USNBaseViewModel model)
        {
            IPublishedContent homeNode = model.Content.Site();

            if (homeNode.IsDocumentType("USNHomepage"))
            {
                IPublishedContent globalSettings = homeNode.GetPropertyValue<IPublishedContent>("websiteConfigurationNode").Children.Where(x => x.IsDocumentType("USNGlobalSettings")).First();
                IPublishedContent navigation = homeNode.GetPropertyValue<IPublishedContent>("websiteConfigurationNode").Children.Where(x => x.IsDocumentType("USNNavigation")).First();

                model.GlobalSettings = globalSettings;
                model.Navigation = navigation;

                return base.Index(model);
            }
            else
                return base.Index(model);
        }

        //[OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public override ActionResult Index(RenderModel model)
        {
            IPublishedContent homeNode = model.Content.Site();

            if (homeNode.IsDocumentType("USNHomepage"))
            {
                USNBaseViewModel customModel = new USNBaseViewModel(model.Content);
                return Populate(customModel);
            }
            else
                return base.Index(model);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string cacheKey = ConstantValues.Cache_Key_Pages;

            if (controllerName.Equals(ConstantValues.Cache_Key_Submission_Full_Page))
            {
                cacheKey = ConstantValues.Cache_Key_Submission_Full_Page;
            }

            try
            {
                object cache = HttpContext.Cache[cacheKey];
                if (cache == null)
                {
                    HttpContext.Cache[cacheKey] = DateTime.UtcNow.ToString();
                }

                Response.AddCacheItemDependency(cacheKey);
            }
            catch (Exception ex)
            {
                throw new SystemException(ex.Message);
            }
        }
    }
}