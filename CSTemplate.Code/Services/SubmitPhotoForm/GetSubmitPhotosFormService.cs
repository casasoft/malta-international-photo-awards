﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.AwardCategory;
using Lucene.Net.Search;
using Umbraco.Core.Models;
using Umbraco.Web;
using ConstantValues = CS.Common.Umbraco.Constants.ConstantValues;

namespace CSTemplate.Code.Services.SubmitPhoto
{
    public interface IGetSubmitPhotosFormService
    {
        _SubmitPhotoFormModel Get(UmbracoHelper umbracoHelper, IPublishedContent content);
    }
    [Service]
    public class GetSubmitPhotosFormService : IGetSubmitPhotosFormService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetActiveAwardCategoriesService _getAwardCategoriesService;

        public GetSubmitPhotosFormService(
            IUmbracoHelperService umbracoHelperService,
            IGetActiveAwardCategoriesService getAwardCategoriesService
            )
        {
            _getAwardCategoriesService = getAwardCategoriesService;
            _umbracoHelperService = umbracoHelperService;
        }

        public _SubmitPhotoFormModel Get(UmbracoHelper umbracoHelper, IPublishedContent content)
        {
            var termsAndConditionsRawText =
                _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    Enums.DictionaryKey.Dictionary_Form_Terms_And_Conditions_Text);
            var termsAndConditionsUrl = _umbracoHelperService.TypedContent(umbracoHelper, ConstantValues.Terms_And_Conditions_Page_Id).Url;

            var termsAndConditionsFormatted = String.Format(termsAndConditionsRawText, termsAndConditionsUrl);

            var requiredText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Dictionary_Form_Required_Text);

            var buttonFromText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_Photo_Button_Form_Text);
            
            var PhotoFormTitle = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_Photo_Title_Form_Text);

            var explanationFormText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_Photo_Explanation_From_Text);
            var uploadInformationText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
              Enums.DictionaryKey.Upload_Information_Text);

            _SubmitPhotoFormModel submitPhotoFormModel = new _SubmitPhotoFormModel( )
            {
                NumberOfPhotos = GetNumberOfPhotosSelectList(umbracoHelper),
                Required = requiredText,
                ButtonFormText =  buttonFromText,
                TermsAndConditionsText = termsAndConditionsFormatted,
                UploadInformationText = uploadInformationText,
                ExplanationFormText = explanationFormText,
                
                ListOfSubmitPhotos = new List<_SubmitPhotoItemModel>()
                {
                    new _SubmitPhotoItemModel()
                    {

                        TitleForm = PhotoFormTitle,
                        Required = requiredText,                      
                       // CategorySelected = new SelectListItem(),
                        CategorySelectListItems =  _getAwardCategoriesService.Get(umbracoHelper)

                    }
                }
                   
            };
            return submitPhotoFormModel;
        }

        private List<SelectListItem> GetNumberOfPhotosSelectList(
            //This will be used once we need to retrieve the max and min photos from umbraco
            UmbracoHelper umbracoHelper)
        {
            //todo Retrieve from Umbraco
            var maxPhotos = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submission_Maximum_Photo_Amount);
            var minPhotos = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submission_Minimum_Photo_Amount);
            //----------------- 
            int minPhotoAmount = Convert.ToInt32(minPhotos);
            int maxPhotoAmount = Convert.ToInt32(maxPhotos);

            var numberOfPhotos = new List<SelectListItem>();
            for (var i = minPhotoAmount; i <= maxPhotoAmount; i++)
            {
                var selectListItem = new SelectListItem()
                {
                    Text = i.ToString(),
                    Value = i.ToString(),
                };
                if (i == minPhotoAmount)
                {
                    selectListItem.Selected = true;
                }
                numberOfPhotos.Add(selectListItem);
            }
            return numberOfPhotos;
        }
    }
}
