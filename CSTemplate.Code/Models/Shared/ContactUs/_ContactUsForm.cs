﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebPages.Html;

namespace CSTemplate.Code.Models.Shared
{
    public class _ContactUsForm
    {
        public string TitleForm { get; set; }
        public string TextForm { get; set; }
        public string Required { get; set; }
        public string LabelName { get; set; }
        public string LabelMail { get; set; }
        public string LabelNumber { get; set; }
        public string Subjet { get; set; }
        //public string Message { get; set; }
        public string ButtonText { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        [DisplayName("Subject")]
        public List<SelectListItem> SubjectSelectListItems { get; set; }
        [Required]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        public string Email { get; set; }
        [Required]
        public string Telephone { get; set; }

        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
    }
}
