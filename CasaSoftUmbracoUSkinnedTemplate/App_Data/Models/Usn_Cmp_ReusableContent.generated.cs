//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	// Mixin content Type 1162 with alias "USN_CMP_ReusableContent"
	/// <summary>Reusable Content</summary>
	public partial interface IUsn_Cmp_ReusableContent : IPublishedContent
	{
		/// <summary>Bottom components</summary>
		IEnumerable<IPublishedContent> BottomComponents { get; }

		/// <summary>Override default bottom components</summary>
		bool OverrideDefaultBottomComponents { get; }

		/// <summary>Override default pods</summary>
		bool OverrideDefaultPods { get; }

		/// <summary>Override default top components</summary>
		bool OverrideDefaultTopComponents { get; }

		/// <summary>Pods</summary>
		IEnumerable<IPublishedContent> PagePods { get; }

		/// <summary>Top components</summary>
		IEnumerable<IPublishedContent> TopComponents { get; }
	}

	/// <summary>Reusable Content</summary>
	[PublishedContentModel("USN_CMP_ReusableContent")]
	public partial class Usn_Cmp_ReusableContent : PublishedContentModel, IUsn_Cmp_ReusableContent
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "USN_CMP_ReusableContent";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Usn_Cmp_ReusableContent(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Usn_Cmp_ReusableContent, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Bottom components: Select the components you would like to appear on this page. The components selected will only be displayed if "Yes" is selected above.
		///</summary>
		[ImplementPropertyType("bottomComponents")]
		public IEnumerable<IPublishedContent> BottomComponents
		{
			get { return GetBottomComponents(this); }
		}

		/// <summary>Static getter for Bottom components</summary>
		public static IEnumerable<IPublishedContent> GetBottomComponents(IUsn_Cmp_ReusableContent that) { return that.GetPropertyValue<IEnumerable<IPublishedContent>>("bottomComponents"); }

		///<summary>
		/// Override default bottom components: Select "Yes" to override the default components selected within "Global Settings".
		///</summary>
		[ImplementPropertyType("overrideDefaultBottomComponents")]
		public bool OverrideDefaultBottomComponents
		{
			get { return GetOverrideDefaultBottomComponents(this); }
		}

		/// <summary>Static getter for Override default bottom components</summary>
		public static bool GetOverrideDefaultBottomComponents(IUsn_Cmp_ReusableContent that) { return that.GetPropertyValue<bool>("overrideDefaultBottomComponents"); }

		///<summary>
		/// Override default pods: Select "Yes" to override the default pods selected within "Global Settings".
		///</summary>
		[ImplementPropertyType("overrideDefaultPods")]
		public bool OverrideDefaultPods
		{
			get { return GetOverrideDefaultPods(this); }
		}

		/// <summary>Static getter for Override default pods</summary>
		public static bool GetOverrideDefaultPods(IUsn_Cmp_ReusableContent that) { return that.GetPropertyValue<bool>("overrideDefaultPods"); }

		///<summary>
		/// Override default top components: Select "Yes" to override the default components selected within "Global Settings".
		///</summary>
		[ImplementPropertyType("overrideDefaultTopComponents")]
		public bool OverrideDefaultTopComponents
		{
			get { return GetOverrideDefaultTopComponents(this); }
		}

		/// <summary>Static getter for Override default top components</summary>
		public static bool GetOverrideDefaultTopComponents(IUsn_Cmp_ReusableContent that) { return that.GetPropertyValue<bool>("overrideDefaultTopComponents"); }

		///<summary>
		/// Pods: Select the pods you would like to appear on this page. The pods selected will only be displayed if "Yes" is selected above.
		///</summary>
		[ImplementPropertyType("pagePods")]
		public IEnumerable<IPublishedContent> PagePods
		{
			get { return GetPagePods(this); }
		}

		/// <summary>Static getter for Pods</summary>
		public static IEnumerable<IPublishedContent> GetPagePods(IUsn_Cmp_ReusableContent that) { return that.GetPropertyValue<IEnumerable<IPublishedContent>>("pagePods"); }

		///<summary>
		/// Top components: Select the components you would like to appear on this page. The components selected will only be displayed if "Yes" is selected above.
		///</summary>
		[ImplementPropertyType("topComponents")]
		public IEnumerable<IPublishedContent> TopComponents
		{
			get { return GetTopComponents(this); }
		}

		/// <summary>Static getter for Top components</summary>
		public static IEnumerable<IPublishedContent> GetTopComponents(IUsn_Cmp_ReusableContent that) { return that.GetPropertyValue<IEnumerable<IPublishedContent>>("topComponents"); }
	}
}
