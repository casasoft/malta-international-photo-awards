﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using umbraco.NodeFactory;
using Umbraco.Core.Models;

namespace CS.Common.Umbraco.Models.Multilingual
{
    public class MultilingualUrlTrackerDataModel
    {
        public MultilingualUrlTrackerDataModel()
        {
            OldContentMultilingualUrls = new Dictionary<CultureInfo, string>();
            CurrentContentChildren = new List<MultilingualUrlTrackerDataModel>();
        }

        public IContent CurrentContent { get; set; }
        public int OldContentDomainRootNodeId { get; set; }
        public Dictionary<CultureInfo, string> OldContentMultilingualUrls { get; set; }
        public List<MultilingualUrlTrackerDataModel> CurrentContentChildren { get; set; }
    }
}