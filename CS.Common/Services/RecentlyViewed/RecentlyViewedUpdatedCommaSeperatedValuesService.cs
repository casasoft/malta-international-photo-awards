﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Helpers;
using CS.Common.Services;

namespace CS.Common.Services.RecentlyViewed
{

    public interface IRecentlyViewedUpdatedCommaSeperatedValuesService
    {
        /// <summary>
        /// A method which will update the comma seperated values with the new value to be added. 
        /// </summary>
        /// <param name="existingCommaSeperatedValues"></param>
        /// <param name="newValueToBeAdded"></param>
        /// <param name="amountOfValues"></param>
        /// <returns></returns>
        string RecentlyViewedUpdatedCommaSeperatedValues(string existingCommaSeperatedValues, string newValueToBeAdded, int amountOfValues);
    }

    [Service]
    public class RecentlyViewedUpdatedCommaSeperatedValuesService : IRecentlyViewedUpdatedCommaSeperatedValuesService
    {
        public RecentlyViewedUpdatedCommaSeperatedValuesService() { }

        /// <summary>
        /// A method which will update the comma seperated values with the new value to be added. 
        /// </summary>
        /// <param name="existingCommaSeperatedValues"></param>
        /// <param name="newValueToBeAdded"></param>
        /// <param name="amountOfValues"></param>
        /// <returns></returns>
        public string RecentlyViewedUpdatedCommaSeperatedValues(string existingCommaSeperatedValues, string newValueToBeAdded, int amountOfValues)
        {
            var commaSeperatedValuesAsList = existingCommaSeperatedValues.Split(',').ToList();

            if (!commaSeperatedValuesAsList.Contains(newValueToBeAdded))
            {
                if (commaSeperatedValuesAsList.Count < amountOfValues)
                {
                    //it is a new value
                    //add it as the first one in the list
                    commaSeperatedValuesAsList.Insert(0, newValueToBeAdded);
                }
                else
                {
                    //there is already 5 elements in the list
                    //remove the last one
                    commaSeperatedValuesAsList.RemoveAt(commaSeperatedValuesAsList.Count - 1);
                    commaSeperatedValuesAsList.Insert(0, newValueToBeAdded);
                }
            }
            else
            {
                var indexOftheExistingValue = commaSeperatedValuesAsList.IndexOf(newValueToBeAdded);

                //value is already in the list
                commaSeperatedValuesAsList.MoveItemAtIndexToFront(indexOftheExistingValue);
            }

            return string.Join(",", commaSeperatedValuesAsList.ToArray());
        }
    }
}
