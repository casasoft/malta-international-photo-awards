﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared
{
    public class _OrderPhotosModel
    {
        public int OrderId { get; set; }
        public List<string> ListOfPhotosUrls { get; set; }
        public DateTime OrderPhotoDate { get; set; }
        public string OrderPhotoLocation { get; set; }
        public string OrderCategory { get; set; }
        public string OrderPhotogDescription { get; set; }
        public string OrderItemTitle { get; set; }
        public decimal OrderPhotoPrice { get; set; }
        public decimal OrderFeedbackPrice { get; set; }

        public string FeedbackRequesPriceText { get; set; }
        public string SubmissionPriceText { get; set; }

        public string SubmitTypeFeedbackText { get; set;}
        public string SubmitTypeSubmitAndFeedbackText { get; set; }
        public string SubmitTypeSubmitText { get; set; }

        public CS.Common.Constants.Enums.OrderPhotosSubmitType OrderPhotosType { get; set; }
    }
}
