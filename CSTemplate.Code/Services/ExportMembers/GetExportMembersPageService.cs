﻿using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.ExportMembers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.ExportMembers
{
    public interface IGetExportMembersPageService
    {
        ExportMembersPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content);
    }
    [Service]
    public class GetExportMembersPageService : IGetExportMembersPageService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        public GetExportMembersPageService(
            IUmbracoHelperService umbracoHelperService
            )
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public ExportMembersPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content)
        {
            var explanationText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Export_Members_Explanation_Text);
            var buttonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Export_Members_Button_Text);
            ExportMembersPageModel exportMembersPageModel = new ExportMembersPageModel(content)
            {
                ExplanationText = explanationText,
                ButtonText = buttonText,
            };
            return exportMembersPageModel;
        }
    }
}
