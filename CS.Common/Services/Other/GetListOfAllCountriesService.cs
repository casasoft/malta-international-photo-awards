﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;

namespace CS.Common.Services.Other
{

    public interface IGetListOfAllCountriesService
    {
        List<string> GetListOfAllCountries();
    }

    [Service]
    public class GetListOfAllCountriesService : IGetListOfAllCountriesService
    {
        public GetListOfAllCountriesService()
        {

        }

        public List<string> GetListOfAllCountries()
        {
            return CultureInfo.GetCultures(CultureTypes.SpecificCultures).Select(ObjCultureInfo => new RegionInfo(ObjCultureInfo.Name)).Select(objRegionInfo => objRegionInfo.EnglishName).ToList();
        }

    }
}
