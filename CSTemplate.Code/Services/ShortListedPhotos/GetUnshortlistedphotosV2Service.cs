﻿using CS.Common.Services;
using CSTemplate.Code.Models.Shortlist_Photos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CSTemplate.Code.Constants.Enums;
using CSTemplate.Code.Constants;
using Umbraco.Web;
using Umbraco.Core.Models;
using CSTemplate.Code.Services.Common;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Services.Submissions;
using CSTemplate.Code.Models.Shared;

namespace CSTemplate.Code.Services.ShortListedPhotos
{
    public interface IGetUnshortlistedphotosV2Service
    {
        List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, SubmissionFilters shortListFilters, ShortListedStatusFilter shortListedStatus, string searchKey, int awardId, int awardCategoryId);
    }
    [Service]
    public class GetUnshortlistedphotosV2Service : IGetUnshortlistedphotosV2Service
    {
        private readonly IGetSubmissionsByKeywordsPaidSubmissionService _getSubmissionsByKeywordsPaidSubmissionService;
        private readonly IGetSubmissionsFilterByShortListedStatusService _getSubmissionsFilterByShortListedStatusService;
        private readonly IGetUnShortListedPhotosListingService _getUnShortListedPhotosListingService;
        private readonly IGetSubmissionsFilterByAwardEditionIdService _getSubmissionsFilterByAwardEditionIdService;

        public GetUnshortlistedphotosV2Service(
            IGetSubmissionsByKeywordsPaidSubmissionService getSubmissionsByKeywordsPaidSubmissionService,
            IGetSubmissionsFilterByShortListedStatusService getSubmissionsFilterByShortListedStatusService,
            IGetUnShortListedPhotosListingService getUnShortListedPhotosListingService,
            IGetSubmissionsFilterByAwardEditionIdService getSubmissionsFilterByAwardEditionIdService
          
            )
        {
           _getSubmissionsByKeywordsPaidSubmissionService = getSubmissionsByKeywordsPaidSubmissionService;
           _getSubmissionsFilterByShortListedStatusService = getSubmissionsFilterByShortListedStatusService;
            this._getUnShortListedPhotosListingService = getUnShortListedPhotosListingService;
            this._getSubmissionsFilterByAwardEditionIdService = getSubmissionsFilterByAwardEditionIdService;
        }

        public List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, SubmissionFilters shortListFilters, ShortListedStatusFilter shortListedStatusFilter, string searchKey, int awardId, int awardCategoryId)
        {
            
            var listItemOfPhoto = new List<_ItemPhotoModel>();
            
            if (!string.IsNullOrEmpty(searchKey))
            {
                listItemOfPhoto = _getSubmissionsByKeywordsPaidSubmissionService.Get(umbracoHelper, searchKey, shortListFilters, shortListedStatusFilter,awardId, awardCategoryId);
            }
            else
            {
                if ((int)shortListedStatusFilter>=0 ||(int)shortListFilters>=0)
                {
                   
                    if (awardId>0)
                    {
                        listItemOfPhoto = _getSubmissionsFilterByAwardEditionIdService.Get(umbracoHelper, awardId, awardCategoryId, shortListFilters, shortListedStatusFilter);
                    }
                    else
                    {
                        listItemOfPhoto = _getSubmissionsFilterByShortListedStatusService.Get(umbracoHelper, shortListFilters, shortListedStatusFilter);
                    }
                    

                }
                else if (awardId>0)
                {
                    listItemOfPhoto = _getSubmissionsFilterByAwardEditionIdService.Get(umbracoHelper, awardId, awardCategoryId, shortListFilters,shortListedStatusFilter);
                }
                else
                {
                    listItemOfPhoto = _getUnShortListedPhotosListingService.Get(umbracoHelper);
                }
               
            }
            return listItemOfPhoto;
        }

        
    }
}
