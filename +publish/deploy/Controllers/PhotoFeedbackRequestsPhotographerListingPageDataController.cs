﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Photo_Feedback_Requests;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Feedbacks;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class PhotoFeedbackRequestsPhotographerListingPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        private readonly IGetPhotoFeedbackRequestsPhotographerFilterByPhotographerIdService _getPhotoFeedbackRequestsPhotographerFilterByPhotographerIdService;
        //private readonly IGetPagerModelService _getPagerModelService;

        public PhotoFeedbackRequestsPhotographerListingPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetPhotoFeedbackRequestsPhotographerFilterByPhotographerIdService getPhotoFeedbackRequestsPhotographerFilterByPhotographerIdService
            )
        {
            _getPhotoFeedbackRequestsPhotographerFilterByPhotographerIdService = getPhotoFeedbackRequestsPhotographerFilterByPhotographerIdService;
            //_getPagerModelService = getPagerModelService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
        }
        public virtual ActionResult PhotoFeedbackRequestsPhotographerListingPage(RenderModel model, string page)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            int pageNumber = 1;
            if (!string.IsNullOrEmpty(Request.QueryString["page"]) && Request.QueryString["page"] != null)
            {
                pageNumber = Convert.ToInt32(Request.QueryString["page"]);
            }
            var currentMember = Members.GetCurrentMemberId();
            PhotoFeedbackRequestsPhotographerListingPageModel photoFeedbackRequestsPhotographerListingPageModel = _getPhotoFeedbackRequestsPhotographerFilterByPhotographerIdService.Get(umbracoHelper,currentMember,model.Content,pageNumber);

            #region DummyData

            //new PhotoFeedbackRequestsPhotographerListingPageModel(model.Content);

            // DateTime currentTime = DateTime.Now;



            // int itemTotalAmounts = 40;
            // //how many item to show on the page
            // photoFeedbackRequestsPhotographerListingPageModel.ItemsPerPage = 6;
            // //total number of listing items.
            // photoFeedbackRequestsPhotographerListingPageModel.ItemsTotalAmount = itemTotalAmounts;

            // var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper, photoFeedbackRequestsPhotographerListingPageModel.ItemsPerPage, photoFeedbackRequestsPhotographerListingPageModel.ItemsTotalAmount);
            // photoFeedbackRequestsPhotographerListingPageModel.ListingItemsAsMasonryPartial = new _ListingItemsAsMasonryPartialModel()
            // {
            //     ListOfPhotos = new List<_ItemPhotoModel>()
            //     {
            //         #region DummyData

            //     new _ItemPhotoModel()
            //     {
            //         ItemPhotoDate = currentTime,
            //         ItemPhotoImgURL = "/dist/assets/img/1.jpg",
            //         ItemPhotoAward = "Landscape photography Award",
            //         ItemPhotoLocation = "Vence, Italy",
            //         ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //         ItemPhotoCategory = "nature",
            //         PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //         SubmissionStatusTitleText = "Status: ",
            //         CurrentSubmissionStatus = "Waiting for Feedback",
            //         SubmissionId = "62",
            //         ItemPhotoTitle = "Test title Malta",

            //     },
            //     new _ItemPhotoModel()
            //     {
            //         ItemPhotoDate = currentTime,
            //         ItemPhotoImgURL = "/dist/assets/img/2.jpg",
            //         ItemPhotoAward = "Landscape photography Award",
            //         ItemPhotoLocation = "Vence, Italy",
            //         ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //         ItemPhotoCategory = "nature",
            //         PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test2/",
            //         SubmissionStatusTitleText = "Status: ",
            //         CurrentSubmissionStatus = "Waiting for Feedback",
            //         SubmissionId = "63",
            //         ItemPhotoTitle = "Test title Malta",
            //     },
            //     new _ItemPhotoModel()
            //     {
            //         ItemPhotoDate = currentTime,
            //         ItemPhotoImgURL = "/dist/assets/img/3.jpg",
            //         ItemPhotoAward = "No awards",
            //         ItemPhotoLocation = "Vence, Italy",
            //         ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //         ItemPhotoCategory = "nature",
            //         PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //         SubmissionStatusTitleText = "Status: ",
            //         CurrentSubmissionStatus = "Waiting for Feedback",
            //         SubmissionId = "64",
            //         ItemPhotoTitle = "Test title Malta",
            //     },
            //     new _ItemPhotoModel()
            //     {
            //         ItemPhotoDate = currentTime,
            //         ItemPhotoImgURL = "/dist/assets/img/4.jpg",
            //         ItemPhotoAward = "No awards",
            //         ItemPhotoLocation = "Vence, Italy",
            //         ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //         ItemPhotoCategory = "nature",
            //         PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //         SubmissionStatusTitleText = "Status: ",
            //         CurrentSubmissionStatus = "Waiting for Feedback",
            //         SubmissionId = "68",
            //         ItemPhotoTitle = "Test title Malta",
            //     },
            //     new _ItemPhotoModel()
            //     {
            //         ItemPhotoDate = currentTime,
            //         ItemPhotoImgURL = "/dist/assets/img/5.jpg",
            //         ItemPhotoAward = "Landscape photography Award",
            //         ItemPhotoLocation = "Vence, Italy",
            //         ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //         ItemPhotoCategory = "war",
            //         PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //         SubmissionStatusTitleText = "Status: ",
            //         CurrentSubmissionStatus = "Waiting for Feedback",
            //         SubmissionId = "69",
            //         ItemPhotoTitle = "TestWith sp3c1al $ign$&characters url",
            //     },
            //     new _ItemPhotoModel()
            //     {
            //         ItemPhotoDate = currentTime,
            //         ItemPhotoImgURL = "/dist/assets/img/1.jpg",
            //         ItemPhotoAward = "No awards",
            //         ItemPhotoLocation = "Vence, Italy",
            //         ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //         ItemPhotoCategory = "nature",
            //         PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //         SubmissionStatusTitleText = "Status: ",
            //         CurrentSubmissionStatus = "Waiting for Feedback",
            //         SubmissionId = "69",
            //         ItemPhotoTitle = "TestWith sp3c1al $ign$&characters url",

            //     },
            //     new _ItemPhotoModel()
            //     {
            //         ItemPhotoDate = currentTime,
            //         ItemPhotoImgURL = "/dist/assets/img/2.jpg",

            //         ItemPhotoAward = "No awards",
            //         ItemPhotoLocation = "Vence, Italy",
            //         ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //         ItemPhotoCategory = "nature",
            //         PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //         SubmissionStatusTitleText = "Status: ",
            //         CurrentSubmissionStatus = "Waiting for Feedback",
            //         SubmissionId = "64",
            //         ItemPhotoTitle = "TestWith sp3c1al $ign$&characters url",
            //     },
            //     new _ItemPhotoModel()
            //     {
            //         ItemPhotoDate = currentTime,
            //         ItemPhotoImgURL = "/dist/assets/img/3.jpg",

            //         ItemPhotoAward = "No awards",
            //         ItemPhotoLocation = "Vence, Italy",
            //         ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //         ItemPhotoCategory = "nature",
            //         PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //         SubmissionStatusTitleText = "Status: ",
            //         CurrentSubmissionStatus = "Waiting for Feedback",
            //         SubmissionId = "69",
            //         ItemPhotoTitle = "$%esting",
            //     },
            //     #endregion
            //     },

            //     Pagination = pagerModel

            // };

            #endregion


            return base.Populate(photoFeedbackRequestsPhotographerListingPageModel);
        }
    }
}