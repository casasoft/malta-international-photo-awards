﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace CS.Common.Services.Cache
{
    public interface ICacheService
    {
        object AddItemToCache(string key,
            object value,
            CacheDependency dependencies,
            DateTime absoluteExpiration,
            TimeSpan slidingExpiration,
            CacheItemPriority priority,
            CacheItemRemovedCallback onRemoveCallback);

        T GetItemFromCache<T>(string key);
    }

    [Service]
    public class CacheService : ICacheService
    {
        public object AddItemToCache(string key,
            object value,
            CacheDependency dependencies,
            DateTime absoluteExpiration,
            TimeSpan slidingExpiration,
            CacheItemPriority priority,
            CacheItemRemovedCallback onRemoveCallback)
        {
            return HttpContext.Current.Cache.Add(key,
                value,
                dependencies,
                absoluteExpiration,
                slidingExpiration,
                priority,
                onRemoveCallback);
        }

        public T GetItemFromCache<T>(string key)
        {
            return (T) HttpContext.Current.Cache[key];
        }

    }
}
