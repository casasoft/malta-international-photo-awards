﻿angular.module("umbraco").controller("SubmissionPhotographer",
    function ($scope, $http) {
        var id = $scope.property.value;
        var url = "backoffice/User/UserApi/GetUserFullName/" + id;
        $http.get(url).then(function (response) {
            $scope.person = response.data;

        });
    });