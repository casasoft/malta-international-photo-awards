﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Orders
{

    public interface IGetOrderPocoFilterByOrderIdService
    {
        OrderPoco Get(int orderId);
    }

    [Service]
    public class GetOrderPocoFilterByOrderIdService : IGetOrderPocoFilterByOrderIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetOrderPocoFilterByOrderIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public OrderPoco Get(int orderId)
        {
            if (orderId <= 0) return new OrderPoco();
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var orderTableName = Constants.DatabaseConstantValues.OrdersTableName;
            var orderIdField = Constants.DatabaseConstantValues.Orders_Poco_Primary_Key_Value;

            var select = "*";
            var whereCondition = $"{orderIdField} = {orderId}";
            var order = new Sql().Select(select).From(orderTableName).Where(whereCondition);
            var orderResult = umbracoDb.Fetch<OrderPoco>(order).FirstOrDefault();

            return orderResult;
        }
    }
}
