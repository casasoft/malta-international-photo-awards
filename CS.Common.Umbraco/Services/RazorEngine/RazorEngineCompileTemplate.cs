﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Helpers;
using CS.Common.Services;

namespace CS.Common.Umbraco.Services.RazorEngine
{

    public interface IRazorEngineCompileTemplate
    {
        string ComplieRazorTemplateFromPassedString(string template, string templateKey, object model);
    }

    [Service]
    public class RazorEngineCompileTemplate : IRazorEngineCompileTemplate
    {

        public string ComplieRazorTemplateFromPassedString(string template, string templateKey, object model)
        {
            try
            {
                var result = RazorHelpers.CompileAndEvaluateRazorTemplate(template,
                    templateKey,
                    model);

                var decoded = System.Web.HttpUtility.HtmlDecode(result);

                return decoded;
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

    }
}
