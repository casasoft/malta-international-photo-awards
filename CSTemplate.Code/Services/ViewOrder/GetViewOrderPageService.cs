﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.PaymentButtons;
using CSTemplate.Code.Models.ViewOrder;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Order;
using CSTemplate.Code.Services.OrderItems;
using CSTemplate.Code.Services.Orders;
using CSTemplate.Code.Services.Payment.Bilderlings;
using CSTemplate.Code.Services.Users;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.ViewOrder
{
    public interface IGetViewOrderPageService
    {
        ViewOrderPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content, int orderId, int userId);
    }
    [Service]
    public class GetViewOrderPageService : IGetViewOrderPageService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetOrderItemsFilterByOrderIdOrUserIdService _getOrderItemsFilterByOrderIdService;
        private readonly IGetOrderTotalCalculateService _getOrderTotalCalculateService;
        private readonly IGetOrderDetailsFilterByOrderId _getOrderDetailsFilterByOrderId;
        private readonly IGetIsAStoryTellingOrderService _getIsAStoryTellingOrderService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;
        private readonly IGetBilderlingsPaymentModelService _getBilderlingsPaymentModelService;
        private readonly IGetOrderByOrderIdService _getOrderByOrderIdService;
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetNewOrderRefForOrderService _getNewOrderRefForOrderService;

        public GetViewOrderPageService(
            IUmbracoHelperService umbracoHelperService,
            IGetOrderItemsFilterByOrderIdOrUserIdService getOrderItemsFilterByOrderIdService,
            IGetOrderTotalCalculateService getOrderTotalCalculateService,
            IGetOrderDetailsFilterByOrderId getOrderDetailsFilterByOrderId,
            IGetIsAStoryTellingOrderService getIsAStoryTellingOrderService,
            IGetUserFilterByUserIdService getUserFilterByUserIdService,
            IGetBilderlingsPaymentModelService getBilderlingsPaymentModelService,
            IGetOrderByOrderIdService
            getOrderByOrderIdService,
            IGetCurrentUmbracoDatabaseService
            getCurrentUmbracoDatabaseService,
            IGetNewOrderRefForOrderService
            getNewOrderRefForOrderService)
        {
            _getNewOrderRefForOrderService = getNewOrderRefForOrderService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
            _getOrderByOrderIdService = getOrderByOrderIdService;
            _getBilderlingsPaymentModelService = getBilderlingsPaymentModelService;
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _getIsAStoryTellingOrderService = getIsAStoryTellingOrderService;
            _getOrderDetailsFilterByOrderId = getOrderDetailsFilterByOrderId;
            _getOrderTotalCalculateService = getOrderTotalCalculateService;
            _getOrderItemsFilterByOrderIdService = getOrderItemsFilterByOrderIdService;
            _umbracoHelperService = umbracoHelperService;
        }

        public ViewOrderPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content, int orderId, int userId)
        {
            ViewOrderPageModel viewOrderPageModel = new ViewOrderPageModel(content);
            viewOrderPageModel.ListOfOrderPhotos = new List<_OrderPhotosModel>();
            
            var orderDetail = _getOrderDetailsFilterByOrderId.Get(orderId);
            if (orderDetail.UserId != userId || orderId <= 0 || userId <= 0)
            {
                return null;
            }

            var printButtonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Print_Button_Text);
            var orderDetailsTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Order_Details_Text);
            var orderDetailsDateText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Order_Date_Text);
            var orderDetailsOrderNumberText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Order_Number_Text);
            var orderDetailsStatusText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Order_Status_Text);
            var orderDetailsTotalText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Order_Total_Text);


            var billingDetailsTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Billing_Details_Text);
            var billingDetailsNameText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Billing_Name_Text);
            var billingDetailsEmailText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Billing_Email_Text);
            var billingDetailsContactUsText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Billing_ContactUs_Text);
            var billingDetailsAddressText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.View_Order_Billing_Address_Text);



            var photoTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_Summary_Photo_Title_Table);
            var priceTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_Summary_Pirce_Title_Table);
            var subtotalTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_Summary_Subtotal_Title_Table);


            var cardbuttonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_Summary_Card_Button_Text);
            var awardtermsAndConditionsRawText =
                _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    Enums.DictionaryKey.Dictionary_Form_Award_Rules_And_Terms_And_Conditions_Text);
            var awardRulesUrl = _umbracoHelperService.TypedContent(umbracoHelper,
                CS.Common.Umbraco.Constants.ConstantValues.Awards_Rules_Page_Id).Url;
            var termsAndConditionsUrl = _umbracoHelperService.TypedContent(umbracoHelper, CS.Common.Umbraco.Constants.ConstantValues.Terms_And_Conditions_Page_Id).Url;

            var awardRulesAndtermsAndConditionsFormatted = String.Format(awardtermsAndConditionsRawText, awardRulesUrl, termsAndConditionsUrl);
            var paypalbuttonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_Summary_Paypal_Button_Text);

            DateTime currentTime = DateTime.Now;
          
            CSTemplate.Code.Constants.Enums.PaymentStatus paymentStatus = (CSTemplate.Code.Constants.Enums.PaymentStatus)orderDetail.PaymentStatusEnumId;

            var orderItems = _getOrderItemsFilterByOrderIdService.Get(umbracoHelper: umbracoHelper, orderId: orderId, userId: 0);
            var orderTotal = _getOrderTotalCalculateService.Get(orderId);
            var orderStatus = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, paymentStatus);
            var isAStoryTelling = _getIsAStoryTellingOrderService.Get(orderId);
            var userDetail = _getUserFilterByUserIdService.Get(umbracoHelper, userId);
            var userEmail = userDetail.InsertedEmail;
            var userFullName = userDetail.Name + " " + userDetail.Surname;
           
            //here we need to update Order Ref Id,
            //since for bilderlings, we need it unique every time we try to pay
            //we need to update Order Ref Id, it is not already paid
            var orderToUpdate = _getOrderByOrderIdService.Get(orderId);
            var newOrderRefId = "";
            
            if (!orderToUpdate.PaymentStatusEnumId.Equals((int)Enums.PaymentStatus.OK))
            {
                newOrderRefId = _getNewOrderRefForOrderService.Get();
                using (var db = _getCurrentUmbracoDatabaseService.Get())
                {
                    orderToUpdate.OrderRefId = newOrderRefId;
                    db.Update(DatabaseConstantValues.OrdersTableName,
                        DatabaseConstantValues.Orders_Poco_Primary_Key_Value, orderToUpdate);
                }
            }
                viewOrderPageModel = new ViewOrderPageModel(content)
                {
                    PayStatus = orderDetail.PaymentStatusEnumId,
                    ButtonText = printButtonText,
                    OrderTitleText = orderDetailsTitleText,
                    OrderDateText = orderDetailsDateText,
                    OrderNumberText = orderDetailsOrderNumberText,
                    OrderStatusText = orderDetailsStatusText,
                    OrderTotalText = orderDetailsTotalText,
                    OrderDate = orderDetail.Date,
                    OrderNumber = orderDetail.OrderItemId.ToString(),
                    OrderStatus = orderStatus,
                    OrderTotal = orderTotal,
                    IsAStoryTelling = isAStoryTelling,
                    IsOrderOwner = true,

                    BillingTitleText = billingDetailsTitleText,
                    BillingNameText = billingDetailsNameText,
                    BillingEmailText = billingDetailsEmailText,
                    BillingContactText = billingDetailsContactUsText,
                    BillingAddressText = billingDetailsAddressText,
                    BillingName = userFullName,
                    BillingEmail = userEmail,
                    BillingContact = userDetail.InsertedPhone,
                    //BillingAddress = "6, spencer Fltas, triq Dun Gorg Preca, Hamrun, HMR 1605, Malta",

                    TablePhotoTitle = photoTitleText,
                    TablePriceTitle = priceTitleText,
                    TableSubtotalTitle = subtotalTitleText,
                    SubtotalPrice = orderTotal,
                    ListOfOrderPhotos = orderItems,


                    PaymentButtons = new _PaymentButtonsModel()
                    {
                        OrderId = orderId,
                        OrderRefId = orderToUpdate.OrderRefId,
                        ClientEmailAddress = userEmail,
                        TransactionDescription = orderDetail.Description,
                        SubTotalPrice = orderTotal,
                        ListOfOrderPhotos = orderItems,

                        CardButtonText = cardbuttonText,
                        AwardRulesTermsAndConditionsText = awardRulesAndtermsAndConditionsFormatted,
                        PaypalButtonText = paypalbuttonText,

                        BilderlingsPaymentModel = _getBilderlingsPaymentModelService.Get(!String.IsNullOrEmpty(newOrderRefId) ? newOrderRefId : orderDetail.OrderRefId,
                            orderTotal, userEmail, userDetail.Name, userDetail.Surname, orderItems)
                    }

                };

            return viewOrderPageModel;
        }
    }
}
