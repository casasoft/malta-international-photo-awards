﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Services.Page;
using CS.Common.Umbraco.Models.ContentFinder;
using CS.Common.Umbraco.Services.Umbraco.Wrappers;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace CS.Common.Umbraco.Services.ContentFinder
{
    public interface IContentFinderTryFindContentService
    {
        /// <summary>
        /// Try find the content for this particular content finder
        /// </summary>
        /// <param name="publishedContentRequest">The current request</param>
        /// <param name="dictionaryKey">The unique key to categorise this content finder with</param>
        /// <param name="contentFinderMatchUrlService">A service which matches the URL and returns a result</param>
        /// <returns></returns>
        bool TryFindContent(PublishedContentRequest publishedContentRequest,
            string dictionaryKey,
            IContentFinderMatchUrlService contentFinderMatchUrlService);
    }

    [Service]
    public class ContentFinderTryFindContentService : IContentFinderTryFindContentService
    {
        


        private readonly IUmbracoApplicationRuntimeCacheService _umbracoApplicationRuntimeCacheService;
        private readonly IPublishedContentRequestWrapperService _publishedContentRequestWrapperService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IFileServiceWrapper _fileServiceWrapper;

        public delegate ContentFinderItem LoadContentFinderItem(UmbracoContext context, string url);

        public delegate ContentFinderMatch MatchContentFinderUrl(string url);

        public const string ContentFinderDictionaryCacheKey = "ContentFinderTryFindContentDictionaryCacheKey";


        public ContentFinderTryFindContentService(IUmbracoApplicationRuntimeCacheService umbracoApplicationRuntimeCacheService,
            IPublishedContentRequestWrapperService publishedContentRequestWrapperService,
            IUmbracoHelperService umbracoHelperService,
            IFileServiceWrapper fileServiceWrapper)
        {
            _fileServiceWrapper = fileServiceWrapper;
            _umbracoHelperService = umbracoHelperService;
            _publishedContentRequestWrapperService = publishedContentRequestWrapperService;
            _umbracoApplicationRuntimeCacheService = umbracoApplicationRuntimeCacheService;
        }

        public bool TryFindContent(PublishedContentRequest publishedContentRequest,
            string dictionaryKey,
            IContentFinderMatchUrlService contentFinderMatchUrlService)
        {
            ConcurrentDictionary<string, ConcurrentDictionary<string, ContentFinderItem>> concurrentDictionary =
                _umbracoApplicationRuntimeCacheService.GetCacheItem(ContentFinderDictionaryCacheKey,
                    () => new ConcurrentDictionary<string, ConcurrentDictionary<string, ContentFinderItem>>());
            string url = publishedContentRequest.Uri.AbsolutePath;


            ConcurrentDictionary<string, ContentFinderItem> contentFinderItems = null;
            if (!concurrentDictionary.ContainsKey(dictionaryKey))
            {
                concurrentDictionary[dictionaryKey] = new ConcurrentDictionary<string, ContentFinderItem>();
            }
            contentFinderItems = concurrentDictionary[dictionaryKey];

            ContentFinderItem item = null;
            if (!contentFinderItems.ContainsKey(url))
            {
                //Load it


                var contentFinderMatch = contentFinderMatchUrlService.MatchUrl(publishedContentRequest);
                var umbracoContext = publishedContentRequest.RoutingContext.UmbracoContext;
                if (contentFinderMatch.Match)
                {
                    var matchingContentNode = contentFinderMatch.MatchingContent;
                    if (matchingContentNode == null)
                    {
                        throw new Exception(
                            string.Format(
                                "Content node for Content Finder Item '{0}' should be present.  Please update either the content node.",
                                dictionaryKey));
                    }
                    var template = _fileServiceWrapper.GetTemplate(umbracoContext, matchingContentNode.TemplateId);
                    item = new ContentFinderItem()
                    {
                        Content = matchingContentNode,
                        TemplateAlias = template.Alias
                    };
                }

                contentFinderItems[url] = item;
            }
            item = contentFinderItems[url];

            if (item != null)
            {
                //Match
                _publishedContentRequestWrapperService.UpdateContentAndTemplate(publishedContentRequest,
                    item.Content,
                    item.TemplateAlias);
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
