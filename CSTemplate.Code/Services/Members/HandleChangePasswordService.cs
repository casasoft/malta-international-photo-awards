﻿using System;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Members.ChangePassword;
using Umbraco.Core.Security;
using Umbraco.Web;
using Umbraco.Web.Security;

namespace CSTemplate.Code.Services.Members
{

    public interface IHandleChangePasswordService
    {
        bool Handle(UmbracoHelper umbracoHelper,
            ChangePasswordModel model, int currentMemberIdLoggedIn,
            MembershipHelper membershipHelper);
    }

    [Service]
    public class HandleChangePasswordService : IHandleChangePasswordService
    {
        private readonly IMemberServiceWrapper _memberServiceWrapper;

        public HandleChangePasswordService(IMemberServiceWrapper memberServiceWrapper)
        {
            _memberServiceWrapper = memberServiceWrapper;
        }

        public bool Handle(UmbracoHelper umbracoHelper,
            ChangePasswordModel model, int currentMemberIdLoggedIn,
            MembershipHelper membershipHelper)
        {
            var provider = MembershipProviderExtensions.GetMembersMembershipProvider();
            var currentMemberAsIMember = _memberServiceWrapper.GetById(umbracoHelper.UmbracoContext,
                currentMemberIdLoggedIn);

            var oldPasswordIsCorrect = provider.ValidateUser(currentMemberAsIMember.Username, model.OldPassword);
            if (oldPasswordIsCorrect)
            {
                try
                {
                    _memberServiceWrapper.SavePassword(umbracoHelper.UmbracoContext,
                        currentMemberAsIMember,
                        model.Password);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }
    }
}
