﻿using System;
using System.Globalization;

namespace CS.Common.Helpers
{
    public static class NumberHelpers
    {
        
        public static bool IsNumberInteger(object obj)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Convert.ToString(obj), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }
        
        public static string FormatCurrencyForCultureWithCurrency(decimal price, CultureInfo culture, string currencySymbol, int? decimalPoints = null)
        {
            //Swap it
            string prevCurrency = culture.NumberFormat.CurrencySymbol;
            culture.NumberFormat.CurrencySymbol = currencySymbol;
            if (decimalPoints.HasValue)
            {
                culture.NumberFormat.CurrencyDecimalDigits = decimalPoints.Value;
            }
            var priceStr = FormatCurrency(price, culture);
            culture.NumberFormat.CurrencySymbol = prevCurrency;
            return priceStr;

        }
        public static string FormatCurrency(decimal price, CultureInfo culture)
        {
            
            return price.ToString("C", culture);
        }
    }
}
