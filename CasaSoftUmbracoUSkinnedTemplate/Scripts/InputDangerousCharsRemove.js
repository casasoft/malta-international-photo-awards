﻿$(function () {
    var charList = ['<', '>', '\''];
    $("input[type='text'],input[type='password']").keyup(function () {

        var x = $(this).val();
        for (i = 0; i < charList.length; i++) {
            if (x.includes(charList[i])) {
                $(this).val('');
            }
        }

    });
})