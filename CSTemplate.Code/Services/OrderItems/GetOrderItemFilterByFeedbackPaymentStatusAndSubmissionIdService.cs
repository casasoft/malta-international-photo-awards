﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Orders;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.OrderItems
{

    public interface IGetOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService
    {
        OrderItemsPoco Get(int submissionId,bool payForFeedback=true);
    }

    [Service]
    public class GetOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService : IGetOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetOrderPocoFilterByOrderIdService _getOrderPocoFilterByOrderIdService;

        public GetOrderItemFilterByFeedbackPaymentStatusAndSubmissionIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService, IGetOrderPocoFilterByOrderIdService getOrderPocoFilterByOrderIdService)
        {
            _getOrderPocoFilterByOrderIdService = getOrderPocoFilterByOrderIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }   
        public OrderItemsPoco Get(int submissionId, bool payForFeedback = true)
        {
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var orderItemTable = Constants.DatabaseConstantValues.OrderItemsTableName;
            var orderIdField = Constants.DatabaseConstantValues.OrderItems_Order_Id_Field_Name;
            var orderItemPrimaryKey = Constants.DatabaseConstantValues.OrderItems_Poco_Primary_Key_Value;
            var submissionIdField = Constants.DatabaseConstantValues.OrderItems_Submission_Id_Field_Name;
            var payForFeedbackField = Constants.DatabaseConstantValues.OrderItems_Pay_For_Feedback_Field_Name;
            var payForFeedbackStatus = Convert.ToInt32(payForFeedback);
            var sqlQuery = new Sql($@"
                        SELECT TOP(1)
                               *
                            FROM {orderItemTable}
                                WHERE {submissionIdField} = {submissionId} AND {payForFeedbackField} = {payForFeedbackStatus} ORDER BY {orderItemPrimaryKey} DESC
            ");
            var orderItem = umbracoDb.Query<OrderItemsPoco>(sqlQuery).FirstOrDefault();
            if (orderItem!=null)
            {
                var orderDetail = _getOrderPocoFilterByOrderIdService.Get(orderItem.OrderId);
                var deleted = (int)Constants.Enums.PaymentStatus.DELETED;
                if (orderDetail.PaymentStatusEnumId == deleted)
                {
                    return null;
                }
            }
            
            return orderItem;
        }
    }
}
