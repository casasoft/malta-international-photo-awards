﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Payment;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.SubmitPhotos;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.SubmitPhoto;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Payment
{

    public interface IGetPaymentPageModelService
    {
        PaymentPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content, 
            string orderRefId, string paymentAmount, string transactionDescription, string clientEmailAddress);
    }

    [Service]
    public class GetPaymentPageModelService : IGetPaymentPageModelService
    {

        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetPaymentPageModelService(
            IUmbracoHelperService umbracoHelperService,
            IGetCurrentUmbracoDatabaseService
            getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
            _umbracoHelperService = umbracoHelperService;
        }

        public PaymentPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content, 
            string orderRefId, string paymentAmount, string transactionDescription, string clientEmailAddress)
        {

            PaymentPageModel paymentPageModel = new PaymentPageModel(content);

            paymentPageModel.RouteValues = new RouteValueDictionary();

            if (String.IsNullOrEmpty(orderRefId) || String.IsNullOrEmpty(paymentAmount)
                || String.IsNullOrEmpty(transactionDescription)
                || String.IsNullOrEmpty(clientEmailAddress))
            {
                paymentPageModel.ItIsAPayment = false;
            }
            else
            {
                paymentPageModel.RouteValues["paymentAmount"] = Convert.ToDecimal(paymentAmount);
                paymentPageModel.RouteValues["clientEmailAddress"] = clientEmailAddress;
                paymentPageModel.RouteValues["transactionDescription"] = transactionDescription;
                paymentPageModel.RouteValues["orderRefId"] = orderRefId;
                paymentPageModel.ItIsAPayment = true;
            }

            return paymentPageModel;
        }

    }
}
