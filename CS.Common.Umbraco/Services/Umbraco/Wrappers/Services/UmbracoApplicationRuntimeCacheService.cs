﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using Umbraco.Core;
using Umbraco.Core.Cache;
using System.Web.Caching;

namespace CS.Common.Umbraco.Services.Umbraco.Wrappers.Services
{
    public interface IUmbracoApplicationRuntimeCacheService
    {
        IRuntimeCacheProvider GetApplicationRuntimeCacheProvider();

        T GetCacheItem<T>(string cacheKey,
            Func<T> getCacheItem,
            TimeSpan? timeout = null,
            bool isSliding = false,
            CacheItemPriority priority = CacheItemPriority.Normal,
            CacheItemRemovedCallback removedCallback = null,
            string[] dependentFiles = null);

        void InsertCacheItem(string cacheKey,
            Func<object> getCacheItem,
            TimeSpan? timeout = null,
            bool isSliding = false,
            CacheItemPriority priority = CacheItemPriority.Normal,
            CacheItemRemovedCallback removedCallback = null,
            string[] dependentFiles = null);

        void ClearAllCache();
    }

    [Service]
    public class UmbracoApplicationRuntimeCacheService : IUmbracoApplicationRuntimeCacheService
    {


        public IRuntimeCacheProvider GetApplicationRuntimeCacheProvider()
        {
            return ApplicationContext.Current.ApplicationCache.RuntimeCache;
        }

        public void ClearAllCache()
        {
            GetApplicationRuntimeCacheProvider().ClearAllCache();
        }

        public T GetCacheItem<T>(string cacheKey,
            Func<T> getCacheItem,
            TimeSpan? timeout = null,
            bool isSliding = false,
            CacheItemPriority priority = CacheItemPriority.Normal,
            CacheItemRemovedCallback removedCallback = null,
            string[] dependentFiles = null)
        {
            return GetApplicationRuntimeCacheProvider().GetCacheItem(cacheKey,
                getCacheItem,
                timeout,
                isSliding,
                priority,
                removedCallback,
                dependentFiles);
        }

        public void InsertCacheItem(string cacheKey,
            Func<object> getCacheItem,
            TimeSpan? timeout = null,
            bool isSliding = false,
            CacheItemPriority priority = CacheItemPriority.Normal,
            CacheItemRemovedCallback removedCallback = null,
            string[] dependentFiles = null)
        {
            GetApplicationRuntimeCacheProvider().InsertCacheItem(cacheKey, 
                getCacheItem,
                timeout,
                isSliding,
                priority,
                removedCallback,
                dependentFiles);
        }

    }
}
