﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mail;
using SendGrid;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Models.SendGrid;
using CS.Common.Services;

namespace CS.Common.SendGrid.Wrappers.Services
{

    public interface ISendGridEmailService
    {
        /// <summary>
        /// Sending Email through SendGrid
        /// </summary>
        /// <param name="emailSubject"></param>
        /// <param name="emailHtmlText"></param>
        /// <param name="mailToEmailAddress"></param>
        /// <param name="replyToEmailAddress"></param>
        /// <param name="mailBcc"></param>
        /// <param name="mailFrom"></param>
        /// <param name="sendGridTemplateID">Send Grid Template GUID</param>
        /// <param name="attachments"></param>
        /// <returns>Returns true if Successful, Returns False if not successful</returns>
        bool SendEmail(string emailSubject, string emailHtmlText, string mailToEmailAddress, string replyToEmailAddress, MailAddress[] mailBcc, MailAddress mailFrom, string sendGridTemplateID, IEnumerable<SendGridAttachmentModel> attachments = null);
    }

    [Service]
    public class SendGridGridEmailService : ISendGridEmailService
    {
        public SendGridGridEmailService()
        {

        }

        /// <summary>
        /// Sending Email through SendGrid
        /// </summary>
        /// <param name="emailSubject"></param>
        /// <param name="emailHtmlText"></param>
        /// <param name="mailToEmailAddress"></param>
        /// <param name="replyToEmailAddress"></param>
        /// <param name="mailBcc"></param>
        /// <param name="mailFrom"></param>
        /// <param name="sendGridTemplateID">Send Grid Template GUID</param>
        /// <param name="attachments"></param>
        /// <returns>Returns true if Successful, Returns False if not successful</returns>
        public bool SendEmail(string emailSubject, string emailHtmlText, string mailToEmailAddress, string replyToEmailAddress, MailAddress[] mailBcc, MailAddress mailFrom, string sendGridTemplateID, IEnumerable<SendGridAttachmentModel> attachments = null)
        {

            SmtpClient client = new SmtpClient(CS.Common.Helpers.SmtpHelpers.SmtpHost);

            //If you need to authenticate
            client.Credentials = new NetworkCredential(CS.Common.Helpers.SmtpHelpers.SmtpUsername, CS.Common.Helpers.SmtpHelpers.SmtpPassword);

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = mailFrom;
            mailMessage.To.Add(mailToEmailAddress);

            if (!String.IsNullOrEmpty(replyToEmailAddress))
            {
                mailMessage.ReplyToList.Add(replyToEmailAddress);
            }

           
            mailMessage.Subject = emailSubject;
            mailMessage.Body = emailHtmlText;
            mailMessage.IsBodyHtml = true;

            //foreach (var attachment in attachments)
            //{
            //    mailMessage.Attachments.Add(attachment);
            //}


            try
            {
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}
