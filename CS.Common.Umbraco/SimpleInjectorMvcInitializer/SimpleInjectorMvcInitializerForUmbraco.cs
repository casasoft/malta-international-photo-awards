﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using CS.Common.Attributes;
using CS.Common.Helpers;
using CS.Common.Services;
using Our.Umbraco.Vorto;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Integration.WebApi;
using Umbraco.Web;


namespace CS.Common.Umbraco.SimpleInjectorMvcInitializer
{
    public class SimpleInjectorMvcInitializerForUmbraco
    {
        private class TypeWithAttribute
        {
            public ServiceAttribute ServiceAttribute { get; set; }
            public Type Type { get; set; }
        }

        private static string getFullNameOfService(Type service)
        {
            string result = service.FullName;
            if (!service.FullName.StartsWith("CS.Common"))
            {
                //Specific project so add a character which should be the last to sort after 'z'
                result = "ι" + service.FullName;

            }
            return result;
        }

        private static ConcurrentBag<Assembly> getAllCasaSoftAssemblies()
        {
            var casaSoftProjectAssemblies = new ConcurrentBag<Assembly>();
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();

            foreach (var a in assemblies)
            {
                var attrib = Attribute.GetCustomAttribute(a, typeof(CasaSoftAssemblyAttribute));
                if (attrib != null)
                {
                    casaSoftProjectAssemblies.Add(a);
                }

            }
            return casaSoftProjectAssemblies;
        }
        private static List<TypeWithAttribute> getAllServicesFromAssemblies(IEnumerable<Assembly> assemblies)
        {
            IEnumerable<Type> types = assemblies
                .SelectMany(s => s.GetTypes());

            types = types.Where(x => x.GetCustomAttributes(typeof(ServiceAttribute), true).Length > 0);
            return types.ToList()
                .ConvertAll(
                    x =>
                    {
                        return new TypeWithAttribute()
                        {
                            Type = x,
                            ServiceAttribute =
                                ReflectionHelpers.GetAttributesFromType<ServiceAttribute>(x).FirstOrDefault()
                        };
                    });
        }
        public static void InitializeSimpleInjectorMvc(Container container, 
            ConcurrentBag<Assembly> assemblies,
            IEnumerable<Assembly> listOfUmbracoRelatedApiAssemblies,
            Action<string> loggingFunc,
            HttpConfiguration configuration)
        {

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("These are all the assemblies in this order:");

            foreach (var assembly in assemblies)
            {
                sb.AppendLine(assembly.FullName);
            }
            loggingFunc(sb.ToString());

            var serviceImplementations = getAllServicesFromAssemblies(assemblies);

            serviceImplementations.Sort((a, b) =>
            {
                var serviceAttributeA = a.ServiceAttribute;
                var serviceAttributeB = b.ServiceAttribute;
                if (serviceAttributeA.Priority != ServiceAttribute.ServiceAttributeDefaultValue ||
                    serviceAttributeB.Priority != ServiceAttribute.ServiceAttributeDefaultValue)
                {
                    return serviceAttributeB.Priority.CompareTo(serviceAttributeA.Priority);
                }
                else
                {
                    //Sort by namespace
                    return String.Compare(getFullNameOfService(a.Type),
                        getFullNameOfService(b.Type),
                        StringComparison.Ordinal);
                }


            });
            sb = new StringBuilder();
            sb.AppendLine("These are all the services in this order to register before removing duplicates:");
            foreach (var service in serviceImplementations)
            {
                sb.AppendLine(service.Type.FullName);
            }

            loggingFunc(sb.ToString());


            //Remove duplicates
            serviceImplementations = ListHelpers.RemoveDuplicates(serviceImplementations,
                x =>
                {
                    Type[] serviceInterface = x.Type.GetInterfaces();
                    if (serviceInterface.Length == 0)
                    {
                        throw new Exception(string.Format("Service of type '{0}' must implement an interface",
                            x.Type.FullName));
                    }
                    return serviceInterface.FirstOrDefault().FullName;
                }).ToList();


            sb = new StringBuilder();
            sb.AppendLine("These are all the services in this order to register afer removing duplicates:");
            foreach (var service in serviceImplementations)
            {
                sb.AppendLine(service.Type.FullName);
            }

            loggingFunc(sb.ToString());

            //Register them all
            foreach (var serviceImplementation in serviceImplementations)
            {
                if (!serviceImplementation.Type.IsClass)
                {
                    throw new Exception(
                        string.Format(
                            "In order to register service of type '{0}' with SimpleInjector, it needs to be an implementation / Class",
                            serviceImplementation.Type.FullName));
                }
                Type[] serviceInterface = serviceImplementation.Type.GetInterfaces();
                /* if (serviceInterface.Length > 1)
                {
                    throw new Exception(string.Format("Service of type '{0}' can only implement one Interface",
                        serviceImplementation.FullName));
                }*/
                //Register the service with its respective interface

                //log
                var serviceType = serviceInterface[0];
                var serviceTypeName = serviceType.FullName;

                //LogHelper.Warn<object>();

                var lifestyle = serviceImplementation.ServiceAttribute.Singleton
                    ? Lifestyle.Singleton
                    : Lifestyle.Transient;
                loggingFunc($"Registering service: {serviceTypeName} as lifestyle: {lifestyle}");
                container.Register(serviceType,
                    serviceImplementation.Type,
                    lifestyle);
                loggingFunc("Registered service successfull: " + serviceTypeName);
            }


            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.RegisterWebApiControllers(configuration, listOfUmbracoRelatedApiAssemblies);

            // This is an extension method from the integration package as well.
            container.RegisterMvcIntegratedFilterProvider();

            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}
