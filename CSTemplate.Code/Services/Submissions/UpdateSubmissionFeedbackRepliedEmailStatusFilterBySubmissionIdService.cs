﻿using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Database;
using System;

namespace CSTemplate.Code.Services.Submissions
{
    public interface IUpdateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService
    {
        bool Update(int submissionId);
    }
    [Service]
    public class UpdateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService : IUpdateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService
    {
       
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetSubmissionPocoDetailsFilterByIdService _getSubmissionPocoDetailsFilterByIdService;

        public UpdateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService(
            IGetSubmissionPocoDetailsFilterByIdService getSubmissionPocoDetailsFilterByIdService,
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabase
        )
        {
            _getSubmissionPocoDetailsFilterByIdService = getSubmissionPocoDetailsFilterByIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabase;
        }
        public bool Update(int submissionId)
        {
            var result = false;
            if (submissionId <= 0) return result;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var submissionTable = DatabaseConstantValues.SubmissionsTableName;
            var submissionPrimaryKey = DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            int queryResult = 0;
            var submission = _getSubmissionPocoDetailsFilterByIdService.Get(submissionId);
            if (!submission.FeedbackRepliedMailSent)
            {
                submission.FeedbackRepliedMailSent = Convert.ToBoolean((int)Enums.FeedbackRepliedMailSent.Replied);
                queryResult = umbracoDb.Update(submissionTable, submissionPrimaryKey, submission);
            }
            result = queryResult > 0 ? true : false;
            return result;
        }
    }
}