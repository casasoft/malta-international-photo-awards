﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RazorEngine;
using RazorEngine.Templating;
namespace CS.Common.Helpers
{
    public static class RazorHelpers
    {
        public static string CompileAndEvaluateRazorTemplate(
          string template,
          string templateKey,
          object model)
        {
            
            try
            {
                var modelType = model.GetType();

                //todo: [For: Developers | 2017/01/18] The following code is a workaround suggested here: https://github.com/Antaris/RazorEngine/issues/226. We're still waiting for another solutuion that actually replace the cached template or to remove it. (WrittenBy: Simon)        			

                //var result =
                //    Engine.Razor.RunCompile(template,
                //        templateKey,
                //        modelType,
                //        model);

                var result = Engine.Razor
                    .RunCompile(template, TextHelpers.GetMd5Hash(template), modelType, model);


                return result.Trim();

            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }
    }
}
