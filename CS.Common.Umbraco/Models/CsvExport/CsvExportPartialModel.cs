﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Umbraco.Models.CsvExport
{
    public class CsvExportPartialModel
    {
        public int Source { get; set; }
        public bool GetChildrenOfSource { get; set; }
        /// <summary>
        /// Get the Children of Children of Source
        /// </summary>
        public bool GetChildrenOfChildrenOfSource { get; set; }

        public string ChildrenOfChildrenOfSourceHeaderTitle { get; set; }

        public string DocumentTypeAlias { get; set; }
        public bool EncodeHtmlTags { get; set; }
        public bool StripHtmlTags { get; set; }
        public string Delimiter { get; set; }
        public bool RemoveLineBreaks { get; set; }
    }
}
