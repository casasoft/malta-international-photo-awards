﻿using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared.Loader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Loader
{
    public interface IGetLoaderPageService
    {
        _LoaderPageModel Get(UmbracoHelper umbracoHelper);
    }
    [Service]
    public class GetLoaderPageService : IGetLoaderPageService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetLoaderPageService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public _LoaderPageModel Get(UmbracoHelper umbracoHelper)
        {
            var loaderTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Loader_Title_Text);
            var loaderExplanationText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Loader_Explanation_Text);
            var loaderPageModel = new _LoaderPageModel()
            {
                ExplanationText = loaderExplanationText,
                LoaderTitle = loaderTitleText
            };

            return loaderPageModel;

        }
    }
}
