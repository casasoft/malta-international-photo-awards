﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Umbraco.Models.General
{
    public class LinkModel
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string LinkTarget { get; set; }

        public int Level { get; set; }
    }
}
