﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.MembersArea;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class MembersAreaPageDataController : USNBaseController
    {
        public MembersAreaPageDataController()
        {

        }

        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult MembersAreaPage(RenderModel model)
        {


            MembersAreaPageModel memebrsAreaPageModel = new MembersAreaPageModel(model.Content);

            return base.Populate(memebrsAreaPageModel);
        }
    }
}