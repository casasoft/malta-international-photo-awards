﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.OrderItems
{

    public interface IGetOrderItemPocoFilterByOrderIdService
    {
        OrderItemsPoco Get(int orderId);
    }

    [Service]
    public class GetOrderItemPocoFilterByOrderIdService : IGetOrderItemPocoFilterByOrderIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetOrderItemPocoFilterByOrderIdService(IGetCurrentUmbracoDatabaseService  getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public OrderItemsPoco Get(int orderId)
        {
            if (orderId <= 0) return new OrderItemsPoco();
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var orderItemsTableName = Constants.DatabaseConstantValues.OrderItemsTableName;
            var orderItemsOrderIdField = Constants.DatabaseConstantValues.OrderItems_Order_Id_Field_Name;

            var select = "*";
            var whereCondition = $"{orderItemsOrderIdField} = {orderId}";

            var sqlQuery = new Sql().Select(select).From(orderItemsTableName).Where(whereCondition);

            var orderItemResult = umbracoDb.Fetch<OrderItemsPoco>(sqlQuery).FirstOrDefault();

            return orderItemResult;
        }
    }
}
