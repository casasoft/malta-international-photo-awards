﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.WebPages.Html;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.App_Plugins.UIOMaticCustomFields.SubmissionStatusEnum
{
    [PluginController("SubmissionStatus")]
    public class SubmissionStatusApiController : UmbracoAuthorizedJsonController
    {
        
        public List<SelectListItem> GetAll()
        {
            var statusList = Enum.GetValues(typeof(CSTemplate.Code.Constants.Enums.SubmissionStatus)).Cast<CSTemplate.Code.Constants.Enums.SubmissionStatus>();
            var list = new List<SelectListItem>();
            foreach (var status in statusList)
            {
                list.Add(new SelectListItem()
                {
                    Text = CS.Common.Helpers.EnumHelpers.GetDescriptionAttributeValue(status),
                    Value = Convert.ToInt32(status).ToString()
                });
            }
            return list;
        }

        public string GetStatus(int id)
        {
            var enumStatus = (CSTemplate.Code.Constants.Enums.SubmissionStatus)id;
            return CS.Common.Helpers.EnumHelpers.GetDescriptionAttributeValue(enumStatus);
        }
    }
}
