﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CS.Common.Collections;

namespace CS.Common.Helpers
{
    public class PagingHelpers
    {
        /// <summary>
        /// This method helper returns the  paged results for current page based on a query string passed in the request
        /// </summary>
        /// <typeparam name="TDataModel">Model coming from the source origin (API, Umbraco...)</typeparam>
        /// <typeparam name="TViewModel">Model for the partial view</typeparam>
        /// <param name="getItemsFromUmbracoHandler"></param>
        /// <param name="request">HTTP Page Request for the call</param>
        /// <param name="pageSize">Number of elements for the page</param>
        /// <param name="convertFromUmbracoToModelHandler"></param>
        /// <returns>Returns the paged results for current page</returns>
        public static SearchPagedResults<TViewModel> GetPagedResultsWithTotalCountForCurrentPageFromQuerystring
            <TDataModel, TViewModel>(
            Func<IEnumerable<TDataModel>> getItemsFromUmbracoHandler,
            HttpRequestBase request,
            int pageSize,
            Converter<TDataModel, TViewModel> convertFromUmbracoToModelHandler)
        {
            var page = 1;
            int.TryParse(request.QueryString["page"], out page);
            return GetPagedResultsWithTotalCount(getItemsFromUmbracoHandler,
                page,
                pageSize,
                convertFromUmbracoToModelHandler);
        }

        public static SearchPagedResults<TItemModel> getPagedResultsWithTotalCountForCurrentPageFromQuerystring
            <TUmbracoData, TItemModel>(
            Func<IEnumerable<TUmbracoData>> getItemsFromUmbracoHandler,
            HttpRequestBase request,
            int pageSize,
            Converter<TUmbracoData, TItemModel> convertFromUmbracoToModelHandler)
        {
            var page = 1;
            int.TryParse(request.QueryString["page"], out page);
            return getPagedResultsWithTotalCount(getItemsFromUmbracoHandler,
                page,
                pageSize,
                convertFromUmbracoToModelHandler);
        }

        public static SearchPagedResults<TItemModel> getPagedResultsWithTotalCount<TUmbracoData, TItemModel>(
            Func<IEnumerable<TUmbracoData>> getItemsFromUmbracoHandler,
            int pageNumber,
            int pageSize,
            Converter<TUmbracoData, TItemModel> convertFromUmbracoToModelHandler)
        {
            var umbracoItems = getItemsFromUmbracoHandler();
            var totalItems = umbracoItems.Count();
            var totalPages = ListHelpers.GetTotalPagesForPageSizeFromTotalItems(totalItems, pageSize);

            // Preventive code
            if (pageNumber > totalPages)
            {
                pageNumber = totalPages;
            }
            else if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            var listUmbracoItems = ListHelpers.GetPageFromList(umbracoItems, pageNumber, pageSize);

            var listModels = listUmbracoItems.ToList().ConvertAll(convertFromUmbracoToModelHandler);

            return new SearchPagedResults<TItemModel>(listModels, totalItems);
        }

        /// <summary>
        /// This helper returns the paged results with the total count of it
        /// </summary>
        /// <typeparam name="TDataModel">Model coming from the source origin (API, Umbraco...)</typeparam>
        /// <typeparam name="TViewModel">Model for the partial view</typeparam>
        /// <param name="getItemsFromDataModelHandler"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="convertFromDataModelToViewModelHandler"></param>
        /// <returns>Returns the paged results for the total count</returns>
        public static SearchPagedResults<TViewModel> GetPagedResultsWithTotalCount<TDataModel, TViewModel>(
            Func<IEnumerable<TDataModel>> getItemsFromDataModelHandler,
            int pageNumber,
            int pageSize,
            Converter<TDataModel, TViewModel> convertFromDataModelToViewModelHandler)
        {
            var umbracoItems = getItemsFromDataModelHandler();
            var totalItems = umbracoItems.Count();
            var totalPages = ListHelpers.GetTotalPagesForPageSizeFromTotalItems(totalItems, pageSize);

            // Preventive code
            if (pageNumber > totalPages)
            {
                pageNumber = totalPages;
            }
            else if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            var listUmbracoItems = ListHelpers.GetPageFromList(umbracoItems, pageNumber, pageSize);

            var listModels = listUmbracoItems.ToList().ConvertAll(convertFromDataModelToViewModelHandler);

            return new SearchPagedResults<TViewModel>(listModels, totalItems);
        }
    }
}
