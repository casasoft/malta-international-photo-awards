﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using Umbraco.Core.Logging;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Payment
{

    public interface IProcessPaymentRedirectionService
    {
        string Process(UmbracoHelper umbracoHelper, XmlDocument objParamsXml);
    }

    [Service]
    public class ProcessPaymentRedirectionService : IProcessPaymentRedirectionService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public ProcessPaymentRedirectionService(IUmbracoHelperService
            umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }

        public string Process(UmbracoHelper umbracoHelper, XmlDocument objParamsXml)
        {
            var urlToReturn = "";

            if (objParamsXml.GetElementsByTagName(ConstantValues.PaymentResponseXMLAttributeResultName).Count > 0)
            {
                //return the result value of the transaction
                String resultValue = objParamsXml.GetElementsByTagName(ConstantValues.PaymentResponseXMLAttributeResultName)[0].InnerText;
                //check if result returned
                if (resultValue.Length > 0)
                {
                    var paymentPageForCurrentDomain = 
                        _umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)
                        .Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals(ConstantValues.Payment_Page_Document_Type_Alias));

                    //result returned
                    switch (resultValue.ToUpper().Trim())
                    {
                        case "OK":
                            //validate that the params transaction values do match the original transaction
                            //validateResponseWithTool(objFastPayXml);
                            LogHelper.Info<ProcessPaymentResponseService>("Payment OK");
                            var firstOrDefaultSuccessful = paymentPageForCurrentDomain.Children.FirstOrDefault(x => x.Name.Equals("Successful"));
                            if (
                                firstOrDefaultSuccessful != null)
                                urlToReturn = CS.Common.Umbraco.Helpers.UrlHelpers.GetAbsoluteUrlWithTheCorrentProtocol(
                                    firstOrDefaultSuccessful.Url);
                            break;
                        case "NOTOK":
                            var firstOrDefaultFailed= paymentPageForCurrentDomain.Children.FirstOrDefault(x => x.Name.Equals("Failed"));
                            if (
                                firstOrDefaultFailed != null)
                                urlToReturn = CS.Common.Umbraco.Helpers.UrlHelpers.GetAbsoluteUrlWithTheCorrentProtocol(
                                    firstOrDefaultFailed.Url);
                            break;
                        case "DECLINED":
                            var firstOrDefaultDeclined = paymentPageForCurrentDomain.Children.FirstOrDefault(x => x.Name.Equals("Declined"));
                            if (
                                firstOrDefaultDeclined != null)
                                urlToReturn = CS.Common.Umbraco.Helpers.UrlHelpers.GetAbsoluteUrlWithTheCorrentProtocol(
                                    firstOrDefaultDeclined.Url);
                            break;
                        case "PENDING":
                            var firstOrDefaultFailedPending = paymentPageForCurrentDomain.Children.FirstOrDefault(x => x.Name.Equals("Failed"));
                            if (
                                firstOrDefaultFailedPending != null)
                                urlToReturn = CS.Common.Umbraco.Helpers.UrlHelpers.GetAbsoluteUrlWithTheCorrentProtocol(
                                    firstOrDefaultFailedPending.Url);
                            break;
                        case "CANCEL":
                            var firstOrDefaultCancelled = paymentPageForCurrentDomain.Children.FirstOrDefault(x => x.Name.Equals("Cancelled"));
                            if (
                                firstOrDefaultCancelled != null)
                                urlToReturn = CS.Common.Umbraco.Helpers.UrlHelpers.GetAbsoluteUrlWithTheCorrentProtocol(
                                    firstOrDefaultCancelled.Url);
                            break;
                        default:
                            //DO something: OTHER RESULT
                            break;
                    }

                }
                else
                {
                    //ERROR: Transaction Result is empty
                    throw new Exception("Transaction Result is empty");
                }
            }

            return urlToReturn;
        }
    }
}
