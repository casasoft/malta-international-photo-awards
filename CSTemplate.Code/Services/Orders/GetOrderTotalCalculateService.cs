﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Database;

namespace CSTemplate.Code.Services.Orders
{

    public interface IGetOrderTotalCalculateService
    {
        decimal Get(int orderId);
    }

    [Service]
    public class GetOrderTotalCalculateService : IGetOrderTotalCalculateService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetOrderTotalCalculateService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public decimal Get(int orderId)
        {
            decimal total = 0;
            if (orderId <= 0) return total;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var orderItemsTable = DatabaseConstantValues.OrderItemsTableName;
            var feedbackCostField = DatabaseConstantValues.OrderItems_Feedback_Cost_Field_Name;
            var submissionCost = DatabaseConstantValues.OrderItems_Submission_Cost_Field_Name;
            var orderIdField = DatabaseConstantValues.OrderItems_Order_Id_Field_Name;
            total = umbracoDb.ExecuteScalar<decimal>($@"SELECT SUM({feedbackCostField})+SUM({submissionCost}) FROM {orderItemsTable} Where {orderIdField} = {orderId}");
            return total;
        }
    }
}
