﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;

namespace CSTemplate.Code.Services.Orders
{

    public interface IGetNewOrderRefForOrderService
    {
        string Get();
    }

    [Service]
    public class GetNewOrderRefForOrderService : IGetNewOrderRefForOrderService
    {
        public GetNewOrderRefForOrderService()
        {
            
        }

        public string Get()
        {
            return  Guid.NewGuid().ToString().Replace("-", "").Substring(0, 29);
        }
    }
}
