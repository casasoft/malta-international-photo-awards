//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	// Mixin content Type 1174 with alias "USN_CMP_SP_BannerSection"
	/// <summary>Standard Page Banner Section</summary>
	public partial interface IUsn_Cmp_Sp_BannerSection : IPublishedContent
	{
		/// <summary>Add color overlay</summary>
		bool AddColorOverlay { get; }

		/// <summary>Content</summary>
		object Content { get; }

		/// <summary>Display banner section</summary>
		bool DisplayHeaderImageSection { get; }

		/// <summary>Animate</summary>
		bool HeaderAnimate { get; }

		/// <summary>Background color</summary>
		string HeaderBackgroundColor { get; }

		/// <summary>Button color</summary>
		string HeaderButtonColor { get; }

		/// <summary>Heading</summary>
		USNStarterKit.USNHeading.Models.USNHeading HeaderHeading { get; }

		/// <summary>Image</summary>
		IPublishedContent HeaderImage { get; }

		/// <summary>Image alt text</summary>
		string HeaderImageAltText { get; }

		/// <summary>Link</summary>
		Umbraco.Web.Models.RelatedLinks HeaderLink { get; }

		/// <summary>Parallax</summary>
		bool HeaderParallax { get; }

		/// <summary>Scroll prompt</summary>
		bool HeaderScrollPrompt { get; }

		/// <summary>Secondary heading</summary>
		string HeaderSecondaryHeading { get; }

		/// <summary>Banner style</summary>
		USNStarterKit.USNEnums.Options HeaderStyle { get; }

		/// <summary>Text</summary>
		IHtmlString HeaderText { get; }

		/// <summary>Text alignment</summary>
		USNStarterKit.USNEnums.Options HeaderTextAlignment { get; }

		/// <summary>Settings</summary>
		object Settings { get; }

		/// <summary>Video</summary>
		IPublishedContent Video { get; }
	}

	/// <summary>Standard Page Banner Section</summary>
	[PublishedContentModel("USN_CMP_SP_BannerSection")]
	public partial class Usn_Cmp_Sp_BannerSection : PublishedContentModel, IUsn_Cmp_Sp_BannerSection
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "USN_CMP_SP_BannerSection";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Usn_Cmp_Sp_BannerSection(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Usn_Cmp_Sp_BannerSection, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Add color overlay: If “Yes” is selected a semi transparent block of the background color is added over your image.
		///</summary>
		[ImplementPropertyType("addColorOverlay")]
		public bool AddColorOverlay
		{
			get { return GetAddColorOverlay(this); }
		}

		/// <summary>Static getter for Add color overlay</summary>
		public static bool GetAddColorOverlay(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<bool>("addColorOverlay"); }

		///<summary>
		/// Content: {div class="panel-heading"}{h3}Content{/h3}{/div}
		///</summary>
		[ImplementPropertyType("content")]
		public object Content
		{
			get { return GetContent(this); }
		}

		/// <summary>Static getter for Content</summary>
		public static object GetContent(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue("content"); }

		///<summary>
		/// Display banner section
		///</summary>
		[ImplementPropertyType("displayHeaderImageSection")]
		public bool DisplayHeaderImageSection
		{
			get { return GetDisplayHeaderImageSection(this); }
		}

		/// <summary>Static getter for Display banner section</summary>
		public static bool GetDisplayHeaderImageSection(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<bool>("displayHeaderImageSection"); }

		///<summary>
		/// Animate
		///</summary>
		[ImplementPropertyType("headerAnimate")]
		public bool HeaderAnimate
		{
			get { return GetHeaderAnimate(this); }
		}

		/// <summary>Static getter for Animate</summary>
		public static bool GetHeaderAnimate(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<bool>("headerAnimate"); }

		///<summary>
		/// Background color: Color selected will dictate the color to be used for any text.  If a background image is used, select the color that works best for the text over the image.
		///</summary>
		[ImplementPropertyType("headerBackgroundColor")]
		public string HeaderBackgroundColor
		{
			get { return GetHeaderBackgroundColor(this); }
		}

		/// <summary>Static getter for Background color</summary>
		public static string GetHeaderBackgroundColor(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<string>("headerBackgroundColor"); }

		///<summary>
		/// Button color
		///</summary>
		[ImplementPropertyType("headerButtonColor")]
		public string HeaderButtonColor
		{
			get { return GetHeaderButtonColor(this); }
		}

		/// <summary>Static getter for Button color</summary>
		public static string GetHeaderButtonColor(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<string>("headerButtonColor"); }

		///<summary>
		/// Heading: Your heading hierarchy is important for SEO. This will not affect font size.
		///</summary>
		[ImplementPropertyType("headerHeading")]
		public USNStarterKit.USNHeading.Models.USNHeading HeaderHeading
		{
			get { return GetHeaderHeading(this); }
		}

		/// <summary>Static getter for Heading</summary>
		public static USNStarterKit.USNHeading.Models.USNHeading GetHeaderHeading(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<USNStarterKit.USNHeading.Models.USNHeading>("headerHeading"); }

		///<summary>
		/// Image: Takeover: 1500px x 1100px Medium: 1500px x 550px Short: 1500px x 400px Parallax: 1500px x 1100px  Focal point is defined within your "Media" section.
		///</summary>
		[ImplementPropertyType("headerImage")]
		public IPublishedContent HeaderImage
		{
			get { return GetHeaderImage(this); }
		}

		/// <summary>Static getter for Image</summary>
		public static IPublishedContent GetHeaderImage(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<IPublishedContent>("headerImage"); }

		///<summary>
		/// Image alt text
		///</summary>
		[ImplementPropertyType("headerImageAltText")]
		public string HeaderImageAltText
		{
			get { return GetHeaderImageAltText(this); }
		}

		/// <summary>Static getter for Image alt text</summary>
		public static string GetHeaderImageAltText(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<string>("headerImageAltText"); }

		///<summary>
		/// Link: The "Caption" field will be used for your link text.
		///</summary>
		[ImplementPropertyType("headerLink")]
		public Umbraco.Web.Models.RelatedLinks HeaderLink
		{
			get { return GetHeaderLink(this); }
		}

		/// <summary>Static getter for Link</summary>
		public static Umbraco.Web.Models.RelatedLinks GetHeaderLink(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<Umbraco.Web.Models.RelatedLinks>("headerLink"); }

		///<summary>
		/// Parallax
		///</summary>
		[ImplementPropertyType("headerParallax")]
		public bool HeaderParallax
		{
			get { return GetHeaderParallax(this); }
		}

		/// <summary>Static getter for Parallax</summary>
		public static bool GetHeaderParallax(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<bool>("headerParallax"); }

		///<summary>
		/// Scroll prompt
		///</summary>
		[ImplementPropertyType("headerScrollPrompt")]
		public bool HeaderScrollPrompt
		{
			get { return GetHeaderScrollPrompt(this); }
		}

		/// <summary>Static getter for Scroll prompt</summary>
		public static bool GetHeaderScrollPrompt(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<bool>("headerScrollPrompt"); }

		///<summary>
		/// Secondary heading
		///</summary>
		[ImplementPropertyType("headerSecondaryHeading")]
		public string HeaderSecondaryHeading
		{
			get { return GetHeaderSecondaryHeading(this); }
		}

		/// <summary>Static getter for Secondary heading</summary>
		public static string GetHeaderSecondaryHeading(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<string>("headerSecondaryHeading"); }

		///<summary>
		/// Banner style
		///</summary>
		[ImplementPropertyType("headerStyle")]
		public USNStarterKit.USNEnums.Options HeaderStyle
		{
			get { return GetHeaderStyle(this); }
		}

		/// <summary>Static getter for Banner style</summary>
		public static USNStarterKit.USNEnums.Options GetHeaderStyle(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<USNStarterKit.USNEnums.Options>("headerStyle"); }

		///<summary>
		/// Text
		///</summary>
		[ImplementPropertyType("headerText")]
		public IHtmlString HeaderText
		{
			get { return GetHeaderText(this); }
		}

		/// <summary>Static getter for Text</summary>
		public static IHtmlString GetHeaderText(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<IHtmlString>("headerText"); }

		///<summary>
		/// Text alignment
		///</summary>
		[ImplementPropertyType("headerTextAlignment")]
		public USNStarterKit.USNEnums.Options HeaderTextAlignment
		{
			get { return GetHeaderTextAlignment(this); }
		}

		/// <summary>Static getter for Text alignment</summary>
		public static USNStarterKit.USNEnums.Options GetHeaderTextAlignment(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<USNStarterKit.USNEnums.Options>("headerTextAlignment"); }

		///<summary>
		/// Settings: {div class="panel-heading"}{h3}Settings{/h3}{/div}
		///</summary>
		[ImplementPropertyType("settings")]
		public object Settings
		{
			get { return GetSettings(this); }
		}

		/// <summary>Static getter for Settings</summary>
		public static object GetSettings(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue("settings"); }

		///<summary>
		/// Video: MP4 format only. Image above will display until video loads. Small file size recommended for quick downloads.
		///</summary>
		[ImplementPropertyType("video")]
		public IPublishedContent Video
		{
			get { return GetVideo(this); }
		}

		/// <summary>Static getter for Video</summary>
		public static IPublishedContent GetVideo(IUsn_Cmp_Sp_BannerSection that) { return that.GetPropertyValue<IPublishedContent>("video"); }
	}
}
