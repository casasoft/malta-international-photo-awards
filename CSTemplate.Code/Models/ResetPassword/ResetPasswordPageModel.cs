﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.ResetPassword
{
    public class ResetPasswordPageModel : USNBaseViewModel
    {
        public bool UserCanResetPassword { get; set; }
        public _ResetPasswordFormModel ResetPasswordForm { get; set; }
        public string ErrorMessage { get; set; }
        public string ResetPasswordMessage { get; set; }

        public ResetPasswordPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
