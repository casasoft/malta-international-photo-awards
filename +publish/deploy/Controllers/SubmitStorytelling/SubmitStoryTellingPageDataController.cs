﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.SubmitStorytelling;
using CSTemplate.Code.Services.SubmitStorytelling;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.SubmitStorytelling
{
    public partial class SubmitStoryTellingPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetSubmitStoryTellingPageService _getSubmitStoryTellingPageService;

        public SubmitStoryTellingPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetSubmitStoryTellingPageService getSubmitStoryTellingPageService
            )
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getSubmitStoryTellingPageService = getSubmitStoryTellingPageService;

        }

        // GET: SubmitStoryTellingPageData
        public virtual ActionResult SubmitSToryTellingPage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            SubmitStoryTellingPageModel submitStoryTellingPageModel =
                _getSubmitStoryTellingPageService.Get(umbracoHelper, model.Content);
            return base.Populate(submitStoryTellingPageModel);
        }
    }
}