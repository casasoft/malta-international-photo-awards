﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.SubmitPhotos;
using CSTemplate.Code.Services.Payment;
using CSTemplate.Code.Services.SubmitPhoto;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Payment
{
    public partial class PaymentPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetPaymentPageModelService _getPaymentPageModelService;

        public PaymentPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetPaymentPageModelService
            getPaymentPageModelService)
        {
            _getPaymentPageModelService = getPaymentPageModelService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
        }

        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User_And_Short_Time)]
        public virtual ActionResult PaymentPageData(RenderModel model)
        {
            var orderRefIdObject = TempData[ConstantValues.TempDataPaymentOrderRefId];
            var paymentAmountObject = TempData[ConstantValues.TempDataPaymentPaymentAmount];
            var transactionDescriptionObject = TempData[ConstantValues.TempDataPaymentTransactionDescription];
            var clientEmailAddressObject = TempData[ConstantValues.TempDataPaymentClientEmailAddress];

            string orderRefId = "", paymentAmount = "", transactionDescription = "", clientEmailAddress = "";

            if (orderRefIdObject != null)
            {
                orderRefId = orderRefIdObject.ToString();
            }
            if (paymentAmountObject != null)
            {
                paymentAmount = paymentAmountObject.ToString();
            }
            if (transactionDescriptionObject != null)
            {
                transactionDescription = transactionDescriptionObject.ToString();
            }
            if (clientEmailAddressObject != null)
            {
                clientEmailAddress = clientEmailAddressObject.ToString();
            }

            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var paymentPageModel = _getPaymentPageModelService.Get(umbracoHelper, model.Content, orderRefId, paymentAmount, transactionDescription, clientEmailAddress);

            return base.Populate(paymentPageModel);
        }
      
    }
}