﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Members
{
    public class _MemberItemModel
    {
        public int MemberId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string InsertedEmail { get; set; }
        public string InsertedPhone { get; set; }
        public string Nationality { get; set; }
        public string County { get; set; }
    }
}
