﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.Loader;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.SubmitPhotos
{
    public class SubmitPhotoPageModel : USNBaseViewModel
    {
        public string ExplanationText { get; set; }
        public string SubExplanationText { get; set; }
        public string BrowserSuggestionText { get; set; }
        public _LoaderPageModel LoaderPageModel { get; set; }
        public _SubmitPhotoFormModel SubmitPhotoForm { get; set; }
        public SubmitPhotoPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
