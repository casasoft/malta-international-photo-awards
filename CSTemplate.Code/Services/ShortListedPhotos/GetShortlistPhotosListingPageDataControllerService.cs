﻿using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shortlist_Photos;
using CSTemplate.Code.Services.Awards;
using CSTemplate.Code.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;
using static CSTemplate.Code.Constants.Enums;

namespace CSTemplate.Code.Services.ShortListedPhotos
{
    public interface IGetShortlistPhotosListingPageDataControllerService
    {
        ShortlistPhotosListingPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent publishedContent, SubmissionFilters shortListFilters, ShortListedStatusFilter shortListedStatus, string searchKey, int awardId, int awardCategoryId, int pageNumber = 1);
    }
    [Service]
    public class GetShortlistPhotosListingPageDataControllerService : IGetShortlistPhotosListingPageDataControllerService
    {
        private readonly IGetPagerModelService _getPagerModelService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetAllAwardEditionsService _getAllAwardEditionsService;
        private readonly IGetUnshortlistedphotosV2Service _getUnshortlistedphotosV2Service;

        public GetShortlistPhotosListingPageDataControllerService(
            IGetPagerModelService getPagerModelService, 
            IUmbracoHelperService umbracoHelperService,
            IGetAllAwardEditionsService getAllAwardEditionsService,
            IGetUnshortlistedphotosV2Service getUnshortlistedphotosV2Service

            )
        {
            this._getPagerModelService = getPagerModelService;
            this._umbracoHelperService = umbracoHelperService;
            this._getAllAwardEditionsService = getAllAwardEditionsService;
            this._getUnshortlistedphotosV2Service = getUnshortlistedphotosV2Service;
        }
        public ShortlistPhotosListingPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent publishedContent, SubmissionFilters shortListFilters, ShortListedStatusFilter shortListedStatus, string searchKey, int awardId, int awardCategoryId, int pageNumber = 1)
        {
            var shortlistPhotosListingPageModel = new ShortlistPhotosListingPageModel(publishedContent);

            #region shortListFilterFormModel

            var awardList = _getAllAwardEditionsService.Get(umbracoHelper);
            shortlistPhotosListingPageModel.ShortListFilterFormModel = new _ShortListFilterFormModel();
            shortlistPhotosListingPageModel.ShortListFilterFormModel.AwardEditionList = awardList;
            shortlistPhotosListingPageModel.ShortListFilterFormModel.AwardEditonLabelText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.AwardEdition_Filter_Label_Text);

            shortlistPhotosListingPageModel.ShortListFilterFormModel.SearchKeyFilterLabelText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.Search_Parameter_Filter_Label_Text);

            shortlistPhotosListingPageModel.ShortListFilterFormModel.ShortListStatusLabelText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.Shortlist_Status_Filter_Label_Text);

            shortlistPhotosListingPageModel.ShortListFilterFormModel.SubmissionFilterLabelText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.Submission_Filter_Label_Text);

            shortlistPhotosListingPageModel.ShortListFilterFormModel.Title= _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.Shortlisted_Submissions_Filter_Form_Title_Text);

            shortlistPhotosListingPageModel.ShortListFilterFormModel.FilterButtonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.Shortlisted_Submissions_Filter_Button_Text);

            shortlistPhotosListingPageModel.ShortListFilterFormModel.AwardEditonCategoryLabelText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.AwardEdition_Category_Filter_Label_Text);

            #endregion

            #region Pager
            var listOfItemPhotoModel = _getUnshortlistedphotosV2Service.Get(umbracoHelper, shortListFilters, shortListedStatus, searchKey, awardId, awardCategoryId);
            int itemTotalAmounts = listOfItemPhotoModel.Count;

            //how many item to show on the page
            shortlistPhotosListingPageModel.ItemsPerPage = CSTemplate.Code.Constants.ConstantValues.Mansory_Item_ShortListed_Count;
            //total number of listing items.
            shortlistPhotosListingPageModel.ItemsTotalAmount = itemTotalAmounts;
            var pageSize = shortlistPhotosListingPageModel.ItemsPerPage;

            listOfItemPhotoModel = listOfItemPhotoModel.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper,
                shortlistPhotosListingPageModel.ItemsPerPage, shortlistPhotosListingPageModel.ItemsTotalAmount);

            shortlistPhotosListingPageModel.Pagination = pagerModel;

            shortlistPhotosListingPageModel.ShortlistPhotosFormModel = new ShortlistPhotosFormModel()
            {
                ListOfPhotos = listOfItemPhotoModel,
                ShortlistButtonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, CSTemplate.Code.Constants.Enums.DictionaryKey.Short_List_Button_Form_Text)
            };
            #endregion

            return shortlistPhotosListingPageModel;
        }
    }
}
