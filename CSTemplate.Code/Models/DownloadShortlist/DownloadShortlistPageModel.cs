﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.DownloadShortlist
{
    public class DownloadShortlistPageModel : USNBaseViewModel
    {
        public string ExplanationText { get; set; }
        public string ButtonText { get; set; }

        public DownloadShortlistPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
