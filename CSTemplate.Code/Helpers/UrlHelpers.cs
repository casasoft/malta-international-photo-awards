﻿using CSTemplate.Code.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using static CSTemplate.Code.Constants.Enums;

namespace CSTemplate.Code.Helpers
{
    public static class UrlHelpers
    {
        public static string GetAbsoluteUrlWithHostNameFromWebConfigConfiguration(string relativeUrl)
        {
            string hostName = ConfigurationManager.AppSettings["HostName"];
            return string.Concat(hostName, relativeUrl);
        }

        //public static string GetFleetSearchListingPageUrl(
        //    UrlHelper urlHelper,
        //    HttpRequestBase request,
        //    string curretCultureName,
        //    string currentPageLanguageName,
        //    string manufacturer,
        //    string configuration,
        //    int? passengers,
        //    int? luggage,
        //    LUGGAGE_QUANTITY_UNIT? luggageType
        //    )
        //{
        //    var rvd = new Dictionary<string, object>()
        //    {
        //        {
        //            ConstantValues.CurrentCultureRouteParamName, curretCultureName
        //        },
        //        {
        //            ConstantValues.FleetPageCustomRouteParamName, currentPageLanguageName
        //        }
        //    };

        //    if (!String.IsNullOrEmpty(manufacturer))
        //    {
        //        rvd.Add(ConstantValues.FleetManufacturerRouteParamName, manufacturer);
        //    }

        //    var fleetSearchListingPageUrl = urlHelper.RouteUrl(ConstantValues.FleetSearchCustomRouteName,
        //        new RouteValueDictionary(rvd));

        //    var uriBuilder = new UriBuilder(request.Url.Scheme, request.Url.Host, -1, fleetSearchListingPageUrl);

        //    var queryString = HttpUtility.ParseQueryString(uriBuilder.Query);

        //    if (!String.IsNullOrEmpty(configuration))
        //    {
        //        queryString[ConstantValues.FleetConfigurationFromQueryStringName] = configuration;
        //    }

        //    if (passengers.HasValue)
        //    {
        //        queryString[ConstantValues.FleetPassengerFromQueryStringName] = passengers.Value.ToString();
        //    }

        //    if (luggage.HasValue)
        //    {
        //        queryString[ConstantValues.FleetLuggageFromQueryStringName] = luggage.Value.ToString();
        //    }

        //    if (luggageType.HasValue)
        //    {
        //        queryString[ConstantValues.FleetLuggageTypeFromQueryStringName] = luggageType.ToString();
        //    }

        //    uriBuilder.Query = queryString.ToString();
        //    fleetSearchListingPageUrl = uriBuilder.ToString();

        //    return fleetSearchListingPageUrl;
        //}
    }
}
