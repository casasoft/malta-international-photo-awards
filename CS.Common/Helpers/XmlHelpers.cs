﻿using System.IO;
using System.Xml.Serialization;

namespace CS.Common.Helpers
{
    public static class XmlHelpers
    {
        public static void SerializeObjectToXmlFile(object o, string path)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(o.GetType());
            FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write);
            xmlSerializer.Serialize(fs, o);
            fs.Close();
            fs.Dispose();
        }
        public static T DeserializeObjectFromXmlFile<T>(string path)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            var retType = (T)xmlSerializer.Deserialize(fs);
            fs.Close();
            fs.Dispose();
            return retType;

        }

        public static T DeserializeObjectFromXmlString<T>(string xml)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            //StringBuilder sb = new StringBuilder();
            //StringWriter stringWriter = new StringWriter(sb);
            //stringWriter.Write(xml);
            StringReader sr = new StringReader(xml);
            object obj = xmlSerializer.Deserialize(sr);
            //            ms.Dispose();
            return (T)obj;

        }
    }
}
