﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.OrderSummary;
using CSTemplate.Code.Services.OrderSummary;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class OrderSummaryPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetOrderSummaryPageService _getOrderSummaryPageService;

        public OrderSummaryPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetOrderSummaryPageService getOrderSummaryPageService
        )
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getOrderSummaryPageService = getOrderSummaryPageService;
        }


        // GET: OrderSummaryPageData
        public virtual ActionResult OrderSummaryPage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            OrderSummaryPageModel orderSummaryPageModel = _getOrderSummaryPageService.Get(umbracoHelper, model.Content,
                User.Identity.Name,Members.GetCurrentMemberId());
            return base.Populate(orderSummaryPageModel);
        }
    }
}