﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using DevTrends.MvcDonutCaching;
using RazorEngine.Templating;
using Umbraco.Core;
using Umbraco.Core.Cache;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;
using Umbraco.Web.Cache;
using Umbraco.Web.Routing;

namespace CSTemplate.Code.Startup
{
    public class SatrtupHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            CacheRefresherBase<PageCacheRefresher>.CacheUpdated += this.CachePageEvents_CacheUpdated;
            CacheRefresherBase<DictionaryCacheRefresher>.CacheUpdated += this.CacheDictionaryEvents_CacheUpdated;
            CacheRefresherBase<MediaCacheRefresher>.CacheUpdated += this.CacheMediaEvents_CacheUpdated;
        }

        private void CacheMediaEvents_CacheUpdated(MediaCacheRefresher sender, CacheRefresherEventArgs e)
        {
            ClearOutputCache();
        }

        private void CacheDictionaryEvents_CacheUpdated(DictionaryCacheRefresher sender, CacheRefresherEventArgs e)
        {
            ClearOutputCache();
        }

        private void CachePageEvents_CacheUpdated(PageCacheRefresher sender, CacheRefresherEventArgs e)
        {
            ClearOutputCache();
        }

        public void ClearOutputCache()
        {
            HttpRuntime.Cache.Remove(ConstantValues.Cache_Key_Pages);
            HttpRuntime.Cache.Remove(ConstantValues.Cache_Key_Submission_Full_Page);
        }
    }
}
