﻿﻿angular.module("umbraco").controller("AwardEditionList",
    function ($scope, $http) {
        $http.get("backoffice/Category/CategoryApi/GetAllAwards/").then(function (res) {
            $scope.awardEditionList = res.data;
        });
       
    });