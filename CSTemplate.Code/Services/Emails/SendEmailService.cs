﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CS.Common.Models.SendGrid;
using CS.Common.Services;
using CS.Common.Umbraco.Models.Authentication;
using CS.Common.Umbraco.Services.RazorEngine;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.Emails
{
    public interface ISendEmailService
    {
        bool SendEmail(UmbracoHelper umbracoContext, string emailSubject, string emailHtmlText, string mailToEmailAddress, string replyToEmailAddress, MailAddress[] mailBcc, MailAddress mailFrom, IEnumerable<SendGridAttachmentModel> attachments = null);
    }

    [Service]
    public class SendEmailService : ISendEmailService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IRazorEngineCompileTemplate _razorEngineCompileTemplate;

        public SendEmailService(
            IUmbracoHelperService umbracoHelperService,
            IRazorEngineCompileTemplate razoerEngineCompileTemplate)
        {
            _umbracoHelperService = umbracoHelperService;
            _razorEngineCompileTemplate = razoerEngineCompileTemplate;
        }

        public bool SendEmail(UmbracoHelper umbracoContext, string emailSubject, string emailHtmlText, string mailToEmailAddress, string replyToEmailAddress, MailAddress[] mailBcc, MailAddress mailFrom, IEnumerable<SendGridAttachmentModel> attachments = null)
        {
            SmtpClient client = new SmtpClient(CS.Common.Helpers.SmtpHelpers.SmtpHost);
            client.Port = CS.Common.Helpers.SmtpHelpers.SmtpPort;

            //If you need to authenticate
            client.Credentials = new NetworkCredential(CS.Common.Helpers.SmtpHelpers.SmtpUsername, CS.Common.Helpers.SmtpHelpers.SmtpPassword);

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = mailFrom;

            if (mailToEmailAddress != null)
            {
                mailMessage.To.Add(mailToEmailAddress);

                if (!String.IsNullOrEmpty(replyToEmailAddress))
                {
                    mailMessage.ReplyToList.Add(replyToEmailAddress);
                }

                var commonMessageTemplateModel = new CommonEmailMessageTemplate()
                {
                    RenderBody = emailHtmlText
                };
                var commonEmailTemplate = new EmailTemplate(_umbracoHelperService.TypedContent(umbracoContext, ConstantValues.Email_Template_Comomon_Template_Header_And_Footer));
                var keyForEmailBody = $"{commonEmailTemplate.Name}-{"body"}-{commonEmailTemplate.UpdateDate}";
                var keyForEmailSubject = $"{commonEmailTemplate.Name}-{"subject"}-{commonEmailTemplate.UpdateDate}";
                emailSubject += " " + _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(commonEmailTemplate.Subject.ToString(), keyForEmailSubject, commonMessageTemplateModel).EnsureStartsWith(' ');
                emailHtmlText = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(commonEmailTemplate.EmailBody.ToString(), keyForEmailBody, commonMessageTemplateModel);


                //replace relative urls with current domain
                string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

                var emailHtmlTextReplaced = emailHtmlText.Replace("<a href=\"/", String.Format("<a href=\"{0}/", domainName));

                mailMessage.Subject = emailSubject;
                mailMessage.Body = emailHtmlTextReplaced;
                mailMessage.IsBodyHtml = true;

                //foreach (var attachment in attachments)
                //{
                //    mailMessage.Attachments.Add(attachment);
                //}

                try
                {
                    client.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    return false;
                }

            }

            return true;
        }
    }
}
