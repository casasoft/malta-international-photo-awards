﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Photos;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Shared.SurfaceControllers
{
    public partial class SubmitPhotoFormSurfaceController : SurfaceController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly ISubmitNewPhotosService _submitNewPhotosService;

        public SubmitPhotoFormSurfaceController(IUmbracoControllerWrapperService controllerWrapperService,ISubmitNewPhotosService submitNewPhotosService)
        {
            
            _submitNewPhotosService = submitNewPhotosService;
            _umbracoControllerWrapperService = controllerWrapperService;
        }

        // GET: SubmitPhotoFormSurface
        [HttpPost]
        public virtual ActionResult SubmitPhotoForm(_SubmitPhotoFormModel submitPhotoFormModel)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var userId = Members.GetCurrentMemberId();
            var submitModel = submitPhotoFormModel.ListOfSubmitPhotos;
            var result = _submitNewPhotosService.Submit(umbracoHelper, userId, submitModel);
            var redirectToUrl = result==true?CSTemplate.Code.Constants.ConstantValues.Order_Summary_Page_Link: CSTemplate.Code.Constants.ConstantValues.Submit_Photo_Page_Link;


            return base.Redirect(redirectToUrl);
        }
    }
}