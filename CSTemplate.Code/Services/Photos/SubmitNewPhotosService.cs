﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.OrderItems;
using CSTemplate.Code.Services.Orders;
using CSTemplate.Code.Services.Submissions;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using databaseConstants = CSTemplate.Code.Constants.DatabaseConstantValues;
using constantEnums = CSTemplate.Code.Constants.Enums;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Photos
{

    public interface ISubmitNewPhotosService
    {
        /// <summary>
        /// It will submitted new photos
        /// </summary>
        /// <returns></returns>
        bool Submit(UmbracoHelper umbracoHelper, int userId, List<_SubmitPhotoItemModel> photoList);
    }

    [Service]
    public class SubmitNewPhotosService : ISubmitNewPhotosService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IAddNewOrderService _addNewOrderService;
        private readonly IUploadPhotosService _uploadPhotosService;
        private readonly ICreateNewSubmissionService _createNewSubmissionService;
        private readonly IAddNewOrderItemService _addNewOrderItemService;
        private readonly IUmbracoHelperService _umbracoHelperService;

        public SubmitNewPhotosService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IAddNewOrderService addNewOrderService,
            IUploadPhotosService uploadPhotosService,
            ICreateNewSubmissionService createNewSubmissionService,
            IAddNewOrderItemService addNewOrderItemService,
            IUmbracoHelperService umbracoHelperService
            )
        {
            _umbracoHelperService = umbracoHelperService;
            _addNewOrderItemService = addNewOrderItemService;
            _createNewSubmissionService = createNewSubmissionService;
            _uploadPhotosService = uploadPhotosService;
            _addNewOrderService = addNewOrderService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public bool Submit(UmbracoHelper umbracoHelper, int userId, List<_SubmitPhotoItemModel> photoList)
        {
            if ((photoList == null || photoList.Count <= 0) || userId <= 0) return CSTemplate.Code.Constants.ConstantValues.Process_Failed;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            bool processResult = CSTemplate.Code.Constants.ConstantValues.Process_Failed;
            if (photoList.Any(x => x.isAStortyTelling == Convert.ToBoolean(constantEnums.IsStoryTelling.Yes)))
            {
                processResult = Submission(umbracoHelper, umbracoDb, userId, photoList, Convert.ToBoolean(constantEnums.IsStoryTelling.Yes));
            }
            else
            {
                processResult = Submission(umbracoHelper, umbracoDb, userId, photoList, Convert.ToBoolean(constantEnums.IsStoryTelling.No));
            }
            return processResult;
        }


        private bool Submission(UmbracoHelper umbracoHelper, UmbracoDatabase umbracoDb, int userId, List<_SubmitPhotoItemModel> photoList,
            bool itIsStoryTellingSubmission)
        {
            var feedbackRequest = photoList.Any(x => x.FeedBackRequest);
            var feedbackRequestCount = photoList.Where(x => x.FeedBackRequest).ToList().Count;
            string description = string.Empty;

            if (itIsStoryTellingSubmission && feedbackRequest)
            {
                //var descriptionEnum = constantEnums.DictionaryKey.Order_Desctiption_Submission_Of_Count_Photos;
                //description = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, descriptionEnum);
                //description = string.Format(description, photoList.Count.ToString());
                description = string.Format("Submission of Storytelling and {0} feedbacks.", feedbackRequestCount);
            }
            else if (itIsStoryTellingSubmission)
            {
                //var descriptionEnum = constantEnums.DictionaryKey.Order_Desctiption_Submission_of_StoryTelling;
                //description = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, descriptionEnum);
                //description = string.Format(description, photoList.Count.ToString());
                description = "Submission of Storytelling.";
            }
            else if (!itIsStoryTellingSubmission && feedbackRequest)
            {
                //var descriptionEnum = constantEnums.DictionaryKey.Order_Desctiption_Submission_Of_Count_Photos_And_Count_Of_Feedbacks;
                //var mm = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, descriptionEnum);
                //description = string.Format(mm, photoList.Count.ToString(), feedbackRequestCount.ToString());
                description = string.Format("Submission of {0} photos and {1} feedbacks.", photoList.Count, feedbackRequestCount);
            }
            else
            {
                //description = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, constantEnums.DictionaryKey.Order_Desctiption_Submission_Of_Count_Photos);
                //description = string.Format(description, photoList.Count.ToString());
                description = string.Format("Submission of {0} photos", photoList.Count);
            }

            var photosTableName = databaseConstants.PhotosTableName;
            var photoPrimaryKeyField = databaseConstants.Photos_Poco_Primary_Key_Value;
            int counterForAddingPhotos = 0;
            int lastCreatedSubmissionId = 0;
            bool storyTellingOrderItemCreated = false;
            var newOrderId = _addNewOrderService.Add(userId, description);
            bool newOrderItem = false;
            if (itIsStoryTellingSubmission)
            {

                lastCreatedSubmissionId = _createNewSubmissionService.Create(umbracoHelper, userId, photoList.FirstOrDefault());
            }

            var dictionaryValueOfStoryTellingAmount = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Constants.Enums.DictionaryKey.Story_Telling_Amount);
            var storyTellingAmount = Convert.ToInt32(dictionaryValueOfStoryTellingAmount);

            var storyTellingAmountMessage = "Dear User! You Are Submitting a StoryTelling. Please check your photo amount.Story Telling Submissions have to had just {0} Photo !";
            var amountExceptionMessage = string.Format(storyTellingAmountMessage, storyTellingAmount);
            var storyTellingCountCheck = false;
            foreach (var photo in photoList)
            {
                if (!itIsStoryTellingSubmission)
                {
                    lastCreatedSubmissionId = _createNewSubmissionService.Create(umbracoHelper, userId, photo);
                }
                foreach (var submittedPhoto in photo.SubmitPhoto)
                {
                    if (itIsStoryTellingSubmission && !storyTellingCountCheck)
                    {
                        if (photo.SubmitPhoto.Count != storyTellingAmount)
                        {
                            throw new Exception(amountExceptionMessage);
                        }
                        storyTellingCountCheck = true;
                    }
                    var newPhoto = new PhotoPoco
                    {
                        PhotoUrl = _uploadPhotosService.Upload(submittedPhoto),
                        SubmissionId = lastCreatedSubmissionId,
                    };

                    var insertedPhoto = umbracoDb.Insert(photosTableName, photoPrimaryKeyField, newPhoto);
                    var insertedPhotoId = Convert.ToInt32(insertedPhoto);
                    if (itIsStoryTellingSubmission && storyTellingOrderItemCreated == false)
                    {
                        newOrderItem = _addNewOrderItemService.Add(umbracoHelper, lastCreatedSubmissionId, newOrderId, photo.isAStortyTelling, photo.FeedBackRequest, Convert.ToBoolean(constantEnums.PaymentForSubmission.Yes));
                        storyTellingOrderItemCreated = true;
                    }
                    else if (!itIsStoryTellingSubmission)
                    {
                        newOrderItem = _addNewOrderItemService.Add(umbracoHelper, lastCreatedSubmissionId, newOrderId, photo.isAStortyTelling, photo.FeedBackRequest, Convert.ToBoolean(constantEnums.PaymentForSubmission.Yes));
                    }

                    if (newOrderItem) counterForAddingPhotos++;
                }

            }
            bool processResult = (counterForAddingPhotos == photoList.Count ? CSTemplate.Code.Constants.ConstantValues.Process_Successful : CSTemplate.Code.Constants.ConstantValues.Process_Failed);
            if (itIsStoryTellingSubmission)
            {
                processResult = (counterForAddingPhotos == storyTellingAmount ? CSTemplate.Code.Constants.ConstantValues.Process_Successful : CSTemplate.Code.Constants.ConstantValues.Process_Failed);
            }


            return processResult;
        }
    }
}
