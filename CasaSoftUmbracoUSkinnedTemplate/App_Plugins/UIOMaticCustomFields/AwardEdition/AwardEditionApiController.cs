﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Editors;
using CSTemplate.Code.Services.Awards;

namespace CasaSoftUmbracoUSkinnedTemplate.App_Plugins.UIOMaticCustomFields.AwardEdition
{
    [PluginController("AwardEdition")]
    public class AwardEditionApiController : UmbracoAuthorizedJsonController
    {
        private readonly IGetAwardFilterByAwardIdService _getAwardFilterByAwardIdService;
        private Umbraco.Web.UmbracoHelper _umbracoHelper;
        public AwardEditionApiController(IGetAwardFilterByAwardIdService getAwardFilterByAwardIdService)
        {
            _umbracoHelper = new Umbraco.Web.UmbracoHelper(UmbracoContext.Current);
            _getAwardFilterByAwardIdService = getAwardFilterByAwardIdService;
        }
        // GET: AwardEditionApi
        public string GetName(int id)
        {
            var awardName = _getAwardFilterByAwardIdService.Get(_umbracoHelper, id);
            return awardName.Name;
        }
    }
}