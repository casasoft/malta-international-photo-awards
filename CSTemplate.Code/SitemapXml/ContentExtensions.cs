﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Constants;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.SitemapXml
{
    public static class ContentExtensions
    {

        public static IEnumerable<IPublishedContent> DescendantSitemapNodes(this IPublishedContent node, string hideFromSitemapPropertyAlias = null, string hideChildrenFromSitemapPropertyAlias = null)
        {
            if (!node.GetPropertyValue<bool>(ConstantValues.ExludeFromXmlSitemapAliasName)) yield return node;
            if (!node.GetPropertyValue<bool>(ConstantValues.ExcludeAllChildrenFromXMLSitemapAliasName))
            {
                foreach (var child in node.Children())
                {
                    foreach (var grandChild in child.DescendantSitemapNodes(hideFromSitemapPropertyAlias, hideChildrenFromSitemapPropertyAlias))
                        yield return grandChild;
                }
            }
        }
    }
}
