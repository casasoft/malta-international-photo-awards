﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.DataAnnotations
{
    
    public class ReplaceDangerousCharactersAttribute : ValidationAttribute
    {
        private readonly List<char> _dangerousCharacters;
        public ReplaceDangerousCharactersAttribute()
        {
            _dangerousCharacters = new List<char>();
            _dangerousCharacters.Add('\'');
            _dangerousCharacters.Add('!');
            _dangerousCharacters.Add(',');
            _dangerousCharacters.Add('<');
            _dangerousCharacters.Add('>');
            _dangerousCharacters.Add('&');
            _dangerousCharacters.Add('[');
            _dangerousCharacters.Add(']');

        }
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            var content = value as string;
            foreach (var item in _dangerousCharacters)
            {
                if (content.Contains(item))
                {
                    return new ValidationResult(
                    FormatErrorMessage("Please do not use dangerous characters"));
                }
            }

            return ValidationResult.Success;
        }
        
    }
}
