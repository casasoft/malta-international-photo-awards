﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Caching;
using CS.Common.Helpers;
using CS.Common.Models.Paging;
using CS.Common.Services;
using CS.Common.Services.Page;
using CS.Common.Umbraco.Services.RazorEngine;
using CS.Common.Umbraco.Services.Umbraco.Wrappers;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using RazorEngine.Templating;
using Umbraco.Web;

namespace CS.Common.Umbraco.Services.Pager
{

    public interface ICreatePagingModelForCurrentUrlService
    {
        PagerModel CreatePagerModelForCurrentUrl(
            UmbracoHelper umbracoHelper,
            CreatePagingModelForCurrentUrlService.CreatePagerModelForCurrentUrlParameters parameters);
    }

    [Service]
    public class CreatePagingModelForCurrentUrlService : ICreatePagingModelForCurrentUrlService
    {
        public const string PagerActiveCssClassName = "active";

        public class PagerShowingResultsRazorTemplateModel
        {
            public int From { get; set; }
            public int To { get; set; }
            public int TotalResults { get; set; }
            public string ItemTypeSingular { get; set; }
            public string ItemTypePlural { get; set; }
        }

        public class CreatePagerModelForCurrentUrlParameters
        {
            public int ItemsPerPage { get; set; }
            public int TotalNumberOfItems { get; set; }

            public string ItemTypeTextSingular { get; set; } = "result";

            public string ItemTypeTextPlural { get; set; } = "results";

            /// <summary>
            /// This means the maximum amount of pages to show near to the page.  So if the page is current [4], and the max pages is 5, it will show pages
            /// [2] [3] [4] [5] [6]
            /// </summary>
            public int MaxAmtPagesToShow { get; set; } = 5;

            public Enum ShowingResultsRazorTemplateKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.PagerShowingResultsRazorTemplate;

            public Enum PreviousPageTextKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.PagerPreviousPageText;

            public Enum FirstPageTextKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.PagerFirstPageText;

            public Enum NextPageTextKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.PagerNextPageText;

            public Enum LastPageTextKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.PagerLastPageText;

            public Enum GoToFirstPageTitleKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.PagerTitleGoToFirstPage;

            public Enum GoToPreviousPageTitleKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.PagerTitleGoToPreviousPage;

            public Enum GoToNextPageTitleKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.PagerTitleGoToNextPage;

            public Enum GoToLastPageTitleKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.PagerTitleGoToLastPage;

            public Enum GoToPageTitleKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.PagerTitleGoToPage;

            public Enum ThisIsCurrentPageTitleKey { get; set; } =
                CS.Common.Umbraco.Constants.Enums.DictionaryKey.ThisIsCurrentPage;

            public CreatePagerModelForCurrentUrlParameters()
            {

            }
        }

        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IPageService _pageService;

        private readonly IRazorEngineCompileTemplateFromDictionaryItemService
            _razorEngineCompileTemplateFromDictionaryItemService;

        public CreatePagingModelForCurrentUrlService(
            IUmbracoHelperService umbracoHelperService,
            IPageService pageService,
            IRazorEngineCompileTemplateFromDictionaryItemService razorEngineCompileTemplateFromDictionaryItemService)
        {
            _razorEngineCompileTemplateFromDictionaryItemService = razorEngineCompileTemplateFromDictionaryItemService;
            _pageService = pageService;
            _umbracoHelperService = umbracoHelperService;
        }

        private string computeUrlForPage(string currentUrl, int pageNumber)
        {

            NameValueCollection nvc = new NameValueCollection()
            {
                {
                    CS.Common.Umbraco.Constants.ConstantValues.QuerystringParamCurrentPage, pageNumber.ToString()
                }
            };
            string url = UrlHelpers.UpdateQuerystringForUrlFromNameValueCollection(currentUrl,
                nvc,
                UrlHelpers.UpdateQuerystringForUrlFromNameValueCollectionUpdateType.Update);
            return url;

        }

        public PagerModel CreatePagerModelForCurrentUrl(
            UmbracoHelper umbracoHelper,
            CreatePagerModelForCurrentUrlParameters parameters)
        {
            //string currentPageUrl = _pageService.GetCurrentPageUrlAbsolutePath();
            var currentPageUrl = _pageService.GetCurrentPageUri().ToString();
            /*
            var itemTypeTextSingular = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(
                umbracoHelper,
                parameters.ItemTypeTextSingularKey);
            var itemTypeTextPlural = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                parameters.ItemTypeTextPluralKey);
                */

            int currentPage =
                _pageService.GetVariableFromQuerystring<int>(umbracoHelper.UmbracoContext.HttpContext.Request,
                    CS.Common.Umbraco.Constants.ConstantValues.QuerystringParamCurrentPage);

            var totalPages = (int)Math.Ceiling((double)parameters.TotalNumberOfItems / (double)parameters.ItemsPerPage);
            if (currentPage < 1)
            {
                currentPage = 1;
            }
            else if (currentPage > totalPages)
            {
                currentPage = totalPages;
            }

            var fromAmount = ((currentPage - 1) * parameters.ItemsPerPage) + 1;
            var toAmount = Math.Min(fromAmount + parameters.ItemsPerPage - 1, parameters.TotalNumberOfItems);

            var model = new PagerShowingResultsRazorTemplateModel()
            {
                From = fromAmount,
                To = toAmount,
                TotalResults = parameters.TotalNumberOfItems,
                ItemTypePlural = parameters.ItemTypeTextPlural,
                ItemTypeSingular = parameters.ItemTypeTextSingular
            };



            var showingText =
                _razorEngineCompileTemplateFromDictionaryItemService.CompileRazorTemplateFromUmbracoDictionaryItem(
                    umbracoHelper,
                    parameters.ShowingResultsRazorTemplateKey,
                    model);

            int pageRangeFrom =
                (int)Math.Max(1, (double)(currentPage - (int)Math.Floor((double)parameters.MaxAmtPagesToShow / 2d)));
            int pageRangeTo = Math.Min(pageRangeFrom + parameters.MaxAmtPagesToShow - 1, totalPages);
            bool isFirstPage = currentPage == 1;
            bool isLastPage = currentPage == totalPages;

            PagerModel pagerModel = new PagerModel();
            pagerModel.Pages = new List<PagerItemModel>();
            pagerModel.ShowingResultsOfPagesText = showingText;

            if (!isFirstPage)
            {
                //Add First & Prev pages
                var firstPageNumber = 1;
                pagerModel.Pages.Add(new PagerItemModel()
                {
                    Title = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                        parameters.GoToFirstPageTitleKey),
                    PageNumber = firstPageNumber,
                    Text =
                        _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                            parameters.FirstPageTextKey),
                    CssClassName = "first-page",
                    Url = computeUrlForPage(currentPageUrl, firstPageNumber)
                });

                var previousPageNumber = currentPage - firstPageNumber;
                var previousPageUrl = computeUrlForPage(currentPageUrl, previousPageNumber);

                _pageService.SetHttpContextItem(CS.Common.Umbraco.Constants.ConstantValues.ViewContextItem_CanonicalPrevUrl, previousPageUrl);
                pagerModel.Pages.Add(new PagerItemModel()
                {
                    Title = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                        parameters.GoToPreviousPageTitleKey),
                    PageNumber = previousPageNumber,
                    Text =
                        _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                            parameters.PreviousPageTextKey),
                    CssClassName = "previous-page",
                    Url = previousPageUrl
                });
            }
            //Add the pages
            for (int i = pageRangeFrom; i <= pageRangeTo; i++)
            {
                var pagerItemModel = new PagerItemModel()
                {
                    Title = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                        parameters.GoToNextPageTitleKey),
                    PageNumber = i,
                    Text = i.ToString(),
                    CssClassName = string.Format("page-{0}", i),
                    Url = computeUrlForPage(currentPageUrl, i)
                };
                //custom
                if (i < currentPage)
                {
                    pagerItemModel.Title = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    parameters.GoToPreviousPageTitleKey);
                }

                if (i == currentPage)
                {
                    //This is the active page
                    pagerItemModel.CssClassName += string.Format(" {0}", PagerActiveCssClassName);
                    pagerItemModel.Title = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                        parameters.ThisIsCurrentPageTitleKey);
                }
                pagerModel.Pages.Add(pagerItemModel);
            }
            if (!isLastPage)
            {
                //Add Next & Last pages
                var nextPageNumber = currentPage + 1;
                var nextPageUrl = computeUrlForPage(currentPageUrl, nextPageNumber);
                _pageService.SetHttpContextItem(CS.Common.Umbraco.Constants.ConstantValues.ViewContextItem_CanonicalNextUrl, nextPageUrl);
                pagerModel.Pages.Add(new PagerItemModel()
                {
                    Title = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                        parameters.GoToNextPageTitleKey),
                    PageNumber = nextPageNumber,
                    Text =
                        _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                            parameters.NextPageTextKey),
                    CssClassName = "next-page",
                    Url = nextPageUrl
                });

                var lastPageNumber = totalPages;
                pagerModel.Pages.Add(new PagerItemModel()
                {
                    Title = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                        parameters.GoToLastPageTitleKey),
                    PageNumber = lastPageNumber,
                    Text =
                        _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                            parameters.LastPageTextKey),
                    CssClassName = "last-page",
                    Url = computeUrlForPage(currentPageUrl, lastPageNumber)
                });
            }

            return pagerModel;
        }

    }
}
