﻿angular.module("umbraco").controller("MyGalleryCategory",
    function ($scope, $http) {
        $http.get("backoffice/Category/CategoryApi/GetAllAwards/").then(function (res) {

            $scope.awardEditions = res.data;
        });
        $scope.galleryCategories = [];
        $scope.myMethod = function () {
            //alert($scope.selected);
            var url = "backoffice/GalleryCategory/GalleryCategoryApi/GetAll/" + $scope.selected;
            $http.get(url).then(function (response) {
                $scope.galleryCategories = response.data;
                console.log($scope.galleryCategories);
                console.log(response.data);
            });
        }
    });