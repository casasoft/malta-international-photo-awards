﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIOMatic.Attributes;
using UIOMatic.Enums;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
namespace CSTemplate.Code.Models.Pocos
{
    [TableName("pocoPhotos")]
    [PrimaryKey("Id", autoIncrement = true)]
    [ExplicitColumns]
    [UIOMatic("pocoPhotos", "Photos", "Photo", FolderIcon = "icon-flag", ItemIcon = "icon-flag", ShowOnSummaryDashboard = true, RenderType = UIOMaticRenderType.List)]
    public class PhotoPoco
    {
        [Column("Id")]
        [PrimaryKeyColumn(Name = "Id", AutoIncrement = true)]
        [UIOMaticListViewField(Name = "Id")]
        public int Id { get; set; }

        [UIOMaticField(Name = "SubmissionId", Description = "SubmissionId", View = UIOMatic.Constants.FieldEditors.Label)]
        [UIOMaticListViewField(Name = "SubmissionId")]
        [Column("SubmissionId")]
        public int SubmissionId { get; set; }
        [ResultColumn]
        public SubmissionPoco Submission { get; set; }

        [Required]
        [UIOMaticField(Name = "Featured", Description = "Please select featured attribute value")]
        [UIOMaticListViewField(Name = "Featured")]
        [Column("Featured")]
        public bool Featured { get; set; }

        [Required]
        [UIOMaticField(Name = "PhotoUrl", Description = "Please select the photo", View = UIOMatic.Constants.FieldEditors.File)]
        [UIOMaticListViewField(Name = "PhotoUrl")]
        [Column("PhotoUrl")]
        public string PhotoUrl { get; set; }

      
    }
}