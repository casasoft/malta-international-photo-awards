﻿using CS.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web.Routing;

namespace CS.Common.Umbraco.Services.Umbraco.Wrappers.Services
{
    public interface IPublishedContentRequestWrapperService
    {
        void UpdateContentAndTemplate(PublishedContentRequest publishedContentRequest, IPublishedContent content, string templateAlias);
    }

    [Service]
    public class PublishedContentRequestWrapperService : IPublishedContentRequestWrapperService
    {
        public void UpdateContentAndTemplate(PublishedContentRequest publishedContentRequest, IPublishedContent content, string templateAlias)
        {
            publishedContentRequest.TrySetTemplate(templateAlias);
            publishedContentRequest.PublishedContent = content;
        }
    }
}
