﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Helpers;
using CS.Common.SendGrid.Wrappers.Services;
using CS.Common.Services;
using CS.Common.Umbraco.Services.RazorEngine;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Register;
using CSTemplate.Code.Services.Emails;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;
using Enums = CS.Common.Constants.Enums;

namespace CSTemplate.Code.Services.Registration
{

    public interface ISendEmailsAfterRegistrationService
    {
        void SendEmails(UmbracoHelper umbracoHelper, Umbraco.Web.PublishedContentModels.Member memberModel, string newMemberEmailAddress);
    }

    [Service]
    public class SendEmailsAfterRegistrationService : ISendEmailsAfterRegistrationService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly ISendEmailService _sendEmailService;
        private readonly IRazorEngineCompileTemplate _razorEngineCompileTemplate;
        private readonly IGetEmailTemplatesPlaceholderForCurrentDomainRootNodeService _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService;

        public SendEmailsAfterRegistrationService(IUmbracoHelperService
            umbracoHelperService,
            ISendEmailService
            sendEmailService,
            IRazorEngineCompileTemplate
            razorEngineCompileTemplate,
            IGetEmailTemplatesPlaceholderForCurrentDomainRootNodeService
            getEmailTemplatesPlaceholderForCurrentDomainRootNodeService)
        {
            _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService = getEmailTemplatesPlaceholderForCurrentDomainRootNodeService;
            _razorEngineCompileTemplate = razorEngineCompileTemplate;
            _sendEmailService = sendEmailService;
            _umbracoHelperService = umbracoHelperService;
        }

        public void SendEmails(UmbracoHelper umbracoHelper, Umbraco.Web.PublishedContentModels.Member memberModel, string newMemberEmailAddress)
        {
            var emailTemplates =
               _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService.Get(umbracoHelper);

            var fromMailAddress = new MailAddress(CS.Common.Helpers.SmtpHelpers.SmtpFrom, emailTemplates.EmailSenderName);

            //new ContactPage(_umbracoHelperService.TypedContent(umbracoHelper.UmbracoContext, ConstantValues.ContactPageId)).ReceivingMessagesOn;
            var sendGridTemplateID = ConstantValues.Default_SendGrid_Template_ID;

            var enumCountry = (Enums.CountryIso3166)Enum.Parse(typeof(Enums.CountryIso3166), memberModel.Country);
            var registrationEmailModel = new RegistrationEmailModel()
            {
                MemberName = memberModel.ClientName,
                MemberSurname = memberModel.ClientSurname,
                MemberCountry = enumCountry.GetDisplayName(),
                MemberEmail = newMemberEmailAddress,
                MemberNationality = memberModel.Nationality,
                MemberPhone = memberModel.Phone,
                MemberAgreeToReceivePromotionalMaterials = memberModel.AgreeToReceivePromotionalMaterials ? "Yes" : "No"
            };

            var emailTemplateForNewMember = new EmailTemplate(emailTemplates.Children.FirstOrDefault(x => x.Id.Equals(1516)));
            var emailTemplateForAdministration = new EmailTemplate(emailTemplates.Children.FirstOrDefault(x => x.Id.Equals(1515)));

            var keyForNewMemberEmailBody = $"{emailTemplateForNewMember.Name}-{"body"}-{emailTemplateForNewMember.UpdateDate}";
            var keyForAdministrationEmailBody = $"{emailTemplateForAdministration.Name}-{"body"}-{emailTemplateForAdministration.UpdateDate}";

            var keyForNewMemberEmailSubject = $"{emailTemplateForNewMember.Name}-{"subject"}-{emailTemplateForNewMember.UpdateDate}";
            var keyForAdministrationEmailSubject = $"{emailTemplateForAdministration.Name}-{"subject"}-{emailTemplateForAdministration.UpdateDate}";

            var newMemberEmailSubject = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplateForNewMember.Subject, keyForNewMemberEmailSubject, registrationEmailModel);
            var administrationEmailSubject = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplateForAdministration.Subject, keyForAdministrationEmailSubject, registrationEmailModel);

            var newMemberEmailBody = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplateForNewMember.EmailBody.ToString(), keyForNewMemberEmailBody, registrationEmailModel);
            var administrationEmailBody = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplateForAdministration.EmailBody.ToString(), keyForAdministrationEmailBody, registrationEmailModel);

            //send Email to New Member
            _sendEmailService.SendEmail(umbracoHelper, newMemberEmailSubject, newMemberEmailBody, newMemberEmailAddress,
                null, null, fromMailAddress);

            //send Email To Administration
            _sendEmailService.SendEmail(umbracoHelper, administrationEmailSubject, administrationEmailBody, emailTemplates.AdministrationEmailReceiver,
                null, null, fromMailAddress);

        }
    }
}
