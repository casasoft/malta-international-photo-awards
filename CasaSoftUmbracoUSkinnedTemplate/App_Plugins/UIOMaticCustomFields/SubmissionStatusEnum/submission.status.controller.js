﻿angular.module("umbraco").controller("MySubmissionStatuses",
    function ($scope, $http) {
        $scope.init = function () {
            $http.get("backoffice/SubmissionStatus/SubmissionStatusApi/GetAll").then(function (response) {
                $scope.persons = response.data;
            });
        };
        $scope.propertyValue = function () {
            
            return $scope.property.value;
        };
    });
