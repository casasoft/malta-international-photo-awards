﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared.AwardCategories;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.AwardCategory
{

    public interface IGetAwardCategoryStoryTellingService
    {
        List<SelectListItem> Get(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class GetAwardCategoryStoryTellingService : IGetAwardCategoryStoryTellingService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetAwardCategoryStoryTellingService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public List<SelectListItem> Get(UmbracoHelper umbracoHelper)
        {

            List<SelectListItem> returnofAwardCategoryList = new List<SelectListItem>();
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;

            var awardEditionsNode = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionPageData)x).Where(x => DateTime.Now.Date >= x.StartedDate && DateTime.Now <= x.EndedDate).FirstOrDefault();

            if (awardEditionsNode!=null)
            {
                var awardCategories = awardEditionsNode.Descendants()
                    .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Category_Page_Data_Document_Type_Alias))
                    .Select(x => (AwardEditionCategoryPageData)x).Where(x => x.IsStoryTelling == true).ToList();

                foreach (var awardCategory in awardCategories)
                {
                    returnofAwardCategoryList.Add(new SelectListItem()
                    {
                        Text = awardCategory.Name,
                        Value = awardCategory.Id.ToString()
                    });
                }
            }

           
            return returnofAwardCategoryList;
        }
    }
}
