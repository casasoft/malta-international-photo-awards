﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models;
using CSTemplate.Code.Models.GoogleReCaptcha;
using CSTemplate.Code.Models.Register;
using CSTemplate.Code.Models.Shared;
using Newtonsoft.Json;
using Umbraco.Web.Models;
using SelectListItem = System.Web.WebPages.Html.SelectListItem;


namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class RegisterPageDataController : USNBaseController
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        public RegisterPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _umbracoHelperService = umbracoHelperService;
        }
        // GET: RegisterPageData
        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult RegisterPage(RenderModel model)
        {

            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var explanationText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    Enums.DictionaryKey.RegisterPage_Loader_Explanation_Text);
            var loaderTitle = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    Enums.DictionaryKey.RegisterPage_Loader_Title_Text);
            RegisterPageModel registerPageModel = new RegisterPageModel(model.Content);
            registerPageModel.LoaderPageModel = new CSTemplate.Code.Models.Shared.Loader._LoaderPageModel
            {
                ExplanationText = explanationText,
                LoaderTitle = loaderTitle
            };
            var termsAndConditionsRawText =
                _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    Enums.DictionaryKey.Dictionary_Form_Terms_And_Conditions_Text);
            var termsAndConditionsUrl = _umbracoHelperService.TypedContent(umbracoHelper, CS.Common.Umbraco.Constants.ConstantValues.Terms_And_Conditions_Page_Id).Url;

            var termsAndConditionsFormatted = String.Format(termsAndConditionsRawText, termsAndConditionsUrl);

            var yourdetailsText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Register_Page_Title__Your_Details_Form);
            var accountDetailsText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Register_Page_Title__Acount_Details_Form);
            var requiredText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Dictionary_Form_Required_Text);

            var promotialMaterialText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Dictionary_Form_Promotional_Material_Text);

            var registerButtonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Register_Page_Button_Text);


            registerPageModel.RegisterForm = new _RegisterFormModel()
            {
                
                YourDetailsSectionTitle = yourdetailsText,
                AccountDetailsSectionTitle = accountDetailsText,
                RegisterRequiredField = requiredText,
                TermsAndConditionsText = termsAndConditionsFormatted,
                PromotionalMaterialsDescriptionParagraph = promotialMaterialText,
                ButtonFormText = registerButtonText,
                IsRegistrationProcess = true,
                
            };



            return base.Populate(registerPageModel);
        }

    }
}