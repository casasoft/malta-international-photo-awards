﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared.Order;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Orders
{

    public interface IGetOrderDetailsFilterByOrderId
    {
        _OrderItemModel Get(int orderId);
    }

    [Service]
    public class GetOrderDetailsFilterByOrderId : IGetOrderDetailsFilterByOrderId
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetOrderDetailsFilterByOrderId(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public _OrderItemModel Get(int orderId)
        {
            var orderItemModel = new _OrderItemModel();
            if (orderId <= 0) return orderItemModel;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var orderTableName = Constants.DatabaseConstantValues.OrdersTableName;
            var orderPrimaryKeyField = Constants.DatabaseConstantValues.Orders_Poco_Primary_Key_Value;
            var orderStatus = Constants.DatabaseConstantValues.Orders_Payment_Status_Enum_Id_Field_Name;
            var deleted = (int)Constants.Enums.PaymentStatus.DELETED;
            var sqlQuery = new Sql().Select("*").From(orderTableName).Where($"{orderPrimaryKeyField}={orderId} AND {orderStatus} != {deleted}");
            var sqlQueryResult = umbracoDb.Fetch<OrderPoco>(sqlQuery).FirstOrDefault();
            if(sqlQueryResult!=null)
            {
                orderItemModel.OrderItemId = sqlQueryResult.Id;
                orderItemModel.Date = sqlQueryResult.Date;
                orderItemModel.Description = sqlQueryResult.Description;
                orderItemModel.OrderRefId = sqlQueryResult.OrderRefId;
                orderItemModel.PSPID = sqlQueryResult.PSPID;
                orderItemModel.PaymentStatusEnumId = (CSTemplate.Code.Constants.Enums.PaymentStatus)sqlQueryResult.PaymentStatusEnumId;
                orderItemModel.UserId = sqlQueryResult.UserId;
                orderItemModel.OrderId = sqlQueryResult.Id;
            }
            

            return orderItemModel;
        }
    }
}
