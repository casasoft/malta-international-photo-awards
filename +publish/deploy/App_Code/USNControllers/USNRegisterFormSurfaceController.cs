﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using USN.USNModels;


namespace USN.USNControllers
{
    public class USNRegisterFormSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        // GET: USNRegisterFormSurface
        public ActionResult Index()
        {
            var model = new USNRegisterFromViewModel();
            model.RegisterTitle = "New user?";
            model.RegisterText = "Create an account!";
            model.RegisterButtonText = " REGISTER ACCOUNT";
            model.RegisterUrl = "/login/register";


            return PartialView("USNForms/USN_RegisterForm", model);
        }
    }
}