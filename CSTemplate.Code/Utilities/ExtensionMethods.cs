﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CS.Common.Umbraco.Helpers;
using CSTemplate.Code.Constants;
using Umbraco.Core.Models;
using Umbraco.Web;
using CSTemplate.Code.Models.ModelsBuilder;
using Umbraco.Core.Logging;
using Umbraco.Web.Models;
using CS.Common.Helpers;
using CSTemplate.Code.Services.Contact;
using CSTemplate.Code.Services.Error;

namespace CSTemplate.Code.Utilities
{
    public static class ExtensionMethods
    {
        private static UmbracoHelper Umbraco
        {
            get { return new UmbracoHelper(UmbracoContext.Current); }
        }

        #region IPublishedContent - Methods

        #region Node

        /// <summary>
        /// Return the HomePage generated model (highest node) where default settings are stored.
        /// </summary>
        public static HomePageData Website(this IPublishedContent content, bool noCache = false)
        {
            //var website = (HomePage)System.Web.HttpContext.Current.Items["Home"];

            //if (website == null || noCache)
            //{

            //    website = (HomePage)content.AncestorOrSelf(1);

            //    if (!noCache)
            //    {
            //        System.Web.HttpContext.Current.Items["Home"] = website;
            //    }
            //}

            return content.Id == ConstantValues.ContentIdHome ? new HomePageData(content) : new HomePageData(content.AncestorOrSelf(1));
        }

        #endregion

        #endregion

        #region ImageCropDataSet - Methods

        public static string GetCropUrl(
            this ImageCropDataSet cropDataSet,
            int? width = null,
            int? height = null,
            string cropAlias = null,
            int? quality = null,
            ImageCropMode? imageCropMode = null,
            ImageCropAnchor? imageCropAnchor = null,
            bool preferFocalPoint = false,
            bool useCropDimensions = true,
            string cacheBusterValue = null,
            string furtherOptions = null,
            ImageCropRatioMode? ratioMode = null,
            bool upScale = true
        )
        {
            var imageResizerUrl = new StringBuilder();

            imageResizerUrl.Append(cropDataSet.Src);

            if (imageCropMode == ImageCropMode.Crop || imageCropMode == null)
            {
                var crop = cropDataSet.GetCrop(cropAlias);

                var cropBaseUrl = cropDataSet.GetCropBaseUrl(cropAlias, preferFocalPoint);
                if (cropBaseUrl != null)
                {
                    imageResizerUrl.Append(cropBaseUrl);
                }
                else
                {
                    return null;
                }

                if (crop != null & useCropDimensions)
                {
                    width = crop.Width;
                    height = crop.Height;
                }
            }
            else
            {
                imageResizerUrl.Append("?mode=" + imageCropMode.ToString().ToLower());

                if (imageCropAnchor != null)
                {
                    imageResizerUrl.Append("&anchor=" + imageCropAnchor.ToString().ToLower());
                }
            }

            if (quality != null)
            {
                imageResizerUrl.Append("&quality=" + quality);
            }

            if (width != null && ratioMode != ImageCropRatioMode.Width)
            {
                imageResizerUrl.Append("&width=" + width);
            }

            if (height != null && ratioMode != ImageCropRatioMode.Height)
            {
                imageResizerUrl.Append("&height=" + height);
            }

            if (ratioMode == ImageCropRatioMode.Width && height != null)
            {
                //if only height specified then assume a sqaure
                if (width == null)
                {
                    width = height;
                }
                var widthRatio = (decimal)width / (decimal)height;
                imageResizerUrl.Append("&widthratio=" + widthRatio.ToString(CultureInfo.InvariantCulture));
            }

            if (ratioMode == ImageCropRatioMode.Height && width != null)
            {
                //if only width specified then assume a sqaure
                if (height == null)
                {
                    height = width;
                }
                var heightRatio = (decimal)height / (decimal)width;
                imageResizerUrl.Append("&heightratio=" + heightRatio.ToString(CultureInfo.InvariantCulture));
            }

            if (upScale == false)
            {
                imageResizerUrl.Append("&upscale=false");
            }

            if (furtherOptions != null)
            {
                imageResizerUrl.Append(furtherOptions);
            }

            if (cacheBusterValue != null)
            {
                imageResizerUrl.Append("&rnd=").Append(cacheBusterValue);
            }

            return imageResizerUrl.ToString();
        }

        public static ImageCropData GetCrop(this ImageCropDataSet dataset, string cropAlias)
        {
            if (dataset == null || dataset.Crops == null || !dataset.Crops.Any())
                return null;

            return dataset.Crops.GetCrop(cropAlias);
        }

        public static ImageCropData GetCrop(this IEnumerable<ImageCropData> dataset, string cropAlias)
        {
            if (dataset == null || !dataset.Any())
                return null;

            if (string.IsNullOrEmpty(cropAlias))
                return dataset.FirstOrDefault();

            return dataset.FirstOrDefault(x => x.Alias.ToLowerInvariant() == cropAlias.ToLowerInvariant());
        }

        public static string GetCropBaseUrl(this ImageCropDataSet cropDataSet, string cropAlias, bool preferFocalPoint)
        {
            var cropUrl = new StringBuilder();

            var crop = cropDataSet.GetCrop(cropAlias);

            // if crop alias has been specified but not found in the Json we should return null
            if (string.IsNullOrEmpty(cropAlias) == false && crop == null)
            {
                return null;
            }
            if ((preferFocalPoint && cropDataSet.HasFocalPoint()) || (crop != null && crop.Coordinates == null && cropDataSet.HasFocalPoint()) || (string.IsNullOrEmpty(cropAlias) && cropDataSet.HasFocalPoint()))
            {
                cropUrl.Append("?center=" + cropDataSet.FocalPoint.Top.ToString(CultureInfo.InvariantCulture) + "," + cropDataSet.FocalPoint.Left.ToString(CultureInfo.InvariantCulture));
                cropUrl.Append("&mode=crop");
            }
            else if (crop != null && crop.Coordinates != null)
            {
                cropUrl.Append("?crop=");
                cropUrl.Append(crop.Coordinates.X1.ToString(CultureInfo.InvariantCulture)).Append(",");
                cropUrl.Append(crop.Coordinates.Y1.ToString(CultureInfo.InvariantCulture)).Append(",");
                cropUrl.Append(crop.Coordinates.X2.ToString(CultureInfo.InvariantCulture)).Append(",");
                cropUrl.Append(crop.Coordinates.Y2.ToString(CultureInfo.InvariantCulture));
                cropUrl.Append("&cropmode=percentage");
            }
            else
            {
                cropUrl.Append("?anchor=center");
                cropUrl.Append("&mode=crop");
            }
            return cropUrl.ToString();
        }

        #endregion

        #region Error

        /// <summary>
        /// Log an exception and send an email.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="nodeId"></param>
        /// <param name="type"></param>
        public static void LogException<T>(this UmbracoHelper umbraco, Exception ex)
        {
            try
            {
                int nodeId = -1;
                if (System.Web.HttpContext.Current.Items["pageID"] != null)
                {
                    int.TryParse(System.Web.HttpContext.Current.Items["pageID"].ToString(), out nodeId);
                }

                StringBuilder comment = new StringBuilder();
                StringBuilder commentHtml = new StringBuilder();

                commentHtml.AppendFormat("<p><strong>Url:</strong><br/>{0}</p>", System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
                commentHtml.AppendFormat("<p><strong>Node id:</strong><br/>{0}</p>", nodeId);

                //Add the exception.
                comment.AppendFormat("Exception: {0} - StackTrace: {1}", ex.Message, ex.StackTrace);
                commentHtml.AppendFormat("<p><strong>Exception:</strong><br/>{0}</p>", ex.Message);
                commentHtml.AppendFormat("<p><strong>StackTrace:</strong><br/>{0}</p>", ex.StackTrace);

                if (ex.InnerException != null)
                {
                    //Add the inner exception.
                    comment.AppendFormat("- InnerException: {0} - InnerStackTrace: {1}", ex.InnerException.Message, ex.InnerException.StackTrace);
                    commentHtml.AppendFormat("<p><strong>InnerException:</strong><br/>{0}</p>", ex.InnerException.Message);
                    commentHtml.AppendFormat("<p><strong>InnerStackTrace:</strong><br/>{1}</p>", ex.InnerException.StackTrace);
                }

                //Log the Exception into Umbraco.
                LogHelper.Error<T>(comment.ToString(), ex);

                var service = IocHelpers.Container.GetInstance<ISendErrorEmailService>();
                service.SendEmail(umbraco, commentHtml.ToString());


            }
            catch
            {
                //Do nothing because nothing can be done with this exception.
            }
        }

        #endregion

    }
}
