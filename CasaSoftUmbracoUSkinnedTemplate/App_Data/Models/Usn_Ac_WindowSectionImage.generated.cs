//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>Window Image</summary>
	[PublishedContentModel("USN_AC_WindowSectionImage")]
	public partial class Usn_Ac_WindowSectionImage : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "USN_AC_WindowSectionImage";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Usn_Ac_WindowSectionImage(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Usn_Ac_WindowSectionImage, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Add color overlay: If “Yes” is selected a semi transparent block of the background color is added over your image.
		///</summary>
		[ImplementPropertyType("addColorOverlay")]
		public bool AddColorOverlay
		{
			get { return this.GetPropertyValue<bool>("addColorOverlay"); }
		}

		///<summary>
		/// Background color: Color selected will dictate the color to be used for any text. If a background image is used, select the color that works best for the text over the image.
		///</summary>
		[ImplementPropertyType("backgroundColor")]
		public string BackgroundColor
		{
			get { return this.GetPropertyValue<string>("backgroundColor"); }
		}

		///<summary>
		/// Image: Image will be cropped to one of the following sizes depending on order within this section.  800px x 800px 800px x 400px  Focal point is defined within your "Media" section.
		///</summary>
		[ImplementPropertyType("backgroundImage")]
		public IPublishedContent BackgroundImage
		{
			get { return this.GetPropertyValue<IPublishedContent>("backgroundImage"); }
		}

		///<summary>
		/// Button color
		///</summary>
		[ImplementPropertyType("buttonColor")]
		public string ButtonColor
		{
			get { return this.GetPropertyValue<string>("buttonColor"); }
		}

		///<summary>
		/// Heading: Your heading hierarchy is important for SEO. This will not affect font size.
		///</summary>
		[ImplementPropertyType("heading")]
		public USNStarterKit.USNHeading.Models.USNHeading Heading
		{
			get { return this.GetPropertyValue<USNStarterKit.USNHeading.Models.USNHeading>("heading"); }
		}

		///<summary>
		/// Image alt text
		///</summary>
		[ImplementPropertyType("imageAltText")]
		public string ImageAltText
		{
			get { return this.GetPropertyValue<string>("imageAltText"); }
		}

		///<summary>
		/// Name: The name you enter is for your reference only and will not appear on the website.
		///</summary>
		[ImplementPropertyType("itemName")]
		public string ItemName
		{
			get { return this.GetPropertyValue<string>("itemName"); }
		}

		///<summary>
		/// Link: The "Caption" field will be used for your link text.
		///</summary>
		[ImplementPropertyType("link")]
		public Umbraco.Web.Models.RelatedLinks Link
		{
			get { return this.GetPropertyValue<Umbraco.Web.Models.RelatedLinks>("link"); }
		}

		///<summary>
		/// Secondary heading
		///</summary>
		[ImplementPropertyType("secondaryHeading")]
		public string SecondaryHeading
		{
			get { return this.GetPropertyValue<string>("secondaryHeading"); }
		}

		///<summary>
		/// Text
		///</summary>
		[ImplementPropertyType("text")]
		public IHtmlString Text
		{
			get { return this.GetPropertyValue<IHtmlString>("text"); }
		}
	}
}
