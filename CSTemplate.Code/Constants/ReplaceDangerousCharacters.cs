﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Constants
{
    public static class ReplaceDangerousCharacters
    {
        public static string Replace(string content)
        {
            string values = content;
            values = values.Replace("'", "");
            values = values.Replace("!", "");
            values = values.Replace(" ", "");
            values = values.Replace(",", "");
            values = values.Replace("<", "");
            values = values.Replace(">", "");
            values = values.Replace("&", "");
            values = values.Replace("[", "");
            values = values.Replace("]", "");
            values = values.Replace("ı", "i");
            values = values.Replace("İ", "i");
            values = values.Replace("ö", "o");
            values = values.Replace("ü", "u");
            values = values.Replace("Ü", "u");
            values = values.Replace("Ö", "o");
            values = values.Replace("ç", "c");
            values = values.Replace("Ç", "c");
            values = values.Replace("Ş", "s");
            values = values.Replace("ş", "s");
            values = values.Replace("Ğ", "g");
            values = values.Replace("ğ", "g");

            return values;

        }
    }
}
