﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shortlist_Photos;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Submissions;

namespace CSTemplate.Code.Services.ShortListedPhotos
{

    public interface ISubmitAsAShortListPhotoService
    {
        void Submit(ShortlistPhotosFormModel shortlistPhotosFormModel);
    }

    [Service]
    public class SubmitAsAShortListPhotoService : ISubmitAsAShortListPhotoService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetSubmissionPocoDetailsFilterByIdService _getSubmissionPocoDetailsFilterByIdService;

        public SubmitAsAShortListPhotoService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService, IGetSubmissionPocoDetailsFilterByIdService getSubmissionPocoDetailsFilterByIdService)
        {
            _getSubmissionPocoDetailsFilterByIdService = getSubmissionPocoDetailsFilterByIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public void Submit(ShortlistPhotosFormModel shortlistPhotosFormModel)
        {
                 	
            if (shortlistPhotosFormModel.ListOfPhotos==null) return;
            if (shortlistPhotosFormModel.ListOfPhotos.Count <= 0) return;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var submissionTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            foreach (var photo in shortlistPhotosFormModel.ListOfPhotos)
            {
                var submission = _getSubmissionPocoDetailsFilterByIdService.Get(int.Parse(photo.SubmissionId));
                submission.ShortListedEnumId = (int)photo.ChosenRadioValue;
                if (photo.ChosenRadioValue==Enums.ShortListedStatus.Yes)
                {
                    submission.ShortListedDate = DateTime.Now;
                    umbracoDb.Update(submissionTableName, submissionPrimaryKeyField, submission);
                }
                if (photo.ChosenRadioValue==Enums.ShortListedStatus.No)
                {
                    umbracoDb.Update(submissionTableName, submissionPrimaryKeyField, submission);
                }
                
            };
        }
    }
}
