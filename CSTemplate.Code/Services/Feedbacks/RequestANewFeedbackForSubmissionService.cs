﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.OrderItems;
using CSTemplate.Code.Services.Orders;
using CSTemplate.Code.Services.Submissions;
using Umbraco.Core.Persistence;
using Umbraco.Web;
using databaseConstants = CSTemplate.Code.Constants.DatabaseConstantValues;
namespace CSTemplate.Code.Services.Feedbacks
{

    public interface IRequestANewFeedbackForSubmissionService
    {
        int Request(UmbracoHelper umbracoHelper,int submissionId,int userId);
    }

    [Service]
    public class RequestANewFeedbackForSubmissionService : IRequestANewFeedbackForSubmissionService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IAddNewOrderService _addNewOrderService;
        private readonly IAddNewOrderItemService _addNewOrderItemService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IUpdateSubmissionFeedbackRequestStatusFilterBySubmissionIdService _updateSubmissionFeedbackRequestStatusFilterBySubmissionIdService;
        private readonly IGetSubmissionPocoDetailsFilterByIdService _getSubmissionPocoDetailsFilterByIdService;

        public RequestANewFeedbackForSubmissionService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IAddNewOrderService addNewOrderService, 
            IAddNewOrderItemService addNewOrderItemService,
            IUmbracoHelperService umbracoHelperService,
            IUpdateSubmissionFeedbackRequestStatusFilterBySubmissionIdService updateSubmissionFeedbackRequestStatusFilterBySubmissionIdService,
            IGetSubmissionPocoDetailsFilterByIdService getSubmissionPocoDetailsFilterByIdService
            )
        {
            _getSubmissionPocoDetailsFilterByIdService = getSubmissionPocoDetailsFilterByIdService;
            _updateSubmissionFeedbackRequestStatusFilterBySubmissionIdService = updateSubmissionFeedbackRequestStatusFilterBySubmissionIdService;
            _umbracoHelperService = umbracoHelperService;
            _addNewOrderItemService = addNewOrderItemService;
            _addNewOrderService = addNewOrderService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public int Request(UmbracoHelper umbracoHelper, int submissionId, int userId)
        {
           
            var feedbackOrderDescription = string.Format("Selected {0} feedback request.", "1");
            var newOrder = _addNewOrderService.Add(userId,feedbackOrderDescription);
            var orderId = Convert.ToInt32(newOrder);
            var submissionDetail = _getSubmissionPocoDetailsFilterByIdService.Get(submissionId);
            var isStoryTelling = submissionDetail.IsAStoryTelling;
            var requestForFeedback = Convert.ToBoolean(Constants.Enums.RequestFeedback.Yes);
            var paymentForSubmission = Convert.ToBoolean(Constants.Enums.PaymentForSubmission.No);
            var newOrderItem = _addNewOrderItemService.Add(umbracoHelper, submissionId, orderId, isStoryTelling, requestForFeedback, paymentForSubmission);
            var updateSubmissionFeedbackRequestResult = _updateSubmissionFeedbackRequestStatusFilterBySubmissionIdService.Update(umbracoHelper,submissionId);
            var processResult = newOrderItem == true ? Constants.ConstantValues.Process_Successful : Constants.ConstantValues.Process_Failed;

            return orderId;
        }
    }
}
