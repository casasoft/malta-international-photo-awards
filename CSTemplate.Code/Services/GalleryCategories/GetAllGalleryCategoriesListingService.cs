﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebPages.Html;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.GalleryCategories
{

    public interface IGetAllGalleryCategoriesListingService
    {
        List<SelectListItem> Get(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class GetAllGalleryCategoriesListingService : IGetAllGalleryCategoriesListingService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetAllGalleryCategoriesListingService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public List<SelectListItem> Get(UmbracoHelper umbracoHelper)
        {
            var list = new List<SelectListItem>();
            var categoryPlaceHolderAlias = Constants.ConstantValues.Categories_Place_Holder_Document_Type_Alias;
            var categoriesAlias = Constants.ConstantValues.Category_Gallery_Page_Data_Document_Type_Alias;
            var galleryCategoriesList = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, categoryPlaceHolderAlias)
                .Descendants()
                .Where(x=>x.DocumentTypeAlias.Equals(categoriesAlias))
                .Select(x=>(CategoryGalleryPageData)x)
                .ToList();
            foreach (var categoryGalleryPageData in galleryCategoriesList)
            {
                list.Add(new SelectListItem()
                {
                    Value = categoryGalleryPageData.Id.ToString(),
                    Text = categoryGalleryPageData.Name
                });
            }
            return list;
        }
    }
}
