﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;

namespace CS.Common.Helpers
{
    public static class ReflectionHelpers
    {
        public static TAttribute GetAttributeFromType<TAttribute>(
          Type type)
          where TAttribute : Attribute
        {
            return (TAttribute)GetAttributeFromType(type, typeof(TAttribute)) as TAttribute;
        }

        public static object GetAttributeFromType(
            Type type,
            Type attributeType)
        {
            object att = type.GetCustomAttributes(
                attributeType,
                true
                ).FirstOrDefault();
            return att;
        }

        public static IEnumerable<Type> GetAllTypesWithAttribute<T>() where T : Attribute
        {
            return GetAllTypesWithAttribute(typeof(T));
        }

        public static IEnumerable<Type> GetAllTypesWithAttribute(Type attributeType)
        {
            IEnumerable<Type> types = GetAllTypesInCurrentDomainAssemblies().Where(x => x.GetCustomAttributes(attributeType, true).Length > 0);
            return types;
        }
        public static IEnumerable<Type> GetAllTypesExtendingFromType<T>()
        {
            var type = typeof(T);
            return GetAllTypesExtendingFromType(type);
        }

        public static IEnumerable<Type> GetAllTypesInCurrentDomainAssemblies()
        {
            IEnumerable<Type> types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes());
            return types;
        }

        public static IEnumerable<Type> GetAllTypesExtendingFromType(Type type)
        {
            IEnumerable<Type> types = GetAllTypesInCurrentDomainAssemblies()
                .Where(p => type.IsAssignableFrom(p));
            return types;
        }

        public static object CreateObjectWithAnyConstructor(Type typeToCreate, params object[] parameters)
        {
            ConstructorInfo constructor = null;

            try
            {
                // Binding flags exclude public constructors.
                constructor =
                    typeToCreate.GetConstructors()
                        .Where(x => x.GetParameters().Length == parameters.Length)
                        .FirstOrDefault();
                //constructor = typeToCreate.GetConstructor(BindingFlags.Instance | BindingFlags.Public, null, new Type[0], null);
            }
            catch (Exception)
            {
                throw;
            }

            if (constructor == null || constructor.IsAssembly)
                // Also exclude internal constructors.
                throw new InvalidOperationException(string.Format("A private or " +
                        "protected constructor is missing for '{0}'.", typeToCreate.Name));
            object _instance = null;
            try
            {
                _instance = constructor.Invoke(parameters);
            }
            catch (MemberAccessException ex)
            {
                var m = Regex.Match(ex.Message, "Cannot create an instance of (.*?)Factory because it is an abstract class.");
                if (m.Success)
                {
                    string factoryName = m.Groups[1].Value + "Factory";
                    string msg = ex.Message +
                                 " - If you are using this factory, make sure you mark it as 'UsedInProject' in the builder";
                    throw new InvalidOperationException(msg);
                }
                else
                {
                    throw;
                }


            }
            catch (Exception)
            {
                throw;
            }
            return _instance;
        }


        public static T CreateObjectWithAnyConstructor<T>(params object[] parameters)
        {
            return (T)CreateObjectWithAnyConstructor(typeof(T), parameters);

        }

        public static T CreateObjectWithInternalConstructor<T>()
        {
            return (T)FormatterServices.GetUninitializedObject(typeof(T));

        }

        private static PropertyInfo GetPropertyInfo(Type type, string propertyName)
        {
            PropertyInfo propInfo = null;
            do
            {
                propInfo = type.GetProperty(propertyName,
                       BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                type = type.BaseType;
            }
            while (propInfo == null && type != null);
            return propInfo;
        }

        public static object GetPropertyValue(object obj, string propertyName)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            Type objType = obj.GetType();
            PropertyInfo propInfo = GetPropertyInfo(objType, propertyName);
            if (propInfo == null)
                throw new ArgumentOutOfRangeException("propertyName",
                  string.Format("Couldn't find property {0} in type {1}", propertyName, objType.FullName));
            return propInfo.GetValue(obj, null);
        }

        public static void SetPropertyValue(object obj, string propertyName, object val)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            Type objType = obj.GetType();
            PropertyInfo propInfo = GetPropertyInfo(objType, propertyName);
            if (propInfo == null)
                throw new ArgumentOutOfRangeException("propertyName",
                  string.Format("Couldn't find property {0} in type {1}", propertyName, objType.FullName));
            propInfo.SetValue(obj, val, null);
        }

        public static List<MemberInfo> GetAllPropertiesAndFieldsForObjct<T>(T obj,
            bool loadNonPublicTypes,
            bool loadStaticProperties,
            bool loadAlsoFromInterfaces,
            List<Expression<Func<T, object>>> excludeProperties = null)
        {
            var properties = GetAllPropertiesAndFieldsForType(obj.GetType(),
                loadNonPublicTypes,
                loadStaticProperties,
                loadAlsoFromInterfaces);
            if (excludeProperties != null && excludeProperties.Count > 0)
            {
                properties.RemoveAll(x =>
                {
                    foreach (var excludeProperty in excludeProperties)
                    {
                        var omitPropertyName = ExpressionHelper.GetExpressionText(excludeProperty);
                        if (omitPropertyName == x.Name)
                        {
                            return true;
                        }
                    }
                    return false;
                });
            }
            return properties;
        }

        public static List<MemberInfo> GetAllPropertiesAndFieldsForType(Type t, bool loadNonPublicTypes, bool loadStaticProperties, bool loadAlsoFromInterfaces)
        {
            List<MemberInfo> propsAndFields = new List<MemberInfo>();
            propsAndFields.AddRange(GetAllPropertiesForType(t, loadNonPublicTypes, loadStaticProperties, loadAlsoFromInterfaces));
            propsAndFields.AddRange(GetAllFieldsForType(t));
            return propsAndFields;
        }
        public static PropertyInfo[] GetAllPropertiesForType(Type t, bool loadNonPublicTypes, bool loadStaticProperties,
            bool loadAlsoFromInterfaces)
        {

            List<PropertyInfo> properties = new List<PropertyInfo>();
            List<Type> typesToLoad = new List<Type>();
            typesToLoad.Add(t);
            if (!t.IsClass && loadAlsoFromInterfaces)
            {
                var allInterfaces = t.GetInterfaces().ToList();
                foreach (var intrface in allInterfaces)
                {
                    typesToLoad.Add(intrface);
                }

            }
            foreach (var type in typesToLoad)
            {

                var pList = type.GetProperties(getBindingFlags(
                    loadNonPublicTypes: loadNonPublicTypes,
                    loadStaticProperties: loadStaticProperties));
                foreach (var p in pList)
                {
                    properties.Add(p);
                }

            }
            return properties.ToArray();

        }

        /// <summary>
        /// Important: This will return the last property name in the expression.
        /// i.e 'Name' from MemberData.ContactDetails.Name
        /// </summary>
        /// <typeparam name="T">object type</typeparam>
        /// <param name="selector">expression</param>
        /// <returns></returns>
        public static string GetPropertyName<T>(Expression<Func<T, object>> selector)
        {

            var pINfo = GetPropertyBySelector(selector);
            return pINfo.Name;


        }


        public static object GetPrivateVariableFromStaticObject(Type t, string name)
        {

            var field = t.GetField(name, BindingFlags.NonPublic | BindingFlags.Static);
            object value = field.GetValue(null);
            return value;

        }


        public static PropertyInfo GetPropertyBySelector<T>(Expression<Func<T, object>> selector)
        {
            return GetPropertyBySelector((Expression)selector);


        }
        
        public static object CreateObject(Type t)
        {

            return CreateObjectWithAnyConstructor(t);
        }
        public static T CreateObjectWithAnyConstructor<T>()
        {
            return (T)CreateObjectWithAnyConstructor(typeof(T));

        }
       

       

        public static List<TAttribute> GetAttributesForMemberInfo<TAttribute>(MemberInfo p) where TAttribute : Attribute
        {


            var attrs = Attribute.GetCustomAttributes(p, typeof(TAttribute), true).Cast<TAttribute>();
            return attrs.ToList();
        }



        //-------------


        public static object GetMaxValueForBasicDataType(Type type)
        {
            Type typeToCheck = GetDataTypeForNullableTypeIfNullable(type);

            if (typeToCheck == typeof(DateTime))
                return DateTime.MaxValue;
            else if (typeToCheck == typeof(int))
                return int.MaxValue;
            else if (typeToCheck == typeof(long))
                return long.MaxValue;
            else if (typeToCheck == typeof(double))
                return double.MaxValue;
            else if (typeToCheck == typeof(short))
                return short.MaxValue;
            else if (typeToCheck == typeof(float))
                return float.MaxValue;
            else
                throw new InvalidOperationException(string.Format("Could not determine value for MaxValue of basic data type <{0}>", typeToCheck.ToString()));
        }
        public static object GetMinValueForBasicDataType(Type type)
        {
            Type typeToCheck = GetDataTypeForNullableTypeIfNullable(type);

            if (typeToCheck == typeof(DateTime))
                return DateTime.MinValue;
            else if (typeToCheck == typeof(int))
                return int.MinValue;
            else if (typeToCheck == typeof(long))
                return long.MinValue;
            else if (typeToCheck == typeof(double))
                return double.MinValue;
            else if (typeToCheck == typeof(short))
                return short.MinValue;
            else if (typeToCheck == typeof(float))
                return float.MinValue;
            else
                throw new InvalidOperationException(string.Format("Could not determine value for MaxValue of basic data type <{0}>", typeToCheck.ToString()));
        }
        /// <summary>
        /// Returns the attribute information, if any.
        /// 

        /// 
        /// </summary>
        /// <param name="methodOrPropertyInfo"></param>
        /// <returns></returns>
        public static T GetAttributeFromObject<T>(object obj)
        //  where T : Attribute
        {
            Attribute attrib = GetAttributeFromObject(obj, typeof(T));
            return (T)(object)attrib;

        }

        public static TAttributeType GetAttributeFromPropertyExpression<TAttributeType>(System.Linq.Expressions.Expression<Func<TAttributeType, object>> propertySelector)
          where TAttributeType : Attribute
        {
            return GetAttributeFromPropertyExpression<TAttributeType>((Expression)propertySelector);

        }
        public static TAttributeType GetAttributeFromPropertyExpression<TAttributeType>(Expression selector)
          where TAttributeType : Attribute
        {
            var memberInfo = GetMemberInfoBySelector(selector);

            if (memberInfo is PropertyInfo)
            {
                var propertyInfo = (PropertyInfo)memberInfo;
                return GetAttributeForMember<TAttributeType>(propertyInfo);
            }
            else
            {
                return null;
            }
        }

        public static IDictionary<string, object> AnonymousObjectToDictionary(object properties)
        {
            var result = new Dictionary<string, object>();

            if (properties != null)
            {
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(properties))
                {
                    result.Add(property.Name, property.GetValue(properties));
                }
            }

            return result;
        }
        /// <summary>
        /// Return the string value of an Enum if the DescriptionAttribute tag is defined (requires System.ComponentModel)
        /// 
        ///[DescriptionAttribute("Food")]
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Attribute GetAttributeFromObject(object value, Type attribType)
        {
            if (value == null) throw new InvalidOperationException("value cannot be NULL");
            FieldInfo fi = value.GetType().GetField(value.ToString());
            if (fi != null)
            {
                Attribute[] attributesList = Attribute.GetCustomAttributes(fi, true);

                if (attributesList.Length > 0)
                {
                    for (int i = 0; i < attributesList.Length; i++)
                    {
                        var attrib = attributesList[i];
                        if (CheckIfObjectExtendsFrom(attrib, attribType))
                        {
                            return attrib;
                        }
                    }


                }
            }
            return null;
        }

        public static object GetDefaultValueOfType(Type t)
        {
            object value = null;
            if (t.IsValueType)
            {
                value = Activator.CreateInstance(t);
            }
            return value;
        }

        public static void SerializeObjectToNameValueCollection<T>(NameValueCollection nv, T obj, bool convertEnumsToIntValue = false)
        {
            SerializeObjectToNameValueCollection(nv, typeof(T), obj, convertEnumsToIntValue);
        }

        public static void SerializeObjectToNameValueCollection(NameValueCollection nv, Type typeToSerialize, object obj, bool convertEnumsToIntValue = false)
        {
            Type objType = obj.GetType();
            if (!typeToSerialize.IsAssignableFrom(objType))
                throw new InvalidOperationException("Object must implement Type <" + typeToSerialize.FullName +
                                                    " to be able to deserialize");
            var properties = GetAllPropertiesForType(typeToSerialize, false, false, true);
            foreach (var p in properties)
            {
                var value = p.GetValue(obj, null);
                string s = null;
                if (value != null)
                    s = ConversionHelpers.ConvertBasicDataTypeToString(value, ifEnumConvertToIntValue: convertEnumsToIntValue);
                nv[p.Name] = s;
            }

        }
        public static void DeserializeObjectFromNameValueCollection<T>(NameValueCollection nv, T obj)
        {


            DeserializeObjectFromNameValueCollection(nv, typeof(T), obj);
        }

        public static void DeserializeObjectFromNameValueCollection(NameValueCollection nv, Type typeToDeserialize, object obj)
        {
            Type objType = obj.GetType();
            if (!typeToDeserialize.IsAssignableFrom(objType))
                throw new InvalidOperationException("Object must implement Type <" + typeToDeserialize.FullName +
                                                    " to be able to deserialize");
            //Type t = obj.GetType();
            var properties = GetAllPropertiesForType(typeToDeserialize, false, false, true);

            foreach (var p in properties)
            {
                if (!p.PropertyType.IsInterface) //odo: this must handle interfaces too
                {
                    string strValue = nv[p.Name];
                    try
                    {
                        object value = strValue;

                        if (p.PropertyType != typeof(string))
                            value = ConversionHelpers.ConvertStringToBasicDataType(strValue, p.PropertyType);
                        if (p.CanWrite)
                            p.SetValue(obj, value, null);
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidOperationException(
                            "Error while trying to parse property '" + p.Name + "' with value <" + strValue + ">", ex);
                    }
                }
            }
        }

       
        /// <summary>
        /// Creates an instance of a generic type
        /// </summary>
        /// <param name="genericMainType">The main type, e.g List[]</param>
        /// <param name="genericParameters">The parameter type, e.g int.</param>
        /// <returns></returns>
        public static object CreateGenericType(Type genericMainType, IEnumerable<Type> genericParameters)
        {
            Type t = GetGenericType(genericMainType, genericParameters);
            return CreateObjectWithAnyConstructor(t);
        }
        /// <summary>
        /// Creates an instance of a generic type
        /// </summary>
        /// <param name="genericMainType">The main type, e.g List[]</param>
        /// <param name="genericParameters">The parameter type, e.g int.</param>
        /// <returns></returns>
        public static object CreateGenericType(Type genericMainType, params Type[] genericParameters)
        {
            return CreateGenericType(genericMainType, (IEnumerable<Type>)genericParameters);

        }
        /// <summary>
        /// Returns a Generic Type. E.g List Of Int
        /// </summary>
        /// <param name="genericMainType"> The main type, e.g List[] </param>
        /// <param name="genericParameters">The parameter type, e.g int.</param>
        /// <returns></returns>
        public static Type GetGenericType(Type genericMainType, params Type[] genericParameters)
        {
            return GetGenericType(genericMainType, (IEnumerable<Type>)genericParameters);
        }
        /// <summary>
        /// Pass an item of type Dict[string,object] and this will return a list with types string & object
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Type[] GetGenericArguments(Type type)
        {
            return type.GetGenericArguments();
        }
        /// <summary>
        /// Returns the underlying generic type of a type wth generics, e.g. List[int] returns int
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Type GetUnderlyingType(Type type)
        {
            if (type.IsGenericType)
            {
                var genericTypes = GetGenericArguments(type);
                if (genericTypes.Length > 0)
                {
                    return genericTypes[0];
                }
            }
            return type;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="genericMainType">The main type, e.g List[]</param>
        /// <param name="genericParameters">The parameter type, e.g int.</param>
        /// <returns></returns>
        public static Type GetGenericType(Type genericMainType, IEnumerable<Type> genericParameters)
        {
            List<Type> listParmas = new List<Type>();
            listParmas.AddRange(genericParameters);
            Type result = genericMainType.MakeGenericType(listParmas.ToArray());

            return result;
        }






        

        public static void CopyPropertiesAndFields<T>(T from, T to)
        {
            Type fromType = typeof(T);

            //toType = to.GetType();
            {

                var items = ReflectionHelpers.GetAllPropertiesAndFieldsForType(fromType, loadNonPublicTypes: true, loadStaticProperties: false, loadAlsoFromInterfaces: true);

                foreach (var item in items)
                {
                    try
                    {

                        if (item is PropertyInfo)
                        {
                            PropertyInfo fromProp = (PropertyInfo)item;
                            if (fromProp.CanWrite)
                            {
                                object fromVal = fromProp.GetValue(from, null);
                                fromProp.SetValue(to, fromVal, null);
                            }
                        }
                        else if (item is FieldInfo)
                        {
                            FieldInfo field = (FieldInfo)item;
                            object fromVal = field.GetValue(from);
                            field.SetValue(to, fromVal);
                        }

                    }
                    catch
                    {

                    }


                }
            }


        }





        public static string GetNameFromType(Type t)
        {

            if (t.IsGenericType &&
        t.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                t = t.GetGenericArguments()[0];
            }

            return t.Name;
        }
        public static string GetFullNameFromType(Type t, bool addAssembly = false)
        {
            string s = t.AssemblyQualifiedName;
            return s;
        }

        public static PropertyInfo GetProperty<T>(Expression<Func<T, object>> propertySelector)
        {
            return GetPropertyBySelector(propertySelector);
        }
        public static PropertyInfo GetProperty<T>(T obj, Expression<Func<T, object>> propertySelector)
        {
            return GetProperty<T>(propertySelector);
        }

        public static PropertyInfo GetPropertyBySelector(Expression selector)
        {
            return (PropertyInfo)GetMemberInfoBySelector(selector);


        }

        /// <summary>
        /// This will return the last part of the expression 
        /// i.e Given Class.SubClass.SubSubClass.Title, this will return 'Title'.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static string GetPropertyNameBySelector<T>(T obj, System.Linq.Expressions.Expression<Func<T, object>> selector)
        {
            return GetPropertyNameBySelector(selector);

        }

        public static string GetPropertyNameBySelector(Expression selector)
        {
            var pInfo = GetPropertyBySelector(selector);
            return pInfo.Name;

        }

        public static List<System.Linq.Expressions.Expression<Func<T, object>>> GetListOfFieldSelectorsFromObject<T>(params System.Linq.Expressions.Expression<Func<T, object>>[] selectors)
        {
            return selectors.ToList();



        }
        public static MethodInfo GetMethodBySelector(Expression selector)
        {
            return (MethodInfo)GetMemberInfoBySelector(selector);

        }
        public static MemberInfo GetMemberInfoBySelector(Expression selector)
        {
            Expression body = selector;
            if (body is LambdaExpression)
            {
                body = ((LambdaExpression)body).Body;
            }
            switch (body.NodeType)
            {
                case ExpressionType.Call:
                    return (((System.Linq.Expressions.MethodCallExpression)body).Method);
                case ExpressionType.MemberAccess:
                    return ((PropertyInfo)((MemberExpression)body).Member);
                case ExpressionType.Convert:
                    {
                        UnaryExpression unaryExp = (UnaryExpression)body;
                        var op = unaryExp.Operand;
                        System.Linq.Expressions.MemberExpression memberExp = (MemberExpression)op;
                        return memberExp.Member;

                    }
                default:
                    throw new InvalidOperationException("Cannot retrieve member info from selector!");
            }


        }
        public static object GetPropertyOrFieldValue(MemberInfo property, object o)
        {
            if (property is PropertyInfo)
            {
                return ((PropertyInfo)property).GetValue(o, null);
            }
            else if (property is FieldInfo)
            {
                return ((FieldInfo)property).GetValue(o);

            }
            else
            {
                return null;
            }
        }

        private static object getValueForObjectByStringSelector(object o, List<string> selectorParts, int selectorIndex)
        {
            if (selectorIndex < selectorParts.Count)
            {
                string currSelector = selectorParts[selectorIndex];
                var properties = GetAllPropertiesAndFieldsForType(o.GetType(), false, false, true);
                foreach (var memberInfo in properties)
                {
                    if (string.Compare(memberInfo.Name, currSelector) == 0)
                    {
                        //Property found
                        object propertyValue = GetValueForMemberInfo(o, memberInfo);
                        if (selectorIndex == selectorParts.Count - 1)
                        {
                            //Found last value
                            return propertyValue;
                        }
                        else
                        {
                            //Check inside
                            return getValueForObjectByStringSelector(propertyValue, selectorParts, selectorIndex + 1);
                        }
                    }
                }
            }

            return null;

        }
        /// <summary>
        /// Get the value of an object by selector, e.g. Member.FirstName
        /// </summary>
        /// <param name="o"></param>
        /// <param name="selector"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static object GetValueForObjectByStringSelector(object o, string selector, string delimiter = ".")
        {
            List<string> selectorParts = TextHelpers.Split(selector, delimiter);
            return getValueForObjectByStringSelector(o, selectorParts, 0);
        }

        public static object GetValueForObjectBySelector(object o, Expression selector)
        {
            object result = null;
            MemberInfo memberInfo = GetMemberInfoBySelector(selector);
            if (memberInfo is PropertyInfo)
            {
                PropertyInfo pInfo = (PropertyInfo)memberInfo;
                result = pInfo.GetValue(o, null);
            }
            else if (memberInfo is MethodInfo)
            {
                MethodInfo mInfo = (MethodInfo)memberInfo;
                result = mInfo.Invoke(o, null);

            }
            else
            {
                throw new InvalidOperationException("Cannot retrieve member info from selector!");
            }
            return result;

        }
        /// <summary>
        /// Returns all types with the given attribute
        /// </summary>
        /// <param name="attribType"></param>
        /// <param name="a">Assembly to load types from.  If left null, entry assembly is selected</param>
        /// <returns></returns>
        public static List<Type> GetAllTypesWithAttribute(Type attribType, Assembly a = null, bool checkInherited = false)
        {


            if (a == null)
            {
                a = Assembly.GetEntryAssembly();
            }
            List<Type> list = new List<Type>();
            list.AddRange(a.GetTypes());
            for (int i = 0; i < list.Count; i++)
            {
                var c = list[i];
                if (!CheckIfTypeContainsAttribute(c, attribType, checkInherited))
                {
                    list.RemoveAt(i);
                    i--;
                }
            }
            return list;
        }

        public static bool CheckIfTypeContainsAttribute(Type classType, Type attribType, bool checkInherited = false)
        {
            object[] attribs = Attribute.GetCustomAttributes(classType, attribType, checkInherited);
            return attribs.Length > 0;
        }
        public static bool CheckIfPropertyContainsAttribute<TAttribType>(PropertyInfo propertyInfo, bool checkInherited = false)
        {
            Type attribType = typeof(TAttribType);
            return CheckIfPropertyContainsAttribute(propertyInfo, attribType, checkInherited);
        }

        public static bool CheckIfPropertyContainsAttribute(PropertyInfo propertyInfo, Type attribType, bool checkInherited = false)
        {

            object[] attribs = Attribute.GetCustomAttributes(propertyInfo, attribType, checkInherited);
            return attribs.Length > 0;
        }

     
        
        public static Type GetTypeFromAssembly(Assembly a, string className)
        {
            {
                string pattern = @"(.*?)(\<.*?\>)";
                var match = Regex.Match(className, pattern, RegexOptions.Singleline);
                if (match.Success)
                {
                    string s = match.Groups[2].Value;
                    int cnt = 1;
                    for (int i = 0; i < s.Length; i++)
                    {
                        if (s[i] == ',')
                            cnt++;
                    }

                    className = match.Groups[1].Value + "`" + cnt;
                }
            }
            return a.GetType(className);
        }

        /// <summary>
        /// Returns the C# code for the data type, e.g System.Object.  Works also for generic Lists, e.g IList<Karl>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetCSharpCodeForDataType(Type type)
        {
            string s = type.FullName;
            if (type.IsGenericType)
            {
                string typeName = type.FullName.Substring(0, type.FullName.IndexOf("`"));
                typeName += "<" + type.GetGenericArguments()[0].FullName + ">";
                s = typeName;
            }
            return s;
        }
        public static MethodInfo GetMethodFromType(Type type, string methodName)
        {
            return type.GetMethod(methodName, getBindingFlags(true, true));
        }
        private static BindingFlags getBindingFlags(bool loadNonPublicTypes, bool loadStaticProperties)
        {
            BindingFlags flags;
            flags = BindingFlags.Instance | BindingFlags.Public;
            if (loadStaticProperties) flags = flags | BindingFlags.Static;
            if (loadNonPublicTypes) flags = flags | BindingFlags.NonPublic;


            return flags;
        }
        public static List<MemberInfo> GetAllPropertiesAndMethodsForType(Type t, bool loadNonPublicTypes, bool loadStaticProperties,
            bool loadAlsoFromInterfaces)
        {
            List<MemberInfo> list = new List<MemberInfo>();
            list.AddRange(GetAllPropertiesForType(t, loadNonPublicTypes, loadStaticProperties, loadAlsoFromInterfaces));
            list.AddRange(GetAllMethodsForType(t, loadNonPublicTypes, loadStaticProperties));
            return list;

        }
       

        public static TProperty GetValueForObjectFromExpression<TModel, TProperty>(
            TModel obj,
            Expression<Func<TModel, TProperty>> expression)
        {
            return expression.Compile().Invoke(obj);
        }

        public static object GetValueForMemberInfo(object o, MemberInfo memberInfo)
        {
            if (memberInfo is PropertyInfo)
            {
                PropertyInfo p = (PropertyInfo)memberInfo;
                return p.GetValue(o);
            }
            else if (memberInfo is FieldInfo)
            {
                FieldInfo f = (FieldInfo)memberInfo;
                return f.GetValue(o);
            }
            return null;
        }

        public static List<FieldInfo> GetAllFieldsForType(Type t)
        {
            List<FieldInfo> fields = new List<FieldInfo>();

            //propsAndFields.AddRange(t.GetProperties(getBindingFlags(loadNonPublicTypes: loadNonPublicTypes, loadStaticProperties: loadStaticProperties)));
            fields.AddRange(t.GetFields());

            return fields;

        }

        public static MethodInfo[] GetAllMethodsForType(Type t, bool loadNonPublicTypes, bool loadStaticProperties)
        {
            return t.GetMethods(getBindingFlags(
                                                                loadNonPublicTypes: loadNonPublicTypes,
                                                                loadStaticProperties: loadStaticProperties));

        }
        public static Type GetMemberInfoType(MemberInfo member)
        {
            Type type = null;
            switch (member.MemberType)
            {
                case MemberTypes.Event:
                    type = ((EventInfo)member).EventHandlerType;
                    break;
                case MemberTypes.Field:
                    type = ((FieldInfo)member).FieldType;
                    break;
                case MemberTypes.Method:
                    type = ((MethodInfo)member).ReturnType;
                    break;
                case MemberTypes.Property:
                    type = ((PropertyInfo)member).PropertyType;
                    break;
                default:
                    throw new ArgumentException
                        (
                        "Input MemberInfo must be if type EventInfo, FieldInfo, MethodInfo, or PropertyInfo"
                        );
            }
            return (type);
        }
    
        public static Type GetPropertyUnderlyingType(PropertyInfo property)
        {
            return ReflectionHelpers.GetUnderlyingType(property.PropertyType);
        }
        public static PropertyInfo GetPropertyForType(Type t, string propertyName, bool loadNonPublicTypes = false, bool loadStaticProperties = false)
        {
            return t.GetProperty(propertyName, getBindingFlags(
                                                                loadNonPublicTypes: loadNonPublicTypes,
                                                                loadStaticProperties: loadStaticProperties));

        }

        

        public static IEnumerable<Assembly> GetExecutingAssemblies(bool excludeDotNetAssemblies = true)
        {
            List<Assembly> list = new List<Assembly>();
            list.AddRange(AppDomain.CurrentDomain.GetAssemblies());
            if (excludeDotNetAssemblies)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    var a = list[i];
                    if (CheckIfAssemblyIsADotNetFrameworkOne(a))
                    {
                        list.RemoveAt(i);
                        i--;
                    }
                }
            }
            return list;
        }


        /// <summary>
        /// Returns whether a type is a subclass of a generic type.  You cannot use the standard 'IsSubclassOf' to do this check as it will not work
        /// For example, checks whether a FrontendFactory is derived from baseFrontendFactory<,,,,>
        /// </summary>
        /// <param name="typeToCheck"></param>
        /// <param name="genericClass"></param>
        /// <returns></returns>
        public static bool CheckIfTypeIsSubclassOfGenericType(Type typeToCheck, Type genericClass)
        {
            while (typeToCheck != null && typeToCheck != typeof(object))
            {
                var cur = typeToCheck.IsGenericType ? typeToCheck.GetGenericTypeDefinition() : typeToCheck;
                if (genericClass == cur)
                {
                    return true;
                }
                typeToCheck = typeToCheck.BaseType;
            }
            return false;
        }

        public static bool CheckIfAssemblyIsADotNetFrameworkOne(Assembly a)
        {
            bool ok = false;
            List<string> namespaceHints = new List<string>();
            namespaceHints.Add("System");
            namespaceHints.Add("mscorlib");
            namespaceHints.Add("nunit");
            namespaceHints.Add("mysql");
            namespaceHints.Add("nhibernate");
            namespaceHints.Add("dynamicproxygenAssemblyV2");
            namespaceHints.Add("FluentNHibernate");
            namespaceHints.Add("iesi");
            namespaceHints.Add("castle");
            namespaceHints.Add("Microsoft");
            namespaceHints.Add("AxInterop");
            namespaceHints.Add("log4net");
            namespaceHints.Add("TestDriven");
            namespaceHints.Add("2ahmrrbh");
            for (int i = 0; i < namespaceHints.Count; i++)
            {
                if (a.FullName.ToLower().StartsWith(namespaceHints[i].ToLower() + ".") ||
                    (a.FullName.ToLower().StartsWith(namespaceHints[i].ToLower() + ",")))
                {
                    ok = true;
                    break;
                }
            }
            return ok;

        }
        
    
        public static List<T> GetAllAttributesMappedToPropertiesAndMethodsInType<T>(Type t) where T : Attribute
        {
            List<T> result = new List<T>();
            {
                var bindingFlags = getBindingFlags(true, false);

                PropertyInfo[] propertyList = t.GetProperties(bindingFlags | BindingFlags.FlattenHierarchy);

                foreach (var currProperty in propertyList)
                {


                    object[] foundAttribs = Attribute.GetCustomAttributes(currProperty, typeof(T), true);
                    result.AddRange(foundAttribs.Cast<T>());
                }
            }
            {
                var bindingFlags = getBindingFlags(true, false);

                MethodInfo[] methodList = t.GetMethods(bindingFlags | BindingFlags.FlattenHierarchy);

                foreach (var method in methodList)
                {


                    object[] foundAttribs = Attribute.GetCustomAttributes(method, typeof(T), true);
                    result.AddRange(foundAttribs.Cast<T>());
                }
            }

            return result;

        }

       
        public static TAttribute GetAttributeForMember<TAttribute>(MemberInfo p) where TAttribute : Attribute
        {
            return (TAttribute)GetAttributeForMember(typeof(TAttribute), p);

        }
        public static Attribute GetAttributeForMember(Type typeOfAttribute, MemberInfo member)
        {
            object[] list = Attribute.GetCustomAttributes(member, typeOfAttribute, true);
            if (list.Length > 0)
                return (Attribute)list[0];
            else
                return default(Attribute);
        }


        private static PropertyInfo getPropertyInfo(Type t, string propertyName, Type propertyReturnType = null)
        {
            PropertyInfo pInfo = null;
            if (propertyReturnType != null)
                pInfo = t.GetProperty(propertyName, propertyReturnType);
            else
                pInfo = t.GetProperty(propertyName);
            return pInfo;
        }
        public static PropertyInfo GetPropertyByName(object obj, string propertyName)
        {
            return getPropertyInfo(obj.GetType(), propertyName);
        }
        public static object GetPropertyValueByName(object obj, string propertyName, Type propertyReturnType = null)
        {
            Type t = obj.GetType();
            PropertyInfo pInfo = getPropertyInfo(t, propertyName, propertyReturnType);
            if (pInfo == null)
                throw new InvalidOperationException("Property with name <" + propertyName + "> does not exist on Type <" + t.ToString() + ">");
            return pInfo.GetValue(obj, null);
        }
       

        /// <summary>
        /// This is the equivalent of doing 'obj is baseType'
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="baseType"></param>
        /// <returns></returns>
        public static bool CheckIfObjectExtendsFrom(object obj, Type baseType)
        {
            return CheckIfTypeExtendsFrom(obj.GetType(), baseType);
        }

        /// <summary>
        /// This is the equivalent of doing 'objType is baseType'
        /// </summary>
        /// <param name="objType"></param>
        /// <param name="baseType"></param>
        /// <returns></returns>
        public static bool CheckIfTypeExtendsFrom(Type objType, Type baseType)
        {
            return baseType.IsAssignableFrom(objType);

        }
        public static Type GetTypeWhichExtendsFromAndIsNotAbstract2(Assembly assembly, Type baseType)
        {
            var types = assembly.GetTypes();
            for (int i = 0; i < types.Length; i++)
            {
                var t = types[i];
                if (!t.IsAbstract && (t.IsAssignableFrom(baseType) || baseType.IsAssignableFrom(t)))
                {
                    return t;
                }
            }
            return null;
        }


        public static void SerializeObjectToFile(object obj, string path)
        {
            Helpers.FileHelpers.CreateDirectory(path);
            var fs = File.Open(path, FileMode.Create, FileAccess.Write);
            BinaryFormatter bf = new BinaryFormatter();

            bf.Serialize(fs, obj);

            fs.Dispose();

        }
        public static object DeserializeObjectFromFile(string path)
        {
            object o = null;
            if (File.Exists(path))
            {
                var fs = File.Open(path, FileMode.Open, FileAccess.Read);

                var bf = new BinaryFormatter();
                try
                {
                    o = bf.Deserialize(fs);
                }
                catch (Exception)
                {
                    o = null;
                }
                fs.Dispose();
            }
            return o;
        }
        public static T DeserializeObjectFromFile<T>(string path)
        {
            return (T)DeserializeObjectFromFile(path);

        }

        public static bool CheckIfTypeIsNullable(Type type)
        {
            return TypeHelpers.IsTypeNullable(type);

        }

        /// <summary>
        /// Returns the data type for a nullable type, e.g int? returns typeof(int)
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Type GetDataTypeForNullableType(Type type)
        {
            return type.GetGenericArguments()[0];
        }

        /// <summary>
        /// Returns the data type for a nullable type if it is nullable, else returns itself, e.g int? returns typeof(int).  int returns int.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Type GetDataTypeForNullableTypeIfNullable(Type type)
        {
            if (CheckIfTypeIsNullable(type))
                return GetDataTypeForNullableType(type);
            else
                return type;

        }

        public static bool CheckIfTypeAllowsNullValue(Type enumType)
        {
            bool b = !enumType.IsPrimitive || CheckIfTypeIsNullable(enumType);
            return b;

        }

        /// <summary>
        /// Checks whether a type is an enumeration.  If it is a nullable enum, it can check as well.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="checkAlsoIfNullable"></param>
        /// <returns></returns>
        public static bool CheckIfTypeIsEnum(Type type, bool checkAlsoIfNullable = true)
        {
            bool isEnum = false;
            if (type.IsEnum)
                isEnum = true;
            else
            {
                if (checkAlsoIfNullable && CheckIfTypeIsNullable(type))
                {
                    var enumType = type.GetGenericArguments()[0];
                    if (enumType.IsEnum)
                    {
                        isEnum = true;
                    }
                }
            }
            return isEnum;

        }
        /// <summary>
        /// Returns the previous executing method, i.e 2 methods before this call.
        /// </summary>
        /// <returns></returns>
        public static string GetPreviousExecutingMethodName()
        {
            StackTrace stackTrace = new StackTrace();
            string methodName = null;
            MethodBase method = null;
            if (stackTrace.FrameCount > 2)
            {
                var frame = stackTrace.GetFrame(2);
                method = frame.GetMethod();
            }
            if (method != null)
            {
                methodName = method.DeclaringType.FullName + "." + method.Name;
            }
            return methodName;

        }


        public static List<T> GetAttributesFromType<T>(Type typeToGetAttributesFrom) where T : Attribute
        {
            return GetAttributesFromType(typeToGetAttributesFrom, typeof(T)).Cast<T>().ToList();
        }

        public static List<Attribute> GetAttributesFromType(Type typeToGetAttributesFrom, Type attributeType)
        {
            Attribute[] list = Attribute.GetCustomAttributes(typeToGetAttributesFrom, attributeType, true);
            return list.ToList();

        }
        /// <summary>
        /// This is relatively slow. 1,000,000 times takes over 12sec to perform, so ideally 'cache' it in strings
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static string GetExpressionText<T>(T item, Expression<Func<T, object>> expression)
        {
            string text = GetExpressionText(expression);
            return text;
        }
        /// <summary>
        /// This is relatively slow. 1,000,000 times takes over 12sec to perform, so ideally 'cache' it in strings
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static string GetExpressionText(Expression expression)
        {
            return GetExpressionText((LambdaExpression)expression);

        }
        /// <summary>
        /// This is relatively slow. 1,000,000 times takes over 12sec to perform, so ideally 'cache' it in strings
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static string GetExpressionText<T>(Expression<Func<T, object>> expression)
        {
            return GetExpressionText((LambdaExpression)expression);
        }

        /// <summary>
        /// This returns the entire expression, and replaces dots (.) with underscores (_).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static string GetExpressionTextForLucene<T>(Expression<Func<T, object>> expression)
        {
            var expressionText = GetExpressionText((LambdaExpression)expression);
            expressionText = expressionText.Replace(".", "_");

            return expressionText;
        }

        public static string GetExpressionText(LambdaExpression p)
        {
            if (p.Body.NodeType == ExpressionType.Convert || p.Body.NodeType == ExpressionType.ConvertChecked)
            {
                p = Expression.Lambda(((UnaryExpression)p.Body).Operand,
                    p.Parameters);
            }
            string text = ExpressionHelper.GetExpressionText(p);
            return text;
        }

        
        
        public static Expression<Func<TModel, TValue>> GetExpression<TModel, TValue>(Expression<Func<TModel, TValue>> selector)
        {
            return selector;
        }
        public static Expression<Func<TModel, TValue>> GetExpression<TModel, TValue>(TModel model, Expression<Func<TModel, TValue>> selector)
        {
            return GetExpression(selector);
        }
        public static Expression<Func<TModel, object>> GetExpressionWithObjectReturnType<TModel>(TModel model, Expression<Func<TModel, object>> selector)
        {
            return selector;
        }
        

        public static void UpdatePropertyByPath(object item, string propertyPath, object updatedValue)
        {
            var tokens = TextHelpers.Split(propertyPath, ".");
            object curr = item;
            PropertyInfo pInfo = null;
            for (int i = 0; i < tokens.Count; i++)
            {
                var token = tokens[i];
                pInfo = curr.GetType().GetProperty(token);
                if (i < tokens.Count - 1)
                {
                    curr = pInfo.GetValue(curr);
                }
                else
                {
                    pInfo.SetValue(curr, updatedValue);
                    break;
                }
            }
        }

     

      

        public static bool CheckIfTypeIsPrimitive(Type pType)
        {
            return pType.IsPrimitive || pType == typeof(DateTime) || pType == typeof(DateTimeOffset) || pType == typeof(string);


        }

        public static string GetCurrentStackTrace()
        {
            StackTrace st = new StackTrace();
            return st.ToString();

        }
        public static bool CheckIfObjectIsNullOrEmpty(object o)
        {
            bool result = true;
            if (o != null)
            {
                if (o is string)
                {
                    string s = (string)o;
                    result = string.IsNullOrEmpty(s);

                }
                else
                {
                    result = false;
                }
            }
            return result;

        }
        public static bool CheckIfObjectIsNullOrWhitespace(object o)
        {
            bool result = true;
            if (o != null)
            {
                if (o is string)
                {
                    string s = (string)o;
                    result = string.IsNullOrWhiteSpace(s);

                }
                else
                {
                    result = false;
                }
            }
            return result;

        }

        public static bool IsMemberInfoAnExplicitImplementation(MemberInfo member)
        {
            bool isExplicit = false;
            if (member is PropertyInfo)
            {
                PropertyInfo propertyInfo = (PropertyInfo)member;
                var method = propertyInfo.GetMethod;
                if (member.Name.Contains(".") &&
                    !member.Name.StartsWith("get_") &&
                    propertyInfo.GetMethod.IsFinal &&
                    propertyInfo.GetMethod.IsVirtual &&
                    propertyInfo.GetMethod.IsPrivate)
                {
                    isExplicit = true;
                }
            }
            return isExplicit;
        }

        public static void CopyProperties<T>(T from, T to)
        {
            CopyProperties(typeof(T), from, to);
        }



        public static void CopyProperties(Type type, object from, object to)
        {
            Type fromType = type;
            Type toType = fromType;

            {
                System.Reflection.PropertyInfo[] fromProps;

                fromProps = GetAllPropertiesForType(fromType, loadNonPublicTypes: true, loadStaticProperties: false, loadAlsoFromInterfaces: true);

                for (int i = 0; i < fromProps.Length; i++)
                {
                    System.Reflection.PropertyInfo fromProp = fromProps[i];
                  

                    PropertyInfo propertyWrite = fromProp;

                    try
                    {
                        if (propertyWrite.CanWrite)
                        {


                            object fromVal = fromProp.GetValue(from, null);

                            propertyWrite.SetValue(to, fromVal, null);

                        }
                        
                    }
                    catch (Exception)
                    {
                    }


                }
            }
        }
        private static MethodInfo getNonPublicMethodOnObject(object obj, string methodName, params object[] methodParameters)
        {
            var flags = BindingFlags.NonPublic | BindingFlags.Instance;

            var myOverloads = obj.GetType()
                              .GetMember(methodName, MemberTypes.Method, flags)
                              .Cast<MethodInfo>();
            var dynMethod = myOverloads.Where(x => x.GetParameters().Length == methodParameters.Length).FirstOrDefault();

            return dynMethod;
        }
        /// <summary>
        /// Invoke a method which is not public such as internal or protected or private
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="methodName"></param>
        /// <param name="methodParameters"></param>
        public static object InvokeNonPublicMethodOnObject(object obj, string methodName, params object[] methodParameters)
        {
            
            var dynMethod = getNonPublicMethodOnObject(obj, methodName, methodParameters);


            return dynMethod.Invoke(obj, methodParameters);
        }
        public static object InvokeNonPublicGenericMethodOnObject(object obj, string methodName, Type[] genericMethodTypes, params object[] methodParameters)
        {
            var dynMethod = getNonPublicMethodOnObject(obj, methodName, methodParameters);
            var genMethod = dynMethod.MakeGenericMethod(genericMethodTypes);

            return genMethod.Invoke(obj, methodParameters);
        }
        public static object InvokeNonPublicGenericMethodOnObject<TGenericMethod>(object obj, string methodName, params object[] methodParameters)
        {
            return InvokeNonPublicGenericMethodOnObject(obj, methodName, new Type[] { typeof(TGenericMethod) }, methodParameters);

        }
        /// <summary>
        /// Call a method on a type which is the base
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="targetObject"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public static object InvokeMethodInBaseNotOverridden(this MethodInfo methodInfo,
            object targetObject, params object[] arguments)
        {
            var parameters = methodInfo.GetParameters();

            if (parameters.Length == 0)
            {
                if (arguments != null && arguments.Length != 0)
                    throw new Exception("Arguments cont doesn't match");
            }
            else {
                if (parameters.Length != arguments.Length)
                    throw new Exception("Arguments cont doesn't match");
            }

            Type returnType = null;
            if (methodInfo.ReturnType != typeof(void))
            {
                returnType = methodInfo.ReturnType;
            }

            var type = targetObject.GetType();
            var dynamicMethod = new DynamicMethod("", returnType,
                    new Type[] { type, typeof(Object) }, type);

            var iLGenerator = dynamicMethod.GetILGenerator();
            iLGenerator.Emit(OpCodes.Ldarg_0); // this

            for (var i = 0; i < parameters.Length; i++)
            {
                var parameter = parameters[i];

                iLGenerator.Emit(OpCodes.Ldarg_1); // load array argument

                // get element at index
                iLGenerator.Emit(OpCodes.Ldc_I4_S, i); // specify index
                iLGenerator.Emit(OpCodes.Ldelem_Ref); // get element

                var parameterType = parameter.ParameterType;
                if (parameterType.IsPrimitive)
                {
                    iLGenerator.Emit(OpCodes.Unbox_Any, parameterType);
                }
                else if (parameterType == typeof(object))
                {
                    // do nothing
                }
                else {
                    iLGenerator.Emit(OpCodes.Castclass, parameterType);
                }
            }

            iLGenerator.Emit(OpCodes.Call, methodInfo);
            iLGenerator.Emit(OpCodes.Ret);

            return dynamicMethod.Invoke(null, new object[] { targetObject, arguments });
        }

    }
}
