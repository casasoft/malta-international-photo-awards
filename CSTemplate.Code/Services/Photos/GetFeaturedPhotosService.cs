﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Constants;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Users;
using Umbraco.Core.Persistence;
using Umbraco.Web;
using constantValues = CSTemplate.Code.Constants;
namespace CSTemplate.Code.Services.Photos
{

    public interface IGetFeaturedPhotosService
    {
        _ListingFeaturedPhotosModel Get(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class GetFeaturedPhotosService : IGetFeaturedPhotosService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;

        public GetFeaturedPhotosService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IUmbracoHelperService umbracoHelperService,
            IGetUserFilterByUserIdService getUserFilterByUserIdService,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService
            )
        {
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _umbracoHelperService = umbracoHelperService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public _ListingFeaturedPhotosModel Get(UmbracoHelper umbracoHelper)
        {
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            //Poco Object table names and selected attributes
            var photosTableName = constantValues.DatabaseConstantValues.PhotosTableName;
            var photoPrimaryKeyField = constantValues.DatabaseConstantValues.Photos_Poco_Primary_Key_Value;
            var photoTableAlias = constantValues.DatabaseConstantValues.aliasForPhotosPocoTableNameAlias;
            var photoUrlField = constantValues.DatabaseConstantValues.Photos_PhotoUrl_Field_Name;
            var photoSubmissionField = constantValues.DatabaseConstantValues.Photos_Submission_Id_Field_Name;

            var submissionsTableName = constantValues.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = constantValues.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = constantValues.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = constantValues.DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = constantValues.DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = constantValues.DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = constantValues.DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = constantValues.DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionTitleField = constantValues.DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionStatusEnumIdField = constantValues.DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;

            var featuredFieldId = constantValues.DatabaseConstantValues.Photos_Featured_Field_Name;

            var orderByDescValue = constantValues.DatabaseConstantValues.ORDER_BY_DESC;
            //Where Conditional condition value
            var featuredActiveValue = Convert.ToInt32(constantValues.Enums.ActiveStatus.Active);

            //Presentation title and explanation
            var sectionTitleDictionaryValue = constantValues.Enums.DictionaryKey.HomePage_Featured_Photos_Title;
            var sectionExplanationDictionaryValue = constantValues.Enums.DictionaryKey.HomePage_Featured_Photos_Explanation;

            var returnOfListingFeaturedPhotosModel = new _ListingFeaturedPhotosModel
            {
                SectionTitle = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, sectionTitleDictionaryValue),
                SummitedPhotosButtontext = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, sectionExplanationDictionaryValue)
            };
            Random rnd = new Random();
            int amount = rnd.Next(4, 20);
            var sqlQuery = new Sql($@"
                    SELECT
                        TOP ({amount})
                            {photoTableAlias}.{photoUrlField},
                            {submissionsTableAlias}.{submissionsDate},
                            {submissionsTableAlias}.{submissionsDescription},
                            {submissionsTableAlias}.{submissionsLocation},
                            {submissionsTableAlias}.{submissionsCategoryId},
                            {submissionsTableAlias}.{submissionsUserId},
                            {submissionsTableAlias}.{submissionTitleField},
                            {submissionsTableAlias}.{submissionPrimaryKeyField}

                        FROM
                            {photosTableName}
                                INNER JOIN {submissionsTableName} ON {photoTableAlias}.{photoSubmissionField}={submissionsTableAlias}.{submissionPrimaryKeyField}
                                    WHERE {photoTableAlias}.{featuredFieldId}={featuredActiveValue} AND ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK})
                                        ORDER BY {photoTableAlias}.{photoPrimaryKeyField} {orderByDescValue}
            ");
            var featuredPhotosSql = umbracoDb.Query<PhotoPoco, SubmissionPoco>(sqlQuery).ToList();

            returnOfListingFeaturedPhotosModel.ListOfPhotos = new List<_ItemPhotoModel>();
            foreach (var featuredPhoto in featuredPhotosSql)
            {
                      	
                var awardInfo = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, featuredPhoto.Submission.CategoryId);
                var photographerInfos = _getUserFilterByUserIdService.Get(umbracoHelper, featuredPhoto.Submission.UserId);
                var photographerFullName = photographerInfos.Name + " " + photographerInfos.Surname;
                returnOfListingFeaturedPhotosModel.ListOfPhotos.Add(
                    new _ItemPhotoModel()
                    {
                        ItemPhotoDate = featuredPhoto.Submission.Date,
                        ItemPhotoImgURL = Constants.ConstantValues.Submitted_Photo_Source_Address + featuredPhoto.PhotoUrl,
                        ItemPhotoName = photographerFullName,
                        ItemPhotoAward = awardInfo.AwardItem.AwardItemName,
                        ItemPhotoLocation = featuredPhoto.Submission.Location,
                        ItemPhotoDescription = featuredPhoto.Submission.Description,
                        ItemPhotoCategory = awardInfo.AwardCategoryItemName,
                        ItemPhotoTitle = featuredPhoto.Submission.Title,
                        SubmissionId = featuredPhoto.Submission.Id.ToString(),
                    }
                );

            }


            return returnOfListingFeaturedPhotosModel;
        }


    }
}