﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Photos;
using Umbraco.Web;
using constantDatabaseObjects = CSTemplate.Code.Constants.DatabaseConstantValues;
using enums = CSTemplate.Code.Constants.Enums;
namespace CSTemplate.Code.Services.UserSubmissionFeedback
{

    public interface IGetUserSubmissionsFeedbackRequestService
    {
        List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper,int userId);
    }

    [Service]
    public class GetUserSubmissionsFeedbackRequestService : IGetUserSubmissionsFeedbackRequestService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetFirstPhotoUrlFilterBySubmissionIdService _getFirstPhotoUrlFilterBySubmissionIdService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;

        public GetUserSubmissionsFeedbackRequestService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService, 
            IGetFirstPhotoUrlFilterBySubmissionIdService getFirstPhotoUrlFilterBySubmissionIdService,
            IUmbracoHelperService umbracoHelperService,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService
            )
        {
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _umbracoHelperService = umbracoHelperService;
            _getFirstPhotoUrlFilterBySubmissionIdService = getFirstPhotoUrlFilterBySubmissionIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, int userId)
        {
            var returnOfItemPhotoModel = new List<_ItemPhotoModel>();
            if (userId <= 0) return returnOfItemPhotoModel;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var orderByDesc = constantDatabaseObjects.ORDER_BY_DESC;

            var submissionTable = constantDatabaseObjects.SubmissionsTableName;
            var submissionsTableAlias = constantDatabaseObjects.aliasForSubmissionsPocoTableNameAlias;
            var submissionUserField = constantDatabaseObjects.Submissions_User_Id_Field_Name;
            var submissionPrimaryKeyField = constantDatabaseObjects.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = constantDatabaseObjects.Submissions_Date_Field_Name;
            var submissionsDescription = constantDatabaseObjects.Submissions_Description_Field_Name;
            var submissionsLocation = constantDatabaseObjects.Submissions_Location_Field_Name;
            var submissionsCategoryId = constantDatabaseObjects.Submissions_Category_Id_Field_Name;
            var submissionFeedbackRequest = constantDatabaseObjects.Submissions_Feedback_Request_Field_Name;
            var submissionStatusEnumIdField = constantDatabaseObjects.Submissions_Status_Enum_Id_Field_Name;

            var feedbackRequest = enums.RequestFeedback.Yes;
            var feedbackRequestedSubmissions = umbracoDb.Query<SubmissionPoco>($@"
                SELECT
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId}
                    FROM {submissionTable}
                        WHERE {submissionsTableAlias}.{submissionUserField} = {userId} 
                    AND {submissionsTableAlias}.{submissionFeedbackRequest} = {feedbackRequest}
                    AND   ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                            ORDER BY {submissionsTableAlias}.{submissionPrimaryKeyField} {orderByDesc}
            ").ToList();
            var statusTitleEnumValue = enums.DictionaryKey.Submission_Status_Title_Text;
            
            foreach (var submission in feedbackRequestedSubmissions)
            {
                var feedbackStatus = (submission.FeedbackRequest == Convert.ToBoolean(enums.FeedbackStatus.FeedbackRecieved) ? enums.FeedbackStatus.FeedbackRecieved:enums.FeedbackStatus.WaitingForFeedback);
                var awardCategory = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, submission.CategoryId);
                returnOfItemPhotoModel.Add(new _ItemPhotoModel
                {
                    ItemPhotoImgURL = _getFirstPhotoUrlFilterBySubmissionIdService.Get(submission.Id),
                    ItemPhotoDate = submission.Date,
                    ItemPhotoAward = awardCategory.AwardItem.AwardItemName,
                    ItemPhotoLocation = submission.Location,
                    ItemPhotoDescription = submission.Description,
                    ItemPhotoCategory = awardCategory.AwardCategoryItemName,
                    SubmissionStatusTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, statusTitleEnumValue),
                    CurrentSubmissionStatus = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, feedbackStatus)
                });
            }

            return returnOfItemPhotoModel;
        }
    }
}
