﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Shared.RequestFeedbackForSubmissionForm;
using CSTemplate.Code.Models.SubmissionFullPage;
using CSTemplate.Code.Services.Feedbacks;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class RequestFeedbackForSubmissionController : SurfaceController
    {
        private readonly IRequestANewFeedbackForSubmissionService _requestANewFeedbackForSubmissionService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        public RequestFeedbackForSubmissionController(IRequestANewFeedbackForSubmissionService requestANewFeedbackForSubmissionService, IUmbracoControllerWrapperService umbracoControllerWrapperService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _requestANewFeedbackForSubmissionService = requestANewFeedbackForSubmissionService;
        }
        [HttpPost]
        public virtual ActionResult HandleRequestFeedbackForSubmission(_RequestFeedbackForSubmissionFormModel requestFeedbackForSubmissionFormModel)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            //its gonna redirect to checkout page in order to pay for the request
            var SubmitFeedbackSuccessPageUrl = "/order-summary";
            var memberId = Members.GetCurrentMemberId();
            var submissionId = int.Parse(requestFeedbackForSubmissionFormModel.SubmissionID);
            var orderId=_requestANewFeedbackForSubmissionService.Request(umbracoHelper,submissionId,memberId);
            SubmitFeedbackSuccessPageUrl += "?orderId=" + orderId;
            return Redirect(SubmitFeedbackSuccessPageUrl);


        }
    }
}