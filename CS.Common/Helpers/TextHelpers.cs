﻿using CS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace CS.Common.Helpers
{
    public static class TextHelpers
    {

        public static string RemoveAccent(this string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }
        /// <summary>
        /// Generates a URL friendly name to use in URL and remove any accents
        /// </summary>
        /// <param name="phrase"></param>
        /// <returns></returns>
        public static string GenerateSlugForUrl(string phrase)
        {
            string str = phrase.RemoveAccent().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        public static bool IsInCamelCase(string s)
        {
            return (s != s.ToLower() && s != s.ToUpper());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="spaceMultipleConsecutiveCapitals">Capital letters which are exactly after each other ex. IBM or MyHTCDesire</param>
        /// <returns></returns>
        public static string AddSpacesToCamelCasedText(string s, bool spaceMultipleConsecutiveCapitals = false)
        {

            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(s))
            {

                for (int i = 0; i < s.Length; i++)
                {
                    string prevChar = "";
                    string nextChar = "";
                    char currChar = s[i];//.Substring(i,1)[0];
                    if (char.IsUpper(currChar) && i > 0)
                    { //curr char is uppercase
                        if (i > 0) prevChar = s[i - 1].ToString();
                        if (i < s.Length - 1) nextChar = s.Substring(i + 1, 1);
                        bool prevCharIsLower = (prevChar.ToLower() == prevChar);
                        bool nextCharIsLower = (nextChar.ToLower() == nextChar);

                        if ((spaceMultipleConsecutiveCapitals) ||
                            (!spaceMultipleConsecutiveCapitals && (prevCharIsLower || nextCharIsLower) && i < s.Length - 1)) //if both previous char and next char are lowercase or null, add space always
                        {
                            //add space
                            sb.Append(" ");
                        }
                    }
                    sb.Append(currChar);
                }
            }
            return sb.ToString();
        }


        /// <summary>
        /// Takes an array of strings and checks if ALL strings are NOT NULL (AND CHECK).
        /// </summary>
        /// <param name="s"></param>
        /// <returns>True if all Not Null - False if one or more is null</returns>
        public static bool CheckIfStringArrayIsNotNullOrWhiteSpace(params string[] s)
        {

            bool result = true;
            foreach (string str in s)
            {
                if (string.IsNullOrWhiteSpace(str))
                {
                    result = false;
                    break;
                }
            }
            return result;
        }



        /// <summary>
        /// Ex THIS IS MY TITLE -> This Is My Title.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string CapitaliseFirstLetterOfEachWord(string s)
        {
            return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
        }

        public static string ConvertCurrencyUnicodeSymbolToHTML(string s)
        {

            s = s.Replace("£", "&pound;");
            s = s.Replace("€", "&euro;");
            s = s.Replace("¥", "&yen;");
            return s;

        }


        /// <summary>
        /// Returns a list of integers from a string which is delimited by a separator
        /// </summary>
        /// <param name="str"></param>
        /// <param name="delimiter"></param>
        /// <param name="allOK"></param>
        /// <returns></returns>
        public static List<TEnum> GetListOfEnumsFromDelimitedString<TEnum>(string str, out bool allOK, string delimiter = ",")
            where TEnum : struct
        {
            List<int> intList = GetListIntegersFromDelimitedString(str, out allOK, delimiter);
            List<TEnum> list = null;
            if (intList != null)
            {
                list = new List<TEnum>();
                foreach (var i in intList)
                {
                    list.Add((TEnum)(object)i);
                }
            }
            return list;
        }

        /// <summary>
        /// Returns a list of integers from a string which is delimited by a separator
        /// </summary>
        /// <param name="str"></param>
        /// <param name="delimiter"></param>
        /// <param name="allOK"></param>
        /// <returns></returns>
        public static List<int> GetListIntegersFromDelimitedString(string str, out bool allOK, string delimiter = ",")
        {
            List<long> longList = GetListLongFromDelimitedString(str, out allOK, delimiter);
            List<int> intList = null;
            if (longList != null)
            {
                intList = longList.ConvertAll<int>(item => (int)item);
            }
            return intList;


        }
        /// <summary>
        /// Returns a list of integers from a string which is delimited by a separator
        /// </summary>
        /// <param name="str"></param>
        /// <param name="delimiter"></param>
        /// <param name="allOK"></param>
        /// <returns></returns>
        public static List<long> GetListLongFromDelimitedString(string str, out bool allOK, string delimiter = ",")
        {
            string[] vals = str.Split(delimiter.ToCharArray());
            List<long> values = new List<long>();
            allOK = true;
            for (int i = 0; i < vals.Length; i++)
            {
                string val = vals[i];
                val = val.Trim();
                long iVal = 0;
                allOK = allOK && long.TryParse(val, out iVal);
                values.Add(iVal);
            }
            return values;



        }
        /// <summary>
        /// Converts the text to a css class. Ex: "Browse New Artists" will be converted to browse-new-artists
        /// </summary>
        /// <param name="s">Text to convert</param>
        /// <returns>The new formatted css class</returns>
        public static string ConvertTextToCssClass(string s)
        {
            if (s != null)
            {
                s = s.Replace(' ', '-').ToLower();
            }
            return s;
        }


        /// <summary>
        /// Converts the text to a full link.  If it does not start with http:// or https://, and is not relative, http is added
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ConvertTextToLink(string s)
        {
            if (!string.IsNullOrWhiteSpace(s))
            {
                string sLower = s.ToLower();
                if (!sLower.StartsWith("/") && !sLower.StartsWith("http://") && !sLower.StartsWith("https://"))
                    s = "http://" + s;
            }
            return s;

        }

        /// <summary>
        /// Very basic.  Converts words ending in y to 'ies' and those ending wihtout an 's' with an s.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ConvertEnglishWordToPlural(string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                if (s.EndsWith("y"))
                {
                    s = s.Substring(0, s.Length - 1);
                    s += "ies";
                }
                else if (!s.EndsWith("s"))
                {
                    s += "s";
                }
            }
            return s;
        }
        /// <summary>
        /// Very basic.  Converts words ending in y to 'ies' and those ending wihtout an 's' with an s.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ConvertEnglishWordToSingular(string s)
        {
            if (s.EndsWith("ies"))
            {
                s = RemoveLastNumberOfCharactersFromString(s, 3);
                s += "y";
            }
            else if (s.EndsWith("s"))
            {
                s = RemoveLastNumberOfCharactersFromString(s, 1);
            }
            return s;
        }

        /// <summary>
        /// Returns the part of the text, from start to end. End is inclusive
        /// </summary>
        /// <param name="str">String</param>
        /// <param name="startIndex">start index</param>
        /// <param name="endIndex">end index</param>
        /// <returns></returns>
        public static string SubStr(this string str, int startIndex, int endIndex)
        {
            return SubStr(str, startIndex, endIndex, true);
        }

        /// <summary>
        /// Returns the part of the text, from start to end
        /// </summary>
        /// <param name="str">String</param>
        /// <param name="startIndex">start index</param>
        /// <param name="endIndex">end index</param>
        /// <param name="inclusiveOfEnd">Whether the end index is included or not</param>
        /// <returns></returns>
        public static string SubStr(this string str, int startIndex, int endIndex, bool inclusiveOfEnd)
        {
            int length = endIndex - startIndex;
            if (inclusiveOfEnd)
                length++;
            return str.Substring(startIndex, length);
        }

        public static string PATH_STOP_LIST_TOKENS = "/_common_CS_v6/static/data/stop_list.csv";
        /// <summary>
        /// Removes any non 0 - 9 characters, except the ones listed
        /// </summary>
        /// <param name="s"></param>
        /// <param name="except"></param>
        /// <returns></returns>
        public static string ConvertPunctuationsToSpaces(string s)
        {
            return ReplaceSpecialCharacters(s, " ");

        }
        /// <summary>
        /// Removes any non 0 - 9 characters, except the ones listed
        /// </summary>
        /// <param name="s"></param>
        /// <param name="except"></param>
        /// <returns></returns>
        public static string RemoveNonNumeralCharacters(string s, params char[] except)
        {
            string result = null;
            if (s != null)
            {
                result = "";
                for (int i = 0; i < s.Length; i++)
                {
                    char c = s[i];
                    bool ok = false;
                    if (c >= '0' && c <= '9')
                    {
                        ok = true;
                    }
                    else
                    {
                        if (except != null)
                        {
                            for (int j = 0; j < except.Length; j++)
                            {
                                if (except[j] == c)
                                {
                                    ok = true;
                                    break;
                                }

                            }
                        }
                    }
                    if (ok)
                    {
                        result += s[i].ToString();
                    }
                }
            }
            return result;
        }
        public static string RemoveSpecialCharacters(string str, string excludeSpecialCharacters = " ")
        {
            return ReplaceSpecialCharacters(str, null, excludeSpecialCharacters);
        }

        public static string ReplaceSpecialCharacters(string str, string replaceWith, string excludeSpecialCharacters = " ")
        {
            StringBuilder sb = new StringBuilder();
            char[] aExcludeCharacters = excludeSpecialCharacters.ToCharArray();

            foreach (char c in str)
            {

                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (aExcludeCharacters.Contains(c)))
                {
                    sb.Append(c);
                }
                else if (!string.IsNullOrEmpty(replaceWith))
                {
                    sb.Append(replaceWith);
                }
            }
            return sb.ToString();
        }
        public static string RemoveLastNumberOfCharactersFromString(string s, int charsToRemove)
        {
            if (!string.IsNullOrWhiteSpace(s))
            {
                int totalToRemove = charsToRemove;
                if (totalToRemove > s.Length)
                    totalToRemove = s.Length;
                if (totalToRemove > 0)
                {

                    s = s.Substring(0, s.Length - totalToRemove);
                }
            }
            return s;
        }
        public static string RemoveLastPartFromStringIfItIs(string s, params string[] toRemoveIfExists)
        {
            string tmp = s;
            if (tmp != null)
            {
                for (int i = 0; i < toRemoveIfExists.Length; i++)
                {
                    string remove = toRemoveIfExists[i];
                    if (remove != null)
                    {
                        if (tmp.ToLower().EndsWith(remove.ToLower()))
                        {
                            tmp = tmp.Substring(0, tmp.Length - remove.Length);
                        }
                    }
                }
            }
            return tmp;
        }
        public static string RemoveDuplicateWords(string s)
        {
            string[] arrTokens = s.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            List<string> tokens = new List<string>();
            tokens.AddRange(arrTokens);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tokens.Count; i++)
            {
                var t = tokens[i];
                t = t.Trim();
                if (t.Length > 1)
                {
                    for (int j = i + 1; j < tokens.Count; j++)
                    {
                        if (string.Compare(tokens[j], t, true) == 0)
                        {
                            tokens.RemoveAt(j); j--;
                        }
                    }
                }
                if (i > 0)
                    sb.Append(" ");
                sb.Append(t);
            }
            return sb.ToString();

        }
        public static string RemoveWordsFromString(string txt, params string[] wordsToRemove)
        {
            if (!string.IsNullOrEmpty(txt))
            {
                List<string> tokens = new List<string>();
                foreach (var w in wordsToRemove)
                {
                    if (!string.IsNullOrEmpty(w))
                    {
                        string[] wordTokens = w.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        tokens.AddRange(wordTokens);
                    }
                }
                string pattern = "";
                for (int i = 0; i < tokens.Count; i++)
                {
                    var t = tokens[i].Trim();
                    if (t.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(t) && t != "-" && t != ".")
                        {
                            if (i > 0)
                                pattern += "|";

                            pattern += ForRegExp(tokens[i]);
                        }
                    }
                }
                txt = Regex.Replace(txt, "(" + pattern + ")", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);
                txt = Regex.Replace(txt, "  ", " ");
                txt = Regex.Replace(txt, "  ", " ");
                txt = Regex.Replace(txt, "  ", " ");
                txt = Regex.Replace(txt, "  ", " ");
                txt = Regex.Replace(txt, "  ", " ");
            }
            return txt;


        }
        /// <summary>
        /// URL encodes a string
        /// </summary>
        /// <param name="s"></param>
        /// <param name="leaveBackSlashesIntact">Whether to leave back slashes intact</param>
        /// <param name="convertToSearchEngineFriendly">Convert to a search engine friendly url</param>
        /// <returns></returns>
        //public static string UrlEncode(string s, bool leaveBackSlashesIntact = true, bool convertToSearchEngineFriendly = false)
        //{
        //    if (!string.IsNullOrWhiteSpace(s))
        //    {
        //        if (leaveBackSlashesIntact)
        //        {
        //            s = s.Replace("/", "BACKWARDSSLASHHERENEEDSTOBECHANGEDBACK");
        //        }
        //        if (convertToSearchEngineFriendly)
        //        {
        //            s = ReplaceTexts(s, " and ", "&");
        //            s = ReplaceTexts(s, " ", "     ", "    ", "   ", "  ");
        //            s = ReplaceTexts(s, "-", " ", "_", ">", "<", ":", ";", "[", "]", "(", ")", "/", "\\", "~");
        //            s = ReplaceTexts(s, "", "$", "#", "%", "", "", "<", ":", ";", "[", "]");

        //        }
        //        s = System.Web.HttpUtility.UrlEncode(s);
        //        if (leaveBackSlashesIntact)
        //        {
        //            s = s.Replace("BACKWARDSSLASHHERENEEDSTOBECHANGEDBACK", "/");
        //        }
        //        if (convertToSearchEngineFriendly)
        //        {
        //            s = ReplaceTexts(s, "/", "/-", "-/");
        //            if (s.StartsWith("-")) s = s.Substring(1);
        //            if (s.EndsWith("-")) s = s.Substring(0, s.Length - 1);
        //        }
        //    }
        //    return s;
        //}


        public static string UrlDecode(string s)
        {
            return System.Web.HttpUtility.UrlDecode(s);
        }
        public static string HtmlEncode(string s, bool convertUnicodeToHtmlEquivalent = true)
        {



            string result = System.Web.HttpUtility.HtmlEncode(s);
            if (convertUnicodeToHtmlEquivalent && !string.IsNullOrEmpty(result))
            {
                result = result.Replace("£", "&pound;");
                result = result.Replace("€", "&euro;");
                result = result.Replace("¥", "&yen;");

            }
            return result;
        }
        public static string HtmlAttributeEncode(string s)
        {
            string result = System.Web.HttpUtility.HtmlAttributeEncode(s);
            return result;
        }

        public static string HtmlDecode(string s)
        {
            return System.Web.HttpUtility.HtmlDecode(s);
        }



        /// <summary>
        /// This will convert a string delimited by something into a list of integers. 
        /// </summary>
        /// <example>
        /// 5,2,3,4,5 to 5 , 2 , 3 , 4, 5 in a list of casted integers
        /// </example>
        /// <param name="s"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static List<int> ConvertStringToInts(string s, char delimiter)
        {

            List<int> ints = new List<int>();
            if (!String.IsNullOrEmpty(s))
            {
                string[] values = s.Split(delimiter);
                for (int i = 0; i < values.Length; i++)
                {
                    int num = 0;
                    if (Int32.TryParse(values[i], out num))
                    {
                        ints.Add(num);
                    }
                }
            }
            return ints;
        }
        /// <summary>
        /// Convert a string delimited by commas to a list of integers
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static List<int> ConvertStringToInts(string s)
        {
            return ConvertStringToInts(s, ',');
        }
        public static string JoinList<T>(IEnumerable<T> list, Func<T, string> toStringHandler, string delimiter, bool addIfNullOrEmpty = false, string lastDelimiter = null)
        {
            return ListHelpers.JoinList(list, toStringHandler, delimiter, addIfNullOrEmpty, lastDelimiter);

        }

        public static string JoinList<T>(IEnumerable<T> list, string delimiter, bool addIfNullOrEmpty = false, string lastDelimiter = null, bool removeDelimiterIfItemEndsWithDelimiter = true)
        {
            return ListHelpers.JoinList(list, (item => (item == null ? null : item.ToString())), delimiter, addIfNullOrEmpty, lastDelimiter, removeDelimiterIfItemEndsWithDelimiter);
        }
        public static string JoinList(string delimiter, bool addIfNullOrEmpty = false, params object[] addressParts)
        {
            List<object> objs = new List<object>();
            for (int i = 0; i < addressParts.Length; i++)
            {
                objs.Add(addressParts[i]);
            }

            return JoinList(objs, delimiter, addIfNullOrEmpty);
        }

        /// <summary>
        /// Returns a string of all the strings appended together, that contain a value.
        /// For example, if you pass [karl], [], [mark], and the delimter is ', '. The result
        /// would be 'karl, mark'.  
        /// </summary>
        /// <param name="delimeter">Delimeters</param>
        /// <param name="strs">Strings</param>
        /// <returns></returns>
        public static string AppendStrings(string delimeter, IEnumerable<string> strs)
        {
            StringBuilder sb = new StringBuilder();
            if (strs != null)
            {
                foreach (var str in strs)
                {
                    string s = str;
                    if (s != null) s = s.Trim();
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(delimeter);
                        }
                        sb.Append(s);
                    }
                }
            }
            return sb.ToString();


        }
        /// <summary>
        /// Returns a string of all the strings appended together, that contain a value.
        /// For example, if you pass [karl], [], [mark], and the delimter is ', '. The result
        /// would be 'karl, mark'.  
        /// </summary>
        /// <param name="delimeter">Delimeters</param>
        /// <param name="strs">Strings</param>
        /// <returns></returns>
        public static string AppendStrings(string delimeter, params string[] strs)
        {
            return AppendStrings(delimeter, (IEnumerable<string>)strs);
        }

        /// <summary>
        /// Escapes all specified texsts with the specified escape character
        /// </summary>
        /// <param name="txt">Text</param>
        /// <param name="escapeWith">Escape With</param>
        /// <param name="texts">Texts / Characters to escape</param>
        /// <returns></returns>
        public static string EscapeCharactersInText(string txt, string escapeWith, params string[] texts)
        {
            for (int i = 0; i < texts.Length; i++)
            {
                string s = texts[i];
                txt = txt.Replace(s, escapeWith + s);
            }
            return txt;


        }
        public static string SplitUpperCaseWordsBySpaces(string title)
        {
            string s = "";
            int lastCapital = -999;
            for (int i = 0; i < title.Length; i++)
            {
                string c = title.Substring(i, 1);
                s += c;
                bool isUpper;
                isUpper = (c == c.ToUpper());
                if (isUpper)
                    lastCapital = i;
                if (!isUpper && (lastCapital == i - 1) && i >= 2)
                {
                    s = s.Insert(s.Length - 2, " ");
                }

            }
            return s;
        }
        public static string ForRegExp(object txt)
        {
            string tmp = (string)txt;
            if (txt != null && tmp != "")
            {
                tmp = tmp.Replace(@"\", @"\\");//replace with four slashes
                tmp = tmp.Replace("’", @"\’");
                tmp = tmp.Replace("‘", @"\‘");
                tmp = tmp.Replace("'", @"\'"); //replace with one slashes before
                tmp = tmp.Replace(".", @"\."); //replace with two slashes before (
                tmp = tmp.Replace("+", @"\+");
                tmp = tmp.Replace("*", @"\*");
                tmp = tmp.Replace("(", @"\(");
                tmp = tmp.Replace(")", @"\)");
                tmp = tmp.Replace("^", @"\^");
                tmp = tmp.Replace("$", @"\$");
                tmp = tmp.Replace("?", @"\?");
                tmp = tmp.Replace("|", @"\|");
                tmp = tmp.Replace("{", @"\{");
                tmp = tmp.Replace("}", @"\}");
                tmp = tmp.Replace("-", @"\-");
                tmp = tmp.Replace("[", @"\[");
                tmp = tmp.Replace("]", @"\]");
            }
            else
            {
                tmp = "";
            }
            return tmp;

        }

        /// <summary>
        /// Returns the initials from a group of words, e.g Karl Cassar returns KC.
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static string GetInitialsFromWord(string words)
        {
            words = words ?? "";
            words = words.Trim();
            string s = "";
            string[] tokens = words.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < tokens.Length; i++)
            {
                tokens[i] = tokens[i].Trim();
                if (tokens[i].Length > 0)
                    s += tokens[i][0];
            }
            return s;

        }
        public static void ParseURL(string url, out string urlPart, out string queryStringPart)
        {
            queryStringPart = "";
            urlPart = url;
            int questionPos = url.LastIndexOf('?');
            if (questionPos > -1)
            {
                urlPart = url.Substring(0, questionPos);
                queryStringPart = url.Substring(questionPos + 1);
            }

        }
        public static string SetFirstLetterLowercase(string s)
        {
            return s == null ? null : Char.ToLowerInvariant(s[0]) + s.Substring(1);
        }
        public static string ToCamelCase(string s)
        {
            return ToCamelCase(s, false);
        }
        /// <summary>
        /// , camel case always starts with a lower case letter (e.g. backColor)
        /// </summary>
        /// <param name="s"></param>
        /// <param name="removeNonAlphabet"></param>
        /// <returns></returns>
        public static string ToCamelCase(string s, bool removeNonAlphabet)
        {
            bool convertNext = true;
            s = s.ToLower();
            string camelCase = "";

            bool isFirstLetter = true;
            for (int i = 0; i < s.Length; i++)
            {
                if (convertNext)
                {
                    if (!removeNonAlphabet || (s[i] >= 'a' && s[i] <= 'z'))
                    {
                        if (isFirstLetter)
                        {
                            isFirstLetter = false;
                            camelCase += s[i].ToString();
                        }
                        else
                        {
                            camelCase += s[i].ToString().ToUpper();
                        }
                    }
                }
                else
                {
                    if (!removeNonAlphabet || (s[i] >= 'a' && s[i] <= 'z'))
                    {
                        camelCase += s[i].ToString();
                    }

                }
                if (s[i] >= 'a' && s[i] <= 'z' || s[i] == '\'')
                {
                    convertNext = false;
                }
                else
                {
                    convertNext = true;
                }
            }
            return camelCase;

        }
        public static string RemoveTagsFromContent(string s, string startTag, string endTag, bool removeContent)
        {
            if (removeContent)
            {
                s = TextHelpers.RemoveTextBetween(s, startTag, endTag, true);
            }
            else
            {
                string pattern = TextHelpers.ForRegExp(startTag) + "|" + TextHelpers.ForRegExp(endTag);
                s = Regex.Replace(s, pattern, "", RegexOptions.Singleline | RegexOptions.IgnoreCase);

            }
            return s;

        }
        public static string RemoveTextBetween(string text, string startToken, string endToken)
        {
            return RemoveTextBetween(text, startToken, endToken, true);
        }
        public static string RemoveTextBetween(string text, string token)
        {
            return RemoveTextBetween(text, token, token, true);
        }
        public static string RemoveTextBetween(string textSource, string startToken, string endToken, bool removeTokens = false, bool caseSensitive = false)
        {
            return ReplaceTextBetween(textSource, "", startToken, endToken, removeTokens, caseSensitive);
        }
        public static string ReplaceTextBetween(string textSource, string newText, string startToken, string endToken, bool replaceTokens = false, bool caseSensitive = false)
        {
            StringComparison stringCmp = StringComparison.CurrentCultureIgnoreCase;
            if (caseSensitive)
                stringCmp = StringComparison.CurrentCulture;

            int startPos = textSource.IndexOf(startToken, stringCmp);
            int endPos = -1;
            while (startPos > -1)
            {
                endPos = textSource.IndexOf(endToken, startPos + 1, stringCmp);
                if (endPos > -1)
                {
                    if (!replaceTokens)
                    {
                        startPos += startToken.Length;
                    }
                    else
                    {
                        endPos += endToken.Length;
                    }
                    textSource = textSource.Remove(startPos, endPos - startPos);
                    textSource = textSource.Insert(startPos, newText);

                }

                startPos = textSource.IndexOf(startToken, startPos + 1, stringCmp);
            }
            if (startPos > -1)
            {

            }

            return textSource;
        }



        
        /// <summary>
        /// Returns the whole word, from the index.  Word boundaries are ' ' and ','
        /// </summary>
        /// <param name="str">String to search for</param>
        /// <param name="index">Index to get word from</param>
        /// <param name="wordBoundaries">List of word boundaries</param>
        /// <returns></returns>
        public static string GetFullWordAtIndex(string str, int index)
        {
            return GetFullWordAtIndex(str, index, new char[] { ' ', ',', '.' });
        }
        /// <summary>
        /// Returns the whole word, from the index.
        /// </summary>
        /// <param name="str">String to search for</param>
        /// <param name="index">Index to get word from</param>
        /// <param name="wordBoundaries">List of word boundaries</param>
        /// <returns></returns>
        public static string GetFullWordAtIndex(string str, int index, char[] wordBoundaries)
        {
            int startBoundary = 0, endboundary = str.Length - 1;
            for (int i = index; i >= 0; i--)
            {

                char c = str[i];
                bool found = false;
                for (int j = 0; j < wordBoundaries.Length; j++)
                {
                    if (c == wordBoundaries[j])
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                    break;
                startBoundary = i;
            }
            for (int i = index; i < str.Length; i++)
            {

                char c = str[i];
                bool found = false;
                for (int j = 0; j < wordBoundaries.Length; j++)
                {
                    if (c == wordBoundaries[j])
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                    break;
                endboundary = i;
            }
            string s = str.Substring(startBoundary, endboundary - startBoundary + 1);
            return s;

        }

        public static string ReplaceLinkBreaksInAStringWithBRtags(string text)
        {
            Regex regex = new Regex(@"(\r\n|\r|\n)+");

            return regex.Replace(text, "<br />");
        }

        public static string GetStringBetweenTokens(string data, string tokenStart, string tokenEnd)
        {
            int startPos, endPos;
            startPos = data.IndexOf(tokenStart);
            endPos = data.IndexOf(tokenEnd);
            string ret = "";
            if (startPos > -1 && endPos > -1)
            {
                ret = data.Substring(startPos + tokenStart.Length, endPos - startPos - tokenStart.Length);
            }
            return ret;
        }

        public static string OutputQueryString(NameValueCollection list)
        {
            string str = "";
            for (int i = 0; i < list.Count; i++)
            {
                if (i > 0)
                    str += "&";
                str += list.AllKeys[i] + "=" + list[i];
            }
            return str;
        }
        /// <summary>
         /// Returns the first string value which is not empty
         /// </summary>
         /// <param name="vars"></param>
         /// <returns></returns>
        public static string OutputFirstNonEmptyString(params string[] vars)
        {
            return vars.FirstOrDefault(x => !string.IsNullOrWhiteSpace(x)) ?? "";
        }

        public static NameValueCollection ParseQueryString(string QS)
        {
            NameValueCollection nv = new NameValueCollection();
            if (QS.Length == 0)
                return nv;
            if (QS[0] == '?')
                QS.Remove(0, 1);
            string[] list = QS.Split('&');
            for (int i = 0; i < list.Length; i++)
            {
                string[] tmp = list[i].Split('=');
                nv.Add(tmp[0], tmp[1]);
            }
            return nv;

        }
        public static byte[] StrToByteArray(string s)
        {
            MemoryStream ms = new MemoryStream(s.Length);
            StreamWriter sw = new StreamWriter(ms);
            sw.Write(s);
            sw.Flush();
            ms.Seek(0, SeekOrigin.Begin);
            byte[] by = new byte[ms.Length];
            ms.Read(by, 0, by.Length);
            sw.Dispose();
            sw.Close();

            ms.Close();
            ms.Dispose();

            return by;
        }
        public static string ByteArrayToStr(object b)
        {
            if (b != null)
                return ByteArrayToStr((byte[])b);
            else
                return null;
        }
        public static string ByteArrayToStr(byte[] b)
        {
            MemoryStream ms = new MemoryStream(b);
            StreamReader sr = new StreamReader(ms);
            string data = sr.ReadToEnd();
            sr.Close();
            ms.Close();
            return data;
        }

        public static string UTF8Encode(string data)
        {

            string tmp = data;
            tmp = tmp.Replace("&", "&amp;");
            tmp = tmp.Replace("'", "&apos;");
            tmp = tmp.Replace("\"", "&quot;");
            tmp = tmp.Replace("<", "&gt;");
            tmp = tmp.Replace(">", "&lt;");
            return tmp;


        }
        public static string PadLines(string data, bool padFirstLine, int amount, int maxCharsPerLine)
        {
            string result = data;
            if (!string.IsNullOrWhiteSpace(data))
            {
                string[] lines = data.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                StringBuilder dataResult = new StringBuilder();
                for (int i = 0; i < lines.Length; i++)
                {
                    string s = lines[i];
                    if (maxCharsPerLine > 0)
                    {
                        while (s.Length > maxCharsPerLine)
                        {
                            string s1 = s.Substring(0, maxCharsPerLine);
                            s = s.Substring(maxCharsPerLine);
                            dataResult.AppendLine(s1);
                        }
                    }
                    dataResult.AppendLine(s);
                }
                result = PadLines(dataResult.ToString(), padFirstLine, amount);
            }
            return result;
        }
        /// <summary>
        /// Pads a string with spaces at the beginning of every line
        /// </summary>
        /// <param name="data">The data</param>
        /// <param name="padFirstLine">Whether to pad the first line as well</param>
        /// <param name="amount">The amount of spaces</param>
        /// <returns>The padded string</returns>
        public static string PadLines(string data, bool padFirstLine, int amount)
        {
            string tmp = data;
            string ret = data;
            if (!string.IsNullOrWhiteSpace(tmp))
            {
                string spacer = "";
                for (int i = 0; i < amount; i++)
                    spacer += " ";
                tmp = tmp.Replace("\r\n", "[#$#CR#$#]");
                tmp = tmp.Replace("\r", "[#$#CR#$#]");
                tmp = tmp.Replace("\n", "[#$#CR#$#]");
                string[] delim = new string[1];
                delim[0] = "[#$#CR#$#]";
                string[] lines = tmp.Split(delim, StringSplitOptions.None);
                ret = "";
                for (int i = 0; i < lines.Length; i++)
                {
                    if ((i > 0) || (padFirstLine))
                    {
                        lines[i] = spacer + lines[i];
                    }
                    ret += lines[i];
                    if (i < lines.Length - 1)
                        ret += "\r\n";
                }
            }
            return ret;



        }

        public static string Show(object txt, string NA)
        {
            if (txt.ToString() != "")
                return txt.ToString();
            else
                return NA;
        }


        /// <summary>
        /// Shows boolean as Yes or No
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public static string ShowBool(bool b)
        {
            if (b)
                return "Yes";
            else
                return "No";
        }
        public static string Display(object o)
        {
            string str = "";
            if (o is string || o is int || o is double || o is Int32 || o is Int16 ||
                o is Int64 || o is float || o is byte || o is char || o is byte)
                str = o.ToString();
            else if (o is DateTime)
                str = ((DateTime)o).ToString("dd MMM yy");
            else if (o is bool)
            {
                if ((bool)o)
                    str = "Yes";
                else
                    str = "No";
            }
            return str;


        }

        public static string ShowLines(string sep, params object[] lines)
        {
            string ret = "";
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].ToString() != "")
                {
                    if (ret != "")
                        ret += sep;
                    ret += lines[i].ToString();
                }
            }
            return ret;
        }
        

        /// <summary>
        /// Limit text to a certain amount of characters, and add an ellipse
        /// 
        /// </summary>
        /// <param name="txt">The text to limit</param>
        /// <param name="totChars">Maximum characters to show</param>
        /// <returns>The limited text</returns>
        /*public static string LimitText(string txt, int totChars, string ellipsis = "...")
        {
            string ret = "";
            if (!String.IsNullOrEmpty(txt))
            {
                if (txt.Length > totChars)
                {
                    ret = txt.Substring(0, totChars - ellipsis.Length) + ellipsis;
                }
                else
                {
                    ret = txt;
                }
            }
            return ret;
        }*/

        /// <summary>
        /// Limit text to a certain amount of characters, and add an ellipse without breaking words in half
        /// </summary>
        /// <param name="txt">The text to limit</param>
        /// <param name="totChars">Maximum characters to show</param>
        /// <param name="checkForWhiteSpace">Check for whitespace</param>
        /// <returns>The limited text</returns>
        public static string LimitText(string txt, int totChars, bool checkForWhiteSpace = false, string suffix = "...")
        {
            string ret = string.Empty;

            if (!string.IsNullOrEmpty(txt) && txt.Length > totChars)
            {
                if (!checkForWhiteSpace)
                {
                    ret = txt.Substring(0, totChars - suffix.Length) + suffix;
                }
                else
                {
                    var charArray = txt.ToCharArray();
                    int characterToCount = totChars - suffix.Length;

                    if (Char.IsWhiteSpace(charArray[characterToCount]))
                    {
                        ret = txt.SubStr(0, characterToCount) + suffix;
                    }

                    for (int i = characterToCount; i > 0; i -= 1)
                    {
                        if (charArray[i] == ' ')
                        {
                            ret = txt.SubStr(0, i) + suffix;
                            break;
                        }
                    }
                }
            }
            else
            {
                ret = txt;
            }

            return ret;
        }



        /// <summary>
        /// Replaces text with HTML formatting text to display correctly
        /// </summary>
        /// <param name="txt">Text to parse</param>
        /// <returns>Text in HTML format</returns>
        public static string TxtForHTML(string txt)
        {
            return TxtForHTML(txt, false);
        }
        /// <summary>
        /// Replaces text with HTML formatting text to display correctly
        /// </summary>
        /// <param name="txt">Text to parse</param>
        /// <returns>Text in HTML format</returns>
        public static string TxtForHTML(string txt, bool changeSpacesToNonBreaking)
        {

            /*if (txt.Contains("&"))
            {
                
            }
            string tmp = txt;
            string[] replaceFrom = new string[] {"&", "'", "\"", ">", "<" };
            string[] replaceTo = new string[] { "&amp;", "&apos;", "&quot;", "&gt;", "&lt;" };
            for (int i = 0; i < replaceTo.Length; i++)
            {
                tmp = tmp.Replace(replaceTo[i], "{[#" + replaceTo[i].Replace("&", "") + "#]}");
            }
            for (int i = 0; i < replaceFrom.Length; i++)
            {
                tmp = tmp.Replace(replaceFrom[i], replaceTo[i]);
            }
            for (int i = 0; i < replaceTo.Length; i++)
            {
                tmp = tmp.Replace("{[#" + replaceTo[i].Replace("&", "") + "#]}",replaceTo[i]);
            }
            */


            if (txt == null) txt = "";
            txt = HtmlEncode(txt);
            if (changeSpacesToNonBreaking)
            {
                txt = txt.Replace("  ", " &nbsp;");
                //txt = txt.Replace("&nbsp; ", "&nbsp;&nbsp;");
            }

            txt = txt.Replace("\r\n", "\r");
            txt = txt.Replace("\n", "\r");
            string result = "";
            for (int i = 0; i < txt.Length; i++)
            {
                int charValue = (int)txt[i];
                if (charValue == '\r')
                    result += "<br />";
                else
                    result += TextHelpers.ConvertCharToHTMLHex(txt[i]);
            }
            return result;

        }
        /// <summary>
        /// Converts a character to its HTML hex value, if it is required (HTML Hex is for example &#0192; --> À 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static string ConvertCharToHTMLHex(char c)
        {
            int charValue = (int)c;
            if (charValue > 160)
            {

                return "&#" + charValue.ToString("0000") + ";";
            }
            else
                return c.ToString(CultureInfo.InvariantCulture);

        }

        public static bool Contains(string str, string containsText, bool caseInsensitive = false)
        {
            if (caseInsensitive)
            {
                return CultureInfo.InvariantCulture.CompareInfo.IndexOf(str, containsText, CompareOptions.IgnoreCase) != -1;
            }
            else
            {
                return str.Contains(containsText);
            }
        }

        /// <summary>
        /// Makes a piece of text one line, by changing all enters to spaces
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static string MakeOneLine(string txt)
        {
            return MakeOneLine(txt, " ");
        }
        /// <summary>
        /// Makes a piece of text one line, by changing all enters to spaces
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static string MakeOneLine(string txt, string replaceTo)
        {
            string s = txt;
            s = ReplaceTexts(s, replaceTo, "\r\n", "\r", "\n");
            while (s.EndsWith(replaceTo))
            {
                s = s.Substring(0, s.Length - replaceTo.Length);
            }
            return s;
        }


        /// <summary>
        /// Removes a line(s) from a specified identifier (startTxt) to the end of the
        /// carriage return
        /// </summary>
        /// <param name="startTxt">The identifier to check for, for which to remove the lines</param>
        /// <param name="text">The text itself</param>
        /// <returns>The modified text</returns>
        public static string RemoveLinesFromText(string startTxt, string text)
        {
            string tmp = text;
            while (tmp.IndexOf(startTxt) > -1)
            {
                int pos = tmp.IndexOf(startTxt);
                int CRPos = tmp.Substring(pos).IndexOf("\r\n") + pos;
                if (CRPos == -1)
                    CRPos = text.Length;
                else
                    CRPos += 2;
                if (pos > -1)
                {
                    tmp = (tmp.Remove(pos, CRPos - pos));
                }
            }
            return tmp;



        }
        public static string RepeatText(string txt, int times)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < times; i++)
            {
                sb.Append(txt);
            }
            return sb.ToString();
        }
        /// <summary>
        /// Replaces texts - Case insensitive and text to replace is automatically escaped for regular expressions
        /// </summary>
        /// <param name="txt">Text</param>
        /// <param name="replaceWith">Replace with text</param>
        /// <param name="txtToReplace">Text to Replace. </param>
        /// <returns></returns>
        public static string ReplaceTexts(string txt, string replaceWith, params string[] txtToReplace)
        {
            return ReplaceTexts(txt, replaceWith, false, false, txtToReplace);
        }

        public static string ReplaceTag(string str, string tag, string replaceWith, string leftBracket = "[", string rightBracket = "]")
        {
            replaceWith = replaceWith ?? "";
            if (!string.IsNullOrEmpty(str) && !string.IsNullOrEmpty(replaceWith) && !string.IsNullOrEmpty(tag))
            {
                tag = tag.Trim();
                if (leftBracket != null && tag.StartsWith(leftBracket)) tag = tag.Substring(leftBracket.Length);
                if (rightBracket != null && tag.EndsWith(rightBracket)) tag = tag.Substring(0, tag.Length - rightBracket.Length);

                Regex r = new Regex("\\" + leftBracket + "\\s*" + tag + "\\s*" + "\\" + rightBracket, RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Singleline);

                str = r.Replace(str, replaceWith);
            }
            return str;
        }

        //public static string ParseTemplateTextCached(string template, object model, string cacheKey)
        //{
        //    if (!string.IsNullOrWhiteSpace(template))
        //    {
        //        string result = PageHelpers.GetCachedObject<string>(cacheKey);
        //        if (string.IsNullOrWhiteSpace(result))
        //        {
        //            result = ParseTemplateText(template, model);
        //            PageHelpers.SetCachedObject(cacheKey, result, 600);
        //        }

        //        return result;
        //    }
        //    else
        //    {
        //        return template;
        //    }
        //}


        private static long templateMsCount = 0;

        public static string GetRazorTemplateUniqueId(string templateUniqueID, string languageCode = null)
        {

            if (!string.IsNullOrWhiteSpace(languageCode))
            {
                templateUniqueID += "-" + languageCode;
            }
            return templateUniqueID;
        }


      
        /// <summary>
        /// Replaces texts
        /// </summary>
        /// <param name="txt">Text</param>
        /// <param name="replaceWith">Replace with text</param>
        /// <param name="caseSensitive">Whether search is case-sensitive or not</param>
        /// <param name="txtToReplaceIncludesRegExp">Whether the text to replace includes regular expressions or not. If not, it is automatically
        /// replaced to escape regular expressions</param>
        /// <param name="txtToReplace">Text to Replace. </param>
        /// <returns></returns>
        public static string ReplaceTexts(string txt, string replaceWith, bool caseSensitive, bool txtToReplaceIncludesRegExp,
            params string[] txtToReplace)
        {
            if (txt != null)
            {
                replaceWith = replaceWith ?? "";
                StringBuilder sbRegExPattern = new StringBuilder();
                if (txtToReplace.Length > 0)
                {
                    bool oneOk = false;
                    sbRegExPattern.Append("(");
                    for (int i = 0; i < txtToReplace.Length; i++)
                    {
                        string r = txtToReplace[i] ?? "";
                        if (!string.IsNullOrEmpty(r))
                        {


                            r = !txtToReplaceIncludesRegExp ? ForRegExp(r) : r;
                            if (i > 0)
                                sbRegExPattern.Append("|");
                            sbRegExPattern.Append(r);
                            oneOk = true;
                        }

                    }
                    sbRegExPattern.Append(")");
                    if (oneOk)
                    {
                        txt = Regex.Replace(txt, sbRegExPattern.ToString(), replaceWith, !caseSensitive ? RegexOptions.IgnoreCase : RegexOptions.None);
                    }
                }
            }
            return txt;
        }
        /// <summary>
        /// Retrieves a tag content from a piece of Helpers.Text.  Optionally, can be
        /// removed from text
        /// </summary>
        /// <param name="startTag">The start tag</param>
        /// <param name="endTag">The end tag</param>
        /// <param name="txt">The text itself.  By reference so that it can be changed</param>
        /// <param name="removeCRwithTags">Whether to remove any carriage returns and
        /// line feeds before and after  the start/end tags</param>
        /// <param name="removeFromText">Whether to remove it from text</param>
        /// <returns>The required tag contents</returns>
        public static string GetTextBetween(string startTag, string endTag,
                                            ref string txt, bool removeCRwithTags,
                                            bool removeFromText)
        {
            int start, end;
            int startTagLen = 0, endTagLen = 0; ;
            string tag = "";
            start = txt.ToLower().IndexOf(startTag.ToLower());
            end = txt.ToLower().IndexOf(endTag.ToLower(), start + 1);
            startTagLen = startTag.Length;
            endTagLen = endTag.Length;
            if (removeCRwithTags && txt.Substring(end - 2, 2) == "\r\n")
            {
                end -= 2;
                endTagLen += 2;
            }
            if (removeCRwithTags && txt.Substring(start + startTagLen, 2) == "\r\n") startTagLen += 2;
            if (removeCRwithTags && txt.Substring(end + endTagLen, 2) == "\r\n") endTagLen += 2;
            if (start > -1 && end > -1)
            {
                tag = txt.Substring(start + startTagLen, end - (start + startTagLen));
                if (removeFromText)
                {
                    txt = txt.Remove(start, end + endTag.Length - start);
                }
            }
            return tag;

        }



        public static string Capitalize(string txt, bool firstLetterOnly = false)
        {
            return Capitalize(txt, false, firstLetterOnly: firstLetterOnly);
        }

        public static string Capitalize(string txt, bool makeOthersSmall, bool firstLetterOnly = false)
        {
            if (!string.IsNullOrEmpty(txt))
            {
                StringBuilder data = new StringBuilder(txt.Length);



                bool wasSpace = true;

                for (int i = 0; i < txt.Length; i++)
                {
                    if (txt[i] == ' ')
                    {
                        wasSpace = true;
                        data.Append(txt[i]);
                    }
                    else if (wasSpace)
                    {
                        if (txt[i] >= 'a' && txt[i] <= 'z')
                        {
                            data.Append((char)(txt[i] - 'a' + 'A')); //make upper case
                            if (firstLetterOnly)
                            {
                                data.Append(txt.Substring(i + 1).ToArray());
                                break; // stop it
                            }
                        }
                        else
                        {
                            data.Append(txt[i]);
                        }
                        wasSpace = false;
                    }
                    else
                    {
                        if (makeOthersSmall && txt[i] >= 'A' && txt[i] <= 'Z')
                            data.Append((char)(txt[i] + 'a' - 'A')); //make lowercase
                        else
                            data.Append(txt[i]);
                    }
                }

                return data.ToString();
            }
            else
            {
                return txt;
            }
        }

        public static string NormalizeControlID(string id)
        {
            id = id.Replace(" ", ""); // cannot have spaces
            id = id.Replace("/", ""); // cannot have slashes
            return id;

        }

        public static string SplitTextIntoMultiLine(string txt, int maxLineSize)
        {
            string[] words = txt.Split(' ');
            StringBuilder sb = new StringBuilder();
            string lastLine = "";
            for (int i = 0; i < words.Length; i++)
            {
                if (lastLine == "")
                {
                    lastLine += words[i];
                }
                else
                {
                    if ((lastLine.Length + words[i].Length + 1) <= maxLineSize)
                    {
                        lastLine += " " + words[i];
                    }
                    else
                    {
                        sb.AppendLine(lastLine);
                        lastLine = "";
                        i--;
                    }
                }
            }
            if (!string.IsNullOrEmpty(lastLine))
                sb.AppendLine(lastLine);

            return sb.ToString();

        }
        public static string SplitTextMultilineHTML(string txt, int linesize)
        {
            string tmp = txt;
            int i = 0;
            while (i < tmp.Length)
            {
                i += linesize;

                if (i < tmp.Length)
                {
                    tmp = tmp.Insert(i, "<br />");
                    i += "<br />".Length;
                }
            }
            return tmp;

        }
        public static string GenerateString(string repeat, int times)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < times; i++)
            {
                sb.Append(repeat);
            }
            return sb.ToString();
        }

        public static List<string> Split(string txt, params string[] delims)
        {
            if (txt != null)
                return txt.Split(delims, StringSplitOptions.RemoveEmptyEntries).ToList();
            else
                return new List<string>();
        }
        public static List<int> SplitTextIntoIntegers(string txt, params string[] delims)
        {
            return SplitTextIntoLongs(txt, delims).ConvertAll(item => (int)item);
        }

        public static List<long> SplitTextIntoLongs(string txt, params string[] delims)
        {
            List<long> result = new List<long>();
            if (txt != null)
            {
                List<string> delimeters = new List<string>();
                if (delims != null)
                {
                    delimeters.AddRange(delims);
                }
                if (delimeters.Count == 0)
                {
                    delimeters.Add("|");
                    delimeters.Add(",");
                    delimeters.Add("#");
                }
                string[] sTokens = txt.Split(delimeters.ToArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < sTokens.Length; i++)
                {
                    var s = sTokens[i];
                    if (s != null)
                    {
                        s = s.Trim();
                    }
                    long lng = 0;
                    if (long.TryParse(s, out lng))
                    {
                        result.Add(lng);

                    }
                }
            }
            return result;
        }

        public static string PadText(string txt, Enums.TextAlign align, int size)
        {
            return PadText(txt, align, size, " ");
        }
        public static string PadText(string txt, Enums.TextAlign align, int size, string padChar)
        {
            string s = txt;
            if (!string.IsNullOrEmpty(s) && s.Length < size)
            {
                int totSpaces = 0, spacesLeft = -1, spacesRight = -1;
                totSpaces = size - txt.Length;
                if (align == Enums.TextAlign.Left)
                {
                    spacesLeft = 0;
                }
                if (align == Enums.TextAlign.Right)
                {
                    spacesRight = 0;
                }
                if (align == Enums.TextAlign.Center)
                {
                    spacesLeft = totSpaces / 2;
                }
                if (spacesLeft == -1)
                    spacesLeft = totSpaces - spacesRight;
                if (spacesRight == -1)
                    spacesRight = totSpaces - spacesLeft;
                s = "";
                for (int i = 0; i < spacesLeft; i++)
                    s += padChar;
                s += txt;
                for (int i = 0; i < spacesRight; i++)
                    s += padChar;
            }
            return s;

        }

        

        public static string GetPriceForHTML(double price)
        {
            return GetPriceForHTML(price, "&euro;");
        }

        public static string GetPriceForHTML(double price, string currency)
        {

            string magnitude = price < 0 ? "-" : "";
            price = Math.Abs(price);

            return magnitude + currency + price.ToString("#,0.00");
        }

        public static string Substring(string str, int fromIndex, int toIndex)
        {
            return str.Substring(fromIndex, toIndex - fromIndex);
        }

        public static string ConvertPlainTextToHTML(
            string str,
            bool wrapInParagraphTags = true,
            bool convertSingleLineBreaksIntoParagraphs = false)
        {
            if (str != null)
            {
                str = HtmlEncode(str);
                while (str.Contains("  "))
                {
                    str = Regex.Replace(str, "  ", "&nbsp; ", RegexOptions.Singleline); //remove all enters
                }
                if (wrapInParagraphTags)
                {
                    string lineBreakConstant = "__LINE_BREAK__";
                    str = Regex.Replace(str, "\r\n", lineBreakConstant, RegexOptions.Singleline); //remove all enters
                    str = Regex.Replace(str, "\n", lineBreakConstant, RegexOptions.Singleline); //remove all enters

                    string lineBreakSeparator = lineBreakConstant;
                    if (!convertSingleLineBreaksIntoParagraphs)
                    {
                        lineBreakSeparator += lineBreakConstant;
                    }
                    str += lineBreakSeparator; // add ending paragraph

                    str = Regex.Replace(str, "(.*?)" + lineBreakSeparator, "<p>$1</p>", RegexOptions.Singleline); //remove all enters
                    str = Regex.Replace(str, "(\r\n|\r|\n)", "", RegexOptions.Singleline); //remove all enters
                    str = Regex.Replace(str, lineBreakConstant, "<br />", RegexOptions.Singleline); //remove all enters
                }
                else
                {
                    str = Regex.Replace(str, "(\r\n|\r|\n)", "<br />", RegexOptions.Singleline); //remove all enters
                }
            }
            return str;
        }

        private static string _ConvertHTMLToPlainText_AHrefMatchProcessor(Match m)
        {
            string s = m.Value;
            if (!m.Groups[6].Value.ToLower().StartsWith("http"))
                s = m.Groups[3].Value;
            return s;

        }

        public static string ReplaceAllHtmlTagAttributeQuotes(string html, char quoteCharacter)
        {

            //html = "asdfasdfasdfsd<a href='asdfasdf'>asdadf'asdfa<a href='asdfasdf'></a>sdf</a>";

            StringBuilder sb = new StringBuilder(html);
            Regex r = new Regex("<([^/].*?)>", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Singleline);
            var matches = r.Matches(html);

            foreach (Match match in matches)
            {
                int index = match.Index;
                Regex r2 = new Regex("'|\"", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Singleline);
                var matches2 = r2.Matches(match.Value);
                foreach (Match match2 in matches2)
                {
                    int realIndex = match2.Index + index;
                    sb[realIndex] = quoteCharacter;
                }
            }
            html = sb.ToString();
            return html;
        }

        public static string ConvertHTMLToPlainText(string str, bool addLineBreaksAfterBlockTags)
        {
            return ConvertHTMLToPlainText(str, addLineBreaksAfterBlockTags ? "\r\n\r\n" : " ", addLineBreaksAfterBlockTags ? "\r\n" : " ");
        }

        private static Regex _htmlToPlainText_LineBreak = new Regex("(\r|\n|\t)", RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex _htmlToPlainText_AnchorTags = new Regex("<a(.*?)href=(\"|')(.*?)(\"|')(.*?)>(.*?)</a>", RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex _htmlToPlainText_EndTags = new Regex("</(tr|h1|h2|h3|h4|h5|h6|p|blockquote).*?>|<br.*?/>", RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex _htmlToPlainText_LineBreaks = new Regex("<br.*?/>", RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex _htmlToPlainText_ExtraTags = new Regex("<.*?/?>", RegexOptions.Singleline | RegexOptions.Compiled);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="blockEndTagReplaceCharacters"></param>
        /// <param name="brReplaceCharacters"></param>
        /// <param name="replaceLinksWithHrefIfNotMatching">Replaces an anchor link with it's href, if its text does not match the same href</param>
        /// <returns></returns>
        public static string ConvertHTMLToPlainText(string str, string blockEndTagReplaceCharacters = "\r\n\r\n", string brReplaceCharacters = "\r\n", bool replaceLinksWithHrefIfNotMatching = true)
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = _htmlToPlainText_LineBreak.Replace(str, ""); //remove all enters
                if (replaceLinksWithHrefIfNotMatching)
                {//change a hrefs, so that if the text in between does not include link, copy link
                    var matches = _htmlToPlainText_AnchorTags.Matches(str);
                    str = RegexHelpers.ProcessRegExMatches(str, matches, _ConvertHTMLToPlainText_AHrefMatchProcessor);
                }
                if (!string.IsNullOrEmpty(blockEndTagReplaceCharacters))
                {
                    str = _htmlToPlainText_EndTags.Replace(str, blockEndTagReplaceCharacters); //change all blocks to 2 enter
                }
                if (!string.IsNullOrEmpty(brReplaceCharacters))
                {
                    str = _htmlToPlainText_LineBreaks.Replace(str, brReplaceCharacters); //change all line breaks to enter
                }
                //Just in case they are not removed above when replacing
                str = _htmlToPlainText_ExtraTags.Replace(str, "");
                str = RemoveMultipleSpaces(str);
                str = HtmlDecode(str);
            }

            return str;


            //<asdfsdfasdfasdf askdfja;ksjf ;asfd>asdfasdfasdf</asdfasdf>
        }

        public static string RemoveMultipleSpaces(string text)
        {
            text = text ?? "";
            text = Regex.Replace(text, "( )+", " ");
            return text;
        }

        public static string GetFirstHtmlParagraph(string htmlText, bool removeTags)
        {
            int index = htmlText.IndexOf("</p>");
            if (index != -1)
            {
                htmlText = htmlText.Substring(0, index + "</p>".Length);
            }
            if (removeTags)
            {
                htmlText = ConvertHTMLToPlainText(htmlText, true);
            }
            htmlText = htmlText.Trim();
            return htmlText;
        }

        public static string GetFirstPlainTextParagraph(string text)
        {
            text = text.Trim();
            int lineFeedIndex = text.IndexOf("\r\n");

            if (lineFeedIndex == -1) lineFeedIndex = text.IndexOf("\n");

            if (lineFeedIndex != -1)
            {
                text = text.Substring(0, lineFeedIndex);
                text = text.Trim();
            }
            return text;
        }

        public static string GetFirstParagraph(string text, bool removeTagsIfHtml)
        {
            bool isHtml = HtmlHelpers.IsHtml(text);
            if (isHtml)
            {
                return GetFirstHtmlParagraph(text, removeTagsIfHtml);
            }
            else
            {
                return GetFirstPlainTextParagraph(text);
            }
        }

        public static string Base64Encode(string data)
        {

            try
            {
                byte[] encData_byte = new byte[data.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(data);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Encode" + e.Message);
            }



        }

        public static string Base64Decode(string data)
        {
            try
            {
                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                byte[] todecode_byte = Convert.FromBase64String(data);
                int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                string result = new String(decoded_char);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Decode" + e.Message);
            }
        }

        public static string ReplaceDiacriticsInText(string txt)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (txt != null)
            {


                String normalizedString = txt.Normalize(NormalizationForm.FormD);

                for (int i = 0; i < normalizedString.Length; i++)
                {
                    Char c = normalizedString[i];
                    if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                        stringBuilder.Append(c);
                }
            }
            string s = stringBuilder.ToString();
            s = TextHelpers.ReplaceTexts(s, "c", "ċ");
            s = TextHelpers.ReplaceTexts(s, "z", "ż");
            s = TextHelpers.ReplaceTexts(s, "h", "ħ");
            s = TextHelpers.ReplaceTexts(s, "g", "ġ");
            s = TextHelpers.ReplaceTexts(s, "C", "Ċ");
            s = TextHelpers.ReplaceTexts(s, "Z", "Ż");
            s = TextHelpers.ReplaceTexts(s, "H", "Ħ");
            s = TextHelpers.ReplaceTexts(s, "G", "Ġ");
            return s;


        }
        public static int CountCharactersInString(string str, string regExpPatternToCount)
        {
            Regex r = new Regex(regExpPatternToCount, RegexOptions.Multiline | RegexOptions.Singleline);
            MatchCollection matches = r.Matches(str);
            return matches.Count;
        }

        /// <summary>
        /// Ensures that the text link starts with HTTP or HTTPs
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string EnsureTextIsWebLink(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                string valueToLower = value.ToLower();

                if (!valueToLower.StartsWith("http://") &&
                    !valueToLower.StartsWith("https://") &&
                    !valueToLower.StartsWith("ftp://"))
                    value = "http://" + value;
            }
            return value;


        }


        /// <returns></returns>
        /// <summary>
        /// Removes the text BEFORE the first occurrence of text.  E.g, if str is http://www.test.com and text to find is ://, it will result in www.test.com
        /// </summary>
        /// <param name="str"></param>
        /// <param name="textToFind"></param>
        /// <param name="leaveTextToFindIntact">Whether to leave the text to find intact.  If true, example would return ://www.test.com </param>
        /// <returns></returns>
        public static string RemoveTextBeforeFirstIndexOf(this string str, string textToFind, bool leaveTextToFindIntact = false)
        {
            int indexOf = str.IndexOf(textToFind);


            return _removeTextBeforeIndex(str, indexOf, textToFind.Length, leaveTextToFindIntact);
        }
        /// <summary>
        /// Removes the text AFTER the first occurrence of text.  E.g, if str is http://www.test.com and text to find is ://, it will result in http://
        /// </summary>
        /// <param name="str">Text</param>
        /// <param name="textToFind">Text to find</param>
        /// <param name="leaveTextToFindIntact">Whether to leave the text to find intact.  If false, example would return www.test.com </param>
        /// <returns></returns>
        public static string RemoveTextAfterFirstIndexOf(this string str, string textToFind, bool leaveTextToFindIntact = false)
        {
            int indexOf = str.IndexOf(textToFind);

            return _removeTextAfterIndex(str, indexOf, textToFind.Length, leaveTextToFindIntact);
        }

        public static string RemoveTextAfterLastIndexOf(this string str, string textToFind, bool leaveTextToFindIntact = false)
        {
            int indexOf = str.LastIndexOf(textToFind);


            return _removeTextAfterIndex(str, indexOf, textToFind.Length, leaveTextToFindIntact);
        }

        public static string RemoveTextBeforeLastIndexOf(this string str, string textToFind, bool leaveTextToFindIntact = false)
        {
            int indexOf = str.LastIndexOf(textToFind);
            return _removeTextBeforeIndex(str, indexOf, textToFind.Length, leaveTextToFindIntact);
        }
        private static string _removeTextAfterIndex(string str, int indexOf, int lenOfText, bool leaveTextToFindIntact)
        {
            if (leaveTextToFindIntact && indexOf > -1)
            {
                indexOf += lenOfText;
            }
            if (indexOf > -1)
            {
                str = str.Substring(0, indexOf);
            }
            return str;
        }

        public static string ConvertUnderstoreToHypen(string text)
        {
            return text.Replace('_', '-');
        }

        private static string _removeTextBeforeIndex(string str, int indexOf, int lenOfText, bool leaveTextToFindIntact)
        {
            int len = lenOfText;
            if (leaveTextToFindIntact)
            {
                len = 0;
            }
            if (indexOf > -1)
            {
                str = str.Substring(indexOf + len);
            }
            return str;
        }

        /// <summary>
        /// This will format a URL to include the HTTP://, if it does not start with it or a slash.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string FormatUrlToIncludeHttp(string value)
        {
            string s = value;
            if (s != null)
            {
                if (!s.StartsWith("/") && !s.ToLower().StartsWith("http://"))
                {
                    s = "http://" + s;
                }

            }
            return s;
        }

        public static List<string> SplitTextIntoLines(string text)
        {
            string[] lines = text.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
            List<string> result = new List<string>();
            result.AddRange(lines);
            return result;

        }

        /// <summary>
        /// Pads all lines by an amount of spces
        /// </summary>
        /// <param name="p"></param>
        /// <param name="padAmount"></param>
        /// <returns></returns>
        public static string PadAllLines(string text, int padAmount)
        {
            string padding = RepeatText(" ", padAmount);
            var lines = SplitTextIntoLines(text);
            StringBuilder sb = new StringBuilder();
            foreach (var l in lines)
            {
                sb.AppendLine(padding + text);
            }
            return sb.ToString();

        }
        public static string GetTextBetweenIndexes(string text, int startIndex, int endIndex)
        {
            string s = null;
            if (!string.IsNullOrEmpty(text))
            {
                int len = endIndex - startIndex + 1;
                s = text.Substring(startIndex, len);
            }
            return s;
        }

        public static int LastIndexOfBeforePosition(string text, string txtToFind, int beforePosition, int startIndex = 0)
        {
            int result = -1;
            if (!string.IsNullOrEmpty(text))
            {
                string s = text;
                if (text.Length > beforePosition)
                    s = text.Substring(beforePosition);
                result = s.LastIndexOf(txtToFind);
            }
            return result;
        }

        /// <summary>
        /// Compares strings. Works even if either s1 or s2 is null
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static bool CompareCaseInsensitiveForExactMatch(this string s, string stringToCompareWith)
        {
            return string.Compare(s, stringToCompareWith, true) == 0;

        }

        /// <summary>
        /// Compares strings. Works even if either s1 or s2 is null
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static int CompareStrings(string s1, string s2)
        {
            return System.String.CompareOrdinal(s1, s2);
        }

        /// <summary>
        /// This is used to 'hide' data in a string, like credit card numbers, etc
        /// </summary>
        /// <param name="pass"></param>
        /// <param name="p"></param>
        /// <param name="p_2"></param>
        /// <returns></returns>
        public static string HideStringData(string txt, int charactersToShowFromStart, int charactersToShowFromEnd, char charToHideWith = '*')
        {
            StringBuilder sb = new StringBuilder();
            if (txt != null)
            {
                for (int i = 0; i < txt.Length; i++)
                {
                    var c = txt[i];
                    if ((i < charactersToShowFromStart) ||
                        (i >= txt.Length - charactersToShowFromEnd))
                    {
                        sb.Append(c);
                    }
                    else
                    {
                        sb.Append(charToHideWith);
                    }
                }
            }
            return sb.ToString();
        }
        private static void HideStringDataTest()
        {
            string s = "karl";
            string result = HideStringData(s, 2, 2);
            result = HideStringData(s, 1, 1);
            result = HideStringData(s, 0, 6);
            result = HideStringData(s, 6, 0);
            s = "karlcassar";
            result = HideStringData(s, 2, 2);
            result = HideStringData(s, 4, 2);
            s = null;
            result = HideStringData(s, 4, 2);
            s = "";
            result = HideStringData(s, 4, 2);
            s = "k";
            result = HideStringData(s, 0, 0);
            s = "kus";
            result = HideStringData(s, 4, 2);

        }

        public static string Trim(string code)
        {
            string s = code;
            if (s != null)
            {
                s = s.Trim();
            }
            return s;
        }

        public static string UnescapeEscapedCharacters(string input)
        {//\\\" > \"
            input = input.Replace("\\r", "\r");
            input = input.Replace("\\n", "\n");
            input = input.Replace("\\t", "\t");
            input = input.Replace("\\\"", "\"");
            return input;
        }

        public static string RemoveCharacterFromString(string str, string character)
        {
            return str.Replace(character, "");
        }

        public static string GetMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            foreach (byte t in hash)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
