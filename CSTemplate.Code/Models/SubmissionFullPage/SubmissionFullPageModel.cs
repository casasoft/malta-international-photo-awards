﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Models.Shared.RequestFeedbackForSubmissionForm;
using CSTemplate.Code.Models.Shared.SubmitFeedbackForm;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.SubmissionFullPage
{
    public class SubmissionFullPageModel : USNBaseViewModel
    {
        public string SubmissionTitle { get; set; }
        public string AuthorOfSubmission { get; set; }
        public List<_ItemPhotoModel> ListOfImages { get; set; } //list of images to show
        
        //feedback
        public bool IsJury { get; set; }
        public bool IsOwnerOfSubmission { get; set; }
        public bool HasBeenSubmittedForFeedback { get; set; }
        public bool JurySubmittedFeedback { get; set; }
        public bool FeedbackAlreadyReceived { get; set; }
        public bool AwardEditionCompleted { get; set; }

        public DateTime SubmissionDate { get; set; }
        public string SubmissionLocation { get; set; }
        public string SubmissionDescription { get; set; }

        public _SubmitFeedbackFormModel SubmitFeedbackFormModel { get; set; }
        public _RequestFeedbackForSubmissionFormModel RequestFeedbackForSubmissionFormModel { get; set; }


        public string ReceivedFeedbackTitleText { get; set; }
        public string ReceivedFeedbackContent { get; set; }

        public bool RequestFeedbackSectionVisible { get; set; }




        public string AuthorsOtherSubmissionsParagraph { get; set; }
        public _ListingItemsAsMasonryPartialModel ListingOfAuthorsOtherSubmissions { get; set; }
        public int ItemsPerPage { get; set; }
        public int ItemsTotalAmount { get; set; }
        
        //success mesage when Jury submitted a feedback
        public string SuccessMessage { get; set; }

        //public bool PendingForFeedbackPayment { get; set; }
        //public int SubmissionFeedbackRequestOrderId { get; set; }

        public SubmissionFullPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
