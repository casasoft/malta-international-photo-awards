﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Users;
using OfficeOpenXml;
using Umbraco.Web;
using System.Web.Mvc;
using CSTemplate.Code.Constants;

namespace CSTemplate.Code.Services.ShortListedPhotos
{

    public interface IDownloadShortListedPhotosService
    {
        byte[] Download(UmbracoHelper umbracoHelper, string absoluteUrl);
    }

    [Service]
    public class DownloadShortListedPhotosService : IDownloadShortListedPhotosService
    {
        private readonly IGetShortListedPhotoListingService _getShortListedPhotoListingService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;

        public DownloadShortListedPhotosService(
            IGetShortListedPhotoListingService getShortListedPhotoListingService,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService,
            IGetUserFilterByUserIdService getUserFilterByUserIdService
        )
        {
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _getShortListedPhotoListingService = getShortListedPhotoListingService;
        }
        public byte[] Download(UmbracoHelper umbracoHelper, string absoluteUrl)
        {
            var stream = new MemoryStream();
            var shortListedPhotos = _getShortListedPhotoListingService.Get();
            int columnCounter = 2;

            using (var package = new ExcelPackage())
            {
                var workSheet = package.Workbook.Worksheets.Add("ShortListedPhotos");
                workSheet.Cells["A1"].Value = "ImageUrl";
                workSheet.Cells["B1"].Value = "ImageName";
                workSheet.Cells["C1"].Value = "Description";
                workSheet.Cells["D1"].Value = "Date";
                workSheet.Cells["E1"].Value = "Award Edition";
                workSheet.Cells["F1"].Value = "Award Category";
                workSheet.Cells["G1"].Value = "Photographer";
                workSheet.Cells["H1"].Value = "ShortListedDate";

                workSheet.Cells["A1"].Style.Font.Bold = true;
                workSheet.Cells["B1"].Style.Font.Bold = true;
                workSheet.Cells["C1"].Style.Font.Bold = true;
                workSheet.Cells["D1"].Style.Font.Bold = true;
                workSheet.Cells["E1"].Style.Font.Bold = true;
                workSheet.Cells["F1"].Style.Font.Bold = true;
                workSheet.Cells["G1"].Style.Font.Bold = true;
                workSheet.Cells["H1"].Style.Font.Bold = true;

                workSheet.DefaultColWidth = 60;
                workSheet.DefaultRowHeight = 30;
                if (shortListedPhotos != null && shortListedPhotos.Count > 0)
                {
                    foreach (var shortListedPhoto in shortListedPhotos)
                    {
                        var awardAndCategoryInformations = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, shortListedPhoto.Submission.CategoryId);
                        var photographerName = _getUserFilterByUserIdService.Get(umbracoHelper, shortListedPhoto.Submission.UserId);

                        var fullPhotoUrl = absoluteUrl + shortListedPhoto.PhotoUrl;
                        workSheet.Cells[$"A{columnCounter}"].Hyperlink = new Uri(fullPhotoUrl);
                        workSheet.Cells[$"B{columnCounter}"].Value = shortListedPhoto.Submission.Title;
                        workSheet.Cells[$"C{columnCounter}"].Value = shortListedPhoto.Submission.Description;
                        workSheet.Cells[$"D{columnCounter}"].Value = shortListedPhoto.Submission.Date.ToString("dd/MM/yyyy");
                        workSheet.Cells[$"E{columnCounter}"].Value = awardAndCategoryInformations.AwardItem.AwardItemName;
                        workSheet.Cells[$"F{columnCounter}"].Value = awardAndCategoryInformations.AwardCategoryItemName;
                        workSheet.Cells[$"G{columnCounter}"].Value = photographerName.Name + " " + photographerName.Surname;
                        workSheet.Cells[$"H{columnCounter}"].Value = shortListedPhoto.Submission.ShortListedDate.ToString("dd/MM/yyyy");

                        columnCounter++;
                    }

                }


                package.Save();

                return package.GetAsByteArray();
            }

        }
    }
}
