﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Constants;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Members;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Database;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;
namespace CSTemplate.Code.Services.Users
{
    public interface IGetUserFilterByNameAndSurnameService
    {
        List<_MemberItemModel> Get(UmbracoHelper umbracoHelper, string nameSurname);
    }
    [Service]
    public class GetUserFilterByNameAndSurnameService : IGetUserFilterByNameAndSurnameService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        public GetUserFilterByNameAndSurnameService(
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public List<_MemberItemModel> Get(UmbracoHelper umbracoHelper, string nameSurname)
        {
            var returnMemberModel = new List<_MemberItemModel>();
            if (string.IsNullOrEmpty(nameSurname)) return returnMemberModel;
            
            var getAllMember = ApplicationContext.Current.Services.MemberService.GetAllMembers();
            var filterMembers = getAllMember.Where(x => x.Name.Contains(nameSurname)).ToList();
            foreach (var memberItem in filterMembers)
            {
                var member = new Member(_umbracoHelperService.TypedMember(umbracoHelper, memberItem.Id));
                returnMemberModel.Add(new _MemberItemModel
                {
                    MemberId = member.Id,
                    Name = member.ClientName,
                    Surname = member.ClientSurname,
                    InsertedPhone = member.Phone,
                    Nationality = member.Nationality,
                    County = member.Country,
                    InsertedEmail = memberItem.Email,
                });
               
            }
            
            return returnMemberModel;
        }
    }
}
