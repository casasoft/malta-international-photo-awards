﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.CategoryGallery;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Gallery;
using Microsoft.Owin.Security.Provider;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class CategoryGalleryPageDataController : USNBaseController
    {
        //private readonly IGetPagerModelService _getPagerModelService;
        //private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        private readonly IGetAllGallerySubmissionsListingFilterByGalleryCategoryIdService _getAllGallerySubmissionsListingFilterByGalleryCategoryIdService;

        public CategoryGalleryPageDataController(
            //IGetPagerModelService getPagerModelService 
            //IUmbracoHelperService umbracoHelperService, 
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetAllGallerySubmissionsListingFilterByGalleryCategoryIdService getAllGallerySubmissionsListingFilterByGalleryCategoryIdService
            )
        {
            _getAllGallerySubmissionsListingFilterByGalleryCategoryIdService = getAllGallerySubmissionsListingFilterByGalleryCategoryIdService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            //_umbracoHelperService = umbracoHelperService;
            //_getPagerModelService = getPagerModelService;
        }

        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User_And_Short_Time)]
        public virtual ActionResult CategoryGalleryPage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var galleryCategoryId = model.Content.Id;
            int pageNumber = 1;
            if (!string.IsNullOrEmpty(Request.QueryString["page"]) && Request.QueryString["page"] != null)
            {
                pageNumber = Convert.ToInt32(Request.QueryString["page"]);
            }
            CategoryGalleryPageModel categoryGalleryPageModel = _getAllGallerySubmissionsListingFilterByGalleryCategoryIdService.Get(umbracoHelper, model.Content, galleryCategoryId, pageNumber);
                
                
            //new CategoryGalleryPageModel(model.Content);

            //DateTime currentTime = DateTime.Now;


            //int itemTotalAmounts = 40;
            ////how many item to show on the page
            //categoryGalleryPageModel.ItemsPerPage = 6;

            ////total number of listing items.
            //categoryGalleryPageModel.ItemsTotalAmount = itemTotalAmounts;

            //var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper, categoryGalleryPageModel.ItemsPerPage, categoryGalleryPageModel.ItemsTotalAmount);



            //categoryGalleryPageModel.ListingItemsAsMasonryPartial = new _ListingItemsAsMasonryPartialModel()
            //{

            //    ListOfPhotos = new List<_ItemPhotoModel>()
            //    {
            //        #region DummyData
            //         new _ItemPhotoModel()
            //                {
            //                    ItemPhotoTitle = "Test title Malta1",
            //                    SubmissionId = "141",
            //                    PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //                    ItemPhotoCategory = "nature",
            //                    ItemPhotoDate = currentTime,
            //                    ItemPhotoImgURL = "/dist/assets/img/1.jpg",
            //                    ItemPhotoName = "John Smith",
            //                    ItemPhotoAward = "Landscape photography Award",
            //                    ItemPhotoLocation = "Vence, Italy",
            //                    ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",

            //                },
            //                new _ItemPhotoModel()
            //                {
            //                    ItemPhotoTitle = "Test title Malta1",
            //                    SubmissionId = "142",
            //                    ItemPhotoCategory = "nature",
            //                    ItemPhotoDate = currentTime,
            //                    ItemPhotoImgURL = "/dist/assets/img/2.jpg",
            //                    ItemPhotoName = "John Smith",
            //                    ItemPhotoAward = "Landscape photography Award",
            //                    ItemPhotoLocation = "Vence, Italy",
            //                    ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //                },
            //                new _ItemPhotoModel()
            //                {

            //                    ItemPhotoTitle = "Test title Malta1",
            //                    SubmissionId = "143",
            //                    ItemPhotoCategory = "nature",
            //                    ItemPhotoDate = currentTime,
            //                    ItemPhotoImgURL = "/dist/assets/img/3.jpg",
            //                    ItemPhotoName = "John Smith",
            //                    ItemPhotoAward = "Landscape photography Award",
            //                    ItemPhotoLocation = "Vence, Italy",
            //                    ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //                },
            //                new _ItemPhotoModel()
            //                {
            //                    ItemPhotoTitle = "Test title Malta1",
            //                    SubmissionId = "109",
            //                    ItemPhotoCategory = "nature",
            //                    ItemPhotoDate = currentTime,
            //                    ItemPhotoImgURL = "/dist/assets/img/4.jpg",
            //                    ItemPhotoName = "John Smith",
            //                    ItemPhotoAward = "Landscape photography Award",
            //                    ItemPhotoLocation = "Vence, Italy",
            //                    ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",

            //                },
            //                new _ItemPhotoModel()
            //                {
            //                    ItemPhotoTitle = "Test title Malta1",
            //                    SubmissionId = "142",
            //                    ItemPhotoDate = currentTime,
            //                    ItemPhotoImgURL = "/dist/assets/img/5.jpg",
            //                    ItemPhotoName = "John Smith",
            //                    ItemPhotoAward = "Landscape photography Award",
            //                    ItemPhotoLocation = "Vence, Italy",
            //                    ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //                    ItemPhotoCategory = "war"
            //                },
            //                new _ItemPhotoModel()
            //                {
            //                    ItemPhotoTitle = "Test title Malta1",
            //                    SubmissionId = "141",
            //                    ItemPhotoCategory = "nature",
            //                    ItemPhotoDate = currentTime,
            //                    ItemPhotoImgURL = "/dist/assets/img/4.jpg",
            //                    ItemPhotoName = "John Smith",
            //                    ItemPhotoAward = "Landscape photography Award",
            //                    ItemPhotoLocation = "Vence, Italy",
            //                    ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",

            //                },
                    

            //        #endregion
                   
            //    },
            //    Pagination = pagerModel
            //};

            //categoryGalleryPageModel.ShowingResults = "1";
            //categoryGalleryPageModel.ItemsPerPage = 2;
            //categoryGalleryPageModel.ItemsTotalAmount = 3;


        
            return base.Populate(categoryGalleryPageModel);
        }


    }
}