﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Umbraco.Models.Authentication
{
    public class CommonEmailMessageTemplate
    {
        public string RenderBody { get; set; }
    }
}
