﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.JuryMember;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;
using constantsValues = CSTemplate.Code.Constants.ConstantValues;
using dictionaryValues = CSTemplate.Code.Constants.Enums.DictionaryKey;
namespace CSTemplate.Code.Services.Juries
{

    public interface IGetAllJuriesService
    {
        _ListingJuryMembersModel Get(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class GetAllJuriesService : IGetAllJuriesService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetAllJuriesService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public _ListingJuryMembersModel Get(UmbracoHelper umbracoHelper)
        {
            var juries = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, constantsValues.Juries_Listing_Page_Data_Document_Type_Alias)
                .Descendants()
                .Where(x => x.DocumentTypeAlias.Equals(constantsValues.Jury_Member_Data_Document_Type_Alias))
                .Select(x => (JuryMemberData)x)
                .ToList();
            List<_JuryMemberModel> listOfJuryMembers = new List<_JuryMemberModel>();
            foreach (var jury in juries)
            {
                listOfJuryMembers.Add(new _JuryMemberModel
                {
                    JudgeName = jury.FullName,
                    JudgePosition = jury.Title,
                    JudgeImgURL = jury.Photo.Url,
                    JudgeDescription = jury.Description.ToHtmlString(),
                    JuryID = jury.Id
                    
                });
            }
            var returnOfListingJuryMembersModel = new _ListingJuryMembersModel
            {
                SectionText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, dictionaryValues.HomePage_Juries_Listing_Explanation_Text),
                SectionTitle = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, dictionaryValues.HomePage_Juries_Listing_Title_Text),
                ListofJudges = listOfJuryMembers
                
            };
            return returnOfListingJuryMembersModel;
        }
    }
}
