﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.Emails
{

    public interface IGetEmailTemplatesPlaceholderForCurrentDomainRootNodeService
    {
        EmailTemplates Get(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class GetEmailTemplatesPlaceholderForCurrentDomainRootNodeService : IGetEmailTemplatesPlaceholderForCurrentDomainRootNodeService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetEmailTemplatesPlaceholderForCurrentDomainRootNodeService(IUmbracoHelperService
            umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }

        public EmailTemplates Get(UmbracoHelper umbracoHelper)
        {
            return
              new EmailTemplates(new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode.Children
              .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Email_Template_Document_Type_Alias)).FirstOrDefault());

        }
    }
}
