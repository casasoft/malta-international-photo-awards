﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Shared;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.HomePageComponents
{

    public interface IGetListingPrizesButtonComponentService
    {
        AllListingPrizesPageData Get(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class GetListingPrizesButtonComponentService : IGetListingPrizesButtonComponentService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetListingPrizesButtonComponentService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public AllListingPrizesPageData Get(UmbracoHelper umbracoHelper)
        {
            var allPrizesPageData = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper,Constants.ConstantValues.USN_Advanced_Page_Components_Page_Data_Document_Type_Alias)
              .Descendants()
              .Where(x=>x.DocumentTypeAlias.Equals(Constants.ConstantValues.All_Listing_Prizes_Page_Data_Document_Type_Alias))
              .Select(x=>(AllListingPrizesPageData)x).FirstOrDefault();
            if (allPrizesPageData.ButtonLink == null && allPrizesPageData==null) return null;
                
            
            return allPrizesPageData;
        }
    }
}
