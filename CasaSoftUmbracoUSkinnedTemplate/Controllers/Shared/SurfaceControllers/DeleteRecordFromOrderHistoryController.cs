﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared.OrderHistoryItem;
using CSTemplate.Code.Services.Orders;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Shared.SurfaceControllers
{
    public partial class DeleteRecordFromOrderHistoryController : SurfaceController
    {
        private readonly IDeleteOrderFilterByOrderIdService _deleteOrderFilterByOrderIdService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        public DeleteRecordFromOrderHistoryController(IDeleteOrderFilterByOrderIdService deleteOrderFilterByOrderIdService, IUmbracoControllerWrapperService umbracoControllerWrapperService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _deleteOrderFilterByOrderIdService = deleteOrderFilterByOrderIdService;
        }
        // GET: SubmitPhotoFormSurface
        [HttpPost]
        public virtual ActionResult HandleDeleteRecordFromOrderHistory(_OrderHistoryItem OrderHistoryItemForForm)
        {
            //Radek notes:
            //I created new PaymentStatus - "DELETED". Check Enums.paymentstatus

            //inside this action you have to change the payment status of the order for "deleted".
            //then we will redirect the user to the same OrderSummaryPage but
            // in the OrderHistoryPageDataController return only list of orders that have status different that "deleted". So if the transaction has a status "deleted" don't show it".

            //also make sure that "request feedback" button on the submission full page won't redirect to deleted transaction but it will create a new order if the old one has been deleted by user. 
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            int orderId = int.Parse(OrderHistoryItemForForm.OrderNumber);
            if (orderId > 0)
            {
                _deleteOrderFilterByOrderIdService.Delete(umbracoHelper, orderId);
            }

            HttpRuntime.Cache.Remove(ConstantValues.Cache_Key_Submission_Full_Page);


            return RedirectToCurrentUmbracoPage();
        }
    }
}