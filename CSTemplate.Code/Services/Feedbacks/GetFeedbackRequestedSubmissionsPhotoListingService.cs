﻿using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Feedbacks
{
    public interface IGetFeedbackRequestedSubmissionsPhotoListingService
    {
        List<PhotoPoco> Get();
    }
    [Service]
    public class GetFeedbackRequestedSubmissionsPhotoListingService : IGetFeedbackRequestedSubmissionsPhotoListingService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetFeedbackRequestedSubmissionsPhotoListingService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public List<PhotoPoco> Get()
        {
            var listOfPhotoModel = new List<PhotoPoco>();

            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var photosTable = Constants.DatabaseConstantValues.PhotosTableName;
            var photosUrlField = Constants.DatabaseConstantValues.Photos_PhotoUrl_Field_Name;
            var photosAlias = Constants.DatabaseConstantValues.aliasForPhotosPocoTableNameAlias;
            var photoSubmissionIdField = Constants.DatabaseConstantValues.Photos_Submission_Id_Field_Name;
            var photosPrimaryKeyField = Constants.DatabaseConstantValues.Photos_Poco_Primary_Key_Value;

            var submissionsTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = Constants.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = Constants.DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = Constants.DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = Constants.DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = Constants.DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = Constants.DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionTitleField = Constants.DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionShortListedEnumIdField = Constants.DatabaseConstantValues.Submissions_Short_Listed_Enum_Id_Field_Name;
            var submissionStatusEnumIdField = Constants.DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;
            var submissionShortListedDateField = Constants.DatabaseConstantValues.Submissions_Short_Listed_Date_Field_Name;

            var submissionFeedbackRequestField = Constants.DatabaseConstantValues.Submissions_Feedback_Request_Field_Name;
            var submissionFeedbackDescriptionField = Constants.DatabaseConstantValues.Submissions_Feedback_Description_Field_Name;

            var featuredFieldId = Constants.DatabaseConstantValues.Photos_Featured_Field_Name;

            var orderByDescValue = Constants.DatabaseConstantValues.ORDER_BY_DESC;

            var sqlQuery = new Sql($@" 
                     SELECT
                        {photosAlias}.{photosPrimaryKeyField},
                        {photosAlias}.{photosUrlField},
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId},
                        {submissionsTableAlias}.{submissionTitleField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionFeedbackRequestField},
                        {submissionsTableAlias}.{submissionFeedbackDescriptionField}
                            FROM
                                {photosTable}
                                 INNER JOIN {submissionsTableName} ON  {photosAlias}.{photoSubmissionIdField} = {submissionsTableAlias}.{submissionPrimaryKeyField} 
                                     WHERE {submissionsTableAlias}.{submissionFeedbackRequestField}={Convert.ToInt32(Constants.Enums.FeedbackStatus.FeedbackRecieved)}  
                                        AND 
                                        ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                                        AND 
                                        {submissionsTableAlias}.{submissionFeedbackDescriptionField} IS NULL
                                         ORDER BY {submissionsTableAlias}.{submissionPrimaryKeyField} {orderByDescValue}
            ");
            var requestFeedbackSubmissionsSqlResult = umbracoDb.Query<PhotoPoco, SubmissionPoco>(sqlQuery).ToList();
            if (requestFeedbackSubmissionsSqlResult.Count > 0)
            {
                foreach (var submissionPhoto in requestFeedbackSubmissionsSqlResult)
                {
                    listOfPhotoModel.Add(new PhotoPoco()
                    {
                        PhotoUrl = submissionPhoto.PhotoUrl,
                        Submission = submissionPhoto.Submission
                    });
                }
            }
            return listOfPhotoModel;
        }
    }
}
