﻿using CS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CS.Common.Helpers
{
    public static class DateHelpers
    {

        public static bool DoRangesIntersect(
         DateTimeOffset range1From, DateTimeOffset range1To,
         DateTimeOffset range2From, DateTimeOffset range2To)
        {
            return !(range1From > range2To || range1To < range2From);
        }

        public static bool DoDateTimeOffsetHasTime(DateTimeOffset dateTime)
        {

            return dateTime.TimeOfDay.TotalSeconds != 0;
        }

        /// <summary>
        /// Converts C# date time format to javascript date time format
        /// </summary>
        /// <param name="dateFormat"></param>
        /// <returns></returns>
        public static string ConvertDateTimeFormatToJavascriptDateTimeFormat(string dateFormat)
        {
            const string tokenLongMonthName = "$LongMonthName$";
            const string tokenLongYearName = "$LongYearName$";
            const string tokenShortYearName = "$ShortYearName$";
            const string tokenShortMonthName = "$monthShortName$";
            dateFormat = dateFormat.Replace("MMMM", tokenLongMonthName);
            dateFormat = dateFormat.Replace("MMM", tokenShortMonthName);
            dateFormat = dateFormat.Replace("yyyy", tokenLongYearName);
            dateFormat = dateFormat.Replace("yy", tokenShortYearName);
            dateFormat = dateFormat.Replace("M", "m");
            dateFormat = dateFormat.Replace("dddd", "DD");
            dateFormat = dateFormat.Replace("ddd", "D");
            dateFormat = dateFormat.Replace(tokenLongMonthName, "MM");
            dateFormat = dateFormat.Replace(tokenShortMonthName, "M");
            dateFormat = dateFormat.Replace(tokenLongYearName, "yy");
            dateFormat = dateFormat.Replace(tokenShortYearName, "y");
            return dateFormat;
        }
        public static string ConvertJavascriptDateTimeFormatToCSharpFormat(string dateFormat)
        {
            const string tokenLongMonthName = "$LongMonthName$";
            const string tokenLongYearName = "$LongYearName$";
            const string tokenShortYearName = "$ShortYearName$";
            const string tokenShortMonthName = "$ShortMonthName$";
            dateFormat = dateFormat.Replace("MM", tokenLongMonthName);
            dateFormat = dateFormat.Replace("M", tokenShortMonthName);
            dateFormat = dateFormat.Replace("DD", "dddd");
            dateFormat = dateFormat.Replace("D", "ddd");
            dateFormat = dateFormat.Replace("yy", tokenLongYearName);
            dateFormat = dateFormat.Replace("y", tokenShortYearName);
            dateFormat = dateFormat.Replace(tokenLongMonthName, "MMMM");
            dateFormat = dateFormat.Replace(tokenShortMonthName, "MMM");
            dateFormat = dateFormat.Replace(tokenShortYearName, "yy");
            dateFormat = dateFormat.Replace(tokenLongYearName, "yyyy");
            dateFormat = dateFormat.Replace("m", "M");
            return dateFormat;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="day"></param>
        /// <param name="dayFormat">E.g. dddd</param>
        /// <returns></returns>
        public static string FormatDayOfWeekWithCurrentCulture(DayOfWeek day, string dayFormat)
        {

            int dayValue = (int)day;
            DateTimeOffset now = DateHelpers.Now();
            int nowDayValue = (int)now.DayOfWeek;
            int daysDiff = dayValue - nowDayValue;
            now = now.AddDays(daysDiff);
            return now.ToString(dayFormat);
        }
        public static string FormatTime(int hours = 0, int minutes = 0, int seconds = 0, string timeFormat = "HH:mm")
        {
            DateTime now = new DateTime(1900, 1, 1, hours, minutes, seconds);
            return now.ToString(timeFormat);
        }
        public static string FormatTimeFromHours(double hours, string timeFormat = "hh:mm")
        {
            int hoursWhole = (int)Math.Floor(hours);
            int minutes = (int)((hours - hoursWhole) * 60);

            return FormatTime(hoursWhole, minutes, 0, timeFormat);
        }
        public static DateTimeOffset GetStartOfDay(this DateTimeOffset date)
        {
            return date.Date;
            //TimeSpan timeSpanToSubtract = new TimeSpan(0,date.Hour,date.Minute,date.Second,date.Millisecond);
            //var newDate = date.Subtract(timeSpanToSubtract);
            //return newDate;

        }
        public static DateTimeOffset GetEndOfDay(this DateTimeOffset date)
        {
            TimeSpan timeSpanToSubtract = new TimeSpan(0, 0, 0, 0, 1);
            var newDate = GetStartOfDay(date).AddDays(1).Subtract(timeSpanToSubtract);
            //TimeSpan timeSpanToSubtract = new TimeSpan(0, date.Hour, date.Minute, date.Second, date.Millisecond);
            //var newDate = date.Subtract(timeSpanToSubtract);
            return newDate;

            //DateTime newDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            //return newDate;

            //DateTimeOffset utc = date.UtcDateTime;
            //var dt = new DateTimeOffset(utc.Year, utc.Month, utc.Day, 23, 59, 59,999, new TimeSpan(0));
            //return dt;
        }
        public static DateTimeOffset SetHours(this DateTimeOffset date, int hours)
        {
            int diff = hours - date.Hour;
            return date.AddHours(diff);
        }
        public static DateTimeOffset SetMinutes(this DateTimeOffset date, int minutes)
        {
            int diff = minutes - date.Minute;
            return date.AddMinutes(diff);
        }
        public static DateTimeOffset SetSeconds(this DateTimeOffset date, int seconds)
        {
            int diff = seconds - date.Second;
            return date.AddSeconds(diff);
        }
        public static DateTimeOffset SetMilliseconds(this DateTimeOffset date, int milliseconds)
        {
            int diff = milliseconds - date.Millisecond;
            return date.AddMilliseconds(diff);
        }
        public static bool IsDateWithinDateRange(DateTimeOffset? rangeStart, DateTimeOffset? rangeEnd, DateTimeOffset date, bool considerHours = true, bool considerMinutes = true, bool considerSeconds = true)
        {
            //Just to make sure they are all the same time offset
            if (rangeStart.HasValue) rangeStart = rangeStart.Value.ToLocalTime();
            if (rangeEnd.HasValue) rangeEnd = rangeEnd.Value.ToLocalTime();
            date = date.ToLocalTime();

            if (!considerHours)
            {
                if (rangeStart.HasValue) rangeStart = rangeStart.Value.SetHours(0);
                if (rangeEnd.HasValue) rangeEnd = rangeEnd.Value.SetHours(0);
                date = date.SetHours(0);
            }
            if (!considerMinutes)
            {
                if (rangeStart.HasValue) rangeStart = rangeStart.Value.SetMinutes(0);
                if (rangeEnd.HasValue) rangeEnd = rangeEnd.Value.SetMinutes(0);
                date = date.SetMinutes(0);
            }
            if (!considerSeconds)
            {
                if (rangeStart.HasValue) rangeStart = rangeStart.Value.SetSeconds(0);
                if (rangeEnd.HasValue) rangeEnd = rangeEnd.Value.SetSeconds(0);
                date = date.SetSeconds(0);
            }


            return ((!rangeStart.HasValue || date >= rangeStart.Value) && (!rangeEnd.HasValue || date <= rangeEnd.Value));
        }



        public static bool AreDatesOnSameDay(params DateTimeOffset[] dates)
        {
            for (int i = 0; i < dates.Length; i++)
            {
                dates[i] = dates[i].ToLocalTime(); // make sure they are all the same
            }


            bool ok = true;
            if (dates.Length > 1)
            {
                DateTimeOffset firstDate = dates[0];
                for (int i = 1; i < dates.Length; i++)
                {
                    if (dates[i].Day != firstDate.Day ||
                        dates[i].Month != firstDate.Month ||
                        dates[i].Year != firstDate.Year)
                    {
                        ok = false;
                        break;
                    }
                }
            }
            return ok;
        }
        public static string FormatDateTimeWithCurrentCulture(DateTimeOffset date, Enums.DateFormatType formatType)
        {
            return date.ToString(EnumHelpers.ConvertEnumToString(formatType), Thread.CurrentThread.CurrentCulture);
        }

        public static string FormatDateTimeWithCurrentCulture(DateTimeOffset date, string dateFormat)
        {
            return FormatDateTimeWithCulture(date, dateFormat, Thread.CurrentThread.CurrentCulture);
        }
        public static string FormatDateTimeWithCulture(DateTimeOffset date, string dateFormat, CultureInfo culture)
        {
            return date.ToString(dateFormat, culture);
        }

        public static string FormatDateTime(DateTimeOffset date, string dateFormat)
        {
            return date.ToString(dateFormat, CultureInfo.InvariantCulture);
        }
        public static string FormatDateTime(DateTime date, string dateFormat)
        {

            return date.ToString(dateFormat, CultureInfo.InvariantCulture);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <param name="format"></param>
        /// <param name="strictlyExactDateFormat">This makes sure that the format exactly must be adhered to, if it is false then for example 'yyyy' and 'yy' match also the same</param>
        /// <returns></returns>
        public static DateTimeOffset? ParseDate(string date, string format, bool strictlyExactDateFormat = false)
        {
            if (!String.IsNullOrWhiteSpace(date))
            {
                DateTimeOffset? result = null;
                DateTimeOffset d;

                if (DateTimeOffset.TryParseExact(date, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out d))
                    result = d;
                if (!strictlyExactDateFormat)
                {
                    bool dateFormatIncludesTime = format.ToLower().Contains(" HH:mm");
                    bool dateIncludesTime = date.Contains(" ") && date.Contains(":");
                    if (dateFormatIncludesTime && !dateIncludesTime)
                    {
                        date += " 00:00";
                        result = ParseDate(date, format, true);

                    }

                    if (!result.HasValue && format.IndexOf("yyyy") != -1)
                    {
                        //Date format is 'yyyy', it could be that date is 'yy'
                        format = format.Replace("yyyy", "yy");
                        result = ParseDate(date, format, true);
                    }
                    else if (!result.HasValue && format.IndexOf("yy") != -1)
                    {
                        //Date format is 'yy' so it could be that date is 'yy'
                        format = format.Replace("yy", "yyyy");
                        result = ParseDate(date, format, true);
                    }
                }
                return result;
            }
            else
            {
                return null;
            }
        }

        public static DateTimeOffset Now()
        {
            return DateTime.Now;
        }

        public static DateTimeOffset? DateFromISO8601String(string d)
        {
            string format = "yyyy-MM-dd";
            DateTimeOffset? parsedDate = null;

            parsedDate = ParseDate(d, format);
            if (parsedDate == null)
            {
                format += " HH:mm:ss";
                parsedDate = ParseDate(d, format);
            }
            return parsedDate;

        }

        public static string DateToISO8601String(DateTimeOffset d, bool includeTime, bool includeMillisecondsWithTime)
        {
            string format = "yyyy-MM-dd";
            if (includeTime)
            {
                format += "THH\\:mm\\:ss";
                if (includeMillisecondsWithTime)
                {
                    format += ".fffffff";
                }
                format += "zzz";
            }
            return d.ToString(format);
        }

        public static DateTimeOffset? ConvertFromUnixTimestampNullable(long unixTimeStamp)
        {
            DateTimeOffset? date = null;

            try
            {
                date = ConvertFromUnixTimestamp(unixTimeStamp);
            }
            catch (Exception ex)
            {

            }

            return date;
        }

        public static DateTimeOffset ConvertFromUnixTimestamp(long unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTimeOffset dtDateTime = new DateTimeOffset(1970, 1, 1, 0, 0, 0, 0, new TimeSpan());
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static double ConvertToUnixTimestamp(DateTimeOffset date)
        {
            var origin = new DateTimeOffset(1970, 1, 1, 0, 0, 0, new TimeSpan());
            double unixTimestamp = (date.ToUniversalTime() - origin).TotalSeconds;
            return unixTimestamp;
        }

        /// <summary>
        /// Returns the time in seconds after 1st jan 1970
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static long GetUnixEpochTime(DateTimeOffset date)
        {
            TimeSpan t = date.UtcDateTime - new DateTime(1970, 1, 1);
            long secondsSinceEpoch = (long)t.TotalSeconds;
            Console.WriteLine(secondsSinceEpoch);
            return secondsSinceEpoch;

        }



        /// <summary>
        /// Get all days in our range
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static List<DateTimeOffset> GetListOfDaysContainedBetweenRange(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            var resultDateTime = new List<DateTimeOffset>();
            for (var day = startDate.Date; day.Date <= endDate.Date; day = day.AddDays(1))
            {
                resultDateTime.Add(day);
            }
            return resultDateTime;
        }

        public static int GetAgeFromDateOfBirth(DateTimeOffset dateOfBirth)
        {
            var today = DateHelpers.Now();
            int age = today.Year - dateOfBirth.Year;
            return age;
        }
        /// <summary>
        /// Returns the previous day of week before a particular date.  If 'dateTime' is same as dayOfWeek, that is returned.  
        /// For example, if dayOfWeek is 'Sunday', and you give Mon 10/11/2014, the date returned is Sun 09/11/2014.  
        /// If you give as dateTime Sun 09/11/2014, returned date is Sun 09/11/2014.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="dayOfWeek"></param>
        /// <returns></returns>
        public static DateTimeOffset GetFirstDayOfWeekBeforeDate(DateTimeOffset dateTime, DayOfWeek dayOfWeek)
        {
            DateTimeOffset curr = dateTime;
            while (curr.DayOfWeek != dayOfWeek)
            {
                curr = curr.AddDays(-1);
            }
            return curr;
        }
        /// <summary>
        /// Returns the next day of week after a particular date.  If 'dateTime' is same as dayOfWeek, that is returned.  For example, if dayOfWeek is 'Sunday', and you give Mon 10/11/2014, the date returned is Sun 16/11/2014.  
        /// If you give as dateTime Sun 16/11/2014, returned date is Sun 16/11/2014.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="dayOfWeek"></param>
        /// <returns></returns>
        public static DateTimeOffset GetNextDayOfWeekAfterDate(DateTimeOffset dateTime, DayOfWeek dayOfWeek)
        {
            DateTimeOffset curr = dateTime;
            while (curr.DayOfWeek != dayOfWeek)
            {
                curr = curr.AddDays(+1);
            }
            return curr;
        }

        public static string FormatDate(DateTime date)
        {
            return date.ToString("dd MMM yyyy");
        }

        public static string FormatDateToIso8601(DateTime date)
        {
            return date.ToString("O");
        }
    }
}
