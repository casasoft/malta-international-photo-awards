﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Models.Paging;
using CSTemplate.Code.Models.Shared.OrderHistoryItem;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.OrderHistory
{
    public class OrderHistoryPageModel: USNBaseViewModel
    {
        public List<_OrderHistoryItem> ListOfOrders { get; set; }
        public string TableDateText { get; set; }
        public string TableOrdeNumberText { get; set; }
        public string TableStatusText { get; set; }
        public string TableDescriptionText { get; set; }
        public string TableTotalText { get; set; }

        //todo: [For: Backend | 2018/09/21] I use it to bind details of the order to delete (WrittenBy: Radek)        			
        public _OrderHistoryItem OrderHistoryItemForForm { get; set; }

        public PagerModel Pagination { get; set; }

        public string ShowingResults { get; set; }
        public int ItemsPerPage { get; set; }
        public int ItemsTotalAmount { get; set; }

        public OrderHistoryPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
