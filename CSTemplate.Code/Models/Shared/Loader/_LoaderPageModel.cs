﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared.Loader
{
    public class _LoaderPageModel
    {
        public string LoaderTitle { get; set; }
        public string ExplanationText { get; set; }

    }
}
