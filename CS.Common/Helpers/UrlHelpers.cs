﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace CS.Common.Helpers
{
    public static class UrlHelpers
    {
        public enum UpdateQuerystringForUrlFromNameValueCollectionUpdateType
        {
            /// <summary>
            /// Update the ones present in the querystringParameters while leaving the others in tact
            /// </summary>
            Update,
            /// <summary>
            /// Replace the entire querysting with the ones passed
            /// </summary>
            Replace
        }
       
        public static string GetNameValueCollectionAsQuerystring(NameValueCollection nvc)
        {
            return nvc == null
                ? ""
                : String.Join("&",
                    nvc.AllKeys.Select(a => a + "=" + HttpUtility.UrlEncode(nvc[a])));
        }

        public static NameValueCollection GetQuerystringFromUrlAsNameValueCollection(string url)
        {
            if (url.StartsWith("/"))
            {
                //This is temporary so that the Uri creation will work as then we will only use the querystring
                url = string.Format("{0}{1}", "http://www.test.com", url);
            }
            Uri uri = new Uri(url);
            return HttpUtility.ParseQueryString(uri.Query);
        }
        /// <summary>
        /// Update the querystring of a URL and return the new URL
        /// </summary>
        /// <param name="url"></param>
        /// <param name="querystringParameters"></param>
        /// <param name="updateType">Whether you want to update the querystring or replace it</param>
        /// <returns></returns>
        public static string UpdateQuerystringForUrlFromNameValueCollection(string url,
            NameValueCollection querystringParameters,
            UpdateQuerystringForUrlFromNameValueCollectionUpdateType updateType =
                UpdateQuerystringForUrlFromNameValueCollectionUpdateType.Update)
        {
            NameValueCollection newQuerystringParameters = querystringParameters;
            if (updateType == UpdateQuerystringForUrlFromNameValueCollectionUpdateType.Update)
            {
                //Update them
                newQuerystringParameters = GetQuerystringFromUrlAsNameValueCollection(url);
                foreach (string key in querystringParameters.Keys)
                {
                    newQuerystringParameters[key] = querystringParameters[key];
                }
            }
            //To get QS + Hashbang
            Regex r = new Regex(@"^(.*?)\?.*?(#.*?)?$");
            var match = r.Match(url);
            string path = url;
            string hashBang = "";
            if (match.Success)
            {
                path = match.Groups[1].Value;
                hashBang = match.Groups.Count >= 2 ? match.Groups[2].Value : "";
            }
            if (newQuerystringParameters.Keys.Count > 0)
            {
                return string.Format("{0}?{1}{2}", path, GetNameValueCollectionAsQuerystring(newQuerystringParameters), hashBang);
            }
            else
            {
                return string.Format("{0}{1}", path, hashBang);
            }

        }

        public static string GetQuerystringFromModelPropertiesAndValues<T>(T model, List<Expression<Func<T, object>>> OmitProperties = null)
        {
            var allProperties = ReflectionHelpers.GetAllPropertiesAndFieldsForType(typeof(T), false, false, false);
            allProperties.RemoveAll(x =>
            {
                foreach (var omitProperty in OmitProperties)
                {
                    if (omitProperty.Name == x.Name)
                    {
                        return true;
                    }
                }
                return false;
            });
            NameValueCollection nvc = new NameValueCollection();
            foreach (var property in allProperties)
            {
                var valueForMemberInfo = ReflectionHelpers.GetValueForMemberInfo(model, property);

                nvc[property.Name] = valueForMemberInfo != null ? valueForMemberInfo.ToString() : "";
            }
            return GetNameValueCollectionAsQuerystring(nvc);
        }

        public static string GetProtocolForCurrentPage()
        {
            string protocol = "http://";
            var currentHttpContext = HttpContext.Current;

            //this variable is needed so that since we might be using cloudfare for secure conenction, the boolean value Request.IsSecureConnection will not be identified
            var httpCfVisitorServerVariable = currentHttpContext.Request.ServerVariables["HTTP_CF_VISITOR"];

            bool isSecureConnection = currentHttpContext.Request.IsSecureConnection;

            if (currentHttpContext != null && currentHttpContext != null)
            {
                if (!String.IsNullOrEmpty(httpCfVisitorServerVariable))
                {
                    if (isSecureConnection || httpCfVisitorServerVariable.Contains("https"))
                    {
                        protocol = "https://";
                    }
                }
                else
                {
                    if (isSecureConnection)
                    {
                        protocol = "https://";
                    }
                }
            }


            return protocol;
        }

        /// <summary>
        /// This method will append to the respective passed relative url the language  prefix before the actual page url
        /// Eg. If current url is /en/something, this method will parse the language prefix and appended to the passed url.
        /// </summary>
        /// <param name="relativeUrl"></param>
        /// <returns>Returns the modifed passed url with the current language prefix </returns>
        public static string AppendLanguagePrefixToUrl(string relativeUrl)
        {
            string currentUrl = HttpContext.Current.Request.Url.AbsolutePath;
            string relativeUrlToReturn = relativeUrl;

            Regex currentUrlPathRegex = new Regex(@"/([a-zA-z-]+)");
            Match currentUrlPathRegexMatch = currentUrlPathRegex.Match(currentUrl);

            string languagePrefix = "";

            if (currentUrlPathRegexMatch.Success)
            {
                languagePrefix = currentUrlPathRegexMatch.Groups[1].Value;
                var urlWithTheInsertedLangaugePrefix = relativeUrlToReturn.Insert(0, "/" + languagePrefix);
                relativeUrlToReturn = urlWithTheInsertedLangaugePrefix;
            }

            return relativeUrlToReturn;
        }

        /// <summary>
        /// A method which will returns the relative url of a full/absolute url
        /// </summary>
        /// <param name="absoluteUrl"></param>
        /// <returns>Relative Url of the full url passed as a parameter</returns>
        public static string GetRelativeUrlFromAbsoluteUrl(string absoluteUrl)
        {
            try
            {
                Uri uri = new Uri(absoluteUrl);//fullUrl is absoluteUrl
                string relativeUrl = uri.PathAndQuery;//The Uri property AbsolutePath gives the relativeUrl

                return relativeUrl;
            }
            catch (Exception ex)
            {
                return absoluteUrl;
                //throw ex;
            }
        }

        /// <summary>
        /// Turns a relative URL into a fully qualified URL.
        /// (ie http://domain.com/path?query) 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="relativeUrl"></param>
        /// <returns></returns>
        public static string GetFullUrl(this HttpRequest request, string relativeUrl)
        {
            return String.Format("{0}://{1}{2}",
                            request.Url.Scheme,
                            request.Url.Authority,
                            VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        public static bool IsAbsoluteUrl(string url)
        {
            Uri result;
            return Uri.TryCreate(url, UriKind.Absolute, out result);
        }

        /// <summary>
        /// Converts the provided app-relative path into an absolute Url containing the 
        /// full host name
        /// </summary>
        /// <param name="relativeUrl">App-Relative path</param>
        /// <returns>Provided relativeUrl parameter as fully qualified Url</returns>
        /// <example>~/path/to/foo to http://www.web.com/path/to/foo</example>
        public static string ToAbsoluteUrl(this string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        public static string FileLocationToBaseDirectoryPath(this string fileLocation)
        {
            return string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, fileLocation);
        }
    }
}
