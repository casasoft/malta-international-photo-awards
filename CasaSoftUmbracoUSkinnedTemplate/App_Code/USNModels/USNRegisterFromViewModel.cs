﻿

namespace USN.USNModels
{
    public class USNRegisterFromViewModel
    {

        public string RegisterTitle { get; set; }
        public string RegisterText { get; set; }
        public  string RegisterButtonText { get; set; }
        public string RegisterUrl { get; set; }
    }
}