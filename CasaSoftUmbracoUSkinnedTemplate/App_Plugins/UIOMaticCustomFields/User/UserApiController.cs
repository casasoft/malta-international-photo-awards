﻿using CSTemplate.Code.Services.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Umbraco.Web;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.App_Plugins.UIOMaticCustomFields.User
{
    [PluginController("User")]
    public class UserApiController : UmbracoAuthorizedJsonController
    {
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;
        private UmbracoHelper _umbracoHelper;
        public UserApiController(IGetUserFilterByUserIdService getUserFilterByUserIdService)
        {
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
        }
        public string GetUserFullName(int id)
        {
            var user = _getUserFilterByUserIdService.Get(_umbracoHelper,id);
            var fullName = user.Name + " " + user.Surname;
            return fullName;
        }
    }
}
