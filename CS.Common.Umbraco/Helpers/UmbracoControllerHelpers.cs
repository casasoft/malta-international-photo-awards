﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace CS.Common.Umbraco.Helpers
{
    public static class UmbracoControllerHelpers
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controller">Either an UmbracoController or PluginController</param>
        /// <returns></returns>
        public static UmbracoHelper GetUmbracoHelperFromUmbracoController(Controller controller)
        {
            if (controller is UmbracoController)
            {
                return (controller as UmbracoController).Umbraco;
            }
            else if (controller is PluginController)
            {
                return (controller as PluginController).Umbraco;
            }
            else
            {
                throw new ArgumentException("Controller '{0}' must be either UmbracoController or PluginController",
                    controller.GetType().FullName);
            }
        }
    }
}
