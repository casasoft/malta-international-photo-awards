﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CS.Common.Helpers
{
    public static class ImageProcessingHelpers
    {
        public static string ResizeCrop(int width, int height, string imageUrl)
        {
            return $"{imageUrl}?width={width}&height={height}&mode=crop";
        }

        public static string ResizeCrop(int width, int height, string imageUrl, string overlayImage)
        {
            return $"{imageUrl}?width={width}&height={height}&mode=crop&overlay={overlayImage}&overlay.opacity=80";
        }


        public static string Resize(int width, string absoluteImageUrl, bool useRemotely = true)
        {
            string urlToReturn = "";

            if (useRemotely)
            {
                urlToReturn += $"/remote.axd?{absoluteImageUrl}";
            }
            else
            {
                urlToReturn += $"{CS.Common.Helpers.UrlHelpers.GetRelativeUrlFromAbsoluteUrl(absoluteImageUrl)}";
            }

            urlToReturn += $"?width={width}";

            return urlToReturn;
        }

        //helper to create an img tag with responsive images inside
        public static string CreateImage(this HtmlHelper htmlHelper, string imageUrl, int[] imageWidthSizes, int cropHeight = 0, string cssClassName = "")
        {
            imageUrl = imageUrl.ToAbsoluteUrl();

            var listOfImagesForDataSrcSet = new List<string>();
            foreach (var imageWidthSize in imageWidthSizes)
            {
                var imageCroppedOrResized = "";
                if (cropHeight != 0)
                {
                    imageCroppedOrResized = CS.Common.Helpers.ImageProcessingHelpers.ResizeCrop(imageWidthSize, cropHeight, imageUrl);
                }
                else
                {
                    imageCroppedOrResized = CS.Common.Helpers.ImageProcessingHelpers.Resize(imageWidthSize, imageUrl, false);
                }

                listOfImagesForDataSrcSet.Add(imageCroppedOrResized + " " + imageWidthSize + "w");
            }

            var dataSrcSet = String.Join(",", listOfImagesForDataSrcSet);

            //this line includes lazyloading but we can't use lazyloasing because of the masonry layout.
            //return String.Format("<img data-sizes='auto' data-srcset='{0}' class='lazyload {1}'/>", dataSrcSet, cssClassName);

            //return String.Format("<img data-sizes='auto' data-srcset='{0}' class='lazyload {1}'/>", dataSrcSet, cssClassName);


            return String.Format("<img data-sizes='auto' srcset='{0}' class='{1}' src='{2}'/>", dataSrcSet, cssClassName,imageUrl);
        }


        public static string GetImageWithOverlay(string imageUrl, string overlayImage)
        {
            return $"{imageUrl}&overlay={overlayImage}&overlay.opacity=80";
        }

        public static string GetImageWithSpecifiedFormatAndQuality(string imageUrl, string format = "jpg", int quality = 80)
        {
            var imagUrlAsAbsolute = imageUrl.ToAbsoluteUrl();
            var uriBuilder = new UriBuilder(imagUrlAsAbsolute);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["format"] = format;
            query["quality"] = quality.ToString();
            uriBuilder.Query = query.ToString();

            var finalAbsoluteUrl = uriBuilder.ToString();

            return CS.Common.Helpers.UrlHelpers.GetRelativeUrlFromAbsoluteUrl(finalAbsoluteUrl);
        }
    }
}