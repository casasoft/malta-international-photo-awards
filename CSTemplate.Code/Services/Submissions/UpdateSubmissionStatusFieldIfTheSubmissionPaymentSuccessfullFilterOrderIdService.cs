using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.OrderItems;

namespace CSTemplate.Code.Services.Submissions
{
    public interface IUpdateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService
    {
        bool Update(int orderId);
    }
    [Service]
    public class UpdateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService : IUpdateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService
    {
        private readonly IGetOrderItemsPocoFilterByOrderIdService _getOrderItemsPocoFilterByOrderIdService;
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetSubmissionPocoDetailsFilterByIdService _getSubmissionPocoDetailsFilterByIdService;

        public UpdateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService(
            IGetSubmissionPocoDetailsFilterByIdService getSubmissionPocoDetailsFilterByIdService,
            IGetOrderItemsPocoFilterByOrderIdService getOrderItemsPocoFilterByOrderIdService,
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabase
        )
        {
            _getSubmissionPocoDetailsFilterByIdService = getSubmissionPocoDetailsFilterByIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabase;
            _getOrderItemsPocoFilterByOrderIdService = getOrderItemsPocoFilterByOrderIdService;
        }
        public bool Update(int orderId)
        {
            var result = false;
            if (orderId <= 0) return result;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var orderItems = _getOrderItemsPocoFilterByOrderIdService.Get(orderId);
            var submissionTable = DatabaseConstantValues.SubmissionsTableName;
            var submissionPrimaryKey = DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            int queryResult = 0;
            foreach (var orderItem in orderItems)
            {
                var submission = _getSubmissionPocoDetailsFilterByIdService.Get(orderItem.SubmissionId);
                if (orderItem.PayForSubmission)
                {
                    submission.StatusEnumID =(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.PaidAndWaitingforShortlisting;
                    queryResult = umbracoDb.Update(submissionTable, submissionPrimaryKey, submission);
                }
            }
            result = queryResult > 0 ? true : false;
            return result;
        }
    }


   
}