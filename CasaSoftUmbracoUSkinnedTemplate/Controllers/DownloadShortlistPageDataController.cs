﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.DownloadShortlist;
using CSTemplate.Code.Services.DownloadShortlist;
using CSTemplate.Code.Services.ShortListedPhotos;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class DownloadShortlistPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetDownloadShortlistPageService _getDownloadShortlistPageService;
        private readonly IDownloadShortListedPhotosService _downloadShortListedPhotosService;
        private readonly IUmbracoHelperService _umbracoHelperService;

        public DownloadShortlistPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetDownloadShortlistPageService getDownloadShortlistPageService,
            IDownloadShortListedPhotosService downloadShortListedPhotosService,
            IUmbracoHelperService umbracoHelperService

            )
        {
            _umbracoHelperService = umbracoHelperService;

            _downloadShortListedPhotosService = downloadShortListedPhotosService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getDownloadShortlistPageService = getDownloadShortlistPageService;

        }

        // GET: DownloadShortlistPageData
        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult DownloadShortlistPage(RenderModel model)
        {
            var _umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            DownloadShortlistPageModel downloadShortlistPageModel =
                _getDownloadShortlistPageService.Get(_umbracoHelper, model.Content);

            return base.Populate(downloadShortlistPageModel);
        }

        [HttpPost]
        public virtual FileResult DownloadShortlistPage()
        {
            var _umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var fileName = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(_umbracoHelper, Enums.DictionaryKey.Download_Short_Listed_Photos_Excel_File_Name);

            var urlBuilder = new System.UriBuilder(Request.Url.AbsoluteUri)
            {
                Path = Url.Content(ConstantValues.Submitted_Photo_Source_Address),
            };
            string absoluteUrl = urlBuilder.ToString();

            var downloadShortListExcelFile = _downloadShortListedPhotosService.Download(_umbracoHelper, absoluteUrl);
            fileName = string.Format(fileName, DateTime.Now);
            return File(downloadShortListExcelFile, "application/octet-stream", $"{fileName}");
        }
    }
}