﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CS.Common.Helpers
{
    public static class RegexHelpers
    {
        public delegate string RegexMatchDelegate(Match regexMatch);

        /// <summary>
        /// Processes a list of regular expression matches.  Each regular expression match is processed, and the returned string is used to replace the existing text.
        /// It will leave all unmatched text intact
        /// 
        /// </summary>
        /// <param name="inputStr">Input string</param>
        /// <param name="matches">List of matches</param>
        /// <param name="matchFunction">Match Process Function.  Takes a match, and returns a new string</param>
        /// <returns>The new string</returns>
        public static string ProcessRegExMatches(string inputStr, MatchCollection matches, RegexMatchDelegate matchFunction)
        {
            int lastPos = -1;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < matches.Count; i++)
            {
                var m = matches[i];
                sb.Append(inputStr.SubStr(lastPos + 1, m.Index, false));
                lastPos = m.Index + m.Length - 1;
                string s = matchFunction(m);
                sb.Append(s);

            }
            sb.Append(inputStr.Substring(lastPos + 1));
            return sb.ToString();
        }
        public static List<string> GetMatchesContent(string str, Regex r)
        {
            var matches = r.Matches(str);

            List<string> sMatches = new List<string>();
            for (int i = 0; i < matches.Count; i++)
            {
                var match = matches[i];
                
                sMatches.Add(match.Groups[match.Groups.Count-1].Value);
            }
            return sMatches;
        }
        /// <summary>
        /// Match a regex expression which has got wildcard '*' and character '?'
        /// </summary>
        /// <param name="str"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static bool MatchWithWildCardPattern(string str, string pattern, bool matchExactWords = true)
        {
            const string literalAsterixReplace = "####";
            const string literalQuestionMarkReplace = "####";
            pattern = pattern.Replace(@"\*", literalAsterixReplace); //literal *
            pattern = pattern.Replace(".", @"\."); //literal .
            pattern = pattern.Replace("\\?", literalQuestionMarkReplace); //literal ?
            //Replace all just in case
            pattern = pattern.Replace("*", ".*?"); //replace with a lazy *
            pattern = pattern.Replace("?", ".");
            pattern = pattern.Replace(literalAsterixReplace, @"\*");
            pattern = pattern.Replace(literalQuestionMarkReplace, @"\?");
            if (matchExactWords)
                pattern = "^" + pattern + "$";

            Regex r = new Regex(pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline);
            bool ok = r.IsMatch(str);
            return ok;
        }

        public static string ForRegEx(string s)
        {
            return TextHelpers.ForRegExp(s);
        }
        /// <summary>
        /// Gets the regular expression for finding 's' as a whole word in a string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetRegexForWholeWord(string s)
        {
            string punctuations = @" |\.|\:|-";
            string regex = "(^|" + punctuations + ")" + ForRegEx(s) + "($|" + punctuations + ")";
            return regex;
        }
        /// <summary>
        /// Gets the regular expression for finding 's' anywhere in a string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetRegexForWordInString(string s)
        {
            string regex = s;
            return regex;
        }
    }
}
