﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.ResetPassword;
using CSTemplate.Code.Models.Shared;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ResetPasswordPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IUmbracoHelperService _umbracoHelperService;

        public ResetPasswordPageDataController(IUmbracoControllerWrapperService
            umbracoControllerWrapperService,
            IUmbracoHelperService
            umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
        }

        // GET: ResetPasswordPageData
        public virtual ActionResult ResetPasswordPage(RenderModel model)
        {

            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var guidQueryString = Request.QueryString[ConstantValues.Memeber_Reset_Password_Guid_Alias];

            ResetPasswordPageModel resetPasswordPageModel = new ResetPasswordPageModel(model.Content);

            resetPasswordPageModel.ResetPasswordForm = new _ResetPasswordFormModel()
            {
                MemberRequestGuid = guidQueryString,
                Message = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Dictionary_Reset_Password_Page_Message),
                ButtonFormText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Dictionary_Reset_Password_Page_Button_Text),
                RequiredWordText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Dictionary_Reset_Password_Required_Text),
            };


            if (!String.IsNullOrEmpty(guidQueryString))
            {
                //query string is not empty
                resetPasswordPageModel.UserCanResetPassword = true;
            }
            else
            {
                //no query string
                resetPasswordPageModel.UserCanResetPassword = false;
                resetPasswordPageModel.ErrorMessage =
                    _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    Enums.DictionaryKey.Dictionary_Reset_Password_Request_Not_Valid);
            }

            return base.Populate(resetPasswordPageModel);
        }
    }
}