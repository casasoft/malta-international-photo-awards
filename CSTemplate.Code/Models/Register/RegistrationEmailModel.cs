﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Register
{
    public class RegistrationEmailModel
    {
        public string MemberName { get; set; }
        public string MemberSurname { get; set; }
        public string MemberEmail { get; set; }
        public string MemberCountry { get; set; }
        public string MemberNationality { get; set; }
        public string MemberPhone { get; set; }
        public string MemberAgreeToReceivePromotionalMaterials { get; set; }
    }
}
