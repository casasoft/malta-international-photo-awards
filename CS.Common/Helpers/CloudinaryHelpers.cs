﻿using System;
using System.Web;

namespace CS.Common.Helpers
{
    public static class CloudinaryHelpers
    {
        const string cloudinaryAccountName = "remaxauctions";


        public static string GetImageFromCloudinary(string imagePath)
        {
            return
                GetCloudinaryPath("", UrlHelpers.GetProtocolForCurrentPage() + GetHttpHost() +
                                  imagePath);
        }

        public static string GetImageFromCloudinary(string transformations, string imagePath)
        {
            return
                GetCloudinaryPath(transformations, UrlHelpers.GetProtocolForCurrentPage() + GetHttpHost() +
                                  imagePath);
        }

        private static string GetHttpHost()
        {
            return HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageTransformations">Any transformations that you want on the image eg: grayscale/e_face/ Each transformation should end up with a forward slash. If there are no transformations leave as an empty string</param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        private static string GetCloudinaryPath(string imageTransformations, string suffix)
        {
            return string.Format("{0}res.cloudinary.com/{1}/image/fetch/f_auto/{2}{3}", UrlHelpers.GetProtocolForCurrentPage(), cloudinaryAccountName, imageTransformations, HttpUtility.UrlEncode(suffix));
        }

        public static string GetMediaGalleryImageFromCloudinary(int cropWidth, int cropHeight, string imagePath)
        {
            return
                GetCloudinaryPath("", String.Format("w_{0},h_{1},c_fill/{2}{3}{4}",
                    cropWidth,
                    cropHeight,
                    UrlHelpers.GetProtocolForCurrentPage(),
                    GetHttpHost(),
                    imagePath));
        }

        public static string GetMediaGalleryImageFromCloudinaryWithDefaultCropSize(string imagePath)
        {
            return GetMediaGalleryImageFromCloudinary(366, 266, imagePath);
        }

        public static string GetGrayScaleImageFromCloudinary(string imagePath)
        {
            return
                GetCloudinaryPath("e_grayscale/", UrlHelpers.GetProtocolForCurrentPage() + GetHttpHost() +
                                  imagePath);
        }
    }
}