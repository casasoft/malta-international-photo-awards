﻿angular.module("umbraco").controller("OrderStatusName",
    function ($scope, $http) {

        var url = "backoffice/Orders/OrdersApi/GetStatusName/" + $scope.property.value;
        $http.get(url).then(function (response) {
            $scope.orderStatusName = response.data;

        });
    });