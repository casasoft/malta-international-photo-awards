﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Services.Test
{
    
    [Service(Priority = 100)]
    public class TestBadCalculatorService : ITestCalculatorService
    {
        public int Add(int num1, int num2)
        {
            return num1 + num2 +2;
        }
    }
}
