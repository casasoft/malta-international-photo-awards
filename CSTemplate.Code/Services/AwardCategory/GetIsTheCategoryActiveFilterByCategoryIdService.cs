﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared.AwardCategories;
using CSTemplate.Code.Models.Shared.Awards;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.AwardCategory
{

    public interface IGetIsTheCategoryActiveFilterByCategoryIdService
    {
        bool Get(UmbracoHelper umbracoHelper, int categoryId);
    }

    [Service]
    public class GetIsTheCategoryActiveFilterByCategoryIdService : IGetIsTheCategoryActiveFilterByCategoryIdService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetIsTheCategoryActiveFilterByCategoryIdService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public bool Get(UmbracoHelper umbracoHelper, int categoryId)
        {
            var isActive = false;
            if (categoryId <= 0) return isActive;
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;

            var awardEditionsNodes = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionPageData)x).ToList();

            foreach (var awardEdition in awardEditionsNodes)
            {
                var awardCategory = awardEdition.Descendants()
                    .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Category_Page_Data_Document_Type_Alias))
                    .Select(x => (AwardEditionCategoryPageData)x).FirstOrDefault(x => x.Id == categoryId);
                if (awardCategory != null)
                {
                    var activeAwardStatus = (DateTime.Now.Date >= awardEdition.StartedDate && DateTime.Now <= awardEdition.EndedDate) == true ? true : false;
                    isActive = activeAwardStatus;
                    break;
                }
            }

            return isActive;
        }
    }
}
