﻿using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Awards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.WebPages.Html;
using Umbraco.Web;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.App_Plugins.UIOMaticCustomFields.Category
{
    [PluginController("Category")]
    public class CategoryApiController : UmbracoAuthorizedJsonController
    {
        private UmbracoHelper _umbracoHelper;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetAllAwardEditionsService getAllCompletedAwardEditionListingService;
        private readonly IGetAwardEditionCategoryListFilterBySubmissionCategoryId getAwardEditionCategoryListFilterBySubmissionCategoryId;

        public CategoryApiController(IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService, IGetAllAwardEditionsService getAllCompletedAwardEditionListingService,
            IGetAwardEditionCategoryListFilterBySubmissionCategoryId getAwardEditionCategoryListFilterBySubmissionCategoryId
            )
        {
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            this.getAllCompletedAwardEditionListingService = getAllCompletedAwardEditionListingService;
            this.getAwardEditionCategoryListFilterBySubmissionCategoryId = getAwardEditionCategoryListFilterBySubmissionCategoryId;
            _umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
        }
        public List<SelectListItem> GetCategories(int id)
        {
            var result = this.getAwardEditionCategoryListFilterBySubmissionCategoryId.Get(_umbracoHelper, id);
            return result;
        }
        public List<SelectListItem> GetAllAwards()
        {
            var awardEditionList = this.getAllCompletedAwardEditionListingService.Get(_umbracoHelper);
            var selectList = new List<SelectListItem>();
            foreach (var item in awardEditionList)
            {
                selectList.Add(new SelectListItem
                {
                    Text=item.AwardItemName,
                    Value=item.AwardItemId.ToString()
                });
            }
            return selectList;

        }

        public string GetCategoryName(int id)
        {
            var category = _getAwardCategoryDetailsFilterByCategoryIdService.Get(_umbracoHelper, id);
            var categoryName = category.AwardCategoryItemName;
            return categoryName;
        }
    }
}
