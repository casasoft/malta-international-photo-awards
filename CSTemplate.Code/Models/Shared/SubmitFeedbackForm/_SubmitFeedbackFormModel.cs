﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared.SubmitFeedbackForm
{
    public class _SubmitFeedbackFormModel
    {
        //textarea
        public string SubmitFeedbackTitleText { get; set; }
        public string SubmitFeedbackSubtitleText { get; set; }
        public string SubmissionID { get; set; }

        [Required(ErrorMessage = "Feedback is required")]
        [DisplayName("Feedback")]
        public string SubmittedFeedbackFromJury { get; set; }

        public string SubmitFeedbackButtonText { get; set; }


    }
}
