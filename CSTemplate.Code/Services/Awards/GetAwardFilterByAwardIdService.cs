﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.Awards
{

    public interface IGetAwardFilterByAwardIdService
    {
        AwardEditionPageData Get(UmbracoHelper umbracoHelper,int awardId);
    }

    [Service]
    public class GetAwardFilterByAwardIdService : IGetAwardFilterByAwardIdService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetAwardFilterByAwardIdService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public AwardEditionPageData Get(UmbracoHelper umbracoHelper, int awardId)
        {
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;

            
            var awardEditionsNode = websiteConfigurationNode
                .Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionPageData)x).Where(x => x.Id == awardId).FirstOrDefault();

            var awardEditionPageData = awardEditionsNode;

            return awardEditionPageData;
        }
    }
}
