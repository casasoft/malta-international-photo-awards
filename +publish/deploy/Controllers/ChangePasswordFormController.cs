﻿using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.ChangePasswordForm;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Members;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSTemplate.Code.Models.Members.ChangePassword;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ChangePasswordFormController : SurfaceController
    {
        private readonly IHandleChangePasswordService _handleChangePasswordService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        public ChangePasswordFormController(IHandleChangePasswordService handleChangePasswordService, IUmbracoControllerWrapperService umbracoControllerWrapperService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _handleChangePasswordService = handleChangePasswordService;
        }
        [HttpPost]
        public virtual ActionResult HandleChangePasswordFormSubmit(_ChangePasswordFormModel changePasswordFormModel)
        {
            var umbracoHelper=_umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var currentMemberId = Members.GetCurrentMemberId();
            ChangePasswordModel changePasswordModel = new ChangePasswordModel
            {
                OldPassword = changePasswordFormModel.CurrentPassword,
                ChangePasswordButtonText = changePasswordFormModel.ButtonFormText,
                Password = changePasswordFormModel.InsertedNewPassword,
                ConfirmPassword = changePasswordFormModel.InsertedConfirmNewPassword
            };
            var passwordChangeResult = _handleChangePasswordService.Handle(umbracoHelper,changePasswordModel, currentMemberId,Members);
            //todo: [For: Frontend | 2018/08/29] Please Show The Message Include in The Querystring. (WrittenBy:Fatih)        			
            var url = ConstantValues.Password_Updated_Page_Link;
            var queryString = "?message=";
            var successfullMessage = queryString + "Password+Changes+Successfull";
            var failedMessage = queryString + "Password+Changes+Failed";
            var redirectToUrl = passwordChangeResult == true ? url + successfullMessage : url + failedMessage;

            return Redirect(redirectToUrl);
        }
    }
}