﻿using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Members;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Photos;
using CSTemplate.Code.Services.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Persistence;
using Umbraco.Web;
using static CSTemplate.Code.Constants.Enums;

namespace CSTemplate.Code.Services.Submissions
{
    public interface IGetSubmissionsFilterByAwardEditionIdService
    {
        List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, int awardEditionId, int awardCategoryId, SubmissionFilters shortListFilters, ShortListedStatusFilter shortListedStatusFilter);
    }
    [Service]
    public class GetSubmissionsFilterByAwardEditionIdService : IGetSubmissionsFilterByAwardEditionIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetFirstPhotoUrlFilterBySubmissionIdService _getFirstPhotoUrlFilterBySubmissionIdService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;

        public GetSubmissionsFilterByAwardEditionIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IUmbracoHelperService umbracoHelperService,
                IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService,
                IGetFirstPhotoUrlFilterBySubmissionIdService getFirstPhotoUrlFilterBySubmissionIdService,
               IGetUserFilterByUserIdService getUserFilterByUserIdService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
            _umbracoHelperService = umbracoHelperService;
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _getFirstPhotoUrlFilterBySubmissionIdService = getFirstPhotoUrlFilterBySubmissionIdService;
            _getUserFilterByUserIdService = getUserFilterByUserIdService;

        }
        public List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, int awardEditionId, int awardCategoryId , SubmissionFilters shortListFilters, ShortListedStatusFilter shortListedStatusFilter)
        {
            var listOfItemPhotoModel = new List<_ItemPhotoModel>();
            if (awardEditionId <= 0) return listOfItemPhotoModel;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var submissionsTableName = DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionTitleField = DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionShortListedEnumIdField = DatabaseConstantValues.Submissions_Short_Listed_Enum_Id_Field_Name;
            var submissionShortListedDateField = DatabaseConstantValues.Submissions_Short_Listed_Date_Field_Name;
            var submissionStatusEnumIdField = DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;
            var submissionAwardEditionId = DatabaseConstantValues.Submission_Award_Edition_Id;
            string whereCondition = string.Empty;

            if (shortListedStatusFilter==ShortListedStatusFilter.All)
            {
                whereCondition = $@"  WHERE    ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                                       ";
            }
            else
            {
                whereCondition = $@"  WHERE 
                                    (
                                        {submissionsTableAlias}.{submissionShortListedEnumIdField} = {(int)shortListedStatusFilter}
                                    )
                                        AND   ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                                       

                                        ";
            }
            if (awardEditionId>0)
            {
                whereCondition += $" AND {submissionsTableAlias}.{submissionAwardEditionId} = {awardEditionId} ";
                if (awardCategoryId > 0)
                {
                    whereCondition += $" AND {submissionsTableAlias}.{submissionsCategoryId}= {awardCategoryId}";
                }
            }
            
            var query = $@" 
                     SELECT
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId},
                        {submissionsTableAlias}.{submissionTitleField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionShortListedEnumIdField},
                        {submissionsTableAlias}.{submissionStatusEnumIdField}
                            FROM
                                {submissionsTableName}
                                    {whereCondition}
            ";

            var orderByExpression = " ORDER BY {0} {1} ";
            var orderByDesc = Constants.DatabaseConstantValues.ORDER_BY_DESC;
            var orderByAsc = Constants.DatabaseConstantValues.ORDER_BY_ASC;
            switch (shortListFilters)
            {
                case SubmissionFilters.DateNewestFirst:
                    query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionPrimaryKeyField}", orderByDesc);
                    break;
                case SubmissionFilters.DateOldestFirst:
                    query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionPrimaryKeyField}", orderByAsc);
                    break;
                case SubmissionFilters.SubmissionNameAscending:
                    query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionTitleField}", orderByAsc);
                    break;
                case SubmissionFilters.SubmissionNameDescending:
                    query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionTitleField}", orderByDesc);
                    break;

            }
            var sqlResult = umbracoDb.Query<SubmissionPoco>(query).ToList();
            foreach (var submittedPhoto in sqlResult)
            {
                var statusEnum = (CSTemplate.Code.Constants.Enums.SubmissionStatus)submittedPhoto.StatusEnumID;
                var status = EnumHelpers.GetDescriptionAttributeValue(statusEnum);
                var awardCategory = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, submittedPhoto.CategoryId);
                var photographerInfos = _getUserFilterByUserIdService.Get(umbracoHelper, submittedPhoto.UserId);

                var photographerFullName = photographerInfos.Name + " " + photographerInfos.Surname;
                listOfItemPhotoModel.Add(new _ItemPhotoModel
                {
                    ItemPhotoImgURL = _getFirstPhotoUrlFilterBySubmissionIdService.Get(submittedPhoto.Id),
                    ItemPhotoDate = submittedPhoto.Date,
                    ItemPhotoName = photographerFullName,
                    ItemPhotoAward = awardCategory.AwardItem.AwardItemName,
                    ItemPhotoLocation = submittedPhoto.Location,
                    ItemPhotoDescription = submittedPhoto.Description,
                    ItemPhotoCategory = awardCategory.AwardCategoryItemName,
                    ItemPhotoTitle = submittedPhoto.Title,
                    SubmissionId = submittedPhoto.Id.ToString(),
                    ShowShortlistRadioButtons = true,
                    SubmissionStatusTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, CSTemplate.Code.Constants.Enums.DictionaryKey.Submission_Status_Title_Text),        			
                    ListOfRadiosValues = Enum.GetValues(typeof(Enums.ShortListedStatus)).Cast<Enums.ShortListedStatus>().ToList(),
                    ChosenRadioValue = Enums.ShortListedStatus.NotYet,
                    CurrentSubmissionStatus = status.ToString()


                });

            }

            return listOfItemPhotoModel;
            

        }

       
    }
}
