﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Umbraco.Constants;
using CS.Common.Umbraco.Models.General;
using Newtonsoft.Json.Linq;
using umbraco.cms.helpers;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CS.Common.Umbraco.Helpers
{
    public static class DataTypesHelpers
    {
        public static List<LinkModel> ExtractLinksInformationFromRelatedLinksDataType
            (UmbracoHelper umbracoHelper, IPublishedContent umbracoContent, string relatedlinksPropertyAlias)
        {
            var listOfLinkModels = new List<LinkModel>();

            var relatedLinks =
                            umbracoContent.GetPropertyValue<JArray>(
                                relatedlinksPropertyAlias);

            foreach(var link in relatedLinks)
            {
                var linkModel = new LinkModel();

                linkModel.Url = (link.Value<bool>(
                    ConstantValues.Related_Links_Data_Type_Is_Internal_Alias))
                    ? umbracoHelper.NiceUrl(
                        link.Value<int>(
                            ConstantValues.Related_Links_Data_Type_Internal_Alias))
                    : link.Value<string>(
                        ConstantValues.Related_Links_Data_Type_Link_Alias);

                linkModel.Title =link.Value<string>(
                   ConstantValues.Related_Links_Data_Type_Caption_Alias);


                linkModel.LinkTarget =link.Value<bool>(
                ConstantValues.Related_Links_Data_Type_New_Window_Alias)
                ? "_blank"
                : null;

                listOfLinkModels.Add(linkModel);
            }

            return listOfLinkModels;
        }


        public static string[] ExtractTextFromTagsDataType(object tags)
        {
            if (tags != null)
            {
                return tags?.ToString().Split(',');
            }
            else
            {
                return new string[] { };
            }
        }
    }
}
