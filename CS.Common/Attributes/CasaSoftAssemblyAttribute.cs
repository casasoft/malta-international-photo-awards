﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class CasaSoftAssemblyAttribute : Attribute
    {

    }
}
