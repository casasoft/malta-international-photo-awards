﻿using CS.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CS.Common.Umbraco.Services.Umbraco.Wrappers.Services
{
    public interface IFileServiceWrapper
    {
        ITemplate GetTemplate(UmbracoContext umbracoContext, int templateId);
    }

    [Service]
    public class FileServiceWrapper : IFileServiceWrapper
    {
        public ITemplate GetTemplate(UmbracoContext umbracoContext, int templateId)
        {
            return umbracoContext.Application.Services.FileService.GetTemplate(templateId);
        }
    }
}
