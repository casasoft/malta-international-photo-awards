﻿angular.module("umbraco").controller("SubmissionCategoryName",
    function ($scope, $http) {
        var categoryId = $scope.property.value;
        var url = "backoffice/Category/CategoryApi/GetCategoryName/" + categoryId;
        $http.get(url).then(function (response) {
            $scope.person = response.data;

        });
    });