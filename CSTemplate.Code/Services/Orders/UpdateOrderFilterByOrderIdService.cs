﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Orders
{

    public interface IUpdateOrderFilterByOrderIdService
    {
        bool Update(OrderPoco order);
    }

    [Service]
    public class UpdateOrderFilterByOrderIdService : IUpdateOrderFilterByOrderIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetOrderPocoFilterByOrderIdService _getOrderPocoFilterByOrderIdService;

        public UpdateOrderFilterByOrderIdService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IGetOrderPocoFilterByOrderIdService getOrderPocoFilterByOrderIdService
            )
        {
            _getOrderPocoFilterByOrderIdService = getOrderPocoFilterByOrderIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public bool Update(OrderPoco order)
        {
            var result = false;
            if (order==null) return result;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var orderTable = Constants.DatabaseConstantValues.OrdersTableName;
            var orderTablePrimaryKey = Constants.DatabaseConstantValues.Orders_Poco_Primary_Key_Value;

            var orderDetail = umbracoDb.Update(orderTable, orderTablePrimaryKey, order);

            result = orderDetail > 0 ? true : result;

            return result;

        }
    }
}
