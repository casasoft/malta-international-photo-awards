﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CS.Common.Services;
using CS.Common.Umbraco.Constants;
using Umbraco.Core;
using Umbraco.Core.Models;

namespace CS.Common.Umbraco.Services.Authentication
{

    public interface ICommonForgotPasswordService
    {
        /// <summary>
        /// A method which will prepare the forgot password process and return you a forgot password link to be send to the user/member you requested to reset the password
        /// Before, make sure that the Member already exists
        /// </summary>
        /// <param name="memberGuidPropertyAliasName"></param>
        /// <param name="resetPasswordGuidFormatProvider">Example: "ddMMyyyyHHmmssFFFF"</param>
        /// <param name="member"></param>
        /// <param name="resetPasswordFullPageUrl"></param>
        /// <returns></returns>
        string GetResetPasswordPageLink(string memberGuidPropertyAliasName, string resetPasswordGuidFormatProvider, IMember member, string resetPasswordFullPageUrl);
    }

    [Service]
    public class CommonForgotPasswordService : ICommonForgotPasswordService
    {
        public CommonForgotPasswordService()
        {

        }

        /// <summary>
        /// A method which will prepare the forgot password process and return you a forgot password link to be send to the user/member you requested to reset the password
        /// Before, make sure that the Member already exists
        /// </summary>
        /// <param name="memberGuidPropertyAliasName"></param>
        /// <param name="resetPasswordGuidFormatProvider">Example: "ddMMyyyyHHmmssFFFF"</param>
        /// <param name="member"></param>
        /// <param name="resetPasswordFullPageUrl"></param>
        /// <returns></returns>
        public string GetResetPasswordPageLink(string memberGuidPropertyAliasName, string resetPasswordGuidFormatProvider, IMember member, string resetPasswordFullPageUrl)
        {
            var memberService = ApplicationContext.Current.Services.MemberService;

            //Set expiry date to 
            DateTime expiryTime = DateTime.Now.AddMinutes(15);

            //Lets update resetGUID property
            var resetGUID = expiryTime.ToString(resetPasswordGuidFormatProvider);
            member.Properties[memberGuidPropertyAliasName].Value = resetGUID;

            //Save the member with the updated property value
            memberService.Save(member);

            var resetPasswordPageUrl = resetPasswordFullPageUrl;
            var uriBuilder = new UriBuilder(resetPasswordPageUrl);
            var queryString = HttpUtility.ParseQueryString(uriBuilder.Query);
            queryString[memberGuidPropertyAliasName] = resetGUID;
            uriBuilder.Query = queryString.ToString();
            uriBuilder.Port = -1;
            resetPasswordPageUrl = uriBuilder.ToString();

            return resetPasswordPageUrl;
        }

    }
}
