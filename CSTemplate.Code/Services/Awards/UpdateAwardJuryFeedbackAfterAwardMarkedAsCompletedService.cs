﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Awards
{

    public interface IUpdateAwardJuryFeedbackAfterAwardMarkedAsCompletedService
    {
        void Update(UmbracoHelper umbracoHelper, int awardEditionId);
    }

    [Service]
    public class UpdateAwardJuryFeedbackAfterAwardMarkedAsCompletedService : IUpdateAwardJuryFeedbackAfterAwardMarkedAsCompletedService
    {
        private readonly IGetAwardFilterByAwardIdService _getAwardFilterByAwardIdService;

        public UpdateAwardJuryFeedbackAfterAwardMarkedAsCompletedService(IGetAwardFilterByAwardIdService awardFilterByAwardIdService
            )
        {
            _getAwardFilterByAwardIdService = awardFilterByAwardIdService;
        }
        public void Update(UmbracoHelper umbracoHelper, int awardEditionId)
        {
            var awardEdition = _getAwardFilterByAwardIdService.Get(umbracoHelper, awardEditionId);
            if (!awardEdition.FinalJuryFeedbackNotificationEmailSent)
            {
                var contentService = ApplicationContext.Current.Services.ContentService;
                var awardAsIContent = contentService.GetById(awardEdition.Id);

                awardAsIContent.SetValue(Constants.ConstantValues.Award_Edition_Page_Data_Final_Jury_Feedback_Notification_Email_Sent_Property, true);
                var result = contentService.SaveAndPublishWithStatus(awardAsIContent, 0, false);
            }
           
        }
    }
}
