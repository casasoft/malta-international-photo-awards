﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Order;
using CSTemplate.Code.Services.Submissions;
using Umbraco.Core.Logging;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Payment
{

    public interface IProcessPaymentResponseService
    {
        void Process(UmbracoHelper umbracoHeper,
            XmlDocument objParamsXml,
            int currentUserId);
    }

    [Service]
    public class ProcessPaymentResponseService : IProcessPaymentResponseService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetOrderByOrderRefIdService _getOrderByOrderRefIdService;
        private readonly IUpdateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService _updateSubmissionStatusFieldIfThePaymentSuccessfullFilterOrderIdService;
        private readonly ISendEmailUponCompletedSubmissionService _sendEmailUponCompletedSubmissionService;

        public ProcessPaymentResponseService(IGetCurrentUmbracoDatabaseService
            getCurrentUmbracoDatabaseService,
            IGetOrderByOrderRefIdService
            getOrderByOrderRefIdService,
            IUpdateSubmissionStatusFieldIfTheSubmissionPaymentSuccessfullFilterOrderIdService 
            updateSubmissionStatusFieldIfThePaymentSuccessfullFilterOrderIdService,
            ISendEmailUponCompletedSubmissionService
            sendEmailUponCompletedSubmissionService)
        {
            _sendEmailUponCompletedSubmissionService = sendEmailUponCompletedSubmissionService;
            _updateSubmissionStatusFieldIfThePaymentSuccessfullFilterOrderIdService = updateSubmissionStatusFieldIfThePaymentSuccessfullFilterOrderIdService;
            _getOrderByOrderRefIdService = getOrderByOrderRefIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public void Process(UmbracoHelper umbracoHeper,
            XmlDocument objParamsXml,
            int currentUserId)
        {
            LogHelper.Info<ProcessPaymentResponseService>("Start processing the payment process.....");

            if (objParamsXml.GetElementsByTagName(ConstantValues.PaymentResponseXMLAttributeResultName).Count > 0)
            {
                //return the result value of the transaction
                String resultValue = objParamsXml.GetElementsByTagName(ConstantValues.PaymentResponseXMLAttributeResultName)[0].InnerText;
                //check if result returned
                if (resultValue.Length > 0)
                {
                    //update donation entry in database
                    var orderRefId = objParamsXml.GetElementsByTagName(ConstantValues.PaymentResponseXMLAttributeOrderReferenceName)[0].InnerText;
                    var pspId = objParamsXml.GetElementsByTagName(ConstantValues.PaymentResponseXMLAttributePSPIDName)[0].InnerText;

                    var orderAsPoco = _getOrderByOrderRefIdService.Get(orderRefId);
                    orderAsPoco.PSPID = Convert.ToInt32(pspId);
                    orderAsPoco.Date = DateTime.Now;

                    //result returned
                    switch (resultValue.ToUpper().Trim())
                    {
                        case "OK":
                            orderAsPoco.PaymentStatusEnumId = (int)Enums.PaymentStatus.OK;

                            //you get the submission linked with this order that has been paid for photos
                            //and change it to payment completed
                            var orderId = orderAsPoco.Id;
                            _updateSubmissionStatusFieldIfThePaymentSuccessfullFilterOrderIdService.Update(orderId);

                            //HERE WE NEED TO SEND EMAILS BOTH TO ADMINISTRATORS AND TO THE CLIENT ABOUT THE NEW SUBMISSIONS
                            try
                            {
                                _sendEmailUponCompletedSubmissionService.Send(umbracoHeper,
                                    orderId,
                                    currentUserId);
                            }catch (Exception ex){}

                            break;
                        case "NOTOK":
                            orderAsPoco.PaymentStatusEnumId = (int)Enums.PaymentStatus.NOTOK;
                            break;
                        case "DECLINED":
                            orderAsPoco.PaymentStatusEnumId = (int)Enums.PaymentStatus.DECLINED;
                            break;
                        case "PENDING":
                            orderAsPoco.PaymentStatusEnumId = (int)Enums.PaymentStatus.PENDING;
                            break;
                        case "CANCEL":
                            orderAsPoco.PaymentStatusEnumId = (int)Enums.PaymentStatus.CANCEL;
                            break;
                        default:
                            //DO something: OTHER RESULT
                            break;
                    }


                    using (var db = _getCurrentUmbracoDatabaseService.Get())
                    {
                        db.Update(DatabaseConstantValues.OrdersTableName,
                          DatabaseConstantValues.Orders_Poco_Primary_Key_Value,
                          orderAsPoco,
                          orderAsPoco.Id);
                    }
                }
                else
                {
                    //ERROR: Transaction Result is empty
                    throw new Exception("Transaction Result is empty");
                }
            }
        }
    }
}
