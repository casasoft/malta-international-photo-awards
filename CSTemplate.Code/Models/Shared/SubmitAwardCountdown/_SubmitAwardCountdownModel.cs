﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared
{
    public class _SubmitAwardCountdownModel
    {
        public string SectionTitle { get; set; }
        public DateTime EndCountdown { get; set; }
        public string BackgroundImgUrl { get; set; }
        public string CountdownDaysText { get; set; }
        public string CountdownHoursText { get; set; }
        public string CountdownMinText { get; set; }
        public string DeadlineDateTime { get; set; }
    }
}
