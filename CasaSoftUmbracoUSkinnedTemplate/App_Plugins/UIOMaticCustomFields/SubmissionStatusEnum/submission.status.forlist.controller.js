﻿angular.module("umbraco").controller("SubmissionStatus",
    function ($scope, $http) {
        var url = "backoffice/SubmissionStatus/SubmissionStatusApi/GetStatus/" + $scope.property.value;
        $http.get(url).then(function (response) {
            $scope.persons = response.data;
           
        });
    });