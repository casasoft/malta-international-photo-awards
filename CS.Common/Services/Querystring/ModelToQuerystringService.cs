﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using System.Linq.Expressions;
using CS.Common.Helpers;
using System.Collections.Specialized;
using System.Web;
using System.Collections;
using System.Web.Mvc;

namespace CS.Common.Services.Querystring
{
    public interface IModelToQuerystringService
    {
        string GetQuerystringFromModelPropertiesAndValues<T>(T model,
            List<Expression<Func<T, object>>> OmitProperties = null);
    }

    [Service]
    public class ModelToQuerystringService : IModelToQuerystringService
    {
        public const string Delimiter = ",";


        public string GetQuerystringFromModelPropertiesAndValues<T>(T model,
            List<Expression<Func<T, object>>> OmitProperties = null)
        {
            var allProperties = ReflectionHelpers.GetAllPropertiesAndFieldsForObjct(model, false, false, false, OmitProperties);
            
            NameValueCollection nvc = new NameValueCollection();
            foreach (var property in allProperties)
            {
                var valueForMemberInfo = ReflectionHelpers.GetValueForMemberInfo(model, property);
                string valueStr = null;
                if (valueForMemberInfo != null)
                {
                    if (TypeHelpers.IsTypeEnumerableList(valueForMemberInfo.GetType()))
                    {
                        //list

                        valueStr = ListHelpers.ConvertListToString((IEnumerable)valueForMemberInfo, null, Delimiter);
                    }
                    else
                    {
                        valueStr = (valueForMemberInfo.ToString());
                    }
                }
                if (valueStr != null)
                {
                    nvc[property.Name] = valueStr;
                }
            }
            return UrlHelpers.GetNameValueCollectionAsQuerystring(nvc);
        }

    }
}
