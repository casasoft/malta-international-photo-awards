﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.OrderHistory;
using CSTemplate.Code.Models.Shared.OrderHistoryItem;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Orders;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.OrderHistory
{

    public interface IGetOrderHistoryPageService
    {
        OrderHistoryPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content,int userId,int pageNumber=1);
    }

    [Service]
    public class GetOrderHistoryPageService : IGetOrderHistoryPageService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetOrderHistoryFilteredByUserIdService _getOrderHistoryFilteredByUserIdService;
        private readonly IGetPagerModelService _getPagerModelService;

        public GetOrderHistoryPageService(
            IUmbracoHelperService umbracoHelperService,
            IGetOrderHistoryFilteredByUserIdService getOrderHistoryFilteredByUserIdService,
            IGetPagerModelService getPagerModelService
        )
        {
            _getPagerModelService = getPagerModelService;
            _getOrderHistoryFilteredByUserIdService = getOrderHistoryFilteredByUserIdService;
            _umbracoHelperService = umbracoHelperService;
        }

        public OrderHistoryPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content, int userId,int pageNumber=1)
        {
            
            var dateTableText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_History_Table_Date_Text);
            var orderNumberTableText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_History_Table_OrderNumber_Text);
            var statusTableText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_History_Table_Status_Text);
            var descriptionTableText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_History_Table_Description_Text);
            var totalTableText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_History_Table_Total_Text);
            var orderLinkUrl = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Order_History_Item_Link_Url);
            DateTime currentTime = DateTime.Now;
            var getOrderList= _getOrderHistoryFilteredByUserIdService.Get(umbracoHelper, userId);


            //pagination
            var itemsPerPage = CSTemplate.Code.Constants.ConstantValues.Order_Item_List_Count;
            var itemsTotalAmount = getOrderList.Count;
            var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper, itemsPerPage, itemsTotalAmount);
            


            OrderHistoryPageModel orderHistoryPageModel = new OrderHistoryPageModel(content)
            {
                TableDateText = dateTableText,
                TableOrdeNumberText = orderNumberTableText,
                TableStatusText = statusTableText,
                TableDescriptionText = descriptionTableText,
                TableTotalText = totalTableText,
                //ListOfOrders = ,
                
                ////pagination
                //Pagination = pagerModel,

                #region Dummy Data

        //ListOfOrders = new List<_OrderHistoryItem>()
        //{
        //    new _OrderHistoryItem()
        //    {
        //        OrderDate = currentTime,
        //        OrderNumber = "23423",
        //        OrderStatus = "Complete",
        //        OrderDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        //        OrderTotal = 90,
        //        OrderLinkUrl = orderLinkUrl,

        //    },
        //    new _OrderHistoryItem()
        //    {
        //        OrderDate = currentTime,
        //        OrderNumber = "12345",
        //        OrderStatus = "Complete",
        //        OrderDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        //        OrderTotal = 20,
        //        OrderLinkUrl = orderLinkUrl,

        //    }
        //}

        #endregion

            };

            int itemTotalAmounts = getOrderList.Count;
            //how many item to show on the page
            orderHistoryPageModel.ItemsPerPage = CSTemplate.Code.Constants.ConstantValues.Mansory_Item_Count;
            var pageSize = orderHistoryPageModel.ItemsPerPage;
            //total number of listing items.
            orderHistoryPageModel.ItemsTotalAmount = itemTotalAmounts;

            getOrderList = getOrderList.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            
            orderHistoryPageModel.ListOfOrders = getOrderList;
            orderHistoryPageModel.Pagination = pagerModel;


            return orderHistoryPageModel;
        }
    }
}
