﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;

namespace CS.Common.Services.Application
{
    public interface IApplicationService
    {
        string GetApplicationRootLocalFolder();
    }

    [Service]
    public class ApplicationService : IApplicationService
    {
        public string GetApplicationRootLocalFolder()
        {
            return System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            
        }

    }
}
