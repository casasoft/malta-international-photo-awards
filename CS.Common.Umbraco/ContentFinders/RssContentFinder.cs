﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using CS.Common.Umbraco.Constants;
using Umbraco.Core;
using Umbraco.Core.Cache;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace CS.Common.Umbraco.ContentFinders
{
    public class RssContentFinder : IContentFinder
    {
        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            if (contentRequest == null)
            {
                return false;
            }

            try
            {
                using (
                    ApplicationContext.Current.ProfilingLogger.TraceDuration<MultilingualContentFinder>(
                        "Started TryFindContent",
                        "Completed TryFindContent"))
                {
                    var helper = new UmbracoHelper(UmbracoContext.Current);

                    if (UmbracoContext.Current != null)
                    {
                        IDomain currentDomain = null;

                        if (contentRequest.UmbracoDomain != null)
                        {
                            currentDomain = contentRequest.UmbracoDomain;
                        }

                        // Get the current url without querystring.
                        //var url = this.RemoveQueryFromUrl(contentRequest.Uri.ToString()).EnsureEndsWith("/");
                        var url = contentRequest.Uri.GetAbsolutePathDecoded().EnsureEndsWith("/");

                        var parts = url.Split(new[] { '/' }, System.StringSplitOptions.RemoveEmptyEntries);
                        var lastPartOfUrl = parts[parts.Length - 1];

                        //We need to check if the last part matches the RSS values /{Path}/rss/ or /{Path}/rss-social-media/
                        if (lastPartOfUrl == ConstantValues.RssTitle || lastPartOfUrl == ConstantValues.RssSocialMediaTitle) // if in root redirect to default language 
                        {
                            //The content item will be the RSS Page in the Website Configuration, so we can match it with the Document Type Controller.
                            var contentItem = UmbracoContext.Current.ContentCache.GetById(ConstantValues.RssPageId);

                            if (contentItem != null)
                            {
                                contentRequest.PublishedContent = contentItem;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error<MultilingualContentFinder>("Error in contenfinder RssContentFinder", ex);
            }

            return contentRequest.PublishedContent != null;
        }
    }
}
