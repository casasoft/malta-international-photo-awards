﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.My_Submissions;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Photos;
using CSTemplate.Code.Services.Users;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Web;
using dbConstants = CSTemplate.Code.Constants.DatabaseConstantValues;
namespace CSTemplate.Code.Services.Submissions
{

    public interface IGetMySubmissionsService
    {
        MySubmissionsListingPageModel Get(UmbracoHelper umbracoHelper,IPublishedContent publishedContent,int userId,int pageNumber=1);
    }

    [Service]
    public class GetMySubmissionsService : IGetMySubmissionsService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetFirstPhotoUrlFilterBySubmissionIdService _getFirstPhotoUrlFilterBySubmissionIdService;
        private readonly IGetPagerModelService _getPagerModelService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;

        public GetMySubmissionsService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService, 
            IGetFirstPhotoUrlFilterBySubmissionIdService getFirstPhotoUrlFilterBySubmissionIdService,
            IGetPagerModelService getPagerModelService,
            IUmbracoHelperService umbracoHelperService,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService,
            IGetUserFilterByUserIdService getUserFilterByUserIdService
            )
        {
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _umbracoHelperService = umbracoHelperService;
            _getPagerModelService = getPagerModelService;
            _getFirstPhotoUrlFilterBySubmissionIdService = getFirstPhotoUrlFilterBySubmissionIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public MySubmissionsListingPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent publishedContent, int userId, int pageNumber = 1)
        {
            var mySubmissionsListingPageModel = new MySubmissionsListingPageModel(publishedContent);
            if (userId <= 0) return mySubmissionsListingPageModel;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();


           

            var submissionsTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = Constants.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = Constants.DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = Constants.DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = Constants.DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = dbConstants.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = dbConstants.Submissions_Category_Id_Field_Name;
            var submissionTitleField = dbConstants.Submissions_Title_Field_Name;
            var submissionStatusEnumIdField = dbConstants.Submissions_Status_Enum_Id_Field_Name;
            

            var featuredFieldId = Constants.DatabaseConstantValues.Photos_Featured_Field_Name;

            var orderByDescValue = Constants.DatabaseConstantValues.ORDER_BY_DESC;

            var sqlQuery = new Sql($@" 
                     SELECT
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId},
                        {submissionsTableAlias}.{submissionTitleField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionStatusEnumIdField}
                            FROM
                                {submissionsTableName}
                                     WHERE {submissionsTableAlias}.{submissionsUserId} = {userId} AND ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK})
                                         ORDER BY {submissionPrimaryKeyField} {orderByDescValue}
            ");

            var mySubmittedPhotosSqlResult = umbracoDb.Query<SubmissionPoco>(sqlQuery).ToList();

            var listOfItemPhotoModel = new List<_ItemPhotoModel>();     			
            foreach (var submittedPhoto in mySubmittedPhotosSqlResult)
            {
                var awardCategory = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, submittedPhoto.CategoryId);
                var statusEnum = (CSTemplate.Code.Constants.Enums.SubmissionStatus)submittedPhoto.StatusEnumID;
                var status = EnumHelpers.GetDescriptionAttributeValue(statusEnum);
                var photographerInfos = _getUserFilterByUserIdService.Get(umbracoHelper, submittedPhoto.UserId);
                var photographerFullName = photographerInfos.Name + " " + photographerInfos.Surname;
                listOfItemPhotoModel.Add(new _ItemPhotoModel
                {
                    ItemPhotoImgURL = _getFirstPhotoUrlFilterBySubmissionIdService.Get(submittedPhoto.Id),
                    ItemPhotoDate = submittedPhoto.Date,
                    ItemPhotoName = photographerFullName,
                    ItemPhotoAward = awardCategory.AwardItem.AwardItemName,
                    ItemPhotoLocation = submittedPhoto.Location,
                    ItemPhotoDescription = submittedPhoto.Description,
                    ItemPhotoCategory = awardCategory.AwardCategoryItemName,
                    ItemPhotoTitle = submittedPhoto.Title,
                    SubmissionId = submittedPhoto.Id.ToString(),
                    CurrentSubmissionStatus = status.ToString(),
                    SubmissionStatusTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,CSTemplate.Code.Constants.Enums.DictionaryKey.Submission_Status_Title_Text)
                   

                });
            }

            int itemTotalAmounts = mySubmittedPhotosSqlResult.Count;
            //how many item to show on the page
            mySubmissionsListingPageModel.ItemsPerPage = CSTemplate.Code.Constants.ConstantValues.Mansory_Item_Count;
            var pageSize = mySubmissionsListingPageModel.ItemsPerPage;
            //total number of listing items.
            mySubmissionsListingPageModel.ItemsTotalAmount = itemTotalAmounts;

            listOfItemPhotoModel = listOfItemPhotoModel.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper, mySubmissionsListingPageModel.ItemsPerPage, mySubmissionsListingPageModel.ItemsTotalAmount);
            mySubmissionsListingPageModel.ListingItemsAsMasonryPartial = new _ListingItemsAsMasonryPartialModel()
            {
                Pagination = pagerModel,
                ListOfPhotos = listOfItemPhotoModel
            };
            return mySubmissionsListingPageModel;
        }
    }
}
