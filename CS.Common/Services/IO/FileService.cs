﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using System.IO;

namespace CS.Common.Services.IO
{
    public interface IFileService
    {
        FileStream FileOpen(string path, FileMode fileMode, FileAccess fileAccess);

        /// <summary>
        /// Saves to file. Creates a new one
        /// </summary>
        /// <param name="path"></param>
        /// <param name="txt"></param>
        void SaveToFile(string path, string txt);

        void SaveToFile(string path, byte[] file);
    }

    [Service]
    public class FileService : IFileService
    {
        public FileStream FileOpen(string path, FileMode fileMode, FileAccess fileAccess)
        {
            return File.Open(path, fileMode, fileAccess);
        }
        /// <summary>
        /// Saves to file. Creates a new one
        /// </summary>
        /// <param name="path"></param>
        /// <param name="txt"></param>
        public void SaveToFile(string path, string txt)
        {
            path = path.Replace("/", "\\");


            string dir = path.Substring(0, path.LastIndexOf('\\') + 1);
            if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir))
            {

                Directory.CreateDirectory(dir);
            }

            File.WriteAllText(path, txt);
            //FileStream fs = new FileStream(path, FileMode.Create);
            //StreamWriter sw = new StreamWriter(fs);
            //sw.Write(txt);
            //sw.Close();
            //fs.Close();

        }
        public void SaveToFile(string path, byte[] file)
        {
            string dir = path.Substring(0, path.LastIndexOf('\\') + 1);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            File.WriteAllBytes(path, file);
            //FileStream fs = new FileStream(path, FileMode.Create);
            //BinaryWriter bw = new BinaryWriter(fs);
            //bw.Write(file);
            //bw.Close();
            //fs.Close();

        }
    }
}
