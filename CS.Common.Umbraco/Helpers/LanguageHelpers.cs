﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Constants;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CS.Common.Umbraco.Helpers
{
    public static class LanguageHelpers
    {
        private const string COOKIE_USER_LANG_PREFERENCE = ConstantValues.COOKIE_USER_LANG_PREFERENCE;
        private const string COOKIE_USER_LANG_PREFERENCE_SUBKEY = ConstantValues.COOKIE_USER_LANG_PREFERENCE_SUBKEY;
        private const string QUERYSTRING_CHANGE_LANGUAGE = ConstantValues.Query_String_Change_Lang_Name;

        public static IEnumerable<ILanguage> GetAllWebsiteLanguages(ApplicationContext applicationContext)
        {
            return (IEnumerable<ILanguage>)applicationContext.ApplicationCache
                .RuntimeCache.GetCacheItem(
                     ConstantValues.Cache_Key_Get_All_Website_Languages,
               () =>
               {
                   return applicationContext.Services.LocalizationService
                       .GetAllLanguages();
               }, new TimeSpan(730, 0, 0));
        }


        public static bool IsLanguageAvailableInUmbraco(IEnumerable<ILanguage> allLanguages, string lang)
        {
            var websiteLanguages = allLanguages;

            if (websiteLanguages != null)
            {
                foreach (var language in websiteLanguages)
                {
                    if (language.CultureInfo.TwoLetterISOLanguageName.Equals(lang))
                    {
                        return true;
                    }
                }
            }

            return false;
        }


        public static string GetUserComputerLanguageIfAvailableInUmbraco(IEnumerable<ILanguage> allLanguages,
            ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            if (request.UserLanguages != null && request.UserLanguages.Length > 0)
            {
                string userLanguage = request.UserLanguages[0];
                //just in case it could be en_US
                if (userLanguage != null)
                {
                    userLanguage = userLanguage.Replace("_", "-");
                    if (userLanguage.Contains("-"))
                    {
                        //Get first 2 letter code
                        userLanguage = userLanguage.Substring(0, userLanguage.IndexOf("-", StringComparison.Ordinal));
                    }
                    if (IsLanguageAvailableInUmbraco(allLanguages, userLanguage))
                    {
                        return userLanguage; 
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        public static string GetCurrentPageLanguage(ActionExecutingContext filtercContext)
        {
            //Get from current culture info
            var currentCulture = CultureInfo.CurrentCulture;
            return currentCulture.TwoLetterISOLanguageName;
        }

        public static string GetUserPreferredLanguageFromCookie(ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            if (request.Cookies != null)
            {
                var cookie = request.Cookies[COOKIE_USER_LANG_PREFERENCE];
                if (cookie != null)
                {
                    return cookie[COOKIE_USER_LANG_PREFERENCE_SUBKEY];
                }
            }
            return null;
        }


        public static void RedirectUserToPreferredLanguageForCurrentPage(string userLanguage,
            ActionExecutingContext filterContext, UmbracoContext currentUmbracoContext)
        {
            var umbracoContext = currentUmbracoContext;
            if (umbracoContext != null)
            {
                var curretNode = umbracoContext.PublishedContentRequest.PublishedContent;

                var allUrls = new List<string>();
                var currentUrl = curretNode.Url;
                var otherUrls = umbracoContext.UrlProvider.GetOtherUrls(curretNode.Id);
                allUrls.Add(currentUrl);
                allUrls.AddRange(otherUrls);

                var redirectUrl = allUrls.FirstOrDefault(x => x.StartsWith("/" + userLanguage + "/"));

                //get query strings
                var queryStrings = filterContext.HttpContext.Request.Url.Query;

                redirectUrl += queryStrings;

                //Get alternative URL of this page from Umbraco and redirect to it
                if (!String.IsNullOrWhiteSpace(redirectUrl))
                {
                    filterContext.Result = new RedirectResult(redirectUrl);
                }
            }
        }

        public static void CheckUserPreferredLanguageAndRedirect(ActionExecutingContext filterContext, ApplicationContext applicationContext,
            UmbracoContext currentUmbracoContext)
        {
            var userPreferredLanguage = GetUserPreferredLanguageFromCookie(filterContext);
            if (userPreferredLanguage == null)
            {
                var userLanguage = GetUserComputerLanguageIfAvailableInUmbraco(
                    GetAllWebsiteLanguages(applicationContext),
                    filterContext);
                //First time so no preferred language for now
                if (userLanguage != null)
                {
                    //store it if he has a preference
                    UpdateUserLanguagePreferenceInCookie(userLanguage, filterContext);

                    if (userLanguage != ConstantValues.LanguageDefaultName)
                    {
                        RedirectUserToPreferredLanguageForCurrentPage(userLanguage,
                            filterContext, currentUmbracoContext);
                    }
                }
            }
            else
            {
                var currPageLanguage = GetCurrentPageLanguage(filterContext);

                //There is a preferred language set by user
                if (string.Compare(userPreferredLanguage, currPageLanguage, StringComparison.InvariantCulture) != 0)
                {
                    //It is not the same so redirect
                    RedirectUserToPreferredLanguageForCurrentPage(userPreferredLanguage, filterContext,
                        currentUmbracoContext);
                }
            }
        }

        public static void UpdateUserLanguagePreferenceInCookie(string lang, ActionExecutingContext filterContext)
        {
            var cookie = new HttpCookie(COOKIE_USER_LANG_PREFERENCE);
            cookie[COOKIE_USER_LANG_PREFERENCE_SUBKEY] = lang;

            filterContext.HttpContext.Response.Cookies.Add(cookie);
        }


        public static bool CheckIfUserHasChangedPreferredLanguageAndUpdateCookie
            (ActionExecutingContext filterContext)
        {
            string userPreferredLanguage = filterContext.HttpContext.Request
                .QueryString[QUERYSTRING_CHANGE_LANGUAGE];
            if (!string.IsNullOrEmpty(userPreferredLanguage))
            {
                UpdateUserLanguagePreferenceInCookie(userPreferredLanguage, filterContext);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
