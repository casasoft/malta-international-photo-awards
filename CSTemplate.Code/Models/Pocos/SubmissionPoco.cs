﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIOMatic.Attributes;
using UIOMatic.Enums;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace CSTemplate.Code.Models.Pocos
{
    [TableName("pocoSubmissions")]
    [PrimaryKey("Id", autoIncrement = true)]
    [ExplicitColumns]
    [UIOMatic("pocoSubmissions", "Submissions", "Submission", FolderIcon = "icon-flag", ItemIcon = "icon-flag", ShowOnSummaryDashboard = true, RenderType = UIOMaticRenderType.List)]
    public class SubmissionPoco
    {
        [Column("Id")]
        [PrimaryKeyColumn(Name = "Id", AutoIncrement = true)]
        [UIOMaticListViewField(Name = "Id")]
        public int Id { get; set; }


        [UIOMaticField(Name = "User", View = "~/App_Plugins/UIOMaticCustomFields/User/user.name.html")]
        [UIOMaticListViewField(Name = "User", View = "~/App_Plugins/UIOMaticCustomFields/User/user.fullname.html")]
        [Column("UserId")]
        public int UserId { get; set; }

        [Required]
        [UIOMaticField(Name = "Title", Description = "Please enter Title", View = UIOMatic.Constants.FieldEditors.Textfield)]
        [UIOMaticListViewField(Name = "Title")]
        [Column("Title")]
        public string Title { get; set; }

        [Required]
        [UIOMaticField(Name = "Description", Description = "Please enter Description", View = UIOMatic.Constants.FieldEditors.Textfield)]
        [UIOMaticListViewField(Name = "Description")]
        [Column("Description")]
        public string Description { get; set; }

        [UIOMaticField(Name = "Submission Status", Description = "Submission Status", View = "~/App_Plugins/UIOMaticCustomFields/SubmissionStatusEnum/submission.status.html")]
        [UIOMaticListViewField(Name = "Submission Status", View = "~/App_Plugins/UIOMaticCustomFields/SubmissionStatusEnum/submission.status.forlist.html")]
        [Column("StatusEnumID")]
        public int StatusEnumID { get; set; }

        [Required]
        [UIOMaticField(Name = "Award Edition", Description = "Please select the AwardEdition", View = "~/App_Plugins/UIOMaticCustomFields/AwardEdition/awardedition.list.html")]
        [UIOMaticListViewField(Name = "Award Edition", View = "~/App_Plugins/UIOMaticCustomFields/AwardEdition/awardedition.name.html")]
        [Column("AwardEditionId")]
        public int AwardEditionId { get; set; }

        [UIOMaticField(Name = "Submission Category", Description = "Submission Category", View = "~/App_Plugins/UIOMaticCustomFields/GalleryCategory/gallery.category.html")]
        [UIOMaticListViewField(Name = "Submission Category", View = "~/App_Plugins/UIOMaticCustomFields/Category/category.name.html")]
        [Column("CategoryId")]
        public int CategoryId { get; set; }

        [UIOMaticField(Name = "Submission Gallery Category", Description = "Submission Gallery Category", View = "~/App_Plugins/UIOMaticCustomFields/GalleryCategory/gallery.category.html")]
        [UIOMaticListViewField(Name = "Submission Gallery Category", View = "~/App_Plugins/UIOMaticCustomFields/GalleryCategory/gallery.category.name.html")]
        [Column("GalleryCategoryId")]
        public int GalleryCategoryId { get; set; }

        [UIOMaticField(Name = "Short Listed Status", Description = "Short Listed Status", View = "~/App_Plugins/UIOMaticCustomFields/ShortListed/shortlist.statuslist.html")]
        [UIOMaticListViewField(Name = "Short Listed Status", View = "~/App_Plugins/UIOMaticCustomFields/ShortListed/shortlisted.status.name.html")]
        [Column("ShortListedEnumId")]
        public int ShortListedEnumId { get; set; }

        [Required]
        [UIOMaticField(Name = "ShortListedDate", Description = "ShortListedDate", View = UIOMatic.Constants.FieldEditors.DateTime)]
        [UIOMaticListViewField(Name = "ShortListedDate")]
        [Column("ShortListedDate")]
        public DateTime ShortListedDate { get; set; }

        [Required]
        [UIOMaticField(Name = "Date", Description = "Date", View = UIOMatic.Constants.FieldEditors.DateTime)]
        [UIOMaticListViewField(Name = "Date")]
        [Column("Date")]
        public DateTime Date { get; set; }

        //[Required]
        [UIOMaticField(Name = "Feedback Description", Description = "Feedback Description", View = UIOMatic.Constants.FieldEditors.Textfield)]
        [UIOMaticListViewField(Name = "Feedback Description")]
        [Column("FeedbackDescription")]
        public string FeedbackDescription { get; set; }

        //[Required]
        //[UIOMaticField(Name = "Location", Description = "Please enter Location", View = UIOMatic.Constants.FieldEditors.Textfield)]
        //[UIOMaticListViewField(Name = "Location")]
        [Column("Location")]
        public string Location { get; set; }

        [Required]
        [UIOMaticField(Name = "Feedback Request Status: ", Description = "It shows the submission requested a feedback.", View = UIOMatic.Constants.FieldEditors.CheckBox)]
        [UIOMaticListViewField(Name = "Feedback Request Status:")]
        [Column("FeedbackRequest")]
        public bool FeedbackRequest { get; set; }

        [Required]
        [UIOMaticField(Name = "Is a Story Telling?", Description = "Please select the IsAStoryTelling", View = UIOMatic.Constants.FieldEditors.CheckBox)]
        [UIOMaticListViewField(Name = "Is a Story Telling?")]
        [Column("IsAStoryTelling")]
        public bool IsAStoryTelling { get; set; }

        [Required]
        [UIOMaticField(Name = "Is Feedback Replied Mail Send?", Description = "It shows the feedback request was replied by Jury and sent inform the submission owner.", View = UIOMatic.Constants.FieldEditors.CheckBox)]
        [UIOMaticListViewField(Name = "Is Feedback Replied Mail Send?")]
        [Column("FeedbackRepliedMailSent")]
        public bool FeedbackRepliedMailSent { get; set; }

        
    }
}