﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.WebsiteConfiguration
{
    public class PricesModel
    {
        public decimal NormalPhotoPrice { get; set; }
        public decimal StoryTellingPhotoPrice { get; set; }
        public decimal FeedbackPrice { get; set; }
        public decimal StorytellingFeedbackPrice { get; set; }
    }
}
