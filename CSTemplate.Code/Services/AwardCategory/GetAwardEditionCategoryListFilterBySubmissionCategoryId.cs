﻿using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebPages.Html;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.AwardCategory
{
    public interface IGetAwardEditionCategoryListFilterBySubmissionCategoryId
    {
        List<SelectListItem> Get(UmbracoHelper umbracoHelper, int categoryId);
    }
    [Service]
    public class GetAwardEditionCategoryListFilterBySubmissionCategoryId : IGetAwardEditionCategoryListFilterBySubmissionCategoryId
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;

        public GetAwardEditionCategoryListFilterBySubmissionCategoryId(IUmbracoHelperService umbracoHelperService, IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService)
        {
            _umbracoHelperService = umbracoHelperService;
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
        }
        public List<SelectListItem> Get(UmbracoHelper umbracoHelper, int categoryId)
        {
            List<SelectListItem> returnofAwardCategoryList = new List<SelectListItem>();
            if (categoryId <= 0) return returnofAwardCategoryList;
            var categoryDetails = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, categoryId);
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;


            var awardEditionsNode = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias) && x.Id==categoryDetails.AwardItem.AwardItemId)
                .Select(x => (AwardEditionPageData)x).FirstOrDefault();

            if (awardEditionsNode != null)
            {
                var awardCategories = awardEditionsNode.Descendants()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Category_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionCategoryPageData)x).ToList();
                if (awardCategories.Count > 0)
                {
                    foreach (var awardCategoryItem in awardCategories)
                    {
                        returnofAwardCategoryList.Add(new SelectListItem()
                        {
                            Text = awardCategoryItem.Name,
                            Value = awardCategoryItem.Id.ToString()
                        });
                    }
                }

            }


            return returnofAwardCategoryList;

        }
    }
}
