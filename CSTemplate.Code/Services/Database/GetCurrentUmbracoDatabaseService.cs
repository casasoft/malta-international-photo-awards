﻿using CS.Common.Services;
using Umbraco.Core;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Database
{

    public interface IGetCurrentUmbracoDatabaseService
    {
        UmbracoDatabase Get();
    }

    [Service]
    public class GetCurrentUmbracoDatabaseService : IGetCurrentUmbracoDatabaseService
    {
        public GetCurrentUmbracoDatabaseService()
        { }

        public UmbracoDatabase Get()
        {
            return ApplicationContext.Current.DatabaseContext.Database;
        }
    }
}
