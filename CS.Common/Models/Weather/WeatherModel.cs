﻿
using WeatherNet.Model;

namespace CS.Common.Models.Weather
{
    public class WeatherModel
    {
        public string IconUrl { get; set; }
        public string Alt { get; set; }
        public CurrentWeatherResult CurrentWeatherResult { get; set; }
        public string Temperature { get; set; }
    }
}
