﻿using CSTemplate.Code.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebPages.Html;
//using Compare = System.ComponentModel.DataAnnotations.CompareAttribute;

namespace CSTemplate.Code.Models.Shared
{
    public class _RegisterFormModel
    {
        public string TitleForm { get; set; }
        public string YourDetailsSectionTitle { get; set; }
        public string AccountDetailsSectionTitle { get; set; }
        public string RegisterRequiredField { get; set; }

        //todo: [For: backend | 2018/08/17] This bool indicates if the user is currently on registration page. Depending on that value the form looks slightly different. (WrittenBy: Radek)        			
        public bool IsRegistrationProcess { get; set; }

        [Required(ErrorMessage = "The 'Name' field is required")]
        [DisplayName("Name")]
        [RegularExpression("^[a-zA-Z -]*",ErrorMessage = "The 'Name' field accepts just alphabetic characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The 'Surname' field is required")]
        [DisplayName("Surname")]
        [RegularExpression("^[a-zA-Z -]*", ErrorMessage = "The 'Surname' field accepts just alphabetic characters")]
        public string Surname { get; set; }


        [Required(ErrorMessage = "Email is required")]
        [EmailAddress]
        [DisplayName("Email Address")]
        public string InsertedEmail { get; set; }


        //[Required(ErrorMessage = "Phone Number is required")]
        //[DisplayName("Contact Number")]
        //public string InsertedPhone { get; set; }


        [Required(ErrorMessage = "Country is required")]
        [DisplayName("Country of Residence")]
        public CS.Common.Constants.Enums.CountryIso3166? InsertedCountry { get; set; }

     
        [Required(ErrorMessage = "The 'Nationality' field is required")]
        [DisplayName("Nationality")]
        [RegularExpression("^[a-zA-Z -]*", ErrorMessage = "The 'Nationality' field accepts just alphabetic characters")]
        public string Nationality { get; set; }

  
        [Required(ErrorMessage = "This field is required")]
        [DisplayName("Where did you hear about us?")]
        public CS.Common.Constants.Enums.WhereDidYouHearAboutUsList? WhereDidYouHearAboutUsInserted { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DisplayName("Where did you hear about us? Please enter your answer")]
        
        public string WhereDidYouHearAboutUsCustomText { get; set; }

        //[RequiredIf("isUpdate", false, ErrorMessage = "Password is required")]
        [DisplayName("Password")]
        [Required(ErrorMessage = "Password is required")]
        [StringLength(100, ErrorMessage = "The password must be at least 8 characters long.", MinimumLength = 8)]
        
        public string InsertedPassword { get; set; }

        //public string ConfirmPasswordPlaceholder { get; set; }

        //[RequiredIf("isUpdate", false, ErrorMessage = "Confirm Password is required")]
        //[Required(ErrorMessage = "Passwords do not match")]
        [DisplayName("Confirm Password")]
        [Compare("InsertedPassword", ErrorMessage = "The password and confirmation password do not match.")]
        
        public string InsertedConfirmPassword { get; set; }
        
        public string TermsAndConditionsText { get; set; }
        public string True
        {
            get
            {
                return "true";
            }
        }

        [Required]
        [Compare("True", ErrorMessage = "The 'Terms and Conditions' field is required")]
        public bool TermsConditions { get; set; }


        public bool PromotionalMaterials { get; set; }
        public string PromotionalMaterialsDescriptionParagraph { get; set; }


        public string ButtonFormText { get; set; }

        //[RequiredIf("isUpdate", false, ErrorMessage = "Password is required")]
        //[DisplayName("Password")]
        //[StringLength(100, ErrorMessage = "The password must be at least 8 characters long.", MinimumLength = 8)]
        //[RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&-+)(])[A-Za-z\d$@$!%*?&-+)(]{8,}", ErrorMessage = "Password needs to have at least 8 characters long, one uppercase letter, one lowercase letter, one number and one special character.")]
        //public string InsertedPassword { get; set; }

        ////public string ConfirmPasswordPlaceholder { get; set; }

        //[RequiredIf("isUpdate", false, ErrorMessage = "Confirm Password is required")]
        ////[Required(ErrorMessage = "Passwords do not match")]
        //[DisplayName("Confirm Password")]
        //[Compare("InsertedPassword", ErrorMessage = "The password and confirmation password do not match.")]
        //public string InsertedConfirmPassword { get; set; }


        //[Required(ErrorMessage = "Country is required")]
        //[DisplayName("Country of Residence")]
        //public CS.Common.Constants.Enums.CountryIso3166? ListOfCountries { get; set; }

        //public string PickedCountry { get; set; }

        //[RequiredIf("isUpdate", false, ErrorMessage = "Password is required")]
        //[StringLength(100, ErrorMessage = "The password must be at least 8 characters long.", MinimumLength = 8)]
        //public string InsertedPassword { get; set; }

        //[RequiredIf("isUpdate", false, ErrorMessage = "Confirm Password is required")]
        ////[Required(ErrorMessage = "Passwords do not match")]
        //[Compare("InsertedPassword", ErrorMessage = "The password and confirmation password do not match.")]
        //public string InsertedConfirmPassword { get; set; }

        //[Range(typeof(bool), "true", "true", ErrorMessage = "The 'Terms and Conditions' field is required")]

        //public string PersonalInformationTitle { get; set; }

        //public string NamePlaceholder { get; set; }


        //[Required(ErrorMessage = "Name is required")]
        //public string InsertedName { get; set; }

        //public string SurnamePlaceholder { get; set; }

        //[Required(ErrorMessage = "Surname is required")]
        //public string InsertedSurname { get; set; }

        //public string EmailPlaceholder { get; set; }


        //public string AddressPlaceholder { get; set; }

        //[Required(ErrorMessage = "Address is required")]
        //public string InsertedAddress { get; set; }

        //public string LocalityPlaceholder { get; set; }

        //[Required(ErrorMessage = "Locality is required")]
        //public string InsertedLocality { get; set; }

        //public string PostCodePlaceholder { get; set; }

        //[Required(ErrorMessage = "Post Code is required")]
        //public string InsertedPostCode { get; set; }

        //public string DateOfBirthPlaceholder { get; set; }

        //[Required(ErrorMessage = "Date of Birth is required")]
        //public string InsertedDateOfBirth { get; set; }

        //public string CountryPlaceholder { get; set; }

        //[Required(ErrorMessage = "Country is required")]
        //public CS.Common.Constants.Enums.CountryIso3166? InsertedCountry { get; set; }

        //public string PasswordTitle { get; set; }
        //public string PasswordPlaceholder { get; set; }

        //public bool isUpdate { get; set; }

        //public string ConfirmPasswordPlaceholder { get; set; }

        //public string SubmitPlaceholder { get; set; }
    }
}
