﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using Umbraco.Core;
using Umbraco.Core.Models;

namespace CS.Common.Umbraco.Helpers
{
    public static class ContentHelpers
    {
        /// <summary>
        /// A method which will return an IContentType when can easily access the proeprty types aliases and actual names
        /// </summary>
        /// <param name="contentTypeId"></param>
        /// <returns></returns>
        public static string GetContentType(int contentTypeId, string propertyAlias)
        {
            var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;

            var ct = contentTypeService.GetContentType(contentTypeId);

            return (from propertyType in ct.PropertyTypes where propertyType.Alias.ToLower().Equals(propertyAlias.ToLower()) select propertyType.Name).FirstOrDefault();
        }

        public static void ClearUmbracoCache()
        {
            var cacheService = new UmbracoApplicationRuntimeCacheService();
            cacheService.ClearAllCache();
        }
    }
}
