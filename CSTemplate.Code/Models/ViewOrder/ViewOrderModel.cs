﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;

namespace CSTemplate.Code.Models.ViewOrder
{
    public class ViewOrderModel
    {
        public DateTime OrderDate { get; set; }
        public string OrderNumber { get; set; }
        public string OrderStatus { get; set; }
        public decimal OrderTotal { get; set; }
        public Enums.PaymentStatus PaymentStatus { get; set; }

        public string BillingName { get; set; }
        public string BillingEmail { get; set; }
        public string BillingContact { get; set; }
        public string BillingAddress { get; set; }

        public List<_OrderPhotosModel> ListOfOrderPhotos { get; set; }

        public decimal SubtotalPrice { get; set; }
        public bool IsAStoryTelling { get; set; }
        public bool IsOrderOwner { get; set; }

        public Enums.PaymentStatus PayStatus { get; set; }
    }
}
