﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Orders
{

    public interface IGetAllOrdersService
    {
        List<OrderPoco> Get();
    }

    [Service]
    public class GetAllOrdersService : IGetAllOrdersService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetAllOrdersService(IGetCurrentUmbracoDatabaseService
            getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public List<OrderPoco> Get()
        {
            var result = false;

            var sqlQuery = new Sql($@"
                SELECT *
	                FROM {DatabaseConstantValues.OrdersTableName} {DatabaseConstantValues.aliasForOrdersPocoTableNameAlias}
                    WHERE {DatabaseConstantValues.aliasForOrdersPocoTableNameAlias}.{DatabaseConstantValues.Orders_UserId_Field_Name} IS NOT NULL
            ");

            using (var db = _getCurrentUmbracoDatabaseService.Get())
            {
                var sqlQueryResult = db.Fetch<OrderPoco>(sqlQuery);

                return sqlQueryResult;
            }
        }
    }
}
