﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Authentication;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.ResetPassword;
using CSTemplate.Code.Models.Shared;
using Umbraco.Web;

namespace CSTemplate.Code.Services.ResetPassword
{

    public interface IGetHandleResetPasswordSubmitFormService
    {
        ResetPasswordSubmitFormResponseModel HandleResetPasswordSubmitForm(UmbracoHelper umbracoHelper,
            _ResetPasswordFormModel model);
    }

    [Service]
    public class GetHandleResetPasswordSubmitFormService : IGetHandleResetPasswordSubmitFormService
    {
        private readonly IMemberServiceWrapper _memberServiceWrapper;
        private readonly ICommonResetPasswordService _commonResetPasswordService;

        public GetHandleResetPasswordSubmitFormService(IMemberServiceWrapper memberServiceWrapper,
            ICommonResetPasswordService commonResetPasswordService)
        {
            _commonResetPasswordService = commonResetPasswordService;
            _memberServiceWrapper = memberServiceWrapper;
        }

        public ResetPasswordSubmitFormResponseModel HandleResetPasswordSubmitForm(UmbracoHelper umbracoHelper,
           _ResetPasswordFormModel model)
        {

            var responseModel = new ResetPasswordSubmitFormResponseModel() { };

            var requestQueryString = model.MemberRequestGuid;
            var resetMember = _memberServiceWrapper.GetByEmail(umbracoHelper.UmbracoContext, model.EmailAddress);

            if (resetMember != null)
            {
                if (!String.IsNullOrEmpty(requestQueryString))
                {
                    var memberResetGuidValue = resetMember.Properties[ConstantValues.Memeber_Reset_Password_Guid_Alias].Value;

                    if (memberResetGuidValue != null)
                    {
                        //See if the QS matches the value on the member property
                        if (memberResetGuidValue.ToString() == requestQueryString)
                        {
                            //Got a match, now check to see if the 15min window hasnt expired
                            DateTime expiryTime = DateTime.ParseExact(requestQueryString,
                                ConstantValues.ResetPasswordGuidFormatProvider,
                                null);

                            //Check the current time is less than the expiry time
                            DateTime currentTime = DateTime.Now;

                            //Check if date has NOT expired (been and gone)
                            if (currentTime.CompareTo(expiryTime) < 0)
                            {
                                bool failed = false;
                                try
                                {
                                    //success
                                    if (_commonResetPasswordService.ResetPassword(resetMember,
                                        ConstantValues.Memeber_Reset_Password_Guid_Alias,
                                        model.InsertedPassword))
                                    {
                                        responseModel.WasSuccess = true;
                                    }
                                    else
                                    {
                                        failed = true;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    failed = true;
                                }

                                if (failed)
                                {
                                    responseModel.WasSuccess = false;
                                    responseModel.Message =
                                        "Password was not reseted due to unexpected error. Please contact the website administrator";
                                }
                            }
                            else
                            {
                                //Request has expired
                                responseModel.WasSuccess = false;
                                responseModel.Message = "Request has expired!";
                            }
                        }
                        else
                        {
                            //ERROR: QS does not match what is stored on member property
                            //Invalid GUID
                            responseModel.WasSuccess = false;
                            responseModel.Message = "Invalid Request! Please make sure that you have opened last email sent to you to reset your password!";
                        }
                    }
                    else
                    {
                        //invalid request - no guid stored in database
                        responseModel.WasSuccess = false;
                        responseModel.Message = "Invalid Request! No request has been submitted for such account!";
                    }

                }
                else
                {
                    //ERROR: No QS present
                    //No GUID present
                    responseModel.WasSuccess = false;
                    responseModel.Message = "Invalid Request!";
                }
            }
            else
            {
                //member does not exists
                responseModel.WasSuccess = false;
                responseModel.Message = "Email does not exists!";
            }


            return responseModel;
        }

    }
}
