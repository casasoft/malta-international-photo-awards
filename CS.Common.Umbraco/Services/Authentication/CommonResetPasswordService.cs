﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using Umbraco.Core;
using Umbraco.Core.Models;

namespace CS.Common.Umbraco.Services.Authentication
{

    public interface ICommonResetPasswordService
    {
        /// <summary>
        /// A method which will reset a member password, for Umbraco.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="memberGuidPropertyAliasName"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        bool ResetPassword(IMember member, string memberGuidPropertyAliasName, string newPassword);
    }

    [Service]
    public class CommonResetPasswordService : ICommonResetPasswordService
    {
        private readonly IMemberServiceWrapper _memberServiceWrapper;

        public CommonResetPasswordService(IMemberServiceWrapper
            memberServiceWrapper)
        {
            _memberServiceWrapper = memberServiceWrapper;
        }

        /// <summary>
        /// A method which will reset a member password, for Umbraco.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="memberGuidPropertyAliasName"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public bool ResetPassword(IMember member, string memberGuidPropertyAliasName, string newPassword)
        {
            var memberService = ApplicationContext.Current.Services.MemberService;

            try
            {
                //Got a match, we can allow user to update password
                //resetMember.RawPasswordValue.Password = model.Password;
                memberService.SavePassword(member, newPassword);

                //Remove the resetGUID value
                member.Properties[memberGuidPropertyAliasName].Value =
                    string.Empty;

                //Save the member
                memberService.Save(member);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
