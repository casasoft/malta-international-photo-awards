﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Orders
{

    public interface IGetViewOrderPageUrlService
    {
        string Get(UmbracoHelper umbracoHelper, string orderNumber);
    }

    [Service]
    public class GetViewOrderPageUrlService : IGetViewOrderPageUrlService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetViewOrderPageUrlService(IUmbracoHelperService
            umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }

        public string Get(UmbracoHelper umbracoHelper, string orderNumber)
        {
            var membersAreaOrderHistoryUrl = _umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)
              .Children(x => x.DocumentTypeAlias.Equals(ConstantValues.Members_Area_Page_Data_Document_Type_Alias)).FirstOrDefault()
              .Children(x => x.DocumentTypeAlias.Equals(ConstantValues.Order_History_Page_Data_Document_Type_Alias)).FirstOrDefault().Url;

            return String.Format("{0}order-{1}", membersAreaOrderHistoryUrl, orderNumber);
        }
    }
}
