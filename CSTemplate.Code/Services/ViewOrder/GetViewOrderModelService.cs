﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.ViewOrder;
using CSTemplate.Code.Services.OrderItems;
using CSTemplate.Code.Services.Orders;
using CSTemplate.Code.Services.Users;
using umbraco;
using Umbraco.Web;

namespace CSTemplate.Code.Services.ViewOrder
{

    public interface IGetViewOrderModelService
    {
        ViewOrderModel Get(UmbracoHelper umbracoHelper, int orderId, int userId);
    }

    [Service]
    public class GetViewOrderModelService : IGetViewOrderModelService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetOrderDetailsFilterByOrderId _getOrderDetailsFilterByOrderId;
        private readonly IGetOrderItemsFilterByOrderIdOrUserIdService _getOrderItemsFilterByOrderIdOrUserIdService;
        private readonly IGetOrderTotalCalculateService _getOrderTotalCalculateService;
        private readonly IGetIsAStoryTellingOrderService _getIsAStoryTellingOrderService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;


        public GetViewOrderModelService(IUmbracoHelperService
            umbracoHelperService,
            IGetOrderDetailsFilterByOrderId
            getOrderDetailsFilterByOrderId,
            IGetOrderItemsFilterByOrderIdOrUserIdService
            getOrderItemsFilterByOrderIdOrUserIdService,
            IGetOrderTotalCalculateService
            getOrderTotalCalculateService,
            IGetIsAStoryTellingOrderService
            getIsAStoryTellingOrderService,
            IGetUserFilterByUserIdService
            getUserFilterByUserIdService)
        {
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _getIsAStoryTellingOrderService = getIsAStoryTellingOrderService;
            _getOrderTotalCalculateService = getOrderTotalCalculateService;
            _getOrderItemsFilterByOrderIdOrUserIdService = getOrderItemsFilterByOrderIdOrUserIdService;
            _getOrderDetailsFilterByOrderId = getOrderDetailsFilterByOrderId;
            _umbracoHelperService = umbracoHelperService;
        }

        public ViewOrderModel Get(UmbracoHelper umbracoHelper, int orderId, int userId)
        {
            ViewOrderModel viewOrderModel = new ViewOrderModel();
            viewOrderModel.ListOfOrderPhotos = new List<_OrderPhotosModel>();

            var orderDetail = _getOrderDetailsFilterByOrderId.Get(orderId);
            if (orderDetail.UserId != userId || orderId <= 0 || userId <= 0) return viewOrderModel;


            var awardRulesUrl = _umbracoHelperService.TypedContent(umbracoHelper,
                CS.Common.Umbraco.Constants.ConstantValues.Awards_Rules_Page_Id).Url;
            var termsAndConditionsUrl = _umbracoHelperService.TypedContent(umbracoHelper, CS.Common.Umbraco.Constants.ConstantValues.Terms_And_Conditions_Page_Id).Url;


            DateTime currentTime = DateTime.Now;

            CSTemplate.Code.Constants.Enums.PaymentStatus paymentStatus = (CSTemplate.Code.Constants.Enums.PaymentStatus)orderDetail.PaymentStatusEnumId;

            var orderItems = _getOrderItemsFilterByOrderIdOrUserIdService.Get(umbracoHelper: umbracoHelper, orderId: orderId, userId: 0);
            var orderTotal = _getOrderTotalCalculateService.Get(orderId);
            var orderStatus = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, paymentStatus);
            var isAStoryTelling = _getIsAStoryTellingOrderService.Get(orderId);
            var userDetail = _getUserFilterByUserIdService.Get(umbracoHelper, userId);
            var userEmail = userDetail.InsertedEmail;
            var userFullName = userDetail.Name + " " + userDetail.Surname;

            viewOrderModel = new ViewOrderModel()
            {
                PayStatus = orderDetail.PaymentStatusEnumId,

                OrderDate = orderDetail.Date,
                OrderNumber = orderDetail.OrderItemId.ToString(),
                OrderStatus = orderStatus,
                OrderTotal = orderTotal,
                IsAStoryTelling = isAStoryTelling,
                IsOrderOwner = true,

                BillingName = userFullName,
                BillingEmail = userEmail,
                BillingContact = userDetail.InsertedPhone,

                SubtotalPrice = orderTotal,
                ListOfOrderPhotos = orderItems,

            };
            return viewOrderModel;
        }
    }
}
