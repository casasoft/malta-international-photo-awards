﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Constants;

namespace CS.Common.Models.Paging
{
    public class PagerItemModel
    {
        public int PageNumber { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string CssClassName { get; set; }
    }
}
