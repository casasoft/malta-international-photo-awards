﻿using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.Awards
{
    public interface IGetActiveAwardEditionService
    {
        AwardEditionPageData Get(UmbracoHelper umbracoHelper);
    }
    [Service]
    public class GetActiveAwardEditionService : IGetActiveAwardEditionService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetActiveAwardEditionService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public AwardEditionPageData Get(UmbracoHelper umbracoHelper)
        {
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;

            var awardEditionNode = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionPageData)x).Where(x => DateTime.Now.Date >= x.StartedDate && DateTime.Now <= x.EndedDate).FirstOrDefault();

            return awardEditionNode;
            
        }
    }
}
