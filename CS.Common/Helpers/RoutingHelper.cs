﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;

namespace CS.Common.Helpers
{
    public class RoutingHelper
    {
        public static RouteValueDictionary GetUrlValuesFromBaseUrl(HttpRequestBase httpRequestBase, string url)
        {
            var baseUri = new Uri(httpRequestBase.Url.Scheme + Uri.SchemeDelimiter + httpRequestBase.Url.Host);
            var uri = new Uri(baseUri, url);
            var urlWithoutQuery = uri.GetLeftPart(UriPartial.Path);
            var urlQuery = uri.Query;

            var request = new HttpRequest(null, urlWithoutQuery, urlQuery);
            var response = new HttpResponse(new StringWriter());
            var httpContext = new HttpContext(request, response);
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

            return routeData.Values;
        }
    }
}
