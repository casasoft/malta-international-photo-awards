﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Components;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Helpers
{
    public static class USkinnedComponentHelpers
    {
        public static IComponentModel GetComponentModelBeforePassingItComponentsSwitch(List<CustomComponentModel> customComponents, IPublishedContent component,
            out List<CustomComponentModel> customComponentsLeft)
        {
            customComponentsLeft = new List<CustomComponentModel>();

            IComponentModel componentModelToReturn = null;

            CustomComponentModel customComponentToBeDeleted = new CustomComponentModel();
            if (customComponents != null)
            {
                foreach (var customComponentModel in customComponents)
                {
                    if (customComponentModel.DocumentTypeAlias.Equals(component.DocumentTypeAlias))
                    {
                        componentModelToReturn = new CustomComponentModel()
                        {
                            PublishedContent = component,
                            PartialViewUrl = customComponentModel.PartialViewUrl,
                            Content = customComponentModel.Content
                        };
                        customComponentToBeDeleted = customComponentModel;
                        break;
                    }
                }

                foreach (var customComponentModel in customComponents)
                {
                    if (!customComponentModel.Equals(customComponentToBeDeleted))
                    {
                        customComponentsLeft.Add(customComponentModel);
                    }
                }

            }
            if (componentModelToReturn == null)
            {
                componentModelToReturn = new USkinnedDefaultComponentModel() { Content = component };
            }

            return componentModelToReturn;
        }
    }
}
