﻿using System.Web.Mvc;
using Umbraco.Core.Models;
using System.Web.Security;
using CS.Common.Constants;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using USN.USNModels;
using Umbraco.Web;
using Enums = CSTemplate.Code.Constants.Enums;

namespace USN.USNControllers
{
    /// <summary>
    /// Not using strongly typed models here so that PureLive mode can be used
    /// </summary>
    public class USNLoginFormSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        //private readonly IUmbracoHelperService _umbracoHelperService;
        //private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        //public USNLoginFormSurfaceController(
        //    IUmbracoHelperService umbracoHelperService,
        //    IUmbracoControllerWrapperService umbracoControllerWrapperService)
        //{
        //    _umbracoHelperService = umbracoHelperService;
        //    _umbracoControllerWrapperService = umbracoControllerWrapperService;
        //}
        public ActionResult Index()
        {
            //var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var model = new USNLoginFormViewModel();

            if (CurrentPage.Url != Request.Url.PathAndQuery)
                model.ReturnUrl = Request.Url.PathAndQuery;
            else
                model.ReturnUrl = CurrentPage.GetPropertyValue<IPublishedContent>("loginSuccessPage").Url;
            model.LoginFormButtonText = CurrentPage.GetPropertyValue<string>("loginFormButtonText");
            model.ForgotPasswordButtonText = "Forgot Password?";
            model.ForgotPasswordButtonUrl = "/login/forgot-password";
            //model.LoginText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.Login_Page_Explanation_Text_Login_Panel);
            //model.LoginTitle = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.Login_Page_Title_Login_Panel);
            //model.Required = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, Enums.DictionaryKey.Dictionary_Form_Required_Text);
            return PartialView("USNForms/USN_LoginForm", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HandleLoginSubmit(USNLoginFormViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            // Login
            if (Membership.ValidateUser(model.Username, model.Password))
            {
                FormsAuthentication.SetAuthCookie(model.Username, false);

                return Redirect(model.ReturnUrl);
            }
            else
            {
               
                TempData.Add("LoginFailure", umbraco.library.GetDictionaryItem("USN Login Form Login Error"));
                return RedirectToCurrentUmbracoPage();
            }
        }
    }

}