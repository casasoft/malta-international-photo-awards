﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.JuryMember;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using CSTemplate.Code.Models.USNModels;

namespace CSTemplate.Code.Models.Home
{
    public class HomePageCustomModel : USNBaseViewModel
    {
        public _ListingJuryMembersModel JudgesList { get; set; }
        

        public HomePageCustomModel(IPublishedContent content) : base(content)
        {

        }
    }
}
