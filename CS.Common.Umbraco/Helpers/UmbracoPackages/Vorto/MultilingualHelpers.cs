﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CS.Common.Helpers;
using CS.Common.Umbraco.Constants;
using CS.Common.Umbraco.Services.Umbraco.Wrappers;
using Our.Umbraco.Vorto.Extensions;
using Our.Umbraco.Vorto.Models;
using Skybrud.Umbraco.GridData;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;
using Umbraco.Web.Templates;
using Newtonsoft.Json;
using CS.Common.Umbraco.Models.Multilingual;
using InfoCaster.Umbraco.UrlTracker;
using InfoCaster.Umbraco.UrlTracker.Helpers;
using InfoCaster.Umbraco.UrlTracker.Models;

namespace CS.Common.Umbraco.Helpers.UmbracoPackages.Vorto
{
    public static class MultilingualHelpers
    {
        public static string GetCountryCodeFromCulture(CultureInfo culture)
        {
            var countryInfo = new RegionInfo(culture.LCID);
            var countryCode = countryInfo.TwoLetterISORegionName.ToLowerInvariant();

            return countryCode;
        }

        public static string AppendCultureNameToUrl(string url, string cultureName, UrlProviderMode urlMode = UrlProviderMode.Relative, Uri currentUri = null)
        {
            var urlWithCultureName = url;

            var indexOfInsertion = 0;
            if (urlMode == UrlProviderMode.Absolute && currentUri != null)
            {
                indexOfInsertion = currentUri.GetLeftPart(UriPartial.Authority).Length;
            }

            urlWithCultureName = urlWithCultureName.Insert(indexOfInsertion, "/" + cultureName);

            return urlWithCultureName;
        }

        public static string GetValueForCurrentCultureOrPassedCulture(VortoValue vortoValue, string cultureName = null, bool getValueForDefaultCultureIfEmpty = true)
        {
            var valueForCurrentCultureOrPassedCulture = String.Empty;

            if (vortoValue != null && vortoValue.Values != null && vortoValue.Values.Count >= 1)
            {
                if (cultureName == null)
                {
                    var currentCulture = CultureInfo.CurrentCulture;
                    cultureName = currentCulture.Name;
                }

                object valueForCurrentCultureAsObj;
                vortoValue.Values.TryGetValue(cultureName, out valueForCurrentCultureAsObj);

                if (valueForCurrentCultureAsObj == null && getValueForDefaultCultureIfEmpty)
                {
                    vortoValue.Values.TryGetValue(ConstantValues.LanguageDefaultName, out valueForCurrentCultureAsObj);
                }

                if (valueForCurrentCultureAsObj != null)
                {
                    valueForCurrentCultureOrPassedCulture = valueForCurrentCultureAsObj.ToString();
                    valueForCurrentCultureOrPassedCulture = TemplateUtilities.ParseInternalLinks(valueForCurrentCultureOrPassedCulture);
                }
            }

            return valueForCurrentCultureOrPassedCulture;
        }

        public static VortoValue ConvertVortoValueFromJsonToObject(string vortoValueAsJson)
        {
            VortoValue vortoValueAsObject = null;

            if (!String.IsNullOrEmpty(vortoValueAsJson))
            {
                try
                {
                    vortoValueAsObject = JsonConvert.DeserializeObject<VortoValue>(vortoValueAsJson);
                }
                catch (Exception ex)
                {
                    //do nothing
                }
            }

            return vortoValueAsObject;
        }

        public static string GetValueForCurrentCultureOrPassedCulture(this string vortoValueAsJson, string cultureName = null)
        {
            var valueForCurrentCultureOrPassedCulture = String.Empty;

            var vortoValue = ConvertVortoValueFromJsonToObject(vortoValueAsJson);
            valueForCurrentCultureOrPassedCulture = GetValueForCurrentCultureOrPassedCulture(vortoValue, cultureName);

            return valueForCurrentCultureOrPassedCulture;
        }

        public static GridDataModel GetValueForGridLayoutForCurrentCulture(VortoValue vortoValue)
        {
            var jsonValueForCurrentCulture = GetValueForCurrentCultureOrPassedCulture(vortoValue);

            return GridDataModel.Deserialize(jsonValueForCurrentCulture);
        }

        public static string GetUrlSegment(this IPublishedContent content, string culture, bool ensureEndsWithSlash = true)
        {
            //We first get the title of the content for the Url Segment
            var urlSegment = content.GetVortoValue<string>("title", culture);

            //If the client inputs an Alternative Page Url Name this takes priority
            if (!string.IsNullOrWhiteSpace(content.UrlName))
            {
                urlSegment = content.UrlName;
            }

            //If there's the case that the title and the alternative page url doesn't exist, we need to use the Page Name as this is always required.
            if (string.IsNullOrEmpty(urlSegment))
            {
                urlSegment = content.Name;
            }

            urlSegment = urlSegment.ToUrlSegment();

            if (ensureEndsWithSlash)
            {
                urlSegment = urlSegment.EnsureEndsWith("/");
            }

            return urlSegment;
        }

        public static string GetUrlSegment(
            VortoValue contentUrlTitleVortoValue,
            VortoValue contentTitleVortoValue,
            string contentName,
            string cultureName)
        {
            var urlSegment = GetValueForCurrentCultureOrPassedCulture(contentUrlTitleVortoValue, cultureName, false);

            if (String.IsNullOrEmpty(urlSegment))
            {
                urlSegment = GetValueForCurrentCultureOrPassedCulture(contentTitleVortoValue, cultureName, false);
            }

            if (String.IsNullOrEmpty(urlSegment))
            {
                urlSegment = contentName;
            }

            return urlSegment.ToUrlSegment().EnsureEndsWith("/");
        }

        public static string GetMultilingualUrl(UmbracoContext umbracoContext,
            IEnumerable<ILanguage> allLanguages, string cultureName, IPublishedContent content,
            Uri current)
        {
            // On the frontend get the domain that matches the current culture. We could also check the domain name, but for now the culture is enough.
            // Otherwise just get the first domain. The urls for the other domains are generated in the GetOtherUrls method.
            //var cultureToUse = UmbracoContext.Current.IsFrontEndUmbracoRequest
            //              ? allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(cultureName))
            //              : allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(ConstantValues.LanguageDefaultName));

            ILanguage cultureToUse = null;

            var isCultureNameValid = allLanguages.SingleOrDefault(x => String.Compare(x.CultureInfo.Name, cultureName, StringComparison.OrdinalIgnoreCase) == 0) != null;
            if (isCultureNameValid)
            {
                cultureToUse = allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(cultureName));
            }
            else
            {
                cultureToUse = allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(ConstantValues.LanguageDefaultName));
            }

            if (content.DocumentTypeAlias.InvariantEquals(ConstantValues.Home_Page_Document_Type_Alias) ||
                content.DocumentTypeAlias.InvariantEquals(ConstantValues.Website_Configuration_Document_Type_Alias))
            {
                // Return the domain if we're on the homepage because on that node we've added the domains.
                if (cultureToUse.CultureInfo.Name.InvariantEquals(ConstantValues.LanguageDefaultName))
                {
                    return "/";
                }

                return "/" + cultureToUse.CultureInfo.Name.EnsureEndsWith("/");
            }

            // Get the parent url and add the url segment of this culture.
            var parentUrl = String.Empty;
            if (content.Parent != null)
            {
                parentUrl = umbracoContext.UrlProvider.GetUrl(content.Parent.Id, current, false);

                if (parentUrl.InvariantEquals("/"))
                {
                    parentUrl += cultureToUse.CultureInfo.Name;
                }

                parentUrl = parentUrl.EnsureEndsWith("/");
            }

            var urlSegment = GetUrlSegment(
                content.GetPropertyValue<VortoValue>(ConstantValues.Common_Content_Url_Title),
                content.GetPropertyValue<VortoValue>(ConstantValues.Common_Content_Title),
                content.Name,
                cultureToUse.CultureInfo.Name);

            var url = parentUrl + urlSegment;
            return url;
        }

        //This method is based upon the method, called - "AddUrlMapping", found in UrlTracker.Repositories.UrlTrackerRepository
        public static bool AddMultilingualUrlMapping(
            IContent content,
            int rootNodeId,
            string url,
            AutoTrackingTypes type,
            CultureInfo culture,
            List<MultilingualUrlTrackerDataModel> contentChildren,
            bool isChild = false)
        {
            if (url != "#" && content.Template != null && content.Template.Id > 0)
            {
                string notes = isChild ? "An ancestor" : "This page";
                switch (type)
                {
                    case AutoTrackingTypes.Moved:
                        notes += " was moved";
                        break;
                    case AutoTrackingTypes.Renamed:
                        notes += " was renamed";
                        break;
                    case AutoTrackingTypes.UrlOverwritten:
                        notes += "'s property 'umbracoUrlName' changed";
                        break;
                    case AutoTrackingTypes.UrlOverwrittenSEOMetadata:
                        notes += string.Format("'s property '{0}' changed", UrlTrackerSettings.SEOMetadataPropertyName);
                        break;
                }

                url = UrlTrackerHelper.ResolveShortestUrl(url);

                if (UrlTrackerSettings.HasDomainOnChildNode)
                {
                    var rootNode = new umbraco.NodeFactory.Node(rootNodeId);
                    var rootUri = new Uri(rootNode.NiceUrl);
                    var shortRootUrl = UrlTrackerHelper.ResolveShortestUrl(rootUri.AbsolutePath);
                    if (url.StartsWith(shortRootUrl, StringComparison.OrdinalIgnoreCase))
                    {
                        url = UrlTrackerHelper.ResolveShortestUrl(url.Substring(shortRootUrl.Length));
                    }
                }

                if (!string.IsNullOrEmpty(url))
                {
                    string query = "SELECT 1 FROM icUrlTracker WHERE RedirectNodeId = @0 AND OldUrl = @1";
                    int exists = ApplicationContext.Current.DatabaseContext.Database.ExecuteScalar<int>(query, content.Id, url);

                    if (exists != 1)
                    {
                        LoggingHelper.LogInformation("UrlTracker Repository | Adding mapping for node id: {0} and url: {1}", new string[] { content.Id.ToString(), url });

                        ApplicationContext.Current.ApplicationCache.RequestCache.ClearCacheItem("MultilingualUrlProvider-GetUrl-" + content.Id + "-" + culture.Name);
                        var contentUrl = UmbracoContext.Current.UrlProvider.GetUrl(content.Id);
                        query = "INSERT INTO icUrlTracker (RedirectRootNodeId, RedirectUrl, OldUrl, Notes) VALUES (@0, @1, @2, @3)";
                        ApplicationContext.Current.DatabaseContext.Database.Execute(query, rootNodeId, contentUrl, url, notes);

                        if (contentChildren != null && contentChildren.Any())
                        {
                            foreach (var child in contentChildren)
                            {
                                AddMultilingualUrlMapping(
                                    child.CurrentContent,
                                    child.OldContentDomainRootNodeId,
                                    child.OldContentMultilingualUrls[culture],
                                    type,
                                    culture,
                                    child.CurrentContentChildren,
                                    true);
                            }
                        }

                        return true;
                    }
                }
            }

            return false;
        }

        private static Guid GetDataTypeDefinitionGuid(Property property)
        {
            var dtdId = property.PropertyType.DataTypeDefinitionId;
            var dtd = ApplicationContext.Current.Services.DataTypeService.GetDataTypeDefinitionById(dtdId);
            return dtd.Key;
        }

        public static void SetVortoProperty(IContent content, string propertyAlias, string propertyNewValue)
        {
            VortoValue propertyVortoValue = null;

            var property = content.Properties[propertyAlias];
            if (property.Value != null)
            {
                propertyVortoValue = JsonConvert.DeserializeObject<VortoValue>(property.Value.ToString());

                if (propertyVortoValue.Values != null)
                {
                    propertyVortoValue.Values[ConstantValues.LanguageDefaultName] = propertyNewValue;
                }
            }
            else
            {   
                var id = GetDataTypeDefinitionGuid(property);
                    
                //add our vorto value object due to constractor
                //propertyVortoValue = new VortoValue();
                propertyVortoValue.DtdGuid = id;
                propertyVortoValue.Values = new Dictionary<string, object>();
                propertyVortoValue.Values.Add(ConstantValues.LanguageDefaultName, propertyNewValue);
            }

            var propertyVortoValueSerialized = JsonConvert.SerializeObject(propertyVortoValue);
            content.SetValue(propertyAlias, propertyVortoValueSerialized);
        }
    }
}       