﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Services.Database;
using databaseConstants = CSTemplate.Code.Constants.DatabaseConstantValues;
namespace CSTemplate.Code.Services.Photos
{

    public interface IGetFirstPhotoUrlFilterBySubmissionIdService
    {
        string Get(int submissionId);
    }

    [Service]
    public class GetFirstPhotoUrlFilterBySubmissionIdService : IGetFirstPhotoUrlFilterBySubmissionIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetFirstPhotoUrlFilterBySubmissionIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public string Get(int submissionId)
        {
            string photoUrl = string.Empty;
            if (submissionId <= 0) return photoUrl;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var photosTable = databaseConstants.PhotosTableName;
            var photoUrlField = databaseConstants.Photos_PhotoUrl_Field_Name;
            var photoPrimaryKeyField = databaseConstants.Photos_Poco_Primary_Key_Value;
            var submissionIdField = databaseConstants.Photos_Submission_Id_Field_Name;
            photoUrl = umbracoDb.Query<string>($@"SELECT {photoUrlField} FROM {photosTable} WHERE {submissionIdField}={submissionId} ORDER BY {photoPrimaryKeyField} DESC").FirstOrDefault();
            var fullPhotoUrl = CSTemplate.Code.Constants.ConstantValues.Submitted_Photo_Source_Address + photoUrl;
            return fullPhotoUrl;
        }
    }
}
