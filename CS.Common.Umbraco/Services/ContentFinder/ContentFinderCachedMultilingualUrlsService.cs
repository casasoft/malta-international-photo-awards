﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Constants;
using Umbraco.Core;
using Umbraco.Core.Cache;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CS.Common.Umbraco.Services.ContentFinder
{

    public interface IContentFinderCachedMultilingualUrlsService
    {
        List<Tuple<int, string>> Get(UmbracoHelper umbracoHelper, IDomain currentDomain);
    }

    [Service]
    public class ContentFinderCachedMultilingualUrlsService : IContentFinderCachedMultilingualUrlsService
    {
        public List<Tuple<int, string>> Get(UmbracoHelper umbracoHelper, IDomain currentDomain = null)
        {
            var multilingualUrlCacheKey = "MultilingualContentFinder-Urls";

            if (currentDomain != null)
            {
                multilingualUrlCacheKey = multilingualUrlCacheKey + "-" + currentDomain.Id;
            }

            return UmbracoContext.Current.Application.ApplicationCache.RuntimeCache.GetCacheItem<List<Tuple<int, string>>>(
                            multilingualUrlCacheKey,
                            () =>
                            {
                                var contentUrls = new List<Tuple<int, string>>();

                                // Get all the nodes in the website.
                                //var allNodes = helper.TypedContent(1129).DescendantsOrSelf().ToList();
                                var allNodes = new List<IPublishedContent>();
                                var allRootNodes = umbracoHelper.TypedContentAtRoot();


                                //If there is a domain set, then it means that you need only to consider
                                //root nodes which have that domain as hostnames
                                if (currentDomain != null)
                                {
                                    foreach (var rootNode in allRootNodes)
                                    {
                                        var rootNodeAssignedDomains = ApplicationContext.Current.Services.DomainService.GetAssignedDomains(rootNode.Id, false);

                                        foreach (var domain in rootNodeAssignedDomains)
                                        {
                                            if (domain.Id == currentDomain.Id)
                                            {
                                                AddNodes(allNodes, rootNode);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var rootNode in allRootNodes)
                                    {
                                        AddNodes(allNodes, rootNode);
                                    }
                                }

                                foreach (var node in allNodes)
                                {
                                    if (node.HasProperty(ConstantValues.Common_Content_Title))
                                    {
                                        // Get all the urls in the website.
                                        // With UrlProvider.GetOtherUrls we also get the urls of the other languages.
                                        contentUrls.Add(new Tuple<int, string>(node.Id, node.Url));
                                        contentUrls.AddRange(UmbracoContext.Current.UrlProvider.GetOtherUrls(node.Id).Select(x => new Tuple<int, string>(node.Id, x)));
                                    }
                                }
                                return contentUrls;
                            });
        }

        private void AddNodes(List<IPublishedContent> allNodes, IPublishedContent rootNode)
        {
            allNodes.Add(rootNode);
            var allNodesUnderRootNode = rootNode.Descendants();
            allNodes.AddRange(allNodesUnderRootNode);
        }
    }
}
