﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Services
{
    public class ServiceAttribute : Attribute
    {

        public bool Singleton { get; set; }
        public const int ServiceAttributeDefaultValue = 0;


        public int Priority { get; set; }
    }
}
