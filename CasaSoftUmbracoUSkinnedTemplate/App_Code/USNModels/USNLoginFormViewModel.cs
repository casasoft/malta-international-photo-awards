﻿using System.ComponentModel.DataAnnotations;

namespace USN.USNModels
{
    /// <summary>
    /// Not using strongly typed models here so that PureLive mode can be used
    /// </summary>
    public class USNLoginFormViewModel
    {
        [Required]
        
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        
        public string Password { get; set; }

        public string ReturnUrl { get; set; }

        public string LoginFormButtonText { get; set; }
        public string LoginTitle { get; set; }
        public string LoginText { get; set; }
        public string Required { get; set; }

        public string ForgotPasswordButtonText { get; set; }
        public string ForgotPasswordButtonUrl { get; set; }



    }
}