﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.ProfilePage;
using CSTemplate.Code.Services.SubmitStorytelling;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Shared.SubmitStorytellingForm
{
    [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
    public partial class _SubmitStoryTellingFormController : USNBaseController
    {

        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetSubmitStoryTellingFormService _getSubmitStoryTellingFormService;

        public _SubmitStoryTellingFormController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetSubmitStoryTellingFormService getSubmitStoryTellingFormService
        )
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getSubmitStoryTellingFormService = getSubmitStoryTellingFormService;
        }

        // GET: SubmitStoryTellingForm
        //public virtual ActionResult _SubmitStoryTellingForm(RenderModel model)
        //{
        //    var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
        //    _SubmitStoryTellingFormModel submitStoryTellingFormModel =
        //        _getSubmitStoryTellingFormService.Get(umbracoHelper, model.Content);
        //    return base.Populate(submitStoryTellingFormModel);
        //}
    }
}