﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Models.Shared;

namespace CS.Common.Models.Paging
{
    public class PagerModel
    {
        public List<PagerItemModel> Pages { get; set; }
        /// <summary>
        /// E.g. Showing 1 - 10 of 190 results
        /// </summary>
        public string ShowingResultsOfPagesText { get; set; }
    }
}
