﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared
{
    public class _PrizeItemModel
    {
        public string PrizeItemImgURL { get; set; }
        public string PrizeItemTitle { get; set; }
        public string PrizeItemText { get; set; }
    }
}
