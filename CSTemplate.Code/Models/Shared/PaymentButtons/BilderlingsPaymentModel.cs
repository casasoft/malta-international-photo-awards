﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared.PaymentButtons
{
    public class BilderlingsPaymentModel
    {
        public string XShopName { get; set; }
        public string ShopPassword { get; set; }
        public string XNonce { get; set; }
        public string XRequestSignature { get; set; }
        public string OrderRefId { get; set; }
        public decimal amount { get; set; }
        public string Currency { get; set; }
        public string PaymentMethod { get; set; }

        public string ClientEmailAddress { get; set; }
        public string ClientName { get; set; }
        public string ClientSurname { get; set; }

        public List<string> ListOfProducts { get; set; }
    }
}