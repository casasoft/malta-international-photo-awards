﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Attributes
{
    public class PriorityAttribute : Attribute
    {

        public PriorityAttribute(int priority)
        {
            this.Priority = priority;
        }

        public int Priority { get; set; }


    }
}
