﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Constants;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Photos;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Shared.SurfaceControllers
{
    public partial class SubmitStoryTellingFormSurfaceController : SurfaceController
    {
        private readonly ISubmitNewPhotosService _submitNewPhotosService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetAwardCategoryStoryTellingService _getAwardCategoryStoryTellingService;

        // GET: SubmitStoryTellingFormSurface
        public SubmitStoryTellingFormSurfaceController(
            ISubmitNewPhotosService submitNewPhotosService,
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetAwardCategoryStoryTellingService getAwardCategoryStoryTellingService
            )
        {
            _getAwardCategoryStoryTellingService = getAwardCategoryStoryTellingService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _submitNewPhotosService = submitNewPhotosService;
        }

        [System.Web.Http.HttpPost]
        public virtual ActionResult SubmitStoryTellingForm(_SubmitStoryTellingFormModel submitStoryTellingFormModel)
        {
           
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var currentMemberId = Members.GetCurrentMemberId();
            var submitModel = submitStoryTellingFormModel.SubmitPhotoItem;
            var category = _getAwardCategoryStoryTellingService.Get(umbracoHelper);
            submitModel.CategorySelected = category.FirstOrDefault().Value;

            var result = _submitNewPhotosService.Submit(umbracoHelper, currentMemberId, new List<_SubmitPhotoItemModel>() { submitModel });

            var redirectToUrl = result == true ? CSTemplate.Code.Constants.ConstantValues.Order_Summary_Page_Link : CSTemplate.Code.Constants.ConstantValues.Submit_StoryTelling_Page_Link;
            return base.Redirect(redirectToUrl);
           

            
        }
    }
}