﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.ChangePasswordForm
{
    public class _ChangePasswordFormModel
    {

        public string Message { get; set; }
        public string RequiredWordText { get; set; }
        public string ButtonFormText { get; set; }

        //when user changes password from members area dashboard (when the user is logged in)
        [DisplayName("Current Password")]
        [Required(ErrorMessage = "Password is required")]
        [StringLength(100, ErrorMessage = "The password must be at least 8 characters long.", MinimumLength = 8)]
        public string CurrentPassword { get; set; }

        [DisplayName("New Password")]
        [StringLength(100, ErrorMessage = "The password must be at least 8 characters long.", MinimumLength = 8)]
        public string InsertedNewPassword { get; set; }

        [DisplayName("Confirm New Password")]
        [Compare("InsertedNewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string InsertedConfirmNewPassword { get; set; }

        public bool PasswordChangedSuccesfully { get; set; }

    }
}
