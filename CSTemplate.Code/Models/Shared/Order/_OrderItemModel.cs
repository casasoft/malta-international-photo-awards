﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared.Order
{
    public class _OrderItemModel
    {
        public int OrderItemId { get; set; }
        public int PSPID { get; set; }
        public int UserId { get; set; }
        public int OrderId { get; set; }
        public string OrderRefId { get; set; }
        public DateTime Date { get; set; }
        public CSTemplate.Code.Constants.Enums.PaymentStatus PaymentStatusEnumId { get; set; }
        public string Description { get; set; }

        
    }
}
