﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Database;
using databaseConstants = CSTemplate.Code.Constants.DatabaseConstantValues;
using CSTemplate.Code.Services.Awards;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Submissions
{

    public interface ICreateNewSubmissionService
    {
        /// <summary>
        /// It creates new submission on database.
        /// </summary>
        /// <param name="userId">int userId</param>
        /// <param name="photo">_SubmitPhotoItemModel photo</param>
        /// <returns>It turns last added Submission Id Value</returns>
        int Create(UmbracoHelper umbracoHelper, int userId, _SubmitPhotoItemModel photo);

    }

    [Service]
    public class CreateNewSubmissionService : ICreateNewSubmissionService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetActiveAwardEditionService _getActiveAwardEditionService;

        public CreateNewSubmissionService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService, IGetActiveAwardEditionService getActiveAwardEditionService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
            this._getActiveAwardEditionService = getActiveAwardEditionService;
        }
        public int Create(UmbracoHelper umbracoHelper,int userId, _SubmitPhotoItemModel photo)
        {
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var submissionsTableName = databaseConstants.SubmissionsTableName;
            var primaryKeyField = databaseConstants.Submissions_Poco_Primary_Key_Value;
            var sqlMinDate = System.Data.SqlTypes.SqlDateTime.MinValue;
            var sqlMaxDate = System.Data.SqlTypes.SqlDateTime.MaxValue;
            var activeAward = _getActiveAwardEditionService.Get(umbracoHelper);
            var feedbackRepliedEmailSent = Convert.ToBoolean((int)Constants.Enums.FeedbackRepliedMailSent.Notyet);
            //todo: [For: Fatih | 2018/09/03] Check here again (WrittenBy: Fatih)        			
            try
            {
                var between = (photo.PhotoDate < sqlMinDate) || (photo.PhotoDate > sqlMaxDate);
                if (between)
                {
                    photo.PhotoDate = DateTime.Now;
                }
            }
            catch (Exception e)
            {
                photo.PhotoDate = DateTime.Now;
            }


            var newSubmission = new SubmissionPoco
            {
                //todo: [For: Fatih | 2018/09/04] Location Information (WrittenBy: Fatih)        			
                UserId = userId,
                Description = photo.isAStortyTelling==true?photo.DescriptionStoryTelling:photo.DescriptionPhoto,
                Date = photo.PhotoDate,
                CategoryId = Int32.Parse(photo.CategorySelected),
                Location = "",
                FeedbackRequest = photo.FeedBackRequest,
                IsAStoryTelling = photo.isAStortyTelling,
                Title = photo.isAStortyTelling == true ?photo.StoryTellingTitle:photo.PhotoTitle,
                StatusEnumID=Convert.ToInt32(Constants.Enums.PaymentStatus.NOTOK),
                AwardEditionId=activeAward.Id,
                FeedbackRepliedMailSent=feedbackRepliedEmailSent
                


            };
            var createNewSubmissionOnDb = umbracoDb.Insert(submissionsTableName, primaryKeyField, newSubmission);
            var lastCreatedSubmissionId = Convert.ToInt32(createNewSubmissionOnDb);
            return lastCreatedSubmissionId;
        }
    }
}
