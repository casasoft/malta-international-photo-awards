﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Umbraco.Constants
{
    public class ConstantValues
    {
        public const string QuerystringParamCurrentPage = "page";
        public const string ViewContextItem_CanonicalPrevUrl = "canonicalPrevUrl";
        public const string ViewContextItem_CanonicalNextUrl = "canonicalNextUrl";

        public const string CheckBoxListPropertyEditorAlias = "Umbraco.CheckBoxList";
        public const string DropDownMultiplePropertyEditorAlias = "Umbraco.DropDownMultiple";
        public const string DropDownPropertyEditorAlias = "Umbraco.DropDown";
        public const string DatePropertyEditorAlias = "Umbraco.Date";
        public const string TrueFalsePropertyEditorAlias = "Umbraco.TrueFalse";
        public const string IntegerPropertyEditorAlias = "Umbraco.Integer";
        public const string ListViewPropertyEditorAlias = "Umbraco.ListView";
        public const string ImageCropperPropertyEditorAlias = "Umbraco.ImageCropper";
        public const string TagsPropertyEditorAlias = "Umbraco.Tags";
        public const string GridPropertyEditorAlias = "Umbraco.Grid";
        public const string MultipleMediaPickerAlias = "Umbraco.MultipleMediaPicker";

        public const int NotAuthorizedPageId = 2343;
        public const string LastAccessedUrlQueryStringName = "lastAccessedUrl";

        public const string MemberIsApprovedAliasName = "umbracoMemberApproved";
        public const string MemberIslockedOutAliasName = "umbracoMemberLockedOut";

        public const string Common_Content_Title = "title";
        public const string Common_Content_Summary = "summary";
        public const string Common_Content_Url_Title = "urlTitle";
        public const string Common_Rss_Publish_Social_Media = "publishAutomaticallyOnSocialMedia";

        public const string Related_Links_Data_Type_Internal_Alias = "internal";
        public const string Related_Links_Data_Type_Is_Internal_Alias = "isInternal";
        public const string Related_Links_Data_Type_New_Window_Alias = "newWindow";
        public const string Related_Links_Data_Type_Caption_Alias = "caption";
        public const string Related_Links_Data_Type_Link_Alias = "link";

        public const string Home_Page_Document_Type_Alias = "USNHomepage";
        public const string Website_Configuration_Document_Type_Alias = "websiteConfiguration";

        //RSS
        public const int RssPageId = 3180;
        public const string RssTitle = "rss";
        public const string RssSocialMediaTitle = "rss-social-media";

        //Multilingual
        public const string LanguageDefaultName = "en";

        public const string Cache_Key_Get_All_Website_Languages = "GetAllWebsiteLanguages";
        public const string Query_String_Change_Lang_Name = "changeLang";
        public const string COOKIE_USER_LANG_PREFERENCE = "userPreferredLanguageValue";
        public const string COOKIE_USER_LANG_PREFERENCE_SUBKEY = "value";

        //Query Identifier
        public const string Query_Identifier_Property_Alias = "queryIdentifier";

        //Page Ids

        public const int Terms_And_Conditions_Page_Id = 1287;
        public const int Awards_Rules_Page_Id = 1556;
    }
}
