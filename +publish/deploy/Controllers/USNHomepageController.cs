﻿using CSTemplate.Code.Models.USNModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Components;
using CSTemplate.Code.Models.Home;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.JuryMember;
using CSTemplate.Code.Services.Awards;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using CSTemplate.Code.Services.Juries;
using CSTemplate.Code.Services.Photos;
using CSTemplate.Code.Services.Prizes;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class USNHomepageController : USNBaseController
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetAllJuriesService _getAllJuriesService;
        private readonly IGetAllPrizesListingService _getAllPrizesListingService;
        private readonly IGetAwardEndedDateService _getAwardEndedDateService;
        private readonly IGetFeaturedPhotosService _getFeaturedPhotosService;

        public USNHomepageController(
            IUmbracoHelperService umbracoHelperService,
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetAllJuriesService getAllJuriesService,
            IGetAllPrizesListingService getAllPrizesListingService,
            IGetAwardEndedDateService getAwardEndedDateService,
            IGetFeaturedPhotosService getFeaturedPhotosService
            )
        {
            _getFeaturedPhotosService = getFeaturedPhotosService;
            _getAwardEndedDateService = getAwardEndedDateService;
            _getAllPrizesListingService = getAllPrizesListingService;
            _getAllJuriesService = getAllJuriesService;

            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _umbracoHelperService = umbracoHelperService;
        }

        public virtual ActionResult HomePage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            HomePageCustomModel homePageModel = new HomePageCustomModel(model.Content);
            var member=Services.MemberService.GetAllMembers().ToList();
            //DateTime currentTime = DateTime.Now;
            //DateTime deadLine = DateTime.Now.AddDays(2).AddHours(1).AddMinutes(2);
            //these will be coming from a service eventaully, after the frontend will set them as dummy data
            homePageModel.CustomComponents = new List<CustomComponentModel>()
            {
                //example
                new CustomComponentModel()
                {
                    DocumentTypeAlias = "allJuryListingComponentData",
                    PartialViewUrl = "ListingJury/_ListingJuryMembers",
                    Content = _getAllJuriesService.Get(umbracoHelper)
                    #region Dummy Data

                    

                   
                    //Content = new _ListingJuryMembersModel()
                    //{
                    //    SectionTitle = "Jury",
                    //    SectionText =  "These are the judges adjudicating the awards",

                    //    #region DummyData
                    //    ListofJudges = new List<_JuryMemberModel>()
                    //    {
                    //        new _JuryMemberModel()
                    //        {
                    //            JudgeName = "John Smith",
                    //            JudgePosition = "CEO",
                    //            JudgeDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies, eros id porttitor rutrum, lacus felis dictum lectus, et condimentum lacus felis sed justo.",
                    //            JudgeImgURL = "dist/assets/img/boy.jpg",
                    //        },
                    //        new _JuryMemberModel()
                    //        {
                    //            JudgeName = "John Smith",
                    //            JudgePosition = "CEO",
                    //            JudgeDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies, eros id porttitor rutrum, lacus felis dictum lectus, et condimentum lacus felis sed justo.",
                    //            JudgeImgURL = "dist/assets/img/boy.jpg",
                    //        },
                    //        new _JuryMemberModel()
                    //        {
                    //            JudgeName = "John Smith",
                    //            JudgePosition = "CEO",
                    //            JudgeDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies, eros id porttitor rutrum, lacus felis dictum lectus, et condimentum lacus felis sed justo.",
                    //            JudgeImgURL = "dist/assets/img/boy.jpg",
                    //        },
                    //        new _JuryMemberModel()
                    //        {
                    //            JudgeName = "John Smith",
                    //            JudgePosition = "CEO",
                    //            JudgeDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ellamcorper suscipit.",
                    //            JudgeImgURL = "dist/assets/img/boy.jpg",
                    //        },
                    //        new _JuryMemberModel()
                    //        {
                    //            JudgeName = "Tanika CaRUANA",
                    //            JudgePosition = "Managing Director",
                    //            JudgeDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ellamcorper suscipit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    //            JudgeImgURL = "dist/assets/img/boy.jpg",
                    //        },
                    //        new _JuryMemberModel()
                    //        {
                    //            JudgeName = "Peter Abela",
                    //            JudgePosition = "Founder",
                    //            JudgeDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies, eros id porttitor rutrum, lacus felis dictum lectus, et condimentum lacus felis sed justo.",
                    //            JudgeImgURL = "dist/assets/img/boy.jpg",
                    //        }
                    //    }

                    //    #endregion
                    //}
                    #endregion
                },
                new CustomComponentModel()
                {
                    DocumentTypeAlias = "featuredPhotosCarouselComponentData",
                    PartialViewUrl = "FeaturedPhotos/_ListingFeaturedPhotos",
                    Content = _getFeaturedPhotosService.Get(umbracoHelper)
                    #region Dummy Data

                    //Content = new _ListingFeaturedPhotosModel()
                    //{
                    //    SectionTitle = "Featured Photos",
                    //    SummitedPhotosButtontext = "VIEW ALL SUBMITED PHOTOS",
                    //    #region DummyData
                    //    ListOfPhotos = new List<_ItemPhotoModel>()
                    //    {
                    //        new _ItemPhotoModel()
                    //        {
                    //            ItemPhotoDate = currentTime,
                    //            ItemPhotoImgURL = "/dist/assets/img/1.jpg",
                    //            ItemPhotoName = "John Smith",
                    //            ItemPhotoAward = "Landscape photography Award",
                    //            ItemPhotoLocation = "Vence, Italy",
                    //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
                    //            ItemPhotoCategory = "nature"
                    //        },
                    //        new _ItemPhotoModel()
                    //        {
                    //            ItemPhotoDate = currentTime,
                    //            ItemPhotoImgURL = "/dist/assets/img/2.jpg",
                    //            ItemPhotoName = "John Smith",
                    //            ItemPhotoAward = "Landscape photography Award",
                    //            ItemPhotoLocation = "Vence, Italy",
                    //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
                    //            ItemPhotoCategory = "nature"
                    //        },
                    //        new _ItemPhotoModel()
                    //        {
                    //            ItemPhotoDate = currentTime,
                    //            ItemPhotoImgURL = "/dist/assets/img/3.jpg",
                    //            ItemPhotoName = "John Smith",
                    //            ItemPhotoAward = "Landscape photography Award",
                    //            ItemPhotoLocation = "Vence, Italy",
                    //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
                    //        },
                    //        new _ItemPhotoModel()
                    //        {
                    //            ItemPhotoDate = currentTime,
                    //            ItemPhotoImgURL = "/dist/assets/img/4.jpg",
                    //            ItemPhotoName = "John Smith",
                    //            ItemPhotoAward = "Landscape photography Award",
                    //            ItemPhotoLocation = "Vence, Italy",
                    //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
                    //            ItemPhotoCategory = "nature"
                    //        },
                    //        new _ItemPhotoModel()
                    //        {
                    //            ItemPhotoDate = currentTime,
                    //            ItemPhotoImgURL = "/dist/assets/img/5.jpg",
                    //            ItemPhotoName = "John Smith",
                    //            ItemPhotoAward = "Landscape photography Award",
                    //            ItemPhotoLocation = "Vence, Italy",
                    //            ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
                    //            ItemPhotoCategory = "war"
                    //        }
                    //    }
                        
                    //    #endregion
                    //}

                    #endregion
                    
                },
                new CustomComponentModel()
                {
                    DocumentTypeAlias = "allListingPrizesPageData",
                    PartialViewUrl = "AllListingPrizes/_AllListingPrizes",
                    Content = _getAllPrizesListingService.Get(umbracoHelper)
                    #region Dummy Data
                    //Content = new _AllListingPrizesModel()
                    //{
                    //    SectionTitle = "Prizes",
                    //    SectionText = "These are the great prices you can win by participating",
                    //    ListOfPrizes =  new List<_PrizeItemModel>()
                    //    {
                    //        new _PrizeItemModel()
                    //        {
                    //            PrizeItemImgURL = "/dist/assets/img/money.png",
                    //            PrizeItemTitle =  "€1000",
                    //            PrizeItemText = "In cash"

                    //        },new _PrizeItemModel()
                    //        {
                    //            PrizeItemImgURL = "/dist/assets/img/camera.png",
                    //            PrizeItemTitle =  "Canon Camera",
                    //            PrizeItemText = "Including accessories"

                    //        },new _PrizeItemModel()
                    //        {
                    //            PrizeItemImgURL = "/dist/assets/img/tripod.png",
                    //            PrizeItemTitle =  "Camera Tripod",
                    //            PrizeItemText = "Including difderent heights"

                    //        }
                    //    }
                    //}
                    

                    #endregion
                    

                },
                new CustomComponentModel()
                {
                    DocumentTypeAlias = "submitAwardCountdownComponentData",
                    PartialViewUrl = "SubmitAwardCountdown/_SubmitAwardCountdown",
                    Content = _getAwardEndedDateService.Get(umbracoHelper)
                    #region Dummy Data

                    //Content = new _SubmitAwardCountdownModel()
                    //{
                    //    SectionTitle = "Time Left To Submit Your Application",
                    //    CountdownDaysText = "Days",
                    //    CountdownHoursText = "Hours",
                    //    CountdownMinText = "Min",
                    //    BackgroundImgUrl = "dist/assets/img/countdownbackground.jpg",
                    //    DeadlineDateTime = deadLine.ToString(CultureInfo.InvariantCulture)
                    //}

                    #endregion
                  

                }


            };
            
            return base.Populate(homePageModel);
        }
    }
}