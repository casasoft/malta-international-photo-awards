﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared
{
    public class _AllListingPrizesModel
    {
        public string SectionTitle { get; set; }
        public string SectionText { get; set; }

        //todo: [For: Backend | 2018/09/06] We have added a button (WrittenBy: Radek)        			
        public string ButtonText { get; set; }
        public string ButtonURL { get; set; }
        public string ButtonTarget { get; set; }
        public List<_PrizeItemModel> ListOfPrizes { get; set; }
    }
}
