﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Payment;
using DevTrends.MvcDonutCaching;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers.Payment
{
    public partial class PaymentPageSurfaceController : SurfaceController
    {
        private readonly UmbracoHelper _umbracoHelper;
        private readonly IProcessPaymentResponseService _processPaymentResponseService;
        private readonly IProcessPaymentRedirectionService _processPaymentRedirectionService;
        private readonly IUmbracoHelperService _umbracoHelperService;

        public PaymentPageSurfaceController(IUmbracoControllerWrapperService
            umbracoControllerWrapperService,
            IProcessPaymentResponseService
            processPaymentResponseService,
            IProcessPaymentRedirectionService
            processPaymentRedirectionService,
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
            _processPaymentRedirectionService = processPaymentRedirectionService;
            _processPaymentResponseService = processPaymentResponseService;
            _umbracoHelper = umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
        }

        [HttpPost]
        public virtual ActionResult ProceedToPayment(string orderRefId, decimal subTotalPrice, string transactionDescription, string clientEmailAddress)
        {
            var paymentPageId = _umbracoHelperService.LoadRootNodeForCurrentDomain(_umbracoHelper).Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Payment_Page_Document_Type_Alias)).FirstOrDefault().Id;

            TempData[ConstantValues.TempDataPaymentOrderRefId] = orderRefId;
            TempData[ConstantValues.TempDataPaymentPaymentAmount] = subTotalPrice.ToString();
            TempData[ConstantValues.TempDataPaymentTransactionDescription] = transactionDescription;
            TempData[ConstantValues.TempDataPaymentClientEmailAddress] =  clientEmailAddress;

            return RedirectToUmbracoPage(paymentPageId);
        }

        public virtual ActionResult InvokeFastPay(decimal paymentAmount, string clientEmailAddress, string orderRefId,
         string transactionDescription)
        {
            String transactionXmlString = "";
            String redirectionUrl = CS.Common.Umbraco.Helpers
                .UrlHelpers.GetAbsoluteUrlWithTheCorrentProtocol("/payment/PaymentRedirectionUrl");
            String statusUrl = CS.Common.Umbraco.Helpers
                .UrlHelpers.GetAbsoluteUrlWithTheCorrentProtocol("/payment/paymentsuccessfullstatusUrl");
            String failedRedirectionUrl = CS.Common.Umbraco.Helpers
                .UrlHelpers.GetAbsoluteUrlWithTheCorrentProtocol("/payment/paymentfailedUrl"); 

            String secretWord = ConstantValues.ApcoHashingSW;
            String profileID = ConstantValues.ApcoProfileId;
            String actionType = "1";
            String value = paymentAmount.ToString();
            String currency = "978";
            String email = clientEmailAddress;
            String language = "en";
            String orderReference = orderRefId;
            String udf1 = transactionDescription;
            String udf2 = "";
            String udf3 = "";
            String cssTemplate = "Default";
            //build the xml string of the Transaction
            transactionXmlString = "<Transaction hash=\"" + secretWord + "\">";
            transactionXmlString += "<RedirectionURL>" + redirectionUrl + "</RedirectionURL>";
            transactionXmlString += "<status_url urlEncode=\"true\">" + statusUrl + "</status_url>";
            transactionXmlString += "<FailedRedirectionURL>" + failedRedirectionUrl + "</FailedRedirectionURL>";
            transactionXmlString += "<ProfileID>" + profileID + "</ProfileID>";
            transactionXmlString += "<ActionType>" + actionType + "</ActionType>";
            transactionXmlString += "<Value>" + value + "</Value>";
            transactionXmlString += "<Curr>" + currency + "</Curr>";
            transactionXmlString += "<Email>" + email + "</Email>";
            transactionXmlString += "<Lang>" + language + "</Lang>";
            transactionXmlString += "<Enc />";
            transactionXmlString += "<ORef>" + orderReference + "</ORef>";
            transactionXmlString += "<UDF1>" + udf1 + "</UDF1>";
            transactionXmlString += "<UDF2>" + udf2 + "</UDF2>";
            transactionXmlString += "<UDF3>" + udf3 + "</UDF3>";
            transactionXmlString += "<noRetry />";
            transactionXmlString += "<ShowSSLPadlock />";
            transactionXmlString += "<ShowTermsCond />";
            transactionXmlString += "<showDesc />";
            transactionXmlString += "<CSSTemplate>" + cssTemplate + "</CSSTemplate>";
            transactionXmlString += "</Transaction>";

            XmlTextReader xmlTextRead = new XmlTextReader(new StringReader(transactionXmlString));
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(xmlTextRead);
            String md5HashedXml = CS.Common.Helpers.TextHelpers.GetMd5Hash(xmlDoc.InnerXml);

            //Dim SecretWord As String = XMLDoc.ChildNodes(0).Attributes("hash").Value
            xmlDoc.ChildNodes[0].Attributes["hash"].Value = md5HashedXml;

            var URL = "https://www.apsp.biz/Pay/FP5A/Checkout.aspx";

            var XMLData = HttpUtility.HtmlEncode(xmlDoc.InnerXml);


            System.Collections.Specialized.NameValueCollection Inputs = new System.Collections.Specialized.NameValueCollection();

            Inputs.Add("params", XMLData);

            StringBuilder sb = new StringBuilder();

            sb.Append("<html><head>");
            sb.Append(String.Format("</head><body onload=\"document.form1.submit()\">"));
            sb.Append(String.Format("<form name=\"form1\" method=\"post\" action=\"{0}\" >", URL));
            sb.Append(String.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", Inputs.Keys[0], Inputs[Inputs.Keys[0]]));
            sb.Append("</form>");
            sb.Append("</body></html>");

            return Content(sb.ToString());
        }

        [ValidateInput(false)]
        public virtual ActionResult PaymentSuccessfullStatusUrl()
        {

            //log
            LogHelper.Info<PaymentPageSurfaceController>("Entered on PaymentSuccessfullStatusUrl");

            XmlDocument objParamsXml = new XmlDocument();

            try
            {
                //retrieve the fastpay response DomDocument
                objParamsXml = getParamsAndConvertToXmlDocumentForStatusUrl();

                //get the Result of the transaction
                if (objParamsXml.GetElementsByTagName("Result").Count > 0)
                {
                    LogHelper.Info<PaymentPageSurfaceController>("Processing Payment");
                    _processPaymentResponseService.Process(_umbracoHelper,
                        objParamsXml);
                }
                else
                {
                    //Result element not found - throw exception
                    throw new Exception("Result element not found in the [objFastPayXml] XML");
                }
            }
            catch (Exception ex)
            {
                //exception occured
                Response.Write(ex.Message);
            }

            return Content("");
        }

        [ValidateInput(false)]
        public virtual ActionResult PaymentRedirectionUrl()
        {
            //StreamReader stream = new StreamReader(Request.InputStream);
            //string x = stream.ReadToEnd();
            //string xml = HttpUtility.UrlDecode(x);

            XmlDocument objFastPayXml = new XmlDocument();
            try
            {
                LogHelper.Info<PaymentPageSurfaceController>("Entered on PaymentRedirectionUrl");

                //retrieve the fastpay response DomDocument
                objFastPayXml = getParamsAndConvertToXmlDocument();
                //validate XML
                String resultValue = objFastPayXml.GetElementsByTagName("Result")[0].InnerText;
                //check if result returned
                if (resultValue.Length > 0)
                {
                    var urlToBeRedirectTo = _processPaymentRedirectionService.Process(_umbracoHelper,
                        objFastPayXml);

                    //result returned
                    switch (resultValue.ToUpper().Trim())
                    {
                        case "OK":
                            //DO something: NOTOK -> The transaction was successful
                            Response.Write("OK");
                            Response.Write("<script>top.location='"
                                + urlToBeRedirectTo + "';parent.location='" + urlToBeRedirectTo + "';</script>");

                            var cacheManager = new OutputCacheManager();
                            cacheManager.RemoveItems();

                            break;
                        case "NOTOK":
                            //DO something: NOTOK -> The transaction was not successful
                            Response.Write("NOTOK");
                            Response.Write("<script>top.location='" + urlToBeRedirectTo
                                + "';parent.location='" + urlToBeRedirectTo + "';</script>");
                            break;
                        case "DECLINED":
                            //DO something: DECLINED -> The transaction was not successful
                            Response.Write("DECLINED messge");
                            Response.Write("<script>top.location='" + urlToBeRedirectTo
                                + "';parent.location='" + urlToBeRedirectTo + "';</script>");
                            break;
                        case "PENDING":
                            //DO something: PENDING -> The transaction is still pending
                            Response.Write("PENDING messge");
                            Response.Write("<script>top.location='" + urlToBeRedirectTo
                                + "';parent.location='" + urlToBeRedirectTo + "';</script>");
                            break;
                        case "CANCEL":
                            //DO something: CANCEL -> The transaction is cancelled
                            Response.Write("CANCEL messge");
                            Response.Write("<script>top.location='" + urlToBeRedirectTo
                                + "';parent.location='" + urlToBeRedirectTo + "';</script>");
                            break;
                        default:
                            //DO something: OTHER RESULT
                            break;
                    }
                }
                else
                {
                    //ERROR: invalid result
                    throw new Exception("Invalid Result on FastPay");
                }
            }
            catch (Exception ex)
            {
                //exception occured
                Response.Write(ex.Message);
            }
            finally
            {
                //clean objects
                objFastPayXml = null;
            }

            return Content("");
        }

        public virtual ActionResult PaymentFailed()
        {
            LogHelper.Info<PaymentPageSurfaceController>("Entered on PaymentFailed");

            return RedirectToUmbracoPage(_umbracoHelperService.LoadRootNodeForCurrentDomain(_umbracoHelper).Id);
        }

        private XmlDocument getParamsAndConvertToXmlDocumentForStatusUrl()
        {
            LogHelper.Info<PaymentPageSurfaceController>("Entered Get Params and Convert To XML Document For Status Url");
            if (Request != null)
            {
                XmlDocument objParamsXml = new XmlDocument();
                LogHelper.Info<PaymentPageSurfaceController>("Defined XML Document");
                try
                {
                    LogHelper.Info<PaymentPageSurfaceController>("Entered Try");
                    //check if the params object has been passed in the POST
                    String strParamsVal = "";
                    LogHelper.Info<PaymentPageSurfaceController>("Defined strParamsVal Parameter");
                    LogHelper.Info<PaymentPageSurfaceController>("Try to read Request.Form(params)");

                    if (Request.Form != null)
                    {
                        if (!String.IsNullOrEmpty(Request.Params["params"]))
                        {
                            LogHelper.Info<PaymentPageSurfaceController>("Params Found");
                            //found in POST
                            strParamsVal = Server.UrlDecode(Request.Params["params"]);
                            LogHelper.Info<PaymentPageSurfaceController>("Found in POST: " + strParamsVal);
                        }
                        else
                        {
                            //not found - throw exception
                            LogHelper.Error<PaymentPageSurfaceController>("Could not retrieve the result XML from the POST!!! Request.Params",
                                new Exception("Could not retrieve the result XML from the POST!!!! - Request.Params"));
                            throw new Exception("Could not retrieve the result XML from the POST!!!! Request.Params");
                        }
                        if (!String.IsNullOrEmpty(strParamsVal))
                        {
                            //convert from string to XML document
                            objParamsXml.LoadXml(strParamsVal);
                        }
                    }
                    else
                    {
                        LogHelper.Error<PaymentPageSurfaceController>("Request Form is null",
                                  new Exception("Request Form is null"));

                    }

                }
                catch (Exception ex)
                {
                    //error occured, throw exception
                    LogHelper.Error<PaymentPageSurfaceController>("Error when trying to get Params", ex);
                    throw new Exception("Error when trying to get Params :" + ex.Message);
                }

                //return xml params object
                LogHelper.Info<PaymentPageSurfaceController>("Return objParamsXML");
                return objParamsXml;
            }
            else
            {
                LogHelper.Info<PaymentPageSurfaceController>("Null Request");
            }

            return null;

        }

        /// <summary>
        /// Collects the response from FastPay and converts to DomDocument(XML)
        /// </summary>
        /// <returns>Returns an XML DomDocument object with the response of FastPay</returns>
        private XmlDocument getParamsAndConvertToXmlDocument()
        {
            XmlDocument objParamsXml = new XmlDocument();
            try
            {
                String strParamsVal = "";
                if (!String.IsNullOrEmpty(Request.QueryString["params"]))
                {
                    //found in GET
                    strParamsVal = Server.UrlDecode(Request.QueryString["params"]);
                }
                else
                {
                    //not found - throw exception
                    throw new Exception("Could not retrieve the result XML from the GET!!!!");
                }
                if (!String.IsNullOrEmpty(strParamsVal))
                {
                    //convert from string to XML document
                    objParamsXml.LoadXml(strParamsVal);
                }
            }
            catch (Exception ex)
            {
                //error occured, throw exception
                throw ex;
            }
            //return xml params object
            return objParamsXml;
        }
    }
}