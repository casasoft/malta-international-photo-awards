﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.OrderItems;
using CSTemplate.Code.Services.Orders;

namespace CSTemplate.Code.Services.Feedbacks
{

    public interface ICheckSubmissionFeedbackPaymentStatusFilterBySubmissionIdService
    {
        bool Check(int submissionId);
    }

    [Service]
    public class CheckSubmissionFeedbackPaymentStatusFilterBySubmissionIdService : ICheckSubmissionFeedbackPaymentStatusFilterBySubmissionIdService
    {
        private readonly IGetSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService _getSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService;
        private readonly IGetOrderDetailsFilterByOrderId _getOrderDetailsFilterByOrderId;

        public CheckSubmissionFeedbackPaymentStatusFilterBySubmissionIdService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService, IGetSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService getSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService,
            IGetOrderDetailsFilterByOrderId getOrderDetailsFilterByOrderId
            )
        {
            _getSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService = getSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService;
            _getOrderDetailsFilterByOrderId = getOrderDetailsFilterByOrderId;
        }
        public bool Check(int submissionId)
        {
            var status = false;
            if (submissionId <= 0) return false;
            var submissionOrderItemList = _getSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService.Get(submissionId);
            foreach (var orderItems in submissionOrderItemList)
            {
                var conditionOne = (orderItems.PayForSubmission == true && orderItems.PayForFeedback == true);
                var conditionTwo = orderItems.PayForFeedback == true;
                var orderId = orderItems.OrderId;
                if (conditionOne || conditionTwo)
                {
                    var order = _getOrderDetailsFilterByOrderId.Get(orderId);
                    if (order.PaymentStatusEnumId==Enums.PaymentStatus.OK)
                    {
                        status = true;
                    }
                }
            }
            return status;
           
        }
    }
}
