﻿using CS.Common.Services;
using CSTemplate.Code.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;
using static CSTemplate.Code.Constants.Enums;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Users;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Models.Pocos;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Photos;
using CS.Common.Helpers;

namespace CSTemplate.Code.Services.Submissions
{
    public interface IGetSubmissionFilterBySubmissionOwnerNameAndSurnameService
    {
        List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, string nameSurname, SubmissionFilters shortListFilters, ShortListedStatusFilter shortListedStatus);
    }
    [Service]
    public class GetSubmissionFilterBySubmissionOwnerNameAndSurnameService : IGetSubmissionFilterBySubmissionOwnerNameAndSurnameService
    {
        private readonly IGetUserFilterByNameAndSurnameService _getUserFilterByNameAndSurnameService;
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetFirstPhotoUrlFilterBySubmissionIdService _getFirstPhotoUrlFilterBySubmissionIdService;
        public GetSubmissionFilterBySubmissionOwnerNameAndSurnameService(
            IGetUserFilterByNameAndSurnameService getUserFilterByNameAndSurnameService, 
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService, 
            IUmbracoHelperService umbracoHelperService,   
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService,
            IGetFirstPhotoUrlFilterBySubmissionIdService getFirstPhotoUrlFilterBySubmissionIdService)
        {
            _getUserFilterByNameAndSurnameService = getUserFilterByNameAndSurnameService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
            _getFirstPhotoUrlFilterBySubmissionIdService = getFirstPhotoUrlFilterBySubmissionIdService;
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _umbracoHelperService = umbracoHelperService;
        }
        public List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, string nameSurname, SubmissionFilters shortListFilters, ShortListedStatusFilter shortListedStatus)
        {
            var listOfItemPhotoModel=new List<_ItemPhotoModel>();
            if (string.IsNullOrEmpty(nameSurname)) return listOfItemPhotoModel;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var submissionsTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = Constants.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = Constants.DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = Constants.DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = Constants.DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionTitleField = DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionShortListedEnumIdField = DatabaseConstantValues.Submissions_Short_Listed_Enum_Id_Field_Name;
            var submissionShortListedDateField = DatabaseConstantValues.Submissions_Short_Listed_Date_Field_Name;
            var submissionStatusEnumIdField = DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;


            var featuredFieldId = Constants.DatabaseConstantValues.Photos_Featured_Field_Name;


          
            var users=_getUserFilterByNameAndSurnameService.Get(umbracoHelper, nameSurname);

            foreach (var user in users)
            {

                var query = $@" 
                     SELECT
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId},
                        {submissionsTableAlias}.{submissionTitleField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionShortListedEnumIdField},
                        {submissionsTableAlias}.{submissionStatusEnumIdField}

                            FROM
                                {submissionsTableName}
                                   WHERE 
                                    (
                                        {submissionsTableAlias}.{submissionShortListedEnumIdField} = {(int)shortListedStatus}
                                    )
                                        AND
                                        {submissionsTableAlias}.{submissionsUserId} = {user.MemberId}
                                        AND 
                                          ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                                        AND {submissionsTableAlias}.{submissionShortListedDateField} IS NULL
                                    
            ";
               
                var orderByExpression = " ORDER BY {0} {1} ";
                var orderByDesc = Constants.DatabaseConstantValues.ORDER_BY_DESC;
                var orderByAsc = Constants.DatabaseConstantValues.ORDER_BY_ASC;
                switch (shortListFilters)
                {
                    case CSTemplate.Code.Constants.Enums.SubmissionFilters.DateNewestFirst:
                        query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionsDate}", orderByDesc);
                        break;
                    case CSTemplate.Code.Constants.Enums.SubmissionFilters.DateOldestFirst:
                        query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionsDate}", orderByAsc);
                        break;
                    case CSTemplate.Code.Constants.Enums.SubmissionFilters.SubmissionNameAscending:
                        query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionTitleField}", orderByAsc);
                        break;
                    case CSTemplate.Code.Constants.Enums.SubmissionFilters.SubmissionNameDescending:
                        query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionTitleField}", orderByDesc);
                        break;

                }
                var sqlResult = umbracoDb.Query<SubmissionPoco>(query).ToList();
                foreach (var submittedPhoto in sqlResult)
                {
                    var statusEnum = (CSTemplate.Code.Constants.Enums.SubmissionStatus)submittedPhoto.StatusEnumID;
                    var status = EnumHelpers.GetDescriptionAttributeValue(statusEnum);
                    var awardCategory = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, submittedPhoto.CategoryId);
                    var photographerInfos = user;
                    var photographerFullName = photographerInfos.Name + " " + photographerInfos.Surname;
                    listOfItemPhotoModel.Add(new _ItemPhotoModel
                    {
                        ItemPhotoImgURL = _getFirstPhotoUrlFilterBySubmissionIdService.Get(submittedPhoto.Id),
                        ItemPhotoDate = submittedPhoto.Date,
                        ItemPhotoName = photographerFullName,
                        ItemPhotoAward = awardCategory.AwardItem.AwardItemName,
                        ItemPhotoLocation = submittedPhoto.Location,
                        ItemPhotoDescription = submittedPhoto.Description,
                        ItemPhotoCategory = awardCategory.AwardCategoryItemName,
                        ItemPhotoTitle = submittedPhoto.Title,
                        SubmissionId = submittedPhoto.Id.ToString(),
                        ShowShortlistRadioButtons = true,
                        SubmissionStatusTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, CSTemplate.Code.Constants.Enums.DictionaryKey.Submission_Status_Title_Text),
                        //todo: [For: FrontEnd | 2018/08/27] ListOfRadiosValues Has To Be SelectedListItem (WrittenBy: Fatih)        			
                        ListOfRadiosValues = Enum.GetValues(typeof(Enums.ShortListedStatus)).Cast<Enums.ShortListedStatus>().ToList(),
                        ChosenRadioValue = Enums.ShortListedStatus.NotYet,
                        CurrentSubmissionStatus = status.ToString()


                    });
                }
            }

            return listOfItemPhotoModel;

        }
    }
}
