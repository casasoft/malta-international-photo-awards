﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.ForgotPassword;
using CSTemplate.Code.Models.Shared;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ForgotPasswordPageDataController : USNBaseController
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;

        public ForgotPasswordPageDataController(IUmbracoControllerWrapperService
            umbracoControllerWrapperService,
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _umbracoHelperService = umbracoHelperService;
        }

        // GET: ForgotPasswordPageData
        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult ForgotPasswordPage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            ForgotPasswordPageModel forgotPasswordPageModel = new ForgotPasswordPageModel(model.Content) {};

            forgotPasswordPageModel.ForgotPasswordForm = new _ForgotPasswordFormModel()
            {
                Message = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Forgot_Password_Description_Text),
                ButtonFormText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Forgot_Password_Button_Text),
                Required = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    Enums.DictionaryKey.Dictionary_Form_Required_Text),
            };

            return base.Populate(forgotPasswordPageModel);
        }
    }
}