﻿using CS.Common.Attributes;
using CS.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Helpers
{
    public static class EnumHelpers
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<DisplayAttribute>()
                            .GetName();
        }

        private static string _getEnumValueName(object value,
            bool addSpacesToCamelCasedName,
            bool convertUnderscoresToSpaces = true)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            string s = value.ToString();
            if (addSpacesToCamelCasedName)
                s = TextHelpers.AddSpacesToCamelCasedText(s);

            if (convertUnderscoresToSpaces)
            {
                //s = s.Replace("_", " ");
                s = s.Replace("_", " > ");
            }
            return s;
        }

        public static string GetDescriptionAttributeValue(object value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi != null)
            {
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    return attributes[0].Description;
                }
            }
            return null;
        }

        private static string _stringValueOf(object value,
            bool addSpacesToCamelCasedName,
            bool getFromDescriptionAttributeIfAvailable,
            bool convertUnderscoresToSpaces)
        {
            string s = null;
            if (value != null)
            {
                if (getFromDescriptionAttributeIfAvailable)
                {
                    var description = GetDescriptionAttributeValue(value);

                    if (!string.IsNullOrWhiteSpace(description))
                    {
                        return description;
                    }
                }
                s = _getEnumValueName(value, addSpacesToCamelCasedName, convertUnderscoresToSpaces);
            }
            return s;
        }

        private static string stringValueOf<TEnum>(TEnum value,
            bool addSpacesToCamelCasedName = false,
            bool getFromDescriptionAttributeIfAvailable = true,
            bool convertUnderscoresToSpaces = true)
        {
            return _stringValueOf(value,
                addSpacesToCamelCasedName,
                getFromDescriptionAttributeIfAvailable,
                convertUnderscoresToSpaces);
        }

        private static int priorityValueOf<TEnum>(this TEnum value, int defaultValue = 0)
        {
            return priorityValueOfNullable(value) ?? defaultValue;
        }

        private static int? priorityValueOfNullable<TEnum>(this TEnum value)
        {

            if (value != null)
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                if (fi != null)
                {
                    PriorityAttribute[] attributes = (PriorityAttribute[])fi.GetCustomAttributes(typeof(PriorityAttribute), false);
                    if (attributes.Length > 0)
                    {
                        return attributes[0].Priority;
                    }
                }

            }
            return null;
        }

        private static int enumPriorityComparer<TEnum>(TEnum itemA, TEnum itemB)
        {
            return priorityValueOf(itemA).CompareTo(priorityValueOf(itemB));
        }

        private static int enumStringValueComparerAsc(Enum itemA, Enum itemB)
        {
            return System.String.Compare(stringValueOf(itemA), stringValueOf(itemB), System.StringComparison.Ordinal);
        }

        private static int enumStringValueComparerAsc<TEnum>(TEnum itemA, TEnum itemB)
        {
            return System.String.Compare(stringValueOf(itemA), stringValueOf(itemB), System.StringComparison.Ordinal);
        }

        public static List<Enum> GetListOfEnumValues(Type enumType,
            Enums.EnumsSortBy sortBy = Enums.EnumsSortBy.NameAscending,
            Func<object, int> customEnumPriorityHandler = null)
        {
            List<Enum> list = new List<Enum>();
            bool allowsNullableValue = false;

            if (Helpers.ReflectionHelpers.CheckIfTypeIsNullable(enumType))
            {
                allowsNullableValue = true;
                enumType = enumType.GetGenericArguments()[0];
            }


            Array values = Enum.GetValues(enumType);
            for (int i = 0; i < values.Length; i++)
            {
                var val = values.GetValue(i);
                list.Add((Enum) val);
            }
            if (sortBy == Enums.EnumsSortBy.NameAscending || sortBy == Enums.EnumsSortBy.NameDescending)
            {
                list.Sort(
                    (itemA, itemB) =>
                        enumStringValueComparerAsc(itemA, itemB)*(sortBy == Enums.EnumsSortBy.NameAscending ? 1 : -1));
            }
            else if (sortBy == Enums.EnumsSortBy.PriorityAttributeValue)
            {
                if (customEnumPriorityHandler == null)
                {
                    //Sort by Priority, Title Ascending
                    list =
                        Helpers.ListHelpers.SortByMultipleComparers(list,
                            enumPriorityComparer,
                            enumStringValueComparerAsc).ToList();
                }
                else
                {
                    //Sort by CustomPriority
                    Func<Enum, Enum, int> customEnumToPriorityComparer = (enumA, enumB) =>
                    {
                        int valueA = customEnumPriorityHandler(enumA);
                        int valueB = customEnumPriorityHandler(enumB);
                        return valueA.CompareTo(valueB);
                    };
                    list.Sort((itemA, itemB) => customEnumToPriorityComparer(itemA, itemB));
                }
            }


            if (allowsNullableValue)
            {
                //2012/feb/08 - this was removed (Karl) as it was thought that it doesnt make sense to add the null automatically
                //list.Insert(0, null);
            }


            return list;
        }

        public static List<TEnum> GetListOfEnumValues<TEnum>(Type enumType, Enums.EnumsSortBy sortBy = Enums.EnumsSortBy.NameAscending)
        {
            List<TEnum> list = new List<TEnum>();
            bool allowsNullableValue = false;

            if (Helpers.ReflectionHelpers.CheckIfTypeIsNullable(enumType))
            {
                allowsNullableValue = true;
                enumType = enumType.GetGenericArguments()[0];
            }


            Array values = Enum.GetValues(enumType);
            for (int i = 0; i < values.Length; i++)
            {
                var val = values.GetValue(i);
                list.Add((TEnum)val);
            }
            if (sortBy == Enums.EnumsSortBy.NameAscending || sortBy == Enums.EnumsSortBy.NameDescending)
            {
                list.Sort((itemA, itemB) => enumStringValueComparerAsc(itemA, itemB) * (sortBy == Enums.EnumsSortBy.NameAscending ? 1 : -1));
            }
            else if (sortBy == Enums.EnumsSortBy.PriorityAttributeValue)
            {
                //Sort by Priority, Title Ascending
                list = Helpers.ListHelpers.SortByMultipleComparers<TEnum>(list, enumPriorityComparer, enumStringValueComparerAsc).ToList();
            }


            if (allowsNullableValue)
            {
                //2012/feb/08 - this was removed (Karl) as it was thought that it doesnt make sense to add the null automatically
                //list.Insert(0, null);
            }


            return list;
        }

        public static List<TEnumType> GetListOfEnumValues<TEnumType>(Enums.EnumsSortBy sortBy = Enums.EnumsSortBy.NameAscending)
        {
            var generalLIst = GetListOfEnumValues(typeof(TEnumType), sortBy);

            List<TEnumType> list = new List<TEnumType>();
            foreach (var item in generalLIst)
            {

                list.Add((TEnumType)(object)item);
            }
            return list;
        }


        /// <summary>
        /// Returns an enum, based on the string value.  E.g, if the enum type is DayOfWeek, and the string value is "Monday", that would count. It will even return
        /// enums where the string matches the integer value of the enumeration, e.g "1".
        /// </summary>
        /// <param name="type"></param>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static T ConvertStringToEnum<T>(string stringValue, T defaultValue) where T : struct, IConvertible
        {
            return (T)(object)ConvertStringToEnum(typeof(T), stringValue, (Enum)(object)defaultValue);

        }

        /// <summary>
        /// Returns an enum, based on the string value.  E.g, if the enum type is DayOfWeek, and the string value is "Monday", that would count. It will even return
        /// enums where the string matches the integer value of the enumeration, e.g "1".
        /// </summary>
        /// <param name="type"></param>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static Enum ConvertStringToEnum(Type type, string stringValue, Enum defaultValue)
        {
            var val = ConvertStringToNullableEnum(type, stringValue);
            if (val == null)
            {
                return defaultValue;
            }
            else
            {
                return val;
            }
        }



        public static T ConvertStringToEnum<T>(string enumAsString) where T : struct
        {
            T enumConvertedFromString;
            Enum.TryParse(enumAsString, true, out enumConvertedFromString);

            return enumConvertedFromString;
        }





        public static T? ConvertStringToNullableEnum<T>(string stringValue) where T : struct, IConvertible
        {
            return (T?)(object)ConvertStringToNullableEnum(typeof(T), stringValue);
        }

        public static Enum ConvertStringToNullableEnum(Type type, string stringValue)
        {
            if (stringValue == null) stringValue = "";
            stringValue = stringValue.Trim();
            var enumValues = GetListOfEnumValues(type);
            for (int i = 0; i < enumValues.Count; i++)
            {
                var enumvalue = enumValues[i];
                int numValue = Convert.ToInt32(enumvalue);
                string s = ConvertEnumToString(enumvalue, getFromDescriptionAttributeIfAvailable: true);
                if (string.Compare(s, stringValue, true) == 0 || stringValue.Trim() == numValue.ToString())
                {
                    return enumvalue;
                }
                s = ConvertEnumToString(enumvalue, getFromDescriptionAttributeIfAvailable: false);
                if (string.Compare(s, stringValue, true) == 0 || stringValue.Trim() == numValue.ToString())
                {
                    return enumvalue;
                }
            }
            return null;
        }

        public static int GetGenericEnumValue(Enum enumValue)
        {
            return (int)(object)enumValue;
        }

        /// <summary>
        /// Return the string value of an Enum if the DescriptionAttribute tag is defined (requires System.ComponentModel)
        /// 
        ///[DescriptionAttribute("Food")]
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertEnumToStringByType(Type enumType,
            Enum enumValue,
            bool addSpacesToCamelCasedName = false,
            bool getFromDescriptionAttributeIfAvailable = true,
            bool convertUnderscoresToSpaces = true)
        {
            string s = null;
            if (enumValue != null)
            {
                if (getFromDescriptionAttributeIfAvailable)
                {
                    FieldInfo fi = enumType.GetField(enumValue.ToString());

                    if (fi != null)
                    {
                        DescriptionAttribute[] attributes =
                            (DescriptionAttribute[]) fi.GetCustomAttributes(typeof (DescriptionAttribute), false);
                        if (attributes.Length > 0)
                        {
                            return attributes[0].Description;
                        }
                    }
                }
                s = _getEnumValueName(enumValue, addSpacesToCamelCasedName, convertUnderscoresToSpaces);
            }
            return s;
        }

        public static bool CheckEnumValueContainsBitwiseFlag(Enum value, Enum flag)
        {
            int nValue = (int)(object)value;
            int nFlag = (int)(object)flag;
            int val = nValue & nFlag;
            return val == 1;
        }

        /// <summary>
        /// Return the string value of an Enum if the DescriptionAttribute tag is defined (requires System.ComponentModel)
        /// 
        ///[DescriptionAttribute("Food")]
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertEnumToString<T>(T value, bool addSpacesToCamelCasedName = false, bool getFromDescriptionAttributeIfAvailable = true)
        {
            if (value != null)
            {
                Type t = value.GetType();
                Enum enumValue = null;
                if (value is Enum)
                {
                    enumValue = (Enum)(object)value;
                }
                return ConvertEnumToStringByType(t, enumValue, addSpacesToCamelCasedName, getFromDescriptionAttributeIfAvailable);
            }
            else
            {
                return null;
            }
        }
    }
}
