﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.ChangePassword;
using CSTemplate.Code.Services.ChangePasswordPage;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ChangePasswordPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetChangePasswordPageService _getChangePasswordPageService;
        public ChangePasswordPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetChangePasswordPageService getChangePasswordPageService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getChangePasswordPageService = getChangePasswordPageService;
        }

        // GET: ChangePasswordPageData
        public virtual ActionResult ChangePasswordPage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            ChangePasswordPageModel changePasswordPageModel = _getChangePasswordPageService.Get(umbracoHelper, model.Content);

            return base.Populate(changePasswordPageModel);
        }
    }
}