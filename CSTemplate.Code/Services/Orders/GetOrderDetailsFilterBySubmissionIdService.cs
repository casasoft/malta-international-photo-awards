﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.OrderItems;

namespace CSTemplate.Code.Services.Orders
{

    public interface IGetOrderDetailsFilterBySubmissionIdService
    {
        OrderPoco Get(int submissionId);
    }

    [Service]
    public class GetOrderDetailsFilterBySubmissionIdService : IGetOrderDetailsFilterBySubmissionIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService _getSubmissionOrderItemFilterByOrderItemSubmissionIdService;

        public GetOrderDetailsFilterBySubmissionIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IGetSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService getSubmissionOrderItemFilterByOrderItemSubmissionIdService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
            _getSubmissionOrderItemFilterByOrderItemSubmissionIdService = getSubmissionOrderItemFilterByOrderItemSubmissionIdService;
        }
        public OrderPoco Get(int submissionId)
        {
            var orderPoco = new OrderPoco();
            if (submissionId <= 0) return orderPoco;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var orderTable = DatabaseConstantValues.OrdersTableName;
            var orderPrimaryKeyField = DatabaseConstantValues.Orders_Poco_Primary_Key_Value;
            var orderItem = _getSubmissionOrderItemFilterByOrderItemSubmissionIdService.Get(submissionId);
            //var orderId = orderItem.OrderId;
            //var orderDetail = umbracoDb.Query<OrderPoco>($"SELECT * FROM {orderTable} WHERE {orderPrimaryKeyField} = {orderId}").FirstOrDefault();
            //return orderDetail;
            return null;
        }
    }
}
