﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.OrderHistory;
using CSTemplate.Code.Services.OrderHistory;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class OrderHistoryPageDataController : USNBaseController
    {

        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetOrderHistoryPageService _getOrderHistoryPageService;

        public OrderHistoryPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetOrderHistoryPageService getOrderHistoryPageService
        )
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getOrderHistoryPageService = getOrderHistoryPageService;
        }
        // GET: OrderHistoryPageData
        public virtual ActionResult OrderHistoryPage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var userId = Members.GetCurrentMemberId();
            OrderHistoryPageModel orderHistoryPageModel = _getOrderHistoryPageService.Get(umbracoHelper, model.Content,userId);
            return base.Populate(orderHistoryPageModel);
        }
    }
}