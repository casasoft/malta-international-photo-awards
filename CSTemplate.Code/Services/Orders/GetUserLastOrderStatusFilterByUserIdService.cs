﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Orders
{

    public interface IGetUserLastOrderFilterByUserIdService
    {
        OrderPoco Get(int userId);
    }

    [Service]
    public class GetUserLastOrderFilterByUserIdService : IGetUserLastOrderFilterByUserIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetUserLastOrderFilterByUserIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public OrderPoco Get(int userId)
        {
            var orderPoco=new OrderPoco();
            if (userId <= 0) return orderPoco;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var orderTable = DatabaseConstantValues.OrdersTableName;
            var orderUserIdField = DatabaseConstantValues.Orders_UserId_Field_Name;
            var orderPrimaryKey = DatabaseConstantValues.Orders_Poco_Primary_Key_Value;
            var sqlQuery = new Sql($"SELECT TOP(1) * FROM {orderTable} WHERE {orderUserIdField}={userId} ORDER BY {orderPrimaryKey} DESC");
            orderPoco = umbracoDb.Query<OrderPoco>(sqlQuery).FirstOrDefault();
            return orderPoco;
        }
    }
}
