﻿

using System.Globalization;
using System.Web;
using Umbraco.Core;

namespace CasaSoftUmbracoUSkinnedTemplate
{

    public class Global : Umbraco.Web.UmbracoApplication
    {
        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            //-VaryByCustomUrl - Caching is invalidated for every URL change - These are used for items which change by URL, e.g.A product, a News item etc.
            if (custom.InvariantEquals("url"))
            {
                //caching by url and not by culture because in the url there is the culture change as well
                return "url=" + context.Request.Url.AbsolutePath;
            }

            if (custom.InvariantEquals("url;user"))
            {
                //caching by culture and user
                return "url=" + context.Request.Url.AbsolutePath + ";user=" + User.Identity.Name;
            }

            if (custom.InvariantEquals("isAuthenticated"))
            {
                return "culture=" + CultureInfo.CurrentCulture.Name + ";isAuthenticated=" + User.Identity.IsAuthenticated;
            }

            //- VaryByCulture - Caching is invalidated for every culture change ONLY - These are used for items which do not change for every page, e.g. main menu, footer, etc.  Things which are in the master page usually.
            if (custom.InvariantEquals("culture"))
            {
                return "culture=" + CultureInfo.CurrentCulture.Name;
            }



            return base.GetVaryByCustomString(context, custom);
        }
    }
}
