﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.Shared
{
    public class _JuryMemberModel
    {

        public string JudgeName { get; set; }
        public string JudgePosition { get; set; }
        public string JudgeDescription { get; set; }
        public string JudgeImgURL { get; set; }
        public int JuryID { get; set; }
    }
}
