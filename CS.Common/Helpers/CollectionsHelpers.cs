﻿using System.Text.RegularExpressions;

namespace CS.Common.Helpers
{
    public static class CollectionsHelpers
    {
        

        /// <summary>
        /// This returns the indexer without its index. i.e returns "Reviews" from the passed "Reviews[0]"
        /// </summary>
        /// <param name="token">i.e Reviews[1]</param>
        /// <returns></returns>
        public static string GetPropertyIdentifierWithoutIndexers(string token)
        {
            var stringToMatchWith = @"(.*?)\[[0-9]+\]";
            var matches = Regex.Match(token, stringToMatchWith);
            return matches.Groups[1].ToString();
        }

        /// <summary>
        /// This returns the indexer's index only. i.e returns 0 from the passe "Reviews[0]"
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static int GetIndexNumberFromIndexer(string token)
        {
            var stringToMatchWith = @"\[([0-9]+)\]";
            var matches = Regex.Match(token, stringToMatchWith);
            return  int.Parse(matches.Groups[1].ToString());
        }
    }
}
