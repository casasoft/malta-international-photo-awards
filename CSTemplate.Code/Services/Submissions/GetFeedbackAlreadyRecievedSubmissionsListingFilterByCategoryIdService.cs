﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Submissions
{

    public interface IGetFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService
    {
        List<SubmissionPoco> Get(int categoryId);
    }

    [Service]
    public class GetFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService : IGetFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public List<SubmissionPoco> Get(int categoryId)
        {

            var listOfSubmissions = new List<SubmissionPoco>();
            if (categoryId <= 0) return listOfSubmissions;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var submissionsTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = Constants.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionStatusEnumIdField = DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;
            var submissionsCategoryId = DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionFeedbackRequestField = Constants.DatabaseConstantValues.Submissions_Feedback_Request_Field_Name;
            var submissionFeedbackDescriptionField = Constants.DatabaseConstantValues.Submissions_Feedback_Description_Field_Name;
            var submissionsUserId = Constants.DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionTitleField = DatabaseConstantValues.Submissions_Title_Field_Name;
            var featuredFieldId = Constants.DatabaseConstantValues.Photos_Featured_Field_Name;
            var submissionFeedbackReplied = Constants.DatabaseConstantValues.Submission_Feedback_Replied_Mail_Sent;
            var orderByDescValue = Constants.DatabaseConstantValues.ORDER_BY_DESC;

            var sqlQuery = new Sql($@" 
                     SELECT
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionFeedbackRequestField},
                        {submissionsTableAlias}.{submissionFeedbackDescriptionField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionTitleField}
                            FROM
                                {submissionsTableName}
                                     WHERE 
                                        {submissionsTableAlias}.{submissionsCategoryId} = {categoryId} 
                            AND 
                        ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK})
                            AND
                        {submissionsTableAlias}.{submissionFeedbackRequestField} = {(int)Enums.FeedbackStatus.FeedbackRecieved}
                            AND
                        {submissionsTableAlias}.{submissionFeedbackDescriptionField} IS NOT NULL
                            AND
                        ( {submissionsTableAlias}.{submissionFeedbackReplied} IS NULL OR {submissionsTableAlias}.{submissionFeedbackReplied} = {(int)Enums.FeedbackRepliedMailSent.Notyet} )
                           ORDER BY {submissionPrimaryKeyField} {orderByDescValue}
            ");

            listOfSubmissions = umbracoDb.Query<SubmissionPoco>(sqlQuery).ToList();
            
            return listOfSubmissions;
        }
    }
}
