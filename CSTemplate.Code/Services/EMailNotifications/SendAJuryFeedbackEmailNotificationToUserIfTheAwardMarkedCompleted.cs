﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CS.Common.SendGrid.Wrappers.Services;
using CS.Common.Services;
using CS.Common.Umbraco.Services.RazorEngine;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.FeedbackEmail;
using CSTemplate.Code.Models.Members;
using CSTemplate.Code.Models.Register;
using CSTemplate.Code.Services.Awards;
using CSTemplate.Code.Services.Emails;
using CSTemplate.Code.Services.Submissions;
using CSTemplate.Code.Services.Users;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.EMailNotifications
{

    public interface ISendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted
    {
        void Send(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class SendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted : ISendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted
    {
        private readonly IGetAllCompletedAwardEditionListingService _getAllCompletedAwardEditionListingService;
        private readonly IGetFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService _getFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly ISendEmailService _sendEmailService;
        private readonly IRazorEngineCompileTemplate _razorEngineCompileTemplate;
        private readonly IGetEmailTemplatesPlaceholderForCurrentDomainRootNodeService _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService;
        private readonly IUpdateAwardJuryFeedbackAfterAwardMarkedAsCompletedService _updateAwardJuryFeedbackAfterAwardMarkedAsCompletedService;
        private readonly IUpdateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService _updateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService;

        public SendAJuryFeedbackEmailNotificationToUserIfTheAwardMarkedCompleted(

            IGetAllCompletedAwardEditionListingService
                getAllCompletedAwardEditionListingService,
            IGetFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService
                getFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService,
            IGetUserFilterByUserIdService
                getUserFilterByUserIdService,
            IUmbracoHelperService
                umbracoHelperService,
            ISendEmailService
                sendEmailService,
            IRazorEngineCompileTemplate
                razorEngineCompileTemplate,
            IGetEmailTemplatesPlaceholderForCurrentDomainRootNodeService
                getEmailTemplatesPlaceholderForCurrentDomainRootNodeService,
            IUpdateAwardJuryFeedbackAfterAwardMarkedAsCompletedService updateAwardJuryFeedbackAfterAwardMarkedAsCompletedService,
            IUpdateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService updateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService
            )
        {
            _updateAwardJuryFeedbackAfterAwardMarkedAsCompletedService = updateAwardJuryFeedbackAfterAwardMarkedAsCompletedService;
            _updateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService = updateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService;
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _getFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService = getFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService;
            _getAllCompletedAwardEditionListingService = getAllCompletedAwardEditionListingService;
            _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService = getEmailTemplatesPlaceholderForCurrentDomainRootNodeService;
            _razorEngineCompileTemplate = razorEngineCompileTemplate;
            _sendEmailService = sendEmailService;
            _umbracoHelperService = umbracoHelperService;
        }

        public void Send(UmbracoHelper umbracoHelper)
        {
            var emailTemplates =
                _getEmailTemplatesPlaceholderForCurrentDomainRootNodeService.Get(umbracoHelper);
            var fromMailAddress = new MailAddress(CS.Common.Helpers.SmtpHelpers.SmtpFrom, emailTemplates.EmailSenderName);
            //var sendGridTemplateID = ConstantValues.Default_SendGrid_Template_ID;
            var completedAwards = _getAllCompletedAwardEditionListingService.Get(umbracoHelper);


            //var counter = 0;
            foreach (var completedAward in completedAwards)
            {
                var submissionsOfFeedbackAlreadyRecieved = _getFeedbackAlreadyRecievedSubmissionsListingFilterByCategoryIdService.Get(completedAward.AwardCategoryItemId);
                foreach (var submission in submissionsOfFeedbackAlreadyRecieved)
                {
                    var member = _getUserFilterByUserIdService.Get(umbracoHelper, submission.UserId);
                    //string singleSubmissionUrl = ConstantValues.SubmissionUrl;
                    //string submissionTitleToUrl = submission.Title;
                    //var path = singleSubmissionUrl + "/" + submissionTitleToUrl + "-" + submission.Id;
                    string submissionTitle = submission.Title;
                    string requestFeedbackListsUrl = ConstantValues.Feeback_Request_Submissions_List_Link;
                    var path = requestFeedbackListsUrl;
                    var urlBuilder = new System.UriBuilder(System.Web.HttpContext.Current.Request.Url.AbsoluteUri)
                    {
                        Path = path
                    };

                    string absoluteUrl = urlBuilder.ToString();

                    var emailModel = new FeedbackInformationEmailModel()
                    {
                        Name = member.Name,
                        Surname = member.Surname,
                        EmailAddress = member.InsertedEmail,
                        PhotoFullPageUrl = urlBuilder.ToString(),
                        SubmissionTitle = submissionTitle

                    };

                    var emailTemplateForSubmissionOwner = new EmailTemplate(emailTemplates.Children.FirstOrDefault(x => x.Id.Equals(ConstantValues.EmailNotificationWithJuryFeedbackAfterAwardMarkedAsCompletedTemplateId)));

                    var keyForNewMemberEmailBody = $"{emailTemplateForSubmissionOwner.Name}-{"body"}-{emailTemplateForSubmissionOwner.UpdateDate}";

                    var keyForNewMemberEmailSubject = $"{emailTemplateForSubmissionOwner.Name}-{"subject"}-{emailTemplateForSubmissionOwner.UpdateDate}";

                    var newMemberEmailSubject = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplateForSubmissionOwner.Subject, keyForNewMemberEmailSubject, emailModel);

                    var newMemberEmailBody = _razorEngineCompileTemplate.ComplieRazorTemplateFromPassedString(emailTemplateForSubmissionOwner.EmailBody.ToString(), keyForNewMemberEmailBody, emailModel);


                    //send Email to SubmissionOwner
                    //_sendGridEmailService.SendEmail(newMemberEmailSubject, newMemberEmailBody, emailModel.EmailAddress,null, null,fromMailAddress, sendGridTemplateID);
                    _sendEmailService.SendEmail(umbracoHelper, newMemberEmailSubject, newMemberEmailBody,
                        emailModel.EmailAddress, null, null, fromMailAddress);
                    _updateSubmissionFeedbackRepliedEmailStatusFilterBySubmissionIdService.Update(submission.Id);
                    //counter++;
                }

                //Change Award Edition JuryFeedback property As True
                _updateAwardJuryFeedbackAfterAwardMarkedAsCompletedService.Update(umbracoHelper, completedAward.AwardItem.AwardItemId);


            }

        }
    }
}
