﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.WebPages.Html;
using Umbraco.Web;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;
using static CSTemplate.Code.Constants.Enums;

namespace CasaSoftUmbracoUSkinnedTemplate.App_Plugins.UIOMaticCustomFields.ShortListed
{
    [PluginController("ShortListed")]
    public class ShortListedApiController : UmbracoAuthorizedJsonController
    {
        private UmbracoHelper _umbracoHelper;

        public ShortListedApiController()
        {
            _umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
        }
        public string Get(int id)
        {
            var enumStatus = (CSTemplate.Code.Constants.Enums.ShortListedStatus)id;
            return enumStatus.ToString();
        }
        public List<SelectListItem> GetShortListItems()
        {
            var enumList=Enum.GetValues(typeof(ShortListedStatus)).Cast<ShortListedStatus>().Select(v => new SelectListItem
            {
                Text = CS.Common.Helpers.EnumHelpers.GetDescriptionAttributeValue(v),
                Value = ((int)v).ToString()
            }).ToList();
            return enumList;
        }
    }
}
