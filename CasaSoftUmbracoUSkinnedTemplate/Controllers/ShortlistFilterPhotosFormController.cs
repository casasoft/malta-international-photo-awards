﻿using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shortlist_Photos;
using CSTemplate.Code.Services.Awards;
using System.Collections.Generic;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ShortlistFilterPhotosFormController : SurfaceController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        public ShortlistFilterPhotosFormController(IUmbracoControllerWrapperService umbracoControllerWrapperService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
        }

        [HttpPost]
        public virtual ActionResult HandleSearchingForShortlistPhotoFullPage(_ShortListFilterFormModel shortListFilterFormModel)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var baseUrl= ConstantValues.Short_Listed_Photos_List_Link;
            string queryString =string.Empty;
            var awardId = shortListFilterFormModel.AwardEditionId == null ? -1 : shortListFilterFormModel.AwardEditionId;
            var shortListedFilters = shortListFilterFormModel.DateandNameFilter==null?-1: (int)shortListFilterFormModel.DateandNameFilter;
            var shortListedStatus = shortListFilterFormModel.ShortListStatusFilter == null ? -1 : (int)shortListFilterFormModel.ShortListStatusFilter;
            var searchKey = shortListFilterFormModel.SearchKey;
            var awardEditionCategoryId = shortListFilterFormModel.AwardEditionCategoryId == null ? -1 : shortListFilterFormModel.AwardEditionCategoryId;

            var awardIdParam = "awardId=";
            var shortListedFiltersParam = "shortListedFilters=";
            var shortListedStatusParam = "shortListedStatus=";
            var searchKeyParam = "searchKey=";
            var awardCategoryIdParam = "awardCategoryId=";
            List<string> queryStringList = new List<string>();
            if (awardId>0)
            {
                
                awardIdParam += awardId;
                queryStringList.Add(awardIdParam);
                if (awardEditionCategoryId>0)
                {
                    awardCategoryIdParam += awardEditionCategoryId;
                    queryStringList.Add(awardCategoryIdParam);
                }
            }
            if (shortListedFilters>=0)
            {
               
                shortListedFiltersParam += shortListedFilters;
                queryStringList.Add(shortListedFiltersParam);
            }
            if (shortListedStatus>=0)
            {
                
                shortListedStatusParam += shortListedStatus;
                queryStringList.Add(shortListedStatusParam);
            }
            if (!string.IsNullOrEmpty(searchKey))
            {
               
               searchKeyParam += searchKey;
               queryStringList.Add(searchKeyParam);
            }
            queryString=CS.Common.Umbraco.Helpers.QuerySringHelpers.BuildQueryStringUrl(baseUrl,
               queryStringList
               );
           
            var url = queryStringList.Count>0?queryString:baseUrl;
            return Redirect(url);
        }
    }

}