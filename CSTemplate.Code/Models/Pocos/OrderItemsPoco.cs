﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIOMatic.Attributes;
using UIOMatic.Enums;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace CSTemplate.Code.Models.Pocos
{
    [TableName("pocoOrderItems")]
    [PrimaryKey("Id", autoIncrement = true)]
    [ExplicitColumns]
    [UIOMatic("pocoOrderItems", "Order Items", "Order Item", FolderIcon = "icon-flag", ItemIcon = "icon-flag", ShowOnSummaryDashboard = true, RenderType = UIOMaticRenderType.List)]
    public class OrderItemsPoco
    {
        [Column("Id")]
        [PrimaryKeyColumn(Name = "Id", AutoIncrement = true)]
        [UIOMaticListViewField(Name = "Id")]
        public int Id { get; set; }

        [Required]
        [UIOMaticField(Name = "Submission", Description = "Submission", View = UIOMatic.Constants.FieldEditors.Dropdown, Config = "{'typeAlias': 'pocoSubmissions', 'valueColumn': 'Id', 'sortColumn': 'Title', 'textTemplate' : '{{Title}}'}")]
        [UIOMaticListViewField(Name = "Submission")]
        [Column("SubmissionId")]
        public int SubmissionId { get; set; }
        [ResultColumn]
        public SubmissionPoco Submission { get; set; }

        [Required]
        [UIOMaticField(Name = "OrderId", Description = "OrderId", View = UIOMatic.Constants.FieldEditors.Dropdown, Config = "{'typeAlias': 'pocoOrders', 'valueColumn': 'Id', 'sortColumn': 'Description', 'textTemplate' : '{{Description}}'}")]
        [UIOMaticListViewField(Name = "OrderId")]
        [Column("OrderId")]
        public int OrderId { get; set; }
        [ResultColumn]
        public OrderPoco Order { get; set; }

        [UIOMaticField(Name = "PayForSubmission", Description = "Is Order Item For Submission Payment?", View = UIOMatic.Constants.FieldEditors.CheckBox)]
        [UIOMaticListViewField(Name = "PayForSubmission")]
        [Column("PayForSubmission")]
        public bool PayForSubmission { get; set; }

        [UIOMaticField(Name = "PayForFeedback", Description = "Is Order Item For Feedback Payment?", View = UIOMatic.Constants.FieldEditors.CheckBox)]
        [UIOMaticListViewField(Name = "PayForFeedback")]
        [Column("PayForFeedback")]
        public bool PayForFeedback { get; set; }

        [UIOMaticField(Name = "Submission Price", Description = "Submission Price",View = UIOMatic.Constants.FieldEditors.Textfield)]
        [UIOMaticListViewField(Name = "Submission Price")]
        [Column("SubmissionCost")]
        public decimal SubmissionCost { get; set; }

        [UIOMaticField(Name = "Feedback Price", Description = "Feedback Price", View = UIOMatic.Constants.FieldEditors.Textfield)]
        [UIOMaticListViewField(Name = "Feedback Price")]
        [Column("FeedbackCost")]
        public decimal FeedbackCost { get; set; }


    }
}
