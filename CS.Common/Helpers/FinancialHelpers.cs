﻿namespace CS.Common.Helpers
{
    public static class FinancialHelpers
    {
        /// <summary>
        /// Returns the Vat only of a price. 
        /// </summary>
        /// <param name="priceIncVat"></param>
        /// <param name="vatRate">Vat Rate must be an integer. 18% would be 18.</param>
        /// <returns></returns>
        public static decimal GetVatOnlyFromPriceIncludingVat(decimal priceIncVat, decimal vatRate)
        {
            decimal vatRateAsRatio = (decimal)vatRate / (decimal)100;

            return priceIncVat * vatRateAsRatio;
        }

        public static decimal GetPriceExcVatFromPriceIncVat(decimal priceIncVat, decimal vatRate)
        {
            var vatOnly = GetVatOnlyFromPriceIncludingVat(priceIncVat, vatRate);
            var priceExcVat = priceIncVat - vatOnly;
            return priceExcVat;
        }
    }
}
