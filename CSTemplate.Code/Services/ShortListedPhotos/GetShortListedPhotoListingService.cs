﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.ShortListedPhotos
{

    public interface IGetShortListedPhotoListingService
    {
        List<PhotoPoco> Get();
    }

    [Service]
    public class GetShortListedPhotoListingService : IGetShortListedPhotoListingService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetShortListedPhotoListingService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public List<PhotoPoco> Get()
        {
            var listOfPhotoModel = new List<PhotoPoco>();

            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var photosTable = Constants.DatabaseConstantValues.PhotosTableName;
            var photosUrlField = Constants.DatabaseConstantValues.Photos_PhotoUrl_Field_Name;
            var photosAlias = DatabaseConstantValues.aliasForPhotosPocoTableNameAlias;
            var photoSubmissionIdField = DatabaseConstantValues.Photos_Submission_Id_Field_Name;
            var photosPrimaryKeyField = DatabaseConstantValues.Photos_Poco_Primary_Key_Value;

            var submissionsTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = Constants.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = Constants.DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = Constants.DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = Constants.DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionTitleField = DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionShortListedEnumIdField = DatabaseConstantValues.Submissions_Short_Listed_Enum_Id_Field_Name;
            var submissionStatusEnumIdField = DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;
            var submissionShortListedDateField = DatabaseConstantValues.Submissions_Short_Listed_Date_Field_Name;
            

            var featuredFieldId = Constants.DatabaseConstantValues.Photos_Featured_Field_Name;

            var orderByDescValue = Constants.DatabaseConstantValues.ORDER_BY_DESC;

            var sqlQuery = new Sql($@" 
                     SELECT

                        {photosAlias}.{photosPrimaryKeyField},
                        {photosAlias}.{photosUrlField},
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId},
                        {submissionsTableAlias}.{submissionTitleField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionShortListedEnumIdField},
                        {submissionsTableAlias}.{submissionShortListedDateField}
                            FROM
                                    {photosTable}
                                 INNER JOIN {submissionsTableName} ON  {photosAlias}.{photoSubmissionIdField} = {submissionsTableAlias}.{submissionPrimaryKeyField} 
                                     WHERE {submissionsTableAlias}.{submissionShortListedEnumIdField} = {(int)Enums.ShortListedStatus.Yes} 
                                        AND 
                                        ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK})
                                         ORDER BY  {submissionsTableAlias}.{submissionShortListedDateField} {orderByDescValue}
            ");

            var shortListedPhotosSqlResult = umbracoDb.Query<PhotoPoco,SubmissionPoco>(sqlQuery).ToList();
            if(shortListedPhotosSqlResult.Count>0)
            {
                foreach (var shortListedPhoto in shortListedPhotosSqlResult)
                {
                    listOfPhotoModel.Add(new PhotoPoco()
                    {
                        PhotoUrl = shortListedPhoto.PhotoUrl,
                        Submission = shortListedPhoto.Submission
                    });
                }
            }
           
            return listOfPhotoModel;
        }
    }
}
