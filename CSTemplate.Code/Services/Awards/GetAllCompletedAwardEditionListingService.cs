﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared.AwardCategories;
using CSTemplate.Code.Models.Shared.Awards;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.Awards
{

    public interface IGetAllCompletedAwardEditionListingService
    {
        List<_AwardCategoryItemModel> Get(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class GetAllCompletedAwardEditionFilterByFinalJuryFeedbackNotificationDoesNotSendService : IGetAllCompletedAwardEditionListingService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetAllCompletedAwardEditionFilterByFinalJuryFeedbackNotificationDoesNotSendService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }

        public List<_AwardCategoryItemModel> Get(UmbracoHelper umbracoHelper)
        {
            var listOfAwardCategoryItemList = new List<_AwardCategoryItemModel>();

            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;

            var awardEditionsNode = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionPageData)x).Where(x => DateTime.Now > x.EndedDate).ToList();
            if (awardEditionsNode.Count > 0)
            {
                foreach (var awardEdition in awardEditionsNode)
                {
                    var awardCategories = awardEdition.Descendants()
                            .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Category_Page_Data_Document_Type_Alias))
                            .Select(x => (AwardEditionCategoryPageData)x).Where(x => x.Parent.Id == awardEdition.Id).ToList();
                    if (awardCategories.Count > 0)
                    {
                        foreach (var awardCategory in awardCategories)
                        {
                            listOfAwardCategoryItemList.Add(new _AwardCategoryItemModel()
                            {
                                AwardCategoryItemId = awardCategory.Id,
                                AwardCategoryItemName = awardCategory.Name,
                                AwardItem = new _AwardItemModel()
                                {
                                    AwardItemId = awardEdition.Id,
                                    AwardItemName = awardEdition.Name,
                                    FinalJuryFeedbackNotificationEmailSent = awardEdition.FinalJuryFeedbackNotificationEmailSent,
                                    AwardItemEndedDate = awardEdition.EndedDate,
                                    AwardItemStartedDate = awardEdition.StartedDate,
                                    IsAnActiveAward = (DateTime.Now.Date >= awardEdition.StartedDate && DateTime.Now <= awardEdition.EndedDate)
                                }
                            });
                        }
                    }

                }
            }

            return listOfAwardCategoryItemList;
        }
    }
}
