﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using CS.Common.Umbraco.Constants;
using Umbraco.Core;

namespace CSTemplate.Code.SitemapXml
{
    public static class SitemapUrlExtension
    {
        public static XElement ToXElement(this SitemapLanguageUrl sitemapUrl, string ns = SitemapLanguageUrl.SitemapNs, string nsXhtmlParam = SitemapLanguageUrl.SitemapXhtml)
        {

            var parts = new object[]
            {
                new XElement(XName.Get(nameof(SitemapLanguageUrl.Loc).ToLowerInvariant(),ns), Uri.EscapeUriString(sitemapUrl.Loc)),
                sitemapUrl.Lastmod.HasValue ? new XElement(XName.Get(nameof(SitemapLanguageUrl.Lastmod).ToLowerInvariant(), ns), sitemapUrl.Lastmod.Value.ToString("yyyy-MM-ddThh:mm:sszzz")) : null,
                sitemapUrl.Priority.HasValue ? new XElement(XName.Get(nameof(SitemapLanguageUrl.Priority).ToLowerInvariant(), ns), sitemapUrl.Priority.Value.ToString("F1")) : null,
                 }.ToList();

            XNamespace nsXhtml = nsXhtmlParam;

            foreach (var languageUrl in sitemapUrl.LanguageUrlValues)
            {

                UriBuilder uri = new UriBuilder(sitemapUrl.Loc);
                var trailingUrl = languageUrl.Split('/').ToList();
                uri.Path = languageUrl;

                var newUrl = uri.Uri.AbsoluteUri;
                if (trailingUrl.Count > 1)
                {
                    var hrefLangValue = trailingUrl[1];
                    if (languageUrl.InvariantEquals("/"))
                    {
                        hrefLangValue = ConstantValues.LanguageDefaultName;
                    }

                    var languageNode = new XElement(nsXhtml + "link",
                        new XAttribute("rel", "alternate"),
                        new XAttribute("hreflang", hrefLangValue),
                        new XAttribute("href", newUrl));

                    parts.Add(languageNode);
                }
            }

            return new XElement(XName.Get("url", ns), parts);
        }
    }
}
