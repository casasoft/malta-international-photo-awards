﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Photos;
using Umbraco.Web;
using databaseConstants = CSTemplate.Code.Constants.DatabaseConstantValues;
namespace CSTemplate.Code.Services.UserSubmissions
{

    public interface IGetUserSubmissionsServices
    {
        List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper,int userId,int? currentSubmissionId);
    }

    [Service]
    public class GetUserSubmissionsServices : IGetUserSubmissionsServices
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetFirstPhotoUrlFilterBySubmissionIdService _getFirstPhotoUrlFilterBySubmissionIdService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;

        public GetUserSubmissionsServices(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService, 
            IGetFirstPhotoUrlFilterBySubmissionIdService getFirstPhotoUrlFilterBySubmissionIdService,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService
            )
        {
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _getFirstPhotoUrlFilterBySubmissionIdService = getFirstPhotoUrlFilterBySubmissionIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, int userId, int? currentSubmissionId)
        {
            var returnOfItemPhotoModel = new List<_ItemPhotoModel>();
            if (userId <= 0) return returnOfItemPhotoModel;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var submissionsTable = databaseConstants.SubmissionsTableName;
            var submissionIdField = databaseConstants.Submissions_Poco_Primary_Key_Value;
            var submissionDateField = databaseConstants.Submissions_Date_Field_Name;
            var submissionLocationField = databaseConstants.Submissions_Location_Field_Name;
            var submissionCategoryIdField = databaseConstants.Submissions_Category_Id_Field_Name;
            var submissionDescriptionIdField = databaseConstants.Submissions_Description_Field_Name;
            var submissionUserIdField = databaseConstants.Submissions_User_Id_Field_Name;
            var submissionStatusEnumIdField = databaseConstants.Submissions_Status_Enum_Id_Field_Name;
            var submissionTitleField = databaseConstants.Submissions_Title_Field_Name;

            var condition = $" WHERE {submissionUserIdField}={userId} AND   ({submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK})  ";

            if (currentSubmissionId>0 && currentSubmissionId!=null)
            {
                condition += $" AND {submissionIdField}!={currentSubmissionId}";
            }

            var userSubmissions = umbracoDb.Query<SubmissionPoco>($@"
                   SELECT 
                    {submissionIdField},
	                {submissionDateField},
	                {submissionLocationField},
	                {submissionCategoryIdField},
	                {submissionDescriptionIdField},
	                {submissionTitleField}
	                FROM {submissionsTable}
		                {condition}
                    ORDER BY {submissionIdField} DESC
            ").ToList();

            foreach (var submission in userSubmissions)
            {
                var awardInformation = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, submission.CategoryId);
                returnOfItemPhotoModel.Add(new _ItemPhotoModel
                {
                    ItemPhotoAward = awardInformation.AwardItem.AwardItemName,
                    ItemPhotoCategory = awardInformation.AwardCategoryItemName,
                    ItemPhotoDate = submission.Date,
                    ItemPhotoDescription = submission.Description,
                    ItemPhotoLocation = submission.Location,
                    ItemPhotoImgURL = _getFirstPhotoUrlFilterBySubmissionIdService.Get(submission.Id),

                    //todo: [For: Backend | 2018/08/31] Added by Radek on 31.08 for testing purposes (WrittenBy: Radek)        			
                    ItemPhotoTitle = submission.Title,
                    SubmissionId = submission.Id.ToString(),
                    
                });
            }
            return returnOfItemPhotoModel;
        }
    }
}
