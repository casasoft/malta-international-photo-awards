﻿using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Services.Feedbacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class DownloadPhotoFeedbackRequestsJuryDataController : SurfaceController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IDownloadRequestedFeedbackSubmissionsPhotosService _downloadRequestedFeedbackSubmissionsPhotosService;

        public DownloadPhotoFeedbackRequestsJuryDataController(IUmbracoControllerWrapperService umbracoControllerWrapperService, IUmbracoHelperService umbracoHelperService, IDownloadRequestedFeedbackSubmissionsPhotosService downloadRequestedFeedbackSubmissionsPhotosService)
        {
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _umbracoHelperService = umbracoHelperService;
            _downloadRequestedFeedbackSubmissionsPhotosService = downloadRequestedFeedbackSubmissionsPhotosService;
        }
        [HttpPost]
        public virtual FileResult DownloadPhotoFeedbackRequestsJury()
        {
            var _umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var fileName = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(_umbracoHelper, Enums.DictionaryKey.Download_Feedback_Requested_Submissions_Excel_File_Name);

            var urlBuilder = new System.UriBuilder(Request.Url.AbsoluteUri)
            {
                Path = Url.Content(ConstantValues.Submitted_Photo_Source_Address),
            };
            string absoluteUrl = urlBuilder.ToString();
            //_downloadShortListedPhotosService.Download(_umbracoHelper, absoluteUrl);
            var downloadExcelFile = _downloadRequestedFeedbackSubmissionsPhotosService.Download(_umbracoHelper, absoluteUrl);
            fileName = string.Format(fileName, DateTime.Now);
            return File(downloadExcelFile, "application/octet-stream", $"{fileName}");

        }
    }
}