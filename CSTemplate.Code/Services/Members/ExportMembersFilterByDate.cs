﻿using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Models.Members;
using CS.Common.Services;
using System;
using System.Linq;
using OfficeOpenXml;
using userModel=Umbraco.Core.Models;
using System.Collections.Generic;

namespace CSTemplate.Code.Services.Members
{
    public interface IExportMembersFilterByDate
    {
        byte[] Export(UmbracoHelper umbracoHelper,DateTime dateFrom,DateTime dateTo);
    }
    [Service]
    public class ExportMembersFilterByDate : IExportMembersFilterByDate
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        public ExportMembersFilterByDate(
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public byte[] Export(UmbracoHelper umbracoHelper, DateTime dateFrom, DateTime dateTo)
        {
            var getAllMembers = ApplicationContext.Current.Services.MemberService.GetAllMembers().ToList();
            if (dateFrom.Date==DateTime.Now.Date && dateTo.Date==DateTime.Now.Date)
            {
                getAllMembers=getAllMembers.ToList();
            }
            else
            {
                getAllMembers= getAllMembers.Where(x => x.CreateDate >= dateFrom && x.CreateDate <= dateTo).ToList();
            }
            var excelResult = ExportToExcelFile(umbracoHelper, getAllMembers);
            return excelResult;


        }

        private byte[] ExportToExcelFile(UmbracoHelper umbracoHelper,List<userModel.IMember> memberList)
        {
            int columnCounter = 2;
            using (var package = new ExcelPackage())
            {
                var workSheet = package.Workbook.Worksheets.Add("MemberList");
                workSheet.Cells["A1"].Value = "RegistrationDate";
                workSheet.Cells["B1"].Value = "Name";
                workSheet.Cells["C1"].Value = "Surname";
                workSheet.Cells["D1"].Value = "Country";
                workSheet.Cells["E1"].Value = "Nationality";
                workSheet.Cells["F1"].Value = "Email";


                workSheet.Cells["A1"].Style.Font.Bold = true;
                workSheet.Cells["B1"].Style.Font.Bold = true;
                workSheet.Cells["C1"].Style.Font.Bold = true;
                workSheet.Cells["D1"].Style.Font.Bold = true;
                workSheet.Cells["E1"].Style.Font.Bold = true;
                workSheet.Cells["F1"].Style.Font.Bold = true;
                workSheet.DefaultColWidth = 60;
                workSheet.DefaultRowHeight = 30;

                foreach (var memberItem in memberList)
                {
                    var member = new Member(_umbracoHelperService.TypedMember(umbracoHelper, memberItem.Id));
                    workSheet.Cells[$"A{columnCounter}"].Value = member.CreateDate.ToString("dd/MM/yyyy");
                    workSheet.Cells[$"B{columnCounter}"].Value = member.Name.ToString();
                    workSheet.Cells[$"C{columnCounter}"].Value = member.ClientSurname.ToString();
                    workSheet.Cells[$"D{columnCounter}"].Value = member.Country.ToString();
                    workSheet.Cells[$"E{columnCounter}"].Value = member.Nationality.ToString();
                    workSheet.Cells[$"F{columnCounter}"].Value = memberItem.Email.ToString();
                    columnCounter++;
                }
                package.Save();

                return package.GetAsByteArray();
            }
            
        }
    }
}
