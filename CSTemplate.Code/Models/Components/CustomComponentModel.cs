﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.Components
{
    public class CustomComponentModel : IComponentModel
    {
        public IPublishedContent PublishedContent { get; set; }
        public object Content { get; set; }
        public string DocumentTypeAlias { get; set; }
        public string PartialViewUrl { get; set; }
    }
}
