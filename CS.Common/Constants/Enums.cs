﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Constants
{
    public static class Enums
    {
        public enum OrderPhotosSubmitType
        {
            SubmissionOnly,
            SubmissionWithFeedback,
            FeedbackOnly
        }
        

        public enum SubmitPhotoItemCategory
        {
            Nature,
            Conceptual,
            Documentary,
            Social

        }

        public enum ContactPageSubjectType
        {
            Sales,
            Test,
            Test2
        }

        public enum PagerPaginationPageType
        {
            FirstPage,
            PreviousPage,
            NextPage,
            LastPage,
            PageNumber
        }

        public enum TextAlign
        {
            Left,
            Right,
            Center
        }
        public enum JsonDateTimeConverter
        {
            Iso,
            JavaScript
        }
        public enum ImageType
        {
            [Description("jpg")]
            JPEG,
            [Description("jpg")]
            ProgressiveJpeg,
            PNG,
            GIF,
            [Description("tif")]
            TIFF,
            BMP,
            None
        }
        /// <summary>
        /// Do not change names as they might match with CSS classes
        /// </summary>
        public enum MediaItemType
        {
            Document,
            Flash,
            Spreadsheet,
            Powerpoint,
            Image,
            Video,
            PDF,
            Music,
            Textfile,
            General
        }
        public enum EnumsSortBy
        {
            PriorityAttributeValue = 0,
            NameAscending = 100,
            NameDescending = 200,
            Value = 300
        }

        public enum DateFormatType
        {
            [Description("d")] // 6/15/2008 
            ShortDate,
            [Description("D")] // Sunday, June 15, 2008 
            FullDate,
            [Description("f")] // Sunday, June 15, 2008 9:15 PM 
            FullDateShortTime,
            [Description("F")] // Sunday, June 15, 2008 9:15:07 PM 
            FullDateLongTime,
            [Description("g")] // 6/15/2008 9:15 PM 
            GeneralDateShortTime,
            [Description("G")] // 6/15/2008 9:15:07 PM 
            GeneralDateLongTime,
            [Description("m")] // June 15 
            MonthDay,
            [Description("o")] // 2008-06-15T21:15:07.0000000 
            ISO8601_RoundTripDateTime,
            [Description("R")] // Mon, 15 Jun 2009 20:45:30 GMT
            RFC1123,
            [Description("s")] // 2009-06-15T13:45:30
            SortableDateTime,
            [Description("t")] // 1:45 PM 
            ShortTime,
            [Description("T")] // 1:45:30 PM
            LongTime,
            [Description("u")] // 2009-06-15 20:45:30Z
            UniversalSortableDateTime,
            [Description("U")] // Monday, June 15, 2009 8:45:30 PM
            UniversalFullDateTime,
            [Description("y")] // June, 2009
            YearMonth
        }
        public enum LISTING_SORT_BY
        {
            [Display(Name = "Relevance")]
            Relevance = 0,
            [Display(Name = "Locality (A .. Z)")]
            LocalityAsc = 5,
            [Display(Name = "Locality (Z .. A)")]
            LocalityDesc = 7,
            [Display(Name = "Price - Low To High")]
            PriceLowToHigh = 10,
            [Display(Name = "Price - High To Low")]
            PriceHighToLow = 20,
            [Display(Name = "Last Updated")]
            LastUpdated = 30
        }

        public enum LISTING_SHOW_AMOUNT
        {
            [Display(Name = "12 per page")]
            PerPage12 = 12,
            [Display(Name = "24 per page")]
            PerPage24 = 24,
            [Display(Name = "48 per page")]
            PerPage48 = 48,
            [Display(Name = "96 per page")]
            PerPage96 = 96
        }

        public enum WhereDidYouHearAboutUsList
        {
            [Display(Name = "Referral from Friend")]
            ReferralFromFriend,
            [Display(Name = "Google Advert")]
            GoogleAdvert,
            [Display(Name = "Facebook")]
            Facebook,
            [Display(Name = "Instagram")]
            Instagram,
            [Display(Name = "Email")]
            Email,
            [Display(Name = "Photo Contest Insider")]
            PhotoContestInsider,
            [Display(Name = "Other")]
            Other,
        }

        public enum CountryIso3166
        {
            [Display(Name = "Afghanistan")]
            Afghanistan,
            //[Display(Name = "Aland Islands")]
            //AlandIslands,
            [Display(Name = "Albania")]
            Albania,
            [Display(Name = "Algeria")]
            Algeria,
            //[Display(Name = "American Samoa")]
            //AmericanSamoa,
            [Display(Name = "Andorra")]
            Andorra,
            [Display(Name = "Angola")]
            Angola,
            //[Display(Name = "Anguilla")]
            //Anguilla,
            //[Display(Name = "Antarctica")]
            //Antarctica,
            [Display(Name = "Antigua and Barbuda")]
            AntiguaandBarbuda,
            [Display(Name = "Argentina")]
            Argentina,
            [Display(Name = "Armenia")]
            Armenia,
            //[Display(Name = "Aruba")]
            //Aruba,
            [Display(Name = "Australia")]
            Australia,
            [Display(Name = "Austria")]
            Austria,
            [Display(Name = "Azerbaijan")]
            Azerbaijan,
            [Display(Name = "Bahamas")]
            Bahamas,  
            [Display(Name = "Bahrain")]
            Bahrain,  
            [Display(Name = "Bangladesh")]
            Bangladesh,  
            [Display(Name = "Barbados")]
            Barbados, 
            [Display(Name = "Belarus")]
            Belarus, 
            [Display(Name = "Belgium")]
            Belgium, 
            [Display(Name = "Belize")]
            Belize, 
            [Display(Name = "Benin")]
            Benin, 
            //[Display(Name = "Bermuda")]
            //Bermuda, 
            [Display(Name = "Bhutan")]
            Bhutan, 
            [Display(Name = "Bolivia")]
            Bolivia, 
            //[Display(Name = "Bonaire")]
            //Bonaire, 
            [Display(Name = "Bosnia and Herzegovina")]
            BosniaandHerzegovina, 
            [Display(Name = "Botswana")]
            Botswana, 
            //[Display(Name = "Bouvet Island")]
            //BouvetIsland, 
            [Display(Name = "Brazil")]
            Brazil, 
            //[Display(Name = "British Indian Ocean Territory")]
            //BritishIndianOceanTerritory, 
            [Display(Name = "Brunei")]
            BruneiDarussalam, 
            [Display(Name = "Bulgaria")]
            Bulgaria, 
            [Display(Name = "Burkina Faso")]
            BurkinaFaso, 
            [Display(Name = "Burundi")]
            Burundi, 
            [Display(Name = "Cambodia")]
            Cambodia, 
            [Display(Name = "Cameroon")]
            Cameroon, 
            [Display(Name = "Canada")]
            Canada, 
            [Display(Name = "Cape Verde")]
            CapeVerde, 
            //[Display(Name = "Cayman Islands")]
            //CaymanIslands, 
            [Display(Name = "Central African Republic")]
            CentralAfricanRepublic, 
            [Display(Name = "Chad")]
            Chad, 
            [Display(Name = "Chile")]
            Chile, 
            [Display(Name = "China")]
            China, 
            //[Display(Name = "Christmas Island")]
            //ChristmasIsland, 
            //[Display(Name = "Cocos")]
            //Cocos, 
            [Display(Name = "Colombia")]
            Colombia, 
            [Display(Name = "Comoros")]
            Comoros, 
            [Display(Name = "Congo")]
            Congo, 
            [Display(Name = "Congo The Democratic Republic Of The")]
            CongoTheDemocraticRepublicOfThe, 
            //[Display(Name = "Cook Islands")]
            //CookIslands, 
            [Display(Name = "Costa Rica")]
            CostaRica, 
            [Display(Name = "Croatia")]
            Croatia, 
            [Display(Name = "Cuba")]
            Cuba, 
            [Display(Name = "Curacao")]
            Cura, 
            [Display(Name = "Cyprus")]
            Cyprus, 
            [Display(Name = "Czech Republic")]
            CzechRepublic, 
            [Display(Name = "Denmark")]
            Denmark, 
            [Display(Name = "Djibouti")]
            Djibouti, 
            [Display(Name = "Dominica")]
            Dominica, 
            [Display(Name = "Dominican Republic")]
            DominicanRepublic, 
            [Display(Name = "Ecuador")]
            Ecuador, 
            [Display(Name = "Egypt")]
            Egypt, 
            [Display(Name = "El Salvador")]
            ElSalvador, 
            [Display(Name = "Equatorial Guinea")]
            EquatorialGuinea, 
            [Display(Name = "Eritrea")]
            Eritrea, 
            [Display(Name = "Estonia")]
            Estonia, 
            [Display(Name = "Ethiopia")]
            Ethiopia, 
            //[Display(Name = "Falkland Islands")]
            //FalklandIslands, 
            //[Display(Name = "Faroe Islands")]
            //FaroeIslands, 
            [Display(Name = "Fiji")]
            Fiji, 
            [Display(Name = "Finland")]
            Finland, 
            [Display(Name = "France")]
            France, 
            //[Display(Name = "French Guiana")]
            //FrenchGuiana, 
            //[Display(Name = "French Polynesia")]
            //FrenchPolynesia, 
            //[Display(Name = "French Southern Territories")]
            //FrenchSouthernTerritories, 
            [Display(Name = "Gabon")]
            Gabon,
            [Display(Name = "Gambia")]
            Gambia, 
            [Display(Name = "Georgia")]
            Georgia, 
            [Display(Name = "Germany")]
            Germany, 
            [Display(Name = "Ghana")]
            Ghana, 
            //[Display(Name = "Gibraltar")]
            //Gibraltar, 
            //[Display(Name = "Gozo")]
            //Gozo, 
            [Display(Name = "Greece")]
            Greece, 
            //[Display(Name = "Greenland")] ->
            //Greenland, 
            [Display(Name = "Grenada")]
            Grenada, 
            //[Display(Name = "Guadeloupe")]
            //Guadeloupe, 
            //[Display(Name = "Guam")]
            //Guam, 
            [Display(Name = "Guatemala")]
            Guatemala, 
            //[Display(Name = "Guernsey")]
            //Guernsey, 
            [Display(Name = "Guinea")]
            Guinea, 
            [Display(Name = "Guinea-Bissau")]
            GuineaBissau, 
            [Display(Name = "Guyana")]
            Guyana, 
            [Display(Name = "Haiti")]
            Haiti, 
            //[Display(Name = "Heard Island and McDonald Islands")]
            //HeardIslandandMcDonaldIslands, 
            [Display(Name = "Honduras")]
            Honduras, 
            [Display(Name = "HongKong")]
            HongKong,
            [Display(Name = "Hungary")]
            Hungary, 
            [Display(Name = "Iceland")]
            Iceland, 
            [Display(Name = "India")]
            India, 
            [Display(Name = "Indonesia")]
            Indonesia, 
            [Display(Name = "Iran")]
            Iran, 
            [Display(Name = "Iraq")]
            Iraq, 
            [Display(Name = "Ireland")]
            Ireland, 
            //[Display(Name = "Isle of Man")]
            //IsleofMan, 
            [Display(Name = "Israel")]
            Israel, 
            [Display(Name = "Italy")]
            Italy,
            //[Display(Name = "Ivory Coast")]
            //CoteDIvoireIvoryCoast,
            [Display(Name = "Jamaica")]
            Jamaica, 
            [Display(Name = "Japan")]
            Japan, 
            //[Display(Name = "Jersey")]
            //Jersey, 
            [Display(Name = "Jordan")]
            Jordan, 
            [Display(Name = "Kazakhstan")]
            Kazakhstan, 
            [Display(Name = "Kenya")]
            Kenya, 
            [Display(Name = "Kiribati")]
            Kiribati, 
            [Display(Name = "Korea Democratic Peoples Republic Of North Korea")]
            KoreaDemocraticPeoplesRepublicOf_NorthKorea, 
            [Display(Name = "Korea Republic Of South Korea")] 
            KoreaRepublicOf_SouthKorea,
            [Display(Name = "Kosovo")]
            Kosovo,
            [Display(Name = "Kuwait")]
            Kuwait, 
            [Display(Name = "Kyrgyzstan")]
            Kyrgyzstan, 
            [Display(Name = "Laos")]
            Laos, 
            [Display(Name = "Latvia")]
            Latvia, 
            [Display(Name = "Lebanon")]
            Lebanon, 
            [Display(Name = "Lesotho")]
            Lesotho, 
            [Display(Name = "Liberia")]
            Liberia, 
            [Display(Name = "Libyan Arab Jamahiriya")]
            LibyanArabJamahiriya, 
            [Display(Name = "Liechtenstein")]
            Liechtenstein, 
            [Display(Name = "Lithuania")]
            Lithuania, 
            [Display(Name = "Luxembourg")]
            Luxembourg, 
            [Display(Name = "Macao")]
            Macao, 
            [Display(Name = "Macedonia")]
            Macedonia, 
            [Display(Name = "Madagascar")]
            Madagascar, 
            [Display(Name = "Malawi")]
            Malawi, 
            [Display(Name = "Malaysia")]
            Malaysia, 
            [Display(Name = "Maldives")]
            Maldives, 
            [Display(Name = "Mali")]
            Mali, 
            [Display(Name = "Malta")]
            Malta, 
            [Display(Name = "Marshall Islands")]
            MarshallIslands, 
            //[Display(Name = "Martinique")]
            //Martinique, 
            [Display(Name = "Mauritania")]
            Mauritania, 
            [Display(Name = "Mauritius")]
            Mauritius, 
            //[Display(Name = "Mayotte")]
            //Mayotte, 
            [Display(Name = "Mexico")]
            Mexico, 
            [Display(Name = "Micronesia")]
            Micronesia, 
            [Display(Name = "Moldova")]
            Moldova, 
            [Display(Name = "Monaco")]
            Monaco, 
            [Display(Name = "Mongolia")]
            Mongolia, 
            [Display(Name = "Montenegro")]
            Montenegro, 
            [Display(Name = "Montserrat")]
            Montserrat, 
            [Display(Name = "Morocco")]
            Morocco, 
            [Display(Name = "Mozambique")]
            Mozambique,
            [Display(Name = "Myanmar/Bruma")]
            Myanmar,
            [Display(Name = "Namibia")]
            Namibia, 
            [Display(Name = "Nauru")]
            Nauru, 
            [Display(Name = "Nepal")]
            Nepal, 
            [Display(Name = "Netherlands")]
            Netherlands, 
            //[Display(Name = "New Caledonia")]
            //NewCaledonia, 
            [Display(Name = "New Zealand")]
            NewZealand, 
            [Display(Name = "Nicaragua")]
            Nicaragua, 
            [Display(Name = "Niger")]
            Niger, 
            [Display(Name = "Nigeria")]
            Nigeria, 
            //[Display(Name = "Niue")]
            //Niue, 
            //[Display(Name = "Norfolk Island")]
            //NorfolkIsland, 
            //[Display(Name = "Northern Mariana Islands")]
            //NorthernMarianaIslands, 
            [Display(Name = "Norway")]
            Norway, 
            [Display(Name = "Oman")]
            Oman, 
            [Display(Name = "Pakistan")]
            Pakistan,
            [Display(Name = "Palau")]
            Palau, 
            [Display(Name = "Palestinian Territory")]
            PalestinianTerritory, 
            [Display(Name = "Panama")]
            Panama, 
            [Display(Name = "Papua New Guinea")]
            PapuaNewGuinea, 
            [Display(Name = "Paraguay")]
            Paraguay, 
            [Display(Name = "Peru")]
            Peru, 
            [Display(Name = "Philippines")]
            Philippines, 
            //[Display(Name = "Pitcairn")]
            //Pitcairn, 
            [Display(Name = "Poland")]
            Poland, 
            [Display(Name = "Portugal")]
            Portugal, 
            //[Display(Name = "Puerto Rico")]
            //PuertoRico, 
            [Display(Name = "Qatar")]
            Qatar, 
            //[Display(Name = "Reunion")]
            //Reunion, 
            [Display(Name = "Romania")]
            Romania, 
            [Display(Name = "Russian Federation")]
            RussianFederation, 
            [Display(Name = "Rwanda")]
            Rwanda, 
            //[Display(Name = "Saint Barth")]
            //SaintBarth, 
            //[Display(Name = "Saint Helena")]
            //SaintHelena, 
            [Display(Name = "Saint Kitts and Nevis")]
            SaintKittsandNevis, 
            [Display(Name = "Saint Lucia")]
            SaintLucia, 
            //[Display(Name = "Saint Martin")]
            //SaintMartin, 
            //[Display(Name = "Saint Pierre and Miquelon")]
            //SaintPierreandMiquelon, 
            [Display(Name = "Saint Vincent and the Grenadines")]
            SaintVincentandtheGrenadines, 
            [Display(Name = "Samoa")]
            Samoa, 
            [Display(Name = "San Marino")]
            SanMarino, 
            [Display(Name = "Sao Tome and Principe")]
            SaoTomeandPrincipe, 
            [Display(Name = "Saudi Arabia")]
            SaudiArabia, 
            [Display(Name = "Senegal")]
            Senegal, 
            [Display(Name = "Serbia")]
            Serbia, 
            [Display(Name = "Seychelles")]
            Seychelles, 
            [Display(Name = "Sierra Leone")]
            SierraLeone, 
            [Display(Name = "Singapore")]
            Singapore, 
            [Display(Name = "Sint Maarten")]
            SintMaarten, 
            [Display(Name = "Slovakia")]
            Slovakia, 
            [Display(Name = "Slovenia")]
            Slovenia, 
            [Display(Name = "Solomon Islands")]
            SolomonIslands, 
            [Display(Name = "Somalia")]
            Somalia, 
            [Display(Name = "South Africa")]
            SouthAfrica, 
            //[Display(Name = "South Georgia and the South Sandwich Islands")]
            //SouthGeorgiaandtheSouthSandwichIslands, 
            [Display(Name = "Spain")]
            Spain, 
            [Display(Name = "Sri Lanka")]
            SriLanka, 
            [Display(Name = "Sudan")]
            Sudan, 
            [Display(Name = "Suriname")]
            Suriname, 
            //[Display(Name = "Svalbard and Jan Mayen")]
            //SvalbardandJanMayen, 
            [Display(Name = "Swaziland")]
            Swaziland, 
            [Display(Name = "Sweden")]
            Sweden, 
            [Display(Name = "Switzerland")]
            Switzerland, 
            [Display(Name = "Syrian Arab Republic")]
            SyrianArabRepublic, 
            [Display(Name = "Taiwan")]
            Taiwan, 
            [Display(Name = "Tajikistan")]
            Tajikistan, 
            [Display(Name = "Tanzania")]
            Tanzania, 
            [Display(Name = "Thailand")]
            Thailand, 
            [Display(Name = "Timor-Leste")]
            TimorLeste, 
            [Display(Name = "Togo")]
            Togo, 
            //[Display(Name = "Tokelau")]
            //Tokelau, 
            [Display(Name = "Tonga")]
            Tonga, 
            [Display(Name = "Trinidad and Tobago")]
            TrinidadandTobago, 
            [Display(Name = "Tunisia")]
            Tunisia, 
            [Display(Name = "Turkey")]
            Turkey, 
            [Display(Name = "Turkmenistan")]
            Turkmenistan, 
            //[Display(Name = "Turks and Caicos Islands")]
            //TurksandCaicosIslands, 
            [Display(Name = "Tuvalu")]
            Tuvalu, 
            [Display(Name = "Uganda")]
            Uganda, 
            [Display(Name = "Ukraine")]
            Ukraine, 
            [Display(Name = "United Arab Emirates")]
            UnitedArabEmirates, 
            [Display(Name = "United Kingdom")]
            UnitedKingdom, 
            [Display(Name = "United States")]
            UnitedStates, 
            //[Display(Name = "United State sMinor Outlying Islands")]
            //UnitedStatesMinorOutlyingIslands, 
            [Display(Name = "Uruguay")]
            Uruguay, 
            [Display(Name = "Uzbekistan")]
            Uzbekistan, 
            [Display(Name = "Vanuatu")]
            Vanuatu, 
            //[Display(Name = "Vatican City")]
            //VaticanCityHolySee, 
            [Display(Name = "Venezuela")]
            Venezuela, 
            [Display(Name = "Vietnam")]
            Vietnam, 
            //[Display(Name = "Virgin Islands, British")]
            //VirginIslandsBritish, 
            //[Display(Name = "Virgin Islands, US")]
            //VirginIslandsUS, 
            //[Display(Name = "Wallis and Futuna")]
            //WallisandFutuna, 
            //[Display(Name = "Western Sahara")]
            //WesternSahara, 
            [Display(Name = "Yemen")]
            Yemen, 
            [Display(Name = "Zambia")]
            Zambia, 
            [Display(Name = "Zimbabwe")]
            Zimbabwe
        }

    }
}
