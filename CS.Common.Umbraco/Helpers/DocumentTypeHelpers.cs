﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Helpers;
using CS.Common.Umbraco.Constants;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace CS.Common.Umbraco.Helpers
{
    public static class DocumentTypeHelpers
    {
 
        public static string ConvertDocumentTypePropertyObjectValueToString(
            object value,
            string propertyEditorAlias,
            UmbracoHelper umbracoContext)
        {
            var valueAsString = String.Empty;

            if (value != null && propertyEditorAlias != ConstantValues.ListViewPropertyEditorAlias)
            {
                switch (propertyEditorAlias)
                {
                    case ConstantValues.CheckBoxListPropertyEditorAlias:
                        {
                            var values = new List<string>();
                            var valuesIds = ((string)value).Split(',');

                            foreach (var valueId in valuesIds)
                            {
                                var preValueAsString = getPreValueAsString(valueId);
                                values.Add(preValueAsString);
                            }

                            valueAsString = String.Join(", ", values);
                            break;
                        }
                    case ConstantValues.DatePropertyEditorAlias:
                        {
                            var valueAsDateTime = (DateTime)value;
                            valueAsString = DateHelpers.FormatDate(valueAsDateTime);
                            break;
                        }
                    case ConstantValues.DropDownPropertyEditorAlias:
                        {
                            valueAsString = getPreValueAsString((string)value);
                            break;
                        }
                    case ConstantValues.GridPropertyEditorAlias:
                    {
                            valueAsString = value.ToString();

                            var fromBeginningOfTextToEnd = valueAsString.Substring(valueAsString.IndexOf("\"value\"") + 10);

                            var valueText = fromBeginningOfTextToEnd.Substring(0, fromBeginningOfTextToEnd.IndexOf("\""));

                            valueAsString = valueText;
                            break;
                    }
                    case ConstantValues.MultipleMediaPickerAlias:
                    {
                            var imagesList = value.ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            var mediaGalleryItems = new List<IPublishedContent>();

                        foreach (var mediaAsPublishedContent in imagesList.Select(media => umbracoContext.TypedMedia(Convert.ToInt32(media))))
                            {
                                if (mediaAsPublishedContent.Children.Any())
                                {
                                    mediaGalleryItems.AddRange(mediaAsPublishedContent.Children.Where(x => x.DocumentTypeAlias != "File"));
                                }
                                else
                                {
                                    mediaGalleryItems.Add(mediaAsPublishedContent);
                                }
                            }

                        var mediaGalleryItemsUrls = mediaGalleryItems.Select(media => $"{HttpContext.Current.Request.Url.Host}{media.Url}").ToList();

                        valueAsString = string.Join(",", mediaGalleryItemsUrls);

                        break;
                    }
                    case ConstantValues.ImageCropperPropertyEditorAlias:
                        {
                            valueAsString = value.ToString();
                            var fromBeginningOfTextToEnd = valueAsString.Substring(valueAsString.IndexOf("\"src\": \"") + 8);
                            var imagePath = fromBeginningOfTextToEnd.Substring(0, fromBeginningOfTextToEnd.IndexOf("\""));

                            valueAsString = $"{HttpContext.Current.Request.Url.Host}{imagePath}";
                            break;
                    }
                    default:
                        {
                            valueAsString = value.ToString();
                            break;
                        }
                }
            }
            else
            {
                valueAsString = "N/A";
            }

            return valueAsString;
        }

        private static string getPreValueAsString(
            string preValueId)
        {
            var preValueIdAsInt = Int32.Parse(preValueId);
            var preValueAsString = umbraco.library.GetPreValueAsString(preValueIdAsInt);

            return preValueAsString;
        }
    }
}