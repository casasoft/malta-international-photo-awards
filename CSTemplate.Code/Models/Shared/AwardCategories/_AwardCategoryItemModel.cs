﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared.Awards;

namespace CSTemplate.Code.Models.Shared.AwardCategories
{
    public class _AwardCategoryItemModel
    {
        public _AwardItemModel AwardItem { get; set; }
        public int AwardCategoryItemId { get; set; }
        public string AwardCategoryItemName { get; set; }
    }
}
