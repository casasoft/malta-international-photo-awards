﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CS.Common.Umbraco.Constants;
using CS.Common.Umbraco.Helpers;
using CS.Common.Umbraco.Helpers.UmbracoPackages.Vorto;
using Umbraco.Core;
using Umbraco.Core.Cache;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;
using Our.Umbraco.Vorto.Models;

namespace CS.Common.Umbraco.UrlProviders
{
    /// <summary>
    /// This is the base multilingual URL provider which you need to extend from
    /// to generate URLs
    /// </summary>
    public abstract class BaseMultilingualUrlProvider : IUrlProvider
    {
        /// <summary>
        /// Compute the path after the language, i.e. /en/[path]
        /// Make sure you finish it with an ending '/'
        /// </summary>
        /// <param name="umbracoContext"></param>
        /// <param name="cultureName"></param>
        /// <param name="content"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        protected abstract string computePathForCurrentNodeForCulture(
            UmbracoContext umbracoContext,
            string cultureName, IPublishedContent content,
            Uri current);

        protected abstract bool IsContentValidForCurrentUrlProvider(IPublishedContent content);

        private bool isCultureValid(string cultureName)
        {
            var allLanguages = LanguageHelpers.GetAllWebsiteLanguages(ApplicationContext.Current).ToList();
            var isCultureNameValid =
                allLanguages.SingleOrDefault(
                    x => String.Compare(x.CultureInfo.Name, cultureName, StringComparison.OrdinalIgnoreCase) == 0) !=
                null;
            return isCultureNameValid;
        }

        private bool isRootNode(IPublishedContent content)
        {
            bool isRootNode = false;

            if (content.Level == 1)
            {
                isRootNode = true;
            }
            return isRootNode;
        }

        private bool isCultureDefaultOne(string cultureName)
        {
            return (cultureName.InvariantEquals(ConstantValues.LanguageDefaultName));
        }

        protected string getPathForCurrentNodeForCultureOrReturnSlashIfRoot(UmbracoContext umbracoContext,
            string cultureName,
            IPublishedContent content,
            Uri current)
        {

            var allLanguages =
                  LanguageHelpers.GetAllWebsiteLanguages(ApplicationContext.Current).ToList();

            ILanguage cultureToUse = null;

            var isCultureNameValid = allLanguages.SingleOrDefault(x => String.Compare(x.CultureInfo.Name, cultureName, StringComparison.OrdinalIgnoreCase) == 0) != null;
            if (isCultureNameValid)
            {
                cultureToUse = allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(cultureName));
            }
            else
            {
                cultureToUse = allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(ConstantValues.LanguageDefaultName));
            }

            cultureName = cultureToUse.CultureInfo.ToString();
            string url = string.Format("/{0}/", cultureName);

            if (isRootNode(content))
            {
                if (isCultureDefaultOne(cultureName))
                {
                    //Since it's default one, return just "/";
                    url = "/";
                }

            }
            else
            {
                url += computePathForCurrentNodeForCulture(umbracoContext, cultureName, content, current);
            }
            return url;

        }

        public string GetUrl(UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            // Don't use umbracoContext.PublishedContentRequest.Culture because this code is also called in the backend.
            var currentCultureName = Thread.CurrentThread.CurrentCulture.Name;
            string providerTypeName = this.GetType().FullName;
            var url = UmbracoContext.Current.Application.ApplicationCache.RequestCache.GetCacheItem<string>(
                string.Format("{0}-GetUrl-{1}-{2}", providerTypeName, id, currentCultureName),
                () => computeGetUrl(umbracoContext, id, current, currentCultureName));
            //Convert it to absolute
            if (mode == UrlProviderMode.Absolute /*&& !string.IsNullOrWhiteSpace(url)*/)
            {
                var uriBuilder = new UriBuilder(current.Scheme, current.Host, current.Port, url);
                url = uriBuilder.ToString();
            }

            return url;
        }

        private string computeGetUrl(UmbracoContext umbracoContext, int id, Uri current, string currentCultureName)
        {

            var content = umbracoContext.ContentCache.GetById(id);

            if (content != null && IsContentValidForCurrentUrlProvider(content))
            {
               
                    return getPathForCurrentNodeForCultureOrReturnSlashIfRoot(umbracoContext,
                        currentCultureName,
                        content,
                        current);
                
            }

            return null;

        }


        public IEnumerable<string> GetOtherUrls(UmbracoContext umbracoContext, int id, Uri current)
        {
            // Don't use umbracoContext.PublishedContentRequest.Culture because this code is also called in the backend.
            var currentCultureName = Thread.CurrentThread.CurrentCulture.Name;

            string providerTypeName = this.GetType().FullName;
            return UmbracoContext.Current.Application.ApplicationCache.RequestCache.GetCacheItem<IEnumerable<string>>(
                string.Format("{0}-GetOtherUrls-{1}-{2}", providerTypeName, id, currentCultureName),
                () =>
                {
                    return computeGetUrls(umbracoContext, id, current, currentCultureName);
                });
        }

        private IEnumerable<string> computeGetUrls(UmbracoContext umbracoContext, int id, Uri current, string currentCultureName)
        {
            var content = umbracoContext.ContentCache.GetById(id);
            List<string> urls = null;
            if (content != null && IsContentValidForCurrentUrlProvider(content))
            {
                //var domains = ApplicationContext.Current.Services.DomainService.GetAll(true).OrderBy(x => x.Id).ToList();

                var allLanguages =
                   LanguageHelpers.GetAllWebsiteLanguages(ApplicationContext.Current).ToList();

                //There is more than one language
                if (allLanguages.Count > 1)
                {
                    urls = new List<string>();

                    removeCurrentCultureOrDefaultCultureIfNotValid(currentCultureName, allLanguages);


                    for (int i = 0; i < allLanguages.Count; i++)
                    {
                        // Get the domain and matching parent url.
                        var culture = allLanguages[i].CultureInfo.Name;

                        var url = getPathForCurrentNodeForCultureOrReturnSlashIfRoot(umbracoContext,
                            culture,
                            content,
                            current);


                        urls.Add(url);
                    }
                }
            }

            return urls;
        }

        private void removeCurrentCultureOrDefaultCultureIfNotValid(string currentCultureName,
            List<ILanguage> allLanguages)
        {
            ILanguage cultureToRemove = null;

            var isCultureNameValid =
                isCultureValid(currentCultureName);
            if (isCultureNameValid)
            {
                cultureToRemove =
                    allLanguages.First(x => x.CultureInfo.Name.InvariantEquals(currentCultureName));
            }
            else
            {
                cultureToRemove =
                    allLanguages.First(
                        x => x.CultureInfo.Name.InvariantEquals(ConstantValues.LanguageDefaultName));
            }

            // Remove the found domain because it's already used.
            allLanguages.Remove(cultureToRemove);
        }
    }
}