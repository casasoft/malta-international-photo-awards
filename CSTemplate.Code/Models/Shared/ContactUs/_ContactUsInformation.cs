﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared.ContactUs
{
    public class _ContactUsInformation
    {
        public string Title { get; set; }
        public string ExplanationText { get; set; }
    }
    
}
