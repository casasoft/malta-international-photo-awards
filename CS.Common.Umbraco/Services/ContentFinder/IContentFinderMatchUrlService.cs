﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web.Routing;

namespace CS.Common.Umbraco.Services.ContentFinder
{
    public class ContentFinderMatch
    {
        public IPublishedContent MatchingContent { get; set; }
        public bool Match { get; set; }
    }
    public interface IContentFinderMatchUrlService
    {
        /// <summary>
        /// Match the URL for this service.  You can take the URL of the current page from publishedContentRequest.Uri.AbsolutePath
        /// </summary>
        /// <param name="publishedContentRequest"></param>
        /// <returns></returns>
        ContentFinderMatch MatchUrl(PublishedContentRequest publishedContentRequest);

    }
}
