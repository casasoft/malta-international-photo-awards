﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages.Html;
using Umbraco.Web;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;
using static CSTemplate.Code.Constants.Enums;
using SelectListItem = System.Web.WebPages.Html.SelectListItem;

namespace CasaSoftUmbracoUSkinnedTemplate.App_Plugins.UIOMaticCustomFields.Orders
{
    [PluginController("Orders")]
    public class OrdersApiController : UmbracoAuthorizedJsonController
    {
        public List<SelectListItem> GetStatusList()
        {
            var orderStatusList = Enum.GetValues(typeof(PaymentStatus)).Cast<PaymentStatus>().Select(v => new SelectListItem
            {
                Text = CS.Common.Helpers.EnumHelpers.GetDescriptionAttributeValue(v),
                Value = ((int)v).ToString()
            }).ToList();
            return orderStatusList;
        }

        public string GetStatusName(int id)
        {
            var enumStatus = (CSTemplate.Code.Constants.Enums.PaymentStatus)id;
            return CS.Common.Helpers.EnumHelpers.GetDescriptionAttributeValue(enumStatus);
        }
    }
}