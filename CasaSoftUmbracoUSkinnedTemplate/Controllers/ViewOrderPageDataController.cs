﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.ViewOrder;
using CSTemplate.Code.Services.Submissions;
using CSTemplate.Code.Services.ViewOrder;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using USNStarterKit.USNHelpers;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ViewOrderPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetViewOrderPageService _getViewOrderPageService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly ISendEmailUponCompletedSubmissionService _sendEmailUponCompletedSubmissionService;

        public ViewOrderPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetViewOrderPageService getViewOrderPageService,
            IUmbracoHelperService umbracoHelperService,
            ISendEmailUponCompletedSubmissionService
            sendEmailUponCompletedSubmissionService)
        {
            _sendEmailUponCompletedSubmissionService = sendEmailUponCompletedSubmissionService;
            _umbracoHelperService = umbracoHelperService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getViewOrderPageService = getViewOrderPageService;
        }

        // GET: ViewOrderPageData
        //[OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult ViewOrderPage(RenderModel model, string id)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            int orderId = int.Parse(id);
            bool doesExist = orderId > 0 ? true : false;
            var currentUserId = Members.GetCurrentMemberId();
            if (!doesExist)
            {
                var pageNotFound = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, ConstantValues.Page_Not_Found_Data_Document_Type_Alias);
                return new RedirectToUmbracoPageResult(pageNotFound);
            }
            ViewOrderPageModel viewOrderPageModel = _getViewOrderPageService.Get(umbracoHelper, model.Content,orderId, currentUserId);
            if (viewOrderPageModel==null)
            {
                var pageNotFound = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, CSTemplate.Code.Constants.ConstantValues.Page_Not_Found_Data_Document_Type_Alias);
                return new RedirectToUmbracoPageResult(pageNotFound);
            }
        
            return base.Populate(viewOrderPageModel);
        }
    }
}