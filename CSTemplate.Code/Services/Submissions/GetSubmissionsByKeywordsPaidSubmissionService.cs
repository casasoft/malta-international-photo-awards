﻿using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Members;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Photos;
using CSTemplate.Code.Services.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Persistence;
using Umbraco.Web;
using static CSTemplate.Code.Constants.Enums;

namespace CSTemplate.Code.Services.Submissions
{
    public interface IGetSubmissionsByKeywordsPaidSubmissionService
    {
        List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper,string param, SubmissionFilters shortListFilters, ShortListedStatusFilter shortListedStatus,int awardId, int awardCategoryId);
    }
    [Service]
    public class GetSubmissionsByKeywordsPaidSubmissionService : IGetSubmissionsByKeywordsPaidSubmissionService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetFirstPhotoUrlFilterBySubmissionIdService _getFirstPhotoUrlFilterBySubmissionIdService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;
        private readonly IGetUserFilterByEmailService _getUserFilterByEmailService;
        private readonly IGetUserFilterByNameAndSurnameService _getUserFilterByNameAndSurnameService;

        public GetSubmissionsByKeywordsPaidSubmissionService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IUmbracoHelperService umbracoHelperService,
                IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService,
                IGetFirstPhotoUrlFilterBySubmissionIdService getFirstPhotoUrlFilterBySubmissionIdService,
               IGetUserFilterByUserIdService getUserFilterByUserIdService,
               IGetUserFilterByEmailService getUserFilterByEmailService,
               IGetUserFilterByNameAndSurnameService getUserFilterByNameAndSurnameService
            )
        {
            this._getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
            this._umbracoHelperService = umbracoHelperService;
            this._getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            this._getFirstPhotoUrlFilterBySubmissionIdService = getFirstPhotoUrlFilterBySubmissionIdService;
            this._getUserFilterByUserIdService = getUserFilterByUserIdService;
            this._getUserFilterByEmailService = getUserFilterByEmailService;
            this._getUserFilterByNameAndSurnameService = getUserFilterByNameAndSurnameService;
        }
        public List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, string param, SubmissionFilters shortListFilters, ShortListedStatusFilter shortListedStatus, int awardId, int awardCategoryId)
        {
            var listOfItemPhotoModel = new List<_ItemPhotoModel>();
            
            if (string.IsNullOrEmpty(param)) return listOfItemPhotoModel;
            var member = new _MemberItemModel();

           

            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var submissionsTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = Constants.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = Constants.DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = Constants.DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = Constants.DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = Constants.DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = Constants.DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionTitleField = Constants.DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionShortListedEnumIdField = Constants.DatabaseConstantValues.Submissions_Short_Listed_Enum_Id_Field_Name;
            var submissionShortListedDateField = Constants.DatabaseConstantValues.Submissions_Short_Listed_Date_Field_Name;
            var submissionStatusEnumIdField = Constants.DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;
            var submissionAwardEditionId = DatabaseConstantValues.Submission_Award_Edition_Id;

            //Title-Description-NameSurname
            var additionalConditionsWithFilterNameSurname = @" AND ({0} OR {1} OR {2} ) ";
           //Title and Description
            var additionalConditions = @" AND ({0} OR {1}) ";
            var titleCheck = $" { submissionsTableAlias}.{ submissionTitleField}  LIKE '%{param}%'  ";
            var descriptionCheck = $" { submissionsTableAlias}. {submissionsDescription} LIKE '%{param}%'  ";
            
            var userFilterNameSurnameFilterResult = _getUserFilterByNameAndSurnameService.Get(umbracoHelper, param);
            var userNameSurnameFilterCondition = string.Empty;
            var userCountControl = userFilterNameSurnameFilterResult.Count > 0;
            if (userCountControl)
            {
                var userIds = userFilterNameSurnameFilterResult.Select(x => x.MemberId.ToString()).ToList();
                var userIdsCondition = @" {0} in ({1})  ";
                string commaSeparatedUserIds = string.Join(",", userIds);
                userNameSurnameFilterCondition = string.Format(userIdsCondition, $"{submissionsTableAlias}.{submissionsUserId}", commaSeparatedUserIds);
               
            }

            if (awardId > 0)
            {
                var awardIdCondition = $" AND  {submissionsTableAlias}.{submissionAwardEditionId}={awardId} ";
                if (awardCategoryId>0)
                {
                    awardIdCondition += $" AND {submissionsTableAlias}.{submissionsCategoryId}= {awardCategoryId}";
                }

                if (userCountControl)
                {
                    userNameSurnameFilterCondition += awardIdCondition;
                }
                else
                {
                    descriptionCheck += awardIdCondition;
                }


            }
            string whereCondition = string.Empty;

            if (shortListedStatus == ShortListedStatusFilter.All)
            {
                whereCondition = $@" WHERE 
                                        ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                                         ";
            }
            else if(shortListedStatus>=0 && shortListedStatus!=ShortListedStatusFilter.All)
            {
                whereCondition = $@"WHERE 
                                    (
                                        {submissionsTableAlias}.{submissionShortListedEnumIdField} = {(int)shortListedStatus}
                                    )
                                        AND   ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                                        
                                        ";
            }
            var sqlQuery = $@"
                    SELECT
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId},
                        {submissionsTableAlias}.{submissionTitleField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionShortListedEnumIdField},
                        {submissionsTableAlias}.{submissionStatusEnumIdField}
                            FROM
                                {submissionsTableName}
                                                                   
            ";
            sqlQuery += whereCondition;

            if (param.Contains("@"))
            {
                member = _getUserFilterByEmailService.Get(umbracoHelper, param);
                var emailCondition = $@"  AND {submissionsTableAlias}.{submissionsUserId} = {member.MemberId}   ";
                sqlQuery += emailCondition;
            }
            else
            {
                if (userCountControl)
                {
                    sqlQuery += string.Format(additionalConditionsWithFilterNameSurname, titleCheck, descriptionCheck, userNameSurnameFilterCondition);
                }
                else
                {
                    sqlQuery += string.Format(additionalConditions, titleCheck, descriptionCheck);
                }
                
            }
            
            var orderByExpression = " ORDER BY {0} {1} ";
            var orderByDesc = Constants.DatabaseConstantValues.ORDER_BY_DESC;
            var orderByAsc = Constants.DatabaseConstantValues.ORDER_BY_ASC;
            switch (shortListFilters)
            {
                case SubmissionFilters.DateNewestFirst:
                    sqlQuery += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionPrimaryKeyField}", orderByDesc);
                    break;
                case SubmissionFilters.DateOldestFirst:
                    sqlQuery += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionPrimaryKeyField}", orderByAsc);
                    break;
                case SubmissionFilters.SubmissionNameAscending:
                    sqlQuery += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionTitleField}", orderByAsc);
                    break;
                case SubmissionFilters.SubmissionNameDescending:
                    sqlQuery += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionTitleField}", orderByDesc);
                    break;
                
            }

            var query = new Sql(sqlQuery);

            var queryResult = umbracoDb.Query<SubmissionPoco>(query).ToList();
            foreach (var submittedPhoto in queryResult)
            {
                var statusEnum = (CSTemplate.Code.Constants.Enums.SubmissionStatus)submittedPhoto.StatusEnumID;
                var status = EnumHelpers.GetDescriptionAttributeValue(statusEnum);
                var awardCategory = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, submittedPhoto.CategoryId);
                var photographerInfos = _getUserFilterByUserIdService.Get(umbracoHelper, submittedPhoto.UserId);

                var photographerFullName = photographerInfos.Name + " " + photographerInfos.Surname;
                listOfItemPhotoModel.Add(new _ItemPhotoModel
                {
                    ItemPhotoImgURL = _getFirstPhotoUrlFilterBySubmissionIdService.Get(submittedPhoto.Id),
                    ItemPhotoDate = submittedPhoto.Date,
                    ItemPhotoName = photographerFullName,
                    ItemPhotoAward = awardCategory.AwardItem.AwardItemName,
                    ItemPhotoLocation = submittedPhoto.Location,
                    ItemPhotoDescription = submittedPhoto.Description,
                    ItemPhotoCategory = awardCategory.AwardCategoryItemName,
                    ItemPhotoTitle = submittedPhoto.Title,
                    SubmissionId = submittedPhoto.Id.ToString(),
                    ShowShortlistRadioButtons = true,
                    SubmissionStatusTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, CSTemplate.Code.Constants.Enums.DictionaryKey.Submission_Status_Title_Text),      			
                    ListOfRadiosValues = Enum.GetValues(typeof(Enums.ShortListedStatus)).Cast<Enums.ShortListedStatus>().ToList(),
                    ChosenRadioValue = Enums.ShortListedStatus.NotYet,
                    CurrentSubmissionStatus = status.ToString()


                });

            }

            return listOfItemPhotoModel;
        }
    }
}
