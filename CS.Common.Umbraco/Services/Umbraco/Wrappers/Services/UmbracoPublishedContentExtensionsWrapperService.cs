﻿using System;
using System.Collections.Generic;
using System.Linq;
using CS.Common.Collections;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Services.Page;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace CS.Common.Umbraco.Services.Umbraco.Wrappers.Services
{
    public class QueryablePagedResults<T>
    {
        public IQueryable<T> Results { get;  }
        public int Count { get; }

        public QueryablePagedResults(IQueryable<T> results, int count)
        {
            this.Results = results;
            this.Count = count;
        }
    }


    public interface IUmbracoPublishedContentExtensionsWrapperService
    {
        IQueryable<IPublishedContent> Where(IEnumerable<IPublishedContent> list, string predicate);
        SearchPagedResults<IPublishedContent> WhereAndCount(IEnumerable<IPublishedContent> list, string predicate);

        IPublishedContent WhereAndGetFirstOrDefault(IEnumerable<IPublishedContent> list, string predicate);

        List<T> Where<T>(IEnumerable<IPublishedContent> list, string predicate)
            where T : PublishedContentWrapped;
        SearchPagedResults<T> WhereAndCount<T>(IEnumerable<IPublishedContent> list, string predicate)
            where T : PublishedContentWrapped;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <param name="orderByPredicate">E.g. price desc</param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IQueryable<IPublishedContent> OrderByAndGetPage(IEnumerable<IPublishedContent> list,
            string orderByPredicate,
            int? pageNumber,
            int? pageSize);



        IQueryable<IPublishedContent> GetPageFromItems(IQueryable<IPublishedContent> list,
            int pageNumber,
            int pageSize);

        IEnumerable<IPublishedContent> GetPageFromItems(IEnumerable<IPublishedContent> list,
            int pageNumber,
            int pageSize);

        List<T> ConvertAllPublishedContentToModels<T>(IEnumerable<IPublishedContent> list);
        T ConvertPublishedContentToModel<T>(IPublishedContent item);


        IPublishedContent GetRootNodeFromContextCurrentItems(IPublishedContent content, bool noCache = false);
    }

    [Service]
    public class UmbracoPublishedContentExtensionsWrapperService : IUmbracoPublishedContentExtensionsWrapperService
    {
        private readonly IPageService _pageService;

        public UmbracoPublishedContentExtensionsWrapperService(IPageService pageService)
        {
            _pageService = pageService;
        }

        public IQueryable<IPublishedContent> Where(IEnumerable<IPublishedContent> list, string predicate)
        {
            return list.Where(predicate);
        }

        public SearchPagedResults<IPublishedContent> WhereAndCount(IEnumerable<IPublishedContent> list, string predicate)
        {
            var results = Where(list, predicate);
            return new SearchPagedResults<IPublishedContent>(results, results.Count());
        }

        public List<T> Where<T>(IEnumerable<IPublishedContent> list, string predicate)
            where T : PublishedContentWrapped
        {
            Converter<IPublishedContent, T> listConverter =
                x => ReflectionHelpers.CreateObjectWithAnyConstructor<T>(x);
            var filteredItems = list;
            if (!string.IsNullOrWhiteSpace(predicate))
            {
                filteredItems =
                    list.Where(predicate);
            }
            return ConvertAllPublishedContentToModels<T>(filteredItems);
        }

        public SearchPagedResults<T> WhereAndCount<T>(IEnumerable<IPublishedContent> list, string predicate)
            where T : PublishedContentWrapped
        {
            var results = Where<T>(list, predicate);
            return new SearchPagedResults<T>(results, results.Count);
        }

        public IQueryable<IPublishedContent> OrderByAndGetPage(
            IEnumerable<IPublishedContent> list,
            string orderByPredicate,
            int? pageNumber,
            int? pageSize)
        {
            IQueryable<IPublishedContent> items = list.OrderBy(orderByPredicate);
            if (pageNumber.HasValue && pageSize.HasValue)
            {
                //Has pagination
                items = GetPageFromItems(items, pageNumber.Value, pageSize.Value);
            }
            return items;
        }


        public IQueryable<IPublishedContent> GetPageFromItems(IQueryable<IPublishedContent> list,
            int pageNumber,
            int pageSize)
        {
            var pageItems = list.Skip((pageNumber - 1)*pageSize).Take(pageSize);
            return pageItems;
        }

        public IEnumerable<IPublishedContent> GetPageFromItems(IEnumerable<IPublishedContent> list,
            int pageNumber,
            int pageSize)
        {
            var pageItems = list.Skip((pageNumber - 1)*pageSize).Take(pageSize);
            return pageItems;
        }
        public T ConvertPublishedContentToModel<T>(IPublishedContent item)
        {

            Converter<IPublishedContent, T> listConverter =
                x => ReflectionHelpers.CreateObjectWithAnyConstructor<T>(x);
            return listConverter(item);
        }
        public List<T> ConvertAllPublishedContentToModels<T>(IEnumerable<IPublishedContent> list)
        {
            var listCasted = new List<T>();
            foreach (var publishedContent in list)
            {
                listCasted.Add(ConvertPublishedContentToModel<T>(publishedContent));
            }
            return listCasted;
        }

        public IPublishedContent GetRootNodeFromContextCurrentItems(IPublishedContent content, bool noCache = false)
        {
            IPublishedContent rootNode = null;
            Func<IPublishedContent> loadRootNodeFunc = () => content.AncestorOrSelf(1);

            if (noCache)
            {
                rootNode = loadRootNodeFunc();
            }
            else
            {
                rootNode =_pageService.GetHttpContextItemAndIfEmptyLoadItAndStoreIt("Home", loadRootNodeFunc);
            }
            
            return rootNode;
        }

        public IPublishedContent WhereAndGetFirstOrDefault(IEnumerable<IPublishedContent> list, string predicate)
        {
            return this.Where(list, predicate).FirstOrDefault();
        }
    }
}
