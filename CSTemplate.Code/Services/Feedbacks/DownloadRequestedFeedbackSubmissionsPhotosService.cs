﻿using CS.Common.Services;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Users;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Feedbacks
{
    public interface IDownloadRequestedFeedbackSubmissionsPhotosService
    {
        byte[] Download(UmbracoHelper umbracoHelper, string absoluteUrl);
    }
    [Service]
    public class DownloadRequestedFeedbackSubmissionsPhotosService : IDownloadRequestedFeedbackSubmissionsPhotosService
    {
        private readonly IGetFeedbackRequestedSubmissionsPhotoListingService _getFeedbackRequestedSubmissionsPhotoListingService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;

        public DownloadRequestedFeedbackSubmissionsPhotosService(
            IGetFeedbackRequestedSubmissionsPhotoListingService getFeedbackRequestedSubmissionsPhotoListingService,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService,
            IGetUserFilterByUserIdService getUserFilterByUserIdService
            )
        {
            _getFeedbackRequestedSubmissionsPhotoListingService = getFeedbackRequestedSubmissionsPhotoListingService;
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
        }
        public byte[] Download(UmbracoHelper umbracoHelper, string absoluteUrl)
        {
            var stream = new MemoryStream();
            var requestedSubmissionsList = _getFeedbackRequestedSubmissionsPhotoListingService.Get();
            int columnCounter = 2;

            using (var package = new ExcelPackage())
            {
                var workSheet = package.Workbook.Worksheets.Add("FeedbackRequestedSubmissions");
                workSheet.Cells["A1"].Value = "ImageUrl";
                workSheet.Cells["B1"].Value = "ImageName";
                workSheet.Cells["C1"].Value = "Description";
                workSheet.Cells["D1"].Value = "Date";
                workSheet.Cells["E1"].Value = "Award Edition";
                workSheet.Cells["F1"].Value = "Award Category";
                workSheet.Cells["G1"].Value = "Photographer";
                

                workSheet.Cells["A1"].Style.Font.Bold = true;
                workSheet.Cells["B1"].Style.Font.Bold = true;
                workSheet.Cells["C1"].Style.Font.Bold = true;
                workSheet.Cells["D1"].Style.Font.Bold = true;
                workSheet.Cells["E1"].Style.Font.Bold = true;
                workSheet.Cells["F1"].Style.Font.Bold = true;
                workSheet.Cells["G1"].Style.Font.Bold = true;
               

                workSheet.DefaultColWidth = 60;
                workSheet.DefaultRowHeight = 30;
                if (requestedSubmissionsList != null && requestedSubmissionsList.Count > 0)
                {
                    foreach (var photo in requestedSubmissionsList)
                    {
                        var awardAndCategoryInformations = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, photo.Submission.CategoryId);
                        var photographerName = _getUserFilterByUserIdService.Get(umbracoHelper, photo.Submission.UserId);

                        var fullPhotoUrl = absoluteUrl + photo.PhotoUrl;
                        workSheet.Cells[$"A{columnCounter}"].Hyperlink = new Uri(fullPhotoUrl);
                        workSheet.Cells[$"B{columnCounter}"].Value = photo.Submission.Title;
                        workSheet.Cells[$"C{columnCounter}"].Value = photo.Submission.Description;
                        workSheet.Cells[$"D{columnCounter}"].Value = photo.Submission.Date.ToString("dd/MM/yyyy");
                        workSheet.Cells[$"E{columnCounter}"].Value = awardAndCategoryInformations.AwardItem.AwardItemName;
                        workSheet.Cells[$"F{columnCounter}"].Value = awardAndCategoryInformations.AwardCategoryItemName;
                        workSheet.Cells[$"G{columnCounter}"].Value = photographerName.Name + " " + photographerName.Surname;

                        columnCounter++;
                    }

                }


                package.Save();

                return package.GetAsByteArray();
            }

        }
    }
}
