﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Xml.Linq;

namespace CS.Common.Helpers
{
    public static class HtmlHelpers
    {
        public static List<string> GetSeparateCssClassesFromString(string cssClasses)
        {
            var items = TextHelpers.Split(cssClasses, " ");
            return items;
        }

        public static string CleanHtmlTemplateForJavascript(string html)
        {
            html = StripAttributeFromHTML(html, "id");
            return html;
        }

        public static string StripAttributeFromHTML(string html, params string[] attributes)
        {
            string sAttributes = "";
            for (int i = 0; i < attributes.Length; i++)
            {
                if (i > 0)
                {
                    sAttributes += "|";
                }
                sAttributes += attributes[i];
            }


            Regex r = new Regex("(" + sAttributes + ")=(\"|')(.*?)(\"|')", RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);
            html = r.Replace(html, "");
            return html;
        }



        public static string HtmlAttributeEncode(string attributeValue)
        {
            return HttpUtility.HtmlAttributeEncode(attributeValue);
        }
        public static string HtmlEncode(string html)
        {
            return HttpUtility.HtmlEncode(html);
        }
        public static string HtmlDecode(string html)
        {
            return HttpUtility.HtmlDecode(html);
        }
        public static string GetAttributeValueFromHTML(string html, string attribute)
        {
            string result = null;
            if (TextHelpers.CheckIfStringArrayIsNotNullOrWhiteSpace(html, attribute))
            {
                Regex r = new Regex(attribute + @"=['""]?(.*?)(['""])?[\s>]",
                                    RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);
                var match = r.Match(html);
                if (match != null)
                {
                    if (match.Groups.Count >= 1)
                    {
                        result = match.Groups[1].Value;
                    }
                    else
                    {
                        result = match.Groups[0].Value;
                    }
                }
            }
            return result;
        }


        public static string StripTargetFromAnchor(string htmlContent)
        {
            return StripAttributeFromHTML(htmlContent, "target");
        }

        private static string AddBlankTargetToAllAnchors(string htmlContent)
        {
            string result = htmlContent;
            Regex r = new Regex(@"(?<=href="")(?<url>[^""]+)(?="")", RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);
            MatchCollection matches = r.Matches(htmlContent);
            int indexMoved = 0;
            string target = " target=\"_blank\" ";
            int targetSize = target.Length;
            foreach (Match m in matches)
            {
                int pos = m.Index + indexMoved;
                result = result.Insert(pos - 7, target);
                indexMoved = indexMoved + targetSize;
            }
            return result;
        }
        /// <summary>
        /// Sanitize URLs which are www.google.com to http://www.google.com
        /// </summary>
        /// <param name="htmlContent"></param>
        /// <returns></returns>
        public static string SanitizeAbsoluteUrls(string htmlContent)
        {
            if (!string.IsNullOrWhiteSpace(htmlContent))
            {
                const string http = "$$http$$";
                const string https = "$$https$$";
                htmlContent = htmlContent.Replace("http://", http);
                htmlContent = htmlContent.Replace("https://", https);
                htmlContent = htmlContent.Replace("www.", "http://www.");
                //This might result in $$https$$http://www.google.com and replace it to $$https$$www.google.com
                htmlContent = htmlContent.Replace(https + "http://", https);
                htmlContent = htmlContent.Replace(http + "http://", http);
                htmlContent = htmlContent.Replace(http, "http://");
                htmlContent = htmlContent.Replace(https, "https://");
            }
            return htmlContent;
        }

        public static string ConvertAllAnchorTargetsToBlank(string htmlContent)
        {
            string tmp = StripTargetFromAnchor(htmlContent);
            return AddBlankTargetToAllAnchors(tmp);
        }
        public static bool IsHtml(string html)
        {
            if (html == null)
            {
                return false;
            }
            Regex r = new Regex(@"(<.*?>)|(&[#a-zA-Z0-9]+?;)", RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.Singleline);
            var match = r.Match(html);
            return match.Success;
        }

        public static string SerializeHtmlAttributes(IDictionary<string, object> attributes)
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, object> attribute in attributes)
            {
                if (attribute.Value != null)
                {
                    string sValue = attribute.Value.ToString();
                    sb.Append(string.Format(" {0}='{1}'", attribute.Key, HtmlAttributeEncode(sValue)));
                }
            }
            return sb.ToString();
        }

        public static string StripHtmlTags(string html)
        {
            if (String.IsNullOrEmpty(html)) return "";
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            return HttpUtility.HtmlDecode(doc.DocumentNode.InnerText);
        }

        public class SelectListItemWithAttributes : SelectListItem
        {
            public IDictionary<string, string> HtmlAttributes { get; set; }
        }

        public static MvcHtmlString DropDownListWithItemAttributesFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression,
           IEnumerable<SelectListItemWithAttributes> selectList,
           object htmlAttributes)
        {
            string name = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));

            var selectDoc = XDocument.Parse(htmlHelper.DropDownList(name, (IEnumerable<SelectListItem>)selectList, htmlAttributes).ToString());

            var options = from XElement el in selectDoc.Element("select").Descendants()
                          select el;

            for (int i = 0; i < options.Count(); i++)
            {
                var option = options.ElementAt(i);
                var attributes = selectList.ElementAt(i);

                foreach (var attribute in attributes.HtmlAttributes)
                {
                    option.SetAttributeValue(attribute.Key, attribute.Value);
                }
            }

            selectDoc.Root.ReplaceNodes(options.ToArray());
            return MvcHtmlString.Create(selectDoc.ToString());
        }
    }
}
