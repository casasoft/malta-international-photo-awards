﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Sitemapify.Models;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.SitemapXml
{
    public static class ConvertSitemapifyUrlsToCustomUrls
    {
        public static IEnumerable<SitemapLanguageUrl> ConvertUrlsToMultiLanguageUrls( IEnumerable<IPublishedContent> nodes, UmbracoContext umbracoContext, 
            GenerateXmlSitemapNodes generateXmlSitemapNodes, Uri uriUrl )
        {
            var sitemapValues = new List<SitemapLanguageUrl>();

            foreach (var nodeItem in nodes)
            {
                var languageItemList = new List<string>();

                var sitemapItem = generateXmlSitemapNodes.CreateSitemapUrlFromPublishedContent(nodeItem, uriUrl);

                var defaultUrl = nodeItem.Url;
                languageItemList.Add(defaultUrl);
                 
                var otherUrls = umbracoContext.UrlProvider.GetOtherUrls(nodeItem.Id);

                foreach(var url in otherUrls)
                {
                    languageItemList.Add(url);
                }

                sitemapValues.Add(new SitemapLanguageUrl(sitemapItem.Loc, languageItemList, sitemapItem.Lastmod, sitemapItem.Priority) );
            }
            return sitemapValues;
        }
    }
}
