﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.Shared
{
    public class _ForgotPasswordFormModel
    {
        public string Message { get; set; }


        [Required(ErrorMessage = "Email is required")]
        [EmailAddress]
        [DisplayName("Email Address")]
        public string InsertedEmail { get; set; }
        public string Required { get; set; }

        public string ButtonFormText { get; set; }
    }
}
