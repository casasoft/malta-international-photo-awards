﻿using System.Collections.Generic;

namespace CS.Common.Helpers
{   
    public static class LetterHelpers
    {       
        public static List<char> GetAlphabet()
        {       
            var alphabet = new List<char>();

            for (char c = 'A'; c <= 'Z'; c++)
            {
                alphabet.Add(c);
            }

            return alphabet;
        }
    }
}
