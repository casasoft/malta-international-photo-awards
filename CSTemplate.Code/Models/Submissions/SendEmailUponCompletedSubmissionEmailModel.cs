﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Submissions
{
    public class SendEmailUponCompletedSubmissionEmailModel
    {
        public string ClientName { get; set; }
        public string ClientEmailAddress { get; set; }
        public string OrderNumber { get; set; }
        public string OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public string OrderTotal { get; set; }

        public string OrderSummaryTable { get; set; }

        public string ViewOrderFullPageUrl { get; set; }
    }
}
