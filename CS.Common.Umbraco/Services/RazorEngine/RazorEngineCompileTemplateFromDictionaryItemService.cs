﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using RazorEngine;
using RazorEngine.Templating;
using Umbraco.Web;

namespace CS.Common.Umbraco.Services.RazorEngine
{

    public interface IRazorEngineCompileTemplateFromDictionaryItemService
    {
        string CompileRazorTemplateFromUmbracoDictionaryItem(
            UmbracoHelper umbracoHelper,
            Enum templateDictionaryKey,
            object model);
    }

    [Service]
    public class RazorEngineCompileTemplateFromDictionaryItemService : IRazorEngineCompileTemplateFromDictionaryItemService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public RazorEngineCompileTemplateFromDictionaryItemService(IUmbracoHelperService umbracoHelperService )
        {
            _umbracoHelperService = umbracoHelperService;
        }

        /// <summary>
        /// When testing use the RazorHelpers method as you need to actually test the result is good with the razor syntax
        /// </summary>
        /// <param name="umbracoHelper"></param>
        /// <param name="templateDictionaryKey"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string CompileRazorTemplateFromUmbracoDictionaryItem(
            UmbracoHelper umbracoHelper,
            Enum templateDictionaryKey,
            object model)
        {
            var template = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                templateDictionaryKey);
            try
            {
                var result = RazorHelpers.CompileAndEvaluateRazorTemplate(template,
                    templateDictionaryKey.ToString(),
                    model);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }
    }
}
