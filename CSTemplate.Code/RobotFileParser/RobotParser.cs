﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace CSTemplate.Code.RobotFileParser
{
    public class RobotParser
    {
        public void GenerateRobotsString(string domain)
        {
            string fileUrl = HostingEnvironment.MapPath("~/robots.txt");

            if (File.Exists(fileUrl))
            {
                string fileContents = File.ReadAllText(fileUrl);
                string sitemapLink = "";


                if (!String.IsNullOrEmpty(fileContents))
                {
                    bool sitemapKeyword = fileContents.ToLower().Contains("sitemap:");
                    if (sitemapKeyword)
                    {
                        string sitemapKeywordText = fileContents.Substring(0, fileContents.IndexOf(':') + 1);
                        sitemapLink = sitemapKeywordText + domain + "/sitemap.xml";
                    }
                    else
                    {
                        File.WriteAllText(fileUrl, String.Empty);
                    }
                }
                else
                {
                    string sitemapKeywordText = "Sitemap:";
                    sitemapLink = sitemapKeywordText + domain + "/sitemap.xml";
                }

                File.WriteAllText(fileUrl, String.Empty);
                File.WriteAllText(fileUrl, sitemapLink);
            }
        }

    }
}
