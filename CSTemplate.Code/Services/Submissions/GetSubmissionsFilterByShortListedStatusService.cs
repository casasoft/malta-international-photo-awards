﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebPages.Html;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shortlist_Photos;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Photos;
using CSTemplate.Code.Services.Users;
using umbraco;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Web;


namespace CSTemplate.Code.Services.Submissions
{

    public interface IGetSubmissionsFilterByShortListedStatusService
    {
        List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, CSTemplate.Code.Constants.Enums.SubmissionFilters shortListFilters,
            CSTemplate.Code.Constants.Enums.ShortListedStatusFilter shortListedStatus = CSTemplate.Code.Constants.Enums.ShortListedStatusFilter.NotYet);
    }

    [Service]
    public class GetSubmissionsFilterByShortListedStatusService : IGetSubmissionsFilterByShortListedStatusService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;     
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetFirstPhotoUrlFilterBySubmissionIdService _getFirstPhotoUrlFilterBySubmissionIdService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetSubmissionsFilterByShortListedStatusService
            (
                IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
                IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService,
                IGetFirstPhotoUrlFilterBySubmissionIdService getFirstPhotoUrlFilterBySubmissionIdService,
                IGetUserFilterByUserIdService getUserFilterByUserIdService,
                IUmbracoHelperService umbracoHelperService
            )
        {
            _umbracoHelperService = umbracoHelperService;
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _getFirstPhotoUrlFilterBySubmissionIdService = getFirstPhotoUrlFilterBySubmissionIdService;
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, CSTemplate.Code.Constants.Enums.SubmissionFilters shortListFilters,
            CSTemplate.Code.Constants.Enums.ShortListedStatusFilter shortListedStatus = CSTemplate.Code.Constants.Enums.ShortListedStatusFilter.NotYet)
        {
            
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var submissionsTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = Constants.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = Constants.DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = Constants.DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = Constants.DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionTitleField = DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionShortListedEnumIdField = DatabaseConstantValues.Submissions_Short_Listed_Enum_Id_Field_Name;
            var submissionShortListedDateField = DatabaseConstantValues.Submissions_Short_Listed_Date_Field_Name;
            var submissionStatusEnumIdField = DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;


            var featuredFieldId = Constants.DatabaseConstantValues.Photos_Featured_Field_Name;
            var whereCondition = string.Empty;
            if ((int)shortListedStatus == (int)CSTemplate.Code.Constants.Enums.ShortListedStatusFilter.All)
            {
                whereCondition += $@" WHERE   ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                                      
                                         ";
            }
            else
            {
                whereCondition += $@" WHERE 
                                    (
                                        {submissionsTableAlias}.{submissionShortListedEnumIdField} = {(int)shortListedStatus}
                                    )
                                        AND 
                                          ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                                        
                                         ";
            }

            
            var query = $@" 
                     SELECT
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId},
                        {submissionsTableAlias}.{submissionTitleField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionShortListedEnumIdField},
                        {submissionsTableAlias}.{submissionStatusEnumIdField}

                            FROM
                                {submissionsTableName}
                                   
                                    
            ";
            query += whereCondition;
          
            var orderByExpression = " ORDER BY {0} {1} ";
            var orderByDesc = Constants.DatabaseConstantValues.ORDER_BY_DESC;
            var orderByAsc = Constants.DatabaseConstantValues.ORDER_BY_ASC;
            switch (shortListFilters)
            {
                case CSTemplate.Code.Constants.Enums.SubmissionFilters.DateNewestFirst:
                    query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionPrimaryKeyField}", orderByDesc);
                    break;
                case CSTemplate.Code.Constants.Enums.SubmissionFilters.DateOldestFirst:
                    query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionPrimaryKeyField}", orderByAsc);
                    break;
                case CSTemplate.Code.Constants.Enums.SubmissionFilters.SubmissionNameAscending:
                    query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionTitleField}", orderByAsc);
                    break;
                case CSTemplate.Code.Constants.Enums.SubmissionFilters.SubmissionNameDescending:
                    query += string.Format(orderByExpression, $"{submissionsTableAlias}.{submissionTitleField}", orderByDesc);
                    break;

            }


            var sqlQuery = new Sql(query);

            var mySubmittedPhotosSqlResult = umbracoDb.Query<SubmissionPoco>(query).ToList();
            var listOfItemPhotoModel = new List<_ItemPhotoModel>();
            foreach (var submittedPhoto in mySubmittedPhotosSqlResult)
            {
                var statusEnum = (CSTemplate.Code.Constants.Enums.SubmissionStatus)submittedPhoto.StatusEnumID;
                var status = EnumHelpers.GetDescriptionAttributeValue(statusEnum);
                var awardCategory = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, submittedPhoto.CategoryId);
                var photographerInfos = _getUserFilterByUserIdService.Get(umbracoHelper, submittedPhoto.UserId);
                var photographerFullName = photographerInfos.Name + " " + photographerInfos.Surname;
                listOfItemPhotoModel.Add(new _ItemPhotoModel
                {
                    ItemPhotoImgURL = _getFirstPhotoUrlFilterBySubmissionIdService.Get(submittedPhoto.Id),
                    ItemPhotoDate = submittedPhoto.Date,
                    ItemPhotoName = photographerFullName,
                    ItemPhotoAward = awardCategory.AwardItem.AwardItemName,
                    ItemPhotoLocation = submittedPhoto.Location,
                    ItemPhotoDescription = submittedPhoto.Description,
                    ItemPhotoCategory = awardCategory.AwardCategoryItemName,
                    ItemPhotoTitle = submittedPhoto.Title,
                    SubmissionId = submittedPhoto.Id.ToString(),
                    ShowShortlistRadioButtons = true,
                    SubmissionStatusTitleText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper, CSTemplate.Code.Constants.Enums.DictionaryKey.Submission_Status_Title_Text),
                           			
                    ListOfRadiosValues = Enum.GetValues(typeof(Enums.ShortListedStatus)).Cast<Enums.ShortListedStatus>().ToList(),
                    ChosenRadioValue = Enums.ShortListedStatus.NotYet,
                    CurrentSubmissionStatus = status.ToString()


                });
            }
           

            return listOfItemPhotoModel;


        }
    }
}
