﻿using System;
using CS.Common.Models.Weather;
using WeatherNet.Clients;
using WeatherNet.Model;

namespace CS.Common.Services.Weather
{

    public interface IOpenWeatherService
    {
        WeatherModel GetCurrentWeatherResult
            (double latitude, double longitude, string language, string units, string apiKey);
    }

    [Service]
    public class OpenWeatherService : IOpenWeatherService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude">36.046166d</param>
        /// <param name="longitude">14.252347d</param>
        /// <param name="language"> ex: en-GB</param>
        /// <param name="units">ex: metric</param>
        /// <param name="apiKey">4e5e836010b247e50a72616ae45aafd3</param>
        /// <returns>WeatherModel</returns>
        public WeatherModel GetCurrentWeatherResult
            (double latitude, double longitude, string language, string units, string apiKey)
        {

            CurrentWeatherResult weather = null;
            try
            {

                //http://api.openweathermap.org/data/2.5/weather?lat=36.046166&lon=14.252347&units=metric&appid=4e5e836010b247e50a72616ae45aafd3
                WeatherNet.ClientSettings.SetApiKey(apiKey);
                //load the weather
                var weatherResult = CurrentWeather.GetByCoordinates(
                    latitude,
                    longitude,
                    language,
                    units);
                if (weatherResult.Success)
                {
                    weather = weatherResult.Item;
                }
            }
            catch (Exception ex)
            {
            }

            if (weather == null) return null;
            
            var weatherModel = new WeatherModel()
            {
                IconUrl = string.Format("http://openweathermap.org/img/w/{0}.png", weather.Icon),
                Alt = weather.Title,
                CurrentWeatherResult = weather,
                Temperature = Math.Round(weather.Temp) + "°C"
            };
            return weatherModel;
        }
    }
}

