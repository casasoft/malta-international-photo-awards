﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;

namespace CSTemplate.Code.Services.OrderItems
{

    public interface IGetSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService
    {
        List<OrderItemsPoco> Get(int submissionId);
    }

    [Service]
    public class GetSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService : IGetSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetSubmissionOrderItemsListingFilterByOrderItemSubmissionIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public List<OrderItemsPoco> Get(int submissionId)
        {
            var listofOrderItemsPoco = new List<OrderItemsPoco>();
            if (submissionId <= 0) return listofOrderItemsPoco;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var orderItemsTable = DatabaseConstantValues.OrderItemsTableName;
            var orderItemsSubmissionIdField = DatabaseConstantValues.OrderItems_Submission_Id_Field_Name;
            var submissionOrderItem = umbracoDb.Query<OrderItemsPoco>($"SELECT * FROM {orderItemsTable} WHERE {orderItemsSubmissionIdField} = {submissionId}").ToList();
            return submissionOrderItem;
        }
    }
}
