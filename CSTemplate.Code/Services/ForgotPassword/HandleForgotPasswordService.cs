﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Authentication;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.ForgotPassword
{

    public interface IHandleForgotPasswordService
    {
        bool HandleForgotPassword(UmbracoHelper umbracoHelper,
            _ForgotPasswordFormModel model,
            IMember member,
            string userFullName,
            string urlHost);
    }

    [Service]
    public class HandleForgotPasswordService : IHandleForgotPasswordService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly ICommonForgotPasswordService _commonForgotPasswordService;
        private readonly IGetSendEmailToMemberToResetPasswordService _getSendEmailToMemberToResetPasswordService;

        public HandleForgotPasswordService(IUmbracoHelperService umbracoHelperService,
            ICommonForgotPasswordService commonForgotPasswordService,
            IGetSendEmailToMemberToResetPasswordService getSendEmailToMemberToResetPasswordService)
        {
            _getSendEmailToMemberToResetPasswordService = getSendEmailToMemberToResetPasswordService;
            _commonForgotPasswordService = commonForgotPasswordService;
            _umbracoHelperService = umbracoHelperService;
        }

        public bool HandleForgotPassword(UmbracoHelper umbracoHelper,
           _ForgotPasswordFormModel model,
           IMember member,
           string userFullName,
           string urlHost)
        {
            try
            {
                //get reset password page url
                var resetPasswordPageUrl = _umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)
                    .Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Login_Page_Document_Type_Alias)).FirstOrDefault()
                    .Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Reset_Password_Document_Type_Alias))
                    .FirstOrDefault().UrlAbsolute();


                resetPasswordPageUrl = _commonForgotPasswordService.GetResetPasswordPageLink(ConstantValues.Memeber_Reset_Password_Guid_Alias, ConstantValues.ResetPasswordGuidFormatProvider, member, resetPasswordPageUrl);

                //sending Email to member
                _getSendEmailToMemberToResetPasswordService.Send(umbracoHelper, model.InsertedEmail, userFullName, resetPasswordPageUrl);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
