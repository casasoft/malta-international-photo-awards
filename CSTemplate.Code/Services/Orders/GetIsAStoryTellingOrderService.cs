﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;

namespace CSTemplate.Code.Services.Orders
{

    public interface IGetIsAStoryTellingOrderService
    {
        bool Get(int orderId);
    }

    [Service]
    public class GetIsAStoryTellingOrderService : IGetIsAStoryTellingOrderService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;

        public GetIsAStoryTellingOrderService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService)
        {
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }

        public bool Get(int orderId)
        {
            var result = false;
            if (orderId <= 0) return result;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var submissionTable = DatabaseConstantValues.SubmissionsTableName;
            var orderItemsTable = DatabaseConstantValues.OrderItemsTableName;

            var submissionAlias = DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var orderItemsAlias = DatabaseConstantValues.aliasForOrderItemsPocoTableNameAlias;

            var submissionPrimaryKeyField = DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var orderItemsSubmissionIdField = DatabaseConstantValues.OrderItems_Submission_Id_Field_Name;
            var orderItemsOrderIdField = DatabaseConstantValues.OrderItems_Order_Id_Field_Name;
            var sqlQuery = new Sql($@"
                SELECT {submissionAlias}.IsAStoryTelling
	                FROM {orderItemsTable}
		                INNER JOIN {submissionTable} ON {orderItemsAlias}.{orderItemsSubmissionIdField}={submissionAlias}.{submissionPrimaryKeyField}
			                WHERE {orderItemsAlias}.{orderItemsOrderIdField}={orderId}; 
            ");
            var sqlQueryResult = umbracoDb.Query<SubmissionPoco>(sqlQuery).FirstOrDefault();
            result = sqlQueryResult.IsAStoryTelling;
            return result;
        }
    }
}
