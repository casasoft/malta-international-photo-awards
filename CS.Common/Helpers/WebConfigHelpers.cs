﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CS.Common.Helpers
{
    public static class WebConfigHelpers
    {
        public static CustomErrorsMode GetRedirectMode()
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration("/");

            return ((CustomErrorsSection)config.GetSection("system.web/customErrors")).Mode;
        }
    }
}
