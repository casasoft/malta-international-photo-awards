﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.PaymentButtons;

namespace CSTemplate.Code.Services.Payment.Bilderlings
{

    public interface IGetBilderlingsPaymentModelService
    {
        BilderlingsPaymentModel Get(string orderRefId, decimal amount, string userEmail, string clientFirstName,
            string clientSurname,
            List<_OrderPhotosModel> orderItems);
    }

    [Service]
    public class GetBilderlingsPaymentModelService : IGetBilderlingsPaymentModelService
    {
        public GetBilderlingsPaymentModelService()
        {
            
        }

        public BilderlingsPaymentModel Get(string orderRefId, decimal amount, string userEmail, string clientFirstName,
            string clientSurname,
            List<_OrderPhotosModel> orderItems)
        {
            var bilderlingsPaymentModel = new BilderlingsPaymentModel()
            {
                XShopName = ConstantValues.BilderlingsShopName,
                ShopPassword = ConstantValues.BilderlingsShopPassword,
                Currency = ConstantValues.BilderlingsCurrency,
                PaymentMethod = ConstantValues.BilderlingsPaymentMethod,
                OrderRefId = orderRefId,
                amount = amount,
                XNonce = Guid.NewGuid().ToString().Replace("-", "").Trim(),
                ClientEmailAddress = userEmail
            };

            var bilderlingsSignatureCalculation = String.Format("{0}{1}{2}{3}{4}{5}{6}",
                bilderlingsPaymentModel.OrderRefId, 
                bilderlingsPaymentModel.amount.ToString(CultureInfo.InvariantCulture),
                bilderlingsPaymentModel.Currency,
                bilderlingsPaymentModel.PaymentMethod,
                bilderlingsPaymentModel.XShopName,
                bilderlingsPaymentModel.XNonce,
                bilderlingsPaymentModel.ShopPassword);

            bilderlingsPaymentModel.XRequestSignature =
                CS.Common.Helpers.CryptographyExtensions.GetHashSha512(bilderlingsSignatureCalculation);

            //get the list of products for Bilderlings
            var listOfProducts = new List<string>();
            foreach (var item in orderItems)
            {
                listOfProducts.Add(item.OrderItemTitle);
            }
            bilderlingsPaymentModel.ListOfProducts = listOfProducts;

            return bilderlingsPaymentModel;
        }
    }
}
