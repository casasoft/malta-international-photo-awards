﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.ForgotPassword;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ForgotPasswordFormController : SurfaceController
    {
        private readonly IMemberServiceWrapper _memberServiceWrapper;
        private readonly UmbracoHelper _umbracoHelper;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IHandleForgotPasswordService _handleForgotPasswordService;

        public ForgotPasswordFormController(IMemberServiceWrapper
            memberServiceWrapper,
            IUmbracoControllerWrapperService
            umbracoControllerWrapperService,
            IUmbracoHelperService
            umbracoHelperService,
            IHandleForgotPasswordService
            handleForgotPasswordService)
        {
            _handleForgotPasswordService = handleForgotPasswordService;
            _umbracoHelperService = umbracoHelperService;
            _memberServiceWrapper = memberServiceWrapper;

            _umbracoHelper = umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
        }


        [HttpPost]
        public virtual ActionResult HandleForgotPasswordFormSubmit(_ForgotPasswordFormModel forgotPasswordFormModel)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Model", _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(
                    _umbracoHelper, Enums.DictionaryKey.Forgot_Password_Email_Address_Required));

                return CurrentUmbracoPage();
            }
            else
            {
                //check if email address is valid
                var member = _memberServiceWrapper.GetByUsername(_umbracoHelper.UmbracoContext, forgotPasswordFormModel.InsertedEmail);

                if (member == null)
                {
                    ModelState.AddModelError("Model", _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(
                   _umbracoHelper, Enums.DictionaryKey.Forgot_Password_Email_Does_Not_Belong_To_Existing_Account));

                    return CurrentUmbracoPage();
                }
                else
                {
                    //member exists
                    //calling service to handleForgotPassword in order to be able to reset password
                    if (_handleForgotPasswordService.HandleForgotPassword(_umbracoHelper,
                            forgotPasswordFormModel,
                            member,
                            member.Name,
                            HttpContext.Request.Url.Host) == false)
                    {
                        ModelState.AddModelError("Model", _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(
                             _umbracoHelper, Enums.DictionaryKey.Unexpected_Error_Text));

                        return CurrentUmbracoPage();
                    }
                }
            }

            var forgotPasswordSuccessPageUrl =_umbracoHelperService.LoadRootNodeForCurrentDomain(_umbracoHelper)
                 .Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Login_Page_Document_Type_Alias)).FirstOrDefault()
                 .Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Forgot_Password_Document_Type_Alias))
                 .FirstOrDefault().Children.FirstOrDefault().Url;
             
            return Redirect(forgotPasswordSuccessPageUrl);
        }
    }
}