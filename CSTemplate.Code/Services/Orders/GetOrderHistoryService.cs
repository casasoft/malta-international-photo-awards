﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared.OrderHistoryItem;
using CSTemplate.Code.Services.Database;
using Umbraco.Core.Persistence;
using Umbraco.Web;
using static CSTemplate.Code.Constants.Enums;

namespace CSTemplate.Code.Services.Orders
{

    public interface IGetOrderHistoryFilteredByUserIdService
    {
        List<_OrderHistoryItem> Get(UmbracoHelper umbracoHelper, int userId);
    }

    [Service]
    public class GetOrderHistoryFilteredByUserIdService : IGetOrderHistoryFilteredByUserIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IGetOrderTotalCalculateService _getOrderTotalCalculateService;
        private readonly IGetViewOrderPageUrlService _getViewOrderPageUrlService;

        public GetOrderHistoryFilteredByUserIdService(IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IUmbracoHelperService umbracoHelperService, 
            IGetOrderTotalCalculateService getOrderTotalCalculateService,
            IGetViewOrderPageUrlService
            getViewOrderPageUrlService)
        {
            _getViewOrderPageUrlService = getViewOrderPageUrlService;
            _getOrderTotalCalculateService = getOrderTotalCalculateService;
            _umbracoHelperService = umbracoHelperService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public List<_OrderHistoryItem> Get(UmbracoHelper umbracoHelper,  int userId)
        {
            var listOfOrderHistoryItem = new List<_OrderHistoryItem>();
            if (userId <= 0) return listOfOrderHistoryItem;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var pocoOrdersTableName = DatabaseConstantValues.OrdersTableName;
            var pocoOrderItemsTableName = DatabaseConstantValues.OrderItemsTableName;
            var pocoSubmissionsTableName = DatabaseConstantValues.SubmissionsTableName;

            var pocoOrdersAlias = DatabaseConstantValues.aliasForOrdersPocoTableNameAlias;
            var pocoOrderItemsAlias = DatabaseConstantValues.aliasForOrderItemsPocoTableNameAlias;
            var pocoSubmissionsAlias = DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;


            var pocoOrdersPrimaryKeyField = DatabaseConstantValues.Orders_Poco_Primary_Key_Value;
            var pocoOrderItemsPrimaryKeyField = DatabaseConstantValues.OrderItems_Poco_Primary_Key_Value;
            var pocoSubmissionsPrimaryKeyField = DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;

            var pocoOrderItemsOrderIdField = DatabaseConstantValues.OrderItems_Order_Id_Field_Name;
            var pocoOrderItemsSubmissionIdField = DatabaseConstantValues.OrderItems_Submission_Id_Field_Name;


            var pocoOrdersDateField = DatabaseConstantValues.Orders_Date_Field_Name;
            var pocoOrdersPaymentStatusEnumIdField = DatabaseConstantValues.Orders_Payment_Status_Enum_Id_Field_Name;
            var pocoOrdersDescriptionField = DatabaseConstantValues.Orders_Description_Field_Name;

            var pocoSubmissionUserIdField = DatabaseConstantValues.Submissions_User_Id_Field_Name;
           
            var deleted = (int)Constants.Enums.PaymentStatus.DELETED;
            var sqlQuery = new Sql($@"
                SELECT 
	                Distinct
	                {pocoOrdersAlias}.{pocoOrdersPrimaryKeyField},
	                {pocoOrdersAlias}.{pocoOrdersDateField},
	                {pocoOrdersAlias}.{pocoOrdersPaymentStatusEnumIdField},
	                {pocoOrdersAlias}.{pocoOrdersDescriptionField}
	                FROM {pocoOrderItemsTableName}
		                INNER JOIN {pocoOrdersTableName} ON {pocoOrderItemsAlias}.{pocoOrderItemsOrderIdField}={pocoOrdersAlias}.{pocoOrdersPrimaryKeyField}
		                INNER JOIN {pocoSubmissionsTableName} ON {pocoOrderItemsAlias}.{pocoOrderItemsSubmissionIdField}={pocoSubmissionsAlias}.{pocoSubmissionsPrimaryKeyField}
			                WHERE {pocoSubmissionsAlias}.{pocoSubmissionUserIdField} = {userId} AND {pocoOrdersAlias}.{pocoOrdersPaymentStatusEnumIdField} != {deleted}
                                ORDER BY {pocoOrdersAlias}.{pocoOrdersPrimaryKeyField} DESC
        ");
            
            var orderHistoryResult = umbracoDb.Query<OrderPoco>(sqlQuery).ToList();
            			
            foreach (var order in orderHistoryResult)
            {
                PaymentStatus paymentStatus = (PaymentStatus)order.PaymentStatusEnumId;

                var orderNumber = order.Id.ToString();
                listOfOrderHistoryItem.Add(new _OrderHistoryItem
                {
                    OrderDate = order.Date,
                    OrderDescription = order.Description,
                    OrderNumber = orderNumber,
                    OrderStatus = EnumHelpers.GetDescriptionAttributeValue(paymentStatus),
                    OrderTotal = _getOrderTotalCalculateService.Get(order.Id),
                    OrderLinkUrl = _getViewOrderPageUrlService.Get(umbracoHelper, orderNumber)
                });
            }
            return listOfOrderHistoryItem;
        }
    }
}
