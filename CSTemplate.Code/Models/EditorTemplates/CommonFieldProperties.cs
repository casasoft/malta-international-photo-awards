﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Constants;

namespace CSTemplate.Code.Models.EditorTemplates
{
    public abstract class CommonFieldProperties
    {
        public Enums.DictionaryKey LabelTextDictionaryKey { get; set; }
        public Enums.DictionaryKey PlaceholderTextDictionaryKey { get; set; }
    }
}