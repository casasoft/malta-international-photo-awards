﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.DownloadShortlist;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CSTemplate.Code.Services.DownloadShortlist
{
    public interface IGetDownloadShortlistPageService
    {
        DownloadShortlistPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content);
    }


    [Service]
    public class GetDownloadShortlistPageService : IGetDownloadShortlistPageService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetDownloadShortlistPageService(
            IUmbracoHelperService umnracpoHelperService
            )
        {
            _umbracoHelperService = umnracpoHelperService;
        }

        public DownloadShortlistPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content)
        {
            var explanationText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Download_Shortlist_Explanation_Text);
            var buttonText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Download_Shortlist_Button_Text);
            DownloadShortlistPageModel downloadShortlistPageModel = new DownloadShortlistPageModel(content)
            {
                ExplanationText = explanationText,
                ButtonText = buttonText,
            };
            return downloadShortlistPageModel;
        }
    }
}
