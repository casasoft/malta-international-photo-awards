﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Services.Database;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Submissions
{

    public interface IUpdateSubmissionFeedbackRequestStatusFilterBySubmissionIdService
    {
        bool Update(UmbracoHelper umbracoHelper,int submissionId, bool feedbackRequest = true);
    }

    [Service]
    public class UpdateSubmissionFeedbackRequestStatusFilterBySubmissionIdService : IUpdateSubmissionFeedbackRequestStatusFilterBySubmissionIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetSubmissionPocoDetailsFilterByIdService _getSubmissionPocoDetailsFilterByIdService;

        public UpdateSubmissionFeedbackRequestStatusFilterBySubmissionIdService(
            
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IGetSubmissionPocoDetailsFilterByIdService getSubmissionPocoDetailsFilterByIdService
            )
        {
            _getSubmissionPocoDetailsFilterByIdService = getSubmissionPocoDetailsFilterByIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public bool Update(UmbracoHelper umbracoHelper, int submissionId, bool feedbackRequest = true)
        {
            var result = false;
            if (submissionId <= 0) return result;
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var submissionTable = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            
            var submission = _getSubmissionPocoDetailsFilterByIdService.Get(submissionId);
            submission.FeedbackRequest = feedbackRequest;

            var updateFeedbackRequest = umbracoDb.Update(submissionTable, submissionPrimaryKeyField, submission);
            result = updateFeedbackRequest > 0 ? true : false;
            return result;
        }
    }
}
