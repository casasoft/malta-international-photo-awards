﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using CS.Common.Models.Paging;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.CategoryGallery;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Models.Shared.RequestFeedbackForSubmissionForm;
using CSTemplate.Code.Models.Shared.SubmitFeedbackForm;
using CSTemplate.Code.Models.SubmissionFullPage;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Submissions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class SubmissionFullPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetSubmissionDetailsService _getSubmissionDetailsService;
        private readonly IUmbracoHelperService _umbracoHelperService;
        //private readonly IGetPagerModelService _getPagerModelService;

        public SubmissionFullPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetSubmissionDetailsService getSubmissionDetailsService,
            IUmbracoHelperService umbracoHelperService
            //IGetPagerModelService getPagerModelService
            )
        {
            //_getPagerModelService = getPagerModelService;
            _getSubmissionDetailsService = getSubmissionDetailsService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _umbracoHelperService = umbracoHelperService;
        }

        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult SubmissionFullPage(int? id, string title, RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var userId = Members.GetCurrentMemberId();
            if (id == null || id <= 0)
            {
                var pageNotFound = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, CSTemplate.Code.Constants.ConstantValues.Page_Not_Found_Data_Document_Type_Alias);
                return new RedirectToUmbracoPageResult(pageNotFound);
            }
            int submissionId = (int)id;
            SubmissionFullPageModel submissionFullPage = _getSubmissionDetailsService.Get(umbracoHelper, model.Content, submissionId, userId);

            #region Dummy Data




            //DateTime currentTime = DateTime.Now;
            //int itemTotalAmounts = 18;
            ////how many submissions to show on the page
            //submissionFullPage.ItemsPerPage = 20;
            ////total number of listing items.
            //submissionFullPage.ItemsTotalAmount = itemTotalAmounts;

            //var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper, submissionFullPage.ItemsPerPage, submissionFullPage.ItemsTotalAmount);

            //submissionFullPage.ListOfImages = new List<_ItemPhotoModel>()
            //{
            //    #region DummyData
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/mysubmis/dsadsa=111",
            //        ItemPhotoAward = "Landscape photography Award",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoDescription = "",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    },
            //    //new _ItemPhotoModel()
            //    //{
            //    //    ItemPhotoDate = currentTime,
            //    //    ItemPhotoImgURL = "/dist/assets/img/3.jpg",
            //    //    ItemPhotoAward = "Landscape photography Award",
            //    //    ItemPhotoLocation = "Vence, Italy",
            //    //    ItemPhotoDescription = "",
            //    //    ItemPhotoCategory = "nature",
            //    //    PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    //},
            //    //new _ItemPhotoModel()
            //    //{
            //    //    ItemPhotoDate = currentTime,
            //    //    ItemPhotoImgURL = "/dist/assets/img/2.jpg",
            //    //    ItemPhotoAward = "Landscape photography Award",
            //    //    ItemPhotoLocation = "Vence, Italy",
            //    //    ItemPhotoDescription = "",
            //    //    ItemPhotoCategory = "nature",
            //    //    PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    //},
            //    //new _ItemPhotoModel()
            //    //{
            //    //    ItemPhotoDate = currentTime,
            //    //    ItemPhotoImgURL = "/dist/assets/img/5.jpg",
            //    //    ItemPhotoAward = "Landscape photography Award",
            //    //    ItemPhotoLocation = "Vence, Italy",
            //    //    ItemPhotoDescription = "",
            //    //    ItemPhotoCategory = "nature",
            //    //    PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    //},
            //    //new _ItemPhotoModel()
            //    //{
            //    //    ItemPhotoDate = currentTime,
            //    //    ItemPhotoImgURL = "/dist/assets/img/4.jpg",
            //    //    ItemPhotoAward = "Landscape photography Award",
            //    //    ItemPhotoLocation = "Vence, Italy",
            //    //    ItemPhotoDescription = "",
            //    //    ItemPhotoCategory = "nature",
            //    //    PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    //},
            //    #endregion
            //};




            //submissionFullPage.SubmissionTitle = "Cameleon on rubble wall";
            //submissionFullPage.AuthorOfSubmission = "John Smith";

            //submissionFullPage.IsJury = true;
            //submissionFullPage.IsOwnerOfSubmission = true;
            //submissionFullPage.HasBeenSubmittedForFeedback = false;
            //submissionFullPage.JurySubmittedFeedback = true;
            //submissionFullPage.FeedbackAlreadyReceived = false;
            //submissionFullPage.AwardEditionCompleted = true;

            //submissionFullPage.SubmissionDate = currentTime;
            //submissionFullPage.SubmissionLocation = "Vence, Italy";
            //submissionFullPage.SubmissionDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada maximus ex, a dictum ex bibendum id. Duis consequat magna purus, vitae sagittis nisi volutpat sit amet. Aliquam ex augue, dictum eget gravida eu, consectetur vitae lorem. Vivamus risus dolor, imperdiet nec justo sed, viverra vestibulum mauris. Vivamus venenatis, orci porta fringilla luctus, dolor odio convallis elit, vitae cursus orci lectus ut massa. Aenean vel risus eget mi consectetur sollicitudin. Vivamus maximus velit ac nunc placerat ultrices. Sed pretium tellus sed quam ullamcorper laoreet. Morbi auctor nulla a nulla sagittis hendrerit.";


            ////submit feedback by Jury
            //submissionFullPage.SubmitFeedbackFormModel = new _SubmitFeedbackFormModel()
            //{
            //    SubmitFeedbackTitleText = "Submit Photo Feedback",
            //    SubmitFeedbackSubtitleText = "This user would like to receive feedback on this photo. Please submit your feedback below.",
            //    SubmissionID = "12",
            //    SubmitFeedbackButtonText = "Submit Feedback"
            //};

            ////request for feedback from photographer
            //submissionFullPage.RequestFeedbackForSubmissionFormModel = new _RequestFeedbackForSubmissionFormModel()
            //{
            //    RequestFeedbackTitleText = "Request Photo Feedback",
            //    RequestPhotoFeedbackDescription = "Click the button below to request feedback by the Jury.",
            //    RequestFeedbackButtonText = "Request Feedback",
            //    SubmissionID = "12",

            //};

            ////feedback received
            //submissionFullPage.ReceivedFeedbackTitleText = "Photo Feedback";
            //submissionFullPage.ReceivedFeedbackContent = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc finibus quam turpis, vel dapibus erat interdum in. Donec semper volutpat diam eget rutrum. Pellentesque dictum risus a tortor aliquam fermentum. Nullam efficitur nulla nec sagittis ultrices. Vivamus posuere condimentum nulla, in mollis odio imperdiet et. Nulla ac ligula at tellus iaculis viverra a non lorem. Nunc bibendum ultrices mollis. Integer mattis, dui in porta pulvinar, mauris metus commodo erat, eu vestibulum ante orci sed justo.";


            //submissionFullPage.AuthorsOtherSubmissionsParagraph = "Other submissions by Joseph Cutajar";
            //submissionFullPage.ListingOfAuthorsOtherSubmissions = new _ListingItemsAsMasonryPartialModel()
            //{
            //    ListOfPhotos = new List<_ItemPhotoModel>()
            //    {
            //        #region DummyData

            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/1.jpg",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/2.jpg",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test2/",
            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/3.jpg",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoAward = "Landscape photography Award",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/4.jpg",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/5.jpg",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoAward = "Landscape photography Award",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "war",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/1.jpg",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/2.jpg",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    },
            //    new _ItemPhotoModel()
            //    {
            //        ItemPhotoDate = currentTime,
            //        ItemPhotoImgURL = "/dist/assets/img/3.jpg",
            //        ItemPhotoLocation = "Vence, Italy",
            //        ItemPhotoAward = "No awards",
            //        ItemPhotoDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit elit sed laoreet gravida. Etiam feugiat pretium aliquam.",
            //        ItemPhotoCategory = "nature",
            //        PhotoFullPageUrl = "/members-area/my-submissions/my-submission-test1/",
            //    },
            //    #endregion
            //    },
            //    Pagination = pagerModel
            //};
            #endregion


            var successMessageQueryStringText = Request.QueryString[
                CSTemplate.Code.Constants.ConstantValues.Jury_Profile_Submit_Feedback_Query_String];


            if (!String.IsNullOrEmpty(successMessageQueryStringText))
            {
                submissionFullPage.SuccessMessage = successMessageQueryStringText;
            }

            return base.Populate(submissionFullPage);
        }
    }
}