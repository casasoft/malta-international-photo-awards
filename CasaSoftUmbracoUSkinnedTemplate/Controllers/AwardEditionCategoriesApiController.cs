﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using Umbraco.Core.Models;
using System.Web.Http;
using CSTemplate.Code.Services.AwardCategory;
using Newtonsoft.Json;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public class AwardEditionCategoriesApiController : UmbracoApiController
    {
      
      
        private readonly IGetAwardEditionCategoryListFilterByAwardEditionId _getAwardEditionCategoryListFilterByAwardEditionId;
        public AwardEditionCategoriesApiController(
           
            IGetAwardEditionCategoryListFilterByAwardEditionId getAwardEditionCategoryListFilterByAwardEditionId
            )
        {
           
            _getAwardEditionCategoryListFilterByAwardEditionId = getAwardEditionCategoryListFilterByAwardEditionId;
        }

        [System.Web.Http.HttpPost]
        public List<System.Web.WebPages.Html.SelectListItem> GetAwardEditionCategories(int awardEditionId)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var awardEditionCategoryList = _getAwardEditionCategoryListFilterByAwardEditionId.Get(umbracoHelper, awardEditionId);
            var jsonSerialize= JsonConvert.SerializeObject(awardEditionCategoryList);
            return awardEditionCategoryList;
        }
    }
}