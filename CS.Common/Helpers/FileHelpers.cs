﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using CS.Common.Services.Application;
using CS.Common.Services.IO;
using CS.Common.Services.Page;
using CS.Common.Constants;

namespace CS.Common.Helpers
{
    public class FileHelpers
    {
        /// <summary>
        /// Creates a directory, from a path that may also include a filename.  E.g 'c:\test\floor.jpg'
        /// </summary>
        /// <param name="dirPath">The path</param>
        public static void CreateDirectory(string dirPath)
        {
          
            int lastSlash = NormaliseToLocalPath(dirPath).LastIndexOf("\\");
            if (lastSlash > -1)
            {
                int dotPos = dirPath.IndexOf(".", lastSlash);
                if (dotPos > -1)
                    dirPath = dirPath.Substring(0, lastSlash + 1);
                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);
            }


        }

        public static void ConvertExcelToCSV(string sourceFile, string worksheetName, string targetFile)
        {
            string strConn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sourceFile + @";Extended Properties=""Excel 12.0 Xml;HDR=No;IMEX=1""";
            OleDbConnection conn = null;
            StreamWriter wrtr = null;
            OleDbCommand cmd = null;
            OleDbDataAdapter da = null; 
            try
            {
                conn = new OleDbConnection(strConn);
                conn.Open();
 
                cmd = new OleDbCommand("SELECT * FROM [" + worksheetName + "$]", conn);
                cmd.CommandType = CommandType.Text;
                wrtr = new StreamWriter(targetFile);
 
                da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
 
                for (int x = 0; x < dt.Rows.Count; x++)
                {
                    string rowString = "";
                    for (int y = 0; y < dt.Columns.Count; y++)
                    {
                        rowString += "\"" + dt.Rows[x][y].ToString() + "\",";
                    }
                    wrtr.WriteLine(rowString);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                    conn.Dispose();
                    cmd.Dispose();
                    da.Dispose();
                    wrtr.Close();
                    wrtr.Dispose();
                }
            }
        }

        public static bool IsFileValidImage(FileInfo file)
        {
            bool isValid = false;
            var ext = GetExtension(file.FullName).ToLower();
            switch (ext)
            {
                case "jpg":
                case "jpeg":
                case "gif":
                case "bmp":
                case "png":
                    {
                        isValid = true;
                    }
                    break;
            }
            return isValid;
        }

        public static FileSystemWatcher WatchDirectoryForChanges(
            string path, 
            FileSystemEventHandler methodToCallOnChange,
            bool includeSubDirectories = true)
        {
            return watchForChanges(
               path: path,
               methodToCallOnChange: methodToCallOnChange,
               watchSingleFile: false,
               watchAttributes: false,
               includeSubDirectories: includeSubDirectories);
        }

        private static FileSystemWatcher watchForChanges(
            string path,
            FileSystemEventHandler methodToCallOnChange,
            bool watchSingleFile = false,
            bool watchAttributes = false,
            bool includeSubDirectories = true)
        {
            if (methodToCallOnChange == null)
                throw new InvalidOperationException("Helpers.IO.WatchFile:: methodToCallOnChange cannot be null");

            string dirName = GetDirectoryName(path);
            var fileWatcher = new FileSystemWatcher(dirName);

            var filters = 
                NotifyFilters.Size |
                NotifyFilters.LastWrite |
                NotifyFilters.LastAccess |
                NotifyFilters.FileName;

            if (watchAttributes) filters = filters | NotifyFilters.Attributes;
            string fileNameToWatch = null;
            if (watchSingleFile)
            {
                fileNameToWatch = GetFilenameAndExtension(path);
            }
            else
            {
                fileNameToWatch = "*.*";
            }
            fileWatcher.Filter = fileNameToWatch;
            fileWatcher.NotifyFilter = filters;
            fileWatcher.IncludeSubdirectories = includeSubDirectories;
            fileWatcher.Changed += methodToCallOnChange;
            fileWatcher.EnableRaisingEvents = true;

            return fileWatcher;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path">Server.MapPath</param>
        /// <param name="methodToCallOnChange"></param>
        /// <param name="watchAttributes">Whether to watch the actual attributes of files and folders</param>
        /// <param name="includeSubDirectories"></param>
        /// <returns></returns>
        public static FileSystemWatcher WatchFileForChanges(
            string path, 
            FileSystemEventHandler methodToCallOnChange,
            bool watchAttributes = false,
            bool includeSubDirectories = true)
        {
            return watchForChanges(
                path: path,
                methodToCallOnChange: methodToCallOnChange,
                watchSingleFile: true,
                watchAttributes: watchAttributes,
                includeSubDirectories: includeSubDirectories);
        }
        /// <summary>
        /// Returns ONLY the filename (excluding extension)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFilenameAndExtension(string path)
        {
            if (path != null)
            {
                path = path.Replace("/", "\\");
                if (path == null) path = "";
                int slashPos = path.LastIndexOf("\\");
                if (slashPos != -1)
                    path = path.Substring(slashPos + 1);
            }
            return path;
        }
        /// <summary>
        /// Returns the directory name of a specified path.  For example:
        /// 
        /// C:\Karl\Test\1.jpg returns "C:\Karl\Test\"
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string GetDirectoryName(string filename)
        {
            int indexOfSlash = filename.LastIndexOf('\\');
            if (indexOfSlash == -1)
            {
                indexOfSlash = filename.LastIndexOf('/');
            }
            if (indexOfSlash != -1)
            {
                indexOfSlash++;
            }
            string s = filename;
            if (indexOfSlash > -1)
            {
                int dotPos = s.IndexOf('.', indexOfSlash);
                if (dotPos > -1)
                {
                    s = (filename.Substring(0, indexOfSlash));
                }
            }
            return s;
        }


        public static string MapPathRoot { get; set; }



        /// <summary>
        /// Resolves a web path, e.g /App_Data to the local equivalent
        /// </summary>
        /// <param name="pathToResolve"></param>
        /// <returns></returns>
        public static string MapWebPathToLocal(string pathToResolve)
        {
            
            string path = pathToResolve;
            path = path.Replace("//", "/");
            path = path.Replace("\\\\", "\\");
            var pageService = IocHelpers.Get<IPageService>();
            var ctx = pageService.GetCurrentHttpContext();
            if (ctx != null)
            {
                string s = "";
                if (path.Contains("/"))
                {

                    try
                    {
                        s = ctx.Server.MapPath(path);
                    }
                    catch (Exception )
                    {
                        if (path.StartsWith("/"))
                        {
                            string basePath = ctx.Server.MapPath("/");
                            s = basePath + path.Substring(1);
                            s = s.Replace("/", "\\");
                        }
                        else
                            throw;
                    }
                }

                else
                {
                    s = ctx.Server.MapPath(path);
                    //s = path;
                }
                return s;
            }
            else
            {
                string s = null;
                if (path.StartsWith("~")) path = path.Substring(1);
                if (path.StartsWith("/")) path = path.Substring(1);
            var applicationService = IocHelpers.Get<IApplicationService>();
                string rootPath = applicationService.GetApplicationRootLocalFolder();
                if (rootPath.EndsWith("\\"))
                    rootPath = rootPath.Substring(0, rootPath.Length - 1);
                while (path.StartsWith("../"))
                {
                    int lastIndexOfSlash = rootPath.LastIndexOf("\\");
                    if (lastIndexOfSlash >= 0)
                    {
                        rootPath = rootPath.Substring(0, lastIndexOfSlash);

                    }
                    else
                    {
                        throw new InvalidOperationException("Not enough folders to traverse path");
                    }
                    path = path.Substring(3);
                }

                s = rootPath + "\\";

                //if (!s.EndsWith("\\"))
                //s += "\\";
                path = path.Replace("/", "\\");
                if (path.StartsWith("\\"))
                    path = path.Remove(0, 1);
                s += path;


                return s;
            }
        }


       
        
        public static void StopFileWatcher(FileSystemWatcher fileWatch)
        {
            fileWatch.EnableRaisingEvents = false;
            fileWatch.Dispose();
        }

        static void fileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Moves file from A to B, and overwrites any existing files.  No errors are given
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public static void MoveOrRenameFile(string from, string to)
        {
            if (from.Contains("/")) from = FileHelpers.MapWebPathToLocal(from);
            if (to.Contains("/")) to = FileHelpers.MapWebPathToLocal(to);
            CreateDirectory(to);
            if (File.Exists(from))
            {
                //The reason this is done as a copy/write, rather than an actual move is because if so, the file
                //is moved with it's original permissions.  If you re-write it, it is created with the permissions based on that folder / logged in user
                //this was an issue in Cfp Formatter, for Advices, as they were being created in Temp folder, and then moved to the actual location,
                //without any access for the IIS User.
                DeleteFile(to);
                var fileStreamRead = File.OpenRead(from);
                var fileStreamWrite = File.OpenWrite(to);
                fileStreamRead.CopyTo(fileStreamWrite);
                fileStreamRead.Dispose();
                fileStreamWrite.Dispose();
                DeleteFile(from);

//                File.Move(from, to);
            }
            
            
        }
        public static string EnsurePathEndsWithSlash(string path, string slashType = null)
        {


            if (!string.IsNullOrEmpty(path))
            {
                if (slashType == null)
                {
                    if (path.Contains("/"))
                        slashType = "/";
                    else
                        slashType = "\\";
                }

                if (!path.EndsWith(slashType))
                    path += slashType;
            }
            return path;
        }
        public static string EnsurePathStartsAndEndsWithSlash(string path, string slashType = null)
        {
            string s = EnsurePathStartsWithSlash(path, slashType);
            s = EnsurePathEndsWithSlash(path, slashType);
            return s;
        }
        public static string EnsurePathDoesNotStartWithSlash(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {

                if (path.StartsWith("\\") || path.StartsWith("/"))
                    path = path.Remove(0, 1);
            }
            return path;
        }
        public static string EnsurePathDoesNotContainDuplicateSlashes(string path)
        {
            string s = path;
            if (!string.IsNullOrEmpty(s))
            {
                s = TextHelpers.ReplaceTexts(s, "/", "////", "///", "//");
                s = TextHelpers.ReplaceTexts(s, @"\", @"\\\\",@"\\\",@"\\");
                
            }
            return s;
        }
        public static string EnsurePathDoesNotEndWithSlash(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {

                if (path.EndsWith("\\") || path.EndsWith("/"))
                    path = path.Substring(0, path.Length -1);
            }
            return path;
        }
        public static string EnsurePathDoesNotStartButEndsWithSlash(string path, string slashType = null)
        {
            
            path = EnsurePathDoesNotStartWithSlash(path);
            path = EnsurePathEndsWithSlash(path, slashType);
            
            return path;
        }

        /// <summary>
        /// This will remove any characters which are invalid in a Windows file name. Such as slashes.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string RemoveAnyInvalidCharactersFromString(string name)
        {
            var replaced = TextHelpers.ReplaceTexts(name, "", @"\", @"/", @":", @"?", @">", @"<", "\"", "|");
            return replaced;
        }

        public static string EnsurePathStartsWithSlash(string path, string slashType = null)
        {


            if (!string.IsNullOrEmpty(path))
            {
                if (slashType == null)
                {
                    if (path.Contains("/"))
                        slashType = "/";
                    else
                        slashType = "\\";
                }

                if (!path.StartsWith(slashType))
                    path = slashType + path;
            }
            return path;
        }

        public static System.Text.Encoding GetFileTextEncoding(byte[] bom)
        {
            System.Text.Encoding enc = null;
            if (bom.Length >= 3 && bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf)
            {
                enc = System.Text.Encoding.UTF8;
            }
            else if (bom.Length >= 2 && bom[0] == 0xff && bom[1] == 0xfe)
            {
                enc = System.Text.Encoding.Unicode;
            }
            else if (bom.Length >= 2 && bom[0] == 0xfe && bom[1] == 0xff)
            {
                enc = System.Text.Encoding.Unicode;
            }
            else if ((bom.Length >= 4 && bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff))
            {
                enc = System.Text.Encoding.Unicode;
            }
            else
            {
                enc = System.Text.Encoding.UTF8;
            }
            return enc;

        }


        /// <summary>
        /// Retrieves files from dir based on extensions.  
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="extensions">Extensions can include even separators like ',', ';','|'</param>
        /// <returns></returns>
        public static List<FileInfo> GetFilesInDirByExtensions(string dir, params string[] extensions)
        {
            return GetFilesInDirByExtensions(new DirectoryInfo(dir), extensions);
        }
        /// <summary>
        /// Retrieves files from dir based on extensions.  
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="extensions">Extensions can include even separators like ',', ';','|'</param>
        /// <returns></returns>
        public static List<FileInfo> GetFilesInDirByExtensions(DirectoryInfo dir, params string[] extensions)
        {
            string pattern = @".*?\.(";
            
            
            for (int i=0; i < extensions.Length;i++)
            {
                var extensionStr = extensions[i];
                string[] tokens = extensionStr.Split(new string[] {",",";","|"}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var ext in tokens)
                {
                    if (!string.IsNullOrEmpty(pattern))
                        pattern += "|";
                    pattern += ext.Trim();
                }
            }
            pattern += ")";

            var files = FindFilesInFolder(dir.FullName, pattern, true, true);
            return files;

        }
        /// <summary>
        /// Recursively
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="regExp"></param>
        /// <param name="regexOptions"></param>
        /// <returns></returns>
        public static List<FileInfo> GetFilesInDirByRegExp(string dir, string regExp, RegexOptions regexOptions)
        {
            return GetFilesInDirByRegExp(new DirectoryInfo(dir),regExp,regexOptions);
        }
        /// <summary>
        /// Recursively
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="regExp"></param>
        /// <param name="regexOptions"></param>
        /// <returns></returns>
        public static List<FileInfo> GetFilesInDirByRegExp(DirectoryInfo dir, string regExp, RegexOptions regexOptions)
        {
            return FindFilesInFolder(dir.FullName, regExp, true, true);
        }
        public static string ConvertByteArrayToHex(byte[] byArray)
        {
            StringBuilder hex = new StringBuilder(byArray.Length * 2);
            foreach (byte b in byArray)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }
        public static byte[] ConvertHexStringToByteArray(string hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string ConvertStringToFilename(string s)
        {
            return ConvertStringToFilename(s, 999);
        }
        public static string ConvertStringToFilename(string s,int maxLen)
        {
            
            if (s == null)
                s = "";
            if (s.Length > maxLen)
            {
                s = s.Substring(0, maxLen);
            }
            s = TextHelpers.ReplaceTexts(s, "-and-", "&");
            List<string> charactersToReplace = new List<string>() {" ","‘","’","‛","“","”","","„","‟","`","ʼ","'","\"",".....","....","...","..", "+", "\\", "/", ":", "*", "?", "\"", "<", ">", "|","#","%"};
            foreach (var c in System.IO.Path.GetInvalidFileNameChars())
            {
                charactersToReplace.Add(c.ToString());
            }
            foreach (var c in System.IO.Path.GetInvalidPathChars())
            {
                charactersToReplace.Add(c.ToString());
            }


            
            s = TextHelpers.ReplaceTexts(s, "-", charactersToReplace.ToArray());
            
            s = TextHelpers.ReplaceTexts(s, "-", "--", "---", "----", "-----");
            s = TextHelpers.ReplaceTexts(s, "-", "--", "---", "----", "-----");
            s = TextHelpers.ReplaceTexts(s, "-", "--", "---", "----", "-----");
            while (s.StartsWith("."))
            {
                s = s.Remove(0, 1);
            }

            return s;
        }
        public static long GetFileSizeInBytes(string path)
        {
            long size = 0;
            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                size = fs.Length;
                fs.Dispose();
            }
            return size;

        }

        public static int GetFileSizeInKB(string path)
        {
            long bytes = GetFileSizeInBytes(path);
            return GetBytesInKB(bytes);
        }

        public static int GetBytesInKB(long bytes)
        {
            long kb = bytes / 1024;
            return (int)kb;
        }
        /// <summary>
        /// Empties a directory, recursively
        /// </summary>
        /// <param name="path"></param>
        public static void EmptyDirectory(string path)
        {
            EmptyDirectory(path, true);
        }

        /// <summary>
        /// Empty Directory
        /// </summary>
        /// <param name="path"></param>
        /// <param name="recursive"></param>
        public static void EmptyDirectory(string path, bool recursive)
        {

            DirectoryInfo dir = new DirectoryInfo(path);
            FileInfo[] files = dir.GetFiles();
            for (int i = 0; i < files.Length; i++)
            {
                files[i].Delete();
            }
            
            if (recursive)
            {
                DirectoryInfo[] dirs = dir.GetDirectories();
                foreach (var d in dirs)
                {
                    EmptyDirectory(d.FullName, true);
                }
            }

        }

        /// <summary>
        /// Deletes a file, without any errors
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteFile(string path)
        {
            if (path.Contains("/"))
            {
                path = FileHelpers.MapWebPathToLocal(path);
            }

            if (File.Exists(path))
            {
                File.Delete(path);
            }


        }

        /// <summary>
        /// Deletes a directory, recursively
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteDirectory(string path)
        {
            DeleteDirectory(path, true);
        }
        public static void DeleteDirectory(string path, bool recursive)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            if (recursive)
            {
                DirectoryInfo[] dirs = dir.GetDirectories();
                foreach (var d in dirs)
                {
                    DeleteDirectory(d.FullName, true);
                }
            }
            EmptyDirectory(path, recursive);
            System.IO.Directory.Delete(path);
            

        }
        /// <summary>
        /// Returns the contents of the specified file.  File must be a full path.
        /// </summary>
        /// <param name="filename">Filename (Full path, using Server.MapPath)</param>
        /// <returns>The contents of the file</returns>
        public static string LoadFileContentsAsString(string filename)
        {
            return LoadFileContentsAsString(filename, System.Text.Encoding.UTF8);

        }
        /// <summary>
        /// Returns the contents of the specified file.  File must be a full path.
        /// </summary>
        /// <param name="filename">Filename (Full path, using Server.MapPath)</param>
        /// <returns>The contents of the file</returns>
        public static string LoadFileContentsAsString(string filename, System.Text.Encoding encoding)
        {
            if (File.Exists(filename))
            {
                FileStream file = IocHelpers.Get<IFileService>().FileOpen(filename, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file,encoding);
                string tmp = sr.ReadToEnd();
                sr.Close();
                file.Close();
                file.Dispose();
                return (tmp);
            }
            else
                return null;

        }
        /// <summary>
        /// Creates a temporary dir and returns the path. Guarantees uniqueness, and ends with a slash '\'
        /// </summary>
        /// <returns></returns>
        public static string CreateTempDir()
        {
            string tmpPath = System.IO.Path.GetTempPath();
            string folder;
            while (true)
            {
                folder = tmpPath + RandomHelpers.GetGuid(true)  + "_" + RandomHelpers.GetString(5, 5) + "\\";
                if (!System.IO.Directory.Exists(folder))
                {
                    CreateDirectory(folder);
                    break;
                }
            }
            return folder;
        }
        /// <summary>
        /// Creates a temporary dir and returns the path. Guarantees uniqueness
        /// </summary>
        /// <returns></returns>
        public static string GetTempFilename(string extension)
        {
            if (extension.StartsWith(".")) extension = extension.Substring(1);
            string tmpPath = System.IO.Path.GetTempPath();
            string path= "";
            while (true)
            {

                string filename = RandomHelpers.GetGuid(removeDashes:true) + RandomHelpers.GetString(5, 5) + "." + extension;
                path = tmpPath + filename;
                if (!System.IO.File.Exists(path))
                {
                    SaveToFile(path, " ");
                    break;
                }
                
            }
            return path;
        }
        /// <summary>
        /// Returns the directory name of a specified path.  For example:
        /// 
        /// C:\Karl\Test\1.jpg returns "C:\Karl\Test\"
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string GetDirName(string filename)
        {
            int indexOf = filename.LastIndexOf('\\');
            if (indexOf == -1)
                indexOf = filename.LastIndexOf('/');
            if (indexOf != -1)
                indexOf++;
            if (indexOf > -1)
                return (filename.Substring(0, indexOf));
            else
                return null;
        }
        /// <summary>
        /// Returns ONLY the filename (excluding extension) - E.g test or c:\folder\test
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFilenameOnly(string path)
        {
            if (path != null)
            {
                path = GetFilenameAndExtension(path);
                int dotPos = path.LastIndexOf('.');
                if (dotPos != -1)
                {
                    path = path.Substring(0, dotPos);
                }
            }
            return path;
        }
        

        public static string ChangeExtension(string path, string newExtension)
        {
            string s = GetDirName(path) + GetFilenameOnly(path) + "." + newExtension;
            return s;
        }
        /// <summary>
        /// Returns the extension of the file, example 'photo.jpg' returns 'jpg'. In Lowercase
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <returns>Extension only (without dot)</returns>
        public static string GetExtension(string filename)
        {
            if (filename == null) filename = "";
            int pos;
            pos = filename.LastIndexOf(".");
            string ext = "";
            if (pos > -1)
                ext = (filename.Substring(pos + 1));
            else
                ext = filename;

            if (ext.Contains("?"))
            {
                ext = ext.Substring(0, ext.IndexOf("?"));
            }

            return ext.ToLower();

        }

        public static Enums.MediaItemType GetFileExtensionType(string filename)
        {
            var extension = GetExtension(filename);
            if (extension == "jpeg" || extension == "jpg" || extension == "png" || extension == "gif" || extension == "bmp")
            {
                return Enums.MediaItemType.Image;
            }
            if (extension == "swf")
            {
                return Enums.MediaItemType.Flash;
            }
            if (extension == "doc" || extension == "docx")
            {
                return Enums.MediaItemType.Document;
            }
            if (extension == "wmv" || extension == "mov" || extension == "mp4" || extension == "flv")
            {
                return Enums.MediaItemType.Video;
            }
            if (extension == "mp3" || extension == "wav" || extension == "mp3a" || extension == "wma")
            {
                return Enums.MediaItemType.Music;
            }
            if (extension == "txt" || extension == "nfo")
            {
                return Enums.MediaItemType.Textfile;
            }
            if (extension == "xls" || extension == "xlsx" || extension == "csv")
            {
                return Enums.MediaItemType.Spreadsheet;
            }
            if (extension == "ppt" || extension == "pptx")
            {
                return Enums.MediaItemType.Powerpoint;
            }
            if (extension == "pdf")
            {
                return Enums.MediaItemType.PDF;
            }
            else
            {
                return Enums.MediaItemType.General;
            }

        }
        /// <summary>
        /// Saves to file, appends if exists
        /// </summary>
        /// <param name="path"></param>
        /// <param name="txt"></param>
        public static void AppendToFile(string path, string txt)
        {
            string dir = path.Substring(0, path.LastIndexOf('\\') + 1);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            FileStream fs = new FileStream(path, FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);
            sw.Write(txt);
            sw.Close();
            fs.Close();

        }

        /// <summary>
        /// Saves to file. Creates a new one
        /// </summary>
        /// <param name="path"></param>
        /// <param name="txt"></param>
        public static void SaveToFile(string path, string txt)
        {
            IocHelpers.Get<IFileService>().SaveToFile(path, txt);


        }
        public static void SaveToFile(string path, byte[] file)
        {
            IocHelpers.Get<IFileService>().SaveToFile(path, file);

        }
        public static byte[] LoadFileAsByteArray(string path)
        {
            return File.ReadAllBytes(path);

            //ArrayList content = new ArrayList();
            //FileStream reader = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            //int cnt = 0;
            //int tot = 0;
            //byte[] buffer;
            //do
            //{
            //    buffer = new byte[8192];
            //    cnt = reader.Read(buffer, 0, buffer.Length);
            //    tot += cnt;
            //    content.Add(buffer);

            //} while (cnt == buffer.Length);
            //byte[] all = new byte[tot];
            //int k = 0;
            //for (int i = 0; i < content.Count; i++)
            //{
            //    if (tot == 0) break;
            //    byte[] currBuffer = (byte[])content[i];
            //    for (int j = 0; j < currBuffer.Length; j++)
            //    {
            //        all[k] = currBuffer[j];
            //        k++;
            //        tot--;
            //        if (tot == 0) break;
            //    }
            //    currBuffer = null;
            //}
            //content.Clear();
            //reader.Close();

            //return all;
        }

        public static byte[] ReadStreamIntoByteArray(System.IO.Stream stream)
        {
            int bufferSize = 32768;
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[bufferSize];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }
        public static byte[] ReadStreamIntoByteArray_old(Stream s)
        {
            
            MemoryStream ms = new MemoryStream();
            byte[] buffer = new byte[512 * 1024];
            int readCnt = 0;
            do
            {
                readCnt = s.Read(buffer, 0, buffer.Length);
                if (readCnt > 0)
                {
                    ms.Write(buffer, 0, readCnt);
                }
            }
            while (readCnt > 0);
            byte[] data = new byte[ms.Length];
            ms.Seek(0, SeekOrigin.Begin);
            ms.Read(data,0,(int)data.Length);
            return data;

        }

        public static void SaveStreamToFileByReadingAsByteArrayFirst(Stream inStream, string path)
        {
            var bytes = FileHelpers.ReadStreamIntoByteArray(inStream);
            File.WriteAllBytes(path, bytes);
        }

        /// <summary>
        /// Saves a stream to a file. Overwrites any existing file
        /// </summary>
        /// <param name="inStream">Stream</param>
        /// <param name="path">Path of file</param>
        public static void SaveStreamToFile(Stream inStream, string path)
        {
            if (inStream.CanSeek)
            {
                inStream.Seek(0, SeekOrigin.Begin);
                inStream.Position = 0;
            }
            string dirName = GetDirName(path);
            Directory.CreateDirectory(dirName);
            

            //using (var tmpFile = File.OpenWrite(path))
            using (var tmpFile = File.Open(path, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
            {
                if (inStream is MemoryStream)
                {
                    MemoryStream ms = (MemoryStream)inStream;
                    ms.WriteTo(tmpFile);
                }
                
                else
                {
                    inStream.CopyTo(tmpFile);
                }
                tmpFile.Close();

            }
            
            //File.SetAccessControl(path, FileSecurity.
        }
        /// <summary>
        /// Saves a stream to a file. Overwrites any existing file
        /// </summary>
        /// <param name="inStream">Stream</param>
        /// <param name="path">Path of file</param>
        public static void SaveStreamToFile_old(Stream inStream, string path)
        {

            Directory.CreateDirectory(GetDirName(path));
            FileStream tmpFile = File.Open(path, FileMode.Create, FileAccess.Write);
            //StreamWriter writer = new StreamWriter(tmpFile);
            //StreamReader reader = new StreamReader(inStream);
            byte[] buffer = new byte[32768];
            inStream.Seek(0, SeekOrigin.Begin);
            while (inStream.Position < inStream.Length)
            {
                int cnt = inStream.Read(buffer, 0, buffer.Length);
                tmpFile.Write(buffer, 0, cnt);
            }
            tmpFile.Close();
            tmpFile.Dispose();
        }
        
        public static string GetStringFromStream(Stream s)
        {
            return GetStringFromStream(s, System.Text.Encoding.UTF7);
        }
        public static string GetStringFromStream(Stream s, System.Text.Encoding encoding)
        {
            StreamReader sr = new StreamReader(s, encoding,true);
            string str = sr.ReadToEnd();
            sr.Close();
            return str;
        }
        private static void findFilesInFolder(DirectoryInfo dir, string filename, bool filenameIncludesRegExp, bool recursive, List<FileInfo> files)
        {
            if (dir.Exists)
            {
                FileInfo[] dirFiles = dir.GetFiles();
                for (int i = 0; i < dirFiles.Length; i++)
                {
                    string currFile = dirFiles[i].Name;
                    string regExp = "^" + (filenameIncludesRegExp ? filename : TextHelpers.ForRegExp(filename) + "$");
                    var match = Regex.Match(currFile, regExp, RegexOptions.IgnoreCase);
                    if (match.Success)
                    {
                        files.Add(dirFiles[i]);
                    }
                }
                if (recursive)
                {
                    DirectoryInfo[] dirsInDir = dir.GetDirectories();
                    foreach (var d in dirsInDir)
                    {
                        findFilesInFolder(d, filename, filenameIncludesRegExp, recursive, files);
                    }
                }
            }

        }
        public static List<FileInfo> FindFilesInFolder(string directory, string filename, bool filenameIncludesRegExp, bool recursive)
        {
            DirectoryInfo dir = new DirectoryInfo(directory);
            List<FileInfo> list = new List<FileInfo>();
            findFilesInFolder(dir, filename, filenameIncludesRegExp, recursive, list); 
            return list;
        }
        public static System.Text.Encoding GetEncodingBasedOnFilename(string filename)
        {
            System.Text.Encoding enc = null;
            string ext = GetExtension(filename);
            switch (ext)
            {
                case "csv": enc = System.Text.Encoding.UTF7; break;
                    
            }
            return enc;
        }
        public static void CopyDirectoryTo(string fromDir, string toDir)
        {
            if (!fromDir.EndsWith("\\")) fromDir += "\\";
            if (!toDir.EndsWith("\\")) toDir += "\\";
            if (Directory.Exists(fromDir))
            {
                DirectoryInfo fromDirInfo = new DirectoryInfo(fromDir);
                CreateDirectory(toDir);
                FileInfo[] files = fromDirInfo.GetFiles();
                for (int i = 0; i < files.Length; i++)
                {
                    string toPath = toDir + files[i].Name;
                    try
                    {
                        files[i].CopyTo(toPath);
                    }
                    catch { }
                }
                DirectoryInfo[] subDirs = fromDirInfo.GetDirectories();
                for (int i = 0; i < subDirs.Length; i++)
                {
                    CopyDirectoryTo(subDirs[i].FullName, toDir + subDirs[i].Name);
                }


            }
        }
        public static void MoveToOrCopyDirectory(string fromDir, string toDir)
        {
            if (Directory.Exists(toDir)) DeleteDirectory(toDir);
            if (Directory.Exists(fromDir))
            {
                DirectoryInfo fromDirInfo = new DirectoryInfo(fromDir);
                bool ok = false;
                try
                {
                    fromDirInfo.MoveTo(toDir);
                    ok = true;
                }
                catch
                {
                    ok = false;
                }
                if (!ok)
                {
                    CopyDirectoryTo(fromDir, toDir);
                }


            }
        }
       

       

        public static string ConvertPathToShorterPathWithEllipses(string path, int charsToShowOnLeft, int charsToShowOnRight)
        {
            string txt = "";
            if (path.Length <= (charsToShowOnLeft + charsToShowOnRight))
                txt =path;
            else
            {
                txt = path.Substring(0, charsToShowOnLeft) + "..." +
                    path.Substring(path.Length - charsToShowOnRight);
            }
            return txt;
        }

        /// <summary>
        /// Converts a relative path to an absolute path, relative from a location.  E.g, ..\karl\test.xml from c:\work\data would result in c:\work\karl\test.xml
        /// </summary>
        /// <param name="relPath">Relative path to convert to an absolute path</param>
        /// <param name="relativeFrom">Relative location to resolve path from. If path is null, it is taken from application path</param>
        /// <returns>Absolute path</returns>
        public static string ConvertRelativePathToAbsolute(string relPath, string relativeFrom = null)
        {
            if (relativeFrom == null)
                relativeFrom = IocHelpers.Get<IApplicationService>().GetApplicationRootLocalFolder();

            relPath = relPath ?? "";
            string origSlashChar = "\\";
            if (relativeFrom.Contains("/")) origSlashChar = "/";

            if (origSlashChar == "/")
            {
                relPath = relPath.Replace("\\", "/");
                relativeFrom = relativeFrom.Replace("\\", "/");
            }
            else
            {
                relPath = relPath.Replace("/", "\\");
                relativeFrom = relativeFrom.Replace("/", "\\");
            }

            relativeFrom = GetDirName(relativeFrom ?? "");
            relativeFrom = relativeFrom.Replace("\\", "/");
            relativeFrom = relativeFrom.Substring(0, relativeFrom.Length - 1);
            relPath = relPath.Replace("\\", "/");

            string path = relativeFrom;
            while (relPath.StartsWith("../"))
            {
                int lastSlash = path.LastIndexOf('/');
                if (lastSlash > -1)
                {
                    path = path.Substring(0, lastSlash); //remove one directory
                    relPath = relPath.Substring(3);// remove the ../
                }
                else
                {
                    throw new InvalidOperationException("Invalid relative path <" + relPath + "> from path <" + relativeFrom + ">");
                }
            }
            if (relPath.StartsWith("/"))
                relPath = relPath.Substring(1);
            path += "/" + relPath; // add the remaining path
            path = path.Replace("/", origSlashChar);
            return path;


        }
        /// <summary>
        /// Converts an absolute path to a relative one, if possible, with respective to a relative location.  E.g, c:\karl\test.xml from c:\work would result in ..\karl\test.xml
        /// </summary>
        /// <param name="path">Path which is to be converted to the relative one.</param>
        /// <param name="relativeFrom">Path from which the resulting path will be relative</param>
        /// <returns>Relative Path</returns>
        public static string ConvertAbsolutePathToRelativePath(string path, string relativeFrom)
        {
            path = path ?? "";
            
            char originalSlashType = '\\';
            if (path.Contains("/")) originalSlashType = '/';

            


            
            relativeFrom = relativeFrom ?? "";

            if (originalSlashType == '/')
            {
                path = path.Replace("\\", "/");
                relativeFrom = relativeFrom.Replace("\\", "/");
            }
            else
            {
                path = path.Replace("/", "\\");
                relativeFrom = relativeFrom.Replace("/", "\\");
            }

            
            relativeFrom = GetDirName(relativeFrom);

            int sameLength = 0;

            for (int i = 1; i < relativeFrom.Length && i < path.Length; i++)
            {


                string pathPart = path.Substring(0, i);
                string relPart = relativeFrom.Substring(0, i);
                if (string.Compare(pathPart, relPart, true) != 0)
                {
                    break;
                }
                if (relativeFrom[i] == originalSlashType)
                    sameLength = i;



            }
            string result = null;
            if (sameLength > 0)
            {

                int totalSlashCount = 0;
                for (int i = sameLength + 1; i < relativeFrom.Length; i++)
                {
                    char c = relativeFrom[i];
                    if (c == originalSlashType)
                        totalSlashCount++;
                }

                for (int i = 0; i < totalSlashCount; i++)
                {
                    result += ".." + originalSlashType;
                }
                result += path.Substring(sameLength + 1);
            }
            else
            {
                result = path;
            }
            return result;


        }

        /// <summary>
        /// This method will add the current directory if the path is relative, e.g 'mysqldump.exe'.  THis will result in say 'c:\app\testing\mysqldump.exe'
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string AddCurrentDirectoryIfPathDoesNotIncludeADir(string path)
        {


            string s = path;
            if (!string.IsNullOrWhiteSpace(s))
            {
                if (!s.Contains(@":\"))
                {
//this means this is a relative path from the current working directory
                    s = System.Environment.CurrentDirectory + @"\" +s;


                }
                s = RemoveDuplicateSlashesFromPath(s);
                s = NormaliseToLocalPath(s);
            }
            return s;
        }

        public static string RemoveDuplicateSlashesFromPath(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                TextHelpers.ReplaceTexts(path, "/", "///", "//");
                TextHelpers.ReplaceTexts(path, "\\", "\\\\\\", "\\\\"); 
            }
            return path;
        }

        public static void CopyFile(string from, string newPath, bool overwriteIfExists = true)
        {
            
            if (File.Exists(from) && string.Compare(from,newPath,true) != 0)
            {
                CreateDirectory(newPath);
                File.Copy(from, newPath, overwriteIfExists);
            }
        }
        /// <summary>
        /// Checks the path, and normalizes it to a local path.  Any / are replaced to a \, and any duplicates also to just a \.  If it starts with a '/', it is resolved using map path first
        /// </summary>
        /// <param name="copyToFolder"></param>
        /// <returns></returns>
        public static string NormaliseToLocalPath(string folder)
        {
            if (!string.IsNullOrEmpty(folder))
            {
                folder = folder.Replace("//", "/");
                folder = folder.Replace("//", "/");
                if (folder.StartsWith("/"))
                    folder = MapWebPathToLocal(folder);
                folder = folder.Replace("/", "\\");
                folder = folder.Replace(@"\\\", @"\");
                folder = folder.Replace(@"\\", @"\");
            }
           
            return folder;
        }

        public static void SerializeObjectToFile(string localPath, object obj)
        {
            CreateDirectory(localPath);
            FileStream fs = File.Open(localPath, FileMode.Create, FileAccess.Write);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, obj);
            fs.Dispose();
            
        }
        public static T DeserializeObjectFromFile<T>(string localPath) where T : class
        {
            return (T)DeserializeObjectFromFile(localPath);
        }
        public static object DeserializeObjectFromFile(string localPath) 
        {
            object obj = null;
            if (File.Exists(localPath))
            {

                FileStream fs = File.Open(localPath, FileMode.Open, FileAccess.Read);
                BinaryFormatter bf = new BinaryFormatter();
                try
                {
                    obj = bf.Deserialize(fs);
                }
                catch (Exception )
                {
                    obj = null;
                }
                fs.Dispose();


            }
            return obj;
        }

        public static bool CheckIfFileMatchesExtensions(string filePath, IEnumerable<string> extensions)
        {
            
            bool ok = true;
            if (extensions != null && extensions.Any())
            {
                ok = false;
                string ext = GetExtension(filePath);
                foreach (var cmpExt in extensions)
                {
                    string s = cmpExt;
                    if (s.StartsWith("."))
                        s = TextHelpers.RemoveLastNumberOfCharactersFromString(s, 1);
                    if (string.Compare(ext, s, true) == 0)
                    {
                        ok = true;
                        break;
                    }
                }
            }
            return ok;

        }
        /// <summary>
        /// Converts one of the enumerated content type to the equivalent text
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string GetMimeContentType(string filename)
        {
            string ext;
            int dotPos = filename.LastIndexOf('.');
            if (dotPos > -1)
                ext = filename.Substring(dotPos + 1).ToLower();
            else
                ext = filename;
            switch (ext)
            {
                case "evy": return "application/envoy";
                case "fif": return "application/fractals";
                case "spl": return "application/futuresplash";
                case "hta": return "application/hta";
                case "acx": return "application/internet-property-stream";
                case "doc":
                case "dot": return "application/msword";
                case "flv": return "video/x-flv";
                case "bin":
                case "class":
                case "dms":
                case "exe":
                case "lha":
                case "lzh": return "application/octet-stream";
                case "oda": return "application/oda";
                case "axs": return "application/olescript";
                case "pdf": return "application/pdf";
                case "prf": return "application/pics-rules";
                case "p10": return "application/pkcs10";
                case "crl": return "application/pkix-crl";
                case "eps":
                case "ps":
                case "ai": return "application/postscript";
                case "rtf": return "application/rtf";
                case "setpay": return "application/set-payment-initiation";
                case "setreg": return "application/set-registration-initiation";
                case "xla":
                case "xlc":
                case "xlm":
                case "xls":
                case "xlt":
                case "xlw": return "application/vnd.ms-excel";
                case "msg": return "application/vnd.ms-outlook";
                case "sst": return "application/vnd.ms-pkicertstore";
                case "cat": return "application/vnd.pkiseccat";
                case "stl": return "application/vnd.ms-pkistl";
                case "pot":
                case "pps":
                case "ppt": return "application/vnd.ms-powerpoint";
                case "mpp": return "application/vnd.ms-project";
                case "wcm":
                case "wdb":
                case "wks":
                case "wps": return "application/vnd.ms-works";
                case "hlp": return "application/winhlp";
                case "bcpio": return "application/x-bcpio";
                case "cdf": return "application/x-cdf";
                case "z": return "application/x-compress";
                case "tgz": return "application/x-compressed";
                case "cpio": return "application/x-cpio";
                case "csh": return "application/x-csh";
                case "dcr":
                case "dir":
                case "dxr": return "application/x-director";
                case "dvi": return "application/x-dvi";
                case "gtar": return "application/x-gtar";
                case "gz": return "application/x-gzip";
                case "hdf": return "application/x-hdf";
                case "isp":
                case "ins": return "application/x-internet-signup";
                case "iii": return "application/x-iphone";
                case "js": return "application/x-javascript";
                case "latex": return "application/x-latex";
                case "mdb": return "application/x-msaccess";
                case "crd": return "application/x-mscardfile";
                case "clp": return "application/x-msclip";
                case "dll": return "application/x-msdownload";
                case "m13":
                case "m14":
                case "mvb": return "application/x-msmediaview";
                case "wmf": return "application/x-msmetafile";
                case "mny": return "application/x-msmoney";
                case "pub": return "application/x-ms-publisher";
                case "scd": return "application/x-msschedule";
                case "trm": return "application/x-msterminal";
                case "wri": return "application/x-mswrite";
                case "nc": return "application/x-netcdf";
                case "pma":
                case "pmc":
                case "pml":
                case "pmr":
                case "pmw": return "application/x-perfmon";
                case "p12":
                case "pfx": return "application/x-pkcs12";
                case "p7b":
                case "spc": return "application/x-pkcs7-certificates";
                case "p7r": return "application/x-pkcs7-certreqresp";
                case "p7c":
                case "p7m": return "application/x-pkcs7-mime";
                case "p7s": return "application/x-pkcs7-signature";
                case "sh": return "application/x-sh";
                case "shar": return "application/x-shar";
                case "swf": return "application/x-shockwave-flash";
                case "sv4cpio": return "application/x-sv4cpio";
                case "sv4crc": return "application/x-sv4crc";
                case "tar": return "application/x-tar";
                case "tcl": return "application/x-tcl";
                case "tex": return "application/x-tex";
                case "texi":
                case "texinfo": return "application/x-texinfo";
                case "roff":
                case "t":
                case "tr": return "application/x-troff";
                case "man": return "application/x-troff-man";
                case "me": return "application/x-troff-me";
                case "ms": return "application/x-troff-ms";
                case "ustar": return "application/x-ustar";
                case "src": return "application/x-wais-source";
                case "cer":
                case "crt":
                case "der": return "application/x-x509-ca-cert";
                case "zip": return "application/zip";
                case "pko": return "application/ynd.ms-pkipko";
                case "au":
                case "snd": return "audio/basic";
                case "m4v": return "video/x-m4v";
                case "mid":
                case "rmi": return "audio/mid";
                case "mp2":
                case "mp3": return "audio/mpeg";
                case "aif":
                case "aifc":
                case "aiff": return "audio/x-aiff";
                case "m3u": return "audio/x-mpegurl";
                case "ra":
                case "ram": return "audio/x-pn-realaudio";
                case "wav": return "audio/x-wav";
                case "bmp": return "image/bmp";
                case "cod": return "image/cis-cod";
                case "gif": return "image/gif";
                case "png": return "image/png";
                case "ief": return "image/ief";
                case "jpe":
                case "jpg":
                case "jpeg": return "image/jpeg";
                case "jfif": return "image/pipeg";
                case "svg": return "image/svg+xml";
                case "tif":
                case "tiff": return "image/tiff";
                case "ras": return "image/x-cmu-raster";
                case "cmx": return "image/x-cmx";
                case "ico": return "image/x-icon";
                case "pnm": return "image/x-portable-anymap";
                case "pbm": return "image/x-portable-bitmap";
                case "pgm": return "image/x-portable-graymap";
                case "ppm": return "image/x-portable-pixmap";
                case "rgb": return "image/x-rgb";
                case "xbm": return "image/x-xbitmap";
                case "xpm": return "image/x-xpixmap";
                case "xwd": return "image/x-xwindowdump";
                case "mht":
                case "mhtml":
                case "nws": return "message/rfc822";
                case "css": return "text/css";
                case "323": return "text/h323";
                case "htm":
                case "html":
                case "stm": return "text/html";
                case "uls": return "text/iuls";
                case "txt":
                case "bas":
                case "c":
                case "h": return "text/plain";

                case "rtx": return "text/richtext";
                case "sct": return "text/scriptlet";
                case "tsv": return "text/tab-separated-values";
                case "htt": return "text/webviewhtml";
                case "htc": return "text/x-component";
                case "etx": return "text/x-setext";
                case "vcf": return "text/x-vcard";

                case "mpa":
                case "mpe":
                case "mpeg":
                case "mpg":
                case "mpv2": return "video/mpeg";
                case "mov":
                case "qt": return "video/quicktime";
                case "lsf":
                case "lsx": return "video/x-la-asf";
                case "asf":
                case "asr":
                case "asx": return "video/x-ms-asf";
                case "avi": return "video/x-msvideo";
                case "movie": return "video/x-sgi-movie";
                case "flr":
                case "vrml":
                case "wrl":
                case "wrz":
                case "xaf":
                case "xof": return "x-world/x-vrml";
                case "xml": return "text/xml";
                case "mp4": return "video/mp4";
                case "csv": return "text/csv";

                default: return "application/octet-stream";

            }


        }
        /// <summary>
        /// Converts one of the enumerated content type to the equivalent text
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetExtensionFromMimeContentType(string contentType)
        {
            contentType = contentType.ToLower();
            switch (contentType)
            {
                case "application/envoy": return "evy";
                case "application/fractals": return "fif";
                case "application/futuresplash": return "spl";
                case "application/hta": return "hta";
                case "application/internet-property-stream": return "acx";
                case "application/msword": return "dot";
                case "video/x-flv": return "flv";

                case "application/octet-stream": return "exe";
                case "application/oda": return "oda";
                case "application/olescript": return "axs";
                case "application/pdf": return "pdf";
                case "application/pics-rules": return "prf";
                case "application/pkcs10": return "p10";
                case "application/pkix-crl": return "crl";

                case "application/postscript": return "ai";
                case "application/rtf": return "rtf";
                case "application/set-payment-initiation": return "setpay";
                case "application/set-registration-initiation": return "setreg";

                case "application/vnd.ms-excel": return "xls";
                case "application/vnd.ms-outlook": return "msg";
                case "application/vnd.ms-pkicertstore": return "sst";
                case "application/vnd.pkiseccat": return "cat";
                case "application/vnd.ms-pkistl": return "stl";


                case "application/vnd.ms-powerpoint": return "ppt";
                case "application/vnd.ms-project": return "mpp";


                case "application/vnd.ms-works": return "wps";
                case "application/winhlp": return "hlp";
                case "application/x-bcpio": return "bcpio";
                case "application/x-cdf": return "cdf";
                case "application/x-compress": return "z";
                case "application/x-compressed": return "tgz";
                case "application/x-cpio": return "cpio";
                case "application/x-csh": return "csh";


                case "application/x-director": return "dxr";
                case "application/x-dvi": return "dvi";
                case "application/x-gtar": return "gtar";
                case "application/x-gzip": return "gz";
                case "application/x-hdf": return "hdf";

                case "application/x-internet-signup": return "ins";
                case "application/x-iphone": return "iii";
                case "application/x-javascript": return "js";
                case "application/x-latex": return "latex";
                case "application/x-msaccess": return "mdb";
                case "application/x-mscardfile": return "crd";
                case "application/x-msclip": return "clp";
                case "application/x-msdownload": return "dll";


                case "application/x-msmediaview": return "mvb";
                case "application/x-msmetafile": return "wmf";
                case "application/x-msmoney": return "mny";
                case "application/x-ms-publisher": return "pub";
                case "application/x-msschedule": return "scd";
                case "application/x-msterminal": return "trm";
                case "application/x-mswrite": return "wri";
                case "application/x-netcdf": return "nc";


                case "application/x-perfmon": return "pmw";

                case "application/x-pkcs12": return "pfx";

                case "application/x-pkcs7-certificates": return "spc";
                case "application/x-pkcs7-certreqresp": return "p7r";

                case "application/x-pkcs7-mime": return "p7m";
                case "application/x-pkcs7-signature": return "p7s";
                case "application/x-sh": return "sh";
                case "application/x-shar": return "shar";
                case "application/x-shockwave-flash": return "swf";
                case "application/x-sv4cpio": return "sv4cpio";
                case "application/x-sv4crc": return "sv4crc";
                case "application/x-tar": return "tar";
                case "application/x-tcl": return "tcl";
                case "application/x-tex": return "tex";

                case "application/x-texinfo": return "texinfo";

                case "application/x-troff": return "tr";
                case "application/x-troff-man": return "man";
                case "application/x-troff-me": return "me";
                case "application/x-troff-ms": return "ms";
                case "application/x-ustar": return "ustar";
                case "application/x-wais-source": return "src";

                case "application/x-x509-ca-cert": return "cer";
                case "application/zip": return "zip";
                case "application/ynd.ms-pkipko": return "pko";

                case "audio/basic": return "snd";

                case "audio/mid": return "mid";

                case "audio/mpeg": return "mp3";

                case "audio/x-aiff": return "aif";
                case "audio/x-mpegurl": return "m3u";

                case "audio/x-pn-realaudio": return "ra";
                case "audio/x-wav": return "wav";
                case "image/bmp": return "bmp";
                case "image/cis-cod": return "cod";
                case "image/gif": return "gif";
                case "image/png": return "png";
                case "ief": return "image/ief";

                case "image/jpeg": return "jpg";
                case "image/pipeg": return "jfif";
                case "image/svg+xml": return "svg";

                case "image/tiff": return "tif";
                case "image/x-cmu-raster": return "ras";
                case "image/x-cmx": return "cmx";
                case "image/x-icon": return "ico";
                case "image/x-portable-anymap": return "pnm";
                case "image/x-portable-bitmap": return "pbm";
                case "image/x-portable-graymap": return "pgm";
                case "image/x-portable-pixmap": return "ppm";
                case "image/x-rgb": return "rgb";
                case "image/x-xbitmap": return "xbm";
                case "image/x-xpixmap": return "xpm";
                case "image/x-xwindowdump": return "xwd";

                case "message/rfc822": return "mhtml";
                case "text/css": return "css";
                case "text/h323": return "323";

                case "text/html": return "html";
                case "text/iuls": return "uls";
                case "text/plain": return "txt";

                case "text/richtext": return "rtx";
                case "text/scriptlet": return "sct";
                case "text/tab-separated-values": return "tsv";
                case "text/webviewhtml": return "htt";
                case "text/x-component": return "htc";
                case "text/x-setext": return "etx";
                case "text/x-vcard": return "vcf";

                case "video/mpeg": return "mpg";
                case "video/quicktime": return "mov";
                case "video/x-la-asf": return "lsf";

                case "video/x-ms-asf": return "asf";
                case "video/x-msvideo": return "avi";
                case "video/x-sgi-movie": return "movie";

                case "x-world/x-vrml": return "flr";
                case "text/xml": return "xml";


            }
            return "";

        }

        /// <summary>
        /// Waits for a file to become unlocked if locked.  It will wait a maximum until timeout, in 5 second intervals
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="maxWait">in seconds</param>
        /// <returns>true if ok, NOT locked.  False if it is still locked after timeout</returns>
        public static bool WaitForFileToUnlockIfLocked(string filePath, int maxWait = 60)

        {//checks evr
            bool isLocked = true;
            for (int i = 0; i < maxWait; i+=5)
            {
                isLocked = IsFileLocked(filePath);
                if (!isLocked)
                {
                    break;
                }
                else
                {
                    System.Threading.Thread.Sleep(5000);    
                }
            }
            return !isLocked;
        }

        /// <summary>
        /// Returns whether a file is locked (file in use)
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsFileLocked(string filePath)
        {
            FileStream stream = null;

            try
            {
                var file = new FileInfo(filePath);
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                
            }
            catch (IOException ex)
            {
                if (ex.Message.Contains("The process cannot access the file "))
                {
                    return true;
                }
                else
                {
                    throw;
                }
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }

    }
}


