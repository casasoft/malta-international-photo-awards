﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.My_Submissions
{
    public class MySubmissionsListingPageModel : USNBaseViewModel
    {
        public _ListingItemsAsMasonryPartialModel ListingItemsAsMasonryPartial { get; set; }
        public string ShowingResults { get; set; }
        public int ItemsPerPage { get; set; }
        public int ItemsTotalAmount { get; set; }

        public MySubmissionsListingPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
