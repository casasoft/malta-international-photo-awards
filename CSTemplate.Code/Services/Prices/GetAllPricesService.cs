﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.WebsiteConfiguration;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.Prices
{

    public interface IGetAllPricesService
    {
        PricesModel Get(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class GetAllPricesService : IGetAllPricesService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetAllPricesService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public PricesModel Get(UmbracoHelper umbracoHelper)
        {
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;

            var globalSettingsNode = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.USN_Global_Settings_Document_Type_Alias))
                .Select(x => (UsnglobalSettings)x).FirstOrDefault();
            var returnPricesModel = new PricesModel
            {
                StoryTellingPhotoPrice = globalSettingsNode.StoryTellingPhotoPrice,
                NormalPhotoPrice = globalSettingsNode.NormalPhotoPrice,
                FeedbackPrice = globalSettingsNode.FeedbackPrice,
                StorytellingFeedbackPrice = globalSettingsNode.StorytellingFeedbackPrize
            };

            return returnPricesModel;
        }
    }
}
