﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;
using Compare = System.ComponentModel.DataAnnotations.CompareAttribute;

namespace CSTemplate.Code.Models.Shared
{
    public class _SubmitPhotoFormModel 
    {
        public List<_SubmitPhotoItemModel> ListOfSubmitPhotos { get; set; }
        public string ExplanationFormText { get; set; }
        public string Required { get; set; }
        public string TermsAndConditionsText { get; set; }
        public string ButtonFormText { get; set; }
        public string UploadInformationText { get; set; }

        public List<SelectListItem> NumberOfPhotos { get; set; }

        [Required(ErrorMessage = "The 'Amount of Photos' is required")]

        [DisplayName("Amount of Photos to Submit")]
        public SelectListItem NumberOfPhotosSelected { get; set; }

        public string True
        {
            get
            {
                return "true";
            }
        }

        [Required]
        [Compare("True", ErrorMessage = "The 'Terms and Conditions' field is required")]
        public bool TermsConditions { get; set; }

        //[Range(typeof(bool), "true", "true", ErrorMessage = "The 'Terms and Conditions' field is required")]


       
    }
}
