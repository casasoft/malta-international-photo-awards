﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.SubmitPhotos;
using CSTemplate.Code.Services.Awards;
using CSTemplate.Code.Services.Orders;
using CSTemplate.Code.Services.Photos;
using CSTemplate.Code.Services.SubmitPhoto;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Enums = CS.Common.Constants.Enums;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class SubmitPhotoPageDataController : USNBaseController
    {
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetSubmitPhotoPageService _getSubmitPhotoPageService;
        private readonly IControlActiveAnyAwardService _controlActiveAnyAwardService;
        private readonly IUmbracoHelperService _umbracoHelperService;

        public SubmitPhotoPageDataController(
            IUmbracoControllerWrapperService umbracoControllerWrapperService,
            IGetSubmitPhotoPageService getSubmitPhotoPageService,
            IControlActiveAnyAwardService controlActiveAnyAwardService,
            IUmbracoHelperService umbracoHelperService
            )
        {
            _umbracoHelperService = umbracoHelperService;
            _controlActiveAnyAwardService = controlActiveAnyAwardService;

            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _getSubmitPhotoPageService = getSubmitPhotoPageService;
        }
        // GET: SubmitPhotoPageData
        [OutputCache(CacheProfile = ConstantValues.Cache_Profile_Name_By_Url_And_Specific_User)]
        public virtual ActionResult SubmitPhotoPage(RenderModel model)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);
            var currentMemberId = Members.GetCurrentMemberId();
           
            bool doesExist = _controlActiveAnyAwardService.Control(umbracoHelper);

            if (!doesExist)
            {
                var pageNotFound = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, CSTemplate.Code.Constants.ConstantValues.Competition_Not_Active_Data_Document_Type_Alias);
                return new RedirectToUmbracoPageResult(pageNotFound);
            }

           
            SubmitPhotoPageModel submitPhotoPageModel = _getSubmitPhotoPageService.Get(umbracoHelper, model.Content);
            return base.Populate(submitPhotoPageModel);
        }


    }
}