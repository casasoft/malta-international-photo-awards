﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;

namespace CS.Common.Services.Email
{

    public interface IEmailService
    {
        bool SendEmail(string subject, string htmlMessage, string toAddress, MailAddress fromMailAddress);
    }

    [Service]
    public class EmailService : IEmailService
    {
        public bool SendEmail(string subject, string htmlMessage, string toAddress, MailAddress fromMailAddress)
        {
            try
            {
                var port = Helpers.SmtpHelpers.SmtpPort;
                var host = Helpers.SmtpHelpers.SmtpHost;
                var defaultCredentials = Helpers.SmtpHelpers.DefaultCredentials;
                var username = Helpers.SmtpHelpers.SmtpUsername;
                var password = Helpers.SmtpHelpers.SmtpPassword;

                MailAddress toMailAddress = new MailAddress(toAddress);

                MailMessage mail = new MailMessage(fromMailAddress, toMailAddress);
                SmtpClient client = new SmtpClient();
                client.Port = port;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = defaultCredentials;
                client.Host = host;
                client.Credentials = new NetworkCredential(username, password);
                mail.Subject = subject;
                mail.Body = htmlMessage;
                mail.IsBodyHtml = true;
                client.Send(mail);

                return true;

            }
            catch (SmtpFailedRecipientException ex)
            {
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }


    }
}
