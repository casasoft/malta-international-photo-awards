﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Umbraco.Models.Authentication
{
    public class EmailMessageResetPasswordModel
    {
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string ResetPasswordUrl { get; set; }
    }
}
