﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.Awards
{

    public interface IControlActiveAnyAwardService
    {
        bool Control(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class ControlActiveAnyAwardService : IControlActiveAnyAwardService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public ControlActiveAnyAwardService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public bool Control(UmbracoHelper umbracoHelper)
        {
            var awardStatus = false;

            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;

            var awardEditionsNode = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionPageData)x).Where(x=>DateTime.Now.Date >= x.StartedDate && DateTime.Now <= x.EndedDate).ToList();

            if (awardEditionsNode!=null)
            {
                awardStatus = awardEditionsNode.Count > 0 ? true : false;
            }
            
            return awardStatus;
        }
    }
}
