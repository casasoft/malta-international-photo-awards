﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CSTemplate.Code.DataAnnotations;
using System.Web.Mvc;

namespace USN.USNModels
{
    public class USNContactFormViewModel
    {
        
        public string FirstName { get; set; }

        
        public string LastName { get; set; }

        [Required]
        public string FullName { get; set; }

        //public SelectListItem SubjectText { get; set; }
        
        //public List<SelectListItem> SubjectSelectListItems { get; set; }
        
        [Required]
        [DisplayName("Subject")]
        public List<System.Web.WebPages.Html.SelectListItem> SubjectSelectListItems { get; set; }

        [Required]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        public string Email { get; set; }
        [Required]
        public string Telephone { get; set; }

        [DataType(DataType.MultilineText)]
        [Required]
        public string Message { get; set; }

        public Boolean NewsletterSignup { get; set; }

        public int CurrentNodeID { get; set; }

        public int GlobalSettingsID { get; set; }
    }

}