﻿using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebPages.Html;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.ContactFormSubjects
{
    public interface IGetContactFormSubjectListService
    {
        List<SelectListItem> Get(UmbracoHelper umbracoHelper);
    }
    [Service]
    public class GetContactFormSubjectListService : IGetContactFormSubjectListService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetContactFormSubjectListService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public List<SelectListItem> Get(UmbracoHelper umbracoHelper)
        {
            var subjectList = new List<SelectListItem>();
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;
            var subjectsListNode = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Contact_Form_Subjects_Listing_Document_Type_Alias))
                .Select(x => (ContactFormSubjectsListing)x).FirstOrDefault();
            if (subjectsListNode!=null)
            {
                var subjects = subjectsListNode.Descendants()
                       .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Contact_Form_Subject_Document_Type_Alias))
                       .Select(x => (ContactFormSubject)x).ToList();
                if (subjects.Count > 0)
                {
                    foreach (var subject in subjects)
                    {
                        subjectList.Add(new SelectListItem
                        {
                            Text = subject.Name,
                            Value = subject.Id.ToString()
                        });
                    }
                }
            }
           
            return subjectList;
        }
    }
}
