﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class USNBaseController : Umbraco.Web.Mvc.RenderMvcController
    {

        protected ActionResult Populate(USNBaseViewModel model)
        {
            IPublishedContent homeNode = model.Content.Site();

            if (homeNode.IsDocumentType("USNHomepage"))
            {
                IPublishedContent globalSettings = homeNode.GetPropertyValue<IPublishedContent>("websiteConfigurationNode").Children.Where(x => x.IsDocumentType("USNGlobalSettings")).First();
                IPublishedContent navigation = homeNode.GetPropertyValue<IPublishedContent>("websiteConfigurationNode").Children.Where(x => x.IsDocumentType("USNNavigation")).First();

                model.GlobalSettings = globalSettings;
                model.Navigation = navigation;

                #region DummyData

                model.FooterPaymentMethod = new _FooterPaymentMethodModel()
                {
                    FooterPhotoTitleImgURL = "/dist/assets/img/logostandard.png",
                    FooterText =
                        "The Malta International Photography Award is a premier photo award that strives at promoting the art of photography from all across the world",
                    FooterPaymentMethodText = "We accept online payment securely via the following services:",
                    FooterPaymentMethodImgURL1 = "/dist/assets/img/paypal.png",
                    FooterPaymentMethodImgURL2 = "/dist/assets/img/visa.png",
                    FooterPaymentMethodImgURL3 = "/dist/assets/img/mastercard.png",

                };

                #endregion


                return base.Index(model);
            }
            else
                return base.Index(model);
        }

        public override ActionResult Index(RenderModel model)
        {
            IPublishedContent homeNode = model.Content.Site();

            if (homeNode.IsDocumentType("USNHomepage"))
            {
                USNBaseViewModel customModel = new USNBaseViewModel(model.Content);
                return Populate(customModel);
            }
            else
                return base.Index(model);
        }
    }
}