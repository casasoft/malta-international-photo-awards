﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.GalleryCategories
{

    public interface IGetGalleryCategoryFilterByGalleryCategoryId
    {
        CategoryGalleryPageData Get(UmbracoHelper umbracoHelper,int galleryCategoryId);
    }

    [Service]
    public class GetGalleryCategoryFilterByGalleryCategoryId : IGetGalleryCategoryFilterByGalleryCategoryId
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetGalleryCategoryFilterByGalleryCategoryId(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }
        public CategoryGalleryPageData Get(UmbracoHelper umbracoHelper,int galleryCategoryId)
        {
           
            var categoryPlaceHolderAlias = Constants.ConstantValues.Categories_Place_Holder_Document_Type_Alias;
            var categoriesAlias = Constants.ConstantValues.Category_Gallery_Page_Data_Document_Type_Alias;
            var galleryCategory = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper, categoryPlaceHolderAlias)
                .Descendants()
                .Where(x => x.DocumentTypeAlias.Equals(categoriesAlias) && x.Id == galleryCategoryId)
                .Select(x => (CategoryGalleryPageData)x).FirstOrDefault();
            return galleryCategory;
        }
    }
}
