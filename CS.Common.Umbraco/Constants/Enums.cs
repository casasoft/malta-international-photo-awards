﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Umbraco.Constants
{
    public static class Enums
    {
        public enum LoginResults
        {
            Successful,
            InvalidUsername,
            MemberLockedOut,
            AccountNotYetApproved,
            IncorrectPassword
        }

        public enum LanguagesNames
        {
            en_US,
            ru_RU
        }

        public enum LanguagePrefixes
        {
            en,
            ru
        }

        public enum NotificationMessageStatus
        {
            Error,
            Success,
            Warning
        }

        public enum DocumentTypeDisplayMode
        {
            Add,
            Edit,
            View
        }

        /// <summary>
        /// The keys here has to be unique throughout the project as else they will conflict in DB
        /// </summary>
        public enum DictionaryKey
        {
            //This model matches CS.Common.Umbraco.Services.Pager.CreatePagingModelForCurrentUrlService.PagerShowingResultsRazorTemplateModel
            [Description("Showing @Model.From - @Model.To of @Model.TotalResults @if(Model.TotalResults == 1) {@Model.ItemTypeSingular} else {@Model.ItemTypePlural}")]
            PagerShowingResultsRazorTemplate,

            [Description("result")]
            PagerShowingResultsDefaultItemTextSingular,

            [Description("results")]
            PagerShowingResultsDefaultItemTextPlural,


            [Description("Go to Last Page")]
            PagerTitleGoToLastPage,

            [Description("Go to Next Page")]
            PagerTitleGoToNextPage,

            [Description("Go to Previous Page")]
            PagerTitleGoToPreviousPage,

            [Description("Go to First Page")]
            PagerTitleGoToFirstPage,

            [Description("Current page")]
            ThisIsCurrentPage,

            [Description("Go to Page @Model")]
            PagerTitleGoToPage,

            [Description("<<")]
            PagerFirstPageText,

            [Description("<")]
            PagerPreviousPageText,

            [Description(">")]
            PagerNextPageText,

            [Description(">>")]
            PagerLastPageText

        }
    }
}
