﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace CSTemplate.Code.Constants
{
    public static class ConstantValues
    {
        public const string GeneralCurrencyFormat = "{0:n0}";
        public const string GeneralDateFormat = "d MMM yyyy";
        public const string GeneralDateTimeFormat = "ddd d MMM yyyy HH:mm";
        public const string EuroSymbol = "&#8364;";

        public const string CheckBoxListPropertyEditorAlias = "Umbraco.CheckBoxList";
        public const string RadioButtonListPropertyEditorAlias = "Umbraco.RadioButtonList";
        public const string DropDownMultiplePropertyEditorAlias = "Umbraco.DropDownMultiple";
        public const string DropDownPropertyEditorAlias = "Umbraco.DropDown";
        public const string DatePropertyEditorAlias = "Umbraco.Date";
        public const string TrueFalsePropertyEditorAlias = "Umbraco.TrueFalse";
        public const string IntegerPropertyEditorAlias = "Umbraco.Integer";
        public const string ListViewPropertyEditorAlias = "Umbraco.ListView";
        public const string VortoPropertyEditorAlias = "Our.Umbraco.Vorto";
        public const string ImageCropperPropertyEditorAlias = "Umbraco.ImageCropper";
        public const string TagsPropertyEditorAlias = "Umbraco.Tags";
        public const string DecimalPropertyEditorAlias = "Umbraco.Decimal";
        public const string TextboxPropertyEditorAlias = "Umbraco.Textbox";
        public const string HiddenInputTemplateName = "HiddenInput";

        /****************Cache Profile Names**************************/

        //Cache Profile Names
        public const string Cache_Profile_Name_One_Day = "OneDay";
        //public const string Cache_Profile_Name_One_Day_Vary_By_Custom_Url = "OneDayVaryByCustomURL";
        public const string Cache_Profile_Name_Cache_Vary_By_Custom_Url = "CacheVaryByCustomURL";
        public const string Cache_Profile_Name_Cache_Vary_By_Custom_For_Search = "CacheVaryByCustomForSearch";
        public const string Cache_Profile_Name_Cache_Vary_By_Culture = "CacheVaryByCulture";
        //public const string Cache_Profile_Name_One_Day_Vary_By_All_Params = "OneDayVaryByAllParams";
        public const string Cache_Profile_Name_Cache_Vary_By_All_Params = "CacheVaryByAllParams";
        public const string Cache_Profile_Name_For_Similar_Boat_Action_Method = "FiveMinutesVaryByCustomURL";
        //public const string Cache_Profile_Name_One_Day_Vary_By_Param_For_Seacrch = "OneDayVaryByCustomForSearch";
        public const string Cache_Profile_Name_Cache_Vary_By_Param_For_Search = "CacheVaryByCustomForSearch";
        public const string Cache_Profile_Name_For_Weather_Update_Method = "OneHour";
        public const string Cache_Profile_Name_By_Url_For_Search_Results = "CacheByUrlForSearchResults";

        public const string Cache_Profile_Name_By_Url_And_Specific_User = "CacheVaryByUrlAndSpecificUser";
        public const string Cache_Profile_Name_By_Url_And_Specific_User_And_Short_Time = "CacheVaryByUrlAndSpecificUserAndShortTime";





        //Paging Strings
        public const string Pager_Item_Title_Go_To_Last_Page = "Go to Last Page";
        public const string Pager_Item_Title_Go_To_Next_Page = "Go to Next Page";
        public const string Pager_Item_Title_Go_To_Previous_Page = "Go to Previous Page";
        public const string Pager_Item_Title_Go_To_First_Page = "Go to First Page";
        public const string Pager_Item_Title_Go_To_Specific_Page = "Go to Page ";

        public const string Pager_Item_Active_Class_Name = "active";


        //Send Grid Templates
        //public const string Default_SendGrid_Template_ID = "cea76272-a0cd-48c8-9615-b3160eeccbd4";
        public const string Default_SendGrid_Template_ID = "314c292a-b786-4a09-8a52-9d3b103bfd81";

        //Credits Cache Info and url and logo
        public const string credits_cache_key = "casasoft-credits-page";
        public const string credits_url = "http://credits.hector.casasoft-dev.com/credits/";
        public const string logo_cache_key = "casasoft-credits-link";
        public const string logo_url = "http://credits.hector.casasoft-dev.com/link/";



        //Document Types
        public const string Payment_Page_Document_Type_Alias = "paymentPageData";
        public const string Login_Page_Document_Type_Alias = "USNLoginPage";
        public const string Register_Page_Document_Type_Alias = "registerPageData";
        public const string Email_Template_Document_Type_Alias = "emailTemplates";
        public const string Reset_Password_Document_Type_Alias = "resetPasswordPageData";
        public const string Forgot_Password_Document_Type_Alias = "forgotPasswordPageData";
        public const string USN_Website_Configuration_Section_Document_Type_Alias = "USNWebsiteConfigurationSection";
        public const string USN_Global_Settings_Document_Type_Alias = "USNGlobalSettings";
        public const string Juries_Listing_Page_Data_Document_Type_Alias = "juriesListingPageData";
        public const string Jury_Member_Data_Document_Type_Alias = "juryMemberData";
        public const string Prizes_Listing_Page_Data_Document_Type_Alias = "prizesListingPageData";
        public const string Prize_Item_Data_Document_Type_Alias = "prizeItemData";
        public const string Award_Edition_Page_Data_Document_Type_Alias = "awardEditionPageData";
        public const string Contact_Form_Subjects_Listing_Document_Type_Alias = "contactFormSubjectsListing";
        public const string Contact_Form_Subject_Document_Type_Alias = "contactFormSubject";
        public const string Award_Edition_Category_Page_Data_Document_Type_Alias = "awardEditionCategoryPageData";
        public const string Page_Not_Found_Data_Document_Type_Alias = "pageNotFoundData";
        public const string Competition_Not_Active_Data_Document_Type_Alias = "competitionNotActiveData";
        public const string Category_Gallery_Page_Data_Document_Type_Alias = "categoryGalleryPageData";
        public const string Categories_Place_Holder_Document_Type_Alias = "categoriesPlaceHolder";
        public const string USN_Advanced_Page_Document_Type_Alias = "USNAdvancedPage";
        public const string All_Listing_Prizes_Page_Data_Document_Type_Alias = "allListingPrizesPageData";
        public const string USN_Advanced_Page_Components_Page_Data_Document_Type_Alias = "USNAdvancedPageComponents";
        public const string Submit_Photo_Selection_Page_Data_Document_Type_Alias = "submitPhotoSelectionPageData";
        public const string Members_Area_Page_Data_Document_Type_Alias = "membersAreaPageData";
        public const string Order_History_Page_Data_Document_Type_Alias = "orderHistoryPageData";
        public const string View_Order_Page_Data_Document_Type_Alias = "viewOrderPageData";


        //Document Type Properties
        public const string Award_Edition_Page_Data_Final_Jury_Feedback_Notification_Email_Sent_Property = "finalJuryFeedbackNotificationEmailSent";

        //Weather Service
        public const double Weather_Latitude = 36.046166d;
        public const double Weather_Longitude = 14.252347d;
        public const string Weather_Api_Key = "4e5e836010b247e50a72616ae45aafd3";
        public const string Weather_Units = "metric";
        public const string Weather_Language = "en-GB";

        //General sizes of images
        public const int GeneralImageSizeSmall = 500;
        public const int GeneralImageSizeMedium = 900;
        public const int GeneralImageSizeLarge = 1900;

        //HomePage
        public const string Count_Down_Background_Photo_Url = "/dist/assets/img/countdownbackground.jpg";

        //Process Result
        public const bool Process_Successful = true;
        public const bool Process_Failed = false;

        //Constant Carousel Amount Value
        public const int DefaultAmountValue = 4;
       

        //Email Templates
        public const int EmailNotificationWithJuryFeedbackAfterAwardMarkedAsCompletedTemplateId = 1699;
        public const int EmailNotificationToClientOnSubmissionAndPaymentCompleted = 2248;
        public const int EmailNotificationToClientOnSubmissionSubmittedButPaymentNotCompleted = 4551;
        public const int EmailNotificationToAdministratorOnSubmissionCompleted = 2255;
        public const int EmailNotificationToAdministratorOnSubmissionNotYetCompleted = 4552;

        //payment
        public const int Default_Payment_Page_Id = 1492;
        public const string TempDataPaymentOrderRefId = "paymentOrderRefId";
        public const string TempDataPaymentPaymentAmount = "paymentAmount";
        public const string TempDataPaymentTransactionDescription = "paymentTransactionDescription";
        public const string TempDataPaymentClientEmailAddress = "paymentClientEmailAddres";

        //Email Templates
        public const int Email_Template_Comomon_Template_Header_And_Footer = 1713;

        //Apco Payment Response XML attribute names
        public const string ApcoProfileId = "8F9FE851D9B74C2D911AA264263A015D";
        public const string ApcoHashingSW = "c6f88fd33a";
        public const string PaymentResponseXMLAttributeResultName = "Result";
        public const string PaymentResponseXMLAttributeOrderReferenceName = "ORef";
        public const string PaymentResponseXMLAttributePSPIDName = "pspid";

        //DocumentTypeAliases
        public const string MySubmissionsListingPageDocTypeAlias = "mySubmissionsListingPageData";


        //My Submission
        public const int Submission_Page_Id = 1623;
        public const string SubmissionUrl = "/photo";

        //Orders
        public const int Order_Page_Id = 1574;
        public const string OrderUrl = "/members-area/order-history";
        public const string SingleOrderUrlToRequestForFeedbackButton = "../members-area/order-history";


        //Payment Logo Urls
        public const string PaypalLogoImgUrl = "dist/assets/img/secure_paypal.png";
        public const string VisaLogoImgUrl = "dist/assets/img/visa.png";
        public const string MasterCardLogoImgUrl = "dist/assets/img/mastercard.png";
        public const string BilderlingsLogImgUrl = "dist/assets/img/bilderlings.png";


        //Registration
        public const string MemberApproved = "umbracoMemberApproved";
        public const string MemberAccountFirstName = "clientName";
        public const string MemberAccountSurname = "clientSurname";
        public const string MemberAccountCountry = "country";
        public const string MemberAccountPhone = "phone";
        public const string MemberAccountNationality = "nationality";
        public const string MemberAccountAgreeToReceivePromotionalMaterials = "agreeToReceivePromotionalMaterials";
        public const string MemberAccountEmail = "Email";
        public const string WhereDidYouHearAboutUs = "whereDidYouHearAboutUs";

        //Member Groups
        public const string Member_Group_Client_Name = "Client";
        public const string Member_Group_Jury_Name = "Jury";

        //Reset Password
        public const string Memeber_Reset_Password_Guid_Alias = "resetPasswordGUID";
        public const string ResetPasswordGuidFormatProvider = "ddMMyyyyHHmmssFFFF";

        //Update Profile
        public const string Member_Profile_Update_Successful_Query_String = "q";

        //Submit feedback
        public const string Jury_Profile_Submit_Feedback_Query_String = "feedback";

     

        //SubmittedPhotoSourceAddress
        public const string Submitted_Photo_Source_Address = "/images/SubmissionsPhotos/";

        //Order History
        public const int Order_History_Page_Id = 1560;

        //ViewOrder
        public const int Default_ViewOrder_Page_Id = 1574;

        public const string Order_Summary_Page_Link = "/order-summary";
        public const string Submit_Photo_Page_Link = "/submit-photos";
        public const string Submit_StoryTelling_Page_Link = "/submit-storytelling/";
        public const string Competition_Not_Active_Page_Link = "/competition-not-active/";
        public const string Password_Updated_Page_Link = "/members-area/change-password/password-updated";

        //FeedbackRequestsForUser
        public const string Feeback_Request_Submissions_List_Link = "/members-area/photo-evaluation-requests/";


        //ShortListedPhotos Link
        public const string Short_Listed_Photos_List_Link = "/members-area/shortlist-photos";

        //Mansory PageItemCount
        public const int Mansory_Item_Count = 20;
        public const int Mansory_Item_ShortListed_Count = 50;
        public const int Photographer_Other_Photos_Submission_Full_Page_Mansory_Item_Count = 12;

        //OrderItemCount
        public const int Order_Item_List_Count = 20;
        

        public const int Default_Successful_Page_Id = 1493;
        public const int Default_Failed_Page_Id = 1495;


        //Paypal
        public const string PayPal_Transaction_Response_Pending = "Pending";
        public const string PayPal_Transaction_Response_Completed = "Completed";
        public const string PayPal_Transaction_Response_Denied = "Denied";

        //Bilderlings
        public const string Bilderlings_Transaction_Payment_Response_Succeeded = "succeeded"; 
        public const string Bilderlings_Transaction_Payment_Response_Failed = "failed ";

        //Link Targets
        public const string Link_Target_Blank = "_blank";
        public const string Link_Target_Top = "_top";
        public const string Link_Target_Self = "_self";


        //reCaptcha not filled
        public const string ReCaptcha = "recaptcha";

        //clearing output cache - cachekeys
        public const string Cache_Key_Pages = "Pages";
        public const string Cache_Key_Submission_Full_Page = "SubmissionFullPageData";


        //bilderlings Constants
        public const string BilderlingsShopName = "maltaphotoaward";
        public const string BilderlingsShopPassword = "Ybf75454!gf!F";
        public const string BilderlingsCurrency = "EUR";
        public const string BilderlingsPaymentMethod = "FD_SMS_3D_OPTIONAL";
    }
}
