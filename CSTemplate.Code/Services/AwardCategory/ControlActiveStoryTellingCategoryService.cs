﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.AwardCategory
{

    public interface IControlActiveStoryTellingCategoryService
    {
        bool Control(UmbracoHelper umbracoHelper);
    }

    [Service]
    public class ControlActiveStoryTellingCategoryService : IControlActiveStoryTellingCategoryService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public ControlActiveStoryTellingCategoryService(IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
            _umbracoHelperService = umbracoHelperService;
        }
        public bool Control(UmbracoHelper umbracoHelper)
        {
            var websiteConfigurationNode = new Usnhomepage(_umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)).WebsiteConfigurationNode;

            var activeAwardOfStoryTellingCategories = false;
            var awardEditionsNode = websiteConfigurationNode.Children()
                .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Page_Data_Document_Type_Alias))
                .Select(x => (AwardEditionPageData)x).Where(x => DateTime.Now.Date >= x.StartedDate && DateTime.Now <= x.EndedDate).FirstOrDefault();

            if (awardEditionsNode!=null)
            {
                activeAwardOfStoryTellingCategories = awardEditionsNode.Descendants()
                    .Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Award_Edition_Category_Page_Data_Document_Type_Alias))
                    .Select(x => (AwardEditionCategoryPageData)x).Any(x => x.IsStoryTelling == true);
            }
            
           
            return activeAwardOfStoryTellingCategories;
        }
    }
}
