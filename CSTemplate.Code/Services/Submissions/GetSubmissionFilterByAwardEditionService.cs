﻿using CS.Common.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.Awards;
using CSTemplate.Code.Services.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Submissions
{
    public interface IGetSubmissionFilterByAwardEditionService
    {
        List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper,int awardEditionId);
    }
    [Service]
    public class GetSubmissionFilterByAwardEditionService : IGetSubmissionFilterByAwardEditionService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetActiveAwardEditionService _getActiveAwardEditionService;

        public GetSubmissionFilterByAwardEditionService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IGetActiveAwardEditionService getActiveAwardEditionService

            )
        {
            this._getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
            this._getActiveAwardEditionService = getActiveAwardEditionService;
        }
        //TODO:It'll handle after Mark's Explanation.
        public List<_ItemPhotoModel> Get(UmbracoHelper umbracoHelper, int awardEditionId)
        {
            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();
            var listOfItemPhotoModel = new List<_ItemPhotoModel>();
            if (awardEditionId<=0)
            {
                var activeAwardEdition= _getActiveAwardEditionService.Get(umbracoHelper);
                awardEditionId = activeAwardEdition.Id;
            }

            var submissionsTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = Constants.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = Constants.DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = Constants.DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = Constants.DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = Constants.DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = Constants.DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionTitleField = Constants.DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionShortListedEnumIdField = Constants.DatabaseConstantValues.Submissions_Short_Listed_Enum_Id_Field_Name;
            var submissionShortListedDateField = Constants.DatabaseConstantValues.Submissions_Short_Listed_Date_Field_Name;
            var submissionStatusEnumIdField = Constants.DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;
            
            var sqlQuery = $@"
                    SELECT
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId},
                        {submissionsTableAlias}.{submissionTitleField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionShortListedEnumIdField},
                        {submissionsTableAlias}.{submissionStatusEnumIdField}
                            FROM
                                {submissionsTableName}
                                     WHERE 
                                    (
                                        {submissionsTableAlias}.{submissionShortListedEnumIdField} = {(int)Enums.ShortListedStatus.NotYet}
                                    )
                                        AND 
                                        ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK}) 
                                        AND {submissionsTableAlias}.{submissionShortListedDateField} IS NULL
                                        
            ";



            return listOfItemPhotoModel;
        }
    }
}
