﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebPages.Html;
using static CSTemplate.Code.Constants.Enums;

namespace CSTemplate.Code.Models.Shared
{
    public class _ItemPhotoModel
    {
        public string ItemPhotoImgURL { get; set; }
        public string ItemPhotoTitle { get; set; }
        public string ItemPhotoName { get; set; }
        public DateTime ItemPhotoDate { get; set; }
        public string ItemPhotoAward { get; set; }
        public string ItemPhotoLocation { get; set; }
        public string ItemPhotoDescription { get; set; }
        public string ItemPhotoCategory { get; set; }
        public string PhotoFullPageUrl { get; set; }
        public string SubmissionId { get; set; }
        public string SubmissionStatusTitleText { get; set; }
        public string CurrentSubmissionStatus { get; set; }
        public DateTime ShortListedDate { get; set; }
        //todo: [For: Backend | 2018/08/24] This shows radio buttons on Shortlist Photos Page (WrittenBy: Radek)        			
        public bool ShowShortlistRadioButtons { get; set; }

        public ShortListedStatus ChosenRadioValue { get; set; }
        public List<ShortListedStatus> ListOfRadiosValues { get; set; }


    }

}
