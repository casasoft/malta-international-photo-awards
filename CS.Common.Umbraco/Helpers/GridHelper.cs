﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skybrud.Umbraco.GridData;

namespace CS.Common.Umbraco.Helpers
{
    public static class GridHelper
    {
        public static bool HasData(this GridDataModel grid)
        {
            bool hasData = false;
            if (grid == null)
            {
                hasData = false;
            }
            else
            {
                hasData = (from section in grid.Sections
                           where section.HasRows
                           from row in section.Rows
                           where row.HasAreas
                           from area in row.Areas
                           let hasGridData = area.HasControls
                           select hasGridData)
                            .Any(hasGridData => hasGridData == true);
            }
            return hasData;
        }
    }
}
