﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CS.Common.Umbraco.Services.Umbraco.Wrappers.Services
{
    public interface IMemberServiceWrapper
    {
        IMember GetByUsername(UmbracoContext umbracoContext, string username);
        IMember GetByEmail(UmbracoContext umbracoContext, string email);
        void Save(UmbracoContext umbracoContext, IMember entity, bool raiseEvents = true);
        IMember GetById(UmbracoContext umbracoContext, int memberId);
        void SavePassword(UmbracoContext umbracoContext, IMember member, string password);
        void AssignRole(UmbracoContext umbracoContext, int memberId, string roleName);
        IEnumerable<string> GetAllRoles(UmbracoContext umbracoContext, int memberId);
    }

    [Service]
    public class MemberServiceWrapper : IMemberServiceWrapper
    {
        public IMember GetByUsername(UmbracoContext umbracoContext, string username)
        {
            return umbracoContext.Application.Services.MemberService.GetByUsername(username);
        }

        public IMember GetByEmail(UmbracoContext umbracoContext, string email)
        {
            return umbracoContext.Application.Services.MemberService.GetByEmail(email);
        }

        public void Save(UmbracoContext umbracoContext, IMember entity, bool raiseEvents = true)
        {
            umbracoContext.Application.Services.MemberService.Save(entity, raiseEvents);
        }

        public IMember GetById(UmbracoContext umbracoContext, int memberId)
        {
            return umbracoContext.Application.Services.MemberService.GetById(memberId);
        }

        public void SavePassword(UmbracoContext umbracoContext, IMember member, string password)
        {
            umbracoContext.Application.Services.MemberService.SavePassword(member, password);
        }

        public void AssignRole(UmbracoContext umbracoContext, int memberId, string roleName)
        {
           umbracoContext.Application.Services.MemberService.AssignRole(memberId, roleName);
        }

        public IEnumerable<string> GetAllRoles(UmbracoContext umbracoContext, int memberId)
        {
            return umbracoContext.Application.Services.MemberService.GetAllRoles(memberId);
        } 
    }
}
