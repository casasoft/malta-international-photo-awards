﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.SubmitPhotoSelection;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace CSTemplate.Code.Services.SubmitPhotoSelectionPage
{
    public interface IGetSubmitPhotoSelectionService
    {
        SubmitPhotoSelectionPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content);
    }

    [Service]
    public class GetSubmitPhotoSelectionPageService : IGetSubmitPhotoSelectionService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetSubmitPhotoSelectionPageService(
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }

        public SubmitPhotoSelectionPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content)
        {
            var selectionPhotos = _umbracoHelperService.GetNodeFromDocumentTypeAndQueryString(umbracoHelper,ConstantValues.Members_Area_Page_Data_Document_Type_Alias)
                .Descendants()
                .Where(x=>x.DocumentTypeAlias.Equals(ConstantValues.Submit_Photo_Selection_Page_Data_Document_Type_Alias))
                .Select(x=>(SubmitPhotoSelectionPageData)x).FirstOrDefault();

            var explanationText = selectionPhotos.TopDescription;

            var submitPhotoCoverPhoto= selectionPhotos.SubmitPhotoCoverPhoto;
            var submitPhotoTitle = selectionPhotos.SubmitPhotoTitle;
            var submitPhotoText = selectionPhotos.SubmitPhotoDescription;
            var submitPhotoCoverPhotoUrl = selectionPhotos.SubmitPhotoCoverPhoto.Url;

            var submitStoryTellingTitle = selectionPhotos.StorytellingTitle;
            var submitStoryTellingText = selectionPhotos.StorytellingDescription;
            var storytellingPhotoCoverPhotoUrl = selectionPhotos.SubmitStorytellingCoverPhoto.Url;

            var submitPhotoUrl = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_Photo_Selection_Url);
            var submitStoryTellingUrl = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Submit_StoryTelling_Selection_Url);

            SubmitPhotoSelectionPageModel submitPhotoSelectionPageModel = new SubmitPhotoSelectionPageModel(content)
            {
                ExplanationText =  explanationText,
                SubmitPhotoTitle = submitPhotoTitle,
                SubmitPhotoText = submitPhotoText,
                SubmitStoryTellingTitle = submitStoryTellingTitle,
                SubmitStoryTellingText = submitStoryTellingText,
                SubmitPhotoImgUrl = submitPhotoCoverPhotoUrl,
                SubmitStoryTellingImgUrl = storytellingPhotoCoverPhotoUrl,
                SubmitPhotoUrl = submitPhotoUrl,
                SubmitStoryTellingUrl = submitStoryTellingUrl,
            };
            return submitPhotoSelectionPageModel;
        }
    }
}
