﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CS.Common.Umbraco.Constants;
using CS.Common.Umbraco.Helpers.UmbracoPackages.Vorto;
using Umbraco.Core;
using Umbraco.Core.Cache;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;
using Our.Umbraco.Vorto.Models;

namespace CS.Common.Umbraco.UrlProviders
{
    /// <summary>
    /// This is the multilingual URL provider for the pages which follow the 
    /// Umbraco URL structure from the Content in Umbraco.  It will read the parents
    /// of the node and return the path.
    /// </summary>
    public class UmbracoMultilingualUrlProvider : BaseMultilingualUrlProvider
    {
        private static string getTitleFromContentForCulture(IPublishedContent content, string cultureName)
        {
            string title = null;
            var contentUrlTitleVortoValue = content.GetPropertyValue<VortoValue>(ConstantValues.Common_Content_Url_Title);

            //First try get the url value
            title = MultilingualHelpers.GetValueForCurrentCultureOrPassedCulture(contentUrlTitleVortoValue, cultureName, false);

            if (string.IsNullOrWhiteSpace(title))
            {
                var contentTitleVortoValue = content.GetPropertyValue<VortoValue>(ConstantValues.Common_Content_Title);
                //Then try get the title value
                title = MultilingualHelpers.GetValueForCurrentCultureOrPassedCulture(contentTitleVortoValue, cultureName, false);
            }

            if (string.IsNullOrWhiteSpace(title))
            {
                title = content.Name;

            }
            if (!string.IsNullOrWhiteSpace(title))
            {
                title = title.ToUrlSegment().EnsureEndsWith("/");

            }
            return title;
        }

        protected override string computePathForCurrentNodeForCulture(UmbracoContext umbracoContext,
            string cultureName,
            IPublishedContent content,
            Uri current)
        {
            string path = "";
            IPublishedContent currNode = content;
            while (currNode.Parent != null)
            {
                //Recursively go through all its parents
                var currNodePathSegment = getTitleFromContentForCulture(currNode, cultureName);
                path = string.Format("{0}{1}", currNodePathSegment, path);
                currNode = currNode.Parent;
            }
            return path;
        }

        protected override bool IsContentValidForCurrentUrlProvider(IPublishedContent content)
        {
            return true;
        }
    }
}