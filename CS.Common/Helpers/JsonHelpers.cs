﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using CS.Common.Constants;

namespace CS.Common.Helpers
{
    public static class JsonHelpers
    {
        //JavaScriptDateTimeConverter - Date Example: 'new Date(1234656000000)'
        public static T Deserialise<T>(
            string json,
            bool throwErrorIfCannotDeserialize = true,
            params JsonConverter[] customConverters)
        {
            T result = default(T);
            if (!string.IsNullOrWhiteSpace(json))
            {
                try
                {
                    var customConvertersList = new List<JsonConverter>();
                    customConvertersList.Add(new JavaScriptDateTimeConverter()); //default
                    if (customConverters.Any())
                    {
                        customConvertersList.AddRange(customConverters);
                    }
                    

                    result = JsonConvert.DeserializeObject<T>(json, customConvertersList.ToArray());
                }
                catch (JsonException ex)
                {
                    if (throwErrorIfCannotDeserialize)
                    {
                        throw;
                    }
                    else
                    {
                        //return null
                    }
                }


            }
            return result;

        }

        //IsoDateTimeConverter - Date Example: '2009-02-15T00:00:00Z'
        public static T DeserialiseWithIsoDateTimeConverter<T>(string json)
        {
            if (!string.IsNullOrWhiteSpace(json))
            {
                return JsonConvert.DeserializeObject<T>(json, new IsoDateTimeConverter());
            }
            else
            {
                return default(T);
            }
        }

        public static string Serialize<T>(
            T obj,
            NullValueHandling nullValueHandling = NullValueHandling.Ignore,
            Formatting? formatting = null,
            Enums.JsonDateTimeConverter dateTimeConverter = Enums.JsonDateTimeConverter.JavaScript,
            params JsonConverter[] customConverters)
        {

            var currCulture = System.Threading.Thread.CurrentThread.CurrentCulture;

            //2013-05-7 This has been done since when ingerman, numbers are written as 65,000 with a comma instead
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");

            if (!formatting.HasValue)
            {
                formatting = Formatting.None;
            }


            JsonSerializer serializer = new JsonSerializer();

            serializer.Formatting = formatting.GetValueOrDefault(Formatting.None);
            serializer.NullValueHandling = nullValueHandling;

            if (dateTimeConverter == Enums.JsonDateTimeConverter.JavaScript)
            {
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
            }
            else if (dateTimeConverter == Enums.JsonDateTimeConverter.Iso)
            {
                serializer.Converters.Add(new IsoDateTimeConverter());
            }

            if (customConverters.Any())
            {
                foreach (var customConverter in customConverters)
                {
                    if (customConverter != null)
                    {
                        serializer.Converters.Add(customConverter);
                    }
                }
            }
            StringWriter sw = new StringWriter();

            JsonWriter jw = new JsonTextWriter(sw);
            serializer.Serialize(jw, obj);




            string json = sw.ToString();
            //json = json.Replace("/script", "\\/script");



            System.Threading.Thread.CurrentThread.CurrentCulture = currCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = currCulture;
            return json;
        }
    }
}
