﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Common.Models.SendGrid
{
    public class SendGridAttachmentModel
    {
        public Stream FileStream { get; set; }
        public string FileName { get; set; }
    }
}
