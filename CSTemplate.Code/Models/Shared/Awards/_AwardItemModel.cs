﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSTemplate.Code.Models.Shared.Awards
{
    public class _AwardItemModel
    {
        public int AwardItemId { get; set; }
        public string AwardItemName { get; set; }
        public DateTime AwardItemStartedDate { get; set; }
        public DateTime AwardItemEndedDate { get; set; }
        public bool IsAnActiveAward { get; set; }
        public bool FinalJuryFeedbackNotificationEmailSent { get; set; }
    }
}
