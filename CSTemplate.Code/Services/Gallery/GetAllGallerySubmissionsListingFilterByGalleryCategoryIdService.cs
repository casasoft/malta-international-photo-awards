﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.CategoryGallery;
using CSTemplate.Code.Models.Pocos;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Models.Shared.ListingItems;
using CSTemplate.Code.Services.AwardCategory;
using CSTemplate.Code.Services.Common;
using CSTemplate.Code.Services.Database;
using CSTemplate.Code.Services.Photos;
using CSTemplate.Code.Services.Users;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Web;

namespace CSTemplate.Code.Services.Gallery
{

    public interface IGetAllGallerySubmissionsListingFilterByGalleryCategoryIdService
    {
        CategoryGalleryPageModel Get(UmbracoHelper umbracoHelper,IPublishedContent publishedContent,int galleryCategoryId, int pageNumber=1);
    }

    [Service]
    public class GetAllGallerySubmissionsListingFilterByGalleryCategoryIdService : IGetAllGallerySubmissionsListingFilterByGalleryCategoryIdService
    {
        private readonly IGetCurrentUmbracoDatabaseService _getCurrentUmbracoDatabaseService;
        private readonly IGetAwardCategoryDetailsFilterByCategoryIdService _getAwardCategoryDetailsFilterByCategoryIdService;
        private readonly IGetUserFilterByUserIdService _getUserFilterByUserIdService;
        private readonly IGetFirstPhotoUrlFilterBySubmissionIdService _getFirstPhotoUrlFilterBySubmissionIdService;
        private readonly IGetPagerModelService _getPagerModelService;

        public GetAllGallerySubmissionsListingFilterByGalleryCategoryIdService(
            IGetCurrentUmbracoDatabaseService getCurrentUmbracoDatabaseService,
            IGetAwardCategoryDetailsFilterByCategoryIdService getAwardCategoryDetailsFilterByCategoryIdService,
            IGetUserFilterByUserIdService getUserFilterByUserIdService,
            IGetFirstPhotoUrlFilterBySubmissionIdService getFirstPhotoUrlFilterBySubmissionIdService,
            IGetPagerModelService getPagerModelService
            )
        {
            _getPagerModelService = getPagerModelService;
            _getFirstPhotoUrlFilterBySubmissionIdService = getFirstPhotoUrlFilterBySubmissionIdService;
            _getUserFilterByUserIdService = getUserFilterByUserIdService;
            _getAwardCategoryDetailsFilterByCategoryIdService = getAwardCategoryDetailsFilterByCategoryIdService;
            _getCurrentUmbracoDatabaseService = getCurrentUmbracoDatabaseService;
        }
        public CategoryGalleryPageModel Get(UmbracoHelper umbracoHelper, IPublishedContent publishedContent, int galleryCategoryId, int pageNumber=1)
        {
            var categoryGalleryPageModel = new CategoryGalleryPageModel(publishedContent);
            if (galleryCategoryId <= 0) return categoryGalleryPageModel;
            var listOfItemPhotoModel = new List<_ItemPhotoModel>();

            var umbracoDb = _getCurrentUmbracoDatabaseService.Get();

            var submissionsTableName = Constants.DatabaseConstantValues.SubmissionsTableName;
            var submissionsTableAlias = Constants.DatabaseConstantValues.aliasForSubmissionsPocoTableNameAlias;
            var submissionPrimaryKeyField = Constants.DatabaseConstantValues.Submissions_Poco_Primary_Key_Value;
            var submissionsDate = Constants.DatabaseConstantValues.Submissions_Date_Field_Name;
            var submissionsDescription = Constants.DatabaseConstantValues.Submissions_Description_Field_Name;
            var submissionsLocation = Constants.DatabaseConstantValues.Submissions_Location_Field_Name;
            var submissionsUserId = DatabaseConstantValues.Submissions_User_Id_Field_Name;
            var submissionsCategoryId = DatabaseConstantValues.Submissions_Category_Id_Field_Name;
            var submissionTitleField = DatabaseConstantValues.Submissions_Title_Field_Name;
            var submissionStatusEnumIdField = DatabaseConstantValues.Submissions_Status_Enum_Id_Field_Name;
            var submissionGalleryCategoryId = DatabaseConstantValues.Submissions_Gallery_Category_Id_Name;

            var featuredFieldId = Constants.DatabaseConstantValues.Photos_Featured_Field_Name;

            var orderByDescValue = Constants.DatabaseConstantValues.ORDER_BY_DESC;

            var sqlQuery = new Sql($@" 
                     SELECT
                        {submissionsTableAlias}.{submissionPrimaryKeyField},
                        {submissionsTableAlias}.{submissionsDate},
                        {submissionsTableAlias}.{submissionsDescription},
                        {submissionsTableAlias}.{submissionsLocation},
                        {submissionsTableAlias}.{submissionsCategoryId},
                        {submissionsTableAlias}.{submissionTitleField},
                        {submissionsTableAlias}.{submissionsUserId},
                        {submissionsTableAlias}.{submissionStatusEnumIdField}
                            FROM
                                {submissionsTableName}
                                     WHERE {submissionsTableAlias}.{submissionGalleryCategoryId} = {galleryCategoryId} AND ({submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.WaitingForPayment} AND {submissionsTableAlias}.{submissionStatusEnumIdField} != {(int)CSTemplate.Code.Constants.Enums.SubmissionStatus.NOTOK})
                                         ORDER BY {submissionPrimaryKeyField} {orderByDescValue}
            ");

            var gallerySubmissionsSqlResult = umbracoDb.Query<SubmissionPoco>(sqlQuery).ToList();
            

            foreach (var submittedPhoto in gallerySubmissionsSqlResult)
            {
                var awardCategory = _getAwardCategoryDetailsFilterByCategoryIdService.Get(umbracoHelper, submittedPhoto.CategoryId);
                var status = (CSTemplate.Code.Constants.Enums.SubmissionStatus)submittedPhoto.StatusEnumID;
                var photographerInfos = _getUserFilterByUserIdService.Get(umbracoHelper, submittedPhoto.UserId);
                var photographerFullName = photographerInfos.Name + " " + photographerInfos.Surname;
                listOfItemPhotoModel.Add(new _ItemPhotoModel()
                {
                    ItemPhotoImgURL = _getFirstPhotoUrlFilterBySubmissionIdService.Get(submittedPhoto.Id),
                    ItemPhotoDate = submittedPhoto.Date,
                    ItemPhotoName = photographerFullName,
                    ItemPhotoAward = awardCategory.AwardItem.AwardItemName,
                    ItemPhotoLocation = submittedPhoto.Location,
                    ItemPhotoDescription = submittedPhoto.Description,
                    ItemPhotoCategory = awardCategory.AwardCategoryItemName,
                    ItemPhotoTitle = submittedPhoto.Title,
                    SubmissionId = submittedPhoto.Id.ToString(),
                });
            }

            int itemTotalAmounts = gallerySubmissionsSqlResult.Count;
            //how many item to show on the page
            categoryGalleryPageModel.ItemsPerPage = CSTemplate.Code.Constants.ConstantValues.Mansory_Item_Count;
            var pageSize = categoryGalleryPageModel.ItemsPerPage;
            //total number of listing items.
            categoryGalleryPageModel.ItemsTotalAmount = itemTotalAmounts;

            listOfItemPhotoModel = listOfItemPhotoModel.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            var pagerModel = _getPagerModelService.CreatePagerModelForCurrentUrl(umbracoHelper, categoryGalleryPageModel.ItemsPerPage, categoryGalleryPageModel.ItemsTotalAmount);
            categoryGalleryPageModel.ListingItemsAsMasonryPartial = new _ListingItemsAsMasonryPartialModel()
            {
                Pagination = pagerModel,
                ListOfPhotos = listOfItemPhotoModel
            };
            return categoryGalleryPageModel;
        }
    }
}
