﻿using CSTemplate.Code.Models.Shared.Awards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CSTemplate.Code.Constants.Enums;

namespace CSTemplate.Code.Models.Shortlist_Photos
{
    public class _ShortListFilterFormModel
    {
        public string Title { get; set; }
        public string FilterButtonText { get; set; }
        public string SubmissionFilterLabelText{ get; set; }
        public SubmissionFilters? DateandNameFilter { get; set; }
        public string ShortListStatusLabelText { get; set; }
        public ShortListedStatusFilter? ShortListStatusFilter { get; set; }
        public int? AwardEditionId { get; set; }
        public string AwardEditonLabelText { get; set; }
        public List<_AwardItemModel> AwardEditionList { get; set; }
        public int? AwardEditionCategoryId { get; set; }
        public string AwardEditonCategoryLabelText { get; set; }
       // public List<_AwardItemModel> AwardEditionCategoryList { get; set; }
        public string SearchKeyFilterLabelText { get; set; }
        public string SearchKey { get; set; }
    }
}
