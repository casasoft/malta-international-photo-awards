﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Web;
using System.Web.Caching;

namespace CS.Common.Helpers
{
    public static class GeoIP
    {
       
        /// <summary>
        /// A method which will check if Request is coming from a SearchnEngineBot
        /// </summary>
        /// <returns>Returns true if request is coming from a bot. Returns false if request is not coming from a bot</returns>
        private static bool IsRequestSearchEngineBot()
        {
            var userAgent = HttpContext.Current.Request.UserAgent;

            if (!String.IsNullOrEmpty(userAgent))
            {
                var userAgentLower = userAgent.ToString().ToLower();

                if (
                      userAgentLower.IndexOf("008") > -1 ||
                      userAgentLower.IndexOf("abachobot") > -1 ||
                      userAgentLower.IndexOf("accoona-ai-agent") > -1 ||
                      userAgentLower.IndexOf("addsugarspiderbot") > -1 ||
                      userAgentLower.IndexOf("anyapexbot") > -1 ||
                      userAgentLower.IndexOf("arachmo") > -1 ||
                      userAgentLower.IndexOf("b-l-i-t-z-b-o-t") > -1 ||
                      userAgentLower.IndexOf("baiduspider") > -1 ||
                      userAgentLower.IndexOf("becomebot") > -1 ||
                      userAgentLower.IndexOf("beslistbot") > -1 ||
                      userAgentLower.IndexOf("billybobbot") > -1 ||
                      userAgentLower.IndexOf("bimbot") > -1 ||
                      userAgentLower.IndexOf("bingbot") > -1 ||
                      userAgentLower.IndexOf("blitzbot") > -1 ||
                      userAgentLower.IndexOf("boitho.com-dc") > -1 ||
                      userAgentLower.IndexOf("boitho.com-robot") > -1 ||
                      userAgentLower.IndexOf("btbot") > -1 ||
                      userAgentLower.IndexOf("catchbot") > -1 ||
                      userAgentLower.IndexOf("cerberian drtrs") > -1 ||
                      userAgentLower.IndexOf("charlotte") > -1 ||
                      userAgentLower.IndexOf("converacrawler") > -1 ||
                      userAgentLower.IndexOf("cosmos") > -1 ||
                      userAgentLower.IndexOf("covario ids") > -1 ||
                      userAgentLower.IndexOf("dataparksearch") > -1 ||
                      userAgentLower.IndexOf("diamondbot") > -1 ||
                      userAgentLower.IndexOf("discobot") > -1 ||
                      userAgentLower.IndexOf("dotbot") > -1 ||
                      userAgentLower.IndexOf("earthcom.info") > -1 ||
                      userAgentLower.IndexOf("emeraldshield.com webbot") > -1 ||
                      userAgentLower.IndexOf("envolk[its]spider") > -1 ||
                      userAgentLower.IndexOf("esperanzabot") > -1 ||
                      userAgentLower.IndexOf("exabot") > -1 ||
                      userAgentLower.IndexOf("fast enterprise crawler") > -1 ||
                      userAgentLower.IndexOf("fast-webcrawler") > -1 ||
                      userAgentLower.IndexOf("fdse robot") > -1 ||
                      userAgentLower.IndexOf("findlinks") > -1 ||
                      userAgentLower.IndexOf("furlbot") > -1 ||
                      userAgentLower.IndexOf("fyberspider") > -1 ||
                      userAgentLower.IndexOf("g2crawler") > -1 ||
                      userAgentLower.IndexOf("gaisbot") > -1 ||
                      userAgentLower.IndexOf("galaxybot") > -1 ||
                      userAgentLower.IndexOf("geniebot") > -1 ||
                      userAgentLower.IndexOf("gigabot") > -1 ||
                      userAgentLower.IndexOf("girafabot") > -1 ||
                      userAgentLower.IndexOf("googlebot") > -1 ||
                      userAgentLower.IndexOf("googlebot-image") > -1 ||
                      userAgentLower.IndexOf("gurujibot") > -1 ||
                      userAgentLower.IndexOf("happyfunbot") > -1 ||
                      userAgentLower.IndexOf("hl_ftien_spider") > -1 ||
                      userAgentLower.IndexOf("holmes") > -1 ||
                      userAgentLower.IndexOf("htdig") > -1 ||
                      userAgentLower.IndexOf("iaskspider") > -1 ||
                      userAgentLower.IndexOf("ia_archiver") > -1 ||
                      userAgentLower.IndexOf("iccrawler") > -1 ||
                      userAgentLower.IndexOf("ichiro") > -1 ||
                      userAgentLower.IndexOf("igdespyder") > -1 ||
                      userAgentLower.IndexOf("irlbot") > -1 ||
                      userAgentLower.IndexOf("issuecrawler") > -1 ||
                      userAgentLower.IndexOf("jaxified bot") > -1 ||
                      userAgentLower.IndexOf("jyxobot") > -1 ||
                      userAgentLower.IndexOf("koepabot") > -1 ||
                      userAgentLower.IndexOf("l.webis") > -1 ||
                      userAgentLower.IndexOf("lapozzbot") > -1 ||
                      userAgentLower.IndexOf("larbin") > -1 ||
                      userAgentLower.IndexOf("ldspider") > -1 ||
                      userAgentLower.IndexOf("lexxebot") > -1 ||
                      userAgentLower.IndexOf("linguee bot") > -1 ||
                      userAgentLower.IndexOf("linkwalker") > -1 ||
                      userAgentLower.IndexOf("lmspider") > -1 ||
                      userAgentLower.IndexOf("lwp-trivial") > -1 ||
                      userAgentLower.IndexOf("mabontland") > -1 ||
                      userAgentLower.IndexOf("magpie-crawler") > -1 ||
                      userAgentLower.IndexOf("mediapartners-google") > -1 ||
                      userAgentLower.IndexOf("mj12bot") > -1 ||
                      userAgentLower.IndexOf("mlbot") > -1 ||
                      userAgentLower.IndexOf("mnogosearch") > -1 ||
                      userAgentLower.IndexOf("mogimogi") > -1 ||
                      userAgentLower.IndexOf("mojeekbot") > -1 ||
                      userAgentLower.IndexOf("moreoverbot") > -1 ||
                      userAgentLower.IndexOf("morning paper") > -1 ||
                      userAgentLower.IndexOf("msnbot") > -1 ||
                      userAgentLower.IndexOf("msrbot") > -1 ||
                      userAgentLower.IndexOf("mvaclient") > -1 ||
                      userAgentLower.IndexOf("mxbot") > -1 ||
                      userAgentLower.IndexOf("netresearchserver") > -1 ||
                      userAgentLower.IndexOf("netseer crawler") > -1 ||
                      userAgentLower.IndexOf("newsgator") > -1 ||
                      userAgentLower.IndexOf("ng-search") > -1 ||
                      userAgentLower.IndexOf("nicebot") > -1 ||
                      userAgentLower.IndexOf("noxtrumbot") > -1 ||
                      userAgentLower.IndexOf("nusearch spider") > -1 ||
                      userAgentLower.IndexOf("nutchcvs") > -1 ||
                      userAgentLower.IndexOf("nymesis") > -1 ||
                      userAgentLower.IndexOf("obot") > -1 ||
                      userAgentLower.IndexOf("oegp") > -1 ||
                      userAgentLower.IndexOf("omgilibot") > -1 ||
                      userAgentLower.IndexOf("omniexplorer_bot") > -1 ||
                      userAgentLower.IndexOf("oozbot") > -1 ||
                      userAgentLower.IndexOf("orbiter") > -1 ||
                      userAgentLower.IndexOf("pagebiteshyperbot") > -1 ||
                      userAgentLower.IndexOf("peew") > -1 ||
                      userAgentLower.IndexOf("polybot") > -1 ||
                      userAgentLower.IndexOf("pompos") > -1 ||
                      userAgentLower.IndexOf("postpost") > -1 ||
                      userAgentLower.IndexOf("psbot") > -1 ||
                      userAgentLower.IndexOf("pycurl") > -1 ||
                      userAgentLower.IndexOf("qseero") > -1 ||
                      userAgentLower.IndexOf("radian6") > -1 ||
                      userAgentLower.IndexOf("rampybot") > -1 ||
                      userAgentLower.IndexOf("rufusbot") > -1 ||
                      userAgentLower.IndexOf("sandcrawler") > -1 ||
                      userAgentLower.IndexOf("sbider") > -1 ||
                      userAgentLower.IndexOf("scoutjet") > -1 ||
                      userAgentLower.IndexOf("scrubby") > -1 ||
                      userAgentLower.IndexOf("searchsight") > -1 ||
                      userAgentLower.IndexOf("seekbot") > -1 ||
                      userAgentLower.IndexOf("semanticdiscovery") > -1 ||
                      userAgentLower.IndexOf("sensis web crawler") > -1 ||
                      userAgentLower.IndexOf("seochat::bot") > -1 ||
                      userAgentLower.IndexOf("seznambot") > -1 ||
                      userAgentLower.IndexOf("shim-crawler") > -1 ||
                      userAgentLower.IndexOf("shopwiki") > -1 ||
                      userAgentLower.IndexOf("shoula robot") > -1 ||
                      userAgentLower.IndexOf("silk") > -1 ||
                      userAgentLower.IndexOf("sitebot") > -1 ||
                      userAgentLower.IndexOf("snappy") > -1 ||
                      userAgentLower.IndexOf("sogou spider") > -1 ||
                      userAgentLower.IndexOf("sosospider") > -1 ||
                      userAgentLower.IndexOf("speedy spider") > -1 ||
                      userAgentLower.IndexOf("sqworm") > -1 ||
                      userAgentLower.IndexOf("stackrambler") > -1 ||
                      userAgentLower.IndexOf("suggybot") > -1 ||
                      userAgentLower.IndexOf("surveybot") > -1 ||
                      userAgentLower.IndexOf("synoobot") > -1 ||
                      userAgentLower.IndexOf("teoma") > -1 ||
                      userAgentLower.IndexOf("terrawizbot") > -1 ||
                      userAgentLower.IndexOf("thesubot") > -1 ||
                      userAgentLower.IndexOf("thumbnail.cz robot") > -1 ||
                      userAgentLower.IndexOf("tineye") > -1 ||
                      userAgentLower.IndexOf("truwogps") > -1 ||
                      userAgentLower.IndexOf("turnitinbot") > -1 ||
                      userAgentLower.IndexOf("tweetedtimes bot") > -1 ||
                      userAgentLower.IndexOf("twengabot") > -1 ||
                      userAgentLower.IndexOf("updated") > -1 ||
                      userAgentLower.IndexOf("urlfilebot") > -1 ||
                      userAgentLower.IndexOf("vagabondo") > -1 ||
                      userAgentLower.IndexOf("voilabot") > -1 ||
                      userAgentLower.IndexOf("vortex") > -1 ||
                      userAgentLower.IndexOf("voyager") > -1 ||
                      userAgentLower.IndexOf("vyu2") > -1 ||
                      userAgentLower.IndexOf("webcollage") > -1 ||
                      userAgentLower.IndexOf("websquash.com") > -1 ||
                      userAgentLower.IndexOf("wf84") > -1 ||
                      userAgentLower.IndexOf("wofindeich robot") > -1 ||
                      userAgentLower.IndexOf("womlpefactory") > -1 ||
                      userAgentLower.IndexOf("xaldon_webspider") > -1 ||
                      userAgentLower.IndexOf("yacy") > -1 ||
                      userAgentLower.IndexOf("yahoo! slurp") > -1 ||
                      userAgentLower.IndexOf("yahoo! slurp china") > -1 ||
                      userAgentLower.IndexOf("yahooseeker") > -1 ||
                      userAgentLower.IndexOf("yahooseeker-testing") > -1 ||
                      userAgentLower.IndexOf("yandexbot") > -1 ||
                      userAgentLower.IndexOf("yandeximages") > -1 ||
                      userAgentLower.IndexOf("yandexmetrika") > -1 ||
                      userAgentLower.IndexOf("yasaklibot") > -1 ||
                      userAgentLower.IndexOf("yeti") > -1 ||
                      userAgentLower.IndexOf("yodaobot") > -1 ||
                      userAgentLower.IndexOf("yooglifetchagent") > -1 ||
                      userAgentLower.IndexOf("youdaobot") > -1 ||
                      userAgentLower.IndexOf("zao") > -1 ||
                      userAgentLower.IndexOf("zealbot") > -1 ||
                      userAgentLower.IndexOf("zspider") > -1 ||
                      userAgentLower.IndexOf("zyborg") > -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Using a third party sevice to get the country code by Ip Address - http://ip-api.com/
        /// </summary>
        /// <param name="ipAddress">User's Ip Address</param>
        /// <returns>Returns the country code (as in uppercase) where the user comes from</returns>
        private static string GetCountryCode(string ipAddress)
        {

            Uri myUri = new Uri("http://ip-api.com/json/" + ipAddress);

            string value = new WebClient().DownloadString(myUri);

            dynamic valueAsJson = JsonConvert.DeserializeObject(value);

            string countryCode = valueAsJson.countryCode;

            if (!String.IsNullOrEmpty(countryCode))
            {
                return countryCode.ToUpper();
            }
            else
            {
                return "";
            }

        }

        /// <summary>
        /// A method which request server variables to get the USER IP Address who is currently accessing the website
        /// </summary>
        /// <returns>Returns user IP Address</returns>
        private static string GetUser_IP()
        {
            string visitorIPAddr = string.Empty;

            if (HttpContext.Current.Request != null)
            {
                string httpXForwardedFor = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (httpXForwardedFor != null)
                {
                    visitorIPAddr = httpXForwardedFor.ToString();
                }
                else
                {
                    visitorIPAddr = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                    if (!String.IsNullOrEmpty(visitorIPAddr))
                    {
                        visitorIPAddr = visitorIPAddr.ToString();
                    }
                }
            }

            return visitorIPAddr;
        }

    
    }
}