﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSTemplate.Code.Models.Shared;

using CSTemplate.Code.Models.USNModels;
using Umbraco.Core.Models;

namespace CSTemplate.Code.Models.OrderSummary
{
    public class OrderSummaryPageModel : USNBaseViewModel
    {
        public string OrderRefId { get; set; }
        public string TransactionDescription { get; set; }
        public string ClientEmailAddress { get; set; }

        public List<_OrderPhotosModel> ListOfOrderPhotos { get; set; }
        public string TablePhotoTitle { get; set; }
        public string TablePriceTitle { get; set; }
        public string TableSubtotalTitle { get; set; }
        public decimal SubtotalPrice { get; set; }
        public _PaymentButtonsModel PaymentButtons { get; set; }
        public bool IsAStoryTelling { get; set; }
        public OrderSummaryPageModel(IPublishedContent content) : base(content)
        {

        }
    }
}
