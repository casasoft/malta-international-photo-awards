﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CS.Common.Helpers;
using CS.Common.Umbraco.Helpers;
using CS.Common.Umbraco.Models.CsvExport;
using CS.Common.Umbraco.Services.Umbraco.Wrappers;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;

namespace CS.Common.Umbraco.Controllers
{
    public class PartialsCsvExportController : SurfaceController
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IUmbracoControllerWrapperService _controllerWrapperService;

        public PartialsCsvExportController(IUmbracoHelperService umbracoHelperService,
            IUmbracoControllerWrapperService controllerWrapperService)
        {
            _controllerWrapperService = controllerWrapperService;
            _umbracoHelperService = umbracoHelperService;
        }

        [HttpPost]
        public FileContentResult ExportAsCsv(CsvExportPartialModel csvExportInfo)
        {

            var src = Services.ContentService.GetById(csvExportInfo.Source);

            if (src != null)
            {
                var csv = String.Empty;

                // Set a value for Delimiter if none exists
                if (string.IsNullOrEmpty(csvExportInfo.Delimiter))
                {
                    csvExportInfo.Delimiter = ",";
                }

                // Set a value for documentTypeAlias if none exists
                if (string.IsNullOrEmpty(csvExportInfo.DocumentTypeAlias))
                {
                    csvExportInfo.DocumentTypeAlias = src.ContentType.Alias;
                }

                var children = src.Children();
                var getChildrenOfChildrenSource = csvExportInfo.GetChildrenOfChildrenOfSource;
                // If getChildrenOfSource and documentTypeAlias is specified
                if (csvExportInfo.GetChildrenOfSource && !String.IsNullOrEmpty(csvExportInfo.DocumentTypeAlias) && children.Any())
                {
                    // LINQ expression that extracts all nodes that matches the specified documentTypeAlias
                    var nodeCollection = children.Where(x => x.ContentType.Alias == csvExportInfo.DocumentTypeAlias).ToList();

                    // Print headers from first node
                    csv = WritePropertyHeaders(nodeCollection[0], csvExportInfo.Delimiter, csv, getChildrenOfChildrenSource, csvExportInfo.ChildrenOfChildrenOfSourceHeaderTitle);


                    // Print values for all nodes
                    foreach (var n in nodeCollection)
                    {
                        csv = WritePropertyValues(n, n.Id, getChildrenOfChildrenSource, csvExportInfo.Delimiter, csvExportInfo.RemoveLineBreaks, csvExportInfo.EncodeHtmlTags, csvExportInfo.StripHtmlTags, csv);
                    }
                }
                // Default action
                else
                {
                    csv = WritePropertyHeaders(src, csvExportInfo.Delimiter, csv, getChildrenOfChildrenSource, csvExportInfo.ChildrenOfChildrenOfSourceHeaderTitle);
                    csv = WritePropertyValues(src, csvExportInfo.Source, false, csvExportInfo.Delimiter, csvExportInfo.RemoveLineBreaks, csvExportInfo.EncodeHtmlTags, csvExportInfo.StripHtmlTags, csv);
                }

                return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", src.Name + ".csv");
            }
            else
            {
                throw new NullReferenceException("Something went wrong with the Source! \nIs the referenced node published?");
            }
        }

        /// 
        /// Writes the property headers to page.
        /// 
        /// The node.
        public string WritePropertyHeaders(IContent elem, string delimiter, string csv, bool includeChildrenOfChildrenSource, string childrenOfChildrenSrouceHeaderTitle)
        {
            // Print the Umbraco property names
            csv = csv + "id" + delimiter;
            csv = csv + "createdDate" + delimiter;
            csv = csv + "lastEditedDate" + delimiter;

            // Going through every property and lists the title;
            foreach (Property p in elem.Properties)
            {
                csv = csv + p.Alias + delimiter;
            }

            if (includeChildrenOfChildrenSource)
            {
                csv = csv + childrenOfChildrenSrouceHeaderTitle + delimiter;
            }

            return csv;
        }

        /// 
        /// Writes the property values to page.
        /// 
        /// The node.
        public string WritePropertyValues(IContent elem, int elemId, bool getChildrenOfChildren, string delimiter, bool removeLineBreaks, bool encodeHtmlTags, bool stripHtmlTags, string csv)
        {
            csv = csv + "\r\n";

            // Print the Umbraco property names
            csv = PrintString(elem.Id.ToString(CultureInfo.InvariantCulture), delimiter, csv);
            csv = csv + delimiter;
            csv = PrintString(DateHelpers.FormatDate(elem.CreateDate), delimiter, csv);
            csv = csv + delimiter;
            csv = PrintString(DateHelpers.FormatDate(elem.UpdateDate), delimiter, csv);
            csv = csv + delimiter;

            // Going through every property and lists the value
            foreach (Property p in elem.Properties)
            {
                var result = DocumentTypeHelpers.ConvertDocumentTypePropertyObjectValueToString(p.Value, p.PropertyType.PropertyEditorAlias, _controllerWrapperService.GetUmbracoHelperForUmbracoController(this));

                // Remove linebreaks if specified
                if (removeLineBreaks)
                {
                    result = result.Replace("\r\n", " ");
                }

                // Trim leading and trailing white-space
                result = result.Trim();

                // Encase double quotes in double quotes
                result = result.Replace("\"", "\"\"");

                // If macro is set to encode HTML tags))
                if (encodeHtmlTags && !stripHtmlTags)
                {
                    result = HttpUtility.HtmlEncode(result);
                }

                // If macro is set to remove HTML tags. (Ignores the encodeHtmlTags property)
                if (stripHtmlTags)
                {
                    result = StripTagsCharArray(result);
                }

                csv = PrintString(result, delimiter, csv);
                csv = csv + delimiter;
            }

            if (getChildrenOfChildren)
            {
                var childrenOfSource = _umbracoHelperService.TypedContent(_controllerWrapperService.GetUmbracoHelperForUmbracoController(this), elemId).Children;

                var result = "";
                foreach (var childOfSource in childrenOfSource)
                {
                    var src = Services.ContentService.GetById(childOfSource.Id);

                    int cnt = 1;

                    foreach (Property childp in src.Properties)
                    {
                        result = result + DocumentTypeHelpers.ConvertDocumentTypePropertyObjectValueToString(childp.Value, childp.PropertyType.PropertyEditorAlias, _controllerWrapperService.GetUmbracoHelperForUmbracoController(this));

                        if (cnt < src.Properties.Count())
                        {
                            result = result + " - ";
                        }

                        cnt++;
                    }

                    result = result + " / ";
                }

                csv = PrintString(result, delimiter, csv);
                csv = csv + delimiter;
            }

            return csv;
        }

        public string PrintString(string stringToPrint, string delimiter, string csv)
        {
            // Check of string contains special characters. If the do, enclose with double quotes
            // If result contains special characters, surround with double quotes
            if (stringToPrint.Contains("\n") || stringToPrint.Contains("\r") || stringToPrint.Contains("\"") || stringToPrint.Contains(delimiter) || stringToPrint.Length == 0)
            {
                csv = csv + "\"" + stringToPrint + "\"";
            }
            else
            {
                csv = csv + stringToPrint;
            }

            return csv;
        }

        /// 
        /// Remove HTML tags from string using char array.
        /// 
        /// The string containing HTML tags.
        public string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            foreach (char let in source)
            {
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
}
