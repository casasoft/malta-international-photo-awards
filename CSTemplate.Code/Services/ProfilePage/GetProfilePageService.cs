﻿using System;
using CS.Common.Helpers;
using CS.Common.Services;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.MyProfile;
using CSTemplate.Code.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;
using ConstantValues = CS.Common.Umbraco.Constants.ConstantValues;

namespace CSTemplate.Code.Services.ProfilePage
{
    public interface IGetProfilePageService
    {
        MyProfilePageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content,
             IMember currentMember, Umbraco.Web.PublishedContentModels.Member currentMemberAsMemberModel);
    }

    [Service]
    public class GetProfilePageService : IGetProfilePageService
    {
        private readonly IUmbracoHelperService _umbracoHelperService;

        public GetProfilePageService(
            IUmbracoHelperService umbracoHelperService)
        {
            _umbracoHelperService = umbracoHelperService;
        }

        public MyProfilePageModel Get(UmbracoHelper umbracoHelper, IPublishedContent content,
            IMember currentMember,
            Umbraco.Web.PublishedContentModels.Member currentMemberAsMemberModel)
        {
            var termsAndConditionsRawText =
                _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                    Enums.DictionaryKey.Dictionary_Form_Terms_And_Conditions_Text);
            var termsAndConditionsUrl = _umbracoHelperService.TypedContent(umbracoHelper, ConstantValues.Terms_And_Conditions_Page_Id).Url;

            var termsAndConditionsFormatted = String.Format(termsAndConditionsRawText, termsAndConditionsUrl);

             var titleFormText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Profile_Page_Title_Form_Text);
            var subTitleFormText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Profile_Page_SubTitle_Form_Text);

            var requiredText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Dictionary_Form_Required_Text);

            var promotionalMaterial = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Dictionary_Form_Promotional_Material_Text);

            var buttonFormText = _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(umbracoHelper,
                Enums.DictionaryKey.Profile_Page_Button_Form_Text);


            MyProfilePageModel myProfilePageModel = new MyProfilePageModel(content)
            {
                UpdateProfileFormModel = new _RegisterFormModel()
                {
                    Name = currentMemberAsMemberModel.ClientName,
                    Surname = currentMemberAsMemberModel.ClientSurname,
                    InsertedEmail = currentMember.Email,
                    //InsertedPhone = currentMemberAsMemberModel.Phone,
                    InsertedCountry = EnumHelpers.ConvertStringToEnum<CS.Common.Constants.Enums.CountryIso3166>(currentMemberAsMemberModel.Country),
                    Nationality = currentMemberAsMemberModel.Nationality,
                    PromotionalMaterials = currentMemberAsMemberModel.AgreeToReceivePromotionalMaterials,

                    //Title = titleFormText,
                    //SubTitle = subTitleFormText,
                    //Required = requiredText,
                    //PromotionalText = promotionalMaterial,
                    //TermsAndConditionsText = termsAndConditionsFormatted,
                    //ButtonFormText = buttonFormText,
                    TitleForm = "Update Profile",
                    YourDetailsSectionTitle = "YOUR DETAILS",
                    RegisterRequiredField = "Required",
                    TermsAndConditionsText = termsAndConditionsFormatted,
                    //TermsAndConditionsDescriptionParagraph = "I agree to the ",
                    //TermsAndConditionsLinkText = "terms and conditions",
                    //TermsAndConditionsLinkURL = "/",
                    PromotionalMaterialsDescriptionParagraph = " I agree to receive promotional materials from MIPA",
                    ButtonFormText = "Update",
                    //todo: [For: backend | 2018/08/17] this property below should be FALSE on the Update Profile Page (WrittenBy: Radek)        			
                    IsRegistrationProcess = false
                }

            };

            return myProfilePageModel;
        }
    }
}