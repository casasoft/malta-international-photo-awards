﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.Common.Umbraco.Services.Umbraco.Wrappers.Services;
using CSTemplate.Code.Constants;
using CSTemplate.Code.Models.Shared;
using CSTemplate.Code.Services.ResetPassword;
using Umbraco.Web.Mvc;

namespace CasaSoftUmbracoUSkinnedTemplate.Controllers
{
    public partial class ResetPasswordFormController : SurfaceController
    {
        private readonly IUmbracoHelperService _umbracoHelperService;
        private readonly IUmbracoControllerWrapperService _umbracoControllerWrapperService;
        private readonly IGetHandleResetPasswordSubmitFormService _getHandleResetPasswordSubmitFormService;

        public ResetPasswordFormController(IUmbracoHelperService
            umbracoHelperService,
            IUmbracoControllerWrapperService
            umbracoControllerWrapperService,
            IGetHandleResetPasswordSubmitFormService
            getHandleResetPasswordSubmitFormService)
        {
            _getHandleResetPasswordSubmitFormService = getHandleResetPasswordSubmitFormService;
            _umbracoControllerWrapperService = umbracoControllerWrapperService;
            _umbracoHelperService = umbracoHelperService;
        }

        [HttpPost]
        public virtual ActionResult HandleResetPasswordFormSubmit(_ResetPasswordFormModel resetPasswordFormModel)
        {
            var umbracoHelper = _umbracoControllerWrapperService.GetUmbracoHelperForUmbracoController(this);

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Model", _umbracoHelperService.GetDictionaryValueOrUpdateDefaultValueIfEmpty(
                  umbracoHelper, Enums.DictionaryKey.Unexpected_Error_Text));

                return CurrentUmbracoPage();
            }
            else
            {
                //calling service to reset user password
                var result = _getHandleResetPasswordSubmitFormService.HandleResetPasswordSubmitForm(umbracoHelper, resetPasswordFormModel);

                if (result.WasSuccess)
                {
                    var resetPasswordSuccessPage = _umbracoHelperService.LoadRootNodeForCurrentDomain(umbracoHelper)
                   .Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Login_Page_Document_Type_Alias)).FirstOrDefault()
                   .Children.Where(x => x.DocumentTypeAlias.Equals(ConstantValues.Reset_Password_Document_Type_Alias))
                   .FirstOrDefault().Children.FirstOrDefault();

                        return RedirectToUmbracoPage(resetPasswordSuccessPage);
                }
                else
                {
                    ModelState.AddModelError("Model", result.Message);

                    return CurrentUmbracoPage();
                }

            }
        }
    }
}